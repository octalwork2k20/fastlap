package com.os.fastlap.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;


/**
 * Created by monikab on 6/16/2017.
 */

public class ChooseImagePickerOptionDialog extends BottomSheetDialog implements View.OnClickListener {

    Context mContext;
    private View mView;
    private TextView takeCameraBt;
    private TextView chooseLibraryBt;
    private TextView take_video_bt;
    private Button cancelBtn;
    private RelativeLayout rrTakeVideo;
    OnImagePickerDialogSelect onImagePickerDialogSelect;

    public ChooseImagePickerOptionDialog(@NonNull Context context, OnImagePickerDialogSelect onImagePickerDialogSelect) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.choose_image_option);
        mView = getWindow().getDecorView();
        mContext = context;
        this.onImagePickerDialogSelect = onImagePickerDialogSelect;
        initView();
    }

    public void makeVisible() {
        rrTakeVideo.setVisibility(View.VISIBLE);
    }

    private void initView() {
        takeCameraBt = (TextView) findViewById(R.id.take_camera_bt);
        chooseLibraryBt = (TextView) findViewById(R.id.choose_library_bt);
        cancelBtn = (Button) findViewById(R.id.cancel_btn);
        take_video_bt = findViewById(R.id.take_video_bt);
        rrTakeVideo = findViewById(R.id.rrTakeVideo);
        rrTakeVideo.setVisibility(View.GONE);
        setOnClickListener();
    }

    public void changeText() {
        takeCameraBt.setText(mContext.getString(R.string.share_on_your_timeline));
        chooseLibraryBt.setText(mContext.getString(R.string.share_on_fb));
    }

    private void setOnClickListener() {
        takeCameraBt.setOnClickListener(this);
        chooseLibraryBt.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        take_video_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.take_camera_bt:
                onImagePickerDialogSelect.OnCameraSelect();
                dismiss();
                break;
            case R.id.choose_library_bt:
                onImagePickerDialogSelect.OnGallerySelect();
                dismiss();
                break;
            case R.id.cancel_btn:
                dismiss();
                break;
            case R.id.take_video_bt:
                onImagePickerDialogSelect.onVideoSelect();
                dismiss();
                break;
        }

    }

}
