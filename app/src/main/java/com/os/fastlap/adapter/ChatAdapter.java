package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanMessage;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by monikab on 3/16/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    public ArrayList<BeanMessage> messageList = new ArrayList<>();
    Context mContext;

    public ChatAdapter(Context context, ArrayList<BeanMessage> messageList) {
        this.messageList = messageList;
        mContext = context;
    }

    // Create new views
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_chat_row, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int pos) {
        try {

            BeanMessage beanMessage = new BeanMessage();
            beanMessage = messageList.get(pos);

            if (beanMessage.getMessageReciverId().compareToIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)) == 0) {
                holder.chatRowsLinerIn.setVisibility(View.VISIBLE);
                holder.chatRowsLinerOut.setVisibility(View.GONE);
                String message = beanMessage.getMessageText();

               /* byte[] data = Base64.decode(message, Base64.NO_WRAP);
                try {
                    message = new String(data, "UTF-8");
                }catch (Exception e){}*/

                holder.clientRowMsgIn.setText(message);
                Log.e("tag","data"+beanMessage.getMessageDate());
               // holder.clientRowDateIn.setText(Util.parseDateToHMA(beanMessage.getMessageDate()));
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanMessage.getMessageSenderImage(), holder.partnerImg, Util.getImageLoaderOption(mContext));
            } else {
                holder.chatRowsLinerIn.setVisibility(View.GONE);
                holder.chatRowsLinerOut.setVisibility(View.VISIBLE);

                String message = beanMessage.getMessageText();

               /* byte[] data = Base64.decode(message, Base64.NO_WRAP);
                try {
                    message = new String(data, "UTF-8");
                }catch (Exception e){}*/


                holder.clientRowMsgOut.setText(message);
                holder.clientRowDateOut.setText(Util.parseDateToHMAMy(beanMessage.getMessageDate()));
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanMessage.getMessageSenderImage(), holder.userImg, Util.getImageLoaderOption(mContext));

            }

            //holder.chatTvDate.setText(Util.compareDate2(beanMessage.getMessageDate()));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView chatTvDate;
        private LinearLayout chatRowsLinerIn;
        private CircleImageView partnerImg;
        private LinearLayout ChatRowIn;
        private TextView clientRowMsgIn;
        private TextView clientRowDateIn;
        private LinearLayout chatRowsLinerOut;
        private LinearLayout ChatRowOut;
        private TextView clientRowMsgOut;
        private TextView clientRowDateOut;
        private CircleImageView userImg;

        ViewHolder(View convertView) {
            super(convertView);
            chatTvDate = (TextView) convertView.findViewById(R.id.chat_tv_date);
            chatRowsLinerIn = (LinearLayout) convertView.findViewById(R.id.chat_rows_liner_in);
            partnerImg = (CircleImageView) convertView.findViewById(R.id.partner_img);
            ChatRowIn = (LinearLayout) convertView.findViewById(R.id.ChatRow_in);
            clientRowMsgIn = (TextView) convertView.findViewById(R.id.client_row_msg_in);
            clientRowDateIn = (TextView) convertView.findViewById(R.id.client_row_date_in);
            chatRowsLinerOut = (LinearLayout) convertView.findViewById(R.id.chat_rows_liner_out);
            ChatRowOut = (LinearLayout) convertView.findViewById(R.id.ChatRow_out);
            clientRowMsgOut = (TextView) convertView.findViewById(R.id.client_row_msg_out);
            clientRowDateOut = (TextView) convertView.findViewById(R.id.client_row_date_out);
            userImg = (CircleImageView) convertView.findViewById(R.id.user_img);
        }
    }


   /* public void setChatData(ArrayList<String> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }*/

}
