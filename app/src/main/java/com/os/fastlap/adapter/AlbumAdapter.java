package com.os.fastlap.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/*
 * Created by monikab on 3/16/2017.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {

    public ArrayList<BeanAlbum.BeanFiles> imageList = new ArrayList<>();
    Context mContext;
    String presetImage;

    public AlbumAdapter(Context context, ArrayList<BeanAlbum.BeanFiles> imageList, String presetImage) {
        this.imageList = imageList;
        this.presetImage=presetImage;
        mContext = context;
    }

    // Create new views
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_list_row, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int pos) {
        BeanAlbum.BeanFiles beanFiles = new BeanAlbum().new BeanFiles();
        beanFiles = imageList.get(pos);
        if(!presetImage.equalsIgnoreCase(""))
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + presetImage, holder.imgBanner);
        else
            holder.imgBanner.setVisibility(View.GONE);

        if (beanFiles.getType().compareTo("1") == 0) {
            holder.play_iv.setVisibility(View.GONE);
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanFiles.getFileName(), holder.image, Util.getImageLoaderOption(mContext));
        } else {
            holder.play_iv.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanFiles.getThumbName(), holder.image, Util.getImageLoaderOption(mContext));
        }
        if(beanFiles.isSelected()){
            holder.image.setAlpha((float) 0.5);
        }
        else {
            holder.image.setAlpha((float) 0.9);
        }
    }
    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image,imgBanner;
        private ImageView play_iv;
        private RelativeLayout lytPhoto;

        ViewHolder(View convertView) {
            super(convertView);

            image = (ImageView) convertView.findViewById(R.id.image);
            play_iv = (ImageView) convertView.findViewById(R.id.play_iv);
            lytPhoto=(RelativeLayout)convertView.findViewById(R.id.lytPhoto);
            imgBanner=(ImageView)convertView.findViewById(R.id.imgBanner);

            play_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(S.IMAGE_BASE_URL + imageList.get(pos).getFileName()));
                        intent.setDataAndType(Uri.parse(S.IMAGE_BASE_URL + imageList.get(pos).getFileName()), "video/*");
                        mContext.startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.no_supportable_Player), Toast.LENGTH_LONG).show();
                    }
                }
            });
            /*image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArrayList<String> list = new ArrayList<>();

                    for (BeanAlbum.BeanFiles beanFiles : imageList) {
                        list.add(S.IMAGE_BASE_URL + beanFiles.getFileName());
                    }
                    new ImageViewer.Builder(mContext, list)
                            .setStartPosition(getAdapterPosition())
                            .allowZooming(true)
                            .show();
                }
            });*/

        }


    }
}
