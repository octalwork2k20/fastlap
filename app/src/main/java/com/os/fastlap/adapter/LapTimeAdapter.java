package com.os.fastlap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.ItemCoord;
import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;
import com.os.fastlap.R;
import com.os.fastlap.beans.SessionsBean;

import java.util.ArrayList;

/**
 * Created by anandj on 7/19/2017.
 */

public class LapTimeAdapter extends SectionedRecyclerViewAdapter<LapTimeAdapter.MainVH> {
    Context mContext;
    private ArrayList<SessionsBean> mLapTime;
    onSelected onselected;
    private boolean onBind;

    public interface onSelected {
        void CheckedChange(boolean status, int position);
    }

    public void setonListner(onSelected onselected) {
        this.onselected = onselected;
    }

    public LapTimeAdapter(Context context, ArrayList<SessionsBean> arrayList) {
        mContext = context;
        this.mLapTime = arrayList;
    }

    @Override
    public int getSectionCount() {
        return mLapTime.size(); // number of sections, you would probably base this on a data set such as a map
    }

    @Override
    public int getItemCount(int sectionIndex) {
        return mLapTime.get(sectionIndex).getLapArray().size(); // number of items in section, you could also pull this from a map of lists
    }

    @Override
    public void onBindHeaderViewHolder(MainVH holder, int section, boolean expanded) {
        // Setup header view
        holder.header_tv.setText(mContext.getString(R.string.sessions) + " " + mLapTime.get(section).getSessionId());
    }

    @Override
    public void onBindViewHolder(MainVH holder, int section, int relativePosition, int absolutePosition) {
        // Setup non-header view.
        // 'section' is section index.
        // 'relativePosition' is index in this section.
        // 'absolutePosition' is index out of all items, including headers and footers.
        // See sample project for a visual of how these indices work.
        onBind = true;

        holder.lap_count_tv.setText(mLapTime.get(section).getLapArray().get(relativePosition).getLap() + ".");
        holder.lap_time_tv.setText(mLapTime.get(section).getLapArray().get(relativePosition).getLapTime());

        if (mLapTime.get(section).getLapArray().get(relativePosition).isSelected()) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }
        onBind = false;

    }

    @Override
    public void onBindFooterViewHolder(MainVH holder, int section) {
        // Setup footer view, if footers are enabled (see the next section)
    }

    @Override
    public MainVH onCreateViewHolder(ViewGroup parent, int viewType) {
        // Change inflated layout based on type
        int layoutRes;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layoutRes = R.layout.section_header;
                break;
            default:
                layoutRes = R.layout.lap_time_list_item;
                break;
        }
        View v = LayoutInflater.from(parent.getContext())
                .inflate(layoutRes, parent, false);
        return new MainVH(v);
    }

    public class MainVH extends SectionedViewHolder
            implements CheckBox.OnCheckedChangeListener {
        TextView lap_count_tv;
        TextView lap_time_tv;
        CheckBox checkbox;
        TextView header_tv;

        public MainVH(View itemView) {
            super(itemView);

            lap_count_tv = itemView.findViewById(R.id.lap_count_tv);
            lap_time_tv = itemView.findViewById(R.id.lap_time_tv);
            checkbox = itemView.findViewById(R.id.checkbox);
            header_tv = itemView.findViewById(R.id.header_tv);

            if (checkbox != null) {
                checkbox.setOnCheckedChangeListener(this);
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b)
        {
            boolean isHeader = isHeader();
            boolean isFooter = isFooter();
            ItemCoord position = getRelativePosition();
            int section = position.section();
            int relativePos = position.relativePos();
            mLapTime.get(section).getLapArray().get(relativePos).setSelected(b);
            if (!onBind) {
                notifyDataSetChanged();
            }
        }
    }
}
