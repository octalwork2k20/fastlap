package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.ChatActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;
import java.util.Locale;



/*
 * Created by anandj on 8/25/2017.
 */

public class AllFriendsProfileAdapter extends RecyclerView.Adapter<AllFriendsProfileAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    private ArrayList<ProfileMyFriendsList> mMyFriendsListsAll = new ArrayList<>();
    AuthAPI authAPI;

    public AllFriendsProfileAdapter(Context context, ArrayList<ProfileMyFriendsList> friendsLists) {
        this.mContext = context;
        this.mMyFriendsLists = friendsLists;
        this.mMyFriendsListsAll.addAll(friendsLists);
        authAPI = new AuthAPI(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_child_friends_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String userName = mMyFriendsLists.get(position).getPersonalInfo().getFirstName() + " " +
                mMyFriendsLists.get(position).getPersonalInfo().getLastName();
        String imageUrl = mMyFriendsLists.get(position).getPersonalInfo().getImage();
        holder.txt_user_name.setText(userName);
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + imageUrl, holder.user_profile, Util.getImageLoaderOption(mContext));


        if (!mMyFriendsLists.get(position).getIsFriend().equalsIgnoreCase("1")) {
            holder.chat_icon.setVisibility(View.GONE);
            holder.add_friend_icon.setVisibility(View.VISIBLE);
        } else {
            holder.chat_icon.setVisibility(View.VISIBLE);
            holder.add_friend_icon.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mMyFriendsLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView user_profile;
        TextView txt_user_name;
        TextView txt_best_lap;
        ImageView chat_icon;
        ImageView add_friend_icon;
        LinearLayout profile_ll;

        public MyViewHolder(View itemView) {
            super(itemView);
            user_profile = itemView.findViewById(R.id.user_profile);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_best_lap = itemView.findViewById(R.id.txt_best_lap);
            chat_icon = itemView.findViewById(R.id.chat_icon);
            add_friend_icon = itemView.findViewById(R.id.add_friend_icon);
            profile_ll = itemView.findViewById(R.id.profile_ll);

            profile_ll.setOnClickListener(this);
            chat_icon.setOnClickListener(this);
            add_friend_icon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.chat_icon:
                    Intent intent = new Intent(mContext, ChatActivity.class);
                    intent.putExtra(S.friendId, mMyFriendsLists.get(getAdapterPosition()).getTimeBin());
                    intent.putExtra(S.userId, mMyFriendsLists.get(getAdapterPosition()).getUserId());
                    intent.putExtra(S.username, mMyFriendsLists.get(getAdapterPosition()).getPersonalInfo().getFirstName());
                    intent.putExtra(S.image, mMyFriendsLists.get(getAdapterPosition()).getPersonalInfo().getImage());
                    intent.putExtra(S.CHAT_TYPE, mMyFriendsLists.get(getAdapterPosition()).getChannalType());
                    mContext.startActivity(intent);

                    break;

                case R.id.profile_ll:
                    Log.e("tag","user ID"+mMyFriendsLists.get(getAdapterPosition()).getUserId());
                    Log.e("tag","friend ID"+mMyFriendsLists.get(getAdapterPosition()).getId());
                    Intent intent3 = new Intent(mContext, ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle.putString(S.user_id, mMyFriendsLists.get(getAdapterPosition()).getUserId());
                    bundle.putString(S.friendId, mMyFriendsLists.get(getAdapterPosition()).getId());
                    bundle.putString("status", "1");
                    intent3.putExtras(bundle);
                    mContext.startActivity(intent3);
                    break;
                case R.id.add_friend_icon:
                    String friendStatus = mMyFriendsLists.get(getAdapterPosition()).getIsFriend();
                    if (friendStatus.compareTo("0") == 0) {
                        String status = "0";
                        authAPI.userFriend(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mMyFriendsLists.get(getAdapterPosition()).getUserId(), status, "");
                    } else if (friendStatus.compareTo("2") == 0) {
                        Util.showSnackBar(add_friend_icon, mContext.getString(R.string.you_already_send_request));
                    }
                    break;
            }
        }
    }

    // search class
    public void filter(String ch) {
        mMyFriendsLists.clear();
        if (ch.isEmpty())
            mMyFriendsLists.addAll(mMyFriendsListsAll);
        else {
            for (ProfileMyFriendsList wp : mMyFriendsListsAll) {
                if (wp.getPersonalInfo().getFirstName().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())) ||
                        wp.getPersonalInfo().getLastName().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())))
                    mMyFriendsLists.add(wp);
            }
        }
        notifyDataSetChanged();
    }
}
