package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.AlbumCommentActivity;
import com.os.fastlap.activity.community.CommunityMoreInfoActivity;
import com.os.fastlap.activity.group.GroupProfileActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.activity.track.TrackDetailActivity;
import com.os.fastlap.beans.dashboardmodals.SearchBean;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhinava on 7/12/2017.
 */

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.MyViewHolder> {

    DisplayImageOptions options;
    Context context;
    ArrayList<SearchBean> search_list = new ArrayList<>();

    public SearchListAdapter(ArrayList<SearchBean> search_list, Context context) {
        this.search_list = search_list;
        this.context = context;
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.profile_default);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageForEmptyUri(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageOnFail(new BitmapDrawable(context.getResources(), default_bitmap))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.userNameTv.setText(search_list.get(position).getName());
        holder.descriptionTv.setText(search_list.get(position).getEmail());
        if (search_list.get(position).isFirst()) {
            holder.userTypeTv.setVisibility(View.VISIBLE);
            holder.userTypeTv.setText(search_list.get(position).getUserType());
        } else {
            holder.userTypeTv.setVisibility(View.GONE);
        }

        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + search_list.get(position).getImage(), holder.userProfileIv, Util.getImageLoaderOption(context));

        holder.parent_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = null;
                if (search_list.get(position).getType().compareToIgnoreCase("user") == 0) {
                    intent = new Intent(context, ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle.putString(S.user_id, search_list.get(position).getId());
                    bundle.putString("status", search_list.get(position).getIsFriend());
                    intent.putExtras(bundle);
                } else if (search_list.get(position).getType().compareToIgnoreCase("group") == 0) {
                    intent = new Intent(context, GroupProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(S.groupId, search_list.get(position).getId());
                    bundle.putString("status", search_list.get(position).getIsFriend());
                    bundle.putString(S.ownerId, search_list.get(position).getOwnerId());
                    intent.putExtras(bundle);
                } else if (search_list.get(position).getType().compareToIgnoreCase("event") == 0) {
                    intent = new Intent(context, CommunityMoreInfoActivity.class);
                    intent.putExtra(S._id, search_list.get(position).getId());
                } else if (search_list.get(position).getType().compareToIgnoreCase("track") == 0) {
                    intent = new Intent(context, TrackDetailActivity.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle1.putString(S.user_id, search_list.get(position).getId());
                    bundle1.putString("status", "");
                    intent.putExtras(bundle1);

                } else if (search_list.get(position).getType().compareToIgnoreCase("album") == 0) {
                    intent = new Intent(context, AlbumCommentActivity.class);
                    intent.putExtra(S.postId, search_list.get(position).getId());
                    intent.putExtra(S.position, 0 + "");
                }

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return search_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView userProfileIv;
        private TextViewPlayRegular userNameTv;
        private TextViewPlayRegular userTypeTv;
        private TextViewPlayRegular descriptionTv;
        private LinearLayout parent_ll;

        public MyViewHolder(View itemView) {
            super(itemView);

            userProfileIv = (CircleImageView) itemView.findViewById(R.id.user_profile_iv);
            userNameTv = (TextViewPlayRegular) itemView.findViewById(R.id.user_name_tv);
            userTypeTv = (TextViewPlayRegular) itemView.findViewById(R.id.userTypeTv);
            descriptionTv = (TextViewPlayRegular) itemView.findViewById(R.id.description_tv);
            parent_ll = (LinearLayout) itemView.findViewById(R.id.parent_ll);
        }
    }

}
