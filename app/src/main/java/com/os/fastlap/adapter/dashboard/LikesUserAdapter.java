package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.dashboard.LikesUserActivity;
import com.os.fastlap.activity.group.GroupMemberActivity;
import com.os.fastlap.beans.dashboardmodals.LikeUserBean;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhinava on 7/12/2017.
 */

public class LikesUserAdapter extends RecyclerView.Adapter<LikesUserAdapter.MyViewHolder> {

    DisplayImageOptions options;
    Context context;
    ArrayList<LikeUserBean> likes_list = new ArrayList<>();
    ClickListner clickListner;
    AuthAPI authAPI;
    OnItemClikcView onItemClikcView;

    public interface ClickListner {
        void requestForAddFriend(int postion);
        void requestForAddFriend(int postion, View view);
    }

    public void setOnclickListner(ClickListner onclickListner) {
        clickListner = onclickListner;
    }

    public void setOnItemClikcView(OnItemClikcView onItemClikcView){
        this.onItemClikcView=onItemClikcView;
    }

    public LikesUserAdapter(ArrayList<LikeUserBean> likes_list, Context context) {
        this.likes_list = likes_list;
        this.context = context;
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.profile_default);
        authAPI = new AuthAPI(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.likes_user_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.userNameTv.setText(likes_list.get(position).getLike_user_name());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + likes_list.get(position).getLike_user_image(), holder.userProfileImg, Util.getImageLoaderOption(context));

        if (likes_list.get(position).getLike_user_friend_status().compareTo("0") == 0) {
            holder.userStatusIcon.setImageResource(R.mipmap.adduser_white);
        } else {
            holder.userStatusIcon.setVisibility(View.GONE);
        }

        holder.userStatusIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (likes_list.get(position).getLike_user_friend_status().compareTo("0") == 0) {
                    holder.userStatusIcon.setVisibility(View.GONE);
                    sendFriendRequest(position);
                }
            }
        });
        if(context instanceof LikesUserActivity){
            holder.userProfileImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClikcView.onClick(MySharedPreferences.getPreferences(context, S.user_id));
                }
            });
        }
        if (context instanceof GroupMemberActivity) {
            holder.userStatusIcon.setVisibility(View.GONE);

            if (likes_list.get(position).getIsAdmin().equals("1")) {
                holder.groupAdminTv.setVisibility(View.VISIBLE);
                holder.groupAdminTv.setText(context.getString(R.string.group_admin));
            } else if (likes_list.get(position).getIsSubAdmin().equals("1")) {
                holder.groupAdminTv.setVisibility(View.VISIBLE);
                holder.groupAdminTv.setText(context.getString(R.string.sub_admin));
            } else {
                holder.groupAdminTv.setVisibility(View.GONE);
            }

            holder.likeuser.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    clickListner.requestForAddFriend(holder.getAdapterPosition(), holder.likeuser);
                    return false;
                }
            });
        }
    }

    private void sendFriendRequest(int position) {
        String status = "0";
        authAPI.userFriend(context, MySharedPreferences.getPreferences(context, S.user_id), likes_list.get(position).getLike_user_id(), status, "");
    }

    @Override
    public int getItemCount() {
        return likes_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView userProfileImg;
        private TextView userNameTv;
        private TextView groupAdminTv;
        private ImageView userStatusIcon;
        private RelativeLayout likeuser;

        public MyViewHolder(View itemView) {
            super(itemView);

            userProfileImg = (CircleImageView) itemView.findViewById(R.id.user_profile_img);
            userNameTv = (TextView) itemView.findViewById(R.id.user_name_tv);
            groupAdminTv = (TextView) itemView.findViewById(R.id.groupAdminTv);
            userStatusIcon = (ImageView) itemView.findViewById(R.id.user_status_icon);
            likeuser = (RelativeLayout) itemView.findViewById(R.id.likeuser);


        }
    }

    public interface OnItemClikcView {
        void onClick(String postion);
    }
}
