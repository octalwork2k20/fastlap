package com.os.fastlap.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.AlbumActivity;
import com.os.fastlap.activity.AlbumCommentActivity;
import com.os.fastlap.activity.GalleryActivity;
import com.os.fastlap.activity.dashboard.LikesUserActivity;
import com.os.fastlap.activity.dashboard.ShareFriendsListActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 3/22/2017.
 */

public class GalleryPostsAdapter extends RecyclerView.Adapter<GalleryPostsAdapter.MyViewHolder> implements OnImagePickerDialogSelect {

    private DisplayImageOptions options;
    Context context;
    int tabType;
    ArrayList<BeanAlbum> post_list;
    ArrayList<BeanAlbum> post_listALL = new ArrayList<>();
    ;
    Activity activity;
    ClickListner clickListner;
    AuthAPI authAPI;
    private int sharePosition;
    private int mediaType;
    OnImagePickerDialogSelect onImagePickerDialogSelect;

    SpannableStringBuilder shareTemp1 = null;
    SpannableStringBuilder shareTemp2 = null;
    SpannableStringBuilder shareTemp3 = null;
    SpannableStringBuilder shareTemp4 = null;


    @Override
    public void OnCameraSelect() {
        ((GalleryActivity) context).onClickOnshare(sharePosition, mediaType);
    }

    @Override
    public void OnGallerySelect() {
        if((context instanceof GalleryActivity)){
            ((GalleryActivity) context).onCLickOnFb(sharePosition, mediaType);

        }
    }

    @Override
    public void onVideoSelect() {

    }

    public void SetDummyList(ArrayList<BeanAlbum> post_list) {
        post_listALL.clear();
        post_listALL.addAll(post_list);
    }


    public interface ClickListner {
        void openAlbum(int position);

        void onClickOnLike(int position, String status);
    }

    public void setOnclickListner(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public GalleryPostsAdapter(ArrayList<BeanAlbum> post_list, Context context, Activity activity,int tabType) {
        Log.e("tag","list size"+post_list.size());
        //this.post_list.addAll(post_list);
        this.post_list = post_list;
        this.context = context;
        this.tabType=tabType;
        this.authAPI = new AuthAPI(context);
        onImagePickerDialogSelect = this;
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.profile_default);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageForEmptyUri(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageOnFail(new BitmapDrawable(context.getResources(), default_bitmap))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        this.activity = activity;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_post_list_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BeanAlbum beanAlbum = post_list.get(position);
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanAlbum.getUserImage(), holder.userProfileIv, Util.getImageLoaderOption(context));
        holder.albumNameTv.setText(beanAlbum.getName());
        holder.albumTitleTv.setText(beanAlbum.getUserName());
        try {
            holder.dateTimeTv.setText(Util.setTimeAgo(context,beanAlbum.getCreated()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (beanAlbum.getFilesList().size() > 0) {
            if (beanAlbum.getFilesList().get(0).getType().compareTo("1") == 0) {
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getFileName(), holder.postImageIv, Util.getImageLoaderOption(context));
                holder.playIv.setVisibility(View.GONE);
            } else {
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getThumbName(), holder.postImageIv, Util.getImageLoaderOption(context));
                holder.playIv.setVisibility(View.VISIBLE);
            }
        }

        holder.raceNameTv.setText(beanAlbum.getEventName());
        holder.circuitNameTv.setText(beanAlbum.getTrackName());

        if (Integer.parseInt(beanAlbum.getLikedPostCount()) > 0) {
            holder.likeCountTv.setText(beanAlbum.getLikedPostCount());
            holder.likeCountTv.setVisibility(View.VISIBLE);
        } else {
            holder.likeCountTv.setText(0+"");
            holder.likeCountTv.setVisibility(View.VISIBLE);
        }

        if (beanAlbum.getCommentPostCount().equalsIgnoreCase("0")){
            holder.commentCountTv.setVisibility(View.VISIBLE);
            holder.commentCountTv.setText(0 + " " + context.getString(R.string.comment));
        }
        else {
            holder.commentCountTv.setVisibility(View.VISIBLE);
            if (beanAlbum.getCommentPostCount().equalsIgnoreCase("1"))
                holder.commentCountTv.setText(beanAlbum.getCommentPostCount() + " " + context.getString(R.string.comment));
            else
                holder.commentCountTv.setText(beanAlbum.getCommentPostCount() + " " + context.getString(R.string.comments));
        }

        if (beanAlbum.getLikedPost().contentEquals("0"))
            holder.likeTextTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
        else
            holder.likeTextTv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));


        if (beanAlbum.getAlbumPostShareUserModals().size() != 0) {
            holder.postShareText.setVisibility(View.VISIBLE);
            shareTemp2 = null;
            shareTemp1 = null;
            shareTemp3 = null;
            shareTemp4 = null;
            getShareFriendsNames(beanAlbum.getAlbumPostShareUserModals(), position);

            Spanned turmsAndConditionSpanned = (Spanned) TextUtils.concat(
                    shareTemp2 == null ? "" : shareTemp2,
                    shareTemp3 == null ? "" : shareTemp3,
                    shareTemp4 == null ? "" : shareTemp4,
                    shareTemp1);
            holder.postShareText.setMovementMethod(LinkMovementMethod.getInstance());
            holder.postShareText.setText(new SpannableStringBuilder(turmsAndConditionSpanned));
        } else
            holder.postShareText.setVisibility(View.GONE);

        holder.addUserCountTv.setText(beanAlbum.getFriendList().size() + "");
        holder.bikeCountTv.setText("0");

        if (TextUtils.isEmpty(beanAlbum.getDateOfEvent())) {
            holder.dateTv.setVisibility(View.GONE);
        } else {
            holder.dateTv.setVisibility(View.VISIBLE);
            holder.dateTv.setText(Util.ConvertDateTimeZoneDate(beanAlbum.getDateOfEvent()));
        }
        if(tabType==4){
            if(!beanAlbum.getPresetImage().equalsIgnoreCase("")){
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanAlbum.getPresetImage(), holder.mImageBanner);
                holder.mImageBanner.setVisibility(View.VISIBLE);
            }
        }


    }

    @Override
    public int getItemCount() {
        return post_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CircleImageView userProfileIv;
        private TextViewPlayBold albumNameTv;
        private TextViewPlayRegular albumTitleTv;
        private LinearLayout timeInfoLl;
        private TextViewPlayRegular dateTimeTv;
        private TextViewPlayRegular addUserCountTv;
        private TextViewPlayRegular bikeCountTv;
        private ImageView postImageIv;
        private ImageView playIv;
        private TextViewPlayRegular raceNameTv;
        private TextViewPlayRegular circuitNameTv;
        private TextViewPlayRegular dateTv;
        private LinearLayout openAlbumLayout;
        private ImageView openAlbumIcon;
        private LinearLayout subscribeLayout;
        private ImageView subscribeIcon;
        private TextViewPlayRegular likeCountTv;
        private TextViewPlayRegular commentCountTv;
        private TextViewPlayRegular shareCountTv;
        private LinearLayout likeLayout;
        private ImageView likeIcon;
        private TextViewPlayRegular likeTextTv;
        private LinearLayout commentLayout;
        private ImageView commentIcon;
        private TextViewPlayRegular commentTextTv;
        private LinearLayout shareLayout;
        private ImageView shareIcon;
        private TextViewPlayRegular shareTextTv;
        private LinearLayout optionLayout;
        private LinearLayout share_option_layout;
        private ImageView optionIcon;
        private TextView postShareText;
        private ImageView mImageBanner;

        public MyViewHolder(View itemView) {
            super(itemView);
            postShareText = itemView.findViewById(R.id.postShareText);
            userProfileIv = (CircleImageView) itemView.findViewById(R.id.user_profile_iv);
            albumNameTv = (TextViewPlayBold) itemView.findViewById(R.id.album_name_tv);
            albumTitleTv = (TextViewPlayRegular) itemView.findViewById(R.id.album_title_tv);
            timeInfoLl = (LinearLayout) itemView.findViewById(R.id.time_info_ll);
            dateTimeTv = (TextViewPlayRegular) itemView.findViewById(R.id.date_time_tv);
            addUserCountTv = (TextViewPlayRegular) itemView.findViewById(R.id.add_user_count_tv);
            bikeCountTv = (TextViewPlayRegular) itemView.findViewById(R.id.bike_count_tv);
            postImageIv = (ImageView) itemView.findViewById(R.id.post_image_iv);
            playIv = (ImageView) itemView.findViewById(R.id.play_iv);
            raceNameTv = (TextViewPlayRegular) itemView.findViewById(R.id.race_name_tv);
            circuitNameTv = (TextViewPlayRegular) itemView.findViewById(R.id.circuit_name_tv);
            dateTv = (TextViewPlayRegular) itemView.findViewById(R.id.date_tv);
            openAlbumLayout = (LinearLayout) itemView.findViewById(R.id.open_album_layout);
            openAlbumIcon = (ImageView) itemView.findViewById(R.id.open_album_icon);
            subscribeLayout = (LinearLayout) itemView.findViewById(R.id.subscribe_layout);
            subscribeIcon = (ImageView) itemView.findViewById(R.id.subscribe_icon);
            likeCountTv = (TextViewPlayRegular) itemView.findViewById(R.id.like_count_tv);
            commentCountTv = (TextViewPlayRegular) itemView.findViewById(R.id.comment_count_tv);
            shareCountTv = (TextViewPlayRegular) itemView.findViewById(R.id.share_count_tv);
            likeLayout = (LinearLayout) itemView.findViewById(R.id.like_layout);
            likeIcon = (ImageView) itemView.findViewById(R.id.like_icon);
            likeTextTv = (TextViewPlayRegular) itemView.findViewById(R.id.like_text_tv);
            commentLayout = (LinearLayout) itemView.findViewById(R.id.comment_layout);
            commentIcon = (ImageView) itemView.findViewById(R.id.comment_icon);
            commentTextTv = (TextViewPlayRegular) itemView.findViewById(R.id.comment_text_tv);
            shareLayout = (LinearLayout) itemView.findViewById(R.id.share_layout);
            shareIcon = (ImageView) itemView.findViewById(R.id.share_icon);
            shareTextTv = (TextViewPlayRegular) itemView.findViewById(R.id.share_text_tv);
            optionLayout = (LinearLayout) itemView.findViewById(R.id.option_layout);
            share_option_layout = (LinearLayout) itemView.findViewById(R.id.share_option_layout);
            optionIcon = (ImageView) itemView.findViewById(R.id.option_icon);
            mImageBanner=(ImageView)itemView.findViewById(R.id.imgBanner);

            playIv.setOnClickListener(this);
            openAlbumLayout.setOnClickListener(this);
            likeLayout.setOnClickListener(this);
            likeCountTv.setOnClickListener(this);
            commentLayout.setOnClickListener(this);
            commentCountTv.setOnClickListener(this);
            optionLayout.setOnClickListener(this);
            share_option_layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.play_iv:
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(S.IMAGE_BASE_URL + post_list.get(getAdapterPosition()).getFilesList().get(0).getFileName()));
                        intent.setDataAndType(Uri.parse(S.IMAGE_BASE_URL + post_list.get(getAdapterPosition()).getFilesList().get(0).getFileName()), "video/*");
                        context.startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(context, context.getResources().getString(R.string.no_supportable_Player), Toast.LENGTH_LONG).show();
                    }
                    break;

                case R.id.open_album_layout:
                    clickListner.openAlbum(getAdapterPosition());
                    break;

                case R.id.like_layout:
                    if (clickListner != null) {
                        String status = "0";
                        int likeCount = Integer.parseInt(post_list.get(getAdapterPosition()).getLikedPostCount());
                        if (post_list.get(getAdapterPosition()).getLikedPost().compareTo("0") == 0) {
                            status = "1";
                            likeCount = likeCount + 1;
                        } else {
                            status = "0";
                            likeCount = likeCount - 1;
                        }
                        post_list.get(getAdapterPosition()).setLikedPost(status);
                        post_list.get(getAdapterPosition()).setLikedPostCount(likeCount + "");
                        clickListner.onClickOnLike(getAdapterPosition(), status);
                        notifyItemChanged(getAdapterPosition());
                    }
                    break;
                case R.id.like_count_tv:
                    Intent intent = new Intent(context, LikesUserActivity.class);
                    intent.putExtra(S.postId, post_list.get(getAdapterPosition()).get_id());
                    intent.putExtra(S.likeType, I.ALBUMLIKE);
                    context.startActivity(intent);
                    break;
                case R.id.comment_layout:
                    if (!(context instanceof AlbumCommentActivity)) {
                        Intent intent1 = new Intent(context, AlbumCommentActivity.class);
                        intent1.putExtra(S.postId, post_list.get(getAdapterPosition()).get_id());
                        intent1.putExtra(S.position, getAdapterPosition() + "");
                        activity.startActivityForResult(intent1, I.COMMENT_ACTIVITY_RESULT_CODE);
                    }
                    break;
                case R.id.comment_count_tv:
                    if (!(context instanceof AlbumCommentActivity)) {
                        Intent intent2 = new Intent(context, AlbumCommentActivity.class);
                        intent2.putExtra(S.postId, post_list.get(getAdapterPosition()).get_id());
                        intent2.putExtra(S.position, getAdapterPosition() + "");
                        activity.startActivityForResult(intent2, I.COMMENT_ACTIVITY_RESULT_CODE);
                    }
                    break;
                case R.id.option_layout:
                    final PopupMenu popup = new PopupMenu(context, GalleryPostsAdapter.MyViewHolder.this.optionLayout);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.post_option_menu);
                    //adding click listener
                    if (!post_list.get(getAdapterPosition()).getUserId().equalsIgnoreCase(MySharedPreferences.getPreferences(context, S.user_id))) {
                        Menu m = popup.getMenu();
                        m.removeItem(R.id.delete_post);
                    }


                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.annoing_text:
                                    reportPost(item.getTitle(), post_list.get(getAdapterPosition()).get_id());
                                    break;
                                case R.id.report_post:
                                    reportPost(item.getTitle(), post_list.get(getAdapterPosition()).get_id());
                                    break;
                                case R.id.hide_post:
                                    reportPost(item.getTitle(), post_list.get(getAdapterPosition()).get_id());
                                    break;
                                case R.id.delete_post:
                                    Util.showAlertDialogWithTwoButtonAlbum(context, context.getString(R.string.are_you_sure_you_want_to_delete), context.getString(R.string.cancel), context.getString(R.string.delete), post_list.get(getAdapterPosition()).get_id());

                                    // reportPost(item.getTitle(), post_list.get(getAdapterPosition()).get_id());
                                    break;
                                case R.id.cancel:
                                    popup.dismiss();
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                    break;

                case R.id.share_option_layout:
                    sharePostDialog(getAdapterPosition(), post_list.get(getAdapterPosition()).getMediaType());
                    break;
            }

        }
    }

    private void sharePostDialog(int adapterPosition, int mediaType) {
        sharePosition = adapterPosition;
        this.mediaType = mediaType;
        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        chooseImagePickerOptionDialog.changeText();

    }

    private void reportPost(CharSequence title, String post_id) {
        authAPI.reportAlbum(context, MySharedPreferences.getPreferences(context, S.user_id), post_id, title + "");
    }

    private void getShareFriendsNames(final List<DashboardPostShareUserModal> dashboardPostShareUserModals, final int pos) {
        ClickableSpan clickableSpanFriendsOne = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent3 = new Intent(context, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString(S.user_id, post_list.get(pos).getAlbumPostShareUserModals().get(0).getUserDataBeans().get_id());
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                context.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        ClickableSpan clickableSpanFriendsTwo = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent3 = new Intent(context, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString(S.user_id, post_list.get(pos).getAlbumPostShareUserModals().get(1).getUserDataBeans().get_id());
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                context.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        ClickableSpan clickableSpanFriendsOthers = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ArrayList<DashboardPostShareUserModal> tagedList = new ArrayList<>();
                tagedList.addAll(dashboardPostShareUserModals);
                Intent intent3 = new Intent(context, ShareFriendsListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                intent3.putExtra(S.user_list, tagedList);
                context.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        try {
            if (dashboardPostShareUserModals.size() == 1) {
                shareTemp2 = Util.getSpannableText(dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
                shareTemp2.setSpan(clickableSpanFriendsOne, 0, shareTemp2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            } else if (dashboardPostShareUserModals.size() == 2) {
                shareTemp2 = Util.getSpannableText(dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
                shareTemp2.setSpan(clickableSpanFriendsOne, 0, shareTemp2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                shareTemp3 = Util.getSpannableText(" and ", "#414141");

                shareTemp4 = Util.getSpannableText(dashboardPostShareUserModals.get(1).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(1).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
                shareTemp4.setSpan(clickableSpanFriendsTwo, 0, shareTemp4.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


            } else {
                shareTemp2 = Util.getSpannableText(dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
                shareTemp2.setSpan(clickableSpanFriendsOne, 0, shareTemp2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                shareTemp3 = Util.getSpannableText(" and ", "#414141");

                shareTemp4 = Util.getSpannableText(String.valueOf(dashboardPostShareUserModals.size() - 1) + " others ", "#bd2436");
                shareTemp4.setSpan(clickableSpanFriendsOthers, 0, shareTemp4.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        shareTemp1 = Util.getSpannableText(" " + context.getString(R.string.shared_a_post), "#414141");
    }

}
