package com.os.fastlap.adapter.paddock;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CompareLapAdapter extends RecyclerView.Adapter<CompareLapAdapter.ViewHolder> {
    ArrayList<BeanTimeLap> mUserList;
    Context context;
    boolean isTitle;

    public CompareLapAdapter(Context context,ArrayList<BeanTimeLap> mUserList,boolean isTitle) {
        this.mUserList = mUserList;
        this.context=context;
        this.isTitle=isTitle;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.compare_lap_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BeanTimeLap beanTimeLap=mUserList.get(position);
        if(isTitle){
            holder.mLytHeader.setVisibility(View.VISIBLE);
            if(position==0){
                holder.mLytHeader.setVisibility(View.VISIBLE);
            }
            else {
                holder.mLytHeader.setVisibility(View.GONE);
            }
        }
        else {
            holder.mLytHeader.setVisibility(View.GONE);
        }

        if(!beanTimeLap.getMaxSpeed().equalsIgnoreCase("")){
            double maxSpeed= Double.parseDouble(beanTimeLap.getMaxSpeed());
            //int x = Math.abs(-5);

            holder.mMaxSpeed.setText(new DecimalFormat("##.##").format(maxSpeed)+" "+"km/h");
        }
        if(!beanTimeLap.getAverageSpeedToShow().equalsIgnoreCase("")){
            double averageSpeed= Double.parseDouble(beanTimeLap.getAverageSpeedToShow());
            holder.mAverageSpeed.setText(new DecimalFormat("##.##").format(averageSpeed)+" "+"km/h");
        }

       // $scope.trackLength = parseInt(track.trackLength) / 1000;
        holder.mUsername.setText(beanTimeLap.getUserName());
        GradientDrawable background = (GradientDrawable) holder.colorTv.getBackground();
        background.setColor(beanTimeLap.getLapColor());
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextViewPlayRegular mUsername,mAverageSpeed,mMaxSpeed;
        TextView colorTv;
        CardView mLytHeader;
        public ViewHolder(View itemView) {
            super(itemView);
            mLytHeader=(CardView)itemView.findViewById(R.id.llOne);
            colorTv=(TextView)itemView.findViewById(R.id.colorTv);
            mUsername=(TextViewPlayRegular)itemView.findViewById(R.id.tv_user_name);
            mMaxSpeed=(TextViewPlayRegular)itemView.findViewById(R.id.tv_max_speed);
            mAverageSpeed=(TextViewPlayRegular)itemView.findViewById(R.id.tv_average_speed);
        }
    }
}
