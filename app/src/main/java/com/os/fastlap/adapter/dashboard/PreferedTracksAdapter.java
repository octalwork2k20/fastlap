package com.os.fastlap.adapter.dashboard;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanPreferedTrack;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 7/12/2017.
 */

public class PreferedTracksAdapter extends RecyclerView.Adapter<PreferedTracksAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<BeanPreferedTrack> trackDatas = new ArrayList<>();
    OnClickOnPreferList onClickOnPreferList;

    public PreferedTracksAdapter(ArrayList<BeanPreferedTrack> trackDatas, Context context) {
        this.trackDatas = trackDatas;
        this.mContext = context;
    }

    public interface OnClickOnPreferList {
        void preferCrossClick(int postion);

        void preferFilterClick(int postion,JSONObject filterData);

        void preferRowClick(int postion);
    }

    public void SetOnclickList(OnClickOnPreferList onClickOnPreferList) {
        this.onClickOnPreferList = onClickOnPreferList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_prefered_track_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //String pending = notification_list.get(position);
        holder.circuit1Tv.setText(trackDatas.get(position).getTrackName());


        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;

        if (trackDatas.size() > 1) {
            LinearLayout.LayoutParams prm1 = new LinearLayout.LayoutParams(width / 2, LinearLayout.LayoutParams.MATCH_PARENT, 1);
            holder.circuit1Ll.setLayoutParams(prm1);
        } else {

            LinearLayout.LayoutParams ll_prm = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            holder.circuit1Ll.setLayoutParams(ll_prm);
        }

        if((position%2)==0)
        {
            holder.circuit1Ll.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }
        else
        {
            holder.circuit1Ll.setBackgroundColor(ContextCompat.getColor(mContext,R.color.theme_graycolor));
        }
    }

    @Override
    public int getItemCount() {
        return trackDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RelativeLayout circuit1Ll;
        private TextViewPlayRegular circuit1Tv;
        private LinearLayout actionbtn;
        private ImageView crossIv;
        private ImageView filterIv;

        public MyViewHolder(View itemView) {
            super(itemView);

            circuit1Ll = (RelativeLayout) itemView.findViewById(R.id.circuit1_ll);
            circuit1Tv = (TextViewPlayRegular) itemView.findViewById(R.id.circuit1_tv);
            actionbtn = (LinearLayout) itemView.findViewById(R.id.actionbtn);
            crossIv = (ImageView) itemView.findViewById(R.id.cross_iv);
            filterIv = (ImageView) itemView.findViewById(R.id.filter_iv);

            crossIv.setOnClickListener(this);
            filterIv.setOnClickListener(this);
            circuit1Ll.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == crossIv) {
                onClickOnPreferList.preferCrossClick(getAdapterPosition());
            }
            if (view == filterIv) {
                onClickOnPreferList.preferFilterClick(getAdapterPosition(),trackDatas.get(getAdapterPosition()).getFilter_data());
            }
            if (view == circuit1Ll) {
                onClickOnPreferList.preferRowClick(getAdapterPosition());
            }

        }
    }
}
