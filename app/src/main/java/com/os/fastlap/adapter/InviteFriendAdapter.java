package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/*
 * Created by anandj on 8/28/2017.
 */

public class InviteFriendAdapter extends RecyclerView.Adapter<InviteFriendAdapter.MyViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener
    {
        void onItemClick(int position);
        void onInviteClick(int position);
    }

    private Context mContext;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    private ArrayList<ProfileMyFriendsList> mMyFriendsListsAll = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private boolean mSelectMultiple;
    private List<Integer> mSelectUser;
    private List<Integer> checkId = new ArrayList<>();
    private String removeFriendTag = "";
    private List<Integer> mSelectNewUser;


    public InviteFriendAdapter(Context context, ArrayList<ProfileMyFriendsList> friendsLists, boolean select_multiple) {
        this.mContext = context;
        this.mMyFriendsLists = friendsLists;
        this.mMyFriendsListsAll.addAll(friendsLists);
        this.mSelectMultiple = select_multiple;
        mSelectUser = new ArrayList<>();
        mSelectNewUser = new ArrayList<>();
    }

    public void addMyFriendsListAll(ArrayList<ProfileMyFriendsList> friendsLists) {
        this.mMyFriendsListsAll.clear();
        this.mMyFriendsListsAll.addAll(friendsLists);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_blockuser_layout, parent, false));
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }

    public void addSelectedTagFriends(List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeans) {
        mSelectUser.addAll(chdckId(dashboardCommentPostTagFriendsBeans));
        notifyDataSetChanged();
    }

    private List<Integer> chdckId(List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeans) {
        checkId = new ArrayList<>();
        for (int i = 0; i < dashboardCommentPostTagFriendsBeans.size(); i++) {
            for (int j = 0; j < mMyFriendsLists.size(); j++) {
                if (dashboardCommentPostTagFriendsBeans.get(i).getFriendIdBean().get_id().equalsIgnoreCase(mMyFriendsLists.get(j).getUserId())) {
                    if (!checkId.contains(j))
                        checkId.add(j);
                }

            }
        }
        return checkId;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        if (mSelectMultiple) {
            holder.terms_checkbox.setVisibility(View.VISIBLE);
            holder.terms_checkbox.setTag(position);
            holder.terms_checkbox.setChecked(mSelectUser.contains(position));
        } else
            holder.terms_checkbox.setVisibility(View.GONE);

        String userName = mMyFriendsLists.get(position).getPersonalInfo().getFirstName() + " " +
                mMyFriendsLists.get(position).getPersonalInfo().getLastName();
        String imageUrl = mMyFriendsLists.get(position).getPersonalInfo().getImage();
        holder.txt_user_name.setText(userName);
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + imageUrl, holder.user_profile, Util.getImageLoaderOption(mContext));


        if(mMyFriendsLists.get(position).getInvitedGroup().equals("1"))
        {
            holder.invite_tv.setText(mContext.getString(R.string.invited));
            holder.invite_tv.setEnabled(false);
            holder.invite_tv.setBackgroundResource(R.drawable.button_gray_bg);
        }
        else
        {
            holder.invite_tv.setText(mContext.getString(R.string.invite));
            holder.invite_tv.setEnabled(true);
            holder.invite_tv.setBackgroundResource(R.drawable.button_bg);
        }


    }

    @Override
    public int getItemCount() {
        return mMyFriendsLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView user_profile;
        TextView txt_user_name;
        AppCompatCheckBox terms_checkbox;
        LinearLayout ll_main;
        TextView invite_tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            user_profile = itemView.findViewById(R.id.track_img_iv);
            txt_user_name = itemView.findViewById(R.id.track_name_tv);
            terms_checkbox = itemView.findViewById(R.id.terms_checkbox);
            invite_tv = itemView.findViewById(R.id.invite_tv);

            ll_main = itemView.findViewById(R.id.ll_main);

            ll_main.setOnClickListener(this);
            terms_checkbox.setOnClickListener(this);
            invite_tv.setOnClickListener(this);
            invite_tv.setVisibility(View.VISIBLE);
            terms_checkbox.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ll_main:
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getAdapterPosition());
                    break;
                case R.id.terms_checkbox:
                    int id = (int) view.getTag();
                    if (mSelectUser.contains(id)) {
                        if (mSelectNewUser.contains(id))
                            mSelectNewUser.remove(Integer.valueOf(id));
                        mSelectUser.remove(Integer.valueOf(id));
                        if (checkId.contains(id)) {
                            if (removeFriendTag.isEmpty())
                                removeFriendTag = mMyFriendsLists.get(id).getUserId();
                            else
                                removeFriendTag = removeFriendTag.concat("," + mMyFriendsLists.get(id).getUserId());
                            checkId.remove(Integer.valueOf(id));
                        }
                    } else {
                        mSelectNewUser.add(id);
                        mSelectUser.add(id);
                    }
                    notifyDataSetChanged();
                    break;
                case R.id.invite_tv:
                    if (onItemClickListener != null)
                        onItemClickListener.onInviteClick(getAdapterPosition());

                    break;
            }
        }
    }


    // search class
    public void filter(String ch) {
        mMyFriendsLists.clear();
        if (ch.isEmpty())
            mMyFriendsLists.addAll(mMyFriendsListsAll);
        else {
            for (ProfileMyFriendsList wp : mMyFriendsListsAll)
            {
                if (wp.getPersonalInfo().getFirstName().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())) ||
                        wp.getPersonalInfo().getLastName().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())) ||
                        wp.getEmail().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())))
                    mMyFriendsLists.add(wp);
            }
        }
        notifyDataSetChanged();
    }
}