package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/*
 * Created by abhinava on 7/12/2017.
 */

public class EventTicketListAdapter extends RecyclerView.Adapter<EventTicketListAdapter.MyViewHolder> {

    Context context;
    ArrayList<EventsBean.Presets> ticket_list = new ArrayList<>();
    CheckedListner checkedListner;

    public interface CheckedListner {
        void CheckChange(int postion);
    }

    public void setOnItemClickListener(CheckedListner listener) {
        checkedListner = listener;
    }

    public EventTicketListAdapter(ArrayList<EventsBean.Presets> comment_list, Context context) {
        this.ticket_list = comment_list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.singleTicketAmountTv.setText(ticket_list.get(position).getPrice() + "$");
        holder.singleIncludeTv.setText(context.getString(R.string.include_nr) + " " + ticket_list.get(position).getTotalUser() + " " + context.getString(R.string.entrance));

        if (ticket_list.get(holder.getAdapterPosition()).isSelected()) {
            holder.singleTicketCountEt.setChecked(true);
        } else {
            holder.singleTicketCountEt.setChecked(false);
        }

        if (Integer.parseInt(ticket_list.get(position).getTotalUser()) == 1) {
            holder.ticketDetailTv.setText(context.getString(R.string.single_entrance));
        } else if (Integer.parseInt(ticket_list.get(position).getTotalUser()) == 2) {
            holder.ticketDetailTv.setText(context.getString(R.string.bring_a_friend));
        } else {
            holder.ticketDetailTv.setText(context.getString(R.string.bring) + " " + ticket_list.get(position).getTotalUser() + " " + context.getString(R.string.friends));
        }


    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout singleTicketRl;
        private TextViewPlayRegular singleTicketAmountTv;
        private TextViewPlayRegular singleIncludeTv;
        private TextView ticketDetailTv;
        private AppCompatRadioButton singleTicketCountEt;

        public MyViewHolder(View itemView) {
            super(itemView);

            singleTicketRl = (RelativeLayout) itemView.findViewById(R.id.single_ticket_rl);
            singleTicketAmountTv = (TextViewPlayRegular) itemView.findViewById(R.id.single_ticket_amount_tv);
            singleIncludeTv = (TextViewPlayRegular) itemView.findViewById(R.id.single_include_tv);
            singleTicketCountEt = (AppCompatRadioButton) itemView.findViewById(R.id.single_ticket_count_et);
            ticketDetailTv = (TextView) itemView.findViewById(R.id.ticketDetailTv);

            if (singleTicketCountEt != null) {
                singleTicketCountEt.setOnClickListener(this);
            }

        }

        @Override
        public void onClick(View view) {
            if (view == singleTicketCountEt) {
                checkedListner.CheckChange(getAdapterPosition());
            }

        }
    }

}
