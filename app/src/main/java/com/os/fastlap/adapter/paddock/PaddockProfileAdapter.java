package com.os.fastlap.adapter.paddock;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.util.constants.I;

import java.util.ArrayList;

/**
 * Created by anandj on 7/17/2017.
 */

public class PaddockProfileAdapter extends RecyclerView.Adapter<PaddockProfileAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<String> mdiaryArrrayList;

    public PaddockProfileAdapter(Context context, ArrayList<String> arrayList) {
        mContext = context;
        mdiaryArrrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_items, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mdiaryArrrayList.size();
    }

    @Override
    public int getItemViewType(int position)
    {

        return I.TYPE_ITEM;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }
}
