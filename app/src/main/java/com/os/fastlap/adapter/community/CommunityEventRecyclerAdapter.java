package com.os.fastlap.adapter.community;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by anandj on 7/19/2017.
 */

public class CommunityEventRecyclerAdapter extends RecyclerView.Adapter<CommunityEventRecyclerAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<EventsBean> mArrayList = new ArrayList<>();
    ArrayList<EventsBean> mArrayListClone = new ArrayList<>();
    int type;
    ClickListner clickListner;

    public interface ClickListner {
        void ticketInfo(int position);

        void moreInfo(int position);
    }

    public void setOnclickListner(ClickListner onclickListner) {
        clickListner = onclickListner;
    }

    public CommunityEventRecyclerAdapter(Context context, ArrayList<EventsBean> arrayList, int type) {
        this.mContext = context;
        mArrayList = arrayList;
        this.type = type;
    }

    public void AddCloneList(ArrayList<EventsBean> mArrayList) {
        mArrayListClone.clear();
        mArrayListClone.addAll(mArrayList);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (type == 0)
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.community_fragment_event_child, parent, false));
        else
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.community_fragment_event_child_grid, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.eventNameTv.setText(mArrayList.get(position).getName());
        holder.locationTv.setText(mArrayList.get(position).getLocation());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mArrayList.get(position).getImage(), holder.imgCar, Util.getImageLoaderOption(mContext));
        holder.dateTv.setText(Util.ConvertDateTimeZoneDateMonth(mArrayList.get(position).getStartDate()));
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private RelativeLayout rrListParent;
        private ImageView imgCar;
        private TextViewPlayBold eventNameTv;
        private ImageView countryFlagIv;
        private TextViewPlayRegular locationTv;
        private TextViewPlayBold dateTv;
        private TextViewPlayBold txtMoreInfo;
        private TextViewPlayBold txtTicketsInfo;

        public MyViewHolder(View itemView) {
            super(itemView);

            rrListParent = (RelativeLayout) itemView.findViewById(R.id.rr_list_parent);
            imgCar = (ImageView) itemView.findViewById(R.id.img_car);
            eventNameTv = (TextViewPlayBold) itemView.findViewById(R.id.event_name_tv);
            countryFlagIv = (ImageView) itemView.findViewById(R.id.country_flag_iv);
            locationTv = (TextViewPlayRegular) itemView.findViewById(R.id.location_tv);
            dateTv = (TextViewPlayBold) itemView.findViewById(R.id.date_tv);
            txtMoreInfo = (TextViewPlayBold) itemView.findViewById(R.id.txt_more_info);
            txtTicketsInfo = (TextViewPlayBold) itemView.findViewById(R.id.txt_tickets_info);

            txtMoreInfo.setOnClickListener(this);
            txtTicketsInfo.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.txt_more_info:
                    clickListner.moreInfo(getAdapterPosition());
                    break;
                case R.id.txt_tickets_info:
                    clickListner.ticketInfo(getAdapterPosition());
                    break;
            }
        }
    }

    public void changeLayoutView(int type) {
        switch (type) {
            case 0:
                /// PhotoRecyclerViewAdapter.MyViewHolder myViewHolder = new MyViewHolder();
                break;
            case 1:
                break;
        }
    }

    // search class
    public void filter(String ch) {
        mArrayList.clear();
        if (ch.isEmpty())
            mArrayList.addAll(mArrayListClone);
        else {
            for (EventsBean wp : mArrayListClone) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())))
                    mArrayList.add(wp);
            }
        }
        notifyDataSetChanged();
    }

    // filter based on Vehicle type
    public void filterById(String id) {
        mArrayList.clear();
        if (id.isEmpty())
            mArrayList.addAll(mArrayListClone);
        for (EventsBean wp : mArrayListClone) {
            if (wp.getVehicleTypeId().equalsIgnoreCase(id.toLowerCase()))
                mArrayList.add(wp);
        }
        notifyDataSetChanged();
    }
}
