package com.os.fastlap.adapter.uploadlaptime;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/*
 * Created by anand j on 8/10/2017.
 */

public class UploadImageAdapter extends RecyclerView.Adapter<UploadImageAdapter.MyViewHolder> {

    /*Callback for list item click event */
    public interface OnItemClickListener
    {
        void onAddImageCLick(View v, int position,int itemPosition);

        void onDeleteImageClick(View v, int position,int itemPosition);
    }

    private Context mContext;
    private ArrayList<PagerBean> mPagerBeen;
    private OnItemClickListener mOnItemClickListener;
    private ArrayList<BeanAlbum.BeanFiles> filesList;
    private boolean albumBeans = false;
    private int itemPosition=0;

    public UploadImageAdapter(ArrayList<PagerBean> imageList, Context context,int itemPosition)
    {
        this.mPagerBeen = imageList;
        this.mContext = context;
        this.itemPosition=itemPosition;
    }

    public void addFileList(ArrayList<BeanAlbum.BeanFiles> imageList) {
        this.filesList = imageList;
        this.albumBeans = true;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    @Override
    public UploadImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pager_row_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(UploadImageAdapter.MyViewHolder holder, int position) {
        if (!albumBeans) {
            if (position == 0) {
                holder.imgPagerDelete.setVisibility(View.GONE);
                holder.imgPagerImage.setImageResource(R.drawable.ic_image_add);
            } else {
                holder.imgPagerDelete.setVisibility(View.VISIBLE);
                if (mPagerBeen.get(position).isEdit()) {
                    ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mPagerBeen.get(position).getImage(), holder.imgPagerImage, Util.getImageLoaderOption(mContext));
                } else {
                    String uri = Uri.fromFile(mPagerBeen.get(position).getFile()).toString();
                    String decoded = Uri.decode(uri);
                    ImageLoader.getInstance().displayImage(decoded, holder.imgPagerImage);
                }
            }
        } else {
            if (position == 0) {
                holder.imgPagerDelete.setVisibility(View.GONE);
                holder.imgPagerImage.setImageResource(R.drawable.ic_image_add);
            } else {
                position = position-1;
                holder.imgPagerDelete.setVisibility(View.VISIBLE);
                if (filesList.get(position).getType().compareTo("1") == 0) {
                    ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + filesList.get(position).getFileName(), holder.imgPagerImage, Util.getImageLoaderOption(mContext));
                } else {
                    ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + filesList.get(position).getThumbName(), holder.imgPagerImage, Util.getImageLoaderOption(mContext));
                }
            }

        }

    }

    @Override
    public int getItemCount() {
        if (albumBeans)
            return filesList.size() + 1;
        return mPagerBeen.size();
    }

    /* ViewHolder for each image item */
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgPagerImage;
        ImageView imgPagerDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgPagerDelete = itemView.findViewById(R.id.imgPagerDelete);
            imgPagerImage = itemView.findViewById(R.id.imgPagerImage);

            imgPagerDelete.setOnClickListener(this);
            imgPagerImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.imgPagerDelete:
                    onImageDelete(this);
                    break;
                case R.id.imgPagerImage:
                    onImageAdd(this);
                    break;
            }
        }
    }

    private void onImageDelete(MyViewHolder holder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onDeleteImageClick(holder.itemView, holder.getAdapterPosition(),itemPosition);
        }
    }

    private void onImageAdd(MyViewHolder holder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onAddImageCLick(holder.itemView, holder.getAdapterPosition(),itemPosition);
        }
    }
}
