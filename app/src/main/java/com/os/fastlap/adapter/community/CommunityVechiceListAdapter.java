package com.os.fastlap.adapter.community;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.ExploreActivity;
import com.os.fastlap.activity.GalleryActivity;
import com.os.fastlap.activity.community.EventActivity;
import com.os.fastlap.activity.community.UserGroupsActivity;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.beans.BeanVehicle;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/18/2017
 */

public class CommunityVechiceListAdapter extends RecyclerView.Adapter<CommunityVechiceListAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<BeanVehicle> mVechineList;
    private String select_id = "";

    public CommunityVechiceListAdapter(Context context, ArrayList<BeanVehicle> VechineList) {
        mContext = context;
        this.mVechineList = VechineList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.community_vechine_list_child, parent, false));
    }

    public void deSelectCurrent() {
        select_id = "";
        notifyDataSetChanged();
    }

    public boolean getCurrentSelectStatus() {
        return !select_id.isEmpty();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        holder.nameTv.setText(mVechineList.get(position).getName());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mVechineList.get(position).getImage(), holder.imageIv, Util.getImageLoaderOption(mContext));

        if (!select_id.isEmpty())
        {
            if(mVechineList.get(position).isSelected())
            {
                holder.nameTv.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            }
            else
            {
                holder.nameTv.setTextColor(mContext.getResources().getColor(R.color.theme_graycolor));
            }

        } else
        {
            holder.nameTv.setTextColor(mContext.getResources().getColor(R.color.theme_graycolor));
        }
        holder.parentLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (select_id.equalsIgnoreCase(String.valueOf(holder.getAdapterPosition())))
                        select_id = "";
                    else {
                        select_id = String.valueOf(holder.getAdapterPosition());
                    }

                    for(int i=0;i<mVechineList.size();i++)
                    {
                        if(i==holder.getAdapterPosition())
                        {
                            mVechineList.get(i).setSelected(true);
                        }
                        else
                        {
                            mVechineList.get(i).setSelected(false);
                        }
                    }

                if (mContext instanceof UserGroupsActivity)
                {
                    ((UserGroupsActivity) mContext).getVehicleTypeClick(holder.getAdapterPosition(), select_id);
                }
                else if(mContext instanceof EventActivity)
                {
                    ((EventActivity) mContext).getVehicleTypeClick(holder.getAdapterPosition(), select_id);
                }
                else if(mContext instanceof ExploreActivity)
                {
                    ((ExploreActivity) mContext).getVehicleTypeClick(holder.getAdapterPosition(), select_id);
                }
                else if(mContext instanceof GalleryActivity)
                {
                    ((GalleryActivity) mContext).getVehicleTypeClick(holder.getAdapterPosition(), select_id);
                }
                else if(mContext instanceof MyLapsActivity)
                {
                    ((MyLapsActivity) mContext).getVehicleTypeClick(holder.getAdapterPosition(), select_id);
                }
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mVechineList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout parentLl;
        private ImageView imageIv;
        private TextViewPlayRegular nameTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            parentLl = itemView.findViewById(R.id.parent_ll);
            imageIv = itemView.findViewById(R.id.image_iv);
            nameTv = itemView.findViewById(R.id.name_tv);
        }
    }
}
