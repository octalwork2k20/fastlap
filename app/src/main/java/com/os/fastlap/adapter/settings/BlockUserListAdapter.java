package com.os.fastlap.adapter.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.BlockUserListingBean;

import java.util.ArrayList;

/*
 * Created by anandj on 8/25/2017.
 */

public class BlockUserListAdapter extends RecyclerView.Adapter<BlockUserListAdapter.MyViewHolder> {

    public interface OnItemClickListener {
        void onUnBlockUser(int position);
    }

    private ArrayList<BlockUserListingBean> mBlockUserListingBeen;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

    public BlockUserListAdapter(Context context, ArrayList<BlockUserListingBean> blockUserListingBeen,OnItemClickListener onItemClickListener) {
        this.mBlockUserListingBeen = blockUserListingBeen;
        this.mContext = context;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_block_user_list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.user_name_tv.setText(mBlockUserListingBeen.get(position).getFirstName() + " " + mBlockUserListingBeen.get(position).getLastName());
    }

    @Override
    public int getItemCount() {
        return mBlockUserListingBeen.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView user_name_tv, unblock_tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            user_name_tv = (TextView) itemView.findViewById(R.id.user_name_tv);
            unblock_tv = (TextView) itemView.findViewById(R.id.unblock_tv);

            unblock_tv.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.unblock_tv:
                    if(mOnItemClickListener!=null)
                        mOnItemClickListener.onUnBlockUser(getAdapterPosition());
                    break;
            }
        }
    }
}
