package com.os.fastlap.adapter.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.SettingManageDeviceConnectedModal;

import java.util.ArrayList;

/**
 * Created by anandj on 8/22/2017.
 */

public class ManageDeviceConnectedAdapter extends RecyclerView.Adapter<ManageDeviceConnectedAdapter.MyViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onDisconnectedDevice(int position);
    }


    private ArrayList<SettingManageDeviceConnectedModal> mConnectedList;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

    public ManageDeviceConnectedAdapter(Context context, ArrayList<SettingManageDeviceConnectedModal> connectedModalArrayList, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mConnectedList = connectedModalArrayList;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ManageDeviceConnectedAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_manage_device_connected_child, parent, false));
    }

    @Override
    public void onBindViewHolder(ManageDeviceConnectedAdapter.MyViewHolder holder, int position) {
        holder.txt_device_name.setText(mConnectedList.get(position).getBrand());
    }

    @Override
    public int getItemCount() {
        return mConnectedList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_device_name, txt_disconnected;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_disconnected = itemView.findViewById(R.id.txt_disconnected);
            txt_device_name = itemView.findViewById(R.id.txt_device_name);

            txt_disconnected.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.txt_disconnected:
                    if(mOnItemClickListener!=null)
                        mOnItemClickListener.onDisconnectedDevice(getAdapterPosition());
                    break;
            }
        }
    }
}
