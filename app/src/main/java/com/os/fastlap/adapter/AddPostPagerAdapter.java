package com.os.fastlap.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.AddPostActivity;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 11/27/2017.
 */

public class AddPostPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<PagerBean> mSelectedImage = new ArrayList<>();

    public AddPostPagerAdapter(Context context, ArrayList<PagerBean> selectedImage) {
        mContext = context;
        this.mSelectedImage = selectedImage;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final ViewGroup layout = (ViewGroup) LayoutInflater.from(mContext).inflate(R.layout.post_item_child1, container, false);
        ImageView imgCar = layout.findViewById(R.id.imgCar);
        ImageView imgPlay = layout.findViewById(R.id.imgPlay);
        ImageView imgPagerDelete = layout.findViewById(R.id.imgPagerDelete);
        imgPlay.setVisibility(View.GONE);
        imgPagerDelete.setVisibility(View.VISIBLE);
        imgPagerDelete.setTag(position);

        if (mSelectedImage.get(position).isEdit())
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mSelectedImage.get(position).getImage(), imgCar, Util.getImageLoaderOption(mContext));
        else {
            String uri = Uri.fromFile(mSelectedImage.get(position).getFile()).toString();
            String decoded = Uri.decode(uri);
            ImageLoader.getInstance().displayImage(decoded, imgCar);
        }
        imgPagerDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                if (mSelectedImage.get(pos).isEdit())
                    ((AddPostActivity) mContext).setRemoveImages(mSelectedImage.get(pos).getImage_id());
                mSelectedImage.remove(pos);
                notifyDataSetChanged();
            }
        });
        container.addView(layout);
        return layout;
    }

    @Override
    public int getCount()   {
        return mSelectedImage.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
