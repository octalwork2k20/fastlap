package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.activity.track.TrackDetailActivity;
import com.os.fastlap.beans.UserTrackBeans;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/*
 * Created by anandj on 7/17/2017.
 */

public class FavoriteTrackAdapter extends RecyclerView.Adapter<FavoriteTrackAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<UserTrackBeans> mFavArrayList;
    AuthAPI authAPI;

    public FavoriteTrackAdapter(Context context, ArrayList<UserTrackBeans> arrayList) {
        mContext = context;
        mFavArrayList = arrayList;
        authAPI = new AuthAPI(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_track_favorite_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mFavArrayList.get(position).getImage(), holder.img_family, Util.getImageLoaderOption(mContext));

        holder.fav_unfav_icon.setVisibility(View.VISIBLE);
        holder.fav_unfav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.changeFavTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mFavArrayList.get(position).getTrack_id(), mFavArrayList.get(position).getTrack_id(), "0");
            }
        });

        holder.img_family.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent4 = null;
                intent4 = new Intent(mContext, TrackDetailActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle1.putString(S.user_id, mFavArrayList.get(holder.getAdapterPosition()).getTrack_id());
                bundle1.putString("status", "1");
                intent4.putExtras(bundle1);
                mContext.startActivity(intent4);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFavArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_family;
        ImageView fav_unfav_icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_family = itemView.findViewById(R.id.img_family);
            fav_unfav_icon = itemView.findViewById(R.id.fav_unfav_icon);

            if (MySharedPreferences.getPreferences(mContext, S.user_id).compareTo(ProfileActivity.profileUserId) == 0) {
                fav_unfav_icon.setVisibility(View.VISIBLE);
            } else {
                fav_unfav_icon.setVisibility(View.GONE);
            }
        }
    }
}
