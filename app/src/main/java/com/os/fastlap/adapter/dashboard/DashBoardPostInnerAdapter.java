package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.dashboardmodals.DashboardPostImagesBeans;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.List;

/**
 * Created by anandj on 9/1/2017.
 */

public class DashBoardPostInnerAdapter extends RecyclerView.Adapter<DashBoardPostInnerAdapter.MyViewHodler> {

    private Context mContext;
    private List<DashboardPostImagesBeans> mDashboardPostImagesBeanses;

    public DashBoardPostInnerAdapter(Context context, List<DashboardPostImagesBeans> dashboardPostImagesBeanses) {
        this.mContext = context;
        mDashboardPostImagesBeanses = dashboardPostImagesBeanses;
    }

    @Override
    public DashBoardPostInnerAdapter.MyViewHodler onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHodler(LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item_child, parent, false));
    }

    @Override
    public void onBindViewHolder(DashBoardPostInnerAdapter.MyViewHodler holder, int position) {
        DashboardPostImagesBeans dashboardPostImagesBeans = mDashboardPostImagesBeanses.get(position);
        if(dashboardPostImagesBeans.getType().equalsIgnoreCase("1"))
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL+dashboardPostImagesBeans.getFileName(),holder.imgCar, Util.getImageLoaderOption(mContext));
        else{

        }

    }

    @Override
    public int getItemCount() {
        return mDashboardPostImagesBeanses.size();
    }

    public class MyViewHodler extends RecyclerView.ViewHolder {

        ImageView imgCar;

        public MyViewHodler(View itemView) {
            super(itemView);
            imgCar = itemView.findViewById(R.id.imgCar);
        }
    }
}
