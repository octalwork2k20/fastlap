package com.os.fastlap.adapter.track;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/19/2017.
 */

public class TracktimeLapAdapter extends RecyclerView.Adapter<TracktimeLapAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<BeanTimeLap> mArrayList;

    public TracktimeLapAdapter(Context context, ArrayList<BeanTimeLap> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.mylaps_mylaps_fragment_child2, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BeanTimeLap beanTimeLap = mArrayList.get(position);
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mArrayList.get(position).getUserImage(), holder.userImageIv, Util.getImageLoaderOption(mContext));
        holder.userNameTv.setText(beanTimeLap.getUserName());
        holder.lapDateTv.setText(Util.ConvertDateTimeZoneDate(beanTimeLap.getLapDate()));
        holder.lapTotalTimeTv.setText(beanTimeLap.getTime());
        holder.lapCarModelTv.setText(beanTimeLap.getVehicleBrandName() + " " + beanTimeLap.getVehicleModelName());

        Drawable img;
        if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.dry)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.sun);
        else if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.cloud)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.cloud_unselect);
        else if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.ice)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.ice_unselect);
        else if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.wind)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.ice_unselect);
        else
            img = ContextCompat.getDrawable(mContext, R.mipmap.rain_unselect);

        holder.weatherIv.setImageDrawable(img);

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout rrListParent;
        private CircleImageView userImageIv;
        private LinearLayout userInfoLl;
        private TextViewPlayBold userNameTv;
        private TextViewPlayRegular lapDateTv;
        private SwitchCompat graphSwitch;
        private ImageView graphIv;
        private LinearLayout raceInfoLl;
        private TextViewPlayRegular lapTotalTimeTv;
        private TextViewPlayRegular lapTimeGapTv;
        private TextViewPlayRegular lapCarModelTv;
        private ImageView videoIconIv;
        private ImageView weatherIv;
        private TextViewPlayBold raceTv;


        public MyViewHolder(View itemView) {
            super(itemView);


            rrListParent = (LinearLayout) itemView.findViewById(R.id.rr_list_parent);
            userImageIv = (CircleImageView) itemView.findViewById(R.id.user_image_iv);
            userInfoLl = (LinearLayout) itemView.findViewById(R.id.user_info_ll);
            userNameTv = (TextViewPlayBold) itemView.findViewById(R.id.user_name_tv);
            lapDateTv = (TextViewPlayRegular) itemView.findViewById(R.id.lap_date_tv);
            graphSwitch = (SwitchCompat) itemView.findViewById(R.id.graph_switch);
            graphIv = (ImageView) itemView.findViewById(R.id.graph_iv);
            raceInfoLl = (LinearLayout) itemView.findViewById(R.id.race_info_ll);
            lapTotalTimeTv = (TextViewPlayRegular) itemView.findViewById(R.id.lap_total_time_tv);
            lapTimeGapTv = (TextViewPlayRegular) itemView.findViewById(R.id.lap_time_gap_tv);
            lapCarModelTv = (TextViewPlayRegular) itemView.findViewById(R.id.lap_car_model_tv);
            videoIconIv = (ImageView) itemView.findViewById(R.id.video_icon_iv);
            weatherIv = (ImageView) itemView.findViewById(R.id.weather_iv);
            raceTv = (TextViewPlayBold) itemView.findViewById(R.id.race_tv);

            raceTv.setVisibility(View.GONE);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

            }
        }
    }
}
