package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.AboutInfoFamilyModal;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 7/17/2017.
 */

public class FamilyProfileAdapter extends RecyclerView.Adapter<FamilyProfileAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<AboutInfoFamilyModal> mFamilyArrayList;

    public FamilyProfileAdapter(Context context, ArrayList<AboutInfoFamilyModal> arrayList) {
        mContext = context;
        mFamilyArrayList = arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_family;
        public MyViewHolder(View view) {
            super(view);
            img_family = (ImageView)view.findViewById(R.id.img_family);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_track_favorite_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mFamilyArrayList.get(position).getUrl(), holder.img_family, Util.getImageLoaderOption(mContext));
    }

    @Override
    public int getItemCount() {
        return mFamilyArrayList.size();
    }
}
