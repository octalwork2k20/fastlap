package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.dashboard.DashboardVehicleListActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/14/2017.
 */

public class GarageProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onEditVehicleList(int position);

        void onDeleteVehicleList(int position);

        void onItemVehicleList(int position);
    }

    private Context mContext;
    private ArrayList<BeanVehicleList> mArrayList;
    private OnItemClickListener mOnItemClickListener;

    public GarageProfileAdapter(Context context, ArrayList<BeanVehicleList> list_data) {
        this.mContext = context;
        mArrayList = list_data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolderList(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_garage_child_activity, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolderList)
        {
            ((MyViewHolderList) holder).txt_vechicle_name.setText(mArrayList.get(position).getVehicleBrandIdModal().getVehicleBrandName() + " " +
                    mArrayList.get(position).getVehicleModelIdModal().getVehicleModelName() + " " + mArrayList.get(position).getVehicleVersionIdModal().getVehicleVersionName());
            ((MyViewHolderList) holder).txt_year.setText(mArrayList.get(position).getVehicleYearIdModal().getVehicleYear());
            ((MyViewHolderList) holder).txt_tyre.setText(mArrayList.get(position).getVehicleTyreBrandIdModal().getBrandName() + " " + mArrayList.get(position).getVehicleTyreModelIdModal().getVehichleModelName());

            ((MyViewHolderList) holder).best_position_tv.setText(mArrayList.get(position).getBestRanking());
            ((MyViewHolderList) holder).best_speed_tv.setText(mArrayList.get(position).getBestSpeed());
            ((MyViewHolderList) holder).uploaded_laps_tv.setText(mArrayList.get(position).getUploadedLapCount() + "");
            ((MyViewHolderList) holder).track_visited_tv.setText(mArrayList.get(position).getTrackVisitedCount() + "");

            ((MyViewHolderList) holder).mStringArrayList.clear();
            if(!mArrayList.get(holder.getAdapterPosition()).getImagesList().isEmpty()){
                for(int i=0;i<mArrayList.get(holder.getAdapterPosition()).getImagesList().size();i++) {
                    if(!mArrayList.get(holder.getAdapterPosition()).getImagesList().get(i).isDefault()) {
                        if(i==0) {
                            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mArrayList.get(holder.getAdapterPosition()).getImagesList().get(i).getImage(), ((MyViewHolderList) holder).imgGaragePic, Util.getImageLoaderOption(mContext));
                        }
                        else {
                            ((MyViewHolderList) holder).mStringArrayList.add(mArrayList.get(holder.getAdapterPosition()).getImagesList().get(i).getImage());
                        }
                    }
                }
            }
            else {
                ((MyViewHolderList) holder).imgGaragePic.setImageResource(R.drawable.car_img);
            }


            ((MyViewHolderList) holder).showVehicleImageAdapter.notifyDataSetChanged();
            ((MyViewHolderList) holder).carDetails.setText(mArrayList.get(position).getDescription());
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {

        return 0;
    }

    private void clickOnEdit(MyViewHolderList holderList) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onEditVehicleList(holderList.getAdapterPosition());
    }

    private void clickOnDelete(MyViewHolderList holderList) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onDeleteVehicleList(holderList.getAdapterPosition());
    }

    private void clickonItem(MyViewHolderList holderList) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onItemVehicleList(holderList.getAdapterPosition());
    }

    public class MyViewHolderList extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RecyclerView recycler_view;
        private ShowVehicleImageAdapter showVehicleImageAdapter;
        public ArrayList<String> mStringArrayList;
        private ImageView img_edit, img_delete;
        private TextView txt_vechicle_name;
        private TextView txt_year;
        private TextView txt_tyre;
        private RelativeLayout parent_rl;
        private RelativeLayout parentRl;
        public ImageView imgGaragePic;
        private LinearLayout llCarDetails;
        private TextViewPlayBold txtVechicleName;
        private ImageView imgDelete;
        private ImageView imgEdit;
        private RelativeLayout uploadedLapsLl;
        private RelativeLayout trackVisitedLl;
        private ImageView imgCal;
        private TextViewPlayRegular txtYear;
        private ImageView imgApline;
        private TextViewPlayRegular txtTyre;
        private LinearLayout postionSpeedLl;
        private ImageView imgTropy;
        private ImageView imgSpeed;
        private RecyclerView recyclerView;
        private HorizontalScrollView horizontalScroll;
        private ImageView imageView;
        private TextViewPlayRegular carDetails;

        private TextView best_position_tv;
        private TextView best_speed_tv;
        private TextView uploaded_laps_tv;
        private TextView track_visited_tv;


        public MyViewHolderList(View itemView) {
            super(itemView);

            imgGaragePic = (ImageView) itemView.findViewById(R.id.img_garage_pic);
            llCarDetails = (LinearLayout) itemView.findViewById(R.id.ll_car_details);
            imgCal = (ImageView) itemView.findViewById(R.id.img_cal);
            imgApline = (ImageView) itemView.findViewById(R.id.img_apline);
            imgTropy = (ImageView) itemView.findViewById(R.id.img_tropy);
            imgSpeed = (ImageView) itemView.findViewById(R.id.img_speed);
            img_edit = (ImageView) itemView.findViewById(R.id.img_edit);
            img_delete = (ImageView) itemView.findViewById(R.id.img_delete);
            horizontalScroll = (HorizontalScrollView) itemView.findViewById(R.id.horizontal_scroll);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            carDetails = (TextViewPlayRegular) itemView.findViewById(R.id.car_details);
            recycler_view = (RecyclerView) itemView.findViewById(R.id.recycler_view);
            parent_rl = (RelativeLayout) itemView.findViewById(R.id.parent_rl);
            txt_vechicle_name = itemView.findViewById(R.id.txt_vechicle_name);
            txt_year = (TextViewPlayRegular) itemView.findViewById(R.id.txt_year);
            txt_tyre = (TextViewPlayRegular) itemView.findViewById(R.id.txt_tyre);
            uploadedLapsLl = (RelativeLayout) itemView.findViewById(R.id.uploaded_laps_ll);
            trackVisitedLl = (RelativeLayout) itemView.findViewById(R.id.track_visited_ll);
            postionSpeedLl = (LinearLayout) itemView.findViewById(R.id.postion_speed_ll);
            best_position_tv = itemView.findViewById(R.id.best_position_tv);
            best_speed_tv = itemView.findViewById(R.id.best_speed_tv);
            uploaded_laps_tv = itemView.findViewById(R.id.uploaded_laps_tv);
            track_visited_tv = itemView.findViewById(R.id.track_visited_tv);


            mStringArrayList = new ArrayList<>();
            showVehicleImageAdapter = new ShowVehicleImageAdapter(mContext, mStringArrayList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            recycler_view.setLayoutManager(layoutManager);
            recycler_view.setAdapter(showVehicleImageAdapter);

            img_delete.setOnClickListener(this);
            img_edit.setOnClickListener(this);
            parent_rl.setOnClickListener(this);

            if (MySharedPreferences.getPreferences(mContext, S.user_id).compareTo(ProfileActivity.profileUserId) == 0) {
                img_edit.setVisibility(View.VISIBLE);
                img_delete.setVisibility(View.GONE);
            } else {
                img_edit.setVisibility(View.GONE);
                img_delete.setVisibility(View.GONE);
            }

            if (mContext instanceof DashboardVehicleListActivity) {
                uploadedLapsLl.setVisibility(View.GONE);
                trackVisitedLl.setVisibility(View.GONE);
                postionSpeedLl.setVisibility(View.GONE);
                llCarDetails.setVisibility(View.GONE);
                img_edit.setVisibility(View.GONE);
                img_delete.setVisibility(View.GONE);
                recycler_view.setVisibility(View.GONE);
            }


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_delete:
                    clickOnDelete(this);
                    break;
                case R.id.img_edit:
                    clickOnEdit(this);
                    break;
                case R.id.parent_rl:
                    clickonItem(this);
                    break;
            }
        }
    }

}
