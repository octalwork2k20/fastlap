package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 8/25/2017.
 */

public class PendingRequestProfileAdapter extends RecyclerView.Adapter<PendingRequestProfileAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<ProfileMyFriendsList> mPendingRequestLists;
    private ArrayList<ProfileMyFriendsList> mPendingRequestListsAll = new ArrayList<>();
    PendingRequestInterface pendingRequestInterface;

    public interface PendingRequestInterface
    {
        void confirmClickListner(int position);

        void deleteClickListner(int position);
    }

    public void OnclickListner(PendingRequestInterface pendingRequestInterface)
    {
        this.pendingRequestInterface=pendingRequestInterface;
    }

    public PendingRequestProfileAdapter(Context context, ArrayList<ProfileMyFriendsList> friendsLists) {
        this.mContext = context;
        this.mPendingRequestLists = friendsLists;
        mPendingRequestListsAll.addAll(friendsLists);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_pending_request_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String userName = mPendingRequestLists.get(position).getPersonalInfo().getFirstName() + " " +
                mPendingRequestLists.get(position).getPersonalInfo().getLastName();
        String imageUrl = mPendingRequestLists.get(position).getPersonalInfo().getImage();
        holder.txt_user_name.setText(userName);
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + imageUrl, holder.user_profile, Util.getImageLoaderOption(mContext));
    }

    @Override
    public int getItemCount() {
        return mPendingRequestLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView user_profile;
        TextView txt_user_name;
        TextView confirm_btn;
        TextView delete_btn;

        public MyViewHolder(View itemView) {
            super(itemView);
            user_profile = itemView.findViewById(R.id.user_profile);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            confirm_btn = itemView.findViewById(R.id.confirm_btn);
            delete_btn = itemView.findViewById(R.id.delete_btn);

            confirm_btn.setOnClickListener(this);
            delete_btn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId())
            {
                case R.id.confirm_btn:
                        pendingRequestInterface.confirmClickListner(getAdapterPosition());
                    break;
                case R.id.delete_btn:
                         pendingRequestInterface.deleteClickListner(getAdapterPosition());
                    break;
            }

        }
    }

}
