package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.dashboard.LikesUserActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.dashboardmodals.ReplyBeans;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by abhinava on 7/12/2017.
 */

public class DashboardReplyAdapter extends RecyclerView.Adapter<DashboardReplyAdapter.MyViewHolder> {

    DisplayImageOptions options;
    Context context;
    ArrayList<ReplyBeans> reply_list = new ArrayList<>();
    ReplyInterFace replyInterFace;


    public DashboardReplyAdapter(ArrayList<ReplyBeans> reply_list, Context context) {
        this.reply_list = reply_list;
        this.context = context;
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.profile_default);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageForEmptyUri(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageOnFail(new BitmapDrawable(context.getResources(), default_bitmap))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public interface ReplyInterFace {
        void onClickReplyRowLike(int postion, String status);

        void onClickReplyDelete(int postion);

        void onClickReplyEdit(int postion);
    }

    public void setOnItemClickListener(ReplyInterFace listener) {
        replyInterFace = listener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_layout_reply_row_selector, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        // Receiving side
        byte[] data = Base64.decode(reply_list.get(position).getReplyText(), Base64.NO_WRAP);
        try {
            String text = new String(data, "UTF-8");
            holder.replyRowCommentTv.setText(text);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        holder.replyRowUserNameTv.setText(reply_list.get(position).getReplyUserName());

        holder.replyRowLikeCountTv.setText(reply_list.get(position).getReplyCountLikeData() + "");

        if (Integer.parseInt(reply_list.get(position).getReplyCountLikeData()) > 0) {
            holder.replyRowLikeCountTv.setVisibility(View.VISIBLE);
        } else {
            holder.replyRowLikeCountTv.setVisibility(View.GONE);
        }


        if (reply_list.get(position).getReplyLikedCommentReply().compareTo("0") == 0) {
            holder.replyRowLikeTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
        } else {
            holder.replyRowLikeTv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }

        try {
            holder.replyRowTimeTv.setText(Util.calculateTimeDiffFromNow(reply_list.get(position).getReplyDateTime(), context));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + reply_list.get(position).getReplyUserImage(), holder.replyRowUserIv, Util.getImageLoaderOption(context));

        if (reply_list.get(holder.getAdapterPosition()).getReplyUserId().equalsIgnoreCase(MySharedPreferences.getPreferences(context, S.user_id))) {
            holder.replyRowInfoIv.setVisibility(View.VISIBLE);
        } else {
            holder.replyRowInfoIv.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return reply_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView replyRowUserIv;
        private ImageView replyRowInfoIv;
        private TextViewPlayBold replyRowUserNameTv;
        private TextViewPlayRegular replyRowCommentTv;
        private TextViewPlayRegular replyRowTimeTv;
        private LinearLayout llLikeComment;
        private TextViewPlayRegular replyRowLikeTv;
        private TextViewPlayRegular replyRowLikeCountTv;


        public MyViewHolder(View itemView) {
            super(itemView);


            replyRowUserIv = (ImageView) itemView.findViewById(R.id.reply_row_user_iv);
            replyRowInfoIv = (ImageView) itemView.findViewById(R.id.reply_row_info_iv);
            replyRowUserNameTv = (TextViewPlayBold) itemView.findViewById(R.id.reply_row_user_name_tv);
            replyRowCommentTv = (TextViewPlayRegular) itemView.findViewById(R.id.reply_row_comment_tv);
            replyRowTimeTv = (TextViewPlayRegular) itemView.findViewById(R.id.reply_row_time_tv);
            llLikeComment = (LinearLayout) itemView.findViewById(R.id.ll_like_comment);
            replyRowLikeTv = (TextViewPlayRegular) itemView.findViewById(R.id.reply_row_like_tv);
            replyRowLikeCountTv = (TextViewPlayRegular) itemView.findViewById(R.id.reply_row_like_count_tv);


            replyRowLikeCountTv.setOnClickListener(this);
            replyRowLikeTv.setOnClickListener(this);
            replyRowInfoIv.setOnClickListener(this);
            replyRowUserIv.setOnClickListener(this);
            replyRowUserNameTv.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.reply_row_like_count_tv:

                    Intent intent = new Intent(context, LikesUserActivity.class);
                    intent.putExtra(S.postId, reply_list.get(getAdapterPosition()).getPostId());
                    intent.putExtra(S.userPostCommentId, reply_list.get(getAdapterPosition()).getPostCommentId());
                    intent.putExtra(S.userPostCommentReplyId, reply_list.get(getAdapterPosition()).getPostCommentId());
                    intent.putExtra(S.likeType, I.REPLYLIKE);
                    context.startActivity(intent);

                    break;

                case R.id.reply_row_like_tv:

                    String status = "0";
                    if (reply_list.get(getAdapterPosition()).getReplyLikedCommentReply().compareTo("0") == 0) {
                        status = "1";
                    } else {
                        status = "0";
                    }
                    reply_list.get(getAdapterPosition()).setReplyLikedCommentReply(status);
                    notifyItemChanged(getAdapterPosition());
                    replyInterFace.onClickReplyRowLike(MyViewHolder.this.getAdapterPosition(), status);
                    break;

                case R.id.reply_row_info_iv:
                    //creating a popup menu
                    PopupMenu popup = new PopupMenu(context, DashboardReplyAdapter.MyViewHolder.this.replyRowInfoIv);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.comment_menu);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.edit:
                                    replyInterFace.onClickReplyEdit(getAdapterPosition());
                                    break;
                                case R.id.delete:
                                    replyInterFace.onClickReplyDelete(getAdapterPosition());
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                    break;

                case R.id.reply_row_user_iv:
                case R.id.reply_row_user_name_tv:

                    Intent intent3 = new Intent(context, ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle.putString(S.user_id, reply_list.get(getAdapterPosition()).getReplyUserId());
                    bundle.putString("status", "0");
                    intent3.putExtras(bundle);
                    context.startActivity(intent3);
            }

        }
    }
}
