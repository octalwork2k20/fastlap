package com.os.fastlap.adapter.track;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.community.CommunityMoreInfoActivity;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/19/2017.
 */

public class TrackEventRecyclerAdapter extends RecyclerView.Adapter<TrackEventRecyclerAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<EventsBean> mArrayList;
    int type;

    public TrackEventRecyclerAdapter(Context context, ArrayList<EventsBean> arrayList, int type) {
        this.mContext = context;
        mArrayList = arrayList;
        this.type = type;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.community_fragment_event_child, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        holder.eventNameTv.setText(mArrayList.get(position).getName());
        holder.locationTv.setText(mArrayList.get(position).getLocation());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mArrayList.get(position).getImage(), holder.imgCar, Util.getImageLoaderOption(mContext));
        holder.dateTv.setText(Util.ConvertDateTimeZoneDateMonth(mArrayList.get(position).getStartDate()));
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RelativeLayout rrListParent;
        private ImageView imgCar;
        private TextViewPlayRegular eventNameTv;
        private ImageView countryFlagIv;
        private TextViewPlayRegular locationTv;
        private TextViewPlayBold dateTv;
        private TextViewPlayBold txtMoreInfo;
        private TextViewPlayBold txtTicketsInfo;

        public MyViewHolder(View itemView) {
            super(itemView);

            rrListParent = (RelativeLayout) itemView.findViewById(R.id.rr_list_parent);
            imgCar = (ImageView) itemView.findViewById(R.id.img_car);
            eventNameTv = (TextViewPlayRegular) itemView.findViewById(R.id.event_name_tv);
            countryFlagIv = (ImageView) itemView.findViewById(R.id.country_flag_iv);
            locationTv = (TextViewPlayRegular) itemView.findViewById(R.id.location_tv);
            dateTv = (TextViewPlayBold) itemView.findViewById(R.id.date_tv);
            txtMoreInfo = (TextViewPlayBold) itemView.findViewById(R.id.txt_more_info);
            txtTicketsInfo = (TextViewPlayBold) itemView.findViewById(R.id.txt_tickets_info);

            txtMoreInfo.setOnClickListener(this);
            txtTicketsInfo.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.txt_more_info:
                    Util.startNewActivity((Activity) mContext, CommunityMoreInfoActivity.class,false);
                    break;
            }
        }
    }
}
