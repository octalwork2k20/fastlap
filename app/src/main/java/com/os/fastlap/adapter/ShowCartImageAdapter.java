package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.CartAlbumImagesBeans;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 10/5/2017.
 */

public class ShowCartImageAdapter extends RecyclerView.Adapter<ShowCartImageAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<CartAlbumImagesBeans> mStringArrayList;

    public ShowCartImageAdapter(Context context, ArrayList<CartAlbumImagesBeans> stringArrayList) {
        this.mContext = context;
        this.mStringArrayList = stringArrayList;
    }

    @Override
    public ShowCartImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_graage_child_vechicle_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ShowCartImageAdapter.MyViewHolder holder, int position) {
        if( mStringArrayList.get(position).getType().equalsIgnoreCase("1"))
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mStringArrayList.get(position).getFileName(), holder.imgPagerImage, Util.getImageLoaderOption(mContext));
        else
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mStringArrayList.get(position).getThumbName(), holder.imgPagerImage, Util.getImageLoaderOption(mContext));
    }

    @Override
    public int getItemCount() {
        return mStringArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPagerImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgPagerImage = itemView.findViewById(R.id.imgPagerImage);
        }
    }
}