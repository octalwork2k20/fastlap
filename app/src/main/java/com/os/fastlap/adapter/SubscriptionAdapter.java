package com.os.fastlap.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanSubscription;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by abhinava on 7/12/2017.
 */

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.MyViewHolder> {

    DisplayImageOptions options;
    Context context;
    ArrayList<BeanSubscription> subscription_list = new ArrayList<>();
    Activity activity;
    int imageViewWidth;
    Clicklistner clicklistner;

    public interface Clicklistner {
        void BuyClick(int position);
    }

    public void setOnclickListner(Clicklistner onclickListner) {
        clicklistner = onclickListner;
    }

    public SubscriptionAdapter(ArrayList<BeanSubscription> subscription_list, Context context, Activity activity) {
        this.subscription_list = subscription_list;
        this.context = context;
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.profile_default);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageForEmptyUri(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageOnFail(new BitmapDrawable(context.getResources(), default_bitmap))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        this.activity = activity;

        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        imageViewWidth = width - 150;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_subscription_detail_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BeanSubscription beanSubscription = new BeanSubscription();
        beanSubscription = subscription_list.get(position);

        holder.tvName.setText(beanSubscription.getName());
        if(Double.parseDouble(beanSubscription.getPrice())>0) {
            //holder.tvName.setText(beanSubscription.getName());
            holder.tvAmount.setText("$"+beanSubscription.getPrice());
        }
        else {
            holder.tvAmount.setText(context.getResources().getString(R.string.free));
           // holder.tvName.setText(beanSubscription.getName());
        }
        if (Integer.parseInt(beanSubscription.getVehicles()) == -1) {
            holder.tvVehicle.setText(context.getString(R.string.unlimited));
            holder.tvVehicle.setTextSize(15);
        } else {
            holder.tvVehicle.setText(beanSubscription.getVehicles());
            holder.tvVehicle.setTextSize(17);

        }

        if (Integer.parseInt(beanSubscription.getLaps()) == -1) {
            holder.tvLaps.setText(context.getString(R.string.unlimited));
            holder.tvLaps.setTextSize(15);
        } else {
            holder.tvLaps.setText(beanSubscription.getLaps());
            holder.tvLaps.setTextSize(17);

        }
        if (Integer.parseInt(beanSubscription.getSessions()) == -1) {
            holder.tvSession.setText(context.getString(R.string.unlimited));
            holder.tvSession.setTextSize(15);
        } else {
            holder.tvSession.setText(beanSubscription.getSessions());
            holder.tvSession.setTextSize(17);

        }
        holder.tvFilter.setText(beanSubscription.getTypeofFilters());

        if (Integer.parseInt(beanSubscription.getComparison()) == -1) {
            holder.tvLapsComparable.setText(context.getString(R.string.unlimited));
            holder.tvLapsComparable.setTextSize(15);
        } else {
            holder.tvLapsComparable.setText(beanSubscription.getComparison());
            holder.tvLapsComparable.setTextSize(17);
        }
        holder.tvDataComparison.setText(beanSubscription.getDataComparisonCapability());
        if (Double.parseDouble(beanSubscription.getPrice()) == 0) {
            holder.buynowTv.setVisibility(View.GONE);
        } else {
            holder.buynowTv.setVisibility(View.VISIBLE);
        }
        if(MySharedPreferences.getBooleanPreferences(context, S.checkSubscription)){
            holder.buynowTv.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return subscription_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout parentRl;
       // LinearLayout detailLl;
        TextViewPlayBold tvAmount;
        TextViewPlayBold tvName;
        TextViewPlayRegular tvVehicle;
        TextViewPlayRegular tvSession;
        TextViewPlayRegular tvLaps;
        TextViewPlayRegular tvFilter;
        TextViewPlayRegular tvLapsComparable;
        TextViewPlayRegular tvDataComparison;
        TextViewPlayBold buynowTv;
        RelativeLayout priceLayoutRl;

        public MyViewHolder(View itemView) {
            super(itemView);


            tvAmount = (TextViewPlayBold) itemView.findViewById(R.id.tvAmount);
            tvName = (TextViewPlayBold) itemView.findViewById(R.id.tvName);
            tvSession=(TextViewPlayRegular)itemView.findViewById(R.id.tvSession);

            tvVehicle = (TextViewPlayRegular) itemView.findViewById(R.id.tvVehicle);
            tvLaps = (TextViewPlayRegular) itemView.findViewById(R.id.tvLaps);
            tvFilter = (TextViewPlayRegular) itemView.findViewById(R.id.tvFilter);
            tvLapsComparable = (TextViewPlayRegular) itemView.findViewById(R.id.tvLapsComparable);
            tvDataComparison = (TextViewPlayRegular) itemView.findViewById(R.id.tvDataComparison);

            buynowTv = (TextViewPlayBold) itemView.findViewById(R.id.buynow_tv);
            priceLayoutRl = (RelativeLayout) itemView.findViewById(R.id.price_layout_rl);

            parentRl = (RelativeLayout) itemView.findViewById(R.id.parent_rl);
           // detailLl = (LinearLayout) itemView.findViewById(R.id.detail_ll);



            RelativeLayout.LayoutParams linear_pram = new RelativeLayout.LayoutParams(imageViewWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
            parentRl.setLayoutParams(linear_pram);

            buynowTv.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.buynow_tv:
                    clicklistner.BuyClick(getAdapterPosition());
                    break;
            }
        }
    }
}
