package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanPreferedTrack;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by abhinava on 7/12/2017.
 */

public class PreferedTrackUserAdapter extends RecyclerView.Adapter<PreferedTrackUserAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<BeanPreferedTrack.BeanTrackUserData> userDatas = new ArrayList<>();

    public PreferedTrackUserAdapter(ArrayList<BeanPreferedTrack.BeanTrackUserData> userDatas, Context context) {
        this.userDatas = userDatas;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_prefered_track_user_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //String pending = notification_list.get(position);
        holder.rankTv.setText(userDatas.get(position).getUserRank());
        holder.nameTv.setText(userDatas.get(position).getUserName());
        //  holder.timeTv.setText(Util.ConvertMiliSecondToSecond(userDatas.get(position).getUserTime()));

        holder.timeTv.setText(userDatas.get(position).getUserTime());

        try {
            if (position == 0) {
                Long aLong = 0L;
                holder.timelapTv.setText(Util.MilisecondsToTime(aLong));
            } else {
                Long aLong = Util.TimeToMiliseconds(userDatas.get(position).getUserTime()) - Util.TimeToMiliseconds(userDatas.get(0).getUserTime());
                holder.timelapTv.setText(Util.MilisecondsToTime(aLong));
            }
        } catch (Exception e) {

        }

        if(userDatas.get(position).getUserId().equalsIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)))
        {
            holder.rankTv.setTypeface(Typeface.DEFAULT_BOLD);
            holder.nameTv.setTypeface(Typeface.DEFAULT_BOLD);
            holder.timeTv.setTypeface(Typeface.DEFAULT_BOLD);
            holder.timelapTv.setTypeface(Typeface.DEFAULT_BOLD);
        }
        else
        {
            holder.rankTv.setTypeface(Typeface.DEFAULT);
            holder.nameTv.setTypeface(Typeface.DEFAULT);
            holder.timeTv.setTypeface(Typeface.DEFAULT);
            holder.timelapTv.setTypeface(Typeface.DEFAULT);
        }


    }

    @Override
    public int getItemCount() {
        return userDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextViewPlayRegular rankTv;
        TextViewPlayRegular nameTv;
        TextViewPlayRegular timeTv;
        TextViewPlayRegular timelapTv;

        public MyViewHolder(View itemView) {
            super(itemView);

            rankTv = (TextViewPlayRegular) itemView.findViewById(R.id.rank_tv);
            nameTv = (TextViewPlayRegular) itemView.findViewById(R.id.name_tv);
            timeTv = (TextViewPlayRegular) itemView.findViewById(R.id.time_tv);
            timelapTv = (TextViewPlayRegular) itemView.findViewById(R.id.timelap_tv);
        }

    }
}
