package com.os.fastlap.adapter.dashboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.BetterViewPager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.haipq.android.flagkit.FlagImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.AddPostActivity;
import com.os.fastlap.activity.DashboardActivity;
import com.os.fastlap.activity.dashboard.AdCommentActivity;
import com.os.fastlap.activity.dashboard.CommentActivity;
import com.os.fastlap.activity.dashboard.LikesUserActivity;
import com.os.fastlap.activity.dashboard.ShareFriendsListActivity;
import com.os.fastlap.activity.dashboard.TaggedFriendsListActivity;
import com.os.fastlap.activity.group.GroupDiaryActivity;
import com.os.fastlap.activity.profile.DiaryActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.activity.track.TrackDetailActivity;
import com.os.fastlap.activity.track.TrackDiaryActivity;
import com.os.fastlap.adapter.CustomPagerAdapter;
import com.os.fastlap.beans.BeanAdvertisement;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostImagesBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostListBean;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.os.fastlap.util.constants.I.ITEM;
import static com.os.fastlap.util.constants.I.LOADING;

/*
 * Created by anandj on 3/22/2017.
 */

public class DashBoardPostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnImagePickerDialogSelect {
    private String TAG = DashBoardPostsAdapter.class.getSimpleName();
    @Override
    public void OnCameraSelect() {
        if (mContext instanceof DashboardActivity)
            ((DashboardActivity) mContext).onClickOnshare(sharePosition);
        else if (mContext instanceof CommentActivity)
            ((CommentActivity) mContext).onClickOnshare(sharePosition);
        else if (mContext instanceof DiaryActivity)
            ((DiaryActivity) mContext).onClickOnshare(sharePosition);
        else if (mContext instanceof TrackDiaryActivity)
            ((TrackDiaryActivity) mContext).onClickOnshare(sharePosition);
        else if (mContext instanceof GroupDiaryActivity)
            ((GroupDiaryActivity) mContext).onClickOnshare(sharePosition);
        else if (mContext instanceof ProfileActivity)
            ((ProfileActivity) mContext).onClickOnshare(sharePosition);
    }
    @Override
    public void OnGallerySelect() {

        if (mContext instanceof DashboardActivity)
            ((DashboardActivity) mContext).onCLickOnFb(sharePosition);
        else if (mContext instanceof CommentActivity)
            ((CommentActivity) mContext).onCLickOnFb(sharePosition);
        else if (mContext instanceof DiaryActivity)
            ((DiaryActivity) mContext).onCLickOnFb(sharePosition);
        else if (mContext instanceof TrackDiaryActivity)
            ((TrackDiaryActivity) mContext).onCLickOnFb(sharePosition);
        else if (mContext instanceof GroupDiaryActivity)
            ((GroupDiaryActivity) mContext).onCLickOnFb(sharePosition);
        else if (mContext instanceof ProfileActivity)
            ((ProfileActivity) mContext).onCLickOnFb(sharePosition);

    }
    @Override
    public void onVideoSelect() {
    }
    /* Callback methods for list item*/
    public interface OnItemClickListener {
        void onClickOnLike(int position, String status);
        //void onClickOnshare(int position);
    }
    Context mContext;
    ArrayList<DashboardPostListBean> mPostListBeen = new ArrayList<>();
    ArrayList<BeanAdvertisement> advertisementArrayList = new ArrayList<>();
    SharingDeleget mSharingDeleget;
    OnItemClickListener mOnItemClickListener;
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private boolean isLoadingAdded = false;
    boolean addBool = true;
    private int sharePosition;
    boolean isAddRefresh = false;
    int refreshAdPos;
    AuthAPI authAPI;
    Activity activity;
    SpannableStringBuilder temp9 = null;
    SpannableStringBuilder temp8 = null;
    SpannableStringBuilder temp4 = null;
    SpannableStringBuilder temp5 = null;
    SpannableStringBuilder temp6 = null;
    SpannableStringBuilder temp7 = null;

    SpannableStringBuilder shareTemp1 = null;
    SpannableStringBuilder shareTemp2 = null;
    SpannableStringBuilder shareTemp3 = null;
    SpannableStringBuilder shareTemp4 = null;

    public DashBoardPostsAdapter(ArrayList<DashboardPostListBean> post_list, ArrayList<BeanAdvertisement> advertisementArrayList, Context context, SharingDeleget mSharingDeleget, OnItemClickListener onItemClickListener, Activity activity) {
        this.mPostListBeen = post_list;
        this.mContext = context;
        this.mSharingDeleget = mSharingDeleget;
        this.mOnItemClickListener = onItemClickListener;
        this.advertisementArrayList = advertisementArrayList;
        authAPI = new AuthAPI(context);
        this.activity = activity;
        onImagePickerDialogSelect = this;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.post_list_items, parent, false);
        viewHolder = new MyViewHolder(v1);
        return viewHolder;
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }
    private void getShareFriendsNames(final List<DashboardPostShareUserModal> dashboardPostShareUserModals, final int pos) {
        ClickableSpan clickableSpanFriendsOne = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent3 = new Intent(mContext, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString(S.user_id, mPostListBeen.get(pos).getDashboardPostShareUserModals().get(0).getUserDataBeans().get_id());
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                mContext.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        ClickableSpan clickableSpanFriendsTwo = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent3 = new Intent(mContext, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString(S.user_id, mPostListBeen.get(pos).getDashboardPostShareUserModals().get(1).getUserDataBeans().get_id());
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                mContext.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        ClickableSpan clickableSpanFriendsOthers = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ArrayList<DashboardPostShareUserModal> tagedList = new ArrayList<>();
                tagedList.addAll(dashboardPostShareUserModals);
                Intent intent3 = new Intent(mContext, ShareFriendsListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                intent3.putExtra(S.user_list, tagedList);
                mContext.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        if (dashboardPostShareUserModals.size() == 1) {
            shareTemp2 = Util.getSpannableText(dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
            shareTemp2.setSpan(clickableSpanFriendsOne, 0, shareTemp2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        else if (dashboardPostShareUserModals.size() == 2) {
            shareTemp2 = Util.getSpannableText(dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
            shareTemp2.setSpan(clickableSpanFriendsOne, 0, shareTemp2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            shareTemp3 = Util.getSpannableText(" and ", "#414141");

            shareTemp4 = Util.getSpannableText(dashboardPostShareUserModals.get(1).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(1).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
            shareTemp4.setSpan(clickableSpanFriendsTwo, 0, shareTemp4.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        } else {
            shareTemp2 = Util.getSpannableText(dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getFirstName() + " " + dashboardPostShareUserModals.get(0).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
            shareTemp2.setSpan(clickableSpanFriendsOne, 0, shareTemp2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            shareTemp3 = Util.getSpannableText(" and ", "#414141");

            shareTemp4 = Util.getSpannableText(String.valueOf(dashboardPostShareUserModals.size() - 1) + " others ", "#bd2436");
            shareTemp4.setSpan(clickableSpanFriendsOthers, 0, shareTemp4.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        shareTemp1 = Util.getSpannableText(" " + mContext.getString(R.string.shared_a_post), "#414141");
    }

    private void getSelectedFriendsNames(final List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeanses, final int pos) {
        ClickableSpan clickableSpanFriendsOne = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent3 = new Intent(mContext, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString(S.user_id, mPostListBeen.get(pos).getDashboardCommentPostTagFriendsBeanses().get(0).getFriendIdBean().get_id());
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                mContext.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        ClickableSpan clickableSpanFriendsTwo = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent3 = new Intent(mContext, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString(S.user_id, mPostListBeen.get(pos).getDashboardCommentPostTagFriendsBeanses().get(1).getFriendIdBean().get_id());
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                mContext.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
                // ds.setColor(Color.TRANSPARENT);
            }
        };
        ClickableSpan clickableSpanFriendsOthers = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ArrayList<DashboardCommentPostTagFriendsBeans> tagedList = new ArrayList<>();
                tagedList.addAll(dashboardCommentPostTagFriendsBeanses);
                Intent intent3 = new Intent(mContext, TaggedFriendsListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString("status", "0");
                intent3.putExtras(bundle);
                intent3.putExtra(S.user_list, tagedList);
                mContext.startActivity(intent3);
            }

            public void updateDrawState(TextPaint ds) {
                // override updateDrawState
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };
        if (dashboardCommentPostTagFriendsBeanses.size() == 1) {
            temp4 = Util.getSpannableText(" with ", "#414141");
            temp5 = Util.getSpannableText(dashboardCommentPostTagFriendsBeanses.get(0).getFriendIdBean().getPersonalInfo().getFirstName() + " " + dashboardCommentPostTagFriendsBeanses.get(0).getFriendIdBean().getPersonalInfo().getLastName(), "#bd2436");

            temp5.setSpan(clickableSpanFriendsOne, 0, temp5.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else if (dashboardCommentPostTagFriendsBeanses.size() == 2) {
            temp4 = Util.getSpannableText(" with ", "#414141");
            temp5 = Util.getSpannableText(dashboardCommentPostTagFriendsBeanses.get(0).getFriendIdBean().getPersonalInfo().getFirstName() + " " + dashboardCommentPostTagFriendsBeanses.get(0).getFriendIdBean().getPersonalInfo().getLastName(), "#bd2436");

            temp5.setSpan(clickableSpanFriendsOne, 0, temp5.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            temp6 = Util.getSpannableText(" and ", "#414141");
            temp7 = Util.getSpannableText(dashboardCommentPostTagFriendsBeanses.get(1).getFriendIdBean().getPersonalInfo().getFirstName() + " " + dashboardCommentPostTagFriendsBeanses.get(1).getFriendIdBean().getPersonalInfo().getLastName(), "#bd2436");

            temp7.setSpan(clickableSpanFriendsTwo, 0, temp7.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            temp4 = Util.getSpannableText(" with ", "#414141");
            temp5 = Util.getSpannableText(dashboardCommentPostTagFriendsBeanses.get(0).getFriendIdBean().getPersonalInfo().getFirstName() + " " + dashboardCommentPostTagFriendsBeanses.get(0).getFriendIdBean().getPersonalInfo().getLastName(), "#bd2436");

            temp5.setSpan(clickableSpanFriendsOne, 0, temp5.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            temp6 = Util.getSpannableText(" and ", "#414141");
            temp7 = Util.getSpannableText(String.valueOf(dashboardCommentPostTagFriendsBeanses.size() - 1) + " others ", "#bd2436");
            temp7.setSpan(clickableSpanFriendsOthers, 0, temp7.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        Intent intent3 = new Intent(mContext, ProfileActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                        bundle.putString(S.user_id, mPostListBeen.get(viewHolder.getAdapterPosition()).getUserDataBeans().get_id());
                        bundle.putString("status", "0");
                        intent3.putExtras(bundle);
                        mContext.startActivity(intent3);
                    }

                    public void updateDrawState(TextPaint ds) {
                        // override updateDrawState
                        ds.setUnderlineText(false); // set to false to remove underline
                    }
                };
                // Shared post
                final MyViewHolder holder = (MyViewHolder) viewHolder;

                DashboardPostListBean dashboardPostListBean=mPostListBeen.get(position);
                if (dashboardPostListBean.getDashboardPostShareUserModals().size() == 0) {
                    holder.postShareText.setVisibility(View.GONE);
                    holder.shareCountTv.setVisibility(View.GONE);
                }
                else {
                    holder.shareCountTv.setVisibility(View.VISIBLE);
                    holder.shareCountTv.setText(dashboardPostListBean.getDashboardPostShareUserModals().size() + " " + mContext.getString(R.string.share));
                    if (mContext instanceof DiaryActivity) {
                        if (!dashboardPostListBean.getUserDataBeans().get_id().equalsIgnoreCase(ProfileActivity.profileUserId))
                        {
                            holder.postShareText.setVisibility(View.VISIBLE);
                            shareTemp2 = null;
                            shareTemp1 = null;
                            shareTemp3 = null;
                            shareTemp4 = null;
                            getShareFriendsNames(mPostListBeen.get(position).getDashboardPostShareUserModals(), position);

                            Spanned turmsAndConditionSpanned = (Spanned) TextUtils.concat(
                                    shareTemp2 == null ? "" : shareTemp2,
                                    shareTemp3 == null ? "" : shareTemp3,
                                    shareTemp4 == null ? "" : shareTemp4,
                                    shareTemp1);
                            holder.postShareText.setMovementMethod(LinkMovementMethod.getInstance());
                            holder.postShareText.setText(new SpannableStringBuilder(turmsAndConditionSpanned));
                        }
                    }
                    else {
                        holder.postShareText.setVisibility(View.VISIBLE);
                        shareTemp2 = null;
                        shareTemp1 = null;
                        shareTemp3 = null;
                        shareTemp4 = null;
                        getShareFriendsNames(mPostListBeen.get(position).getDashboardPostShareUserModals(), position);

                        Spanned turmsAndConditionSpanned = (Spanned) TextUtils.concat(
                                shareTemp2 == null ? "" : shareTemp2,
                                shareTemp3 == null ? "" : shareTemp3,
                                shareTemp4 == null ? "" : shareTemp4,
                                shareTemp1);
                        holder.postShareText.setMovementMethod(LinkMovementMethod.getInstance());
                        holder.postShareText.setText(new SpannableStringBuilder(turmsAndConditionSpanned));
                    }
                }
                SpannableStringBuilder temp1 = null;
                SpannableStringBuilder temp2 = null;
                SpannableStringBuilder temp3 = null;
                temp4 = null;
                temp5 = null;
                temp6 = null;
                temp7 = null;
                //    String m = mPostListBeen.get(position).getUserDataBeans().getPersonalInfo().getFirstName() + " " + mPostListBeen.get(position).getUserDataBeans().getPersonalInfo().getLastName().length();
                temp1 = Util.getSpannableText(mPostListBeen.get(position).getUserDataBeans().getPersonalInfo().getFirstName() + " " + mPostListBeen.get(position).getUserDataBeans().getPersonalInfo().getLastName(), "#bd2436");
                temp1.setSpan(clickableSpan, 0, temp1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                // holder.lapNameTv.setText(name);
                if (mPostListBeen.get(position).getDashboardFeelingIdBeans() != null) {
                    temp2 = Util.getSpannableText(" - Feeling ", "#414141");
                    temp3 = Util.getSpannableText(Util.getEmoticon(Integer.parseInt(mPostListBeen.get(position).getDashboardFeelingIdBeans().getUnicodes().replace("U+", "0x").substring(2), 16))
                            + " " + mPostListBeen.get(position).getDashboardFeelingIdBeans().getName(), "#bd2436");
                }

                //tagFriend = "";
                if (mPostListBeen.get(position).getDashboardCommentPostTagFriendsBeanses().size() != 0)
                    getSelectedFriendsNames(mPostListBeen.get(position).getDashboardCommentPostTagFriendsBeanses(), position);

              /*  if (temp5 != null) {
                    String k = mPostListBeen.get(position).getDashboardCommentPostTagFriendsBeanses().get(0).getFriendIdBean().getPersonalInfo().getFirstName() + " " + mPostListBeen.get(position).getDashboardCommentPostTagFriendsBeanses().get(0).getFriendIdBean().getPersonalInfo().getLastName();
                    temp5.setSpan(clickableSpanFriendsOne, 0, k.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }*/

              if(mPostListBeen.get(position).getTrackData() != null){
                  temp8 = Util.getSpannableText("- checked in at ", "#414141");
                  temp9 = Util.getSpannableText(mPostListBeen.get(position).getTrackData().getName(), "#bd2436");
                  Spanned turmsAndConditionSpanned = (Spanned) TextUtils.concat(
                          temp1,
                          temp2 == null ? "" : temp2,
                          temp3 == null ? "" : temp3,
                          temp8 == null ? "" : temp8,
                          temp9 == null ? "" : temp9,
                          /*temp4 == null ? "" : temp4,
                          temp5 == null ? "" : temp5,*/
                          temp6 == null ? "" : temp6,
                          temp7 == null ? "" : temp7,
                          temp4 == null ? "" : temp4,
                          temp5 == null ? "" : temp5
                          /*temp8 == null ? "" : temp8,
                          temp9 == null ? "" : temp9*/);

                  holder.lapNameTv.setMovementMethod(LinkMovementMethod.getInstance());
                  holder.lapNameTv.setText(new SpannableStringBuilder(turmsAndConditionSpanned));
              }
              else {
                  Spanned turmsAndConditionSpanned = (Spanned) TextUtils.concat(
                          temp1,
                          temp2 == null ? "" : temp2,
                          temp3 == null ? "" : temp3,
                          temp4 == null ? "" : temp4,
                          temp5 == null ? "" : temp5,
                          temp6 == null ? "" : temp6,
                          temp7 == null ? "" : temp7);

                  holder.lapNameTv.setMovementMethod(LinkMovementMethod.getInstance());
                  holder.lapNameTv.setText(new SpannableStringBuilder(turmsAndConditionSpanned));
              }

                holder.userNameTv.setText(mPostListBeen.get(position).getUserDataBeans().getUsername());
                Glide.with(mContext).load(S.IMAGE_BASE_URL + dashboardPostListBean.getUserDataBeans().getPersonalInfo().getImage()).into(holder.userProfileIv);
               // ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mPostListBeen.get(position).getUserDataBeans().getPersonalInfo().getImage(), holder.userProfileIv, Util.getImageLoaderOption(mContext));
                String trackImg = "";
                if (mPostListBeen.get(position).getTrackData() != null) {
                    holder.circuitNameTv.setVisibility(View.VISIBLE);
                    holder.circuitTimeTv.setVisibility(View.VISIBLE);
                    holder.trackInfoLl.setVisibility(View.VISIBLE);

                    holder.circuitNameTv.setText(mPostListBeen.get(position).getTrackData().getName());
                    //holder.circuitTimeTv.setText(String.valueOf(mPostListBeen.get(position).getTrackData().getTrackLength()) + "Km");
                    holder.circuitTimeTv.setText(mPostListBeen.get(position).getTrackData().getCountry());
                    try {
                        holder.countryFlage.setCountryCode(Util.getCountryCode(mPostListBeen.get(position).getTrackData().getCountry()));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    trackImg = mPostListBeen.get(position).getTrackData().getImage();
                } else {
                    holder.circuitNameTv.setVisibility(View.GONE);
                    holder.circuitTimeTv.setVisibility(View.GONE);
                    holder.trackInfoLl.setVisibility(View.GONE);
                }
                if (mPostListBeen.get(position).getTrackData() == null && mPostListBeen.get(position).getDashboardPostImagesBeans().size() == 0) {
                    holder.betterViewPager.setVisibility(View.GONE);
                    holder.postImageIv.setVisibility(View.GONE);
                    holder.imageInfoRl.setVisibility(View.GONE);
                    holder.arraowleft_iv.setVisibility(View.GONE);
                    holder.arraowright_iv.setVisibility(View.GONE);

                }
                else if (mPostListBeen.get(position).getTrackData() == null && mPostListBeen.get(position).getDashboardPostImagesBeans().size() == 1) {
                    holder.arraowleft_iv.setVisibility(View.GONE);
                    holder.arraowright_iv.setVisibility(View.GONE);
                    holder.betterViewPager.setVisibility(View.GONE);
                    holder.imageInfoRl.setVisibility(View.VISIBLE);
                    holder.postImageIv.setVisibility(View.VISIBLE);
                    if (mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getType().equalsIgnoreCase("2")) {
                        holder.imgPlay.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getThumbName(), holder.postImageIv, Util.getImageLoaderOptionDefaultNull(mContext));
                    } else {
                        holder.imgPlay.setVisibility(View.GONE);
                        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getFileName(), holder.postImageIv, Util.getImageLoaderOptionDefaultNull(mContext));
                    }

                }
                else if (mPostListBeen.get(position).getTrackData() != null && mPostListBeen.get(position).getDashboardPostImagesBeans().size() == 0) {
                    holder.betterViewPager.setVisibility(View.GONE);
                    holder.postImageIv.setVisibility(View.VISIBLE);
                    holder.imageInfoRl.setVisibility(View.VISIBLE);
                    holder.arraowleft_iv.setVisibility(View.GONE);
                    holder.arraowright_iv.setVisibility(View.GONE);
                    // if(mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getType().equalsIgnoreCase(S.IMAGETYPE))
                    Glide.with(mContext).load(S.IMAGE_BASE_URL + trackImg).into(holder.userProfileIv);

                 //   ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + trackImg, holder.postImageIv, Util.getImageLoaderOptionDefaultNull(mContext));

                }

                else {
                    holder.customPagerAdapter = new CustomPagerAdapter(mContext, mPostListBeen.get(position).getDashboardPostImagesBeans(), trackImg);
                    holder.betterViewPager.setAdapter(holder.customPagerAdapter);
                    holder.customPagerAdapter.notifyDataSetChanged();
                    // holder.dots_indicator.setViewPager(holder.betterViewPager);
                    holder.betterViewPager.setVisibility(View.VISIBLE);
                    holder.imageInfoRl.setVisibility(View.VISIBLE);
                    holder.postImageIv.setVisibility(View.GONE);



                    if (trackImg.isEmpty()) {
                        if (mPostListBeen.get(position).getDashboardPostImagesBeans().size()>0) {
                            holder.arraowleft_iv.setVisibility(View.GONE);
                            holder.arraowright_iv.setVisibility(View.VISIBLE);
                        } else {
                            holder.arraowleft_iv.setVisibility(View.GONE);
                            holder.arraowright_iv.setVisibility(View.GONE);
                        }
                    } else {
                        if(mPostListBeen.get(position).getDashboardPostImagesBeans().size()>0){
                            holder.arraowleft_iv.setVisibility(View.GONE);
                            holder.arraowright_iv.setVisibility(View.VISIBLE);
                        }
                        else {
                            holder.arraowleft_iv.setVisibility(View.GONE);
                            holder.arraowright_iv.setVisibility(View.GONE);
                        }

                    }
                }

                if (mPostListBeen.get(position).getDescription().isEmpty()) {
                    holder.desc_tv.setVisibility(View.GONE);
                } else {
                    holder.desc_tv.setVisibility(View.VISIBLE);
                    holder.desc_tv.setText(mPostListBeen.get(position).getDescription());
                }
                if (mPostListBeen.get(position).getWeather().equalsIgnoreCase(""))
                    holder.whetherLyt.setVisibility(View.GONE);
                else {
                    holder.weatherTv.setText(mPostListBeen.get(position).getWeather());
                    holder.whetherLyt.setVisibility(View.VISIBLE);
                    //img.setBounds(0, 0, 30, 30);
                    //Drawable img = ContextCompat.getDrawable(mContext, R.mipmap.sun_select);
                    /*if (mPostListBeen.get(position).getWeather().equalsIgnoreCase(mContext.getString(R.string.dry)))
                        img = ContextCompat.getDrawable(mContext, R.mipmap.sun);*/
                }
                if (mPostListBeen.get(position).getSpeed().isEmpty())
                    holder.timerLyt.setVisibility(View.GONE);
                else {
                    holder.timerLyt.setVisibility(View.VISIBLE);
                    double time= Double.parseDouble(mPostListBeen.get(position).getSpeed());
                    holder.timerTv.setText(new DecimalFormat("##.##").format(time) + " " + mContext.getString(R.string.km_per_hour));
                }

                if (Integer.parseInt(mPostListBeen.get(position).getLikedPostCount()) > 0) {
                    holder.likeCountTv.setText(mPostListBeen.get(position).getLikedPostCount());
                    holder.likeCountTv.setVisibility(View.VISIBLE);
                    holder.likeIconRed.setVisibility(View.VISIBLE);
                    holder.likeIconGray.setVisibility(View.GONE);
                    holder.likeTextTv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                } else {
                    holder.likeIconRed.setVisibility(View.GONE);
                    holder.likeIconGray.setVisibility(View.VISIBLE);
                    holder.likeCountTv.setVisibility(View.GONE);
                }

                if(mPostListBeen.get(position).getLapTime()!=null){
                    holder.mLytLapData.setVisibility(View.VISIBLE);
                    holder.mLapLocation.setText(mPostListBeen.get(position).getLapTime().getTitle());
                    holder.mLapVehicle.setText(mPostListBeen.get(position).getLapTime().getLap_vehicle());
                    double time= Double.parseDouble(mPostListBeen.get(position).getLapTime().getLap_speed());
                    holder.mLapSpeed.setText(new DecimalFormat("##.##").format(time) + " " + mContext.getString(R.string.km_per_hour));
                    holder.mLapTime.setText(mPostListBeen.get(position).getLapTime().getLap_time());
                }
                else {
                    holder.mLytLapData.setVisibility(View.GONE);
                }

                if (mPostListBeen.get(position).getCommentPostCount().equalsIgnoreCase("0")){
                    holder.commentCountTv.setVisibility(View.GONE);
                    holder.cmtLyt.setVisibility(View.GONE);
                }
                else {
                    holder.cmtLyt.setVisibility(View.VISIBLE);
                    if(mPostListBeen.get(position).getCommentBeans()!=null){
                        holder.commentCountTv.setVisibility(View.VISIBLE);
                        holder.cmtUserDesc.setText(mPostListBeen.get(position).getCommentBeans().getComment_text());
                        holder.cmtUserName.setText(mPostListBeen.get(position).getCommentBeans().getComment_user_name());
                        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mPostListBeen.get(position).getCommentBeans().getComment_user_image(), holder.cmtUserImg, Util.getImageLoaderOption(mContext));
                    }
                    if (mPostListBeen.get(position).getCommentPostCount().equalsIgnoreCase("1"))
                        holder.commentCountTv.setText(mPostListBeen.get(position).getCommentPostCount() + " " + mContext.getString(R.string.comment));
                    else
                        holder.commentCountTv.setText(mPostListBeen.get(position).getCommentPostCount() + " " + mContext.getString(R.string.comments));
                }
                holder.dashboardPostImagesBeansList.addAll(mPostListBeen.get(position).getDashboardPostImagesBeans());
                if (mPostListBeen.get(position).getBeanVehicleList() != null) {
                    holder.carnameLyt.setVisibility(View.VISIBLE);
                    holder.carNameTv.setText(mPostListBeen.get(position).getBeanVehicleList().getVehicleBrandIdModal().getVehicleBrandName() + " " + mPostListBeen.get(position).getBeanVehicleList().getVehicleModelIdModal().getVehicleModelName() + " " + mPostListBeen.get(position).getBeanVehicleList().getVehicleTypeIdModal().getName());
                } else
                    holder.carnameLyt.setVisibility(View.GONE);

                try {
                    holder.dateTimeTv.setText(Util.setTimeAgo(mContext,mPostListBeen.get(position).getCreated()));
                } catch (ParseException e) {
                    Log.e(TAG, e.toString());
                }

                holder.mAdvertiesView.setVisibility(View.GONE);
                holder.mAddCardView.setVisibility(View.GONE);


                if (advertisementArrayList != null)
                {
                    if (advertisementArrayList.size() > 0) {
                        if (position % 5 == 0) {
                            if (addBool) {
                                int min = 0;
                                int max = advertisementArrayList.size();
                                Random r = new Random();
                                int i1 = r.nextInt(max - min + 1) + min;

                                if (i1 == advertisementArrayList.size()) {
                                    i1 = i1 - 1;
                                }

                                if (isAddRefresh) {
                                    // int pos = (int) holder.ad_like_layout.getTag();
                                    i1 = refreshAdPos;
                                    isAddRefresh = false;
                                }

                                holder.mAdvertiesView.setVisibility(View.VISIBLE);
                                holder.mAddCardView.setVisibility(View.GONE);
                                Glide.with(mContext).load(S.IMAGE_BASE_URL + advertisementArrayList.get(i1).getAddImage()).into(holder.adver_image_iv);
                                holder.adver_desc_tv.setText(advertisementArrayList.get(i1).getAddDesc());
                                Glide.with(mContext).load(S.IMAGE_BASE_URL + advertisementArrayList.get(i1).getCompanyLogo()).into(holder.adCompanyLogoIv);
                                if(TextUtils.isEmpty(advertisementArrayList.get(i1).getAddName())) {
                                    holder.adCompanyNameTv.setText(mContext.getString(R.string.new_lap_lane));
                                }
                                else {
                                    holder.adCompanyNameTv.setText(advertisementArrayList.get(i1).getAddName());
                                }
                                holder.tvSponserd.setText(advertisementArrayList.get(i1).getSponsered());

                                if (advertisementArrayList.get(i1).getLikedPost().contentEquals("0")){
                                    holder.add_likeIconGray.setVisibility(View.VISIBLE);
                                    holder.add_likeIconRed.setVisibility(View.GONE);
                                    holder.ad_like_text_tv.setTextColor(ContextCompat.getColor(mContext, R.color.gray_text_color));
                                }
                                else {
                                    holder.add_likeIconGray.setVisibility(View.GONE);
                                    holder.add_likeIconRed.setVisibility(View.VISIBLE);
                                    holder.ad_like_text_tv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                                }

                                if (Integer.parseInt(advertisementArrayList.get(i1).getLikeCount()) > 0) {
                                    holder.ad_like_count_tv.setText(advertisementArrayList.get(i1).getLikeCount());
                                    holder.ad_like_count_tv.setVisibility(View.VISIBLE);
                                    holder.add_likeIconGray.setVisibility(View.GONE);
                                    holder.add_likeIconRed.setVisibility(View.VISIBLE);
                                    holder.ad_like_text_tv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                                } else {
                                    holder.add_likeIconGray.setVisibility(View.VISIBLE);
                                    holder.add_likeIconRed.setVisibility(View.GONE);
                                    holder.ad_like_count_tv.setVisibility(View.GONE);
                                }

                                if (advertisementArrayList.get(i1).getCommentCount().equalsIgnoreCase("0"))
                                    holder.ad_comment_count_tv.setVisibility(View.GONE);
                                else {
                                    holder.ad_comment_count_tv.setVisibility(View.VISIBLE);
                                    if (advertisementArrayList.get(i1).getCommentCount().equalsIgnoreCase("1"))
                                        holder.ad_comment_count_tv.setText(advertisementArrayList.get(i1).getCommentCount() + " " + mContext.getString(R.string.comment));
                                    else
                                        holder.ad_comment_count_tv.setText(advertisementArrayList.get(i1).getCommentCount() + " " + mContext.getString(R.string.comments));
                                }

                                holder.ad_like_layout.setTag(i1);
                                holder.ad_comment_count_tv.setTag(i1);
                                holder.ad_comment_layout.setTag(i1);
                                holder.ad_like_count_tv.setTag(i1);
                                holder.adver_rl.setTag(i1);
                                addBool = false;

                            } else {
                                AdRequest adRequest = new AdRequest.Builder().build();
                                Log.e("tag","add"+adRequest.getContentUrl());
                                if(adRequest.getContentUrl()!=null){
                                    holder.adView.loadAd(adRequest);
                                    holder.mAdvertiesView.setVisibility(View.GONE);
                                    holder.mAddCardView.setVisibility(View.VISIBLE);
                                    addBool = true;
                                }
                            }
                        }
                    }
                }

                holder.betterViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int pos) {

                        int imageSize = 0;
                        imageSize = mPostListBeen.get(viewHolder.getAdapterPosition()).getDashboardPostImagesBeans().size();
                        if (mPostListBeen.get(viewHolder.getAdapterPosition()).getTrackData() != null) {
                            imageSize = imageSize + 1;
                        }

                        if (pos == 0) {
                            holder.arraowleft_iv.setVisibility(View.GONE);
                        } else if (imageSize > 1) {
                            holder.arraowleft_iv.setVisibility(View.VISIBLE);
                        }

                        if ((imageSize - 1) == pos) {
                            holder.arraowright_iv.setVisibility(View.GONE);
                        } else if (imageSize > 1) {
                            holder.arraowright_iv.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
                holder.ad_like_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int) holder.ad_like_layout.getTag();

                        String status = "0";
                        int likeCount = Integer.parseInt(advertisementArrayList.get(pos).getLikeCount());
                        if (advertisementArrayList.get(pos).getLikedPost().compareTo("0") == 0) {
                            status = "1";
                            likeCount = likeCount + 1;
                        } else {
                            status = "0";
                            likeCount = likeCount - 1;
                        }
                        advertisementArrayList.get(pos).setLikedPost(status);
                        advertisementArrayList.get(pos).setLikeCount(likeCount + "");
                        authAPI.adLikeByUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), advertisementArrayList.get(pos).getId(), status);
                        addBool = true;
                        isAddRefresh = true;
                        refreshAdPos = pos;
                        notifyItemChanged(holder.getAdapterPosition());
                    }
                });
                holder.ad_comment_count_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int) holder.ad_comment_count_tv.getTag();
                        if (!(mContext instanceof CommentActivity)) {
                            Intent intent1 = new Intent(mContext, AdCommentActivity.class);
                            intent1.putExtra(S.postId, advertisementArrayList.get(pos).getId());
                            intent1.putExtra(S.position, pos + "");
                            activity.startActivityForResult(intent1, I.ADCOMMENT_ACTIVITY_RESULT_CODE);
                        }

                    }
                });
                holder.ad_comment_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int) holder.ad_comment_layout.getTag();
                        if (!(mContext instanceof CommentActivity)) {
                            Intent intent1 = new Intent(mContext, AdCommentActivity.class);
                            intent1.putExtra(S.postId, advertisementArrayList.get(pos).getId());
                            intent1.putExtra(S.position, pos + "");
                            activity.startActivityForResult(intent1, I.ADCOMMENT_ACTIVITY_RESULT_CODE);
                        }
                    }
                });
                holder.ad_like_count_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int) holder.ad_like_count_tv.getTag();
                        Intent intentlike = new Intent(mContext, LikesUserActivity.class);
                        intentlike.putExtra(S.postId, advertisementArrayList.get(pos).getId());
                        intentlike.putExtra(S.likeType, I.ADLIKE);
                        mContext.startActivity(intentlike);
                    }
                });

                holder.adver_rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            int pos = (int) holder.ad_like_count_tv.getTag();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(advertisementArrayList.get(pos).getAddURL()));
                            mContext.startActivity(browserIntent);
                        }
                        catch (Exception e){
                            Log.e("tag","Exception :"+e);
                        }
                    }
                });

                break;
            case I.LOADING:
                break;
        }
    }
    @Override
    public int getItemCount() {
        return mPostListBeen.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mPostListBeen.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(DashboardPostListBean mc) {
        mPostListBeen.add(mc);
        notifyItemInserted(mPostListBeen.size() - 1);
    }

    public void addAll(ArrayList<DashboardPostListBean> dashboardPostListBeen) {
        for (DashboardPostListBean mc : dashboardPostListBeen) {
            add(mc);
        }
    }

    public void remove(DashboardPostListBean city) {
        int position = mPostListBeen.indexOf(city);
        if (position > -1) {
            mPostListBeen.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public DashboardPostListBean getItem(int position) {
        return mPostListBeen.get(position);
    }
    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CircleImageView userProfileIv;
        private TextView lapNameTv;
        private TextView userNameTv;
        private TextView dateTimeTv;
        private ImageView postImageIv;
        private TextView circuitNameTv;
        private TextView circuitTimeTv;
        private FlagImageView countryFlage;
        private TextView carNameTv;
        private LinearLayout carnameLyt;
        private LinearLayout whetherLyt;
        private LinearLayout timerLyt;
        //private TextView rankTv;
        private TextView weatherTv;
        private TextView timerTv;
        private LinearLayout likeLayout;
        private TextView likeCountTv;
        private LinearLayout commentLayout;
        private TextView commentCountTv;
        private LinearLayout shareLayout;
        private TextView shareCountTv;
        private TextView likeTextTv;
        private RecyclerView recycler_view;
        private DashBoardPostInnerAdapter dashBoardPostInnerAdapter;
        private List<DashboardPostImagesBeans> dashboardPostImagesBeansList;
        private BetterViewPager betterViewPager;
        private CustomPagerAdapter customPagerAdapter;
        private RelativeLayout adver_rl;
        private ImageView adver_image_iv;
        private TextView adver_desc_tv;
        private TextView desc_tv;
        private AdView adView;
        private CardView mAddCardView;
        private CardView mAdvertiesView;
        private LinearLayout option_layout;
        private LinearLayout trackInfoLl;
        private RelativeLayout imageInfoRl;
        private ImageView arraowleft_iv;
        private ImageView arraowright_iv;
        private ImageView imgPlay;
        private TextView postShareText;

        private ImageView adCompanyLogoIv;
        private ImageView likeIconGray,likeIconRed,add_likeIconGray,add_likeIconRed;
        private TextView adCompanyNameTv;
        private TextView tvSponserd;

        private TextView ad_like_count_tv;
        private TextView ad_comment_count_tv;
        private LinearLayout ad_like_layout;
        private TextView ad_like_text_tv;
        private LinearLayout ad_comment_layout;

        LinearLayout mLytLapData;
        TextViewPlayRegular mLapVehicle;
        TextViewPlayRegular mLapTime;
        TextViewPlayRegular mLapSpeed;
        TextViewPlayRegular mLapLocation;

        RelativeLayout cmtLyt;
        CircleImageView cmtUserImg;
        TextView cmtUserDesc;
        TextView cmtUserName;


        public MyViewHolder(View itemView) {
            super(itemView);

            likeIconGray=(ImageView)itemView.findViewById(R.id.like_icon_gray);
            likeIconRed=(ImageView)itemView.findViewById(R.id.like_icon_red);
            add_likeIconRed=(ImageView)itemView.findViewById(R.id.add_like_icon_red);
            add_likeIconGray=(ImageView)itemView.findViewById(R.id.add_like_icon_gray);
            mLytLapData = (LinearLayout) itemView.findViewById(R.id.lytLapData);
            mLapVehicle = (TextViewPlayRegular) itemView.findViewById(R.id.tvLapVehicle);
            mLapTime = (TextViewPlayRegular) itemView.findViewById(R.id.tvLapTime);
            mLapSpeed = (TextViewPlayRegular) itemView.findViewById(R.id.tvLapSpeed);
            mLapLocation = (TextViewPlayRegular) itemView.findViewById(R.id.tvLapLocation);

            postShareText = itemView.findViewById(R.id.postShareText);
            betterViewPager = (BetterViewPager) itemView.findViewById(R.id.betterViewPager);
            userProfileIv = (CircleImageView) itemView.findViewById(R.id.user_profile_iv);
            lapNameTv = (TextView) itemView.findViewById(R.id.lap_name_tv);
            userNameTv = (TextView) itemView.findViewById(R.id.user_name_tv);
            dateTimeTv = (TextView) itemView.findViewById(R.id.date_time_tv);
            postImageIv = (ImageView) itemView.findViewById(R.id.post_image_iv);
            circuitNameTv = (TextView) itemView.findViewById(R.id.circuit_name_tv);
            circuitTimeTv = (TextView) itemView.findViewById(R.id.circuit_time_tv);
            countryFlage=(FlagImageView) itemView.findViewById(R.id.countryFlage);
            carNameTv = (TextView) itemView.findViewById(R.id.car_name_tv);

            carnameLyt = (LinearLayout) itemView.findViewById(R.id.carnameLyt);
            whetherLyt = (LinearLayout) itemView.findViewById(R.id.whetherLyt);
            timerLyt = (LinearLayout) itemView.findViewById(R.id.timerLyt);

          //  rankTv = (TextView) itemView.findViewById(R.id.rank_tv);
            weatherTv = (TextView) itemView.findViewById(R.id.weather_tv);
            timerTv = (TextView) itemView.findViewById(R.id.timer_tv);
            likeLayout = (LinearLayout) itemView.findViewById(R.id.like_layout);
            likeCountTv = (TextView) itemView.findViewById(R.id.like_count_tv);
            commentLayout = (LinearLayout) itemView.findViewById(R.id.comment_layout);
            commentCountTv = (TextView) itemView.findViewById(R.id.comment_count_tv);
            shareLayout = (LinearLayout) itemView.findViewById(R.id.share_layout);
            shareCountTv = (TextView) itemView.findViewById(R.id.share_count_tv);
            likeTextTv = (TextView) itemView.findViewById(R.id.like_text_tv);
            desc_tv = (TextView) itemView.findViewById(R.id.desc_tv);
            betterViewPager = (BetterViewPager) itemView.findViewById(R.id.betterViewPager);
            recycler_view = (RecyclerView) itemView.findViewById(R.id.recycler_view);
            option_layout = (LinearLayout) itemView.findViewById(R.id.option_layout);
            trackInfoLl = (LinearLayout) itemView.findViewById(R.id.track_info_ll);
            imageInfoRl = (RelativeLayout) itemView.findViewById(R.id.image_info_rl);
            adView = itemView.findViewById(R.id.adView);
            mAdvertiesView = itemView.findViewById(R.id.card_addverties);
            mAddCardView = itemView.findViewById(R.id.cardAddView);
            adver_rl = itemView.findViewById(R.id.adver_rl);
            imgPlay = itemView.findViewById(R.id.imgPlay);
            adver_image_iv = itemView.findViewById(R.id.adver_image_iv);
            adver_desc_tv = itemView.findViewById(R.id.adver_desc_tv);
            arraowleft_iv = itemView.findViewById(R.id.arraowleft_iv);
            arraowright_iv = itemView.findViewById(R.id.arraowright_iv);

            adCompanyLogoIv = itemView.findViewById(R.id.adCompanyLogoIv);
            adCompanyNameTv = itemView.findViewById(R.id.adCompanyNameTv);
            tvSponserd = itemView.findViewById(R.id.tvSponserd);

            ad_like_count_tv = itemView.findViewById(R.id.ad_like_count_tv);
            ad_comment_count_tv = itemView.findViewById(R.id.ad_comment_count_tv);
            ad_like_layout = itemView.findViewById(R.id.ad_like_layout);
            ad_like_text_tv = itemView.findViewById(R.id.ad_like_text_tv);
            ad_comment_layout = itemView.findViewById(R.id.ad_comment_layout);
            cmtLyt=(RelativeLayout)itemView.findViewById(R.id.commentLyt);
            cmtUserDesc=(TextView)itemView.findViewById(R.id.cmtDesc);
            cmtUserImg=(CircleImageView)itemView.findViewById(R.id.cmtUserImage);
            cmtUserName=(TextView)itemView.findViewById(R.id.cmtUserName);

            dashboardPostImagesBeansList = new ArrayList<>();
            dashBoardPostInnerAdapter = new DashBoardPostInnerAdapter(mContext, dashboardPostImagesBeansList);
            recycler_view.setLayoutManager(new GridLayoutManager(mContext, 2));
            recycler_view.setAdapter(dashBoardPostInnerAdapter);

            likeLayout.setOnClickListener(this);
            likeCountTv.setOnClickListener(this);
            commentLayout.setOnClickListener(this);
            commentCountTv.setOnClickListener(this);
            shareLayout.setOnClickListener(this);
            shareCountTv.setOnClickListener(this);
            // user_profile_ll.setOnClickListener(this);
            shareLayout.setOnClickListener(this);
            trackInfoLl.setOnClickListener(this);

            option_layout.setOnClickListener(this);
            arraowleft_iv.setOnClickListener(this);
            arraowright_iv.setOnClickListener(this);
            imgPlay.setOnClickListener(this);
            userProfileIv.setOnClickListener(this);
            postImageIv.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.like_layout:
                    if (mOnItemClickListener != null) {
                        String status = "0";
                        int likeCount = Integer.parseInt(mPostListBeen.get(getAdapterPosition()).getLikedPostCount());
                        if (mPostListBeen.get(getAdapterPosition()).getLikedPost().compareTo("0") == 0) {
                            status = "1";
                            likeCount = likeCount + 1;
                        } else {
                            status = "0";
                            likeCount = likeCount - 1;
                        }
                        mPostListBeen.get(getAdapterPosition()).setLikedPost(status);
                        mPostListBeen.get(getAdapterPosition()).setLikedPostCount(likeCount + "");
                        mOnItemClickListener.onClickOnLike(getAdapterPosition(), status);
                        notifyItemChanged(getAdapterPosition());
                    }
                    break;
                case R.id.like_count_tv:
                    Intent intent = new Intent(mContext, LikesUserActivity.class);
                    intent.putExtra(S.postId, mPostListBeen.get(getAdapterPosition()).get_id());
                    intent.putExtra(S.likeType, I.POSTLIKE);
                    mContext.startActivity(intent);
                    break;
                case R.id.comment_layout:
                    if (!(mContext instanceof CommentActivity)) {
                        Intent intent1 = new Intent(mContext, CommentActivity.class);
                        intent1.putExtra(S.postId, mPostListBeen.get(getAdapterPosition()).get_id());
                        intent1.putExtra(S.position, getAdapterPosition() + "");
                        activity.startActivityForResult(intent1, I.COMMENT_ACTIVITY_RESULT_CODE);
                    }
                    break;
                case R.id.comment_count_tv:
                    if (!(mContext instanceof CommentActivity)) {
                        Intent intent2 = new Intent(mContext, CommentActivity.class);
                        intent2.putExtra(S.postId, mPostListBeen.get(getAdapterPosition()).get_id());
                        intent2.putExtra(S.position, getAdapterPosition() + "");
                        activity.startActivityForResult(intent2, I.COMMENT_ACTIVITY_RESULT_CODE);
                    }
                    break;
                case R.id.user_profile_ll:
                    Intent intent3 = new Intent(mContext, ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle.putString(S.user_id, mPostListBeen.get(getAdapterPosition()).getUserDataBeans().get_id());
                    bundle.putString("status", "0");
                    intent3.putExtras(bundle);
                    mContext.startActivity(intent3);
                    break;
                case R.id.share_layout:
                    //mOnItemClickListener.onClickOnshare(getAdapterPosition());
                    sharePostDialog(getAdapterPosition());

                    break;
                case R.id.track_info_ll:

                    Intent intent4 = null;
                    intent4 = new Intent(mContext, TrackDetailActivity.class);

                    Bundle bundle1 = new Bundle();
                    bundle1.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle1.putString(S.user_id, mPostListBeen.get(getAdapterPosition()).getTrackData().getId());
                    bundle1.putString("status", "");
                    intent4.putExtras(bundle1);
                    mContext.startActivity(intent4);

                    break;

                case R.id.share_count_tv:
                    ArrayList<DashboardPostShareUserModal> tagedList = new ArrayList<>();
                    tagedList.addAll(mPostListBeen.get(getAdapterPosition()).getDashboardPostShareUserModals());
                    Intent intent10 = new Intent(mContext, ShareFriendsListActivity.class);
                    Bundle bundle10 = new Bundle();
                    bundle10.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle10.putString("status", "0");
                    intent10.putExtras(bundle10);
                    intent10.putExtra(S.user_list, tagedList);
                    mContext.startActivity(intent10);

                    break;

                case R.id.option_layout:
                    final PopupMenu popup = new PopupMenu(mContext, DashBoardPostsAdapter.MyViewHolder.this.option_layout);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.dashboard_post_option_menu);
                    if (!mPostListBeen.get(getAdapterPosition()).getUserDataBeans().get_id().equalsIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id))) {
                        Menu m = popup.getMenu();
                        m.removeItem(R.id.edit_post);
                        m.removeItem(R.id.delete_post);
                    }
                    else {
                        Menu m = popup.getMenu();
                        m.removeItem(R.id.hide_post);
                        m.removeItem(R.id.report_post);
                    }
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.annoing_text:
                                    reportPost(item.getTitle(), mPostListBeen.get(getAdapterPosition()).get_id(),S.report_one);
                                    break;
                                case R.id.report_post:
                                    reportPost(item.getTitle(), mPostListBeen.get(getAdapterPosition()).get_id(),S.report_two);
                                    break;
                                case R.id.hide_post:
                                    reportPost(item.getTitle(), mPostListBeen.get(getAdapterPosition()).get_id(),S.report_three);
                                    break;
                                case R.id.edit_post:
                                    if(mContext instanceof DashboardActivity ){
                                        DashboardPostListBean dashboardPostListBean = mPostListBeen.get(getAdapterPosition());
                                        Intent intent1 = new Intent(mContext, AddPostActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(S.data, dashboardPostListBean);
                                        intent1.putExtras(bundle);
                                        ((DashboardActivity) mContext).startActivityForResult(intent1, 1);
                                    }
                                    else if(mContext instanceof  CommentActivity){
                                        DashboardPostListBean dashboardPostListBean = mPostListBeen.get(getAdapterPosition());
                                        Intent intent1 = new Intent(mContext, AddPostActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(S.data, dashboardPostListBean);
                                        intent1.putExtras(bundle);
                                        ((CommentActivity) mContext).startActivityForResult(intent1, 1);
                                    }
                                    else if(mContext instanceof  ProfileActivity){
                                        DashboardPostListBean dashboardPostListBean = mPostListBeen.get(getAdapterPosition());
                                        Intent intent1 = new Intent(mContext, AddPostActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(S.data, dashboardPostListBean);
                                        intent1.putExtras(bundle);
                                        ((ProfileActivity) mContext).startActivityForResult(intent1, 1);
                                    }
                                    break;
                                case R.id.delete_post:
                                    Util.showAlertDialogWithTwoButton(mContext, mContext.getString(R.string.are_you_sure_you_want_to_delete), mContext.getString(R.string.cancel), mContext.getString(R.string.remove), getAdapterPosition());

                                    break;
                                case R.id.cancel:
                                    popup.dismiss();
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                    break;

                case R.id.arraowleft_iv:
                    int last_pos = betterViewPager.getCurrentItem();
                    int next_post = last_pos - 1;
                    betterViewPager.setCurrentItem(next_post);
                    break;

                case R.id.arraowright_iv:
                    int lastpos = betterViewPager.getCurrentItem();
                    int nextpost = lastpos + 1;
                    betterViewPager.setCurrentItem(nextpost);
                    break;
                case R.id.imgPlay:

                    try {
                        // int pos = (int) view.getTag();
                        Intent intent5 = new Intent(Intent.ACTION_VIEW, Uri.parse(S.IMAGE_BASE_URL + mPostListBeen.get(getAdapterPosition()).getDashboardPostImagesBeans().get(0).getFileName()));
                        intent5.setDataAndType(Uri.parse(S.IMAGE_BASE_URL + mPostListBeen.get(getAdapterPosition()).getDashboardPostImagesBeans().get(0).getFileName()), "video/*");

                        mContext.startActivity(intent5);
                    } catch (Exception e) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.no_supportable_Player), Toast.LENGTH_LONG).show();
                    }

                    break;
                case R.id.user_profile_iv:
                    Intent intentProfile = new Intent(mContext, ProfileActivity.class);
                    Bundle bundleProfile = new Bundle();
                    bundleProfile.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundleProfile.putString(S.user_id, mPostListBeen.get(getAdapterPosition()).getUserDataBeans().get_id());
                    bundleProfile.putString("status", "0");
                    intentProfile.putExtras(bundleProfile);
                    mContext.startActivity(intentProfile);
                    break;

                case R.id.post_image_iv:

                    ArrayList<String> list = new ArrayList<>();

                    if (mPostListBeen.get(getAdapterPosition()).getTrackData() == null && mPostListBeen.get(getAdapterPosition()).getDashboardPostImagesBeans().size() == 1) {

                        if (!mPostListBeen.get(getAdapterPosition()).getDashboardPostImagesBeans().get(0).getType().equalsIgnoreCase("2")) {
                            list.add(S.IMAGE_BASE_URL + mPostListBeen.get(getAdapterPosition()).getDashboardPostImagesBeans().get(0).getFileName());
                        }

                    } else if (mPostListBeen.get(getAdapterPosition()).getTrackData() != null && mPostListBeen.get(getAdapterPosition()).getDashboardPostImagesBeans().size() == 0) {

                        String trackImg = mPostListBeen.get(getAdapterPosition()).getTrackData().getImage();
                        list.add(S.IMAGE_BASE_URL + trackImg);
                    }

                    new ImageViewer.Builder(mContext, list)
                            .setStartPosition(getAdapterPosition())
                            .allowZooming(true)
                            .show();
                    break;

            }
        }
    }

    private void reportPost(CharSequence title, String post_id,String reportType) {
        authAPI.reportPost(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), post_id, title + "",reportType);
    }
    private void sharePostDialog(int adapterPosition) {
        sharePosition = adapterPosition;
        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(mContext, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        chooseImagePickerOptionDialog.changeText();
        // Util.showAlertDialogWithTwoButton(mContext, mContext.getString(R.string.are_you_sure_you_want_to_share_this_post), mContext.getString(R.string.cancel), mContext.getString(R.string.confirm), adapterPosition);
    }

}


































    /*public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }*/
