package com.os.fastlap.adapter.paddock;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.os.fastlap.R;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/19/2017.
 */

public class PaddockUploadsSessionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ArrayList<String> mArrayList;
    Activity activity;

    public PaddockUploadsSessionAdapter(Context context, ArrayList<String> arrayList, Activity activity) {
        this.mArrayList = arrayList;
        mContext = context;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.mylaps_uploads_session_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            if (position % 2 == 0) {
                ((MyViewHolder) holder).parentLl.setBackgroundColor(ContextCompat.getColor(activity, R.color.light_gray_color));
            } else {
                ((MyViewHolder) holder).parentLl.setBackgroundColor(ContextCompat.getColor(activity, R.color.gray_text_color));
            }

            if (position == 2) {
                ((MyViewHolder) holder).addBtn.setVisibility(View.VISIBLE);
            } else {
                ((MyViewHolder) holder).addBtn.setVisibility(View.GONE);
            }

            ((MyViewHolder) holder).sessionNoTv.setText("Session " + (position + 1));

            ((MyViewHolder) holder).addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddTrackDialog();
                }
            });


        }
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout parentLl;
        private TextViewPlayRegular trackNameTv;
        private TextViewPlayRegular dateTv;
        private TextViewPlayRegular sessionNoTv;
        private TextViewPlayRegular addBtn;

        public MyViewHolder(View itemView) {
            super(itemView);

            parentLl = (LinearLayout) itemView.findViewById(R.id.parent_ll);
            trackNameTv = (TextViewPlayRegular) itemView.findViewById(R.id.track_name_tv);
            dateTv = (TextViewPlayRegular) itemView.findViewById(R.id.date_tv);
            sessionNoTv = (TextViewPlayRegular) itemView.findViewById(R.id.session_no_tv);
            addBtn = (TextViewPlayRegular) itemView.findViewById(R.id.add_btn);

        }
    }


    private void AddTrackDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.mylaps_add_session_dialog);

        EditTextPlayRegular trackNameEt;
        EditTextPlayRegular versionEt;
        EditTextPlayRegular typeEt;
        EditTextPlayRegular countryEt;
        EditTextPlayRegular cityEt;
        TextViewPlayBold sendRequestBtn;

        trackNameEt = (EditTextPlayRegular) dialog.findViewById(R.id.track_name_et);
        versionEt = (EditTextPlayRegular) dialog.findViewById(R.id.version_et);
        typeEt = (EditTextPlayRegular) dialog.findViewById(R.id.type_et);
        countryEt = (EditTextPlayRegular) dialog.findViewById(R.id.country_et);
        cityEt = (EditTextPlayRegular) dialog.findViewById(R.id.city_et);
        sendRequestBtn = (TextViewPlayBold) dialog.findViewById(R.id.send_request_btn);

        sendRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

}
