package com.os.fastlap.adapter.settings;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/*
 * Created by anandj on 8/28/2017.
 */

public class SettingBlockUserSuggestionListAdapter extends RecyclerView.Adapter<SettingBlockUserSuggestionListAdapter.MyViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private Context mContext;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    private ArrayList<ProfileMyFriendsList> mMyFriendsListsAll = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private boolean mSelectMultiple;
    private List<Integer> mSelectUser;
    private List<Integer> checkId = new ArrayList<>();
    private String removeFriendTag = "";
    private List<Integer> mSelectNewUser;


    public SettingBlockUserSuggestionListAdapter(Context context, ArrayList<ProfileMyFriendsList> friendsLists, boolean select_multiple) {
        this.mContext = context;
        this.mMyFriendsLists = friendsLists;
        this.mMyFriendsListsAll.addAll(friendsLists);
        this.mSelectMultiple = select_multiple;
        mSelectUser = new ArrayList<>();
        mSelectNewUser = new ArrayList<>();
    }

    public void addMyFriendsListAll(ArrayList<ProfileMyFriendsList> friendsLists) {
        this.mMyFriendsListsAll.clear();
        this.mMyFriendsListsAll.addAll(friendsLists);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_blockuser_layout, parent, false));
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }

    public void addSelectedTagFriends(List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeans) {
        mSelectUser.addAll(chdckId(dashboardCommentPostTagFriendsBeans));
        notifyDataSetChanged();
    }

    private List<Integer> chdckId(List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeans) {
        checkId = new ArrayList<>();
        for (int i = 0; i < dashboardCommentPostTagFriendsBeans.size(); i++) {
            for (int j = 0; j < mMyFriendsLists.size(); j++) {
                if (dashboardCommentPostTagFriendsBeans.get(i).getFriendIdBean().get_id().equalsIgnoreCase(mMyFriendsLists.get(j).getUserId())) {
                    if (!checkId.contains(j))
                        checkId.add(j);
                }

            }
        }
        return checkId;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (mSelectMultiple) {
            holder.terms_checkbox.setVisibility(View.VISIBLE);
            holder.terms_checkbox.setTag(position);
            holder.terms_checkbox.setChecked(mSelectUser.contains(position));
        } else
            holder.terms_checkbox.setVisibility(View.GONE);

        String userName = mMyFriendsLists.get(position).getPersonalInfo().getFirstName() + " " +
                mMyFriendsLists.get(position).getPersonalInfo().getLastName();
        String imageUrl = mMyFriendsLists.get(position).getPersonalInfo().getImage();
        holder.txt_user_name.setText(userName);
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + imageUrl, holder.user_profile, Util.getImageLoaderOption(mContext));
    }

    @Override
    public int getItemCount() {
        return mMyFriendsLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView user_profile;
        TextView txt_user_name;
        AppCompatCheckBox terms_checkbox;
        LinearLayout ll_main;

        public MyViewHolder(View itemView) {
            super(itemView);
            user_profile = itemView.findViewById(R.id.track_img_iv);
            txt_user_name = itemView.findViewById(R.id.track_name_tv);
            terms_checkbox = itemView.findViewById(R.id.terms_checkbox);

            ll_main = itemView.findViewById(R.id.ll_main);

            ll_main.setOnClickListener(this);
            terms_checkbox.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ll_main:
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getAdapterPosition());
                    break;
                case R.id.terms_checkbox:
                    int id = (int) view.getTag();
                    if (mSelectUser.contains(id)) {
                        if (mSelectNewUser.contains(id))
                            mSelectNewUser.remove(Integer.valueOf(id));
                        mSelectUser.remove(Integer.valueOf(id));
                        if (checkId.contains(id)) {
                            if (removeFriendTag.isEmpty())
                                removeFriendTag = mMyFriendsLists.get(id).getUserId();
                            else
                                removeFriendTag = removeFriendTag.concat("," + mMyFriendsLists.get(id).getUserId());
                            checkId.remove(Integer.valueOf(id));
                        }
                    } else {
                        mSelectNewUser.add(id);
                        mSelectUser.add(id);
                    }
                    notifyDataSetChanged();
                    break;
            }
        }
    }

    // return selected friends ids
    public JSONArray getSelectedFriendsList() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < mSelectUser.size(); i++) {
            jsonArray.put(mMyFriendsLists.get(mSelectUser.get(i)).getUserId());
        }
        return jsonArray;
    }

    // return remove tag friends
    public String getRemoveFriendTag() {
        return removeFriendTag;
    }

    // return selected friends ids only new select
    public JSONArray getSelectedFriendsListOnlyNew() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < mSelectNewUser.size(); i++) {
            jsonArray.put(mMyFriendsLists.get(mSelectNewUser.get(i)).getUserId());
        }
        return jsonArray;
    }

    // return selected friends name
    public String getSelectedFriendsNames() {
        String name = "";

        if (mSelectUser.size() == 1)
            name = "<font color=#414141> with </font><font color=#bd2436>" + mMyFriendsLists.get(mSelectUser.get(0)).getPersonalInfo().getFirstName() + " " + mMyFriendsLists.get(mSelectUser.get(0)).getPersonalInfo().getLastName() + "</font>";
        else if (mSelectUser.size() == 2)
            name = "<font color=#414141> with </font><font color=#bd2436>" + mMyFriendsLists.get(mSelectUser.get(0)).getPersonalInfo().getFirstName() + " " + mMyFriendsLists.get(mSelectUser.get(0)).getPersonalInfo().getLastName() + "</font><font " +
                    "color=#414141> and </font><font color=#bd2436>" + mMyFriendsLists.get(mSelectUser.get(1)).getPersonalInfo().getFirstName() + " " + mMyFriendsLists.get(mSelectUser.get(1)).getPersonalInfo().getLastName() + "</font";
        else
            name = "<font color=#414141> with </font><font color=#bd2436>" + mMyFriendsLists.get(mSelectUser.get(0)).getPersonalInfo().getFirstName() + " " + mMyFriendsLists.get(mSelectUser.get(0)).getPersonalInfo().getLastName() + "</font><font color=#414141> and </font><font color=#bd2436>" +
                    String.valueOf(mSelectUser.size() - 1) + " others </font>";

        /*for (int i = 0; i < mSelectUser.size(); i++) {
            if (i == 0)
                name = mMyFriendsLists.get(mSelectUser.get(i)).getPersonalInfo().getFirstName() + " " + mMyFriendsLists.get(mSelectUser.get(i)).getPersonalInfo().getLastName();
            else
                name += " , "+mMyFriendsLists.get(mSelectUser.get(i)).getPersonalInfo().getFirstName() + " " + mMyFriendsLists.get(mSelectUser.get(i)).getPersonalInfo().getLastName();
        }*/
        return name;
    }

    // search class
    public void filter(String ch)
    {
        try {
            mMyFriendsLists.clear();
            if (ch.isEmpty())
                mMyFriendsLists.addAll(mMyFriendsListsAll);
            else {
                for (ProfileMyFriendsList wp : mMyFriendsListsAll) {
                    if (wp.getPersonalInfo().getFirstName().toString().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())) ||
                            wp.getPersonalInfo().getLastName().toString().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault()))
                            /*wp.getEmail().toString().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault()))*/)
                        mMyFriendsLists.add(wp);
                }
            }
            notifyDataSetChanged();
        }
        catch (Exception e){
            Log.e("tag","Exception :"+e);
        }


    }
}