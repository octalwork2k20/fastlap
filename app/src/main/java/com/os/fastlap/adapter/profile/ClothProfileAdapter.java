package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.clothmodals.ClothModelListModal;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/17/2017.
 */

public class ClothProfileAdapter extends RecyclerView.Adapter<ClothProfileAdapter.MyViewHolder> {

    public interface OnItemClickListener {
        void onEditClothList(int position);

        void onDeleteClothList(int position);
    }

    private Context mContext;
    private ArrayList<ClothModelListModal> mClothList;

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public ClothProfileAdapter(Context context, ArrayList<ClothModelListModal> mClothList) {
        mContext = context;
        this.mClothList = mClothList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_clothing_child, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.race_name_tv.setText(mClothList.get(position).getClothName());
        holder.brandNameTv.setText(mClothList.get(position).getClothNameIdModal().getName());
        holder.modelNameTv.setText(mClothList.get(position).getClothModelIdModal().getName());
        holder.yearTv.setText(mClothList.get(position).getClothYearIdModal().getYears());
        holder.colorTv.setText(mClothList.get(position).getClothColorIdModal().getName());
        try {
            holder.vehicle_name_tv.setText(mClothList.get(position).getVehicleTypeIdModal().getName());
        }
        catch (Exception e){
            holder.vehicle_name_tv.setText("");
        }


        try {
            if(!mClothList.get(position).getClothImage().equals(null)&&!mClothList.get(position).getClothImage().equalsIgnoreCase(""))
                Util.setImageLoader(S.IMAGE_BASE_URL + mClothList.get(position).getClothImage(), ((MyViewHolder) holder).imgCloth, mContext);
            else
                ((MyViewHolder) holder).imgCloth.setImageResource(R.drawable.nopic);
        }
        catch (Exception e){e.printStackTrace();
        }


        // holder

    }

    @Override
    public int getItemCount() {
        return mClothList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView imgCloth;
        private TextViewPlayBold raceNameTv;
        private TextViewPlayRegular brandNameTv;
        private TextViewPlayRegular vehicle_name_tv;

        private TextViewPlayRegular modelNameTv;
        private TextViewPlayRegular yearTv;
        private TextViewPlayRegular colorTv;

        private TextView race_name_tv;
        private ImageView img_edit, img_delete;

        MyViewHolder(View itemvitem) {
            super(itemvitem);

            imgCloth = (ImageView) itemvitem.findViewById(R.id.img_cloth);
            brandNameTv = (TextViewPlayRegular) itemvitem.findViewById(R.id.brand_name_tv);
            modelNameTv = (TextViewPlayRegular) itemvitem.findViewById(R.id.model_name_tv);
            vehicle_name_tv = (TextViewPlayRegular) itemvitem.findViewById(R.id.vehicle_name_tv);

            yearTv = (TextViewPlayRegular) itemvitem.findViewById(R.id.year_tv);
            colorTv = (TextViewPlayRegular) itemvitem.findViewById(R.id.color_tv);
            race_name_tv = (TextView) itemvitem.findViewById(R.id.race_name_tv);
            img_edit = (ImageView)itemvitem.findViewById(R.id.img_edit);
            img_delete = (ImageView)itemvitem.findViewById(R.id.img_delete);

            img_delete.setOnClickListener(this);
            img_edit.setOnClickListener(this);

            img_edit.setVisibility(View.GONE);

            if(MySharedPreferences.getPreferences(mContext,S.user_id).compareTo(ProfileActivity.profileUserId)==0) {
                img_delete.setVisibility(View.VISIBLE);
            }
            else
            {
                img_delete.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_delete:
                    clickOnDelete(this);
                    break;
                case R.id.img_edit:
                    clickOnEdit(this);
                    break;
            }
        }
    }

    private void clickOnEdit(MyViewHolder myViewHolder) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onEditClothList(myViewHolder.getAdapterPosition());
    }

    private void clickOnDelete(MyViewHolder myViewHolder) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onDeleteClothList(myViewHolder.getAdapterPosition());
    }
}
