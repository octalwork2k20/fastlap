package com.os.fastlap.adapter.paddock;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanSessionData;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/19/2017.
 */

public class PaddockUploadsLapsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ArrayList<BeanSessionData.Laps> lapsListing;
    Activity activity;
    private boolean onBind;


    public PaddockUploadsLapsAdapter(Context context, ArrayList<BeanSessionData.Laps> lapsListing, Activity activity) {
        this.lapsListing = lapsListing;
        mContext = context;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.mylaps_uploads_laps_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            onBind = true;
            BeanSessionData.Laps laps = new BeanSessionData().new Laps();
            laps = lapsListing.get(position);

            ((MyViewHolder) holder).snTv.setText(laps.getLapsIndex());
            ((MyViewHolder) holder).lapTimeTv.setText(laps.getTime());

            if (position % 2 == 0) {
                ((MyViewHolder) holder).parentLl.setBackgroundColor(ContextCompat.getColor(activity, R.color.very_light_gray_color));
            } else {
                ((MyViewHolder) holder).parentLl.setBackgroundColor(ContextCompat.getColor(activity, R.color.very_gray_color));
            }

            if (laps.getStatus().equalsIgnoreCase("1")) {
                ((MyViewHolder) holder).lapsCb.setChecked(true);
            } else {
                ((MyViewHolder) holder).lapsCb.setChecked(false);
            }

            onBind = false;
        }
    }

    @Override
    public int getItemCount() {
        return lapsListing.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements CheckBox.OnCheckedChangeListener {
        private LinearLayout parentLl;
        private TextViewPlayRegular snTv;
        private TextViewPlayRegular lapTimeTv;
        private AppCompatCheckBox lapsCb;
        private ImageView lockIv;

        public MyViewHolder(View itemView) {
            super(itemView);

            parentLl = (LinearLayout) itemView.findViewById(R.id.parent_ll);
            snTv = (TextViewPlayRegular) itemView.findViewById(R.id.sn_tv);
            lapTimeTv = (TextViewPlayRegular) itemView.findViewById(R.id.lap_time_tv);
            lapsCb = (AppCompatCheckBox) itemView.findViewById(R.id.laps_cb);
            lockIv = (ImageView) itemView.findViewById(R.id.lock_iv);

            if (lapsCb != null) {
                lapsCb.setOnCheckedChangeListener(this);
            }

        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                lapsListing.get(getAdapterPosition()).setStatus("1");
            } else {
                lapsListing.get(getAdapterPosition()).setStatus("0");
            }

            if (!onBind) {
                notifyDataSetChanged();
            }
        }
    }
}
