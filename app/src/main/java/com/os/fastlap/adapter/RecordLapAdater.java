package com.os.fastlap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.beans.TrackRecord;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;


public class RecordLapAdater extends RecyclerView.Adapter<RecordLapAdater.ViewHolder> {
    private ArrayList<TrackRecord>arrayList=new ArrayList<>();
    private Context context;

    public RecordLapAdater(Context context,ArrayList<TrackRecord> arrayList) {
        this.arrayList = arrayList;
        this.context=context;
    }
    @NonNull
    @Override
    public RecordLapAdater.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_lap_record_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecordLapAdater.ViewHolder holder, int position) {
        TrackRecord trackRecord=arrayList.get(position);
        holder.tvName.setText(trackRecord.name);
        holder.tvCarValue.setText(trackRecord.vehicle);
        holder.tvDateValue.setText(trackRecord.date);
        holder.tvPilotValue.setText(trackRecord.pilot);
        holder.tvTimeValue.setText(trackRecord.time_lap);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextViewPlayBold tvName;
        TextViewPlayRegular tvPilotValue;
        TextViewPlayRegular tvCarValue;
        TextViewPlayRegular tvDateValue;
        TextViewPlayRegular tvTimeValue;
        public ViewHolder(View itemView) {
            super(itemView);
            tvName=(TextViewPlayBold)itemView.findViewById(R.id.tvName);
            tvPilotValue=(TextViewPlayRegular)itemView.findViewById(R.id.tvPilotValue);
            tvCarValue=(TextViewPlayRegular)itemView.findViewById(R.id.tvCarValue);
            tvDateValue=(TextViewPlayRegular)itemView.findViewById(R.id.tvDateValue);
            tvTimeValue=(TextViewPlayRegular)itemView.findViewById(R.id.tvTimeValue);
        }

    }
}
