package com.os.fastlap.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.Bean_Contact;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by riyazudinp on 6/17/2017.
 */

public class ContactUserAdapter extends RecyclerView.Adapter<ContactUserAdapter.MyViewHolder> {

    private Context context;
    private List<Bean_Contact> mArrayList;
    private List<Bean_Contact> arraylist;

    public ContactUserAdapter(Context context, List<Bean_Contact> mArrayList) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(mArrayList);
    }

    @Override
    public ContactUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_user_row_layout, parent, false);
        return new ContactUserAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactUserAdapter.MyViewHolder holder, int position)
    {
        Bean_Contact bean_contact = mArrayList.get(position);

        if (mArrayList.get(position).getPersonName() != null) {
            holder.txtUserName.setText(mArrayList.get(position).getPersonName());
        } else {
            holder.txtUserName.setText("");
        }


        if (mArrayList.get(position).getPersonImage() != null && mArrayList.get(position).getPersonImage().compareTo("") != 0) {
            try {
                Bitmap bitmap = MediaStore.Images.Media
                        .getBitmap(context.getContentResolver(),
                                Uri.parse(mArrayList.get(position).getPersonImage()));
                holder.imgProfile.setImageBitmap(bitmap);

            } catch (IOException e) {
                holder.imgProfile.setImageResource(R.mipmap.profile_default);

                e.printStackTrace();
            }
        } else {
            holder.imgProfile.setImageResource(R.mipmap.profile_default);
        }


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imgProfile;
        private TextView txtUserName;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
            imgProfile = (CircleImageView) itemView.findViewById(R.id.imgProfile);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mArrayList.clear();
        if (charText.length() == 0) {
            mArrayList.addAll(arraylist);
        } else {
            for (Bean_Contact wp : arraylist) {
                if (wp.getPersonName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mArrayList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
