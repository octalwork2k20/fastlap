package com.os.fastlap.adapter.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanVehicleType;

import java.util.ArrayList;

/**
 * Created by anandj on 8/22/2017.
 */

public class VehicleSettingAdapter extends RecyclerView.Adapter<VehicleSettingAdapter.MyViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private Context mContext;
    private ArrayList<BeanVehicleType> vehicleTypeArrayList;
    private OnItemClickListener onItemClickListener;

    public VehicleSettingAdapter(Context context, ArrayList<BeanVehicleType> vehicleTypeArrayList) {
        mContext = context;
        this.vehicleTypeArrayList = vehicleTypeArrayList;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_setting_item_row, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        holder.vehicleTypeTv.setText(vehicleTypeArrayList.get(position).getName());
        if (vehicleTypeArrayList.get(position).isSelected())
        {
            holder.vehicleTypeTv.setChecked(true);
        } else {
            holder.vehicleTypeTv.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return vehicleTypeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CheckBox vehicleTypeTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            vehicleTypeTv = (CheckBox) itemView.findViewById(R.id.vehicleTypeTv);
            vehicleTypeTv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.vehicleTypeTv:
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getAdapterPosition());
                    break;
            }
        }
    }
}
