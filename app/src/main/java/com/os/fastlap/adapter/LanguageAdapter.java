package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.LanguageBeans;

import java.util.ArrayList;

/**
 * Created by anandj on 8/22/2017.
 */

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private Context mContext;
    private ArrayList<LanguageBeans> mLanguageList;
    private OnItemClickListener onItemClickListener;

    public LanguageAdapter(Context context, ArrayList<LanguageBeans> languageBeansArrayList) {
        mContext = context;
        mLanguageList = languageBeansArrayList;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.select_language_child, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LanguageBeans languageBeans=mLanguageList.get(position);
        holder.language_tv.setText(languageBeans.getName());
        if(languageBeans.isSelected())
        holder.language_tv.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public int getItemCount() {
        return mLanguageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView language_tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            language_tv = (TextView) itemView.findViewById(R.id.language_tv);
            language_tv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.language_tv:
                    if(onItemClickListener!=null)
                        onItemClickListener.onItemClick(getAdapterPosition());
                    break;
            }
        }
    }
}
