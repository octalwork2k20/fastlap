package com.os.fastlap.adapter.community;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.group.GroupProfileActivity;
import com.os.fastlap.beans.BeanGroup;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by anandj on 7/19/2017.
 */

public class CommunityOfficialsAdapter extends RecyclerView.Adapter<CommunityOfficialsAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<BeanGroup> mArrayList = new ArrayList<>();
    ArrayList<BeanGroup> mArrayListClone = new ArrayList<>();
    ClickListner clickListner;

    public interface ClickListner {
        void joinedGroup(int position);
    }

    public void setonclickListner(CommunityOfficialsAdapter.ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public CommunityOfficialsAdapter(Context context, ArrayList<BeanGroup> arrayList) {
        this.mArrayList = arrayList;
        mContext = context;
        mArrayListClone.addAll(arrayList);
    }

    public void addCloneList(ArrayList<BeanGroup> arrayList)
    {
        mArrayListClone.clear();
        mArrayListClone.addAll(arrayList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.community_officials_child, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.groupNameEt.setText(mArrayList.get(position).getName());
        holder.txtDescName.setText(mArrayList.get(position).getDescription());
        holder.txtLikeTotal.setText(mArrayList.get(position).getJoinedCount());

        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mArrayList.get(position).getImage(), holder.postImage, Util.getImageLoaderOption(mContext));

        if (mArrayList.get(position).getAdduserStatus().compareTo("0") == 0) {
            holder.joinedText.setVisibility(View.VISIBLE);
        } else {
            holder.joinedText.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView postImage;
        private TextViewPlayRegular txtLikeTotal;
        private TextViewPlayBold groupNameEt;
        private TextViewPlayRegular txtDescName;
        private TextViewPlayBold sponsoredText;
        private TextView joinedText;
        private LinearLayout visitGroupLl;

        public MyViewHolder(View itemView) {
            super(itemView);

            postImage = (ImageView) itemView.findViewById(R.id.post_image);
            txtLikeTotal = (TextViewPlayRegular) itemView.findViewById(R.id.txt_like_total);
            groupNameEt = (TextViewPlayBold) itemView.findViewById(R.id.group_name_et);
            txtDescName = (TextViewPlayRegular) itemView.findViewById(R.id.txt_desc_name);
            sponsoredText = (TextViewPlayBold) itemView.findViewById(R.id.sponsored_text);
            joinedText = (TextView) itemView.findViewById(R.id.joined_text);
            visitGroupLl = (LinearLayout) itemView.findViewById(R.id.visit_group_ll);

            sponsoredText.setOnClickListener(this);
            joinedText.setOnClickListener(this);
            visitGroupLl.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.visit_group_ll:
                    Intent intent = new Intent(mContext, GroupProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(S.groupId, mArrayList.get(getAdapterPosition()).getId());
                    bundle.putString("status", mArrayList.get(getAdapterPosition()).getAdduserStatus());
                    bundle.putString(S.ownerId, mArrayList.get(getAdapterPosition()).getGroupOwnerId());
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                    break;
                case R.id.joined_text:
                    clickListner.joinedGroup(getAdapterPosition());
                    break;
            }

        }
    }

    // search class
    public void filter(String ch) {
        mArrayList.clear();
        if (ch.isEmpty())
            mArrayList.addAll(mArrayListClone);
        else {
            for (BeanGroup wp : mArrayListClone) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())))
                    mArrayList.add(wp);
            }
        }
        notifyDataSetChanged();
    }

    // filter based on Vehicle type
    public void filterById(String id) {
        mArrayList.clear();
        if (id.isEmpty())
            mArrayList.addAll(mArrayListClone);
        for (BeanGroup wp : mArrayListClone) {
            if (wp.getGroupTypeId().equalsIgnoreCase(id.toLowerCase()))
                mArrayList.add(wp);
        }
        notifyDataSetChanged();
    }
}
