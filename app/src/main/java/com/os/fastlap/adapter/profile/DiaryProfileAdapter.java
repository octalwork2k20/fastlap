package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;

import java.util.ArrayList;

/**
 * Created by anandj on 7/17/2017.
 */

public class DiaryProfileAdapter extends RecyclerView.Adapter<DiaryProfileAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<String> mdiaryArrrayList;

    public DiaryProfileAdapter(Context context, ArrayList<String> arrayList) {
        mContext = context;
        mdiaryArrrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_items, parent, false));

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mdiaryArrrayList.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        return 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }
}
