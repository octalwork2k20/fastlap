package com.os.fastlap.adapter.paddock;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.activity.profile.LapsProfileActivity;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.fragment.paddock.PaddockMyLapsSubFragment;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/14/2017.
 */

public class PaddockMyLapAdapter extends BaseExpandableListAdapter {
    Context mContext;
    List<PaddockMyLapsParentModel> paddockMyLapsParentModels;
    OnClick onClick;
    ArrayList<Integer> selectIds;
    PaddockMyLapsSubFragment paddockMyLapsSubFragment;

    public interface OnClick {
        void onRaceClick(int groupPosition, ArrayList<Integer> selectIds);
        void onFilterDataClick(String groupPosition, String childPosition,int index);
        void onColorPickerDialog(int groupPosition, int childPosition, int color);
    }

    public void setOnClickListner(OnClick onClick) {
        this.onClick = onClick;
    }

    public PaddockMyLapAdapter(Context context, List<PaddockMyLapsParentModel> paddockMyLapsParentModels) {
        this.mContext = context;
        this.paddockMyLapsParentModels = paddockMyLapsParentModels;
        selectIds = new ArrayList<>();
        selectIds.clear();
    }
    public PaddockMyLapAdapter(Context context, List<PaddockMyLapsParentModel> paddockMyLapsParentModels,PaddockMyLapsSubFragment paddockMyLapsSubFragment) {
        this.mContext = context;
        this.paddockMyLapsParentModels = paddockMyLapsParentModels;
        selectIds = new ArrayList<>();
        this.paddockMyLapsSubFragment=paddockMyLapsSubFragment;
        selectIds.clear();
    }


    @Override
    public int getGroupCount() {
        return paddockMyLapsParentModels.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return paddockMyLapsParentModels.get(groupPosition).getMyLapsChildList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return paddockMyLapsParentModels.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return paddockMyLapsParentModels.get(groupPosition).getMyLapsChildList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        PaddockMyLapsParentModel paddockMyLapsParentModel = (PaddockMyLapsParentModel) getGroup(groupPosition);
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.mylaps_mylaps_fragment_child, null);
        RelativeLayout rrListParent;
        ImageView imgCar;
        TextViewPlayBold lapNameTv;
        TextViewPlayBold lapLocationTv;
        TextViewPlayRegular lapUploadedTv;
        TextViewPlayRegular lapBestPostionTv;
        TextViewPlayBold txtViewTrack;
        ImageView imgFilter;

        rrListParent = (RelativeLayout) convertView.findViewById(R.id.rr_list_parent);
        imgCar = (ImageView) convertView.findViewById(R.id.img_car);
        lapNameTv = (TextViewPlayBold) convertView.findViewById(R.id.lap_name_tv);
        lapLocationTv = (TextViewPlayBold) convertView.findViewById(R.id.lap_location_tv);
        lapUploadedTv = (TextViewPlayRegular) convertView.findViewById(R.id.lap_uploaded_tv);
        lapBestPostionTv = (TextViewPlayRegular) convertView.findViewById(R.id.lap_best_postion_tv);
        txtViewTrack = (TextViewPlayBold) convertView.findViewById(R.id.txt_view_track);
        imgFilter = (ImageView) convertView.findViewById(R.id.img_filter);


        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + paddockMyLapsParentModel.getTrackImage(), imgCar, Util.getImageLoaderOption(mContext));

        lapNameTv.setText(paddockMyLapsParentModel.getTrackName() + " " + paddockMyLapsParentModel.getTrackVersionName());
        lapLocationTv.setText(paddockMyLapsParentModel.getTracklocation());

        lapUploadedTv.setText(paddockMyLapsParentModel.getMyLapCount());
        lapBestPostionTv.setText(paddockMyLapsParentModel.getBestPosition());

        imgFilter.setVisibility(View.GONE);

        if(paddockMyLapsSubFragment!=null){
            txtViewTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isExpanded) {
                        ((ExpandableListView) parent).collapseGroup(groupPosition);
                          txtViewTrack.setText(mContext.getResources().getString(R.string.hide_laps));
                        if(paddockMyLapsSubFragment!=null)
                            onClick.onFilterDataClick(paddockMyLapsParentModel.getUserLapId(),"false",groupPosition);
                    }
                    else {
                        ((ExpandableListView) parent).expandGroup(groupPosition, true);
                        txtViewTrack.setText(mContext.getResources().getString(R.string.view_laps));
                        if(paddockMyLapsSubFragment!=null)
                            onClick.onFilterDataClick(paddockMyLapsParentModel.getUserLapId(),"true",groupPosition);
                    }
                }
            });
        }
        else {
            if (isExpanded) {
                txtViewTrack.setText(mContext.getResources().getString(R.string.hide_laps));
            } else {
                txtViewTrack.setText(mContext.getResources().getString(R.string.view_laps));
            }
        }
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.mylaps_mylaps_fragment_child2, null);
        }
        LinearLayout rrListParent;
        CircleImageView userImageIv;
        LinearLayout userInfoLl;
        TextViewPlayBold userNameTv;
        TextViewPlayRegular lapDateTv;
        SwitchCompat graphSwitch;
        TextViewPlayRegular lapTotalTimeTv;
        TextViewPlayRegular lapTimeGapTv;
        TextViewPlayRegular lapCarModelTv;
        ImageView videoIconIv;
        ImageView weatherIv;
        final TextViewPlayBold raceTv;
        TextView colorTv;
        TextViewPlayBold mLapType;
        LinearLayout mLapTypeView;

        mLapTypeView=(LinearLayout)convertView.findViewById(R.id.lytLapType);
        mLapType=(TextViewPlayBold)convertView.findViewById(R.id.tvLapType);
        rrListParent = (LinearLayout) convertView.findViewById(R.id.rr_list_parent);
        userImageIv = (CircleImageView) convertView.findViewById(R.id.user_image_iv);
        userInfoLl = (LinearLayout) convertView.findViewById(R.id.user_info_ll);
        userNameTv = (TextViewPlayBold) convertView.findViewById(R.id.user_name_tv);
        lapDateTv = (TextViewPlayRegular) convertView.findViewById(R.id.lap_date_tv);
        graphSwitch = (SwitchCompat) convertView.findViewById(R.id.graph_switch);
        lapTotalTimeTv = (TextViewPlayRegular) convertView.findViewById(R.id.lap_total_time_tv);
        lapTimeGapTv = (TextViewPlayRegular) convertView.findViewById(R.id.lap_time_gap_tv);
        lapCarModelTv = (TextViewPlayRegular) convertView.findViewById(R.id.lap_car_model_tv);
        videoIconIv = (ImageView) convertView.findViewById(R.id.video_icon_iv);
        weatherIv = (ImageView) convertView.findViewById(R.id.weather_iv);
        raceTv = (TextViewPlayBold) convertView.findViewById(R.id.race_tv);
        colorTv = (TextView) convertView.findViewById(R.id.colorTv);

        BeanTimeLap beanTimeLap = new BeanTimeLap();

        beanTimeLap = (BeanTimeLap) getChild(groupPosition, childPosition);
        userNameTv.setText(beanTimeLap.getUserName());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanTimeLap.getUserImage(), userImageIv, Util.getImageLoaderOption(mContext));
        lapDateTv.setText(Util.ConvertDateTimeZoneDate(beanTimeLap.getLapDate()));
        lapTotalTimeTv.setText(beanTimeLap.getTime());
        lapCarModelTv.setText(beanTimeLap.getVehicleBrandName() + " " + beanTimeLap.getVehicleModelName());
        graphSwitch.setTag(childPosition);


        if (selectIds.contains(childPosition)) {
            graphSwitch.setChecked(true);
            GradientDrawable background = (GradientDrawable) colorTv.getBackground();
            background.setColor(beanTimeLap.getLapColor());
            // colorTv.setBackgroundColor(beanTimeLap.getLapColor());
            colorTv.setVisibility(View.VISIBLE);
        } else {
            graphSwitch.setChecked(false);
            GradientDrawable background = (GradientDrawable) colorTv.getBackground();
            background.setColor(beanTimeLap.getLapColor());
            colorTv.setVisibility(View.GONE);
        }
        Log.e("tag1","Lap Type"+beanTimeLap.getLap_type());
        mLapTypeView.setVisibility(View.GONE);

        if(paddockMyLapsSubFragment!=null){
            if(beanTimeLap.getLap_type().equalsIgnoreCase("BEST_LAPS")){
                mLapTypeView.setVisibility(View.VISIBLE);
                mLapType.setText("Best Laps");
            }
            if(beanTimeLap.getLap_type().equalsIgnoreCase("FRIENDS_LAPS")){
                mLapTypeView.setVisibility(View.VISIBLE);
                mLapType.setText("Friends");
            }
        }

        Drawable img;
        if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.dry)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.sun);
        else if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.cloud)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.cloud_unselect);
        else if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.ice)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.ice_unselect);
        else if (beanTimeLap.getWeather().equalsIgnoreCase(mContext.getString(R.string.wind)))
            img = ContextCompat.getDrawable(mContext, R.mipmap.ice_unselect);
        else
            img = ContextCompat.getDrawable(mContext, R.mipmap.rain_unselect);

        weatherIv.setImageDrawable(img);
        weatherIv.setVisibility(View.GONE);


        raceTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectIds.size() > 0) {
                    onClick.onRaceClick(groupPosition, selectIds);
                } else {
                    Util.showToast(mContext, mContext.getString(R.string.please_select_at_least_two_laps_for_compare));
                }
            }
        });
        if ((getChildrenCount(groupPosition) - 1) == childPosition) {
            if (mContext instanceof MyLapsActivity) {
                raceTv.setVisibility(View.VISIBLE);
            } else {
                raceTv.setVisibility(View.GONE);
            }

        } else {
            raceTv.setVisibility(View.GONE);
        }

        if (mContext instanceof LapsProfileActivity) {
            graphSwitch.setVisibility(View.GONE);
        } else {
            graphSwitch.setVisibility(View.VISIBLE);
        }

        graphSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int id = (int) buttonView.getTag();
                if (isChecked) {
                    if (!selectIds.contains(id))
                        selectIds.add(id);
                } else {
                    if (selectIds.contains(id)) {
                        selectIds.remove(Integer.valueOf(id));
                    }
                }

                notifyDataSetChanged();
            }
        });
        final BeanTimeLap finalBeanTimeLap = beanTimeLap;
        videoIconIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watchYoutubeVideo(mContext, finalBeanTimeLap.getYoutubeURL());
            }
        });

        if (TextUtils.isEmpty(beanTimeLap.getYoutubeURL())) {
            videoIconIv.setVisibility(View.GONE);
        } else {
            videoIconIv.setVisibility(View.VISIBLE);
        }

        final BeanTimeLap finalBeanTimeLap1 = beanTimeLap;
        colorTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onColorPickerDialog(groupPosition, childPosition, finalBeanTimeLap1.getLapColor());
            }
        });
        Log.e("taggggg","1"+beanTimeLap.getTime());
        try {
            if (childPosition == 0)
            {
                Long aLong = 0L;
                Log.e("tag","1"+Util.MilisecondsToTime(aLong));
                if(Util.MilisecondsToTime(aLong).equalsIgnoreCase("0:00:00"))
                    lapTimeGapTv.setText("Best Lap");
                else
                lapTimeGapTv.setText(Util.MilisecondsToTime(aLong));


            } else {
                Long aLong = Util.TimeToMiliseconds(beanTimeLap.getTime()) - Util.TimeToMiliseconds(((BeanTimeLap) getChild(groupPosition, 0)).getTime());
                Log.e("tag","2"+Util.MilisecondsToTime(aLong));

                if(Util.MilisecondsToTime(aLong).equalsIgnoreCase("0:00:00"))
                    lapTimeGapTv.setText("Best Lap");
                else
                    lapTimeGapTv.setText(Util.MilisecondsToTime(aLong));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;

    }

    public static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
