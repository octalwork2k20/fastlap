package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.DashboardTrackListingAll;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by abhinava on 7/12/2017.
 */

public class DashboardTrackListAdapter extends RecyclerView.Adapter<DashboardTrackListAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<DashboardTrackListingAll> mTrackList;
    AuthAPI authAPI;
    private OnItemClickListener mOnItemClickListener;

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onItemVehicleList(int position);
    }

    public DashboardTrackListAdapter(Context context, ArrayList<DashboardTrackListingAll> track_list) {
        this.mTrackList = track_list;
        this.mContext = context;
        authAPI = new AuthAPI(mContext);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_track_list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            Util.setImageLoader(S.IMAGE_BASE_URL + mTrackList.get(position).getTrackData().getImage(), holder.track_img_iv, mContext);
        }
        catch (Exception e){e.printStackTrace();
        }
        holder.track_name_tv.setText(mTrackList.get(position).getTrackData().getName());
        holder.track_location.setText(mTrackList.get(position).getTrackData().getLocation());
        holder.track_length_tv.setText(mTrackList.get(position).getTrackData().getTrackLength() + "Km");
        holder.track_url_tv.setText(mTrackList.get(position).getTrackData().getContactDetails().getUrl());
        if (mTrackList.get(position).getUserTrackData() != null) {
            if (mTrackList.get(position).getUserTrackData().getStatus() == 1) {
                holder.fav_unfav_icon.setImageResource(R.mipmap.starminus);
                holder.fav_unfav_icon.setTag(1);
            } else {
                holder.fav_unfav_icon.setImageResource(R.mipmap.starplus);
                holder.fav_unfav_icon.setTag(0);
            }
        } else {
            holder.fav_unfav_icon.setImageResource(R.mipmap.starplus);
            holder.fav_unfav_icon.setTag(0);
        }

        holder.fav_unfav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user_track_id = "";
                if (mTrackList.get(position).getUserTrackData() != null) {
                    user_track_id = mTrackList.get(position).getUserTrackData().getId();
                } else {
                    user_track_id = "";
                }

                if (Integer.parseInt(holder.fav_unfav_icon.getTag().toString()) == 1) {
                    authAPI.changeFavTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mTrackList.get(position).getTrackData().getId(), user_track_id, "0");
                } else {
                    authAPI.changeFavTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mTrackList.get(position).getTrackData().getId(), user_track_id, "1");
                }
            }
        });

        holder.parent_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickonItem(holder);
            }
        });
    }

    private void clickonItem(DashboardTrackListAdapter.MyViewHolder holderList) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onItemVehicleList(holderList.getAdapterPosition());
    }

    @Override
    public int getItemCount() {
        return mTrackList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView track_img_iv, fav_unfav_icon;
        TextView track_name_tv, track_location, track_length_tv, track_url_tv;
        LinearLayout parent_ll;

        public MyViewHolder(View itemView) {
            super(itemView);
            track_img_iv = itemView.findViewById(R.id.track_img_iv);
            track_name_tv = itemView.findViewById(R.id.track_name_tv);
            track_location = itemView.findViewById(R.id.track_location);
            track_length_tv = itemView.findViewById(R.id.track_length_tv);
            track_url_tv = itemView.findViewById(R.id.track_url_tv);
            fav_unfav_icon = itemView.findViewById(R.id.fav_unfav_icon);
            parent_ll = itemView.findViewById(R.id.parent_ll);


        }
    }


}
