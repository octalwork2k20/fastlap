package com.os.fastlap.adapter.uploadlaptime;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.os.fastlap.R;
import com.os.fastlap.activity.UploadLapTimeActivity;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anandj on 8/22/2017.
 */

public class UploadLapTimeStep2Adapter extends RecyclerView.Adapter<UploadLapTimeStep2Adapter.MyViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onWeatherClick(int position, String weather);

        void onTyreBrandClick(int position);

        void onTyreModelClick(int position);
    }

    private Context mContext;
    private JSONArray step2DataList;
    private OnItemClickListener onItemClickListener;

    public UploadLapTimeStep2Adapter(Context context, JSONArray step2DataList) {
        mContext = context;
        this.step2DataList = step2DataList;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_lap_time_step2_child, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            JSONObject beanStep2Data = new JSONObject();
            beanStep2Data = step2DataList.getJSONObject(position);

          //  holder.sessionNoTv.setText(beanStep2Data.getString("sessionText"));

            holder.session1WeatherRainIv.setImageResource(R.mipmap.rain_unselect);
            holder.session1WeatherIceIv.setImageResource(R.mipmap.ice_unselect);
            holder.session1WeatherSunIv.setImageResource(R.mipmap.sun_unselect);
            holder.session1WeatherCloudIv.setImageResource(R.mipmap.cloud_unselect);

            String weather=beanStep2Data.getString("weather");
            if (weather.compareToIgnoreCase(mContext.getString(R.string.rain)) == 0) {
                holder.session1WeatherRainIv.setImageResource(R.mipmap.rain_select);
            } else if (weather.compareToIgnoreCase(mContext.getString(R.string.ice)) == 0) {
                holder.session1WeatherIceIv.setImageResource(R.mipmap.ice_select);
            } else if (weather.compareToIgnoreCase(mContext.getString(R.string.sunday)) == 0) {
                holder.session1WeatherSunIv.setImageResource(R.mipmap.sun_select);
            } else {
                holder.session1WeatherCloudIv.setImageResource(R.mipmap.cloud_slect);
            }

            Log.e("tag","brand Name"+UploadLapTimeActivity.beanUploadData.getVehicleBrandName());
            if(UploadLapTimeActivity.beanUploadData.getRentVehicle().equalsIgnoreCase("0")){
                holder.tvVehicleBrand.setText(UploadLapTimeActivity.beanUploadData.getVehicleBrandName());
                holder.tvVehicleModel.setText(UploadLapTimeActivity.beanUploadData.getVehicleModelName());
            }
            else {
                holder.tvVehicleBrand.setText(UploadLapTimeActivity.beanUploadData.getRentedVehicleBrandName());
                holder.tvVehicleModel.setText(UploadLapTimeActivity.beanUploadData.getRentedVehicleModelName());
            }
            holder.session1Notes.setText(beanStep2Data.getString("notes"));

            holder.session1Notes.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String text = editable.toString().trim();
                    if (text.length() > 0)
                    {
                        try {
                            step2DataList.getJSONObject(holder.getAdapterPosition()).put("notes",text);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
            });
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return step2DataList.length();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextViewPlayBold sessionNoTv;
        private ImageView session1WeatherRainIv;
        private ImageView session1WeatherIceIv;
        private ImageView session1WeatherSunIv;
        private ImageView session1WeatherCloudIv;
        private TextViewPlayRegular tvVehicleBrand;
        private TextViewPlayRegular tvVehicleModel;
        private EditTextPlayRegular session1Notes;

        public MyViewHolder(View itemView) {
            super(itemView);

            sessionNoTv = (TextViewPlayBold) itemView.findViewById(R.id.sessionNoTv);
            session1WeatherRainIv = (ImageView) itemView.findViewById(R.id.session1_weather_rain_iv);
            session1WeatherIceIv = (ImageView) itemView.findViewById(R.id.session1_weather_ice_iv);
            session1WeatherSunIv = (ImageView) itemView.findViewById(R.id.session1_weather_sun_iv);
            session1WeatherCloudIv = (ImageView) itemView.findViewById(R.id.session1_weather_cloud_iv);
            tvVehicleBrand = (TextViewPlayRegular) itemView.findViewById(R.id.tvVehicleBrand);
            tvVehicleModel = (TextViewPlayRegular) itemView.findViewById(R.id.tvVehicleModel);
            session1Notes = (EditTextPlayRegular) itemView.findViewById(R.id.session1_notes);

            session1WeatherRainIv.setOnClickListener(this);
            session1WeatherIceIv.setOnClickListener(this);
            session1WeatherSunIv.setOnClickListener(this);
            session1WeatherCloudIv.setOnClickListener(this);
            tvVehicleBrand.setOnClickListener(this);
            tvVehicleModel.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.session1_weather_rain_iv:
                    onItemClickListener.onWeatherClick(getAdapterPosition(), mContext.getString(R.string.rain));
                    break;

                case R.id.session1_weather_ice_iv:
                    onItemClickListener.onWeatherClick(getAdapterPosition(), mContext.getString(R.string.ice));
                    break;

                case R.id.session1_weather_sun_iv:
                    onItemClickListener.onWeatherClick(getAdapterPosition(), mContext.getString(R.string.sunday));
                    break;

                case R.id.session1_weather_cloud_iv:
                    onItemClickListener.onWeatherClick(getAdapterPosition(), mContext.getString(R.string.cloud));
                    break;

                case R.id.tvVehicleBrand:
                    onItemClickListener.onTyreBrandClick(getAdapterPosition());
                    break;

                case R.id.tvVehicleModel:
                    onItemClickListener.onTyreModelClick(getAdapterPosition());
                    break;
            }
        }
    }
}
