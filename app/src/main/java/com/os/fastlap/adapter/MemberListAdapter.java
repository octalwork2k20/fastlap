package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.dashboardmodals.LikeUserBean;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 7/17/2017.
 */

public class MemberListAdapter extends RecyclerView.Adapter<MemberListAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<LikeUserBean> mMemberList=new ArrayList<>();

    public MemberListAdapter(Context context, ArrayList<LikeUserBean> arrayList) {
        mContext = context;
        mMemberList = arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_member;
        public MyViewHolder(View view) {
            super(view);
            img_member = (ImageView)view.findViewById(R.id.img_member);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.member_list_row_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mMemberList.get(position).getLike_user_image(), holder.img_member, Util.getImageLoaderOption(mContext));
    }

    @Override
    public int getItemCount() {
        return mMemberList.size();
    }
}
