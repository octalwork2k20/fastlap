package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 11/8/2017
 */

public class TaggedUserAdapter extends RecyclerView.Adapter<TaggedUserAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<DashboardCommentPostTagFriendsBeans> likes_list = new ArrayList<>();
    private ArrayList<DashboardPostShareUserModal> share_list = new ArrayList<>();

    public TaggedUserAdapter(ArrayList<DashboardCommentPostTagFriendsBeans> likes_list, Context context) {
        this.likes_list = likes_list;
        this.context = context;
    }

    @Override
    public TaggedUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.likes_user_list_item, parent, false);
        return new TaggedUserAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TaggedUserAdapter.MyViewHolder holder, final int position) {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + likes_list.get(position).getFriendIdBean().getPersonalInfo().getImage(), holder.userProfileImg, Util.getImageLoaderOption(context));
        holder.userNameTv.setText(likes_list.get(position).getFriendIdBean().getPersonalInfo().getFirstName() + " " + likes_list.get(position).getFriendIdBean().getPersonalInfo().getLastName());
        holder.userStatusIcon.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return likes_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CircleImageView userProfileImg;
        private TextView userNameTv;
        private ImageView userStatusIcon;
        private RelativeLayout likeuser;

        public MyViewHolder(View itemView) {
            super(itemView);

            userProfileImg = itemView.findViewById(R.id.user_profile_img);
            userNameTv = itemView.findViewById(R.id.user_name_tv);
            userStatusIcon = itemView.findViewById(R.id.user_status_icon);
            likeuser = itemView.findViewById(R.id.likeuser);
            likeuser.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.likeuser:
                    Intent intent3 = new Intent(context, ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle.putString(S.user_id, likes_list.get(getAdapterPosition()).getFriendIdBean().get_id());
                    bundle.putString("status", "0");
                    intent3.putExtras(bundle);
                    context.startActivity(intent3);
                    break;
            }
        }
    }
}

