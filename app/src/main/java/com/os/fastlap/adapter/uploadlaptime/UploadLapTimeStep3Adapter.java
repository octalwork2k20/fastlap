package com.os.fastlap.adapter.uploadlaptime;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanStep3Data;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 8/22/2017.
 */

public class UploadLapTimeStep3Adapter extends RecyclerView.Adapter<UploadLapTimeStep3Adapter.MyViewHolder> implements UploadImageAdapter.OnItemClickListener, OnImagePickerDialogSelect {

    @Override
    public void onAddImageCLick(View v, int position, int itemPosition) {
        if (step3DataList.get(itemPosition).getImageList().get(position).isDefault()) {
            this.itemPosition = itemPosition;
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                checkRequestPermission();
            } else {
                showImageDialog();
            }
        }
    }

    @Override
    public void onDeleteImageClick(View v, int position, int itemPosition) {
        this.itemPosition = itemPosition;
        if (step3DataList.get(itemPosition).getImageList().get(position).getImage_id() != null && !step3DataList.get(itemPosition).getImageList().get(position).getImage_id().isEmpty()) {
            if (removeImages.isEmpty())
                removeImages = step3DataList.get(itemPosition).getImageList().get(position).getImage_id();
            else
                removeImages = removeImages + "," + step3DataList.get(itemPosition).getImageList().get(position).getImage_id();
        }

        /* check before delete image is for edit or add*/
        if (!step3DataList.get(itemPosition).getImageList().get(position).isEdit())
            step3DataList.get(itemPosition).getImageList().remove(position);
        else {
            step3DataList.get(itemPosition).getImageList().remove(position);
        }

        // refresh adapter
        notifyDataSetChanged();

    }

    /*Callback method for list item */
    public interface OnItemClickListener {
        void OnCamSelect(int postion);

        void OnGallSelect(int postion);
    }

    private Context mContext;
    private ArrayList<BeanStep3Data> step3DataList;
    private OnItemClickListener onItemClickListener;
    UploadImageAdapter uploadImageAdapter;
    AuthAPI authAPI;
    String removeImages = "";
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    int itemPosition = 0;


    public UploadLapTimeStep3Adapter(Context context, ArrayList<BeanStep3Data> step3DataList) {
        mContext = context;
        this.step3DataList = step3DataList;
        authAPI = new AuthAPI(mContext);
        onImagePickerDialogSelect = this;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_lap_time_step3_child, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        BeanStep3Data beanStep3Data = new BeanStep3Data();
        beanStep3Data = step3DataList.get(position);

        holder.sessionNoTv.setText(beanStep3Data.getSessionNo());

        holder.session1UrlTv.setText(beanStep3Data.getVideoURL());
        if (TextUtils.isEmpty(beanStep3Data.getVideoURL())) {
            holder.session1UploadedDetailLl.setVisibility(View.GONE);
        } else {
            holder.session1UploadedDetailLl.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(beanStep3Data.getVideoImage(), holder.session1UplodedurlImageIv, Util.getImageLoaderOption(mContext));
            holder.session1UplodedurlTracknameTv.setText(beanStep3Data.getChannelTitle());
            holder.session1UplodedurlEventnameTv.setText(beanStep3Data.getVideoTitle());
        }

        uploadImageAdapter = new UploadImageAdapter(step3DataList.get(position).getImageList(), mContext, position);
        uploadImageAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView.setLayoutManager(layoutManager);
        holder.recyclerView.setAdapter(uploadImageAdapter);


        holder.session1UrlCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String youtubeURL = holder.session1UrlTv.getText().toString().trim();
                    String videoID = "";
                    if (youtubeURL.length() > 0) {
                        videoID = youtubeURL.substring(youtubeURL.indexOf("=") + 1, youtubeURL.length());
                        step3DataList.get(holder.getAdapterPosition()).setVideoURL(youtubeURL);
                        authAPI.getYoutubeUrlData(mContext, videoID, mContext.getString(R.string.youtube_key), holder.getAdapterPosition());
                    }
                }
            }
        });
        holder.session1UrlTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                String text = editable.toString().trim();
                if (text.length() > 0)
                {
                    step3DataList.get(holder.getAdapterPosition()).setVideoURL(text);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return step3DataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextViewPlayBold sessionNoTv;
        private AppCompatCheckBox session1UrlCheckbox;
        private EditTextPlayRegular session1UrlTv;
        private LinearLayout session1UploadedDetailLl;
        private ImageView session1UplodedurlImageIv;
        private TextViewPlayRegular session1UplodedurlTracknameTv;
        private TextViewPlayRegular session1UplodedurlEventnameTv;
        private TextViewPlayRegular session1UplodedurlChannalTv;
        private AppCompatCheckBox session1PhotoCheckbox;
        private EditTextPlayRegular session1PhotoTv;
        private LinearLayout session1PhotoDetailLl;
        private RecyclerView recyclerView;


        public MyViewHolder(View itemView) {
            super(itemView);

            sessionNoTv = (TextViewPlayBold) itemView.findViewById(R.id.sessionNoTv);
            session1UrlCheckbox = (AppCompatCheckBox) itemView.findViewById(R.id.session1_url_checkbox);
            session1UrlTv = (EditTextPlayRegular) itemView.findViewById(R.id.session1_url_tv);
            session1UploadedDetailLl = (LinearLayout) itemView.findViewById(R.id.session1_uploaded_detail_ll);
            session1UplodedurlImageIv = (ImageView) itemView.findViewById(R.id.session1_uplodedurl_image_iv);
            session1UplodedurlTracknameTv = (TextViewPlayRegular) itemView.findViewById(R.id.session1_uplodedurl_trackname_tv);
            session1UplodedurlEventnameTv = (TextViewPlayRegular) itemView.findViewById(R.id.session1_uplodedurl_eventname_tv);
            session1UplodedurlChannalTv = (TextViewPlayRegular) itemView.findViewById(R.id.session1_uplodedurl_channal_tv);
            session1PhotoCheckbox = (AppCompatCheckBox) itemView.findViewById(R.id.session1_photo_checkbox);
            session1PhotoTv = (EditTextPlayRegular) itemView.findViewById(R.id.session1_photo_tv);
            session1PhotoDetailLl = (LinearLayout) itemView.findViewById(R.id.session1_photo_detail_ll);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {
        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(mContext, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        //pagerBean = null;
    }

    @Override
    public void OnCameraSelect() {
        onItemClickListener.OnCamSelect(itemPosition);
    }

    @Override
    public void OnGallerySelect() {
        onItemClickListener.OnGallSelect(itemPosition);
    }

    @Override
    public void onVideoSelect() {

    }


}
