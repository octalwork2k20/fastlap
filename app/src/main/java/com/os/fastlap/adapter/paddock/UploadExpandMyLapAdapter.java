package com.os.fastlap.adapter.paddock;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.adapter.profile.AddVehicleImageAdapter;
import com.os.fastlap.beans.BeanSessionData;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/14/2017.
 */

public class UploadExpandMyLapAdapter extends BaseExpandableListAdapter implements AddVehicleImageAdapter.OnItemClickListener {
    Context mContext;
    ArrayList<BeanSessionData> sessionArrayList;
    OnClick onClick;
    AuthAPI authAPI;
    AddVehicleImageAdapter mAddVehicleImageAdapter;
    int selectedPosition = 0;

    @Override
    public void onAddImageCLick(View v, int position) {
        onClick.onAddImageCLick(selectedPosition, position);
    }

    @Override
    public void onDeleteImageClick(View v, int position) {
        onClick.onDeleteImageClick(selectedPosition, position);
    }

    public interface OnClick {
        public void vehicleTvClick(int groupPosition);

        public void onDateClick(int groupPosition);

        public void tyreBrandTvClick(int groupPosition);

        public void tyreModelTvClick(int groupPosition);

        public void weatherClick(int groupPosition, String weather);

        public void saveTvClick(int groupPosition);

        public void onAddImageCLick(int groupPosition, int position);

        public void onDeleteImageClick(int groupPosition, int position);

    }

    public void setOnClickListner(OnClick onClick) {
        this.onClick = onClick;
    }

    public UploadExpandMyLapAdapter(Context context, ArrayList<BeanSessionData> sessionArrayList) {
        mContext = context;
        this.sessionArrayList = sessionArrayList;
        authAPI = new AuthAPI(mContext);

    }


    @Override
    public int getGroupCount() {
        return sessionArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return sessionArrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return sessionArrayList.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = layoutInflater.inflate(R.layout.mylaps_upload_parentview, null);

        BeanSessionData beanSessionData = new BeanSessionData();
        beanSessionData = sessionArrayList.get(groupPosition);


        LinearLayout sessionsLayout;
        TextViewPlayRegular cremonaTv;
        TextViewPlayRegular dateTv;
        TextViewPlayRegular sessionNoTv;
        TextViewPlayRegular newBtn;

        sessionsLayout = (LinearLayout) view.findViewById(R.id.sessions_layout);
        cremonaTv = (TextViewPlayRegular) view.findViewById(R.id.cremona_tv);
        dateTv = (TextViewPlayRegular) view.findViewById(R.id.date_tv);
        sessionNoTv = (TextViewPlayRegular) view.findViewById(R.id.session_no_tv);
        newBtn = (TextViewPlayRegular) view.findViewById(R.id.new_btn);


        cremonaTv.setText(beanSessionData.getTrackName() + " " + beanSessionData.getTrackVersionName());
        dateTv.setText(beanSessionData.getDateOfRecording());
        sessionNoTv.setText(mContext.getString(R.string.session1) + beanSessionData.getSessionIndex());

        if (isExpanded) {
            sessionsLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            cremonaTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            dateTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            sessionNoTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            cremonaTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
            selectedPosition = groupPosition;
        } else {
            cremonaTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down_gray, 0);
            if (groupPosition % 2 == 0) {
                sessionsLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.light_gray_color));
                cremonaTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                dateTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                sessionNoTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
            } else {
                sessionsLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.usertype_back));
                cremonaTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                dateTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                sessionNoTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
            }
        }


        return view;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View view, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.mylaps_upload_childview, null);

        BeanSessionData beanSessionData = new BeanSessionData();
        beanSessionData = sessionArrayList.get(groupPosition);


        TextViewPlayRegular dateOfRecordingTv;
        LinearLayout conditionLl;
        ImageView cloudIv;
        ImageView iceIv;
        ImageView sunIv;
        ImageView rainIv;
        TextViewPlayRegular vehicleTv;
        TextViewPlayRegular tyreBrandTv;
        TextViewPlayRegular tyreModelTv;
        EditTextPlayRegular sessionsNoteEt;
        AppCompatCheckBox sessionUrlCheckbox;
        EditTextPlayRegular sessionUrlTv;
        LinearLayout sessionUploadedDetailLl;
        ImageView sessionUplodedurlImageIv;
        TextViewPlayRegular sessionUplodedurlTracknameTv;
        TextViewPlayRegular sessionUplodedurlEventnameTv;
        TextViewPlayRegular sessionUplodedurlChannalTv;
        AppCompatCheckBox sessionPhotoCheckbox;
        ImageView sessionCameraIv;
        EditTextPlayRegular sessionPhotoTv;
        RecyclerView recycler_view;
        TextViewPlayBold saveTv;


        dateOfRecordingTv = (TextViewPlayRegular) view.findViewById(R.id.date_of_recording_tv);
        conditionLl = (LinearLayout) view.findViewById(R.id.condition_ll);
        cloudIv = (ImageView) view.findViewById(R.id.cloud_iv);
        iceIv = (ImageView) view.findViewById(R.id.ice_iv);
        sunIv = (ImageView) view.findViewById(R.id.sun_iv);
        rainIv = (ImageView) view.findViewById(R.id.rain_iv);
        vehicleTv = (TextViewPlayRegular) view.findViewById(R.id.vehicle_tv);
        tyreBrandTv = (TextViewPlayRegular) view.findViewById(R.id.tyre_brand_tv);
        tyreModelTv = (TextViewPlayRegular) view.findViewById(R.id.tyre_model_tv);
        sessionsNoteEt = (EditTextPlayRegular) view.findViewById(R.id.sessions_note_et);
        sessionUrlCheckbox = (AppCompatCheckBox) view.findViewById(R.id.session_url_checkbox);
        sessionUrlTv = (EditTextPlayRegular) view.findViewById(R.id.session_url_tv);
        sessionUploadedDetailLl = (LinearLayout) view.findViewById(R.id.session_uploaded_detail_ll);
        sessionUplodedurlImageIv = (ImageView) view.findViewById(R.id.session_uplodedurl_image_iv);
        sessionUplodedurlTracknameTv = (TextViewPlayRegular) view.findViewById(R.id.session_uplodedurl_trackname_tv);
        sessionUplodedurlEventnameTv = (TextViewPlayRegular) view.findViewById(R.id.session_uplodedurl_eventname_tv);
        sessionUplodedurlChannalTv = (TextViewPlayRegular) view.findViewById(R.id.session_uplodedurl_channal_tv);
        sessionPhotoCheckbox = (AppCompatCheckBox) view.findViewById(R.id.session_photo_checkbox);
        sessionCameraIv = (ImageView) view.findViewById(R.id.session_camera_iv);
        sessionPhotoTv = (EditTextPlayRegular) view.findViewById(R.id.session_photo_tv);
        saveTv = (TextViewPlayBold) view.findViewById(R.id.save_tv);
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);


        dateOfRecordingTv.setText(beanSessionData.getDateOfRecording());

        rainIv.setImageResource(R.mipmap.rain_unselect);
        iceIv.setImageResource(R.mipmap.ice_unselect);
        sunIv.setImageResource(R.mipmap.sun_unselect);
        cloudIv.setImageResource(R.mipmap.cloud_unselect);

        String weather = beanSessionData.getWeather();
        if (weather.compareToIgnoreCase(mContext.getString(R.string.rain)) == 0) {
            rainIv.setImageResource(R.mipmap.rain_select);
        } else if (weather.compareToIgnoreCase(mContext.getString(R.string.ice)) == 0) {
            iceIv.setImageResource(R.mipmap.ice_select);
        } else if (weather.compareToIgnoreCase(mContext.getString(R.string.dry)) == 0) {
            sunIv.setImageResource(R.mipmap.sun_select);
        } else {
            cloudIv.setImageResource(R.mipmap.cloud_slect);
        }

        vehicleTv.setText(beanSessionData.getUserVehicleModelName());

        tyreBrandTv.setText(beanSessionData.getVehicletyreBrandName());
        tyreModelTv.setText(beanSessionData.getVehicletyreModelName());
        sessionsNoteEt.setText(beanSessionData.getNotes());

        String videoURL = beanSessionData.getYoutubeUrl().trim();
        sessionUrlTv.setText(videoURL);

        if (!TextUtils.isEmpty(beanSessionData.getChannelTitle())) {
            sessionUploadedDetailLl.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(beanSessionData.getVideoImage(), sessionUplodedurlImageIv, Util.getImageLoaderOption(mContext));
            sessionUplodedurlTracknameTv.setText(beanSessionData.getChannelTitle());
            sessionUplodedurlEventnameTv.setText(beanSessionData.getVideoTitle());
        } else {
            if (videoURL.length() > 0) {
                sessionUploadedDetailLl.setVisibility(View.GONE);
                String videoID = "";
                videoID = videoURL.substring(videoURL.indexOf("=") + 1, videoURL.length());
                authAPI.getYoutubeUrlData(mContext, videoID, mContext.getString(R.string.youtube_key), groupPosition);
            }
        }

        authAPI.getVehicleTyreBrand(mContext, beanSessionData.getUserVehicletypeId());
        authAPI.getVehicleTyreModel(mContext, beanSessionData.getUserVehicletypeId(), beanSessionData.getVehicletyreBrandId());

        mAddVehicleImageAdapter = new AddVehicleImageAdapter(beanSessionData.getSessionsImagesList(), mContext);
        //  mAddVehicleImageAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setAdapter(mAddVehicleImageAdapter);
        mAddVehicleImageAdapter.setOnItemClickListener(this);


        vehicleTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // onClick.vehicleTvClick(groupPosition);
            }
        });
        dateOfRecordingTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onDateClick(groupPosition);
            }
        });
        tyreBrandTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.tyreBrandTvClick(groupPosition);
            }
        });
        tyreModelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.tyreModelTvClick(groupPosition);
            }
        });
        cloudIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.weatherClick(groupPosition, mContext.getString(R.string.cloud));
            }
        });
        sunIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.weatherClick(groupPosition, mContext.getString(R.string.dry));
            }
        });
        iceIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.weatherClick(groupPosition, mContext.getString(R.string.ice));
            }
        });
        rainIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.weatherClick(groupPosition, mContext.getString(R.string.rain));
            }
        });
        saveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.saveTvClick(groupPosition);
            }
        });

        sessionsNoteEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString().trim();
                if (text.length() > 0) {
                    sessionArrayList.get(groupPosition).setNotes(text);
                }

            }
        });

        sessionUrlTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString().trim();
                if (text.length() > 0) {
                    sessionArrayList.get(groupPosition).setYoutubeUrl(text);
                }

            }
        });


        return view;

    }

    public static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

}
