package com.os.fastlap.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.AlbumCommentActivity;
import com.os.fastlap.activity.NotificationListActivity;
import com.os.fastlap.activity.dashboard.CommentActivity;
import com.os.fastlap.activity.dashboard.ReplyActivity;
import com.os.fastlap.activity.profile.InfoActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.NotificationBeans;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhinava on 7/12/2017
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<NotificationBeans> notification_list = new ArrayList<>();
    private NotificationInterface notificationInterface;

    public interface NotificationInterface {
        void ItemClickListner(int position);
    }

    public void OnClickListner(NotificationInterface notificationInterface) {
        this.notificationInterface = notificationInterface;
    }


    public NotificationAdapter(ArrayList<NotificationBeans> notification_list, Context context) {
        this.notification_list = notification_list;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String message=notification_list.get(position).getMessage();
        //String nickName = "<font color=\"#bc2939\"><strong>"+notification_list.get(position).getNickName()+"</strong></font>";
       // holder.notiMsgTv.setText(Html.fromHtml(message.replace(notification_list.get(position).getNickName(),nickName)), TextView.BufferType.SPANNABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            holder.notiMsgTv.setText(Html.fromHtml(message), TextView.BufferType.SPANNABLE);
        }
        else {
            holder.notiMsgTv.setText(Html.fromHtml(message));
        }
        try {
            holder.notiTimeTv.setText(Util.calculateTimeDiffFromNow(notification_list.get(position).getCreated(), mContext));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (notification_list.get(position).getStatus() == 0) {
            holder.parent_ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.very_light_gray_color));
        } else {
            holder.parent_ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        }
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + notification_list.get(position).getSenderImage(), holder.userProfileIv, Util.getImageLoaderOption(mContext));
        holder.swipeRevealLayout.close(true);
     //   holder.swipeRevealLayout.setLockDrag(true);

    }

    @Override
    public int getItemCount() {
        return notification_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CircleImageView userProfileIv;
        private TextView notiMsgTv;
        private TextView notiTimeTv;
        private LinearLayout parent_ll;
        private FrameLayout frameDelete;
        private SwipeRevealLayout swipeRevealLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            userProfileIv = itemView.findViewById(R.id.user_profile_iv);
            notiMsgTv = itemView.findViewById(R.id.noti_msg_tv);
            notiTimeTv = itemView.findViewById(R.id.noti_time_tv);
            parent_ll = itemView.findViewById(R.id.parent_ll);
            frameDelete = itemView.findViewById(R.id.frameDelete);
            swipeRevealLayout = itemView.findViewById(R.id.swiperefresh);

            parent_ll.setOnClickListener(this);
            frameDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.parent_ll:
                    if (notification_list.get(getAdapterPosition()).getStatus() == 0) {
                        notificationInterface.ItemClickListner(getAdapterPosition());
                        notification_list.get(getAdapterPosition()).setStatus(1);
                        notifyItemChanged(getAdapterPosition());
                    }
                    jumpOnTaponScreen(notification_list.get(getAdapterPosition()).getType(), getAdapterPosition());
                    break;
                case R.id.frameDelete:
                    if (mContext instanceof NotificationListActivity)
                        ((NotificationListActivity) mContext).removeNotificationApi(notification_list.get(getAdapterPosition()).get_id());
                    break;
            }

        }
    }
    private void jumpOnTaponScreen(String type, int position) {
        Intent intent = null;
        switch (type) {
            case "FriendRequestSent":
            case "AcceptRequest":
                try {
                    Log.e("tag","user ID"+notification_list.get(position).getSenderId());
                    Log.e("tag","friend ID"+MySharedPreferences.getPreferences(mContext,S.user_id));
                    intent = new Intent(mContext, InfoActivity.class);
                    intent.putExtra(S.friendId, MySharedPreferences.getPreferences(mContext,S.user_id));
                    intent.putExtra(S.user_id, notification_list.get(position).getSenderId());
                    ProfileActivity.profileUserId=notification_list.get(position).getSenderId();
                    intent.putExtra(S.status, "1");
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case "PostLike":
            case "PostComment":
            case "PostCommentLike":
                try {
                    if (notification_list.get(position).getCustomData() != null) {
                        JSONObject jsonObject = new JSONObject(notification_list.get(position).getCustomData());
                        intent = new Intent(mContext, CommentActivity.class);
                        intent.putExtra(S.postId, jsonObject.has(S.postId)?jsonObject.getString(S.postId):"");
                        intent.putExtra(S.position, "0");
                    }
                } catch (Exception e) {
                    Log.e(NotificationListActivity.TAG, e.toString());
                }
                break;
            case "PostCommentReply":
                try {
                    if (notification_list.get(position).getCustomData() != null) {
                        JSONObject jsonObject = new JSONObject(notification_list.get(position).getCustomData());
                        intent = new Intent(mContext, ReplyActivity.class);
                        intent.putExtra(S.postId, jsonObject.has(S.postId)?jsonObject.getString(S.postId):"");
                        intent.putExtra(S.userPostCommentId, jsonObject.has(S.userPostCommentId)?jsonObject.getString(S.userPostCommentId):"");
                    }

                } catch (Exception e) {
                    Log.e(NotificationListActivity.TAG, e.toString());
                }
                break;
                case "AlbumLike":
                case "AlbumComment":
                    try {
                        if (notification_list.get(position).getCustomData() != null) {
                            JSONObject jsonObject = new JSONObject(notification_list.get(position).getCustomData());
                            intent = new Intent(mContext, AlbumCommentActivity.class);
                            intent.putExtra(S.postId, jsonObject.has(S.albumId)?jsonObject.getString(S.albumId):"");
                            intent.putExtra(S.position, "0");

                        }
                    } catch (Exception e) {
                        Log.e(NotificationListActivity.TAG, e.toString());
                    }
                    break;
            default:
        }
        if (intent != null)
            mContext.startActivity(intent);
    }
}
