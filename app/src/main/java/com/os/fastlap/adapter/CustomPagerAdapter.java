package com.os.fastlap.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.dashboardmodals.DashboardPostImagesBeans;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by anandj on 9/4/2017.
 */

public class CustomPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<DashboardPostImagesBeans> mPagerList;
    private String trackImg = "";

    public CustomPagerAdapter(Context mContext, List<DashboardPostImagesBeans> mPagerList, String trackImg) {
        this.mContext = mContext;
        this.mPagerList = mPagerList;
        this.trackImg=trackImg;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position)
    {
        ViewGroup layout = (ViewGroup) LayoutInflater.from(mContext).inflate(R.layout.post_item_child, container, false);
        ImageView imgCar = layout.findViewById(R.id.imgCar);
        ImageView imgPlay = layout.findViewById(R.id.imgPlay);
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int pos = (int) view.getTag();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(S.IMAGE_BASE_URL + mPagerList.get(pos).getFileName()));
                    intent.setDataAndType(Uri.parse(S.IMAGE_BASE_URL + mPagerList.get(pos).getFileName()), "video/*");
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.no_supportable_Player), Toast.LENGTH_LONG).show();
                }
            }
        });

        if (trackImg.isEmpty())
        {
            String path = "";
            if (mPagerList.get(position).getType().equalsIgnoreCase(S.videoType)) {
                path = mPagerList.get(position).getThumbName().replace(" ", "%20");
            } else
                path = mPagerList.get(position).getFileName().replace(" ", "%20");
            //ImageLoader.getInstance().displayImage( S.IMAGE_BASE_URL + path, imgCar, Util.getImageLoaderOption(mContext));
            ShowImage(imgCar, S.IMAGE_BASE_URL + path);

            if (mPagerList.get(position).getType().equalsIgnoreCase(S.videoType)) {
                imgPlay.setTag(position);
                imgPlay.setVisibility(View.VISIBLE);
            } else
                imgPlay.setVisibility(View.GONE);
        }
        else {
          /*  String path = trackImg.replace(" ", "%20");
             ImageLoader.getInstance().displayImage( S.IMAGE_BASE_URL + path, imgCar, Util.getImageLoaderOption(mContext));
             imgPlay.setVisibility(View.GONE);*/
            if (position == 0) {
                String path = trackImg.replace(" ", "%20");
                // ImageLoader.getInstance().displayImage( S.IMAGE_BASE_URL + path, imgCar, Util.getImageLoaderOption(mContext));
                ShowImage(imgCar, S.IMAGE_BASE_URL + path);
                imgPlay.setVisibility(View.GONE);
            } else {
                String path = "";
                if (mPagerList.get(position - 1).getType().equalsIgnoreCase(S.videoType)) {
                    imgPlay.setVisibility(View.VISIBLE);
                    imgPlay.setTag(position);
                    path = mPagerList.get(position - 1).getThumbName().replace(" ", "%20");
                } else {
                    imgPlay.setVisibility(View.GONE);
                    path = mPagerList.get(position - 1).getFileName().replace(" ", "%20");
                }
                //ImageLoader.getInstance().displayImage( S.IMAGE_BASE_URL + path, imgCar, Util.getImageLoaderOption(mContext));
                ShowImage(imgCar, S.IMAGE_BASE_URL + path);
            }
        }

        imgCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> list = new ArrayList<>();

                if (trackImg.isEmpty()) {
                    String path = "";
                    for (DashboardPostImagesBeans dashboardPostImagesBeans : mPagerList) {
                        if (!dashboardPostImagesBeans.getType().equalsIgnoreCase(S.videoType)) {
                            path = S.IMAGE_BASE_URL + dashboardPostImagesBeans.getFileName().replace(" ", "%20");
                            list.add(path);
                        }
                    }
                } else {
                    String path = S.IMAGE_BASE_URL + trackImg.replace(" ", "%20");
                    list.add(path);

                    for (DashboardPostImagesBeans dashboardPostImagesBeans : mPagerList) {
                        if (!dashboardPostImagesBeans.getType().equalsIgnoreCase(S.videoType)) {
                            path = S.IMAGE_BASE_URL + dashboardPostImagesBeans.getFileName().replace(" ", "%20");
                            list.add(path);
                        }
                    }
                }


                new ImageViewer.Builder(mContext, list)
                        .setStartPosition(position)
                        .allowZooming(true)
                        .show();
            }
        });


        container.addView(layout);
        return layout;
    }

    public void ShowImage(final ImageView imageView, String filePath) {
        Log.e("tag","file path"+filePath);
        ImageLoader.getInstance().displayImage(filePath, imageView, Util.getImageLoaderOptionDefaultNull(mContext));
    }


    @Override
    public int getCount() {
        if (trackImg.isEmpty())
            return mPagerList.size();
        else
            return mPagerList.size() + 1;
    }
    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
