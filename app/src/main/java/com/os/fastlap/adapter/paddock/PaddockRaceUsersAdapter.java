package com.os.fastlap.adapter.paddock;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsRaceActivity;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/19/2017.
 */

public class PaddockRaceUsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<BeanTimeLap> mArrayList;
    Activity activity;
    int width;
    public PaddockRaceUsersAdapter(Context context, ArrayList<BeanTimeLap> arrayList, Activity activity, int width) {
        this.mArrayList = arrayList;
        mContext = context;
        this.width = width;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.mylaps_race_userlist_row1, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder)
        {
            ((MyViewHolder) holder).userNameTv.setText(mArrayList.get(position).getUserName());
            ((MyViewHolder) holder).mVehicleName.setText(mArrayList.get(position).getVehicleBrandName()+" "+mArrayList.get(position).getVehicleModelName());
            Log.e("tag","TIme "+mArrayList.get(position).getBestLapTime());

            ((MyViewHolder) holder).bestTimeTv.setText(mArrayList.get(position).getBestLapTime());

            if(!mArrayList.get(position).getDistance().equalsIgnoreCase("")){
                double ditance= Double.parseDouble(mArrayList.get(position).getDistance());
                ((MyViewHolder) holder).avgSpeedTv.setText(new DecimalFormat("##.##").format(ditance));
            }

            if(!mArrayList.get(position).getAccel_x().equalsIgnoreCase("")){
                double xValue= Double.parseDouble(mArrayList.get(position).getAccel_x());
                ((MyViewHolder) holder).tvX.setText(new DecimalFormat("##.##").format(xValue));
            }
            if(!mArrayList.get(position).getAccel_y().equalsIgnoreCase("")){
                double yValue= Double.parseDouble(mArrayList.get(position).getAccel_y());
                ((MyViewHolder) holder).tvY.setText(new DecimalFormat("##.##").format(yValue));
            }
            if(!mArrayList.get(position).getAccel_z().equalsIgnoreCase("")){
                double zValue= Double.parseDouble(mArrayList.get(position).getAccel_z());
                ((MyViewHolder) holder).tvZ.setText(new DecimalFormat("##.##").format(zValue));
            }
            if(!mArrayList.get(position).getBrake_on().equalsIgnoreCase("")){
                double brackOn= Double.parseDouble(mArrayList.get(position).getBrake_on());
                ((MyViewHolder) holder).tvBrakeOn.setText(new DecimalFormat("##.##").format(brackOn));
            }
            if(!mArrayList.get(position).getBrake_calc().equalsIgnoreCase("")){
                double brakeCalc= Double.parseDouble(mArrayList.get(position).getBrake_calc());
                ((MyViewHolder) holder).tvBrakeCalc.setText(new DecimalFormat("##.##").format(brakeCalc));
            }
            if(!mArrayList.get(position).getAcc_on().equalsIgnoreCase("")){
                double accOn= Double.parseDouble(mArrayList.get(position).getAcc_on());
                ((MyViewHolder) holder).tvAccOn.setText(new DecimalFormat("##.##").format(accOn));

            }
            if(!mArrayList.get(position).getAcc_calc().equalsIgnoreCase("")){
                double accCalc= Double.parseDouble(mArrayList.get(position).getAcc_calc());
                ((MyViewHolder) holder).tvAccCalc.setText(new DecimalFormat("##.##").format(accCalc));
            }

            int color=mArrayList.get(position).getLapColor();
           // String defaultColor = String.format("%06X", (0xFFFFFF & color));
            ((MyViewHolder) holder).userProfileIv.setBorderColor(mArrayList.get(position).getLapColor());
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mArrayList.get(position).getUserImage(), ((MyViewHolder) holder).userProfileIv, Util.getImageLoaderOption(mContext));
            try {
                String speed = MySharedPreferences.getPreferences(mContext, S.speed);
                if (!TextUtils.isEmpty(speed)) {
                    if (!speed.equalsIgnoreCase("MILES")) {
                        ((MyViewHolder) holder).maxSpeedTv.setText(mArrayList.get(position).getMaxSpeed() + " " + mContext.getString(R.string.KMPH));
                       // ((MyViewHolder) holder).avgSpeedTv.setText(mArrayList.get(position).getAvgSpeed() + " " + mContext.getString(R.string.KMPH));
                    } else {
                        double max = Double.parseDouble(mArrayList.get(position).getMaxSpeed());
                        max = max / 1.61;

                        double avg = Double.parseDouble(mArrayList.get(position).getAvgSpeed());
                        avg = avg / 1.61;

                        ((MyViewHolder) holder).maxSpeedTv.setText(new DecimalFormat("0.00").format(max) + " " + mContext.getString(R.string.MPH));
                       // ((MyViewHolder) holder).avgSpeedTv.setText(new DecimalFormat("0.00").format(avg) + " " + mContext.getString(R.string.MPH));
                    }
                } else {
                    ((MyViewHolder) holder).maxSpeedTv.setText(mArrayList.get(position).getMaxSpeed() + " " + mContext.getString(R.string.KMPH));
                    //((MyViewHolder) holder).avgSpeedTv.setText(mArrayList.get(position).getAvgSpeed() + " " + mContext.getString(R.string.KMPH));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*if (position % 2 != 0) {
                ((MyViewHolder) holder).parentLl.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).userNameTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).userRankTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).bestTimeTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).maxSpeedTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).avgSpeedTv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));

                ((MyViewHolder) holder).tvX.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).tvY.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).tvZ.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));

                ((MyViewHolder) holder).tvAccCalc.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).tvAccOn.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).tvBrakeCalc.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).tvBrakeOn.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));

            } else {
                ((MyViewHolder) holder).parentLl.setBackgroundColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                ((MyViewHolder) holder).userNameTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).userRankTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).bestTimeTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).maxSpeedTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).avgSpeedTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));

                ((MyViewHolder) holder).tvX.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).tvY.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).tvZ.setTextColor(ContextCompat.getColor(mContext, R.color.white));


                ((MyViewHolder) holder).tvAccCalc.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).tvAccOn.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).tvBrakeCalc.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                ((MyViewHolder) holder).tvBrakeOn.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            }*/

            if(!MySharedPreferences.getBooleanPreferences(mContext,S.checkSubscription)){
                ((MyViewHolder) holder).lytX.setVisibility(View.GONE);
                ((MyViewHolder) holder).lytY.setVisibility(View.GONE);
                ((MyViewHolder) holder).lytZ.setVisibility(View.GONE);

                ((MyViewHolder) holder).lytBrakeAcc.setVisibility(View.GONE);
                //((MyViewHolder) holder).viewBrakeAcc.setVisibility(View.INVISIBLE);

                ((MyViewHolder) holder).lytAccCalc.setVisibility(View.GONE);
               // ((MyViewHolder) holder).viewAccCalc.setVisibility(View.INVISIBLE);
            }
            if(MyLapsRaceActivity.isAccOn){
                //((MyViewHolder) holder).viewAccOn.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).lytAccOn.setVisibility(View.VISIBLE);
            }
            else {
               // ((MyViewHolder) holder).viewAccOn.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytAccOn.setVisibility(View.GONE);
            }
            if(MyLapsRaceActivity.isAccCalc){
                //((MyViewHolder) holder).viewAccCalc.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).lytAccCalc.setVisibility(View.VISIBLE);
            }
            else {
                //((MyViewHolder) holder).viewAccCalc.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytAccCalc.setVisibility(View.GONE);
            }
            if(MyLapsRaceActivity.isBrakeOn){
                //((MyViewHolder) holder).viewBrakeOn.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).lytBrakeOn.setVisibility(View.VISIBLE);
            }
            else {
                //((MyViewHolder) holder).viewBrakeOn.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytBrakeOn.setVisibility(View.GONE);
            }
            if(MyLapsRaceActivity.isBrakeCalc){
                //((MyViewHolder) holder).viewBrakeAcc.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).lytBrakeAcc.setVisibility(View.VISIBLE);
            }
            else {
                //((MyViewHolder) holder).viewBrakeAcc.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytBrakeAcc.setVisibility(View.GONE);
            }
            if(MyLapsRaceActivity.isXView){
                //((MyViewHolder) holder).viewX.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).lytX.setVisibility(View.VISIBLE);
            }
            else {
                //((MyViewHolder) holder).viewX.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytX.setVisibility(View.GONE);
            }
            if(MyLapsRaceActivity.isYView){
                //((MyViewHolder) holder).viewY.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).lytY.setVisibility(View.VISIBLE);
            }
            else {
                //((MyViewHolder) holder).viewY.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytY.setVisibility(View.GONE);
            }
            if(MyLapsRaceActivity.isZView){
                //((MyViewHolder) holder).viewZ.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytZ.setVisibility(View.VISIBLE);
            }
            else {
                //((MyViewHolder) holder).viewZ.setVisibility(View.INVISIBLE);
                ((MyViewHolder) holder).lytZ.setVisibility(View.GONE);
            }

            //LinearLayout.LayoutParams prm1 = new LinearLayout.LayoutParams(width / 2, LinearLayout.LayoutParams.MATCH_PARENT, 1);
            //((MyViewHolder) holder).parentLl.setLayoutParams(prm1);


        }
    }
    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView userProfileIv;
        private TextView userNameTv;
        private TextView bestTimeTv;
        private TextView maxSpeedTv;
        private TextView avgSpeedTv;
        private TextView mVehicleName;
        private TextView tvX;
        private TextView tvY;
        private TextView tvZ;
        public  LinearLayout lytX;
        public   LinearLayout lytY;
        public  LinearLayout lytZ;

        public TextView tvAccOn;
        public TextView tvBrakeOn;
        public TextView tvBrakeCalc;
        public TextView tvAccCalc;

        public  LinearLayout lytAccOn;
        public  LinearLayout lytBrakeOn;
        public  LinearLayout lytBrakeAcc;
        public  LinearLayout lytAccCalc;

        public  View viewX;
        public  View viewY;
        public  View viewZ;

        public  View viewAccOn;
        public  View viewBrakeOn;
        public  View viewBrakeAcc;
        public  View viewAccCalc;

        public MyViewHolder(View itemView) {
            super(itemView);
            lytAccOn = (LinearLayout) itemView.findViewById(R.id.lytAccOn);
            lytBrakeOn = (LinearLayout) itemView.findViewById(R.id.lytBrakeOn);
            lytBrakeAcc = (LinearLayout) itemView.findViewById(R.id.lytBrakeAcc);
            lytAccCalc = (LinearLayout) itemView.findViewById(R.id.lytAccCalc);

            tvAccOn = (TextView) itemView.findViewById(R.id.tvAccOn);
            tvBrakeOn = (TextView) itemView.findViewById(R.id.tvBrakeOn);
            tvBrakeCalc = (TextView) itemView.findViewById(R.id.tvBrakeCalc);
            tvAccCalc = (TextView) itemView.findViewById(R.id.tvAccCalc);
            mVehicleName=(TextView)itemView.findViewById(R.id.tvVehicleName);
            userProfileIv = (CircleImageView) itemView.findViewById(R.id.user_profile_iv);
            userNameTv = (TextView) itemView.findViewById(R.id.user_name_tv);
            bestTimeTv = (TextView) itemView.findViewById(R.id.best_time_tv);
            maxSpeedTv = (TextView) itemView.findViewById(R.id.max_speed_tv);
            avgSpeedTv = (TextView) itemView.findViewById(R.id.avg_speed_tv);

            lytX = (LinearLayout) itemView.findViewById(R.id.lytX);
            lytY = (LinearLayout) itemView.findViewById(R.id.lytY);
            lytZ = (LinearLayout) itemView.findViewById(R.id.lytZ);

            tvX = (TextView) itemView.findViewById(R.id.AccelX);
            tvY = (TextView) itemView.findViewById(R.id.AccelY);
            tvZ = (TextView) itemView.findViewById(R.id.AccelZ);

        }
    }


}
