package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanGallery;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 7/13/2017.
 */

public class PhotoRecyclerViewAdapter extends RecyclerView.Adapter<PhotoRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<BeanGallery> mPhoto_list = new ArrayList<>();

    public PhotoRecyclerViewAdapter(Context context, ArrayList<BeanGallery> photo_list) {

        this.mPhoto_list = photo_list;
        this.mContext = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_car;
        ImageView play_icon;
        TextView count_tv;
        TextView albumname_tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_car = (ImageView) itemView.findViewById(R.id.img_car);
            play_icon = (ImageView) itemView.findViewById(R.id.play_icon);
            count_tv = (TextView) itemView.findViewById(R.id.count_tv);
            albumname_tv = (TextView) itemView.findViewById(R.id.albumname_tv);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_photo_child, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (mPhoto_list.get(position).getType().compareTo("1") == 0) {
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mPhoto_list.get(position).getFileName(), holder.img_car, Util.getImageLoaderOption(mContext));
            holder.count_tv.setText("1 " + mContext.getString(R.string.photo));
            holder.play_icon.setVisibility(View.GONE);
        } else if (mPhoto_list.get(position).getType().compareTo("2") == 0) {
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mPhoto_list.get(position).getThumbName(), holder.img_car, Util.getImageLoaderOption(mContext));
            holder.count_tv.setText("1 " + mContext.getString(R.string.video));
            holder.play_icon.setVisibility(View.VISIBLE);
        } else {
            holder.img_car.setImageResource(R.drawable.car_img);
            holder.count_tv.setText("1 " + mContext.getString(R.string.media));
            holder.play_icon.setVisibility(View.GONE);
        }

        holder.albumname_tv.setText(Util.ConvertDateTimeZoneDate(mPhoto_list.get(position).getCreated()));

        holder.play_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(S.IMAGE_BASE_URL + mPhoto_list.get(position).getFileName()));
                    intent.setDataAndType(Uri.parse(S.IMAGE_BASE_URL + mPhoto_list.get(position).getFileName()), "video/*");
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.no_supportable_Player), Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mPhoto_list.size();
    }
}
