package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.ButtonPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.List;



/**
 * Created by anandj on 7/14/2017.
 */

public class LapsProfileAdapter extends RecyclerView.Adapter<LapsProfileAdapter.MyViewHolder> {

    private Context mContext;
    private List<PaddockMyLapsParentModel> laps_list;


    public LapsProfileAdapter(Context context, List<PaddockMyLapsParentModel> laps_list){
        mContext = context;
        this.laps_list = laps_list;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imgLpas;
        TextViewPlayBold trackNameTv;
        TextViewPlayRegular rankTv;
        TextViewPlayRegular timeTv;
        TextViewPlayRegular dateTv;
        TextViewPlayRegular vehicleNameTv;
        ButtonPlayBold viewMoreBtn;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgLpas = (ImageView) itemView.findViewById(R.id.img_lpas);
            trackNameTv = (TextViewPlayBold) itemView.findViewById(R.id.track_name_tv);
            rankTv = (TextViewPlayRegular) itemView.findViewById(R.id.rank_tv);
            timeTv = (TextViewPlayRegular) itemView.findViewById(R.id.time_tv);
            dateTv = (TextViewPlayRegular) itemView.findViewById(R.id.date_tv);
            vehicleNameTv = (TextViewPlayRegular) itemView.findViewById(R.id.vehicle_name_tv);
            viewMoreBtn = (ButtonPlayBold) itemView.findViewById(R.id.view_more_btn);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_laps_child_activity,parent,false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        PaddockMyLapsParentModel paddockMyLapsParentModel = new PaddockMyLapsParentModel();
        paddockMyLapsParentModel =laps_list.get(position);

        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + paddockMyLapsParentModel.getTrackImage(), holder.imgLpas, Util.getImageLoaderOption(mContext));
        holder.trackNameTv.setText(paddockMyLapsParentModel.getTrackName());
        holder.timeTv.setText(paddockMyLapsParentModel.getMyLapsChildList().get(0).getTime());
        holder.dateTv.setText(Util.ConvertDateTimeZoneDate(paddockMyLapsParentModel.getMyLapsChildList().get(0).getLapDate() + ""));
        holder.rankTv.setText("0");
        holder.vehicleNameTv.setText("");
    }

    @Override
    public int getItemCount() {
        return laps_list.size();
    }
    @Override
    public int getItemViewType(int position)
    {

        return 0;
    }

}
