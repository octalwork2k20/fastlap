package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;

import java.util.ArrayList;

/**
 * Created by anandj on 7/17/2017.
 */

public class AwardProfileAdapter extends RecyclerView.Adapter<AwardProfileAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<String> mAwardList;

    public AwardProfileAdapter(Context context, ArrayList<String> awardList) {
        mContext = context;
        mAwardList = awardList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_award_child, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txt_info.setText("3/4 Upload 10 Laps");
    }

    @Override
    public int getItemCount() {
        return mAwardList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_info;

        MyViewHolder(View itemvitem) {
            super(itemvitem);
            txt_info = (TextView) itemvitem.findViewById(R.id.txt_info);
        }
    }
}
