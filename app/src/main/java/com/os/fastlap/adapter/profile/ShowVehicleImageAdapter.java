package com.os.fastlap.adapter.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 8/10/2017.
 */

public class ShowVehicleImageAdapter extends RecyclerView.Adapter<ShowVehicleImageAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<String> mStringArrayList;

    public ShowVehicleImageAdapter(Context context, ArrayList<String> stringArrayList) {
        this.mContext = context;
        this.mStringArrayList = stringArrayList;
    }

    @Override
    public ShowVehicleImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_graage_child_vechicle_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ShowVehicleImageAdapter.MyViewHolder holder, int position) {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mStringArrayList.get(position), holder.imgPagerImage, Util.getImageLoaderOption(mContext));
    }

    @Override
    public int getItemCount() {
        return mStringArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPagerImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgPagerImage = itemView.findViewById(R.id.imgPagerImage);
        }
    }
}
