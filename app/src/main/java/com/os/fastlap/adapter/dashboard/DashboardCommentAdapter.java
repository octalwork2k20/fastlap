package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.beans.dashboardmodals.CommentBeans;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by abhinava on 7/12/2017.
 */

public class DashboardCommentAdapter extends RecyclerView.Adapter<DashboardCommentAdapter.MyViewHolder> {

    DisplayImageOptions options;
    Context context;
    List<CommentBeans> comment_list = new ArrayList<>();
    AuthAPI authAPI;
    CommentInterFace commentInterFace;
    public int pos;

    public interface CommentInterFace {
        void onClickCommentRowReply(int postion);

        void onClickReplyViewCount(int postion);

        void onClickCommentRowLikeCount(int postion);

        void onClickCommentRowLike(int postion, String status);

        void onClickCommentDelete(int postion);

        void onClickCommentEdit(int postion);
    }

    public void setOnItemClickListener(DashboardCommentAdapter.CommentInterFace listener) {
        commentInterFace = listener;
    }

    public DashboardCommentAdapter(List<CommentBeans> comment_list, Context context) {
        this.comment_list = comment_list;
        this.context = context;
        authAPI = new AuthAPI(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_layout_comment_row_selector, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // Receiving side
        byte[] data = Base64.decode(comment_list.get(position).getComment_text(), Base64.NO_WRAP);
        try {
            String text = new String(data, "UTF-8");
            holder.commentRowCommentTv.setText(text);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        holder.commentRowUserNameTv.setText(comment_list.get(position).getComment_user_name());

        holder.commentRowLikeCountTv.setText(comment_list.get(position).getComment_like_count() + "");

        if (Integer.parseInt(comment_list.get(position).getComment_like_count()) > 0) {
            holder.commentRowLikeCountTv.setVisibility(View.VISIBLE);
        } else {
            holder.commentRowLikeCountTv.setText(0 + "");
            holder.commentRowLikeCountTv.setVisibility(View.VISIBLE);
        }

        if (Integer.parseInt(comment_list.get(position).getComment_reply_count()) > 0) {
            holder.replyViewCountTv.setVisibility(View.VISIBLE);
        } else {
            holder.replyViewCountTv.setVisibility(View.GONE);
        }

        if (comment_list.get(position).getComment_like_status().compareTo("0") == 0) {
            holder.commentRowLikeTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
        } else {
            holder.commentRowLikeTv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }


        holder.replyViewCountTv.setText("View " + comment_list.get(position).getComment_reply_count() + " previous replies");
        try {
            holder.commentRowTimeTv.setText(Util.calculateTimeDiffFromNow(comment_list.get(position).getComment_date(), context));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + comment_list.get(position).getComment_user_image(), holder.commetRowUserIv, Util.getImageLoaderOption(context));

        if(comment_list.get(holder.getAdapterPosition()).getComment_user_id().equalsIgnoreCase(MySharedPreferences.getPreferences(context,S.user_id)))
        {
            holder.commentRowInfoIv.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.commentRowInfoIv.setVisibility(View.GONE);
        }

    }

    public int getPosition() {
        return pos;
    }

    public void setPosition(int pos) {
        this.pos = pos;
    }


    @Override
    public int getItemCount() {
        return comment_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView commetRowUserIv;
        private TextViewPlayBold commentRowUserNameTv;
        private ImageView commentRowInfoIv;
        private TextViewPlayRegular commentRowCommentTv;
        private TextViewPlayRegular commentRowTimeTv;
        private TextView commentRowLikeTv;
        private TextView commentRowReplyTv;
        private TextViewPlayRegular commentRowLikeCountTv;
        private TextViewPlayBold replyViewCountTv;

        public MyViewHolder(View itemView) {
            super(itemView);


            commetRowUserIv = (ImageView) itemView.findViewById(R.id.commet_row_user_iv);
            commentRowUserNameTv = (TextViewPlayBold) itemView.findViewById(R.id.comment_row_user_name_tv);
            commentRowInfoIv = (ImageView) itemView.findViewById(R.id.comment_row_info_iv);
            commentRowCommentTv = (TextViewPlayRegular) itemView.findViewById(R.id.comment_row_comment_tv);
            commentRowTimeTv = (TextViewPlayRegular) itemView.findViewById(R.id.comment_row_time_tv);
            commentRowLikeTv = (TextView) itemView.findViewById(R.id.comment_row_like_tv);
            commentRowReplyTv = (TextView) itemView.findViewById(R.id.comment_row_reply_tv);
            commentRowLikeCountTv = (TextViewPlayRegular) itemView.findViewById(R.id.comment_row_like_count_tv);
            replyViewCountTv = (TextViewPlayBold) itemView.findViewById(R.id.reply_view_count_tv);

            commentRowReplyTv.setOnClickListener(this);
            replyViewCountTv.setOnClickListener(this);
            commentRowLikeCountTv.setOnClickListener(this);
            commentRowLikeTv.setOnClickListener(this);
            commentRowInfoIv.setOnClickListener(this);
            commetRowUserIv.setOnClickListener(this);
            commentRowUserNameTv.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.comment_row_reply_tv:
                    commentInterFace.onClickCommentRowReply(MyViewHolder.this.getAdapterPosition());
                    break;
                case R.id.reply_view_count_tv:
                    commentInterFace.onClickReplyViewCount(MyViewHolder.this.getAdapterPosition());
                    break;

                case R.id.comment_row_like_count_tv:
                    commentInterFace.onClickCommentRowLikeCount(MyViewHolder.this.getAdapterPosition());
                    break;

                case R.id.comment_row_like_tv:
                    String status = "0";
                    if (comment_list.get(getAdapterPosition()).getComment_like_status().compareTo("0") == 0) {
                        status = "1";
                    } else {
                        status = "0";
                    }
                    comment_list.get(getAdapterPosition()).setComment_like_status(status);
                    notifyItemChanged(getAdapterPosition());
                    commentInterFace.onClickCommentRowLike(MyViewHolder.this.getAdapterPosition(), status);
                    break;

                case R.id.comment_row_info_iv:
                    //creating a popup menu
                    PopupMenu popup = new PopupMenu(context, MyViewHolder.this.commentRowInfoIv);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.comment_menu);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.edit:
                                    commentInterFace.onClickCommentEdit(getAdapterPosition());
                                    break;
                                case R.id.delete:
                                    commentInterFace.onClickCommentDelete(getAdapterPosition());
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                    break;
                case R.id.commet_row_user_iv:
                case R.id.comment_row_user_name_tv:

                    Intent intent3 = new Intent(context, ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                    bundle.putString(S.user_id, comment_list.get(getAdapterPosition()).getComment_user_id());
                    bundle.putString("status", "0");
                    intent3.putExtras(bundle);
                    context.startActivity(intent3);

                    break;

            }
        }


    }

}
