package com.os.fastlap.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.os.fastlap.R;
import com.os.fastlap.beans.LapTime;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DashboardLapTimeAdapter extends RecyclerView.Adapter<DashboardLapTimeAdapter.ViewHolder> {

    ArrayList<LapTime>arrayList;
    OnItemClick onItemClick;

    public DashboardLapTimeAdapter(ArrayList<LapTime> arrayList,OnItemClick onItemClick) {
        this.arrayList = arrayList;
        this.onItemClick=onItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_lap_time_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        LapTime lapTime=arrayList.get(position);
        holder.mTvTitle.setText(lapTime.getTitle());
        holder.mTvVehicle.setText(lapTime.getLap_vehicle());
        holder.mTvSpeed.setText(new DecimalFormat("##.###").format(Double.parseDouble(lapTime.getLap_speed())));
        holder.mTvLapTime.setText(lapTime.getLap_time());
        holder.lytParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout lytParent;
        TextViewPlayRegular mTvLapTime;
        TextViewPlayRegular mTvSpeed;
        TextViewPlayRegular mTvVehicle;
        TextViewPlayBold mTvTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            lytParent=(RelativeLayout)itemView.findViewById(R.id.lytParent);
            mTvLapTime=(TextViewPlayRegular)itemView.findViewById(R.id.tvLapTimeValue);
            mTvSpeed=(TextViewPlayRegular)itemView.findViewById(R.id.tvMaxSpeedValue);
            mTvTitle=(TextViewPlayBold)itemView.findViewById(R.id.tvTitle);
            mTvVehicle=(TextViewPlayRegular)itemView.findViewById(R.id.tvVehicleValue);
        }
    }

    public interface OnItemClick{
        void onClick(int pos);
    }
}
