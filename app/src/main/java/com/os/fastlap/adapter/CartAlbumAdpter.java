package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.CartAlbumActivity;
import com.os.fastlap.beans.CartAlbumBeans;
import com.os.fastlap.beans.CartAlbumImagesBeans;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anandj on 10/5/2017.
 */

public class CartAlbumAdpter extends RecyclerView.Adapter<CartAlbumAdpter.MyViewHolder> {
    private List<CartAlbumBeans> mAlbumBeansList;
    private Context mContext;

    public CartAlbumAdpter(Context context, List<CartAlbumBeans> cartAlbumBeansList) {
        this.mContext = context;
        this.mAlbumBeansList = cartAlbumBeansList;
    }

    @Override
    public CartAlbumAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.album_my_card_child, parent, false));
    }

    @Override
    public void onBindViewHolder(CartAlbumAdpter.MyViewHolder holder, int position) {
        holder.txt_productsname.setText(mAlbumBeansList.get(position).getAlbumIdBeans().getName());
        holder.txtPrice.setText(mAlbumBeansList.get(position).getAlbumIdBeans().getAlbumPrice());
        holder.txtDate.setText(Util.ConvertDateTimeZone(mAlbumBeansList.get(position).getAlbumIdBeans().getDateOfEvent()));

        holder.cartAlbumImagesBeansArrayList.clear();
        for (int i = 0; i < mAlbumBeansList.get(position).getCartAlbumImagesBeanses().size(); i++) {
            if (i == 0) {
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mAlbumBeansList.get(position).getCartAlbumImagesBeanses().get(i).getFileName(), holder.img_cart_pic, Util.getImageLoaderOption(mContext));
            } else {
                holder.cartAlbumImagesBeansArrayList.add(mAlbumBeansList.get(position).getCartAlbumImagesBeanses().get(i));
            }
        }


        holder.showCartImageAdapter.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mAlbumBeansList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView img_cart_pic, img_delete;
        TextView txt_productsname, txtPrice, txtDate;
        RecyclerView recycler_view;
        ShowCartImageAdapter showCartImageAdapter;
        ArrayList<CartAlbumImagesBeans> cartAlbumImagesBeansArrayList;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_cart_pic = itemView.findViewById(R.id.img_cart_pic);
            txt_productsname = itemView.findViewById(R.id.txt_productsname);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtDate = itemView.findViewById(R.id.txtDate);
            img_delete = itemView.findViewById(R.id.img_delete);
            recycler_view = itemView.findViewById(R.id.recycler_view);
            cartAlbumImagesBeansArrayList = new ArrayList<>();
            showCartImageAdapter = new ShowCartImageAdapter(mContext, cartAlbumImagesBeansArrayList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            recycler_view.setLayoutManager(layoutManager);
            recycler_view.setAdapter(showCartImageAdapter);
            img_delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_delete:
                    if (mContext instanceof CartAlbumActivity)
                        ((CartAlbumActivity) mContext).deleteCartAlbum(getAdapterPosition());
                    break;
            }
        }
    }
}
