package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.BeanMessage;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhinava on 7/12/2017.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    Context context;
    ArrayList<BeanMessage> chat_list = new ArrayList<>();
    ArrayList<BeanMessage> chatListAll = new ArrayList<>();
    Onclick onclick;

    public interface Onclick {
        void onRowClick(int position);
    }

    public void setOnclickListner(Onclick onclickListner) {
        this.onclick = onclickListner;
    }

    public ChatListAdapter(ArrayList<BeanMessage> chat_list, Context context) {
        this.chat_list = chat_list;
        this.chatListAll.addAll(chat_list);
        this.context = context;
    }

    public void addChatListAll(ArrayList<BeanMessage> chat_list) {
        this.chatListAll.clear();
        this.chatListAll.addAll(chat_list);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BeanMessage beanMessage = chat_list.get(position);

        holder.chatUserName.setText(beanMessage.getMessageSenderName());
        holder.chatLastMsg.setText(beanMessage.getMessageText());

        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanMessage.getMessageSenderImage(), holder.userProfileIv, Util.getImageLoaderOption(context));
        if (beanMessage.getUnReadCount() > 0) {
            holder.unreadCountFl.setVisibility(View.VISIBLE);
            holder.unreadCount.setText(beanMessage.getUnReadCount() + "");
        } else {
            holder.unreadCountFl.setVisibility(View.GONE);
            holder.unreadCount.setText(beanMessage.getUnReadCount() + "");
        }

       if(beanMessage.getOnLineStatus()){
           holder.userStatusIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.gps));
       }
       else {
           holder.userStatusIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.gps_red));
       }
        if (beanMessage.getLastSendUserType().compareToIgnoreCase("M") == 0)
        {
            holder.chatTimeTv.setText(Util.parseDateToHMAMy(beanMessage.getMessageDate()));
        } else {
            holder.chatTimeTv.setText(Util.parseDateToHMA(beanMessage.getMessageDate()));
        }

    }
    @Override
    public int getItemCount() {
        return chat_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView userProfileIv;
        private ImageView userStatusIcon;
        private TextView chatUserName;
        private TextView chatLastMsg;
        private TextView chatTimeTv;
        private TextView unreadCount;
        private LinearLayout msg_background;
        private FrameLayout unreadCountFl;

        public MyViewHolder(View itemView) {
            super(itemView);
            msg_background = (LinearLayout) itemView.findViewById(R.id.msg_background);
            userProfileIv = (CircleImageView) itemView.findViewById(R.id.user_profile_iv);
            userStatusIcon = (ImageView) itemView.findViewById(R.id.user_status_icon);
            chatUserName = (TextView) itemView.findViewById(R.id.chat_user_name);
            chatLastMsg = (TextView) itemView.findViewById(R.id.chat_last_msg);
            chatTimeTv = (TextView) itemView.findViewById(R.id.chat_time_tv);
            unreadCount = (TextView) itemView.findViewById(R.id.unread_count);
            unreadCountFl = (FrameLayout) itemView.findViewById(R.id.unread_count_fl);

            msg_background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onclick.onRowClick(getAdapterPosition());
                }
            });
        }
    }

    public void removeItem(int position) {
        chat_list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, chat_list.size());
    }

    // search class
    public void filter(String ch) {
        chat_list.clear();
        if (ch.isEmpty())
            chat_list.addAll(chatListAll);
        else {
            for (BeanMessage wp : chatListAll) {
                if (wp.getMessageSenderName().toUpperCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())) ||
                        wp.getMessageSenderName().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())))
                    chat_list.add(wp);
            }
        }
        notifyDataSetChanged();
    }
}
