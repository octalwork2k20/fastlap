package com.os.fastlap.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.os.fastlap.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by riyazudinp on 6/18/2017.
 */

public class GroupPrepareAdapter extends RecyclerView.Adapter<GroupPrepareAdapter.ViewHolder> {

    private Context context;
    public ArrayList<String> data;
    DisplayImageOptions options;


    public GroupPrepareAdapter(Context context, ArrayList<String> data) {
        this.data = data;
        this.context = context;
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.profile_default);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageForEmptyUri(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageOnFail(new BitmapDrawable(context.getResources(), default_bitmap))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_prepare_row_layout, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView user_image_iv2;
        private CircleImageView user_image_iv;
        private TextView txtUserName;

        ViewHolder(View convertView) {
            super(convertView);
            user_image_iv2 = (CircleImageView) convertView.findViewById(R.id.user_image_iv2);
            user_image_iv = (CircleImageView) convertView.findViewById(R.id.user_image_iv);
            txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
        }

    }


}
