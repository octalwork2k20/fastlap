package com.os.fastlap.adapter.dashboard;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.adapter.profile.ShowVehicleImageAdapter;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 7/14/2017.
 */

public class DashboardVehicleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /*Callback method for list item */
    public interface OnItemClickListener {
        void onItemVehicleList(int position);
    }

    private Context mContext;
    private ArrayList<BeanVehicleList> mArrayList;
    private OnItemClickListener mOnItemClickListener;

    public DashboardVehicleListAdapter(Context context, ArrayList<BeanVehicleList> list_data) {
        this.mContext = context;
        mArrayList = list_data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolderList(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_garage_child_activity, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolderList) {
            ((MyViewHolderList) holder).txt_vechicle_name.setText(mArrayList.get(position).getVehicleBrandIdModal().getVehicleBrandName() + " " +
                    mArrayList.get(position).getVehicleModelIdModal().getVehicleModelName() + " " + mArrayList.get(position).getVehicleVersionIdModal().getVehicleVersionName());
            ((MyViewHolderList) holder).txt_year.setText(mArrayList.get(position).getVehicleYearIdModal().getVehicleYear());
            ((MyViewHolderList) holder).txt_tyre.setText(mArrayList.get(position).getVehicleTyreBrandIdModal().getBrandName() + " " + mArrayList.get(position).getVehicleTyreModelIdModal().getVehichleModelName());


            ((MyViewHolderList) holder).mStringArrayList.clear();
            for(int i=0;i<mArrayList.get(holder.getAdapterPosition()).getImagesList().size();i++)
            {
                if(!mArrayList.get(holder.getAdapterPosition()).getImagesList().get(i).isDefault())
                {
                    if(i==0)
                    {
                        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + mArrayList.get(holder.getAdapterPosition()).getImagesList().get(i).getImage(), ((MyViewHolderList) holder).imgGaragePic, Util.getImageLoaderOption(mContext));
                    }
                    else
                    {
                        ((MyViewHolderList) holder).mStringArrayList.add(mArrayList.get(holder.getAdapterPosition()).getImagesList().get(i).getImage());
                    }

                }
            }
            ((MyViewHolderList) holder).showVehicleImageAdapter.notifyDataSetChanged();
            ((MyViewHolderList) holder).carDetails.setText(mArrayList.get(position).getDescription());

            ((MyViewHolderList) holder).imgDelete.setVisibility(View.GONE);
            ((MyViewHolderList) holder).imgEdit.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return I.TYPE_ITEM;
    }

    private void clickonItem(MyViewHolderList holderList) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onItemVehicleList(holderList.getAdapterPosition());
    }

    public class MyViewHolderList extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RecyclerView recycler_view;
        private ShowVehicleImageAdapter showVehicleImageAdapter;
        private ArrayList<String> mStringArrayList;
        private ImageView img_edit, img_delete;
        private TextView txt_vechicle_name;
        private TextView txt_year;
        private TextView txt_tyre;
        private RelativeLayout parent_rl;
        private RelativeLayout parentRl;
        private ImageView imgGaragePic;
        private LinearLayout llCarDetails;
        private TextViewPlayBold txtVechicleName;
        private ImageView imgDelete;
        private ImageView imgEdit;
        private RelativeLayout uploadedLapsLl;
        private RelativeLayout trackVisitedLl;
        private ImageView imgCal;
        private TextViewPlayRegular txtYear;
        private ImageView imgApline;
        private TextViewPlayRegular txtTyre;
        private LinearLayout postionSpeedLl;
        private ImageView imgTropy;
        private ImageView imgSpeed;
        private RecyclerView recyclerView;
        private HorizontalScrollView horizontalScroll;
        private ImageView imageView;
        private TextViewPlayRegular carDetails;


        public MyViewHolderList(View itemView) {
            super(itemView);
            imgDelete = itemView.findViewById(R.id.img_delete);
            imgGaragePic = (ImageView) itemView.findViewById(R.id.img_garage_pic);
            llCarDetails = (LinearLayout) itemView.findViewById(R.id.ll_car_details);
            imgCal = (ImageView) itemView.findViewById(R.id.img_cal);
            imgApline = (ImageView) itemView.findViewById(R.id.img_apline);
            imgTropy = (ImageView) itemView.findViewById(R.id.img_tropy);
            imgSpeed = (ImageView) itemView.findViewById(R.id.img_speed);
            imgEdit = (ImageView) itemView.findViewById(R.id.img_edit);
            horizontalScroll = (HorizontalScrollView) itemView.findViewById(R.id.horizontal_scroll);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            carDetails = (TextViewPlayRegular) itemView.findViewById(R.id.car_details);
            recycler_view = (RecyclerView) itemView.findViewById(R.id.recycler_view);
            parent_rl = (RelativeLayout) itemView.findViewById(R.id.parent_rl);
            txt_vechicle_name = itemView.findViewById(R.id.txt_vechicle_name);
            txt_year = (TextViewPlayRegular) itemView.findViewById(R.id.txt_year);
            txt_tyre = (TextViewPlayRegular) itemView.findViewById(R.id.txt_tyre);
            uploadedLapsLl = (RelativeLayout) itemView.findViewById(R.id.uploaded_laps_ll);
            trackVisitedLl = (RelativeLayout) itemView.findViewById(R.id.track_visited_ll);
            postionSpeedLl = (LinearLayout) itemView.findViewById(R.id.postion_speed_ll);


            mStringArrayList = new ArrayList<>();
            showVehicleImageAdapter = new ShowVehicleImageAdapter(mContext, mStringArrayList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            recycler_view.setLayoutManager(layoutManager);
            recycler_view.setAdapter(showVehicleImageAdapter);

         //   img_delete.setOnClickListener(this);
           // img_edit.setOnClickListener(this);
            parent_rl.setOnClickListener(this);

          /*  if (mContext instanceof DashboardVehicleListActivity)
            {
                uploadedLapsLl.setVisibility(View.GONE);
                trackVisitedLl.setVisibility(View.GONE);
                postionSpeedLl.setVisibility(View.GONE);
                llCarDetails.setVisibility(View.GONE);
                img_edit.setVisibility(View.GONE);
                img_delete.setVisibility(View.GONE);
                recycler_view.setVisibility(View.GONE);
            }*/
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.parent_rl:
                    clickonItem(this);
                    break;
            }
        }
    }

}
