package com.os.fastlap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.beans.chatmodels.AppUserBean;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by riyazudinp on 6/17/2017.
 */

public class AppUserAdapter extends RecyclerView.Adapter<AppUserAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<AppUserBean> user_list=new ArrayList<>();
    private ArrayList<AppUserBean> user_list_clone=new ArrayList<>();

    public AppUserAdapter(Context context, ArrayList<AppUserBean> data) {
        this.context = context;
        this.user_list = data;
        this.user_list_clone.addAll(user_list);
    }

    @Override
    public AppUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_user_row_layout, parent, false);
        return new AppUserAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppUserAdapter.MyViewHolder holder, int position)
    {
        holder.txtUserName.setText(user_list.get(position).getPersonalInfo().getFirstName()+" "+user_list.get(position).getPersonalInfo().getLastName());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL+ user_list.get(position).getPersonalInfo().getImage(), holder.imgProfile, Util.getImageLoaderOption(context));
    }

    @Override
    public int getItemCount() {
        return user_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imgProfile;
        private TextView txtUserName;

        public MyViewHolder(View itemView) {
            super(itemView);


            txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
            imgProfile = (CircleImageView) itemView.findViewById(R.id.imgProfile);

        }
    }

}
