package com.os.fastlap.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanTrackVersion;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/*
 * Created by riyazudinp on 6/17/2017.
 */

public class TrackVersionSpinnerAdapter extends RecyclerView.Adapter<TrackVersionSpinnerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<BeanTrackVersion.Version> arraylist = new ArrayList<>();
    SpinnerSelectorInterface spinnerSelectorInterface;
    int type;
    Dialog dialog1;

    public TrackVersionSpinnerAdapter(Context context, ArrayList<BeanTrackVersion.Version> data, SpinnerSelectorInterface spinnerSelectorInterface, int type, Dialog dialog1) {
        this.context = context;
        this.arraylist = data;
        this.spinnerSelectorInterface = spinnerSelectorInterface;
        this.type = type;
        this.dialog1 = dialog1;

    }

    @Override
    public TrackVersionSpinnerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spin_row, parent, false);
        return new TrackVersionSpinnerAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TrackVersionSpinnerAdapter.MyViewHolder holder, final int position) {
        holder.text.setText(arraylist.get(position).getVersionName());

        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerSelectorInterface.vehicleTypeName(arraylist.get(position).getVersionName(), arraylist.get(position).getVersionId(), type);
                spinnerSelectorInterface.vehicleTypeName(arraylist.get(position).getVersionName(), arraylist.get(position).getVersionId(), type,position);
                dialog1.cancel();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextViewPlayRegular text;

        public MyViewHolder(View itemView) {
            super(itemView);

            text = (TextViewPlayRegular) itemView.findViewById(R.id.text);

        }
    }

}
