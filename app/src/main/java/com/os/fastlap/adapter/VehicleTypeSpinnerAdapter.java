package com.os.fastlap.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/*
 * Created by riyazudinp on 6/17/2017.
 */

public class VehicleTypeSpinnerAdapter extends RecyclerView.Adapter<VehicleTypeSpinnerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<BeanVehicleType> arraylist = new ArrayList<>();
    SpinnerSelectorInterface spinnerSelectorInterface;
    int type;
    Dialog dialog1;

    public VehicleTypeSpinnerAdapter(Context context, ArrayList<BeanVehicleType> data, SpinnerSelectorInterface spinnerSelectorInterface, int type, Dialog dialog1) {
        this.context = context;
        this.arraylist = data;
        this.spinnerSelectorInterface = spinnerSelectorInterface;
        this.type = type;
        this.dialog1 = dialog1;

    }

    @Override
    public VehicleTypeSpinnerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spin_row, parent, false);
        return new VehicleTypeSpinnerAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final VehicleTypeSpinnerAdapter.MyViewHolder holder, final int position) {
        if (arraylist.get(position).getUnicode() != null && arraylist.get(position).getUnicode().isEmpty())
            holder.text.setText(arraylist.get(position).getName());
        else
            holder.text.setText(Util.getEmoticon(Integer.parseInt(arraylist.get(position).getUnicode().replace("U+", "0x").substring(2), 16)) + " " + arraylist.get(position).getName());

        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerSelectorInterface.vehicleTypeName(arraylist.get(position).getName(), arraylist.get(position).getId(), type);
                spinnerSelectorInterface.vehicleTypeName(arraylist.get(position).getName(), arraylist.get(position).getId(), type, position);
                dialog1.cancel();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextViewPlayRegular text;

        public MyViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);

        }
    }

}
