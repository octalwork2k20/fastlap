package com.os.fastlap.util.constants;

/**
 * Created by anandj on 4/12/2017.
 */

public class I {
    public static final int SPLASH_DISPLAY_LENGTH = 500;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final int DASHBOARD_SCREEN = 1;
    public static final int CHAT_SCREEN = 2;
    public static final int RECORD_SCREEN = 3;
    public static final int NOTIFICATION_SCREEN = 4;
    public static final int MENU_SCREEN = 5;

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;

    public static final int LISTVIEW = 0;
    public static final int GRIDVIEW = 1;

    public static final int LAP_LAYOUT = 0;
    public static final int USER_LAYOUT = 1;


    public static final int SETTING_BLOCK_USER_LIST = 0;
    public static final int SETTING_MANAGE_DEVICE_LIST = 1;

    public static final int RUNTIME_PERMISSION_CHECK = 1;
    public static final int MOBILE_VERIFY = 1;
    public static final int MOBILE_VERIFY_NOT = 0;


    public static final int VEHICLE_TYPE = 0;
    public static final int VEHICLE_BRAND = 1;
    public static final int VEHICLE_MODEL = 2;
    public static final int VEHICLE_VERSION = 3;
    public static final int VEHICLE_YEAR = 4;
    public static final int TYRE_BRAND = 5;
    public static final int TYRE_MODEL = 6;
    public static final int PRIVACY = 7;
    public static final int GROUPTYPE = 8;
    public static final int COUNTRYNAME = 9;
    public static final int VEHICLE_YEAR2 = 10;
    public static final int TRACK_CONDITION = 11;

    public static final int CLOTH_BRAND = 0;
    public static final int CLOTH_MODEL = 1;
    public static final int CLOTH_YEAR = 2;
    public static final int CLOTH_COLOR = 3;
    public static final int CLOTH_VEHICLE = 4;


    public static final int FEELING = 3;
    public static final int WEATHER = 4;

    public static final int LIMIT = 4;


    public static final int POSTLIKE = 0;
    public static final int COMMENTLIKE = 1;
    public static final int REPLYLIKE = 2;
    public static final int ALBUMLIKE = 3;
    public static final int ALBUMCOMMENTLIKE = 4;
    public static final int ALBUMREPLYLIKE = 5;
    public static final int ADLIKE = 6;

    public static final int ITEM = 0;
    public static final int LOADING = 1;


    public static final int SIGNEDUPEVENT = 5;
    public static final int FAVORITEEVENT = 3;
    public static final int FRIENDSEVENT = 2;
    public static final int ALLEVENT = 1;


    public static final int PROFILE = 0;
    public static final int COVER = 1;

    public static final int PROFILEEVENT = 0;
    public static final int TRACKEVENT = 1;
    public static final int GROUPEVENT = 2;


    public static final int PHOTOGALLERY = 1;
    public static final int VIDEOGALLERY = 2;


    public static final int USERGALLERY = 0;
    public static final int GROUPGALLERY = 1;
    public static final int TRACKGALLERY = 2;


    public static final int ALL = 0;
    public static final int COUNTRY = 1;


    public static final int MYALBUM = 1;
    public static final int FRIENDSALBUM = 2;
    public static final int ALLALBUM = 3;


    public static final int PAYMENT_SUBSCRIPTION = 0;
    public static final int ALBUMPAYMET = 1;
    public static final int EVENT_TICKET_PAYMET = 2;


    public static final int COMMENT_ACTIVITY_RESULT_CODE = 101;
    public static final int ADCOMMENT_ACTIVITY_RESULT_CODE = 102;


    public static final int SELECT_FILE_IMAGE_VIDEO = 100;

    public static final int ADDPOSTACTIVTYCALLBACK = 1;


    public static final int PICK_FILE_REQUEST = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int MEDIA_TYPE_IMAGE = 3;
    public static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;

    public static final int ALBUM_ACTIVITY_REQUEST = 101;

    public static final int FAVORITETRACKS = 1;
    public static final int ALLTRACKS = 2;
    public static final int COUNTRYTRACKS = 3;


    public static final int DASHBOARDSEARCH = 1;
    public static final int MYLAPSSEARCH = 2;
    public static final int GROUPSSEARCH = 3;
    public static final int EVENTSSEARCH = 4;
    public static final int GALLERYSEARCH = 5;
    public static final int EXPLORESEARCH = 6;
}



