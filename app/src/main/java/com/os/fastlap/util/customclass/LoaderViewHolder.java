package com.os.fastlap.util.customclass;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.os.fastlap.R;

public class LoaderViewHolder extends RecyclerView.ViewHolder {


    ProgressBar mProgressBar;

    public LoaderViewHolder(View itemView) {
        super(itemView);

        mProgressBar=(ProgressBar)itemView.findViewById(R.id.progressbar);

    }
}