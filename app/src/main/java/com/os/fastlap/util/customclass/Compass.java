package com.os.fastlap.util.customclass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by abhinava on 1/12/2018.
 */

public class Compass extends View {

    private float directionX;
    private float directionY;

    public Compass(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public Compass(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public Compass(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(
                MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int w = getMeasuredWidth();
        int h = getMeasuredHeight();
        int r;
        if (w > h) {
            r = h / 2;
        } else {
            r = w / 2;
        }
        r = 8;

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.LTGRAY);

       /* canvas.drawCircle(w / 2, h / 2, r, paint);
        //canvas.drawLine(w/2, 0, w/2, h, paint);
       // canvas.drawLine(0, h/2, w, h/2, paint);

        paint.setColor(Color.RED);
        canvas.drawLine(
                w / 2,
                h / 2,
                (float) (w / 2 + r * Math.sin(-direction)),
                (float) (h / 2 - r * Math.cos(-direction)),
                paint);*/

      /*  canvas.drawCircle(
                (float) (w / 2 + 15 * Math.sin(-direction)),
                (float) (h / 2 - 15 * Math.cos(-direction)),r,paint);*/
        canvas.drawCircle(
                (float) ((w / 2) + (directionX * 2)),
                (float) ((h / 2) + (-directionY * 2)), r, paint);

    }

    public void update(float x, float y) {
        directionX = x;
        directionY = y;
        invalidate();
    }

}

