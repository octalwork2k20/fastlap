package com.os.fastlap.util.customclass;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;

/**
 * Created by anandj on 7/21/2017.
 */

@SuppressLint("AppCompatCustomView")
public class RadioButtonPlayRegular extends AppCompatRadioButton {

    public RadioButtonPlayRegular(Context context) {
        super(context);
        init();
    }

    public RadioButtonPlayRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadioButtonPlayRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "font/play_regular.ttf"));
    }

}
