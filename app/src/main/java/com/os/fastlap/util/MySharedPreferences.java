package com.os.fastlap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.os.fastlap.util.constants.S;


/**
 * Created by Anand Jain on 7/17/2015.
 */
public class MySharedPreferences {

    public static void clearAll(@NonNull Context context) {
        SharedPreferences sp = context.getSharedPreferences(S.fastlap_sharedpreferencename, Context.MODE_PRIVATE);
        sp.edit().clear().apply();
    }

    public static void setPreferences(@NonNull Context context, String unit, String pref_name) {
        SharedPreferences sp = context.getSharedPreferences(S.fastlap_sharedpreferencename, Context.MODE_PRIVATE);
        if (!unit.isEmpty())
            sp.edit().putString(pref_name, unit).apply();
    }

    public static void setBooleanPreferences(@NonNull Context context, boolean unit, String pref_name) {
        SharedPreferences sp = context.getSharedPreferences(S.fastlap_sharedpreferencename, Context.MODE_PRIVATE);
        sp.edit().putBoolean(pref_name, unit).apply();

    }
    @Nullable
    public static boolean getBooleanPreferences(@NonNull Context context, String pref_name) {
        SharedPreferences sp = context.getSharedPreferences(S.fastlap_sharedpreferencename, Context.MODE_PRIVATE);
        return sp.getBoolean(pref_name, false);
    }

    @Nullable
    public static String getPreferences(@NonNull Context context, String pref_name) {
        SharedPreferences sp = context.getSharedPreferences(S.fastlap_sharedpreferencename, Context.MODE_PRIVATE);
        return sp.getString(pref_name, "");
    }
}
