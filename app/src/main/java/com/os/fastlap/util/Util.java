package com.os.fastlap.util;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.os.fastlap.R;
import com.os.fastlap.activity.AlbumCommentActivity;
import com.os.fastlap.activity.CartAlbumActivity;
import com.os.fastlap.activity.ChatListActivity;
import com.os.fastlap.activity.DashboardActivity;
import com.os.fastlap.activity.GalleryActivity;
import com.os.fastlap.activity.NotificationListActivity;
import com.os.fastlap.activity.ReplyAlbumActivity;
import com.os.fastlap.activity.SettingMenuActivity;
import com.os.fastlap.activity.UploadLapTimeActivity;
import com.os.fastlap.activity.community.EventTicketPurchase;
import com.os.fastlap.activity.dashboard.AdCommentActivity;
import com.os.fastlap.activity.dashboard.CommentActivity;
import com.os.fastlap.activity.dashboard.ReplyActivity;
import com.os.fastlap.activity.group.CreateGroupActivity;
import com.os.fastlap.activity.group.GroupDiaryActivity;
import com.os.fastlap.activity.payment.AddCardDetail;
import com.os.fastlap.activity.profile.AddAboutInfoActivity;
import com.os.fastlap.activity.profile.AddAlbumActivity;
import com.os.fastlap.activity.profile.AddClothesActivity;
import com.os.fastlap.activity.profile.AddVehicleActivity;
import com.os.fastlap.activity.profile.DiaryActivity;
import com.os.fastlap.activity.profile.GarageProfileActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.activity.track.TrackDiaryActivity;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.apache.poi.util.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static java.lang.Float.parseFloat;
import static me.crosswall.lib.coverflow.core.LinkageCoverTransformer.TAG;

/**
 * Created by abhinava on 7/10/2017.
 */

public class Util {
    public static int IMAGE_MAX_SIZE = 1024;
    public static LruCache<String, Bitmap> mMemoryCache;
    public  static String path ="/storage/emulated/0/Android/data/com.os.fastlap/files";
    public static String GpsBroadcastName="GPS_BROADCAST";
    public final static int clickDeley = 800; //ms
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static Context context;




    public static String setTimeAgo(Context ctx, String time) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);// 2017-11-14T10:walkthrough_one:40.095Z
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setLenient(false);

        Date date = new Date();
        TimeZone timeZone = TimeZone.getDefault();

        //   Date prevTime = sdf.parse(time.replaceAll("Z$", "+0000"));
        Date prevTime = sdf.parse(time);
        sdf.setTimeZone(timeZone);
        long prevMillisecond = prevTime.getTime();

        context = ctx;
        if (prevMillisecond < 1000000000000L) {

            prevMillisecond *= 1000;
        }
        long now = getCurrentTime();

        if (prevMillisecond > now || prevMillisecond <= 0) {
            return "just now";
        }

        final long diff = now - prevMillisecond;

        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }
    public static String method(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
    private static long getCurrentTime() {
        return new Date().getTime();
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static String getEmojiByUnicode(String unicode) {
        //int uni = 0+1F601;//Integer.parseInt(unicode.replace("U+","0x"));
        return Html.fromHtml("\\xF0\\x9F\\x98\\x81").toString();
    }
    public static String getEmoticon(int originalUnicode) {
        return new String(Character.toChars(originalUnicode));
    }

    public static DisplayImageOptions getImageLoaderOption(Context context) {
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.profile_default);
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageForEmptyUri(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageOnFail(new BitmapDrawable(context.getResources(), default_bitmap))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static DisplayImageOptions getImageLoaderOptionDefaultNull(Context context) {
        Bitmap default_bitmap = BitmapFactory.decodeResource(context.getResources(), 0);
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageForEmptyUri(new BitmapDrawable(context.getResources(), default_bitmap))
                .showImageOnFail(new BitmapDrawable(context.getResources(), default_bitmap))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static String isImageOrVideoFile(String path) {
        String mime = "";
        String mimeType = URLConnection.guessContentTypeFromName(path);
        if (mimeType != null && mimeType.startsWith("image"))
            mime = S.image;
        else if (mimeType != null && mimeType.startsWith("video"))
            mime = S.video;
        return mime;
    }

    public static Bitmap createVideoThumbnail(String filePath) {
        return ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND);
    }

    public static String getGalleryPath(Context context) {
        return Environment.getExternalStorageDirectory() + "";


    }


    public static String getFbProfileImageUrl(String id) {
        return "https://graph.facebook.com/" + id + "/picture?width=200&height=150";
    }

    public static boolean isValidEmail(String target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        //String[] ema_arr = new String[5];
        // ema_arr[0] =
    }


    public static void changeTabsFont(TabLayout mTabHost, Context context) {
        ViewGroup vg = (ViewGroup) mTabHost.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(context.getAssets(), "font/play_regular.ttf"));
                }
            }
        }
    }


    public static JSONObject getDeviceInformation() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("SERIAL", Build.SERIAL);
        jsonObject.put("MODEL", Build.MODEL);
        jsonObject.put("Manufacture", Build.MANUFACTURER);
        jsonObject.put("brand", Build.BRAND);
        jsonObject.put("Version Code", Build.VERSION.RELEASE);

        return jsonObject;
    }

    public static String getDeviceSerial(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            }
            return Build.getSerial();
        } else
            return Build.SERIAL;
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    public static String getDeviceManufacture() {
        return Build.MANUFACTURER;
    }

    public static String getDeviceBrand() {
        return Build.BRAND;
    }

    public static String getDeviceVersionCode() {
        return Build.VERSION.RELEASE;
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Za-z]).{6,20}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static String getDeviceToken(Context context) {
        String device_token = "";
        if (MySharedPreferences.getPreferences(context, S.user_device_token) == null || MySharedPreferences.getPreferences(context, S.user_device_token).isEmpty()) {
            if (FirebaseInstanceId.getInstance().getToken() != null)
                device_token = FirebaseInstanceId.getInstance().getToken();
        }
        else{
            device_token = MySharedPreferences.getPreferences(context, S.user_device_token);

        }
        Log.e("tag","device token"+device_token);

        return device_token;
    }

    public static void saveDataInLocalStorage(JSONObject jsonObject1, Context context) throws JSONException {

        MySharedPreferences.setPreferences(context, jsonObject1.getString(S._id), S.user_id);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.username), S.username);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.email), S.email);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.email_verify), S.email_verify);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.mobile_verify), S.mobile_verify);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.otp), S.otp);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.deviceToken), S.deviceToken);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.deviceType), S.deviceType);
        MySharedPreferences.setPreferences(context, jsonObject1.getString(S.role), S.role);

        //JSONObject deviceInfo = jsonObject1.getJSONObject(S.device_info);
        JSONObject addressJson = jsonObject1.getJSONObject(S.address);
        MySharedPreferences.setPreferences(context, addressJson.getString(S.latitude_response), S.latitude_response);
        MySharedPreferences.setPreferences(context, addressJson.getString(S.longitude_response), S.longitude_response);
        MySharedPreferences.setPreferences(context, addressJson.getString(S.nationality_response), S.nationality_response);
        MySharedPreferences.setPreferences(context, addressJson.getString(S.location_response), S.location_response);

        JSONObject personalInfo = jsonObject1.getJSONObject(S.personalInfo);
        MySharedPreferences.setPreferences(context, personalInfo.getString(S.firstName_response), S.firstName_response);
        MySharedPreferences.setPreferences(context, personalInfo.getString(S.lastName_response), S.lastName_response);
        MySharedPreferences.setPreferences(context, personalInfo.getString(S.voucherCode_response), S.voucherCode_response);
        MySharedPreferences.setPreferences(context, personalInfo.getString(S.dateOfBirth_response), S.dateOfBirth_response);
        MySharedPreferences.setPreferences(context, personalInfo.getString(S.mobileNumber_response), S.mobileNumber_response);

        MySharedPreferences.setPreferences(context, personalInfo.getString(S.language_response), S.language_response);
        MySharedPreferences.setPreferences(context, personalInfo.getString(S.image), S.image);
        MySharedPreferences.setPreferences(context, personalInfo.getString(S.coverImage), S.coverImage);

        // Util.showToast(context, msg);
        Util.startNewActivity((Activity) context, DashboardActivity.class, false);
    }

    public static String getUniquePassword() {

        // Using numeric values
        String numbers = "0123456789";

        // Using random method
        Random rndm_method = new Random();

        char[] otp = new char[6];

        for (int i = 0; i < 6; i++) {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            otp[i] =
                    numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return new String(otp);
    }

    public static String getLatitude() {
        String lat = "";
        if (FastLapApplication.location != null)
            lat = String.valueOf(FastLapApplication.location.getLatitude());

        return lat;
    }

    public static String getLongitude() {
        String lng = "";
        if (FastLapApplication.location != null)
            lng = String.valueOf(FastLapApplication.location.getLongitude());

        return lng;
    }

    public static String getuniqueUserName(String fName, String lName) {
        // will generated a random number
        // between 10- 100000
        Random r = new Random();
        int num = r.nextInt(10000 - 10) + 10;
        return fName + lName + num;
    }

    public static void showSnackBar(View v, String msg) {
        Snackbar snackbar = Snackbar
                .make(v, msg, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void startNewActivity(Activity context, Class c, boolean animation) {
        context.startActivity(new Intent(context, c));
        if (animation)
            context.overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
    }

    public static String getLocale(Context context) {
        return context.getResources().getConfiguration().locale.getCountry();
    }

    // set filter in place picker to search only show certain country cities
    public static AutocompleteFilter getAutoCompleteFilter(Context context) {
        return new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();
    }

    public static void showAlertDialog(Context context, String title, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.toast_layout);
        dialog.setCancelable(false);
        TextViewPlayBold titleTv;
        TextViewPlayRegular messageTv;
        TextViewPlayBold okTv;

        titleTv = (TextViewPlayBold) dialog.findViewById(R.id.title_tv);
        messageTv = (TextViewPlayRegular) dialog.findViewById(R.id.message_tv);
        okTv = (TextViewPlayBold) dialog.findViewById(R.id.ok_tv);

        titleTv.setText(title);
        messageTv.setText(message);

        okTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
        dialog.show();
    }

    public static void showAlertDialogWithAction(final Context context, String title, final String message, final String apiName) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.toast_layout);
        dialog.setCancelable(false);
        TextViewPlayBold titleTv;
        TextViewPlayRegular messageTv;
        TextViewPlayBold okTv;

        titleTv = (TextViewPlayBold) dialog.findViewById(R.id.title_tv);
        messageTv = (TextViewPlayRegular) dialog.findViewById(R.id.message_tv);
        okTv = (TextViewPlayBold) dialog.findViewById(R.id.ok_tv);

        titleTv.setText(title);
        messageTv.setText(message);

        okTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof AddVehicleActivity)
                    ((AddVehicleActivity) context).onBackPressed();
                else if (context instanceof AddClothesActivity)
                    ((AddClothesActivity) context).onBackPressed();
                else if (context instanceof GarageProfileActivity)
                    ((GarageProfileActivity) context).getVehicleList();
                else if (context instanceof AddAboutInfoActivity)
                    ((AddAboutInfoActivity) context).onBackPressed();
                else if (context instanceof SettingMenuActivity) {
                    if (apiName.equalsIgnoreCase(S.deviceDisconnectById_api))
                        ((SettingMenuActivity) context).callbackDisconnectedResponse();
                    else if (apiName.equalsIgnoreCase(S.deviceDisconnect_api))
                        ((SettingMenuActivity) context).callbackDisconnectedAllDeviceResponse();
                    else if (apiName.equalsIgnoreCase(S.userBlockStatus_api))
                        ((SettingMenuActivity) context).callbackUserBlockResponse();
                } else if (context instanceof ProfileActivity)
                    ((ProfileActivity) context).callbackFromSuccessDialog();
               /* else if (context instanceof AddPostActivity)
                    ((AddPostActivity) context).postSaveResponseCallback();*/
                else if (context instanceof CommentActivity)
                    ((CommentActivity) context).postCommentListing();
                else if (context instanceof CreateGroupActivity)
                    ((CreateGroupActivity) context).onBackPressed();
                else if (context instanceof AddCardDetail) {
                    Intent returnIntent = new Intent();
                    ((Activity) context).setResult(Activity.RESULT_OK, returnIntent);
                    ((Activity) context).finish();

                } else if (context instanceof CartAlbumActivity) {
                    if (apiName.equalsIgnoreCase(S.removeCartAlbum_api))
                        ((CartAlbumActivity) context).callViewCartAblum();
                    else if (apiName.equalsIgnoreCase(S.albumCheckout_api))
                        ((CartAlbumActivity) context).finish();
                } else if (context instanceof UploadLapTimeActivity) {
                    if (apiName.compareToIgnoreCase("5") == 0) {
                        ((UploadLapTimeActivity) context).lapNotFound();
                    } else if (apiName.compareToIgnoreCase("6") == 0) {
                        ((UploadLapTimeActivity) context).finish();
                        Intent intent = new Intent(context, DashboardActivity.class);
                        context.startActivity(intent);
                    } else {
                        ((UploadLapTimeActivity) context).trackNotfound();
                    }

                } else if (apiName.equalsIgnoreCase(S.removeNotification_api)) {
                    ((NotificationListActivity) context).callNotificationListing(true);
                } else if (context instanceof DashboardActivity) {
                    ((DashboardActivity) context).startPostListingFirstPosition();

                } else if (context instanceof EventTicketPurchase) {
                    ((EventTicketPurchase) context).goToEvent();
                } else if (context instanceof AddAlbumActivity) {
                    if (apiName.equalsIgnoreCase(S.addalbum))
                        ((AddAlbumActivity) context).addAlbumResponseCallback();
                    else if (apiName.equalsIgnoreCase(S.updateAlbum_api))
                        ((AddAlbumActivity) context).editAlbumResponseCallback();
                }


                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
        dialog.show();
    }

    public static void showAlertDialogWithTwoButton(final Context context, final String message, String positive_button, String negative_button, final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.setCancelable(false);
        TextView txt_msg, cancel_btn, logout_btn;
        txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        cancel_btn = (TextView) dialog.findViewById(R.id.cancel_btn);
        logout_btn = (TextView) dialog.findViewById(R.id.logout_btn);
        txt_msg.setText(message);

        cancel_btn.setText(positive_button);
        logout_btn.setText(negative_button);

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof GarageProfileActivity)
                    ((GarageProfileActivity) context).callDeleteVechicleListItem(position);
                else if (context instanceof SettingMenuActivity)
                    ((SettingMenuActivity) context).accountDeactivate();
                else if (context instanceof CommentActivity)
                    ((CommentActivity) context).callDeleteComment(position);
                else if (context instanceof AdCommentActivity)
                    ((AdCommentActivity) context).callDeleteComment(position);
                else if (context instanceof ReplyActivity)
                    ((ReplyActivity) context).callDeleteReply(position);
                else if (context instanceof DashboardActivity) {
                    if (message.equalsIgnoreCase(context.getString(R.string.are_you_sure_you_want_to_delete)))
                        ((DashboardActivity) context).onClickOnDelete(position);
                    else
                        ((DashboardActivity) context).onClickOnshare(position);
                } else if (context instanceof GroupDiaryActivity) {
                    if (message.equalsIgnoreCase(context.getString(R.string.are_you_sure_you_want_to_delete)))
                        ((GroupDiaryActivity) context).onClickOnDelete(position);
                    else
                        ((GroupDiaryActivity) context).onClickOnshare(position);
                } else if (context instanceof TrackDiaryActivity) {
                    if (message.equalsIgnoreCase(context.getString(R.string.are_you_sure_you_want_to_delete)))
                        ((TrackDiaryActivity) context).onClickOnDelete(position);
                    else
                        ((TrackDiaryActivity) context).onClickOnshare(position);
                } else if (context instanceof DiaryActivity) {
                    if (message.equalsIgnoreCase(context.getString(R.string.are_you_sure_you_want_to_delete)))
                        ((DiaryActivity) context).onClickOnDelete(position);
                    else
                        ((DiaryActivity) context).onClickOnshare(position);
                } else if (context instanceof ChatListActivity)
                    ((ChatListActivity) context).removeChatSpecificUser(position);
                else if (context instanceof AlbumCommentActivity)
                    ((AlbumCommentActivity) context).callDeleteComment(position);
                else if (context instanceof ReplyAlbumActivity)
                    ((ReplyAlbumActivity) context).callDeleteReply(position);

                dialog.dismiss();
            }
        });


        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
        dialog.show();
    }

    public static void showAlertDialogWithTwoButtonAlbum(final Context context, final String message, String positive_button, String negative_button, final String albumId) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.setCancelable(false);
        TextView txt_msg, cancel_btn, logout_btn;
        txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        cancel_btn = (TextView) dialog.findViewById(R.id.cancel_btn);
        logout_btn = (TextView) dialog.findViewById(R.id.logout_btn);
        txt_msg.setText(message);

        cancel_btn.setText(positive_button);
        logout_btn.setText(negative_button);

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof GalleryActivity)
                    ((GalleryActivity) context).callDeleteAlbum(albumId);
                dialog.dismiss();
            }
        });


        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
        dialog.show();
    }


    public static void checkGPSStatus(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //  buildAlertMessageNoGps(context);
        }
    }

    public static void hideKeyboard(View view, Context context) {
        if (view != null) {
            ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static String convertRetrofitResponce(@NonNull Response<ResponseBody> responce) {
        try {

            InputStream inputStream = responce.body().byteStream();

            BufferedReader bR = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            StringBuilder responseStrBuilder = new StringBuilder();
            while ((line = bR.readLine()) != null) {
                responseStrBuilder.append(line);
            }
            inputStream.close();
            Log.e("tag","responce"+responseStrBuilder.toString());
            return responseStrBuilder.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap getBitmap(Uri uri, Context mContext) {
        InputStream in = null;
        Bitmap returnedBitmap = null;
        ContentResolver mContentResolver;
        try {
            mContentResolver = mContext.getContentResolver();
//            file:///storage/emulated/0/Pictures/aviary-image-1465806238409.jpeg
           /* if(uri.getPath().contains("file:///")) {*/
            in = mContentResolver.openInputStream(uri);
           /* }
            else{
                String file_append="file:///";
                in = mContentResolver.openInputStream(Uri.parse(file_append+uri));
            }*/
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            /*if(uri.getPath().contains("file:///")) {*/
            in = mContentResolver.openInputStream(uri);
           /* }
            else{
                String file_append="file:///";
                in = mContentResolver.openInputStream(Uri.parse(file_append+uri));
//                in = mContentResolver.openInputStream(Uri.parse(file_append+uri));
            }
*/
            Bitmap bitmap = BitmapFactory.decodeStream(in, null, o2);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            in.close();

            //First check
            ExifInterface ei = new ExifInterface(uri.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    returnedBitmap = rotateImage(bitmap, 90);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    returnedBitmap = rotateImage(bitmap, 180);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    returnedBitmap = rotateImage(bitmap, 270);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                default:
                    returnedBitmap = bitmap;
            }
            return returnedBitmap;
        } catch (FileNotFoundException e) {
            //  L.e(e);
        } catch (IOException e) {
            //L.e(e);
        }
        return null;
    }

    public static Uri getImageUri(String path) {
        try {
            return Uri.fromFile(new File(path));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static String ConvertDate(String datea) {
        String convert_date = "";
        try {
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dt.parse(datea);
            SimpleDateFormat dt1 = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat dt2 = new SimpleDateFormat("dd-MM-yyyy");
            convert_date = dt1.format(date) + " on " + dt2.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convert_date;
    }

    public static String changeTimeFormmat(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = sdf.parse(time.replaceAll("Z$", "+0000"));
        String formattedTime = output.format(d);
        return formattedTime;
    }

    public static String newCalculatedTime(String time, Context context) throws ParseException {
        //   time = changeTimeFormmat(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        //  sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setLenient(false);
        Date prevTime = sdf.parse(time);


        Date date = new Date();

        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar c = Calendar.getInstance(timeZone);
        date = c.getTime();
        //sdf.setTimeZone(timeZone);


        // Date prevTime = sdf.parse(time.replaceAll("Z$", "+0000"));
        long prevMillisecond = prevTime.getTime();
        long diff = date.getTime() - prevMillisecond;
        String hms = String.format("%02d hr :%02d min :%02d sec", TimeUnit.MILLISECONDS.toHours(diff),
                TimeUnit.MILLISECONDS.toMinutes(diff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff)),
                TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff)));

        String valToReturn;
        //show only seconds
        if (diff <= 1000 * 60) {
            long val = diff / 1000;
            valToReturn = val + " Sec";
        }
        //show only minutes
        else if (diff <= 1000 * 60 * 60) {
            long val = diff / (1000 * 60);
            valToReturn = val + " Min";
        }

        //show only hours
        else {
            long val = diff / (1000 * 60 * 60);
            if (val <= 24)
                valToReturn = val + " Hours";
            else {
                // show only days
                val = val / 24;
                if (val <= 7)
                    valToReturn = val + " Days";
                    // when days are more than 7 show only date
                else
                    return ConvertDateTimeZone(time);
            }
        }

        Log.i("Utils.java", "Time diff is " + hms);
        return valToReturn + " " + context.getString(R.string.ago);
    }

    public static String calculateTimeDiffFromNow(String time, Context context) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);// 2017-11-14T10:walkthrough_one:40.095Z
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setLenient(false);

        Date date = new Date();
        TimeZone timeZone = TimeZone.getDefault();

        //   Date prevTime = sdf.parse(time.replaceAll("Z$", "+0000"));
        Date prevTime = sdf.parse(time);
        sdf.setTimeZone(timeZone);
        long prevMillisecond = prevTime.getTime();
//        long pm = prevMillisecond + 3600000;
        long diff = date.getTime() - prevMillisecond;
        String hms = String.format("%02d hr :%02d min :%02d sec", TimeUnit.MILLISECONDS.toHours(diff),
                TimeUnit.MILLISECONDS.toMinutes(diff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff)),
                TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff)));

        String valToReturn;


        //show only seconds
        if (diff <= 1000 * 60) {
            long val = diff / 1000;
            //if(val)
            //valToReturn = val + " Sec";
            valToReturn = context.getString(R.string.just_now);
        }
        //show only minutes
        else if (diff <= 1000 * 60 * 60) {
            long val = diff / (1000 * 60);
            valToReturn = val + " Min";
        }

        //show only hours
        else {
            long val = diff / (1000 * 60 * 60);
            val = val / 24;
            if (val <= 7)
                valToReturn = val + " Days";
                // when days are more than 7 show only date
            else
                return ConvertDateTimeZone(time);
            /*if (val <= 24)
                valToReturn = val + " Hours";
            else {
                // show only days
                val = val / 24;
                if (val <= 7)
                    valToReturn = val + " Days";
                    // when days are more than 7 show only date
                else
                    return ConvertDateTimeZone(time);
            }*/
        }

        Log.i("Utils.java", "Time diff is " + hms);
        // valToReturn = valToReturn.equalsIgnoreCase(context.getString(R.string.just_now)) ?valToReturn:valToReturn + " " + context.getString(R.string.ago);
        if (!valToReturn.equalsIgnoreCase(context.getString(R.string.just_now)))
            return valToReturn + " " + "Ago";
        else
            return valToReturn;
    }

    public static String DisplayAmount(String amount) {
        double i2 = Double.parseDouble(amount);
        String amt = new DecimalFormat("0.00").format(i2);

        return amt;
    }

    public static String ConvertDateTimeZone(String datea) {
        String convert_date = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);// 2017-11-14T10:walkthrough_one:40.095Z
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            sdf.setLenient(false);

            Date prevTime = sdf.parse(datea);

            SimpleDateFormat dt1 = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat dt2 = new SimpleDateFormat("dd-MM-yyyy");

            convert_date = dt1.format(prevTime) + " on " + dt2.format(prevTime);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convert_date;
    }

    public static String ConvertDateTimeZoneDate(String datea) {
        String convert_date = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);// 2017-11-14T10:walkthrough_one:40.095Z
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            sdf.setLenient(false);

            Date prevTime = sdf.parse(datea);

            SimpleDateFormat dt2 = new SimpleDateFormat("dd-MMM-yyyy");

            convert_date = dt2.format(prevTime);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convert_date;
    }

    public static String ConvertDateTimeZoneDateYear(String datea) {
        String convert_date = "";
        try {

            String string = datea;
            Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(string.replaceAll("Z$", "+0000"));

            SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MM-dd");

            convert_date = dt2.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convert_date;
    }

    public static String ConvertDateTimeZoneDateMonth(String datea) {
        String convert_date = "";
        try {

            String string = datea;
            Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(string.replaceAll("Z$", "+0000"));

            SimpleDateFormat dt2 = new SimpleDateFormat("dd MMM");

            convert_date = dt2.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convert_date;
    }

    public static String ConvertDateTimeZoneTime(String datea) {
        String convert_date = "";
        try {

            String string = datea;
            Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(string.replaceAll("Z$", "+0000"));

            SimpleDateFormat dt2 = new SimpleDateFormat("HH:mm");

            convert_date = dt2.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convert_date;
    }

    public static String ConvertMiliSecondToSecond(String mili) {
        long time = Long.parseLong(mili);

        return (new SimpleDateFormat("mm:ss.SSS")).format(new Date(time));
    }

    public static String ConvertMiliSecondToDate(String mili) {
        long time = Long.parseLong(mili);

        return (new SimpleDateFormat("yyyy-MM-dd")).format(new Date(time));
    }

    public static String compareDate2(String d) {
        Calendar cal = Calendar.getInstance();
        Date date1 = null, date2 = null;
        String time = "Today";
        SimpleDateFormat originalFormat = new SimpleDateFormat("MMMM dd, yyyy");
        try {
            Calendar cal2 = Calendar.getInstance();
            cal2.setTimeInMillis(Long.parseLong(d));
            date1 = cal2.getTime();

            cal.setTimeInMillis(System.currentTimeMillis());
            date2 = cal.getTime();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        long difference = date2.getTime() - date1.getTime();
        int days = (int) (difference / (1000 * 60 * 60 * 24));
        int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
        hours = (hours < 0 ? -hours : hours);
        min = (min < 0 ? -min : min);
        //Log.i("======= Hours"," :: "+hours);
        if (days == 0) {
            time = "Today";
        } else if (days == 1) {
            time = "Yesterday";
        } else {
            time = originalFormat.format(date1);
        }
        return time;
    }

    public static String parseDateToHMA(String time) {
        double itemDouble = Double.parseDouble(time);
        long itemLong = (long) (itemDouble * 1000);

        /*time = time.replace(".", "");

        if(time.contains("E"))
        {
            time=time.substring(0,(time.indexOf("E")-1));
        }*/

        String outputPattern = "h:mm a";
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            final Calendar cal = Calendar.getInstance();
            // cal.setTimeInMillis(Long.parseLong(time));
            cal.setTimeInMillis(itemLong);
            date = cal.getTime();
            str = outputFormat.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateToHMAMy(String time) {
        /*double itemDouble = Double.parseDouble(time);
        long itemLong = (long) (itemDouble * 1000);

        *//*time = time.replace(".", "");

        if(time.contains("E"))
        {
            time=time.substring(0,(time.indexOf("E")-1));
        }*/

        String outputPattern = "h:mm a";
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.parseLong(time));
            //cal.setTimeInMillis(itemLong);
            date = cal.getTime();
            str = outputFormat.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public synchronized static SpannableStringBuilder getSpannableText(String text, String colorCode) {
        SpannableStringBuilder span1 = new SpannableStringBuilder(text);
        ForegroundColorSpan color1 = new ForegroundColorSpan(Color.parseColor(colorCode));
        span1.setSpan(color1, 0, span1.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return span1;
    }

    public static String MilisecondsToTime(long milisecondTime) {
        String time = "";

        int Seconds = (int) (milisecondTime / 1000);
        int Minutes = Seconds / 60;
        Seconds = Seconds % 60;
        int MilliSeconds = (int) (milisecondTime % 1000);

        time = "" + Minutes + ":"
                + String.format("%02d", Seconds) + ":"
                + String.format("%02d", MilliSeconds);

        return time;
    }

    public static Long TimeToMiliseconds(String time) {
        Long timeLong = 0L;

        String[] str = time.split(":");

        int MilliSeconds = Integer.parseInt(str[2]);
        int Seconds = Integer.parseInt(str[1]) * 1000;
        int minute = Integer.parseInt(str[0]) * 1000 * 60;

        timeLong = Long.parseLong((MilliSeconds + Seconds + minute) + "");

        return timeLong;
    }

    public synchronized static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static String getRealPathFromURI_API19(Uri uri, Context context) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return "Not Found";
    }

    public static String getFilePathFromURI(Context context, Uri contentUri) {
        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "FastLapVideo");


        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {

                // output.setText("Failed to create directory MyCameraVideo.");

                Toast.makeText(context, "Failed to create directory MyCameraVideo.",
                        Toast.LENGTH_LONG).show();

                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }


        // Create a media file name

        // For unique file name appending current timeStamp with file name
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + fileName + ".mp4");
            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveBitmapCache() {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };


    }

    public synchronized void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    /**
     * Create a File for saving an image or video
     */
    public static File getOutputMediaFile(int type, Context context) {

        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "FastLapVideo");


        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {

                // output.setText("Failed to create directory MyCameraVideo.");

                Toast.makeText(context, "Failed to create directory MyCameraVideo.",
                        Toast.LENGTH_LONG).show();

                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }


        // Create a media file name

        // For unique file name appending current timeStamp with file name
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());

        File mediaFile;

        if (type == I.MEDIA_TYPE_VIDEO) {

            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");

        } else if (type == I.MEDIA_TYPE_IMAGE) {
            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".png");
        } else
            return null;


        return mediaFile;
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public static String getUniqueImageName() {
        //will generate a random num
        //between 15-10000
        Random r = new Random();
        int num = r.nextInt(10000 - 15) + 15;
        String fileName = "img_" + num + ".jpg";
        return fileName;
    }

    /**
     * Create a file Uri for saving an image or video
     */
    public static Uri getOutputMediaFileUri(int type, Context context) {
        return FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".com.os.fastlap.provider", Util.getOutputMediaFile(type, context));

        //   return Uri.fromFile(getOutputMediaFile(type));
    }

    public static File saveBitmap(Bitmap bitmap, String path, Context context) {
        File file = null;
        if (bitmap != null) {

            file = Util.getOutputMediaFile(I.MEDIA_TYPE_IMAGE, context);
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(path); //here is set your file path where you want to save or also here you can set file object directly

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static File storeImage(Bitmap image, Context context) {
        File pictureFile = getOutputMediaFile(context);
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        return pictureFile;
    }

    /**
     * Create a File for saving an image or video
     */
    public static File getOutputMediaFile(Context context) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + context.getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + getUniqueImageName();
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static void setLocale(Context context) {
        String lang = MySharedPreferences.getPreferences(context, S.language);
        if (!TextUtils.isEmpty(lang)) {
            Locale myLocale = new Locale(lang);
            Resources res = context.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }
        // reloadUI(); // you may not need this, in my activity ui must be refreshed immediately so it has a function like this.
    }

    public static void setImageLoader(String url, final ImageView img_bg,final Context context)
    {
        try {
            ImageLoader.getInstance().displayImage(url, img_bg, Util.getImageLoaderOption(context), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                    Log.e("tag","onLoadingStarted"+s);

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    Log.e("tag","onLoadingFailed"+s);
                    img_bg.setImageBitmap(null);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    Log.e("tag","onLoadingComplete"+bitmap);
                    //ProfileActivity.img_bg.setImageBitmap(bitmap);
                    Glide.with(context).load(s).into(img_bg);

                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public static boolean dobDiff(String dob)
    {
        try {
            Date userDob = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
            Date today = new Date();
            long diff = today.getTime() - userDob.getTime();
            int numOfYear = (int) ((diff / (1000 * 60 * 60 * 24)) / 365);
            if(numOfYear<13) {
                return false;
            }
            else {
                return true;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public static File createFileFromInputStream(InputStream inputStream) {

        try{
            File f = new File("fast_lap.csv");
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while((length=inputStream.read(buffer)) > 0) {
                outputStream.write(buffer,0,length);
            }

            outputStream.close();
            inputStream.close();

            return f;
        }catch (IOException e) {
            //Logging exception
        }

        return null;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static boolean isGpsAvailabelOrNot(Context context){
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return statusOfGPS;
    }

    public  static ArrayList<String> getAllFileFromStorage(){
        ArrayList<String> list=new ArrayList<>();
        try {
            list.clear();
            Log.d("Files", "Path: " + path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            if(files!=null && files.length > 0){
                Log.d("Files", "Size: "+ files.length);
                list.add("Please select file to upload");
                for (int i = 0; i < files.length; i++) {
                    list.add(files[i].getName());
                    Log.d("Files", "FileName:" + files[i].getName());
                }
            }
        }
        catch (Exception e){
            Log.e("Files", "Size: "+ e);
            e.printStackTrace();
        }
        return list;
    }

    public  static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();
        return date;
    }
    public static void deleteUploadedFile(Context context,String selectedFilePath){
        if(selectedFilePath!=null){
            boolean result=false;
            File file = new File(selectedFilePath);
            if(file.exists()){
                result=file.delete();
                Log.e("tag","file deleted sucefullty"+result);
            }
            else {
                Log.e("tag","file deleted sucefullty"+result);
            }
        }

    }

    public static Bitmap loadBitmap(String url)
    {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try
        {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (bis != null)
            {
                try
                {
                    bis.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }

    public static String getDateFormate(final  String oldDate){

        java.text.DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        java.text.DateFormat outputformat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        String output = null;
        try{
            //Converting the input String to Date
            date= df.parse(oldDate);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);
            //Displaying the date
            System.out.println(output);
            Log.e("tag","New Date"+output);
        }catch(ParseException pe){
            pe.printStackTrace();
        }
        return output;
    }

    public static String getTimeFormat(String ms){
        float totalTime = parseFloat(ms);
        int minutes = (int) Math.floor(totalTime / 60);
        int seconds = (int) Math.floor((totalTime - minutes * 60));
        long miliseconds = (long) Math.floor((totalTime - minutes * 60 - seconds) * 1000);
        if (totalTime > 0 && totalTime!=0.0f) {
            String mMinutes,mSeconds,mMiliseconds;
            mMinutes = minutes > 9 ? String.valueOf(minutes) : '0' + String.valueOf(minutes);
            mSeconds = seconds > 9 ? String.valueOf(seconds) : '0' + String.valueOf(seconds);
            mMiliseconds = miliseconds > 99 ? String.valueOf(miliseconds) : (miliseconds > 9 ? '0' + String.valueOf(miliseconds) : "00" + String.valueOf(miliseconds));
            return mMinutes + ":" + mSeconds + ':' + mMiliseconds;
        } else {
            return "00:00:000";
        }
    }
    //$scope.trackLength = parseInt(track.trackLength) / 1000;
    /*public String lapAverageSpeed(String lapTime,int trackLength){
        int time = strToMiliseconds(lapTime) / (1000 * 60 * 60);
        String averageSpeed = (trackLength / time).toFixed(3);
        return averageSpeed;
    }
    private int strToMiliseconds(String str){
        String[] timeStr =str.split(":");
        int total = 0;
        total+=Integer.parseInt(timeStr[0])*60*1000;
        total+=Integer.parseInt(timeStr[1])*1000;
        total+=Integer.parseInt(timeStr[2]);
        return total;
    }*/
    /*$scope.strToMiliseconds = function(str) {
        var timeStr = str.split(":");
        var total = 0;
        total += parseInt(timeStr[0]) * 60 * 1000;
        total += parseInt(timeStr[1]) * 1000;
        total += parseInt(timeStr[2]);
        return total;
    };*/

    /*$scope.lapAverageSpeed = function (lapTime) {
        var time = parseInt($scope.strToMiliseconds(lapTime)) / (1000 * 60 * 60);
        var averageSpeed = ($scope.trackLength / time).toFixed(3);
        return averageSpeed;
    };*/



    /*function (lapTime) {
       var time = parseInt($scope.strToMiliseconds(lapTime)) / (1000 * 60 * 60);
       var averageSpeed = ($scope.trackLength / time).toFixed(3);
       return averageSpeed;
   };*/
    public static Bitmap getMarkerBitmapFromView(int drawable1, Context context,int color) {
        View customMarkerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);

        markerImageView.setImageResource(drawable1);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, markerImageView.getMeasuredWidth(), markerImageView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();

        Bitmap returnedBitmap = Bitmap.createBitmap(markerImageView.getMeasuredWidth(), markerImageView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        //canvas.drawColor(Color.GREEN, PorterDuff.Mode.SRC_IN);
        Drawable drawable = markerImageView.getDrawable();
        drawable.setColorFilter(new
                PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        if (drawable != null)
            drawable.draw(canvas);
        return returnedBitmap;
    }


    public static String getCountryCode(String countryName) {
        // Get all country codes in a string array.
        String[] isoCountryCodes = Locale.getISOCountries();
        Map<String, String> countryMap = new HashMap<>();

        // Iterate through all country codes:
        for (String code : isoCountryCodes) {
            // Create a locale using each country code
            Locale locale = new Locale("", code);
            // Get country name for each code.
            String name = locale.getDisplayCountry();
            // Map all country names and codes in key - value pairs.
            countryMap.put(name, code);
        }
        // Get the country code for the given country name using the map.
        // Here you will need some validation or better yet
        // a list of countries to give to user to choose from.
        String countryCode = countryMap.get(countryName); // "NL" for Netherlands.

        return countryCode;

    }
    public static Bitmap convertImageURlIntoBitmap(){
        Bitmap image=null;
        try {
            URL url = new URL("http://....");
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(IOException e) {
            System.out.println(e);
        }
        return image;
    }
}
