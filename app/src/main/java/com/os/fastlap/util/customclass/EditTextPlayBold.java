package com.os.fastlap.util.customclass;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by anandj on 7/21/2017.
 */

@SuppressLint("AppCompatCustomView")
public class EditTextPlayBold extends EditText {


    public EditTextPlayBold(Context context) {
        super(context);
        init();
    }

    public EditTextPlayBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextPlayBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditTextPlayBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        setTypeface(Typeface.createFromAsset(getContext().getAssets(),"font/play_bold.ttf"));
    }
}
