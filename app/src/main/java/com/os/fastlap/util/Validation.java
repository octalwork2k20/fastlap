package com.os.fastlap.util;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import java.util.ArrayList;

/*
 * Created by anandj on 7/11/2017.
 */

public class Validation {
    // Login class validation
    public static boolean loginValidation(EditText emailAddressEt, EditText passwordEt, View login_btn, Context context) {
        boolean result = false;
        String email = emailAddressEt.getText().toString().trim();
        String password = passwordEt.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Util.showSnackBar(login_btn, context.getString(R.string.email_empty_valid));
        } else if (!Util.isValidEmail(email)) {
            Util.showSnackBar(login_btn, context.getString(R.string.email_valid));
        } else if (TextUtils.isEmpty(password)) {
            Util.showSnackBar(login_btn, context.getString(R.string.password_empty_valid));
        } else
            result = true;

        return result;
    }

    // Signup user personal details validation
    public static boolean signupUserPersonalDetailsValidation(EditText fullNameEt, EditText last_name_et, EditText nickNameEt, EditText emailAddressEt, EditText mobile_number_et, EditText passwordEt, EditText confirmPasswordEt, View nextBtn, Context context) {
        boolean result = false;
        String f_name = fullNameEt.getText().toString().trim();
        String l_name = last_name_et.getText().toString().trim();
        String n_name = nickNameEt.getText().toString().trim();
        String email = emailAddressEt.getText().toString().trim();
        String mobile = mobile_number_et.getText().toString().trim();
        String password = passwordEt.getText().toString().trim();
        String c_password = confirmPasswordEt.getText().toString().trim();

          if (TextUtils.isEmpty(f_name))
            Util.showSnackBar(nextBtn, context.getString(R.string.first_name_empty_valid));
        else if (f_name.length()<3 || f_name.length()>14)
            Util.showSnackBar(nextBtn, context.getString(R.string.fristname_should_be_minimum_6_character_long));
        else if (TextUtils.isEmpty(l_name))
            Util.showSnackBar(nextBtn, context.getString(R.string.last_name_empty_valid));
        else if (l_name.length()<3 || l_name.length()>14)
            Util.showSnackBar(nextBtn, context.getString(R.string.lastname_should_be_minimum_6_character_long));
        else if (TextUtils.isEmpty(n_name))
            Util.showSnackBar(nextBtn, context.getString(R.string.nick_name_empty_valid));
        else if (n_name.length()<3 || n_name.length()>14)
            Util.showSnackBar(nextBtn, context.getString(R.string.nickname_should_be_minimum_6_character_long));
        else if (TextUtils.isEmpty(email))
            Util.showSnackBar(nextBtn, context.getString(R.string.email_empty_valid));
        else if (!Util.isValidEmail(email))
            Util.showSnackBar(nextBtn, context.getString(R.string.email_valid));
        else if (TextUtils.isEmpty(mobile))
            Util.showSnackBar(nextBtn, context.getString(R.string.mobile_empty_valid));
        else if (TextUtils.getTrimmedLength(mobile) < 9)
            Util.showSnackBar(nextBtn, context.getString(R.string.mobile_langth_valid));
        else if (TextUtils.isEmpty(password))
            Util.showSnackBar(nextBtn, context.getString(R.string.password_empty_valid));
        else if (!Util.isValidPassword(password))
            Util.showSnackBar(nextBtn, context.getString(R.string.validation_error_password));
        else if (TextUtils.isEmpty(c_password))
            Util.showSnackBar(nextBtn, context.getString(R.string.confirm_password_empty_valid));
        else if (!TextUtils.equals(password, c_password))
            Util.showSnackBar(nextBtn, context.getString(R.string.password_confirm_not_match));
        else
            result = true;

        return result;
    }

    // Signup user other detail validation
    public static boolean signupUserDetailsValidation(TextView dobEt, TextView chooseLanguageEt,
                                                      String nationality,String location, EditText enterVoucherCodeEt,
                                                      AppCompatCheckBox terms_checkbox, View signupBtn,
                                                      Context context) {
        boolean result = false;
        String dob = dobEt.getText().toString();
        if (TextUtils.isEmpty(dob))
            Util.showSnackBar(signupBtn, context.getString(R.string.dob_valid_error));
        else if(!Util.dobDiff(dob))
            Util.showSnackBar(signupBtn, context.getString(R.string.dob_validation_error));
        else if(chooseLanguageEt.getText().length()<0)
            Util.showSnackBar(signupBtn, context.getString(R.string.choose_language));
        else if (TextUtils.isEmpty(nationality))
            Util.showSnackBar(signupBtn, context.getString(R.string.select_nationality));
        else if (TextUtils.isEmpty(location))
            Util.showSnackBar(signupBtn, context.getString(R.string.select_location));
        else if (!terms_checkbox.isChecked())
            Util.showSnackBar(signupBtn, context.getString(R.string.term_and_condition_validation));
        else
            result = true;

        return result;
    }

    // Otp screen validation
    public static boolean otpScreenValidation(EditText otp_et, String otp, View verify_btn, Context context) {
        boolean result = false;
        String type_otp = otp_et.getText().toString().trim();
        if (TextUtils.isEmpty(type_otp))
            Util.showSnackBar(verify_btn, context.getString(R.string.otp_empty));
        else if (!TextUtils.equals(type_otp, otp))
            Util.showSnackBar(verify_btn, context.getString(R.string.otp_valid));
        else
            result = true;
        return result;
    }

    // Forgot password validation
    public static boolean forgotPasswordValidation(EditText forgot_mail, View loginBtn, Context context) {
        boolean result = false;
        String email = forgot_mail.getText().toString().trim();
        if (TextUtils.isEmpty(email))
            Util.showSnackBar(loginBtn, context.getString(R.string.email_empty_valid));
        else if (!Util.isValidEmail(email))
            Util.showSnackBar(loginBtn, context.getString(R.string.email_valid));
        else
            result = true;

        return result;
    }

    // Change Password validation
    public static boolean changePasswordValidation(EditText current_et, EditText new_password_et, EditText confirm_new_password_et, View passwordSaveTv, Context context) {
        boolean result = false;
        String cPassword = current_et.getText().toString().trim();
        String nPassword = new_password_et.getText().toString().trim();
        String confirmPassword = confirm_new_password_et.getText().toString().trim();
        if (TextUtils.isEmpty(cPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.current_password_empty_error));
        else if (TextUtils.isEmpty(nPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.new_password_empty_error));
        else if (!Util.isValidPassword(nPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.validation_error_password));
        else if (TextUtils.isEmpty(confirmPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.confirm_password_empty_valid));
        else if (!TextUtils.equals(nPassword, confirmPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.password_confirm_not_match));
        else
            result = true;
        return result;
    }

    // Add Vehicle validation
    public static boolean addVehicleValidation(String vehicleTypeId, String vehicleBrandID, String vehicleModelId, String vehicleVersionId, String vehicleYearId, String vehicleTyreBrandId, String vehicleTyreModelId, int imageSize, TextViewPlayBold button, EditText descriptionEt, Context context) {
        boolean result = false;
        String description = descriptionEt.getText().toString().trim();
        if (TextUtils.isEmpty(vehicleTypeId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_type_empty_error));
        else if (TextUtils.isEmpty(vehicleBrandID))
            Util.showSnackBar(button, context.getString(R.string.vehicle_brand_empty_error));
        else if (TextUtils.isEmpty(vehicleModelId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_model_empty_error));
        else if (TextUtils.isEmpty(vehicleVersionId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_version_empty_error));
        else if (TextUtils.isEmpty(vehicleYearId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_year_empty_error));
        else if (TextUtils.isEmpty(vehicleTyreBrandId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_tyrebrand_empty_error));
        else if (TextUtils.isEmpty(vehicleTyreModelId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_tyremodel_empty_error));
        else if (TextUtils.isEmpty(description))
            Util.showSnackBar(button, context.getString(R.string.please_fill_description));
        /*else if (imageSize < 2)
            Util.showSnackBar(button, context.getString(R.string.vehicle_image_empty_error));*/
        else
            result = true;
        return result;
    }

    // Add Cloth validation
    public static boolean addClothValidatioan(String vehicleTypeId, String vehicleBrandID, String vehicleModelId, String vehicleVersionId, String vehicleYearId, String vehicleTyreBrandId, String vehicleTyreModelId, int imageSize, TextViewPlayBold button, EditText descriptionEt, Context context) {
        boolean result = false;
        String description = descriptionEt.getText().toString().trim();
        if (TextUtils.isEmpty(vehicleTypeId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_type_empty_error));
        else if (TextUtils.isEmpty(vehicleBrandID))
            Util.showSnackBar(button, context.getString(R.string.vehicle_brand_empty_error));
        else if (TextUtils.isEmpty(vehicleModelId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_model_empty_error));
        else if (TextUtils.isEmpty(vehicleVersionId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_version_empty_error));
        else if (TextUtils.isEmpty(vehicleYearId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_year_empty_error));
        else if (TextUtils.isEmpty(vehicleTyreBrandId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_tyrebrand_empty_error));
        else if (TextUtils.isEmpty(vehicleTyreModelId))
            Util.showSnackBar(button, context.getString(R.string.vehicle_tyremodel_empty_error));
        else if (TextUtils.isEmpty(description))
            Util.showSnackBar(button, context.getString(R.string.please_fill_description));
        else if (imageSize < 2)
            Util.showSnackBar(button, context.getString(R.string.vehicle_image_empty_error));
        else
            result = true;
        return result;
    }

    public static boolean addClothValidation(String clothName, String clothBrandID, String clothModelID, String clothYearID, String clothColorID, PagerBean pagerBean, TextViewPlayBold addClothBtn, Context context) {

        boolean result = false;
        if (TextUtils.isEmpty(clothBrandID))
            Util.showSnackBar(addClothBtn, context.getString(R.string.cloth_brand_empty_error));
        else if (TextUtils.isEmpty(clothModelID))
            Util.showSnackBar(addClothBtn, context.getString(R.string.cloth_model_empty_error));
        else if (TextUtils.isEmpty(clothYearID))
            Util.showSnackBar(addClothBtn, context.getString(R.string.cloth_year_empty_error));
        else if (TextUtils.isEmpty(clothColorID))
            Util.showSnackBar(addClothBtn, context.getString(R.string.cloth_color_empty_error));
       /* else if (pagerBean == null)
            Util.showSnackBar(addClothBtn, context.getString(R.string.cloth_image_empty_error));*/
        else
            result = true;
        return result;
    }

    public static boolean updateAboutMeValidation(TextView userLanguageTv, EditText userNumberEt, TextView userEmailEt, EditText userWebsiteEt, EditText userAboutEt, ArrayList<PagerBean> family_photo_pagerBeanArrayList, View saveBtn, Context context) {
        boolean result = false;
        String phone = userNumberEt.getText().toString().trim();
        String website = userWebsiteEt.getText().toString().trim();
        if (TextUtils.isEmpty(phone))
            Util.showSnackBar(saveBtn, context.getString(R.string.mobile_empty_valid));
 /*       else if(family_photo_pagerBeanArrayList.size() < 2)
            Util.showSnackBar(saveBtn,context.getString(R.string.please_select_));*/
        else
            result = true;
        return result;
    }

    // Update user detail validation
    public static boolean updateUserDetailsValidation(String fname, String lname, String username, String dob, String nationality, String city, TextViewPlayBold generalSaveTv, Context context) {
        boolean result = false;
        if (TextUtils.isEmpty(fname))
            Util.showSnackBar(generalSaveTv, context.getString(R.string.first_name_empty_valid));
        else if (TextUtils.isEmpty(lname))
            Util.showSnackBar(generalSaveTv, context.getString(R.string.last_name_empty_valid));
        else if (TextUtils.isEmpty(username))
            Util.showSnackBar(generalSaveTv, context.getString(R.string.nick_name_empty_valid));
        else if (TextUtils.isEmpty(dob))
            Util.showSnackBar(generalSaveTv, context.getString(R.string.dob_valid_error));
        else if(!Util.dobDiff(dob))
            Util.showSnackBar(generalSaveTv, context.getString(R.string.dob_validation_error));
        else if (TextUtils.isEmpty(nationality))
            Util.showSnackBar(generalSaveTv, context.getString(R.string.select_nationality));
        else if (TextUtils.isEmpty(city))
            Util.showSnackBar(generalSaveTv, context.getString(R.string.select_location));
        else
            result = true;

        return result;
    }

    // Add post validation
    public static boolean addPostValidation(ArrayList<PagerBean> imageList, String description, View btnPost, Context context) {
        boolean result = false;
        if (imageList.size() == 0 && description.isEmpty()) {
            Util.showSnackBar(btnPost, context.getString(R.string.please_select_one_photo_video));
        } else
            result = true;
        return result;
    }


    public static boolean addGroupStep1Validation(String groupname, String privacy, String vehicleTypeId, String vehicleBrandID, String trackId, Context context, View nextBtn) {

        boolean result = false;
        if (TextUtils.isEmpty(groupname))
            Util.showSnackBar(nextBtn, context.getString(R.string.group_name_empty_error));
        else if (TextUtils.isEmpty(privacy))
            Util.showSnackBar(nextBtn, context.getString(R.string.privacy_empty_error));
        else if (TextUtils.isEmpty(vehicleTypeId))
            Util.showSnackBar(nextBtn, context.getString(R.string.group_type_empty_error));
        else if (TextUtils.isEmpty(vehicleBrandID))
            Util.showSnackBar(nextBtn, context.getString(R.string.vehicle_type_empty_error));
        else if (TextUtils.isEmpty(trackId))
            Util.showSnackBar(nextBtn, context.getString(R.string.track_empty_error));
        else
            result = true;
        return result;
    }


    public static boolean addGroupStep2Validation(String description, PagerBean profilePagerBean, PagerBean coverPagerBean, Context context, TextViewPlayBold nextBtn) {
        boolean result = false;
        if (TextUtils.isEmpty(description))
            Util.showSnackBar(nextBtn, context.getString(R.string.description_empty_error));
        else if (profilePagerBean == null)
            Util.showSnackBar(nextBtn, context.getString(R.string.group_profile_empty));
        else if (coverPagerBean == null)
            Util.showSnackBar(nextBtn, context.getString(R.string.group_cover_empty));
        else
            result = true;

        return result;
    }

    // Change Password validation
    public static boolean changeEmailValidation(EditText current_et, EditText new_password_et, EditText confirm_new_password_et, View passwordSaveTv, Context context) {
        boolean result = false;
        String cPassword = current_et.getText().toString().trim();
        String nPassword = new_password_et.getText().toString().trim();
        String confirmPassword = confirm_new_password_et.getText().toString().trim();
        if (TextUtils.isEmpty(cPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.current_email_empty_error));
        else if (!TextUtils.equals(cPassword, MySharedPreferences.getPreferences(context, S.email)))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.old_email_confirm_not_match));
        else if (TextUtils.isEmpty(nPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.new_email_empty_error));
        else if (!Util.isValidEmail(nPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.email_valid));
        else if (TextUtils.isEmpty(confirmPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.confirm_email_empty_valid));
        else if (!TextUtils.equals(nPassword, confirmPassword))
            Util.showSnackBar(passwordSaveTv, context.getString(R.string.email_confirm_not_match));
        else
            result = true;
        return result;
    }
}
