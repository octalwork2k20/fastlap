package com.os.fastlap.util;

import android.util.Log;

import com.highsoft.highcharts.Common.HIChartsClasses.HIChart;
import com.highsoft.highcharts.Common.HIChartsClasses.HICredits;
import com.highsoft.highcharts.Common.HIChartsClasses.HIDateTimeLabelFormats;
import com.highsoft.highcharts.Common.HIChartsClasses.HIEvents;
import com.highsoft.highcharts.Common.HIChartsClasses.HIExporting;
import com.highsoft.highcharts.Common.HIChartsClasses.HILabels;
import com.highsoft.highcharts.Common.HIChartsClasses.HILegend;
import com.highsoft.highcharts.Common.HIChartsClasses.HIOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIPlotOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIPoint;
import com.highsoft.highcharts.Common.HIChartsClasses.HISeries;
import com.highsoft.highcharts.Common.HIChartsClasses.HISpline;
import com.highsoft.highcharts.Common.HIChartsClasses.HIStyle;
import com.highsoft.highcharts.Common.HIChartsClasses.HITitle;
import com.highsoft.highcharts.Common.HIChartsClasses.HITooltip;
import com.highsoft.highcharts.Common.HIChartsClasses.HIXAxis;
import com.highsoft.highcharts.Common.HIChartsClasses.HIYAxis;
import com.highsoft.highcharts.Common.HIColor;
import com.highsoft.highcharts.Core.HIChartView;
import com.highsoft.highcharts.Core.HIFunction;
import com.os.fastlap.beans.BeanCompareData;
import com.os.fastlap.beans.BeanLocation;
import com.os.fastlap.beans.BeanTimeLap;

import java.util.ArrayList;
import java.util.HashMap;

public class ChartsManager {

    public ChartsManager() {

    }
    public HIOptions setChart(HIChartView splineView,ArrayList<BeanCompareData> compareList, ArrayList<BeanTimeLap> mUserList, boolean isSpeed, boolean isAccCalc, boolean isAccOn, boolean isBrakeCalc, boolean isBrakeOn,boolean isXView,boolean isYView,boolean isZView)
    {
        ArrayList<HIYAxis>arrYAxis=new ArrayList<>();
        ArrayList<HISpline>dataSets=new ArrayList<>();
        HashMap axisDic = new HashMap();
        dataSets.clear();
        arrYAxis.clear();
        axisDic.clear();

        HIOptions options = new HIOptions();

        HIChart chart = new HIChart();
        chart.setZoomType("x");
        chart.setSpacingBottom(8);
        chart.setSpacingLeft(2);
        chart.setSpacingRight(2);
        chart.setSpacingTop(8);

        chart.setBackgroundColor(HIColor.initWithHexValue("FFFFFF"));

        chart.setBorderRadius(6);
        chart.setType("spline");
        HITitle title = new HITitle();
        title.setText("");

        HITooltip tooltip = new HITooltip();
        tooltip.setShared(true);
        tooltip.setHeaderFormat("");

        tooltip.setFormatter(new HIFunction("function () {if (this.points[0] && this.points[0].point && this.points[0].point.index && this.points[0].series && this.points[0].series.userOptions && this.points[0].series.userOptions.data && this.points[0].series.userOptions.data[this.points[0].point.index] && this.points[0].series.userOptions.data[this.points[0].point.index][2]){var customData = this.points[0].series.userOptions.data[this.points[0].point.index][2];var html = '<b>Name:</b> ' + customData.user + '<br /><b>Distance:</b> ' + customData.distance + '<br />'+'<br /><b>Time:</b> ' + customData.time + '<br />';for (var key in customData){if (customData.hasOwnProperty(key)) {if(key == 'user' || key == 'distance'||key == 'time') {}else if (key == 'speed'){if (("+isSpeed+")) { html += '<b>Speed:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_x') { if ("+isXView+") { html += '<b>Accel X:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_y') { if ("+isYView+") { html += '<b>Accel Y:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_z') { if ("+isZView+") { html += '<b>Accel Z:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_calc') { if ("+isBrakeCalc+") { html += '<b>Brake Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_on') { if ("+isBrakeOn+") { html += '<b>Brake On:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_calc') { if ("+isAccCalc+") { html += '<b>Accel Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_on') { if ("+isAccOn+") { html += '<b>Accel On:</b> ' + customData[key] + '<br />'; } } }}}return html;}"));
        HILegend legend=new HILegend();
        legend.setEnabled(false);

        HICredits credits = new HICredits();
        credits.setEnabled(true);


        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setSeries(new HISeries());
        plotOptions.getSeries().setColor(HIColor.initWithRGBA(255, 255, 255, 0.6));
        plotOptions.getSeries().setPoint(new HIPoint());
        plotOptions.getSeries().getPoint().setEvents(new HIEvents());


        HIExporting exporting = new HIExporting();
        exporting.setEnabled(false);

        if (isSpeed) {
            arrYAxis.add(getSpeedYAxis());
        }
        if (isAccCalc) {
            arrYAxis.add(getAccelCalcYAxis());
        }
        if (isAccOn) {
            arrYAxis.add(getAccelOnYAxis());
        }
        if (isBrakeCalc) {
            arrYAxis.add(getBrakeCalcYAxis());
        }
        if (isBrakeOn) {
            arrYAxis.add(getBrakeOnYAxis());
        }
        if (isXView) {
            arrYAxis.add(getXAxis());
        }
        if (isYView) {
            arrYAxis.add(getYAxis());
        }
        if (isZView) {
            arrYAxis.add(getZAxis());
        }

        if (arrYAxis.size()>0) {
            for( int i=0;i<arrYAxis.size();i++) {
                HIYAxis yAxis=arrYAxis.get(i);
                Log.e("tag","title"+yAxis.getTitle().getText()+" value "+i);
                axisDic.put(yAxis.getTitle().getText(),i);
            }
        }
        Log.e("tag","compareList1  size"+compareList.size());
        for(int i=0;i<compareList.size();i++){
            int color=compareList.get(i).getColor();
            String defaultColor = String.format("%06X", (0xFFFFFF & color));
            ArrayList<BeanLocation> locationList = new ArrayList<>();
            BeanCompareData beanCompareData = new BeanCompareData();
            beanCompareData = compareList.get(i);
            locationList = beanCompareData.getLocationArrayList();

            ArrayList<Object[]> speedData=new ArrayList<>();
            ArrayList<Object[]> brakeCalcData =new ArrayList<>();
            ArrayList<Object[]> brakeOnData =new ArrayList<>();
            ArrayList<Object[]> accelCalcData =new ArrayList<>();
            ArrayList<Object[]> accelOnData =new ArrayList<>();

            ArrayList<Object[]> xData =new ArrayList<>();
            ArrayList<Object[]> yData =new ArrayList<>();
            ArrayList<Object[]> zData =new ArrayList<>();


            ArrayList<Object[]> tooltipData =new ArrayList<>();

            for(int index=0;index<locationList.size();index++){
                HashMap dataDic = new HashMap();
                dataDic.put("speed",locationList.get(index).getSpeed());
                dataDic.put("time",Util.getTimeFormat(locationList.get(index).getTime()));
                dataDic.put("distance",locationList.get(index).getDist());
                dataDic.put("user",mUserList.get(i).getUserName());
                dataDic.put("accel_x",locationList.get(index).getAccel_x());
                dataDic.put("accel_y",locationList.get(index).getAccel_y());
                dataDic.put("accel_z",locationList.get(index).getAccel_z());
                dataDic.put("brake_calc",locationList.get(index).getBrake_calc());
                dataDic.put("acc_calc",locationList.get(index).getAcc_calc());
                dataDic.put("brake_on",locationList.get(index).getBrake_on());
                dataDic.put("acc_on",locationList.get(index).getAcc_on());

                speedData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getSpeed()),dataDic} );
                brakeCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getBrake_calc()),dataDic} );
                brakeOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getBrake_on()),dataDic} );
                accelCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAcc_calc()),dataDic} );
                accelOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAcc_on()),dataDic} );
                xData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAccel_x()),dataDic} );
                yData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAccel_y()),dataDic} );
                zData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAccel_z()),dataDic} );
                tooltipData.add(new Object[]{dataDic});
            }

            if(isSpeed){
                HISpline speedLine = new HISpline();
                speedLine.setTooltip(new HITooltip());
                speedLine.setData(speedData);
                speedLine.setName("Speed");
                speedLine.setYAxis(axisDic.get("Speed(km/h)"));
                speedLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(speedLine);

            }
            if(isBrakeCalc){
                HISpline brakeCalcLine = new HISpline();
                brakeCalcLine.setTooltip(new HITooltip());
                brakeCalcLine.setData(brakeCalcData);
                brakeCalcLine.setName("Brake Calc");
                brakeCalcLine.setYAxis(axisDic.get("Brake Calc"));
                brakeCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeCalcLine);

            }
            if(isBrakeOn){
                HISpline brakeOnLine = new HISpline();
                brakeOnLine.setTooltip(new HITooltip());
                brakeOnLine.setData(brakeOnData);
                brakeOnLine.setName("Brake On");
                brakeOnLine.setYAxis(axisDic.get("Brake On"));
                brakeOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeOnLine);
            }
            if(isAccCalc){
                HISpline accelCalcLine = new HISpline();
                accelCalcLine.setTooltip(new HITooltip());
                accelCalcLine.setData(accelCalcData);
                accelCalcLine.setName("Accel Calc");
                accelCalcLine.setYAxis(axisDic.get("Accel Calc"));
                accelCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelCalcLine);

            }
            if(isAccOn){
                HISpline accelOnLine = new HISpline();
                accelOnLine.setTooltip(new HITooltip());
                accelOnLine.setData(accelOnData);
                accelOnLine.setName("Accel On");
                accelOnLine.setYAxis(axisDic.get("Accel On"));
                accelOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelOnLine);

            }
            if(isXView){
                HISpline XLine = new HISpline();
                XLine.setTooltip(new HITooltip());
                XLine.setData(xData);
                XLine.setName("X");
                XLine.setYAxis(axisDic.get("X"));
                XLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(XLine);
            }
            if(isYView){
                HISpline YLine = new HISpline();
                YLine.setTooltip(new HITooltip());
                YLine.setData(yData);
                YLine.setName("Y");
                YLine.setYAxis(axisDic.get("Y"));
                YLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(YLine);
            }
            if(isZView){
                HISpline ZLine = new HISpline();
                ZLine.setTooltip(new HITooltip());
                ZLine.setData(zData);
                ZLine.setName("Z");
                ZLine.setYAxis(axisDic.get("Z"));
                ZLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(ZLine);
            }

        }
        Log.e("tag","dataSets  size"+dataSets.size());
        options.setChart(chart);
        options.setXAxis(new ArrayList<HIXAxis>(){{add(getTimeXAxis());}});
        options.setYAxis(new ArrayList<>(arrYAxis));
        options.setSeries(new ArrayList<HISeries>(dataSets));
        options.setLegend(legend);
        options.setTitle(title);
        options.setExporting(exporting);
        options.setTooltip(tooltip);
        options.setCredits(credits);
        splineView.setOptions(options);
        return options;
    }

    public HIOptions setChartWithDistance(HIChartView splineView,ArrayList<BeanCompareData> compareList, ArrayList<BeanTimeLap> mUserList, boolean isSpeed, boolean isAccCalc, boolean isAccOn, boolean isBrakeCalc, boolean isBrakeOn,boolean isXView,boolean isYView,boolean isZView)
    {
        ArrayList<HIYAxis>arrYAxis=new ArrayList<>();
        ArrayList<HISpline>dataSets=new ArrayList<>();
        HashMap axisDic = new HashMap();
        dataSets.clear();
        arrYAxis.clear();
        axisDic.clear();

        HIOptions options = new HIOptions();

        HIChart chart = new HIChart();
        chart.setZoomType("x");
        chart.setSpacingBottom(8);
        chart.setSpacingLeft(2);
        chart.setSpacingRight(2);
        chart.setSpacingTop(8);

        chart.setBackgroundColor(HIColor.initWithHexValue("FFFFFF"));

        chart.setBorderRadius(6);
        chart.setType("spline");
        HITitle title = new HITitle();
        title.setText("");

        HITooltip tooltip = new HITooltip();
        tooltip.setShared(true);
        tooltip.setHeaderFormat("");

        tooltip.setFormatter(new HIFunction("function () {if (this.points[0] && this.points[0].point && this.points[0].point.index && this.points[0].series && this.points[0].series.userOptions && this.points[0].series.userOptions.data && this.points[0].series.userOptions.data[this.points[0].point.index] && this.points[0].series.userOptions.data[this.points[0].point.index][2]){var customData = this.points[0].series.userOptions.data[this.points[0].point.index][2];var html = '<b>Name:</b> ' + customData.user + '<br /><b>Distance:</b> ' + customData.distance + '<br />'+'<br /><b>Time:</b> ' + customData.time + '<br />';for (var key in customData){if (customData.hasOwnProperty(key)) {if(key == 'user' || key == 'distance'||key == 'time') {}else if (key == 'speed'){if (("+isSpeed+")) { html += '<b>Speed:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_x') { if ("+isXView+") { html += '<b>Accel X:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_y') { if ("+isYView+") { html += '<b>Accel Y:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_z') { if ("+isZView+") { html += '<b>Accel Z:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_calc') { if ("+isBrakeCalc+") { html += '<b>Brake Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_on') { if ("+isBrakeOn+") { html += '<b>Brake On:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_calc') { if ("+isAccCalc+") { html += '<b>Accel Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_on') { if ("+isAccOn+") { html += '<b>Accel On:</b> ' + customData[key] + '<br />'; } } }}}return html;}"));
        HILegend legend=new HILegend();
        legend.setEnabled(false);

        HICredits credits = new HICredits();
        credits.setEnabled(true);


        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setSeries(new HISeries());
        plotOptions.getSeries().setColor(HIColor.initWithRGBA(255, 255, 255, 0.6));
        plotOptions.getSeries().setPoint(new HIPoint());
        plotOptions.getSeries().getPoint().setEvents(new HIEvents());


        HIExporting exporting = new HIExporting();
        exporting.setEnabled(false);

        if (isSpeed) {
            arrYAxis.add(getSpeedYAxis());
        }
        if (isAccCalc) {
            arrYAxis.add(getAccelCalcYAxis());
        }
        if (isAccOn) {
            arrYAxis.add(getAccelOnYAxis());
        }
        if (isBrakeCalc) {
            arrYAxis.add(getBrakeCalcYAxis());
        }
        if (isBrakeOn) {
            arrYAxis.add(getBrakeOnYAxis());
        }
        if (isXView) {
            arrYAxis.add(getXAxis());
        }
        if (isYView) {
            arrYAxis.add(getYAxis());
        }
        if (isZView) {
            arrYAxis.add(getZAxis());
        }

        if (arrYAxis.size()>0) {
            for( int i=0;i<arrYAxis.size();i++) {
                HIYAxis yAxis=arrYAxis.get(i);
                Log.e("tag","title"+yAxis.getTitle().getText()+" value "+i);
                axisDic.put(yAxis.getTitle().getText(),i);
            }
        }
        Log.e("tag","compareList1  size"+compareList.size());
        for(int i=0;i<compareList.size();i++){
            int color=compareList.get(i).getColor();
            String defaultColor = String.format("%06X", (0xFFFFFF & color));
            ArrayList<BeanLocation> locationList = new ArrayList<>();
            BeanCompareData beanCompareData = new BeanCompareData();
            beanCompareData = compareList.get(i);
            locationList = beanCompareData.getLocationArrayList();

            ArrayList<Object[]> speedData=new ArrayList<>();
            ArrayList<Object[]> brakeCalcData =new ArrayList<>();
            ArrayList<Object[]> brakeOnData =new ArrayList<>();
            ArrayList<Object[]> accelCalcData =new ArrayList<>();
            ArrayList<Object[]> accelOnData =new ArrayList<>();

            ArrayList<Object[]> xData =new ArrayList<>();
            ArrayList<Object[]> yData =new ArrayList<>();
            ArrayList<Object[]> zData =new ArrayList<>();


            ArrayList<Object[]> tooltipData =new ArrayList<>();

            for(int index=0;index<locationList.size();index++){
                HashMap dataDic = new HashMap();
                dataDic.put("speed",locationList.get(index).getSpeed());
                dataDic.put("time",Util.getTimeFormat(locationList.get(index).getTime()));
                dataDic.put("distance",locationList.get(index).getDist());
                dataDic.put("user",mUserList.get(i).getUserName());
                dataDic.put("accel_x",locationList.get(index).getAccel_x());
                dataDic.put("accel_y",locationList.get(index).getAccel_y());
                dataDic.put("accel_z",locationList.get(index).getAccel_z());
                dataDic.put("brake_calc",locationList.get(index).getBrake_calc());
                dataDic.put("acc_calc",locationList.get(index).getAcc_calc());
                dataDic.put("brake_on",locationList.get(index).getBrake_on());
                dataDic.put("acc_on",locationList.get(index).getAcc_on());

                speedData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getSpeed()),dataDic} );
                brakeCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getBrake_calc()),dataDic} );
                brakeOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getBrake_on()),dataDic} );
                accelCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAcc_calc()),dataDic} );
                accelOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAcc_on()),dataDic} );
                xData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAccel_x()),dataDic} );
                yData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAccel_y()),dataDic} );
                zData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAccel_z()),dataDic} );
                tooltipData.add(new Object[]{dataDic});
            }

            if(isSpeed){
                HISpline speedLine = new HISpline();
                speedLine.setTooltip(new HITooltip());
                speedLine.setData(speedData);
                speedLine.setName("Speed");
                speedLine.setYAxis(axisDic.get("Speed(km/h)"));
                speedLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(speedLine);

            }
            if(isBrakeCalc){
                HISpline brakeCalcLine = new HISpline();
                brakeCalcLine.setTooltip(new HITooltip());
                brakeCalcLine.setData(brakeCalcData);
                brakeCalcLine.setName("Brake Calc");
                brakeCalcLine.setYAxis(axisDic.get("Brake Calc"));
                brakeCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeCalcLine);

            }
            if(isBrakeOn){
                HISpline brakeOnLine = new HISpline();
                brakeOnLine.setTooltip(new HITooltip());
                brakeOnLine.setData(brakeOnData);
                brakeOnLine.setName("Brake On");
                brakeOnLine.setYAxis(axisDic.get("Brake On"));
                brakeOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeOnLine);
            }
            if(isAccCalc){
                HISpline accelCalcLine = new HISpline();
                accelCalcLine.setTooltip(new HITooltip());
                accelCalcLine.setData(accelCalcData);
                accelCalcLine.setName("Accel Calc");
                accelCalcLine.setYAxis(axisDic.get("Accel Calc"));
                accelCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelCalcLine);

            }
            if(isAccOn){
                HISpline accelOnLine = new HISpline();
                accelOnLine.setTooltip(new HITooltip());
                accelOnLine.setData(accelOnData);
                accelOnLine.setName("Accel On");
                accelOnLine.setYAxis(axisDic.get("Accel On"));
                accelOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelOnLine);

            }
            if(isXView){
                HISpline XLine = new HISpline();
                XLine.setTooltip(new HITooltip());
                XLine.setData(xData);
                XLine.setName("X");
                XLine.setYAxis(axisDic.get("X"));
                XLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(XLine);
            }
            if(isYView){
                HISpline YLine = new HISpline();
                YLine.setTooltip(new HITooltip());
                YLine.setData(yData);
                YLine.setName("Y");
                YLine.setYAxis(axisDic.get("Y"));
                YLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(YLine);
            }
            if(isZView){
                HISpline ZLine = new HISpline();
                ZLine.setTooltip(new HITooltip());
                ZLine.setData(zData);
                ZLine.setName("Z");
                ZLine.setYAxis(axisDic.get("Z"));
                ZLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(ZLine);
            }

        }
        Log.e("tag","dataSets  size"+dataSets.size());
        options.setChart(chart);
        options.setXAxis(new ArrayList<HIXAxis>(){{add(getDistanceXAxis());}});
        options.setYAxis(new ArrayList<>(arrYAxis));
        options.setSeries(new ArrayList<HISeries>(dataSets));
        options.setLegend(legend);
        options.setTitle(title);
        options.setExporting(exporting);
        options.setTooltip(tooltip);
        options.setCredits(credits);
        splineView.setOptions(options);
        return options;
    }


    private HIXAxis getDistanceXAxis(){
        HIXAxis xaxis = new HIXAxis();
        xaxis.setLabels(new HILabels());
        xaxis.getLabels().setStyle(new HIStyle());
        xaxis.getLabels().getStyle().setColor("#000000");
        xaxis.setTitle(new HITitle());
        xaxis.getTitle().setText("Distance");
        //xaxis.getLabels().setDistance(15);
        xaxis.getLabels().setFormat("{value} m");
        //xaxis.setDateTimeLabelFormats(new HIDateTimeLabelFormats());
       /* xaxis.getDateTimeLabelFormats().setMillisecond("%M:%S.%L");
        xaxis.getDateTimeLabelFormats().setSecond("%M:%S");
        xaxis.getDateTimeLabelFormats().setMinute("%M");
        xaxis.getDateTimeLabelFormats().setHour("");
        xaxis.getDateTimeLabelFormats().setDay("");
        xaxis.getDateTimeLabelFormats().setWeek("");
        xaxis.getDateTimeLabelFormats().setMonth("");
        xaxis.getDateTimeLabelFormats().setYear("");
        xaxis.setType("datetime");*/
        xaxis.setMinRange(10);

        xaxis.getTitle().setStyle(new HIStyle());
        xaxis.getTitle().getStyle().setColor(xaxis.getLabels().getStyle().getColor());
        return xaxis ;

    }

/*    xAxis.dateTimeLabelFormats.millisecond = "%M:%S:%L"
    xAxis.dateTimeLabelFormats.second = "%M:%S"
    xAxis.dateTimeLabelFormats.minute = "%M:%S"*/
    private HIXAxis getTimeXAxis(){
        HIXAxis xaxis = new HIXAxis();
        xaxis.setLabels(new HILabels());
        xaxis.getLabels().setStyle(new HIStyle());
        xaxis.getLabels().getStyle().setColor("#000000");
        xaxis.setTitle(new HITitle());
        xaxis.setAllowDecimals(true);
        xaxis.getTitle().setText("Time");
        xaxis.setDateTimeLabelFormats(new HIDateTimeLabelFormats());
        xaxis.getDateTimeLabelFormats().setMillisecond("%M:%S.%L");
        xaxis.getDateTimeLabelFormats().setSecond("%M:%S");
        xaxis.getDateTimeLabelFormats().setMinute("%M:%S");
        xaxis.getDateTimeLabelFormats().setHour("");
        xaxis.getDateTimeLabelFormats().setDay("");
        xaxis.getDateTimeLabelFormats().setWeek("");
        xaxis.getDateTimeLabelFormats().setMonth("");
        xaxis.getDateTimeLabelFormats().setYear("");
        xaxis.setType("datetime");
        xaxis.setMinRange(10);

        xaxis.getTitle().setStyle(new HIStyle());
        xaxis.getTitle().getStyle().setColor(xaxis.getLabels().getStyle().getColor());
        return xaxis ;

    }
    private HIYAxis getSpeedYAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Speed(km/h)");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(false);
        return yaxis ;
    }
    private HIYAxis getBrakeCalcYAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Brake Calc");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setVisible(false);
        yaxis.setOpposite(true);
        return yaxis ;
    }
    private HIYAxis getBrakeOnYAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Brake On");
        yaxis.setAlignTicks(true);
        yaxis.setMax(1);
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }

    private HIYAxis getAccelCalcYAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Accel Calc");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }

    private HIYAxis getAccelOnYAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.setMax(1);
        yaxis.setAlignTicks(true);
        yaxis.getTitle().setText("Accel On");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
    private HIYAxis getXAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("X");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
    private HIYAxis getYAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Y");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
    private HIYAxis getZAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Y");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
}
