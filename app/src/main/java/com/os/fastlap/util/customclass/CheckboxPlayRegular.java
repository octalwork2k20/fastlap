package com.os.fastlap.util.customclass;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

/**
 * Created by anandj on 7/21/2017.
 */

@SuppressLint("AppCompatCustomView")
public class CheckboxPlayRegular extends AppCompatCheckBox{

    public CheckboxPlayRegular(Context context) {
        super(context);
        init();
    }

    public CheckboxPlayRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CheckboxPlayRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "font/play_regular.ttf"));
    }

}
