package com.os.fastlap.util.constants;

/**
 * Created by ABHINAVA on 11/10/16. string constants
 */

public class S {
  /*  public static final String IMAGE_BASE_URL = "http://182.156.245.85:3000";
    public static final String BASE_URL = "http://182.156.245.85:3000/api/v1/";
    public static final String DEMOBASE_URL = "http://182.156.245.85:3000/";*/

    public static final String Whether_url = "https://query.yahooapis.com/";
    public static final String IMAGE_BASE_URL = "http://182.156.245.83:3000";
   // public static final String BASE_URL = "http://182.156.245.83:3000/api/v1/"
    public static final String BASE_URL = "http://182.156.245.83:3050/api/v1/";

    public static final String DEMOBASE_URL = "http://182.156.245.83:3000/";
    public static final String FILE_BASE_URL = "http://182.156.245.83:3000/";


  /*  public static final String IMAGE_BASE_URL = "http://182.156.245.85:3000/";
    public static final String BASE_URL = "http://182.156.245.85:3000/api/v1/";*/

    public static final String CHAT_SERVER_URL = "http://182.156.245.83:3000";


    //public static final String CHAT_SERVER_URL = "http://192.168.1.62:3000";
    //public static final String CHAT_SERVER_URL = "ws://115.112.57.157:3004/socket/websocket";
    //"http://192.168.1.62:3004";//"https://socketio-chat.now.sh/";

  /*  public static final String IMAGE_BASE_URL = "http://192.168.1.62:3002/";
    public static final String BASE_URL = "http://192.168.1.62:3002/api/v1/";*/

  /* public static final String IMAGE_BASE_URL = "http://192.168.1.80:3000/";
    public static final String BASE_URL = "http://192.168.1.80:3000/api/v1/";*/

  /* public static final String IMAGE_BASE_URL = "http://192.168.1.80:3002/";
    public static final String BASE_URL = "http://192.168.1.80:3002/api/v1/";*/

    /*   public static final String IMAGE_BASE_URL = "http://192.168.1.62:3002/";
       public static final String BASE_URL = "http://192.168.1.62:3002/api/v1/";
   */

  //  public static final String FILE_BASE_URL = "http://182.156.245.85:3000/";
    public static String YOUTUBE_FEEDS_URL = "https://www.googleapis.com";

    public static final String report_one = "1";
    public static final String report_two = "2";
    public static final String report_three = "3";
    public static final String reportType = "3";


    public static final String info = "info";
    public static final String chat_message = "chat_message";
    public static final String sign_up_api = "signup";
    public static final String sign_in_api = "login";
    public static final String verify_otp_api = "verify_otp";
    public static final String social_social_api = "socailsignup";
    public static final String forgot_password_api = "forgot_password";
    public static final String logout_api = "logout";
    public static final String resend_otp_api = "resendOtp";
    public static final String getNationality_api = "getNationality";
    public static final String getstaticPage_api = "staticPage";
    public static final String changePassword_api = "changePassword";
    public static final String vehicleTypeList_api = "vehicleTypeList";
    public static final String vehicleBrandList_api = "vehicleBrandList";
    public static final String vehicleModelList_api = "vehicleModelList";
    public static final String vehicleVersionList_api = "vehicleVersionList";
    public static final String vehicleYearList_api = "vehicleYearsList";
    public static final String vehicleYear1List_api = "vehicleYearList";
    public static final String vehicleTyreBrandList_api = "vehicletyreBrandList";
    public static final String vehicleTyreModelList_api = "vehicleTyreModelList";
    public static final String addUserVehicle_api = "addUserVehicleFinal";
    public static final String editUserVehicle_api = "editUserVehicleFinal";
    public static final String listUserVehicle_api = "listUserVehicle";
    public static final String removeUserVehicle_api = "removeUserVehicle";
    public static final String userClothList_api = "userClothList";
    public static final String clothBrandList_api = "clothBrandList";
    public static final String clothModelList_api = "clothModelList";
    public static final String clothYearList_api = "clothYearList";
    public static final String clothColorList_api = "clothColorList";
    public static final String userAddCloths_api = "userAddCloths";
    public static final String removeUserCloth_api = "removeUserCloths";
    public static final String contactUs_api = "contactUs";
    public static final String userAbout_api = "userAboutData";
    public static final String updateAbout_api = "profile-update";
    //updateAbout_api = "profile-update"
    public static final String editGeneralInfo_api = "editGeneralInfo";
    public static final String userUpdateTimeSystem_api = "userUpdateTimeSystem";
    public static final String userUpdateMetricSystem_api = "userUpdateMetricSystem";
    public static final String userUpdateWeekStartDay_api = "userUpdateWeekStartDay";
    public static final String userDeviceConnected_api = "userDeviceConnected";
    public static final String deviceDisconnectById_api = "deviceDisconnectById";
    public static final String deviceDisconnect_api = "deviceDisconnect";
    public static final String userUpdateProfileStatus_api = "userUpdateProfileStatus";
    public static final String getLanguage_api = "languageListing";
    public static final String trackManageStatus_api = "trackManageStatus";
    public static final String userListing_api = "userListing";
    public static final String trackListingAll_api = "trackListingAll";
    public static final String myFriendList_api = "myFriendList";
    public static final String mutualFriends_api = "mutualFriends";
    public static final String feelingList_api = "feelingList";
    public static final String updateUserImg_api = "updateUserImg";
    public static final String myFriendBlockList_api = "myFriendBlockList";
    public static final String userBlockStatus_api = "userBlockStatus";
    public static final String postSave_api = "postSave";
    public static final String userFriend_api = "userFriends";
    public static final String postLikeByUser_api = "postLikeByUser";
    public static final String postComment_api = "postComment";
    public static final String postCommentLike_api = "postCommentLike";
    public static final String postCommentListing_api = "postCommentListing";
    public static final String postCommentRemove_api = "postCommentRemove";
    public static final String postListing_api = "postListing";
    public static final String postLikeUserListing_api = "postLikeUserListing";
    public static final String postCommentReply_api = "postCommentReply";
    public static final String postCommentLikeUserListing_api = "postCommentLikeUserListing";
    public static final String postCommentReplyListing_api = "postCommentReplyListing";
    public static final String postCommentReplyLikeUserListing_api = "postCommentReplyLikeUserListing";
    public static final String postCommentReplyLike_api = "postCommentReplyLike";
    public static final String postCommentReplyRemove_api = "postCommentReplyRemove";
    public static final String userNotificationList_api = "userNotificationList";
    public static final String userChangeNotificationStatus_api = "userChangeNotificationStatus";
    public static final String myFriendRequestList_api = "myFriendRequestList";
    public static final String searchList_api = "searchList";
    public static final String trackInfo_api = "trackInfo";
    public static final String trackDiaryPost_api = "trackDiaryPost";
    public static final String eventListByTrack_api = "eventListByTrack";
    public static final String createGroup_api = "createGroup";
    public static final String groupTypeList_api = "groupTypeList";
    public static final String groupInfo_api = "groupInfo";
    public static final String userGroupRequest_api = "userGroupRequest";
    public static final String groupMemberList_api = "groupMemberList";
    public static final String groupListing_api = "groupListing";
    public static final String groupEventList_api = "groupEventList";
    public static final String eventCustomerList_api = "eventCustomerList";
    public static final String eventInfo_api = "eventInfo";
    public static final String galleryListing_api = "galleryListing";
    public static final String userEventListing_api = "userEventListing";
    public static final String myTrackLapList_api = "myTrackLapList";
    public static final String updateNotification_api = "updateNotification";
    public static final String userLapTrackListing_api = "userLapTrackListing";
    public static final String albumListingPhoto_api = "albumListingPhoto";
    public static final String albumListingVideo_api = "albumListingVideo";

    public static final String upcomingEvent_api = "upcomingEvent";
    public static final String alltrack = "alltrack";
    public static final String upcomingEventByDate_api = "upcomingEventByDate";
    public static final String myGroupPostListing_api = "myGroupPostListing";
    public static final String preferredTrack_api = "preferredTrack";
    public static final String preferredTrackNew_api = "preferredTrackNew";
    public static final String myPostListing_api = "myPostListing";
    public static final String advertList_api = "advertList";
    public static final String subscriptionLists_api = "subscriptionLists";
    public static final String postShared_api = "postShared";
    public static final String voucherConfirmation_api = "voucherConfirmation";
    public static final String userSubscriptions_api = "userSubscriptions";
    public static final String purchasePhotoGraphers = "purchasePhotoGraphers";

    public static final String getChatFriendAndGroups_api = "getChatFriendAndGroups";
    public static final String addCartAlbum_api = "addCartAlbum";
    public static final String viewAlbumCart_api = "viewAlbumCart";
    public static final String removeCartAlbum_api = "removeCartAlbum";
    public static final String albumCheckout_api = "albumCheckout";
    public static final String trackDetailByLatLong_api = "trackDetailByLatlong";
    public static final String userUpdateMapView_api = "userUpdateMapView";
    public static final String reportAbuse_api = "reportAbuse";
    public static final String youtubeData = "youtube/v3/videos";
    public static final String uploadLaptime = "uploadLaptime";
    public static final String newTrackRequest = "newTrackRequest";
    public static final String changeEmailAddress = "changeEmailAddress";
    public static final String allTrackLapListings = "allTrackLapListings";
    public static final String allTrackLapListingWithFriends = "allTrackLapListingWithFriends";
    public static final String removeNotification_api = "removeNotification";
    public static final String userGalleryListing = "userGalleryListing";
    public static final String updateLaptime = "updateLaptime";
    public static final String postInfo = "postInfo";
    public static final String userSubscribeList = "userSubscribeList";
    public static final String checkSubscription = "checkSubscription";

    public static final String albumLike = "albumLike";
    public static final String albumLikeUserListing = "albumLikeUserListing";
    public static final String albumComment = "albumComment";
    public static final String albumCommentListing = "albumCommentListing";
    public static final String albumCommentLikeUserListing = "albumCommentLikeUserListing";
    public static final String albumCommentLike = "albumCommentLike";
    public static final String albumCommentRemove_api = "albumCommentRemove";
    public static final String albumCommentReply = "albumCommentReply";
    public static final String albumCommentReplyListing = "albumCommentReplyListing";
    public static final String albumCommentReplyLikeUserListing = "albumCommentReplyLikeUserListing";
    public static final String reportAbuseAlbum = "reportAbuseAlbum";
    public static final String albumShared_api = "albumShared";
    public static final String addalbum = "createAlbum";
    public static final String updateAlbum_api = "updateAlbum";
    public static final String eventTicketPurchase = "eventTicketPurchase";
    public static final String postRemove_api = "postRemove";
    public static final String updatePost_api = "updatePost";
    public static final String trackChangeHomeStatus_api = "trackChangeHomeStatus";
    public static final String generalInformationData = "generalInformationData";
    public static final String advertLike = "advertLike";
    public static final String advertLikeUserListing = "advertLikeUserListing";
    public static final String advertComment = "advertComment";
    public static final String advertCommentRemove_api = "advertCommentRemove";
    public static final String advertCommentListing_api = "advertCommentListing";
    public static final String albumRemove = "albumRemove";
    public static final String albumInfo = "albumInfo";
    public static final String groupJoinRequestSend = "groupJoinRequestSend";
    public static final String myFriendListforGroup = "myFriendListforGroup";
    public static final String myPendingGroupRequest = "myPendingGroupRequest";
    public static final String userGroupJoin = "userGroupJoin";
    public static final String uploadLapTimeData = "uploadLapTimeData1";
    public static final String lapsInTrackVersion = "lapsInTrackVersion";
    public static final String userAddToAdminList = "userAddToAdminList";
    public static final String userGroupMemberRemove = "userGroupMemberRemove";
    public static final String updateAllNotificationToRead = "updateAllNotificationToRead";
    public static final String sessionData = "lap-sessoins";
    public static final String removeSessionImage = "removeSessionImage";
    public static final String updateSessions = "updateSessions";
    public static final String updateLapStatus1 = "updateLapStatus-1";
    public static final String userUpdateGroupType = "userUpdateGroupType";
    public static final String trackConditionList = "trackConditionList";


    //new changes by mukesh
    public static final String preferredFilterSave = "preferredFilterSave";

    public static final String trackProfile = "trackProfile";

    public static final String userBlockId = "userBlockId";
    public static final String android_provider = "android";
    public static final String fastlap_sharedpreferencename = "fastlap";

    public static final String user_id = "user_id";
    public static final String message = "message";
    public static final String user_list = "user_list";

    public static final String content = "content";
    public static final String isEdit = "isEdit";
    public static final int api_status_failur = 0;
    public static final int adi_status_success = 1;
    public static final String status = "status";
    public static final String albumPrice = "albumPrice";
    public static final String data = "data";
    public static final String firstName = "personalInfo.firstName";//personalInfo.firstName
    public static final String lastName = "personalInfo.lastName";
   public static final String timeLapListing = "timeLapListing";
    public static final String username = "username";
    public static final String nickname = "nickname";
    public static final String surname = "surname";

    public static final String email = "email";
    public static final String website = "website";
    public static final String NickName = "nickname";
    public static final String DateBirth = "dateBirth";
    public static final String facebook = "facebook";
    public static final String instagram = "instagram";
    public static final String imgCount = "imgCount";
    public static final String socialId = "socialId";
    public static final String password = "password";
    public static final String new_password = "newpassword";
    public static final String url = "url";
    public static final String mobileNumber = "personalInfo.mobileNumber";
    public static final String dateOfBirth = "personalInfo.dateOfBirth";
    public static final String language = "personalInfo.language";
    public static final String nationality = "address.nationality";
    public static final String location = "address.location";
    public static final String coverImage = "coverImage";
    public static final String voucherCode = "personalInfo.voucherCode";
    public static final String latitude = "address.latitude";
    public static final String longitude = "address.longitude";
    public static final String provider = "provider";
    public static final String deviceType = "deviceType";
    public static final String deviceToken = "deviceToken";
    public static final String role = "role";
    public static final String usernameOrEmail = "usernameOrEmail";
    public static final String device_info = "deviceInfo";
    public static final String _id = "_id";
    public static final String overlay_image = "overlay_image";

    public static final String myLapCount="myLapCount";
    public static final String bestPosition="bestPosition";
    public static final String senderId = "senderId";
    public static final String groupName = "groupName";
    public static final String timebin = "timebin";
    public static final String recieverId = "recieverId";
    public static final String ownerId = "ownerId";
    public static final String displayName = "displayName";
    public static final String profileImageURL = "profileImageURL";
    public static final String image = "image";
    public static final String video = "video";
    public static final String images = "images";
    public static final String error_unknown = "Unknown error. Please try again.";
    public static final String user_device_token = "user_device_token";
    public static final String sign_up_data = "sign_up_data";
    public static final String otp = "otp";
    public static final String id = "id";
    public static final String slug = "slug";
    public static final String email_verify = "isEmailVerified";
    public static final String mobile_verify = "isMobileVerified";
    public static final String socialLoginType = "socialLoginType";
    public static final String deviceInfo_SERIAL = "deviceInfo.SERIAL";
    public static final String deviceInfo_MODEL = "deviceInfo.MODEL";
    public static final String deviceInfo_Manufacture = "deviceInfo.Manufacture";
    public static final String deviceInfo_brand = "deviceInfo.brand";
    public static final String deviceInfo_versioncode = "deviceInfo.versioncode";
    public static final String deviceInfo_deviceType = "deviceInfo.deviceType";
    public static final String deviceInfo_deviceToken = "deviceInfo.deviceToken";
    public static final String address = "address";
    public static final String Content = "content";
    public static final String contacts = "contacts";
    public static final String phone = "phone";
    public static final String latitude_response = "latitude";
    public static final String longitude_response = "longitude";
    public static final String nationality_response = "nationality";
    public static final String location_response = "location";
    public static final String personalInfo = "personalInfo";
    public static final String firstName_response = "firstName";
    public static final String lastName_response = "lastName";
    public static final String voucherCode_response = "voucherCode";
    public static final String dateOfBirth_response = "dateOfBirth";
    public static final String mobileNumber_response = "mobileNumber";
    public static final String language_response = "language";
    public static final String language_id = "language_id";
    public static final String default_language = "english";
    public static final String A = "A";

    public static final String imgdata = "imgdata";
    public static final String userTrack = "userTrack";
    public static final String name = "name";
    public static final String groupImage = "groupImage";
    public static final String position = "position";
    public static final String contactDetails = "contactDetails";
    public static final String aboutTrack = "aboutTrack";
    public static final String straights = "straights";
    public static final String lengthOfStraights = "lengthOfStraights";
    public static final String totalStraights = "totalStraights";
    public static final String turns = "turns";
    public static final String trackLength = "trackLength";
    public static final String leftTurns = "leftTurns";
    public static final String rightTurns = "rightTurns";
    public static final String slowRightTurns = "slowRightTurns";
    public static final String slowLeftTurns = "slowLeftTurns";
    public static final String totalTurns = "totalTurns";
    public static final String trackOpenDate = "trackOpenDate";
    public static final String lng = "lng";
    public static final String lat = "lat";
    public static final String code = "code";
    public static final String vehicleBrandName = "vehicleBrandName";
    public static final String vehicleModelName = "vehicleModelName";
    public static final String vehicleVersionName = "vehicleVersionName";
    public static final String vehicleYear = "vehicleYear";
    public static final String brandName = "brandName";
    public static final String vehichleModelName = "vehichleModelName";
    public static final String userId = "userId";
    public static final String whetherQ = "q";

    public static final String timeLapId = "timeLapId";

    public static final String totalAmountReq = "totalAmountReq";
    public static final String albumId = "albumId";
    public static final String albumCartId = "albumCartId";
    public static final String userAlbumImage = "userAlbumImage";
    public static final String comment = "comment";
    public static final String userPostCommentId = "userPostCommentId";
    public static final String userPostCommentLikeId = "userPostCommentLikeId";
    public static final String edit = "edit";
    public static final String postId = "postId";
    public static final String postLikeId = "postLikeId";
    public static final String anotherId = "anotherId";
    public static final String userVehicleId = "userVehicleId";
    public static final String UserPostCommentReplyId = "UserPostCommentReplyId";
    public static final String notificationId = "notificationId";
    public static final String searchKeyword = "searchKeyword";
    public static final String alreadyFriend = "alreadyFriend";

    public static final String clothNameId = "clothNameId";
    public static final String clothName = "clothName";
    public static final String clothBrandId = "clothBrandId";
    public static final String vehicleTypeId = "vehicleTypeId";
    public static final String clothModelId = "clothModelId";
    public static final String clothYearId = "clothYearId";
    public static final String clothColorId = "clothColorId";
    public static final String userClothId = "userClothId";
    public static final String years = "years";
    public static final String title = "title";
    public static final String timeSystem = "timeSystem";
    public static final String metrics = "metrics";
    public static final String weekStartDay = "weekStartDay";
    public static final String userFriendId = "userFriendId";
    public static final String blockUserId = "blockUserId";

    public static final String vehicleBrandId = "vehicleBrandId";
    public static final String vehicleModelId = "vehicleModelId";
    public static final String isSubscribed = "isSubscribed";
    public static final String vehicleVersionId = "vehicleVersionId";

    //chgess
    public static final String vehicleTyreBrandId = "vehicleTyreBrandId";
    public static final String vehicleTyreModelId = "vehicleTyreModelId";

  public static final String vehicleTypeBrandId = "vehicleTypeBrandId";
  public static final String vehicleTypeModelId = "vehicleTypeModelId";

    public static final String vehicleYearId = "vehicleYearId";
    public static final String description = "description";
    public static final String friendId = "friendId";
    public static final String vehichleTyreModelName = "vehicleTyreModelName";
    public static final String groupTypeId = "groupTypeId";
    public static final String eventId = "eventId";
    public static final String removeImage = "removeImage";
    public static final String vehicleYearFrom = "vehicleYearFrom";
    public static final String vehicleYearTo = "vehicleYearTo";

    public static final String about_us = "about-us";
    public static final String terms_and_conditions = "terms-and-conditions";


    public static final String user_type = "1";
    public static final String social_login_type_fb = "1";
    public static final String social_login_type_google = "2";

    public static final String vehicleModal = "vehicleModal";
    public static String vehiclename = "vehicleName";
    public static final String vehicleId = "vehicleId";

    public static String img1 = "img1";
    public static String img2 = "img2";
    public static String img3 = "img3";
    public static String img4 = "img4";
    public static String img = "img";

    public static final String removeImg1 = "removeImg1";
    public static final String removeImg2 = "removeImg2";
    public static final String removeImg3 = "removeImg3";
    public static final String removeImg4 = "removeImg4";
    public static final String images1 = "images1";
    public static final String images2 = "images2";
    public static final String images3 = "images3";
    public static final String images4 = "images4";
    public static final String aboutDescritpion = "aboutDescritpion";
    public static final String removeFamilyImg = "removeFamilyImg";
    public static final String userdata = "userdata";
    public static final String deviceId = "deviceId";
    public static final String versioncode = "versioncode";
    public static final String brand = "brand";
    public static final String Manufacture = "Manufacture";
    public static final String MODEL = "MODEL";
    public static final String SERIAL = "SERIAL";

    public static final String TrackId = "trackId";
    public static final String userTrackId = "userTrackId";
    public static final String page = "page";


    public static String modified = "modified";
    public static String created = "created";
    public static String userTrackData = "userTrackData";
    public static String trackData = "trackData";
    public static String totalLapRecords = "totalLapRecords";

    public static String friendData = "friendData";
    public static final String fromUserId = "fromUserId";
    public static final String toUserId = "toUserId";
    public static String userData = "userData";
    public static final String unicodes = "unicodes";
    public static String trackname = "trackname";
    public static String lapTime = "lapTime";
    public static final String trackId = "trackId";
  public static final String filterData = "filterData";
   //changes by mukesh
  public static final String preferredTrackId = "preferredTrackId";

    public static final String position_text = "position_text";
    public static final String speed = "speed";
    public static final String weather = "weather";
    public static final String feelingId = "feelingId";
    public static final String photoCount = "photoCount";
    public static final String videoCount = "videoCount";
    public static String countLikeData = "countLikeData";
    public static String countReplyCommentData = "countReplyCommentData";
    public static String postData = "postData";
    public static final String postImages = "postImages";
    public static final String commentPostTagFriend = "commentPostTagFriend";
    public static final String postShareUser = "postShareUser";
    public static final String type = "type";
    public static final String fileName = "fileName";
    public static String likedPost = "likedPost";
    public static String likedPostCount = "likedPostCount";
    public static String commentPostCount = "commentPostCount";
    public static String countReplyData = "countReplyData";
    public static String likedComment = "likedComment";
    public static String likedCommentReply = "likedCommentReply";
    public static String likeType = "likeType";
    public static final String userPostCommentReplyId = "userPostCommentReplyId";
    public static final String IMAGETYPE = "1";
    public static final String VIDEOTYPE = "2";
    public static String users = "users";
    public static String groups = "groups";
    public static String events = "events";
    public static String likedFriend = "likedFriend";
    public static String addedGroup = "addedGroup";
    public static String addedEvent = "addedEvent";
    public static String trackOpenedDate = "trackOpenedDate";
    public static final String groupId = "groupId";
    public static String days = "days";
    public static String endDate = "endDate";
    public static String startDate = "startDate";
    public static String remainTicket = "remainTicket";
    public static String totalTicket = "totalTicket";
    public static final String privacy = "privacy";
    public static String ticketPrice = "ticketPrice";
    public static String joinedUser = "joinedUser";
    public static String endTime = "endTime";
    public static String startTime = "startTime";
    public static final String thumbName = "thumbName";
    public static String lapId = "lapId";
    public static final String lapDate = "lapDate";
    public static String lapImage = "lapImage";
    public static final String time = "time";
    public static final String notificationStatus = "notificationStatus";
    public static final String thumb = "thumb";
    public static final String videoType = "2";
    public static String lapUserList = "lapUserList";
    public static String albumFriend = "albumFriend";
    public static String filesAll = "filesAll";
    public static String query = "query";
    public static String results = "results";
    public static String channel = "channel";
    public static String item = "item";
    public static String forecast = "forecast";
    public static String date = "date";
    public static String day = "day";
    public static String text = "text";
    public static final String dateReq = "dateReq";
    public static final String dateOfEvent = "dateOfEvent";
    public static String userLapData = "userLapData";
    public static String expiredDate = "expiredDate";
    public static String comparison = "comparison";
    public static String sessions = "sessions";
    public static final String laps = "laps";
    public static String garage = "garage";
    public static String media = "media";
    public static String price = "price";

    public static String MESSAGE_RECIEVE_EVENT = "addMessage";
    public static String MESSAGE_SEND_EVENT = "newMessage";
    public static String JOIN_EVENT = "join";

    public static String NEWUSER_JOIN_EVENT = "userjoined";
    public static String USER_LEFT_EVENT = "user left";
    public static String USER_TYPING = "typing";
    public static String USER_STOP_TYPING = "stopTyping";
    public static String USER_REMOVE = "removeUser";


    public static String UPDATE_USERS_LIST = "updateUsersList";
    public static String messageDelivered = "messageDelivered";
    public static final String subscriptionPackageId = "subscriptionPackageId";
    public static final String photoGrapherIds = "photoGrapherIds";
    public static final String nationalityList = "getNationalityList";



    public static final String voucherCodeId = "voucherCodeId";
    public static final String amount = "amount";
    public static final String subscriptionEndDate = "subscriptionEndDate";
    public static final String voucherName = "voucherName";
    public static String discountType = "discountType";
    public static final String stripeToken = "stripeToken";
    public static final String channelId = "channelId";
    public static final String roomId = "roomId";

    public static final String CHAT_SENDER_NAME = "name";
    public static final String CHAT_SENDER_USERNAME = "username";
    public static final String CHAT_SENDER_IMAGE = "image";
    public static final String CHAT_SENDER_ID = "senderId";
    public static final String CHAT_CHANNEL_ID = "channelId";
    public static final String CHAT_MESSAGE_TEXT = "content";
    public static final String CHAT_DATE = "date";
    public static final String CHAT_TIMEBIN = "timebin";
    public static final String CHAT_TYPE = "type";
    public static final String ONLINE_STATUS = "online_status";

    public static final String CHAT_GROUP_ID = "groupId";
    public static final String CHAT_GROUP_NAME = "groupName";
    public static final String CHAT_GROUP_IMAGE = "groupImage";
    public static final String mapView = "mapView";
    public static String defaultmapView = "defaultmapView";
    public static String fileLog = "fileLog";
    public static final String customData = "customData";

    public static final String gpsDevices = "gpsDevices";
    public static final String softwareVersion = "softwareVersion";
    public static final String numberOfLapsFound = "numberOfLapsFound";
    public static final String trackVersionId = "trackVersionId";
    public static final String youtubeURL = "youtubeURL";
    public static final String clothModelName = "clothModelName";
    public static final String clothYearName = "clothYearName";
    public static final String clothColorName = "clothColorName";
    public static final String trackName = "trackName";
    public static final String lapData = "lapData";
    public static final String verificationEmailId = "verificationEmailId";
    public static String isFirstTime = "isFirstTime";
    public static String country = "country";
    public static String version = "version";
    public static String startLeft = "startLeft";
    public static String startRight = "startRight";
    public static String left_bottom = "left_bottom";
    public static String right_top = "right_top";
    public static String userLapImageData = "userLapImageData";
    public static final String userLapId = "userLapId";
    public static String imageLog = "imageLog";
    public static String vehicleTyreModelName = "vehicleTyreModelName";
    public static final String selectedValue = "selectedValue";
    public static final String checkLapTimeCSV = "checkLapTimeCSV";
    public static final String csv_file = "csv_file";
    public static String index = "index";
    public static String totalLaps = "totalLaps";
    public static final String anotherUserId = "anotherUserId";
    public static String laptimes = "laptimes";
    public static String list = "list";
    public static String userSubscriptionName = "userSubscriptionName";
    public static String userSubscriptionId = "userSubscriptionId";
    public static String userSubscriptionComparison = "userSubscriptionComparison";
    public static String userSubscriptionSessions = "userSubscriptionSessions";
    public static String userSubscriptionLaps = "userSubscriptionLaps";
    public static String userSubscriptionGarage = "userSubscriptionGarage";
    public static String userSubscriptionMedia = "userSubscriptionMedia";
    public static final String userAlbumCommentId = "userAlbumCommentId";
    public static String likedAlbum = "likedAlbum";
    public static String likedAlbumCount = "likedAlbumCount";
    public static String commentAlbumCount = "commentAlbumCount";
    public static String channelData = "channelData";
    public static final String userAlbumCommentReplyId = "userAlbumCommentReplyId";
    public static final String albumCommentReplyLike_api = "albumCommentReplyLike";
    public static final String albumCommentReplyRemove = "albumCommentReplyRemove";
    public static final String albumName = "albumName";
    public static final String albumShareUser = "albumShareUser";
    public static final String transactionId = "transactionId";
    public static final String quantity = "quantity";
    public static final String ticketDescription = "ticketDescription";
    public static final String token = "token";
    public static final String presetsId = "presetsId";
    public static String presets = "presets";
    public static String types = "types";
    public static String totalUser = "totalUser";
    public static final String removeFriendTag = "removeFriendTag";
    public static String unread_notification="unread_notification";
    public static String home="home";
    public static String favorite="favorite";
    public static String unreadNotificationCount="unreadNotificationCount";
    public static String userLapCount="userLapCount";
    public static String userTrackRoundCount="userTrackRoundCount";
    public static String bestRanking="bestRanking";
    public static String bestSpeed="bestSpeed";
    public static String lapUploaded="lapUploaded";
    public static String absoluteBestLap="absoluteBestLap";
    public static String levelIcon="levelIcon";
    public static String locationIcon="locationIcon";
    public static String trackFavorite="trackFavorite";
    public static String rank="rank";
    public static final String advertisementId="advertisementId";
    public static final String advertisementCommentId="advertisementCommentId";
    public static String companyLogo="companyLogo";
    public static String companyName="companyName";
    public static String likedAdvertisement="likedAdvertisement";
    public static String likedAdvertisementCount="likedAdvertisementCount";
    public static String commentAdvertisementCount="commentAdvertisementCount";
    public static final String personalInfodateOfBirth="personalInfo.dateOfBirth";
    public static final String cover="cover";
    public static final String about="about";
    public static String lapTimes="lapTimes";
    public static String track="track";
    public static String trackVersions="trackVersions";
    public static String joinedGroup="joinedGroup";
    public static String invitedGroup="invitedGroup";
    public static final String dateOfRecording="dateOfRecording";
    public static final String rentVehicle="rentVehicle";
  public static final String rentVehicleTypeId="rentVehicleTypeId";
  public static final String rentVehicleBrandId="rentVehicleBrandId";
  public static final String rentVehicleModelId="rentVehicleModelId";
    public static final String selectedLaps="selectedLaps";
    public static String session="session";
    public static String lapCount="lapCount";
    public static String trackVersionData="trackVersionData";
    public static String fileUrl="fileUrl";
    public static String maxSpeed="maxSpeed";
    public static String averageSpeedToShow="averageSpeedToShow";

    public static final String lapTimeId="lapTimeId";
    public static String admin="admin";
    public static String subAdmin="subAdmin";
    public static final String memberID="memberId";
    public static final String groupAdminId="groupAdminId";
    public static String lowData="lowData";
    public static String height="height";
    public static String width="width";
    public static final String numbers="numbers";
    public static final String notes="notes";
    public static final String sessionIndex="sessionIndex";
    public static String lapIndex="lapIndex";
    public static String averageSpeed="averageSpeed";
    public static String sessionImages="sessionImages";
    public static String userInfo="userInfo";

    public static String isFav="isFav";
    public static String isHome="isHome";
    public static String rxToCheck="rxToCheck";
    public static String lxToCheck="lxToCheck";
    public static String sector="sector";
    public static String noSector="noSector";
    public static final String sessionId="sessionId";
    public static final String sessionImgId="sessionImgId";
    public static String userVehicleImage="userVehicleImage";
    public static final String updateData="updateData";
    public static String tracks="tracks";
    public static String albums="albums";
    public static String albumImages="albumImages";
    //public static final String getWeather_api = "getWeather";
    public static final String weather_api ="v1/public/yql";
    public static final String month="month";

    public static final String Speed="Speed";
    public static final String brakeOn="Brake On";
    public static final String brakeCalc="Brake Calc";
    public static final String accelerationOn="Acceleration On";
    public static final String accelerationCalc="Acceleration Calc";
    public static final String accelZ="AccelZ";
    public static final String accelX="AccelX";
    public static final String accelY="AccelY";

    public static final String lapsInTrackVersionForWeb="lapsInTrackVersionForWeb";
    public static final String lapsInTrackVersionForWebMyFriend="lapsInTrackVersionForWebMyFriend";
    public static final String getOnlineStatusByUserID="online-users-byid";
    public static final String showGroupDetails="show_group_details";

    public static final String getTrackWithTrackVersionData="lapsInTrackVersionByTrack";


    public static final String commentPost="commentPost";
}
