package com.os.fastlap.constant;

/**
 * Created by abhinava on 7/10/2017.
 */

public class FastLapConstant {
    public static String SIGNUPUSERTYPEFRAGMENT = "SignupUsertypeFragment";
    public static String SIGNUPUSERPERSONALDETAILFRAGMENT = "SignupUserPersonalDetailFragment";
    public static String SIGNUPUSEROTHERDETAILFRAGMENT = "SignupUserOtherDetailFragment";
    public static String UPLOADLAPTIMESTEP1FRAGMENT = "UploadLapTimeStep1Fragment";
    public static String UPLOADLAPTIMESTEP2FRAGMENT = "UploadLapTimeStep2Fragment";
    public static String UPLOADLAPTIMESTEP3FRAGMENT = "UploadLapTimeStep3Fragment";


    public static String SETTINGGENERALINFOFRAGMENT = "SettingGeneralInfoFragment";
    public static String CHANGEPASSWORDFRAGMENT = "ChangePasswordFragment";
    public static String CHANGEEMAILFRAGMENT = "ChangeEmailFragment";
    public static String VEHICLESETTINGFRAGMENT = "VehicleSettingFragment";
    public static String BLOCKUSERFRAGMENT = "BlockUserFragment";
    public static String MANAGEDEVICEFRAGMENT = "ManageDeviceFragment";
    public static String DEFAULTMAPVIEWFRAGMENT = "DefaultMapviewFragment";
    public static String TIMESYSTEMFRAGMENT = "TimeSystemFragment";
    public static String WEEKSTARTDAYFRAGMENT = "WeekStartDayFragment";
    public static String METRICSYSTEMFRAGMENT = "MetricSystemFragment";
    public static String DEACTIVATEACCOUNTFRAGMENT = "DeactivateAccountFragment";
    public static String TERMSCONDITIONFRAGMENT = "TermsConditionFragment";
}
