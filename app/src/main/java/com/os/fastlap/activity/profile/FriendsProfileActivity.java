package com.os.fastlap.activity.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.adapter.profile.AllFriendsProfileAdapter;
import com.os.fastlap.adapter.profile.MutualFriendsProfileAdapter;
import com.os.fastlap.adapter.profile.PendingRequestProfileAdapter;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.os.fastlap.R.id.base_toggle_icon;

/*
 * Created by anandj on 7/14/2017.
 */

public class FriendsProfileActivity extends BaseActivity implements View.OnClickListener, PendingRequestProfileAdapter.PendingRequestInterface {

    private final String TAG = FriendsProfileActivity.class.getSimpleName();
    private AppCompatImageView toggle_icon;
    private TextView toolbarheading;
    private View view;
    private ImageView img_bg;
    private RelativeLayout rr;
    private CircleImageView user_icon;
    private CoordinatorLayout coordinator_layout;
    private LinearLayout mutual_ll;
    private LinearLayout pending_ll;
    private LinearLayout footer;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    private ArrayList<ProfileMyFriendsList> mMyMutualLists;
    private ArrayList<ProfileMyFriendsList> mMyPendingRequestLists;
    private AllFriendsProfileAdapter mFriendsProfileAdapter;
    private MutualFriendsProfileAdapter mMutualFriendsProfileAdapter;
    private PendingRequestProfileAdapter pendingRequestProfileAdapter;
    private RecyclerView recycler_view_frd, recycler_view_mutual, recycler_view_pending_request;
    private Context mContext;
    private AuthAPI mAuthAPI;
    private RelativeLayout parent_mutual, parent_allfrd, parent_pendingrequest;
    private ImageView img_arrow_all, img_arrow_mutual, img_arrow_pending;
    private EditText edt_search;
    private TextView txtUsername,txtEmail;
    private TextView no_data_tv;
    TextView menu_menu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_friends_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        initView();
    }

    private void initView() {
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        no_data_tv = findViewById(R.id.no_data_tv);
        edt_search = (EditText) findViewById(R.id.edt_search);
        toggle_icon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        toolbarheading = (TextView) findViewById(R.id.toolbarheading);
        recycler_view_frd = (RecyclerView) findViewById(R.id.recycler_view_frd);
        recycler_view_mutual = (RecyclerView) findViewById(R.id.recycler_view_mutual);
        recycler_view_pending_request = (RecyclerView) findViewById(R.id.recycler_view_pending_request);
        view = findViewById(R.id.view);
        img_bg = (ImageView) findViewById(R.id.img_bg);
        rr = (RelativeLayout) findViewById(R.id.rr);
        user_icon = (CircleImageView) findViewById(R.id.user_icon);
        txtUsername = (TextView) findViewById(R.id.txt_username);
        txtEmail=(TextView)findViewById(R.id.txt_email);
        coordinator_layout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);

        footer = (LinearLayout) findViewById(R.id.footer);
        mutual_ll = (LinearLayout) findViewById(R.id.mutual_ll);
        mutual_ll.setVisibility(View.GONE);
        pending_ll = (LinearLayout) findViewById(R.id.pending_ll);
        parent_mutual = (RelativeLayout) findViewById(R.id.parent_mutual);
        parent_allfrd = (RelativeLayout) findViewById(R.id.parent_allfrd);
        parent_pendingrequest = (RelativeLayout) findViewById(R.id.parent_pendingrequest);
        img_arrow_all = (ImageView) findViewById(R.id.img_arrow_all);
        img_arrow_mutual = (ImageView) findViewById(R.id.img_arrow_mutual);
        img_arrow_pending = (ImageView) findViewById(R.id.img_arrow_pending);
        mAuthAPI = new AuthAPI(mContext);
        mMyFriendsLists = new ArrayList<>();
        mMyMutualLists = new ArrayList<>();
        mMyPendingRequestLists = new ArrayList<>();
        mFriendsProfileAdapter = new AllFriendsProfileAdapter(mContext, mMyFriendsLists);
        mMutualFriendsProfileAdapter = new MutualFriendsProfileAdapter(mContext, mMyMutualLists);
        //   pendingRequestProfileAdapter = new PendingRequestProfileAdapter(mContext, mMyPendingRequestLists);
        clickListner();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        RecyclerView.LayoutManager mutualLayoutManager = new LinearLayoutManager(mContext);
        RecyclerView.LayoutManager pendingLayoutManager = new LinearLayoutManager(mContext);
        recycler_view_frd.setLayoutManager(layoutManager);
        recycler_view_mutual.setLayoutManager(mutualLayoutManager);
        recycler_view_pending_request.setLayoutManager(pendingLayoutManager);
        recycler_view_pending_request.setNestedScrollingEnabled(false);
        recycler_view_frd.setNestedScrollingEnabled(false);
        recycler_view_mutual.setNestedScrollingEnabled(false);


        getMutualFriendList();
        getPendingRequestList();

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Util.hideKeyboard(getCurrentFocus(),mContext);
                    performSearch(v.getText().toString());
                    return true;
                }
                return false;
            }
        });
        setProfileData();

        pendingRequestProfileAdapter = new PendingRequestProfileAdapter(mContext, mMyPendingRequestLists);
        recycler_view_pending_request.setAdapter(pendingRequestProfileAdapter);
        pendingRequestProfileAdapter.OnclickListner(this);

        //   if()
    }

    private void performSearch(String query) {
        mMutualFriendsProfileAdapter.filter(query);
        mFriendsProfileAdapter.filter(query);
    }

    private void getMutualFriendList() {
        if (ProfileActivity.profileUserId.compareTo(MySharedPreferences.getPreferences(mContext, S.user_id)) != 0) {
            mutual_ll.setVisibility(View.VISIBLE);
            mAuthAPI.mutualFriends(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), ProfileActivity.profileUserId);
            mAuthAPI.myFriendList(mContext, ProfileActivity.profileUserId, MySharedPreferences.getPreferences(mContext, S.user_id));
        } else {
            //mutual_ll.setVisibility(View.GONE);
            mAuthAPI.myFriendList(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), "");
        }

    }

    public void getPendingRequestList() {
        if (ProfileActivity.profileUserId.compareTo(MySharedPreferences.getPreferences(mContext, S.user_id)) != 0) {
            //pending_ll.setVisibility(View.GONE);
        } else {
            mAuthAPI.pendingRequest(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
        }
    }

    private void setProfileData() {
        try {
            if(!ProfileActivity.profileUserCoverImage.equalsIgnoreCase("img/cover.png")){
                Util.setImageLoader(S.IMAGE_BASE_URL + ProfileActivity.profileUserCoverImage, img_bg, getApplicationContext());
            }
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + ProfileActivity.profileUserProfileImage, user_icon, Util.getImageLoaderOption(mContext));
            txtUsername.setText(ProfileActivity.profileUserName);
            txtEmail.setText("("+ProfileActivity.profileEmail+")");
        }
        catch (Exception e){e.printStackTrace();
        }

    }


    private void clickListner() {
        toggle_icon.setOnClickListener(this);
        parent_allfrd.setOnClickListener(this);
        parent_mutual.setOnClickListener(this);
        parent_pendingrequest.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading("Friend Profile");
        FastLapApplication.mCurrentContext = mContext;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case base_toggle_icon:
                finish();
                break;
            case R.id.menu_menu:
                Intent intent = new Intent(this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case R.id.parent_mutual:
                if (recycler_view_mutual.getVisibility() == View.VISIBLE) {
                    img_arrow_mutual.setImageResource(R.drawable.new_right_back_red);
                    recycler_view_mutual.setVisibility(View.GONE);

                } else {
                    img_arrow_mutual.setImageResource(R.drawable.new_down_back_red);
                    recycler_view_mutual.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.parent_allfrd:
                if (recycler_view_frd.getVisibility() == View.VISIBLE || no_data_tv.getVisibility() == View.VISIBLE) {
                    img_arrow_all.setImageResource(R.drawable.new_right_back_red);
                    recycler_view_frd.setVisibility(View.GONE);
                    no_data_tv.setVisibility(View.GONE);
                } else {
                    img_arrow_all.setImageResource(R.drawable.new_down_back_red);
                    if (mMyFriendsLists.size() == 0) {
                        no_data_tv.setVisibility(View.VISIBLE);
                    } else
                        recycler_view_frd.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.parent_pendingrequest:
                if (recycler_view_pending_request.getVisibility() == View.VISIBLE) {
                    img_arrow_pending.setImageResource(R.drawable.new_right_back_red);
                    recycler_view_pending_request.setVisibility(View.GONE);
                } else {
                    img_arrow_pending.setImageResource(R.drawable.new_down_back_red);
                    recycler_view_pending_request.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    /* myFriendList webservice response */
    public void myFriendListWebserviceResponse(String response) {
        try {
            mMyFriendsLists.clear();
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                if (jsonArray.length() == 0) {
                    recycler_view_frd.setVisibility(View.GONE);
                    no_data_tv.setVisibility(View.VISIBLE);
                } else {
                    recycler_view_frd.setVisibility(View.VISIBLE);
                    no_data_tv.setVisibility(View.GONE);
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.getString(S.id));
                    profileMyFriendsList.setUserId(jsonObject1.getString(S.userId));
                    profileMyFriendsList.setStatus(jsonObject1.getString(S.status));
                    profileMyFriendsList.setIsFriend(jsonObject1.getString(S.likedFriend));

                    PersonalInfo personalInfo = new PersonalInfo();
                    JSONObject personalInfoJson = jsonObject1.getJSONObject(S.personalInfo);
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    profileMyFriendsList.setPersonalInfo(personalInfo);

                    JSONArray jsonArray1 = new JSONArray();
                    jsonArray1 = jsonObject1.getJSONArray(S.channelData);
                    if (jsonArray1.length() > 0) {
                        profileMyFriendsList.setChannalId(jsonArray1.getJSONObject(0).getString(S._id));
                        profileMyFriendsList.setChannalType(jsonArray1.getJSONObject(0).getString(S.type));
                        profileMyFriendsList.setTimeBin(jsonArray1.getJSONObject(0).getString(S.timebin));
                    }


                    if (!jsonObject1.getString(S.userId).equalsIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id))) {
                        mMyFriendsLists.add(profileMyFriendsList);
                    }


                }
                mFriendsProfileAdapter = new AllFriendsProfileAdapter(mContext, mMyFriendsLists);
                recycler_view_frd.setAdapter(mFriendsProfileAdapter);

            }/* else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* mutualFriends webservice response */
    public void mutualFriendsWebserviceResponse(String response) {
        try {
            mMyMutualLists.clear();
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.getString(S.id));
                    profileMyFriendsList.setUserId(jsonObject1.getString(S.userId));
                    profileMyFriendsList.setStatus(jsonObject1.getString(S.status));

                    PersonalInfo personalInfo = new PersonalInfo();
                    JSONObject personalInfoJson = jsonObject1.getJSONObject(S.personalInfo);
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    profileMyFriendsList.setPersonalInfo(personalInfo);

                    mMyMutualLists.add(profileMyFriendsList);

                }
                mMutualFriendsProfileAdapter = new MutualFriendsProfileAdapter(mContext, mMyMutualLists);
                recycler_view_mutual.setAdapter(mMutualFriendsProfileAdapter);

                if (mMyMutualLists.size() == 0) {
                    mutual_ll.setVisibility(View.GONE);
                } else {
                    mutual_ll.setVisibility(View.VISIBLE);
                }


            } /*else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* pending Request webservice response */
    public void userPendingRequestList(String response) {
        mMyPendingRequestLists.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                        profileMyFriendsList.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                        profileMyFriendsList.setStatus(jsonObject1.has(S.status)?jsonObject1.getString(S.status):"");

                        profileMyFriendsList.setUserId(jsonObject1.getJSONObject(S.fromUserId).getString(S._id));
                        PersonalInfo personalInfo = new PersonalInfo();
                        JSONObject personalInfoJson = jsonObject1.getJSONObject(S.fromUserId).getJSONObject(S.personalInfo);

                        personalInfo.setFirstName(personalInfoJson.has(S.firstName_response)?personalInfoJson.getString(S.firstName_response):"");
                        personalInfo.setLastName(personalInfoJson.has(S.lastName_response)?personalInfoJson.getString(S.lastName_response):"");
                        personalInfo.setVoucherCode(personalInfoJson.has(S.voucherCode_response)?personalInfoJson.getString(S.voucherCode_response):"");
                        personalInfo.setDateOfBirth(personalInfoJson.has(S.dateOfBirth_response)?personalInfoJson.getString(S.dateOfBirth_response):"");
                        personalInfo.setMobileNumber(personalInfoJson.has(S.mobileNumber_response)?personalInfoJson.getString(S.mobileNumber_response):"");
                        personalInfo.setImage(personalInfoJson.has(S.image)?personalInfoJson.getString(S.image):"");
                        personalInfo.setCoverImage(personalInfoJson.has(S.coverImage)?personalInfoJson.getString(S.coverImage):"");
                        personalInfo.setLanguage(personalInfoJson.has(S.language_response)?personalInfoJson.getString(S.language_response):"");
                        profileMyFriendsList.setPersonalInfo(personalInfo);
                        mMyPendingRequestLists.add(profileMyFriendsList);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                if (jsonArray.length() > 0) {
                    pending_ll.setVisibility(View.VISIBLE);
                } else {
                    //pending_ll.setVisibility(View.GONE);
                }
                pendingRequestProfileAdapter.notifyDataSetChanged();

            } else {
                //Util.showAlertDialog(mContext, getString(R.string.alert), msg);
                pendingRequestProfileAdapter.notifyDataSetChanged();
                //pending_ll.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void confirmClickListner(int position) {
        mAuthAPI.setConfirmPendingRequest(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mMyPendingRequestLists.get(position).getUserId(), mMyPendingRequestLists.get(position).getId(), "1");
    }

    @Override
    public void deleteClickListner(int position) {
        mAuthAPI.setConfirmPendingRequest(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mMyPendingRequestLists.get(position).getUserId(), mMyPendingRequestLists.get(position).getId(), "2");
    }

    public void pendingRequestWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
                getPendingRequestList();
                getMutualFriendList();
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    /* user friends webservice response */
    public void userFriendsWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
                getPendingRequestList();
                getMutualFriendList();
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
