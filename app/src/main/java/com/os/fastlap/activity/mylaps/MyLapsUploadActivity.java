package com.os.fastlap.activity.mylaps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.activity.SearchActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.fragment.paddock.PaddockUploadsFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONObject;

import static com.os.fastlap.R.id.viewpager;
/**
 * Created by anandj on 7/21/2017.
 */
public class MyLapsUploadActivity extends BaseActivity implements View.OnClickListener, SpinnerSelectorInterface {
    private TabLayout mTabHost;
    private ViewPager mViewpager;
    Context context;
    ImageView base_toggle_icon;
    private ViewPagerAdapter viewPagerAdapter;
    private String TAG = MyLapsUploadActivity.class.getSimpleName();
    AuthAPI authAPI;
    SpinnerSelectorInterface spinnerSelectorInterface;
    EditText searchEt;
    public String  trackVersionId;
    TextView menu_menu;
    private int pos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mylaps_activity_main);
        context = this;
        spinnerSelectorInterface = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();
        Util.changeTabsFont(mTabHost, context);

    }
    private void initView() {
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        mTabHost = (TabLayout) findViewById(R.id.tab_host);
        mViewpager = (ViewPager) findViewById(viewpager);
        searchEt = findViewById(R.id.searchEt);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        setupViewPager(mViewpager);
        mTabHost.setupWithViewPager(mViewpager);
        clickListner();

        searchEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(S.type, I.MYLAPSSEARCH);
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }
            @Override
            public void onPageSelected(int i) {
                pos=i;
                getSessionData();
            }
            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        getSessionData();
    }
    public void getSessionData() {
        authAPI.getSessionsData(context, MySharedPreferences.getPreferences(context, S.user_id));
    }
    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }
    private void setupViewPager(ViewPager viewpager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new PaddockUploadsFragment(), getString(R.string.sessions));
        viewPagerAdapter.addFragment(new PaddockUploadsFragment(), getString(R.string.laps));
        viewpager.setAdapter(viewPagerAdapter);
    }
    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        FastLapApplication.mCurrentContext = context;
    }
    public void getVehicleTyreBrandResponse(String response) {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && mViewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            if (mCurrentFragment instanceof PaddockUploadsFragment)
                ((PaddockUploadsFragment) mCurrentFragment).getVehicleTyreBrandResponse(response);
        }
    }

    public void getVehicleTyreModelResponse(String response) {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && mViewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            if (mCurrentFragment instanceof PaddockUploadsFragment)
                ((PaddockUploadsFragment) mCurrentFragment).getVehicleTyreModelResponse(response);
        }
    }

    public void getYoutubeURLData(String response, int position) {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && mViewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            if (mCurrentFragment instanceof PaddockUploadsFragment)
                ((PaddockUploadsFragment) mCurrentFragment).getYoutubeURLData(response, position);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.menu_menu:
                Intent intent = new Intent(this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }
    public void updateLapTimeResponse(String response) {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && mViewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            if (mCurrentFragment instanceof PaddockUploadsFragment){
                ((PaddockUploadsFragment) mCurrentFragment).updateLapTimeResponse(response);
                ((PaddockUploadsFragment) mCurrentFragment).showSession();
            }

        }
    }
    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.COUNTRYNAME:
                getCurrentFragment(value);
                break;
        }
    }
    public Fragment getCurrentFragment(String ch) {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && mViewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
           // if (mCurrentFragment instanceof PaddockUploadsFragment)
               // ((PaddockUploadsFragment) mCurrentFragment).getCurrentFragment(ch);
        }

        return mCurrentFragment;
    }
    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }
    public void getSessionDataResponse(String response)
    {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && mViewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            if (mCurrentFragment instanceof PaddockUploadsFragment) {
                ((PaddockUploadsFragment) mCurrentFragment).getSessionDataResponse(response);
                if(pos==0){
                    ((PaddockUploadsFragment) mCurrentFragment).showSession();
                }
                else {
                    ((PaddockUploadsFragment) mCurrentFragment).showLaps();
                }
            }

        }
    }
    public void getUpdateLapResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                getSessionData();

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
