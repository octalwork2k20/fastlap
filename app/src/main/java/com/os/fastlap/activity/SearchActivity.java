package com.os.fastlap.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.adapter.dashboard.SearchListAdapter;
import com.os.fastlap.beans.dashboardmodals.SearchBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 9/5/2017.
 */

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {
    private AppCompatImageView baseToggleIcon;
    private EditTextPlayRegular searchEt;
    private LinearLayout searchProgressLayout;
    private TextViewPlayBold searchTextTv;
    private TextView no_data_tv;
    private RecyclerView searchRecyclerView;
    AuthAPI mAuthAPI;
    Context mContext;
    ArrayList<SearchBean> searchList;
    private String TAG = SearchActivity.class.getSimpleName();
    SearchListAdapter searchListAdapter;
    int searchType = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        mAuthAPI = new AuthAPI(mContext);
        initView();
        searchType = getIntent().getIntExtra(S.type, 0);

        if (searchType == I.DASHBOARDSEARCH)
        {
            searchEt.setHint(getString(R.string.search_users_tracks_groups));
        } else if (searchType == I.MYLAPSSEARCH) {
            searchEt.setHint(getString(R.string.search_tracks_users_events));
        } else if (searchType == I.GROUPSSEARCH) {
            searchEt.setHint(getString(R.string.search_groups_tracks_users));
        } else if (searchType == I.EVENTSSEARCH) {
            searchEt.setHint(getString(R.string.search_events_tracks_users));
        } else if (searchType == I.GALLERYSEARCH) {
            searchEt.setHint(getString(R.string.search_albums_tracks_users));
        } else if (searchType == I.EXPLORESEARCH) {
            searchEt.setHint(getString(R.string.search_tracks_events_groups));
        }
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        searchEt = (EditTextPlayRegular) findViewById(R.id.search_et);
        searchProgressLayout = (LinearLayout) findViewById(R.id.search_progress_layout);
        searchTextTv = (TextViewPlayBold) findViewById(R.id.search_text_tv);
        searchRecyclerView = (RecyclerView) findViewById(R.id.search_recycler_view);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);

        searchList = new ArrayList<>();
        searchList.clear();

        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString().trim();
                if (text.length() > 0) {
                    searchTextTv.setText(getString(R.string.see_result_for) + " " + text);
                    searchProgressLayout.setVisibility(View.VISIBLE);
                    searchRecyclerView.setVisibility(View.GONE);
                    mAuthAPI.searchUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), text);
                }

            }
        });

        searchListAdapter = new SearchListAdapter(searchList, mContext);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        searchRecyclerView.setLayoutManager(layoutManager);
        searchRecyclerView.setItemAnimator(new DefaultItemAnimator());
        searchRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
        searchRecyclerView.setAdapter(searchListAdapter);

        clickListner();
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        String text = searchEt.getText().toString().trim();
        if (text.length() > 0)
        {
            mAuthAPI.searchUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), text);
        }
    }

    public void searchUserResponse(String response) {
        searchList.clear();
        searchProgressLayout.setVisibility(View.GONE);
        searchRecyclerView.setVisibility(View.VISIBLE);

        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONObject jsonObject1 = jsonObject.getJSONObject(S.data);

                if (searchType == I.DASHBOARDSEARCH) {
                    searchList.addAll(getUserData(jsonObject1));
                    searchList.addAll(getTrackData(jsonObject1));
                    searchList.addAll(getGroupData(jsonObject1));
                    searchList.addAll(getEventData(jsonObject1));
                } else if (searchType == I.MYLAPSSEARCH) {
                    searchList.addAll(getTrackData(jsonObject1));
                    searchList.addAll(getUserData(jsonObject1));
                    searchList.addAll(getEventData(jsonObject1));
                } else if (searchType == I.GROUPSSEARCH) {
                    searchList.addAll(getGroupData(jsonObject1));
                    searchList.addAll(getTrackData(jsonObject1));
                    searchList.addAll(getUserData(jsonObject1));
                } else if (searchType == I.EVENTSSEARCH) {
                    searchList.addAll(getEventData(jsonObject1));
                    searchList.addAll(getTrackData(jsonObject1));
                    searchList.addAll(getUserData(jsonObject1));
                } else if (searchType == I.GALLERYSEARCH) {
                    searchList.addAll(getAlbumData(jsonObject1));
                    searchList.addAll(getTrackData(jsonObject1));
                    searchList.addAll(getUserData(jsonObject1));
                } else if (searchType == I.EXPLORESEARCH) {
                    searchList.addAll(getTrackData(jsonObject1));
                    searchList.addAll(getEventData(jsonObject1));
                    searchList.addAll(getGroupData(jsonObject1));
                }

                if (searchList.size() > 0) {
                    no_data_tv.setVisibility(View.generateViewId());
                    searchRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    searchRecyclerView.setVisibility(View.GONE);
                }

                searchListAdapter.notifyDataSetChanged();


            } else {

                no_data_tv.setVisibility(View.VISIBLE);
                searchRecyclerView.setVisibility(View.GONE);
                //Util.showAlertDialog(mContext, getString(R.string.alert), msg);
                searchListAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private ArrayList<SearchBean> getAlbumData(JSONObject jsonObject1) {
        ArrayList<SearchBean> list = new ArrayList<>();
        try {
            JSONArray albumJsonArray = jsonObject1.getJSONArray(S.albums);

            for (int i = 0; i < albumJsonArray.length(); i++) {
                try {
                    JSONObject albumObject = albumJsonArray.getJSONObject(i);
                    SearchBean searchBean = new SearchBean();

                    searchBean.setId(albumObject.getString(S._id));
                    searchBean.setName(albumObject.getString(S.name));
                    searchBean.setEmail(albumObject.getString(S.description));
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = albumObject.getJSONArray(S.albumImages);
                    if (jsonArray.length() > 0)
                        searchBean.setImage(jsonArray.getJSONObject(0).getString(S.fileName));
                    searchBean.setIsFriend("1");
                    searchBean.setType("album");
                    searchBean.setUserType(getString(R.string.albums));

                    if (i == 0) {
                        searchBean.setFirst(true);
                    }
                    searchList.add(searchBean);
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {

        }
        return list;
    }

    private ArrayList<SearchBean> getTrackData(JSONObject jsonObject1) {
        ArrayList<SearchBean> list = new ArrayList<>();
        try {
            JSONArray trackJsonArray = jsonObject1.getJSONArray(S.tracks);

            for (int i = 0; i < trackJsonArray.length(); i++) {
                try {
                    JSONObject trackObject = trackJsonArray.getJSONObject(i);
                    SearchBean searchBean = new SearchBean();

                    searchBean.setId(trackObject.getString(S._id));
                    searchBean.setName(trackObject.getString(S.name));
                    searchBean.setEmail(trackObject.getJSONObject(S.contactDetails).getString(S.email));
                    searchBean.setImage(trackObject.getString(S.image));
                    searchBean.setIsFriend("1");
                    searchBean.setType("track");
                    searchBean.setUserType(getString(R.string.tracks));

                    if (i == 0) {
                        searchBean.setFirst(true);
                    }
                    list.add(searchBean);
                } catch (Exception e) {

                }
            }

        } catch (Exception e) {

        }
        return list;
    }

    private ArrayList<SearchBean> getEventData(JSONObject jsonObject1) {
        ArrayList<SearchBean> list = new ArrayList<>();
        try {
            JSONArray eventJsonArray = jsonObject1.getJSONArray(S.events);

            for (int i = 0; i < eventJsonArray.length(); i++) {
                try {
                    JSONObject eventObject = eventJsonArray.getJSONObject(i);
                    SearchBean searchBean = new SearchBean();

                    searchBean.setId(eventObject.getString(S._id));
                    searchBean.setName(eventObject.getString(S.name));
                    searchBean.setEmail(eventObject.getString(S.name));
                    searchBean.setImage(eventObject.getString(S.image));
                    searchBean.setIsFriend("1");
                    searchBean.setType("event");
                    searchBean.setUserType(getString(R.string.events));

                    if (i == 0) {
                        searchBean.setFirst(true);
                    }
                    list.add(searchBean);
                } catch (Exception e) {
                    e.printStackTrace();
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private ArrayList<SearchBean> getGroupData(JSONObject jsonObject1) {
        ArrayList<SearchBean> list = new ArrayList<>();
        try {
            JSONArray groupJsonArray = jsonObject1.getJSONArray(S.groups);

            for (int i = 0; i < groupJsonArray.length(); i++) {
                try {
                    JSONObject groupObject = groupJsonArray.getJSONObject(i);
                    SearchBean searchBean = new SearchBean();

                    searchBean.setId(groupObject.getString(S._id));
                    searchBean.setName(groupObject.getString(S.name));
                    searchBean.setEmail(groupObject.getString(S.description));
                    searchBean.setImage(groupObject.getString(S.image));
                    searchBean.setIsFriend(groupObject.getString(S.addedGroup));
                    searchBean.setOwnerId(groupObject.getString(S.userId));
                    searchBean.setType("group");
                    searchBean.setUserType(getString(R.string.groups));
                    if (i == 0) {
                        searchBean.setFirst(true);
                    }

                    list.add(searchBean);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;


    }

    private ArrayList<SearchBean> getUserData(JSONObject jsonObject1) {
        ArrayList<SearchBean> list = new ArrayList<>();
        try {

            JSONArray userJsonArray = jsonObject1.getJSONArray(S.users);

            for (int i = 0; i < userJsonArray.length(); i++) {
                try {
                    JSONObject userObject = userJsonArray.getJSONObject(i);
                    SearchBean searchBean = new SearchBean();

                    searchBean.setId(userObject.getString(S._id));
                    searchBean.setName(userObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userObject.getJSONObject(S.personalInfo).getString(S.lastName_response));
                    searchBean.setEmail(userObject.getString(S.email));
                    searchBean.setImage(userObject.getJSONObject(S.personalInfo).getString(S.image));
                    searchBean.setIsFriend(userObject.getString(S.likedFriend));
                    searchBean.setType("user");
                    searchBean.setUserType(getString(R.string.users));

                    if (i == 0) {
                        searchBean.setFirst(true);
                    }

                    list.add(searchBean);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
