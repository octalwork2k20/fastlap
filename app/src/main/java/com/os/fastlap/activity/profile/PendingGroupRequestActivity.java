package com.os.fastlap.activity.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.profile.PendingRequestProfileAdapter;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 12/19/2017.
 */

public class PendingGroupRequestActivity extends BaseActivity implements View.OnClickListener, PendingRequestProfileAdapter.PendingRequestInterface {
    Context mContext;
    AuthAPI authAPI;
    private AppCompatImageView baseToggleIcon;
    private RecyclerView recyclerView;
    private TextViewPlayRegular noDataTv;
    PendingRequestProfileAdapter pendingRequestProfileAdapter;
    private ArrayList<ProfileMyFriendsList> mMyPendingRequestLists;
    private String TAG = PendingGroupRequestActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_group_request_activity);
        mContext = this;
        authAPI = new AuthAPI(mContext);
        initView();

        RequestList();
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        noDataTv = (TextViewPlayRegular) findViewById(R.id.no_data_tv);

        mMyPendingRequestLists = new ArrayList<>();


        pendingRequestProfileAdapter = new PendingRequestProfileAdapter(mContext, mMyPendingRequestLists);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(pendingRequestProfileAdapter);
        pendingRequestProfileAdapter.OnclickListner(this);

        clickListner();
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == baseToggleIcon) {
            finish();
        }

    }

    private void RequestList() {
        authAPI.getPendingGroupRequest(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
    }

    @Override
    public void confirmClickListner(int position) {
        authAPI.sendGroupRequestAction(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mMyPendingRequestLists.get(position).getUserId(), "1");
    }

    @Override
    public void deleteClickListner(int position) {
        authAPI.sendGroupRequestAction(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mMyPendingRequestLists.get(position).getUserId(), "0");
    }

    public void getPendingRequestResponse(String response) {
        mMyPendingRequestLists.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.getString(S._id));
                    profileMyFriendsList.setStatus(jsonObject1.getString(S.status));

                    profileMyFriendsList.setUserId(jsonObject1.getJSONObject(S.groupId).getString(S._id));

                    PersonalInfo personalInfo = new PersonalInfo();
                    personalInfo.setFirstName(jsonObject1.getJSONObject(S.groupId).getString(S.name));
                    personalInfo.setLastName("");
                    personalInfo.setImage(jsonObject1.getJSONObject(S.groupId).getString(S.image));
                    personalInfo.setCoverImage(jsonObject1.getJSONObject(S.groupId).getString(S.coverImage));
                    profileMyFriendsList.setPersonalInfo(personalInfo);

                    mMyPendingRequestLists.add(profileMyFriendsList);

                }

                pendingRequestProfileAdapter.notifyDataSetChanged();

            } else {
                //Util.showAlertDialog(mContext, getString(R.string.alert), msg);
                pendingRequestProfileAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.group_requests));
    }

    public void getRequestStatusResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                RequestList();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
