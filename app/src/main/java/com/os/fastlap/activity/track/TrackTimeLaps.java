package com.os.fastlap.activity.track;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.paddock.PaddockMyLapAdapter;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 9/18/2017.
 */

public class TrackTimeLaps extends BaseActivity implements View.OnClickListener {

    Context context;
    private AppCompatImageView baseToggleIcon;
    AuthAPI authAPI;
    ArrayList<BeanTimeLap> lapListing;
    private String TAG = TrackTimeLaps.class.getSimpleName();
    TextView no_data_tv;

    private PaddockMyLapAdapter paddockMyLapsAdapter;
    private ArrayList<PaddockMyLapsParentModel> my_laps_list;
    private ExpandableListView expandable_list_my_laps;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_time_laps);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();

        authAPI.getTrackWithTrackVersionData(context, MySharedPreferences.getPreferences(context, S.user_id),getIntent().getStringExtra(S.TrackId));
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);


        lapListing = new ArrayList<>();
        lapListing.clear();

        my_laps_list = new ArrayList<>();
        my_laps_list.clear();

        expandable_list_my_laps = findViewById(R.id.expandable_list_my_laps);

        paddockMyLapsAdapter = new PaddockMyLapAdapter(context, my_laps_list);
        expandable_list_my_laps.setAdapter(paddockMyLapsAdapter);


        expandable_list_my_laps.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                setListViewHeight(expandableListView, i);
                return false;
            }
        });

        expandable_list_my_laps.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandable_list_my_laps.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });


        clickListner();
    }

    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();
            if (group != -1) {
                if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                    for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                        View listItem = listAdapter.getChildView(i, j, false, null,
                                listView);
                        listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                        totalHeight += listItem.getMeasuredHeight();

                    }
                }
            }

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }

    public void getMyLapsWebserviceResponse(String response)
    {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                my_laps_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++)
                {

                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    PaddockMyLapsParentModel paddockMyLapsParentModel = new PaddockMyLapsParentModel();
                    paddockMyLapsParentModel.setUserLapId(jsonObject1.getString(S._id));
                    paddockMyLapsParentModel.setMyLapCount(jsonObject1.has(S.myLapCount)?jsonObject1.getString(S.myLapCount):"");
                    paddockMyLapsParentModel.setBestPosition(jsonObject1.has(S.bestPosition)?jsonObject1.getString(S.bestPosition):"");
                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2 = jsonObject1.getJSONArray(S.trackVersionData).getJSONObject(0);
                    JSONObject trackObject=jsonObject2.getJSONObject(S.trackId);
                    if(trackObject!=null&&trackObject.length()>0){

                        paddockMyLapsParentModel.setTrack_id(trackObject.has(S._id)?trackObject.getString(S._id):"");
                        paddockMyLapsParentModel.setTrackName(trackObject.has(S.name)?trackObject.getString(S.name):"");
                        paddockMyLapsParentModel.setTrackImage(trackObject.has(S.image)?trackObject.getString(S.image):"");
                        paddockMyLapsParentModel.setTracklocation(trackObject.has(S.location_response)?trackObject.getString(S.location_response):"");
                        paddockMyLapsParentModel.setTrackLength(trackObject.has(S.trackLength)?trackObject.getString(S.trackLength):"");

                        paddockMyLapsParentModel.setTotalTurns(trackObject.getJSONObject(S.turns).getString(S.totalTurns));
                        //   paddockMyLapsParentModel.setTotalLapRecord(jsonObject2.getJSONObject(S.trackId).getString(S.totalLapRecords));
                        paddockMyLapsParentModel.setTrackurl(trackObject.getJSONObject(S.contactDetails).getString(S.url));
                        paddockMyLapsParentModel.setTrackMobile(trackObject.getJSONObject(S.contactDetails).getString(S.mobileNumber_response));
                        paddockMyLapsParentModel.setTrackEmail(trackObject.getJSONObject(S.contactDetails).getString(S.email));
                        paddockMyLapsParentModel.setTrackCountry(trackObject.has(S.country)?trackObject.getString(S.country):"");

                    }
                    paddockMyLapsParentModel.setTrackVersionName(jsonObject2.has(S.name)?trackObject.getString(S.name):"");
                    paddockMyLapsParentModel.setTrackVersionId(jsonObject2.has(S._id)?trackObject.getString(S._id):"");

                    JSONArray userjsonArray = jsonObject1.getJSONArray(S.lapData);
                    ArrayList<BeanTimeLap> userList = new ArrayList<>();

                    /*!jsonObject1.getString(S._id).equalsIgnoreCase(MySharedPreferences.getPreferences(context, S.user_id));*/

                    for (int j = 0; j < userjsonArray.length(); j++)
                    {
                        try {
                            JSONObject lapObject = userjsonArray.getJSONObject(j);
                            BeanTimeLap beanTimeLap = new BeanTimeLap();

                            Log.e("tag","userID"+lapObject.getString(S._id));
                            Log.e("tag","userID"+MySharedPreferences.getPreferences(context, S.user_id));

                            if(lapObject.getString("status").equalsIgnoreCase("1"))
                            {
                                beanTimeLap.setTimeLap_Id(lapObject.has(S._id)?lapObject.getString(S._id):"");
                                beanTimeLap.setLapId(lapObject.has(S._id)?lapObject.getString(S._id):"");
                                beanTimeLap.setTrackVersionId(lapObject.has(S.trackVersionId)?lapObject.getString(S.trackVersionId):"");

                                beanTimeLap.setTime(lapObject.has(S.time)?lapObject.getString(S.time):"");
                                beanTimeLap.setFileUrl(lapObject.has(S.fileUrl)?lapObject.getString(S.fileUrl):"");
                                beanTimeLap.setMaxSpeed(lapObject.has(S.maxSpeed)?lapObject.getString(S.maxSpeed):"");

                                JSONObject lapTimeId=lapObject.has(S.lapTimeId)?lapObject.getJSONObject(S.lapTimeId):null;
                                if(lapTimeId!=null&&lapTimeId.length()>0){
                                    beanTimeLap.setGroupTypeId(lapTimeId.has(S.groupTypeId)?lapTimeId.getString(S.groupTypeId):"");
                                    beanTimeLap.setLapDate(lapTimeId.has(S.dateOfRecording)?lapTimeId.getString(S.dateOfRecording):"");
                                    beanTimeLap.setVehicleModelName(lapTimeId.getJSONObject(S.userVehicleId).getJSONObject(S.vehicleModelId).getString(S.vehicleModelName));
                                }
                                JSONObject userJsonObject = lapObject.has(S.userId)?lapObject.getJSONObject(S.userId):null;

                                beanTimeLap.setUserId(userJsonObject.getString(S._id));
                                if (TextUtils.isEmpty(paddockMyLapsParentModel.getTrackBestPosition()) || paddockMyLapsParentModel.getTrackBestPosition().equalsIgnoreCase("0")) {
                                    if (userJsonObject.getString(S._id).compareToIgnoreCase(MySharedPreferences.getPreferences(context, S.user_id)) == 0) {
                                        paddockMyLapsParentModel.setTrackBestPosition((j + 1) + "");
                                    }
                                }
                                beanTimeLap.setUserImage(userJsonObject.getJSONObject(S.personalInfo).getString(S.image));
                                beanTimeLap.setUserName(userJsonObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userJsonObject.getJSONObject(S.personalInfo).getString(S.lastName_response));
                                userList.add(beanTimeLap);
                                /*if(lapObject.getString(S._id).equalsIgnoreCase(MySharedPreferences.getPreferences(context, S.user_id))){

                                }*/

                            }

                        } catch (Exception e) {
                          e.printStackTrace();
                        }
                    }

                    if (userList.size() > 0) {
                        paddockMyLapsParentModel.setMyLapsChildList(userList);
                        if (jsonObject2.getJSONObject(S.trackId).getString(S._id).compareToIgnoreCase(TrackDetailActivity.trackId) == 0) {
                            my_laps_list.add(paddockMyLapsParentModel);
                            paddockMyLapsAdapter.notifyDataSetChanged();
                            setListViewHeight(expandable_list_my_laps, -1);
                        }
                    }
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private void getListData() {
        paddockMyLapsAdapter.notifyDataSetChanged();
        setListViewHeight(expandable_list_my_laps, -1);
    }
    @Override
    protected void onResume() {
        super.onResume();
        FastLapApplication.mCurrentContext = context;
        setToolbarHeading(getString(R.string.time_laps));
    }
}
