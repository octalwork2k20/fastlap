package com.os.fastlap.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.dashboard.DashBoardPostsAdapter;
import com.os.fastlap.adapter.dashboard.PreferedTrackUserAdapter;
import com.os.fastlap.adapter.dashboard.PreferedTracksAdapter;
import com.os.fastlap.beans.BeanAdvertisement;
import com.os.fastlap.beans.BeanPreferedTrack;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.LapTime;
import com.os.fastlap.beans.UserDataBeans;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.beans.dashboardmodals.CommentBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardFeelingIdBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostImagesBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostListBean;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.beans.dashboardmodals.FriendIdBean;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.TrackData;
import com.os.fastlap.beans.gragemodals.VehicleBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.MyApiEndpointInterface;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import tech.gusavila92.apache.commons.codec.binary.Base64;

import static com.os.fastlap.util.constants.S.status;
import static com.os.fastlap.util.constants.S.user_id;


/*
 * Created by abhinava on 7/11/2017.
 */

public class DashboardActivity extends BaseActivity implements View.OnClickListener, DashBoardPostsAdapter.OnItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, PreferedTracksAdapter.OnClickOnPreferList, SpinnerSelectorInterface {
    private ImageView mylap_icon;
    private ImageView exploreIcon;
    private RelativeLayout add_post_ll;
    private RecyclerView preferTrackRecyclerView;
    private RecyclerView optionListview;
    private CircleImageView userProfileImg;
    private TextView post_edittext;
    private TextView no_data_tv;
    private EditText searchEt;
    private RecyclerView postListview;
    private LinearLayout add_post_parent_ll;
    private ArrayList<DashboardPostListBean> mPostListBeen = new ArrayList<>();
    private DashBoardPostsAdapter dashBoardPostsAdapter;
    private Context context;
    private SharingDeleget sharingDeleget;
    private AuthAPI mAuthAPI;
    private LinearLayoutManager mLinearLayoutManager;
    private LinearLayoutManager mLinearLayoutManager1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private static final int PAGE_START = 1;
    private int currentPage = 1;
    private ProgressBar mProgressBar;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private NestedScrollView mNestedScrollView;
    public static final String TAG = DashboardActivity.class.getSimpleName();
    ArrayList<BeanPreferedTrack> preferedTrackList;
    ArrayList<BeanPreferedTrack.BeanTrackUserData> preferedUserData = new ArrayList<>();
    PreferedTrackUserAdapter preferedTrackUserAdapter;
    PreferedTracksAdapter preferedTracksAdapter;
    private CallbackManager callbackManager;
    int width;
    public static ArrayList<BeanAdvertisement> advertisementArrayList;
    SwipyRefreshLayout swipyrefreshlayout;

    private ArrayList<BeanVehicleType> vehicleTypes_list;
    private ArrayList<BeanVehicleType> vehicleBrand_list;
    private ArrayList<BeanVehicleType> vehicleYear_list;
    private ArrayList<BeanVehicleType> trackCondition_list;
    String vehicleTypeId = "";
    String vehicleBrandID = "";
    String fromVehicleYearId = "";
    String toVehicleYearId = "";
    String trackCondition = "";
    SpinnerSelectorInterface spinnerSelectorInterface;
    TextViewPlayRegular classTv;
    TextViewPlayRegular categoryTv;
    TextViewPlayRegular brandTv;
    TextViewPlayRegular fromYearTv;
    TextViewPlayRegular toYearTv;
    TextViewPlayRegular trackConditionTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.dashboard_activity);
        context = this;
        spinnerSelectorInterface = this;
        FastLapApplication.mCurrentContext = context;
        initview();
        callRefreshDashboard(true, currentPage);
        checkRequestPermission();
        autoOnGps();
        mAuthAPI.getAdvertisementData(context, MySharedPreferences.getPreferences(context, S.user_id));
        mAuthAPI.userSubscribeList(context, MySharedPreferences.getPreferences(context, S.user_id));
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkUserSubcription();
    }

    private void initview() {
        mAuthAPI = new AuthAPI(context);
        mProgressBar = new ProgressBar(context);
        mNestedScrollView = (NestedScrollView) findViewById(R.id.nestedscrollview);
        // mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mylap_icon = (ImageView) findViewById(R.id.mylap_icon);
        searchEt = (EditText) findViewById(R.id.searchEt);
        exploreIcon = findViewById(R.id.explore_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        preferTrackRecyclerView = (RecyclerView) findViewById(R.id.preferTrackRecyclerView);
        optionListview = (RecyclerView) findViewById(R.id.option_listview);
        userProfileImg = (CircleImageView) findViewById(R.id.user_profile_img);
        postListview = (RecyclerView) findViewById(R.id.post_listview);
        add_post_parent_ll = (LinearLayout) findViewById(R.id.add_post_parent_ll);
        post_edittext = (TextView) findViewById(R.id.post_edittext);
        add_post_ll = (RelativeLayout) findViewById(R.id.add_post_ll);
        swipyrefreshlayout = findViewById(R.id.swipyrefreshlayout);

        preferedTrackList = new ArrayList<>();
        preferedTrackList.clear();
        advertisementArrayList = new ArrayList<>();
        advertisementArrayList.clear();
        clickListner();


        dashBoardPostsAdapter = new DashBoardPostsAdapter(mPostListBeen, advertisementArrayList, context, sharingDeleget, this, this);
        mLinearLayoutManager = new LinearLayoutManager(context);
        postListview.setLayoutManager(mLinearLayoutManager);
        postListview.setItemAnimator(new DefaultItemAnimator());
        postListview.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        postListview.setAdapter(dashBoardPostsAdapter);

        final SkeletonScreen skeletonScreen1 = Skeleton.bind(postListview)
                .adapter(dashBoardPostsAdapter)
                .shimmer(true)
                .angle(20)
                .frozen(true)
                .duration(1200)
                .count(10)
                .load(R.layout.postlistitem_skeletons)
                .show(); //default count is 10
        postListview.postDelayed(new Runnable() {
            @Override
            public void run() {
                skeletonScreen1.hide();
            }
        }, 3000);


        postListview.setNestedScrollingEnabled(false);

        preferedTrackUserAdapter = new PreferedTrackUserAdapter(preferedUserData, context);
        mLinearLayoutManager = new LinearLayoutManager(context);
        optionListview.setLayoutManager(mLinearLayoutManager);
        optionListview.setItemAnimator(new DefaultItemAnimator());
        optionListview.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        optionListview.setAdapter(preferedTrackUserAdapter);

        preferedTracksAdapter = new PreferedTracksAdapter(preferedTrackList, context);
        mLinearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, true);
        preferTrackRecyclerView.setLayoutManager(mLinearLayoutManager);
        preferTrackRecyclerView.setItemAnimator(new DefaultItemAnimator());
        preferTrackRecyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL));
        preferTrackRecyclerView.setAdapter(preferedTracksAdapter);
        preferedTracksAdapter.SetOnclickList(this);

        searchEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(S.type, I.DASHBOARDSEARCH);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

        swipyrefreshlayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction)
            {
                Log.d("DashboardActivity", "Refresh triggered at "
                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPage = 1;
                    callRefreshDashboard(false, currentPage);
                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                    callRefreshDashboard(false, currentPage);
                }

            }
        });

        postListview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
    }

    public void callRefreshDashboard(boolean refreshstatus, int currentPage) {
        mAuthAPI.postListing(context, MySharedPreferences.getPreferences(context, S.user_id), refreshstatus, String.valueOf(currentPage));
    }
    private void clickListner() {
        exploreIcon.setOnClickListener(this);
        add_post_parent_ll.setOnClickListener(this);
        userProfileImg.setOnClickListener(this);
        mylap_icon.setOnClickListener(this);
        post_edittext.setOnClickListener(this);
        add_post_ll.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("tag_octal","user-id"+MySharedPreferences.getPreferences(context, S.user_id));
        FastLapApplication.mCurrentContext = context;
        setBottomMenu(I.DASHBOARD_SCREEN);
        Glide.with(mContext).load(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image)).into(userProfileImg);
        mAuthAPI.getPreferedTrackData(context, MySharedPreferences.getPreferences(context, S.user_id));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.explore_icon:
                Util.startNewActivity(DashboardActivity.this, ExploreActivity.class, false);
                break;
            case R.id.add_post_parent_ll:
            case R.id.post_edittext:
            case R.id.add_post_ll:
                Intent intentPost = new Intent(DashboardActivity.this, AddPostActivity.class);
                startActivityForResult(intentPost, 1);
                break;
            case R.id.user_profile_img:
                Intent intent = new Intent(context, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.DASHBOARD_SCREEN);
                bundle.putString(S.user_id, MySharedPreferences.getPreferences(context, S.user_id));
                bundle.putString("status", "1");
                intent.putExtras(bundle);
                context.startActivity(intent);
                break;
            case R.id.mylap_icon:
                Util.startNewActivity(DashboardActivity.this, MyLapsActivity.class, false);
                break;
        }
    }

    private void autoOnGps()
    {
        FastLapApplication.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        FastLapApplication.mGoogleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(FastLapApplication.mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(DashboardActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }
    public void postListingWebserviceResponse(String response) {
        Log.d("Tag","response: "+response);
        Log.e("Tag","isRefreshing: "+swipyrefreshlayout.isRefreshing());

        if(swipyrefreshlayout.isRefreshing()&&swipyrefreshlayout!=null){
            swipyrefreshlayout.setRefreshing(false);
        }
        if (currentPage == 1) {
            mPostListBeen.clear();
            Util.deleteCache(mContext);
        } else
            mProgressBar.setVisibility(View.GONE);
        try {
            currentPage++;
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    DashboardPostListBean dashboardPostListBean = new DashboardPostListBean();
                    dashboardPostListBean.set_id(jsonObject1.getString(S._id));
                    Object object=jsonObject1.has(S.userId)?jsonObject1.get(S.userId):"";

                    if(object instanceof JSONObject){
                        JSONObject userJson = jsonObject1.getJSONObject(S.userId);
                        UserDataBeans userDataBeans = new UserDataBeans();
                        userDataBeans.set_id(userJson.has(S._id)?userJson.getString(S._id):"");
                        userDataBeans.setEmail(userJson.has(S.email)?userJson.getString(S.email):"");
                        userDataBeans.setUsername(userJson.has(S.nickname)?userJson.getString(S.nickname):"");

                        JSONObject personalInfoJson = userJson.getJSONObject(S.personalInfo);
                        PersonalInfo personalInfo = new PersonalInfo();
                        personalInfo.setLanguage(personalInfoJson.has(S.language_response)?personalInfoJson.getString(S.language_response):"");

                        personalInfo.setCoverImage(personalInfoJson.has(S.coverImage)?personalInfoJson.getString(S.coverImage):"");
                        personalInfo.setImage(personalInfoJson.has(S.image)?personalInfoJson.getString(S.image):"");
                        personalInfo.setMobileNumber(personalInfoJson.has(S.mobileNumber_response)?personalInfoJson.getString(S.mobileNumber_response):"");
                        personalInfo.setDateOfBirth(personalInfoJson.has(S.dateOfBirth_response)?personalInfoJson.getString(S.dateOfBirth_response):"");
                        personalInfo.setVoucherCode(personalInfoJson.has(S.voucherCode_response)?personalInfoJson.getString(S.voucherCode_response):"");
                        personalInfo.setLastName(personalInfoJson.has(S.lastName_response)?personalInfoJson.getString(S.lastName_response):"");
                        personalInfo.setFirstName(personalInfoJson.has(S.firstName_response)?personalInfoJson.getString(S.firstName_response):"");

                        userDataBeans.setPersonalInfo(personalInfo);
                        JSONArray commentPostArray=jsonObject1.has(S.commentPost)?jsonObject1.getJSONArray(S.commentPost):null;
                        if(commentPostArray!=null && commentPostArray.length()>0){
                            JSONObject commentObject = commentPostArray.getJSONObject(0);
                            CommentBeans commentBeans = new CommentBeans();
                            byte[] byteArray = Base64.decodeBase64(commentObject.getString(S.comment).getBytes());
                            String decodedString = new String(byteArray);
                            commentBeans.setComment_text(decodedString);
                            JSONObject userIdObject=commentObject.getJSONObject(S.userId);
                            commentBeans.setComment_user_name(userIdObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userIdObject.getJSONObject(S.personalInfo).getString(S.lastName_response));
                            commentBeans.setComment_user_image(userIdObject.getJSONObject(S.personalInfo).getString(S.image));
                            dashboardPostListBean.setCommentBeans(commentBeans);
                        }
                        dashboardPostListBean.setUserDataBeans(userDataBeans);
                        dashboardPostListBean.setDescription(jsonObject1.has(S.description)?jsonObject1.getString(S.description):"");
                        dashboardPostListBean.setPosition_text(jsonObject1.has(S.position_text)?jsonObject1.getString(S.position_text):"");

                        dashboardPostListBean.setWeather(jsonObject1.has(S.weather)?jsonObject1.getString(S.weather):"");
                        JSONArray lapsDataArray = jsonObject1.getJSONArray("laps");
                        if(lapsDataArray.length()>0){
                            JSONObject lapsDataObject=lapsDataArray.getJSONObject(0);
                            dashboardPostListBean.setSpeed(lapsDataObject.getString("maxSpeed"));
                        }

                        JSONArray feelingJsonArray = jsonObject1.getJSONArray(S.feelingId);
                        for (int j = 0; j < feelingJsonArray.length(); j++) {
                            JSONObject feelingJson = feelingJsonArray.getJSONObject(j);
                            DashboardFeelingIdBeans dashboardFeelingIdBeans = new DashboardFeelingIdBeans();
                            dashboardFeelingIdBeans.set_id(feelingJson.has(S._id)?feelingJson.getString(S._id):"");
                            dashboardFeelingIdBeans.setStatus(feelingJson.has(S.status)?feelingJson.getInt(S.status):0);
                            dashboardFeelingIdBeans.setUnicodes(feelingJson.has(S.unicodes)?feelingJson.getString(S.unicodes):"");
                            dashboardFeelingIdBeans.setName(feelingJson.has(S.name)?feelingJson.getString(S.name):"");
                            dashboardPostListBean.setDashboardFeelingIdBeans(dashboardFeelingIdBeans);
                        }
                        JSONObject timeLapObject=jsonObject1.has(S.timeLapId)?jsonObject1.getJSONObject(S.timeLapId):null;
                        if(timeLapObject.length()>0 && timeLapObject!=null)
                        {
                            try {
                                LapTime lapTime=new LapTime();
                                lapTime.setLap_time(timeLapObject.has(S.time)?timeLapObject.getString(S.time):"");
                                lapTime.setId(timeLapObject.has(S._id)?timeLapObject.getString(S._id):"");
                                lapTime.setLap_speed(timeLapObject.has(S.maxSpeed)?timeLapObject.getString(S.maxSpeed):"");
                                lapTime.setTitle(timeLapObject.getJSONObject(S.trackId).has(S.name)?timeLapObject.getJSONObject(S.trackId).getString(S.name):"");
                                JSONObject lapTimeId=timeLapObject.getJSONObject(S.lapTimeId);
                                try {
                                    JSONObject vehicleObject=lapTimeId.has(S.userVehicleId)?lapTimeId.getJSONObject(S.userVehicleId):null;
                                    if(vehicleObject!=null&& vehicleObject.length()>0){
                                        String modelName=vehicleObject.getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName);
                                        String brandName=vehicleObject.getJSONObject(S.vehicleModelId).getString(S.vehicleModelName);
                                        lapTime.setLap_vehicle(brandName+""+modelName);
                                        dashboardPostListBean.setLapTime(lapTime);
                                    }
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    dashboardPostListBean.setLapTime(lapTime);
                                }

                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        JSONArray trackIdJsonArray = jsonObject1.getJSONArray(S.trackId);
                        for (int j = 0; j < trackIdJsonArray.length(); j++) {
                            JSONObject trackIdJson = trackIdJsonArray.getJSONObject(j);
                            TrackData trackData = new TrackData();
                            trackData.setId(trackIdJson.has(S._id)?trackIdJson.getString(S._id):"");
                            trackData.setOwnerId(trackIdJson.has(S.ownerId)?trackIdJson.getString(S.ownerId):"");
                            trackData.setName(trackIdJson.has(S.name)?trackIdJson.getString(S.name):"");
                            trackData.setImage(trackIdJson.has(S.image)?trackIdJson.getString(S.image):"");
                            trackData.setTrackLength(trackIdJson.has(S.trackLength)?trackIdJson.getInt(S.trackLength):0);
                            trackData.setCountry(trackIdJson.has(S.country)?trackIdJson.getString(S.country):"");
                            dashboardPostListBean.setTrackData(trackData);
                        }

                        JSONArray userVehicleIdJsonArray = jsonObject1.getJSONArray(S.userVehicleId);
                        for (int j = 0; j < userVehicleIdJsonArray.length(); j++) {
                            JSONObject vehicleJson = userVehicleIdJsonArray.getJSONObject(j);
                            BeanVehicleList beanVehicleList = new BeanVehicleList();
                            beanVehicleList.set_id(vehicleJson.has(S._id)?vehicleJson.getString(S._id):"");

                            VehicleTypeIdModal vehicleTypeIdModal = new VehicleTypeIdModal();
                            JSONObject vehicleTypeJson = vehicleJson.getJSONObject(S.vehicleTypeId);
                            vehicleTypeIdModal.set_id(vehicleTypeJson.has(S._id)?vehicleTypeJson.getString(S._id):"");
                            vehicleTypeIdModal.setName(vehicleTypeJson.has(S.name)?vehicleTypeJson.getString(S.name):"");

                            beanVehicleList.setVehicleTypeIdModal(vehicleTypeIdModal);

                            VehicleBrandIdModal vehicleBrandIdModal = new VehicleBrandIdModal();
                            JSONObject vehicleBrandJson = vehicleJson.getJSONObject(S.vehicleBrandId);
                            vehicleBrandIdModal.set_id(vehicleBrandJson.has(S._id)?vehicleBrandJson.getString(S._id):"");
                            vehicleBrandIdModal.setVehicleBrandName(vehicleBrandJson.has(S.vehicleBrandName)?vehicleBrandJson.getString(S.vehicleBrandName):"");
                            vehicleBrandIdModal.setVehicleTypeId(vehicleBrandJson.has(S.vehicleTypeId)?vehicleBrandJson.getString(S.vehicleTypeId):"");

                            beanVehicleList.setVehicleBrandIdModal(vehicleBrandIdModal);

                            try {
                                VehicleModelIdModal vehicleModelIdModal = new VehicleModelIdModal();
                                JSONObject vehicleModalJson = vehicleJson.getJSONObject(S.vehicleModelId);
                                vehicleModelIdModal.set_id(vehicleModalJson.has(S._id)?vehicleModalJson.getString(S._id):"");

                                vehicleModelIdModal.setVehicleBrandId(vehicleModalJson.has(S.vehicleBrandId)?vehicleModalJson.getString(S.vehicleBrandId):"");
                                vehicleModelIdModal.setVehicleTypeId(vehicleModalJson.has(S.vehicleTypeId)?vehicleModalJson.getString(S.vehicleTypeId):"");
                                vehicleModelIdModal.setVehicleModelName(vehicleModalJson.has(S.vehicleModelName)?vehicleModalJson.getString(S.vehicleModelName):"");
                                beanVehicleList.setVehicleModelIdModal(vehicleModelIdModal);
                                beanVehicleList.setDescription(vehicleJson.has(S.description)?vehicleJson.getString(S.description):"");
                                dashboardPostListBean.setBeanVehicleList(beanVehicleList);
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }

                        }

                        List<DashboardPostImagesBeans> dashboardPostImagesBeanses = new ArrayList<>();
                        JSONArray postImagesJsonArray = jsonObject1.getJSONArray(S.postImages);
                        if(postImagesJsonArray!=null && postImagesJsonArray.length()>0){
                            for (int j = 0; j < postImagesJsonArray.length(); j++) {
                                Log.e("tag","postImages");
                                JSONObject postImageJson = postImagesJsonArray.getJSONObject(j);
                                DashboardPostImagesBeans dashboardPostImagesBeans = new DashboardPostImagesBeans();
                                dashboardPostImagesBeans.set_id(postImageJson.has(S._id)?postImageJson.getString(S._id):"");

                                dashboardPostImagesBeans.setUserId(postImageJson.has(S.userId)?postImageJson.getString(S.userId):"");
                                dashboardPostImagesBeans.setPostId(postImageJson.has(S.postId)?postImageJson.getString(S.postId):"");
                                dashboardPostImagesBeans.setStatus(postImageJson.has(S.status)?postImageJson.getInt(S.status):0);
                                dashboardPostImagesBeans.setType(postImageJson.has(S.type)?postImageJson.getString(S.type):"");
                                dashboardPostImagesBeans.setFileName(postImageJson.has(S.fileName)?postImageJson.getString(S.fileName):"");
                                dashboardPostImagesBeans.setThumbName(postImageJson.has(S.thumbName)?postImageJson.getString(S.thumbName):"");
                                if (postImageJson.has(S.lowData)) {
                                    dashboardPostImagesBeans.setImageHeight(Integer.parseInt(postImageJson.getJSONObject(S.lowData).getString(S.height)));
                                    dashboardPostImagesBeans.setImageWidth(Integer.parseInt(postImageJson.getJSONObject(S.lowData).getString(S.width)));
                                }
                                dashboardPostImagesBeanses.add(dashboardPostImagesBeans);
                            }
                        }

                        JSONArray postShareUser = jsonObject1.getJSONArray(S.postShareUser);
                        List<DashboardPostShareUserModal> dashboardPostShareUserModals = new ArrayList<>();
                        for (int j = 0; j < postShareUser.length(); j++) {
                            JSONObject postShareUserJson = postShareUser.getJSONObject(j);
                            DashboardPostShareUserModal dashboardPostShareUserModal = new DashboardPostShareUserModal();

                            dashboardPostShareUserModal.setId(postShareUserJson.has(S._id)?postShareUserJson.getString(S._id):"");
                            dashboardPostShareUserModal.setPostId(postShareUserJson.has(S.postId)?postShareUserJson.getString(S.postId):"");
                            dashboardPostShareUserModal.setModified(postShareUserJson.has(S.modified)?postShareUserJson.getString(S.modified):"");
                            dashboardPostShareUserModal.setCreated(postShareUserJson.has(S.created)?postShareUserJson.getString(S.created):"");

                            JSONObject userIdJson = postShareUserJson.getJSONObject(S.userId);
                            Log.e("tag","postShareUser");

                            UserDataBeans userDataBeans1 = new UserDataBeans();
                            userDataBeans1.set_id(userIdJson.has(S._id)?userIdJson.getString(S._id):"");
                            userDataBeans1.setEmail(userIdJson.has(S.email)?userIdJson.getString(S.email):"");
                            userDataBeans1.setUsername(userIdJson.has(S.username)?userIdJson.getString(S.username):"");

                            JSONObject personalInfoJsonShare = userIdJson.getJSONObject(S.personalInfo);
                            PersonalInfo personalInfo1 = new PersonalInfo();
                            personalInfo1.setLanguage(personalInfoJsonShare.has(S.language_response)?personalInfoJsonShare.getString(S.language_response):"");
                            personalInfo1.setCoverImage(personalInfoJsonShare.has(S.coverImage)?personalInfoJsonShare.getString(S.coverImage):"");

                            personalInfo1.setCoverImage(personalInfoJsonShare.has(S.coverImage)?personalInfoJsonShare.getString(S.coverImage):"");
                            personalInfo1.setImage(personalInfoJsonShare.has(S.coverImage)?personalInfoJsonShare.getString(S.coverImage):"");
                            personalInfo1.setMobileNumber(personalInfoJsonShare.has(S.mobileNumber_response)?personalInfoJsonShare.getString(S.mobileNumber_response):"");
                            personalInfo1.setDateOfBirth(personalInfoJsonShare.has(S.dateOfBirth_response)?personalInfoJsonShare.getString(S.dateOfBirth_response):"");
                            personalInfo1.setVoucherCode(personalInfoJsonShare.has(S.voucherCode_response)?personalInfoJsonShare.getString(S.voucherCode_response):"");
                            personalInfo1.setLastName(personalInfoJsonShare.has(S.lastName_response)?personalInfoJsonShare.getString(S.lastName_response):"");
                            personalInfo1.setFirstName(personalInfoJsonShare.has(S.firstName_response)?personalInfoJsonShare.getString(S.firstName_response):"");


                            userDataBeans1.setPersonalInfo(personalInfo1);
                            dashboardPostShareUserModal.setUserDataBeans(userDataBeans1);
                            dashboardPostShareUserModals.add(dashboardPostShareUserModal);
                        }

                        JSONArray commentPostTagFriend = jsonObject1.getJSONArray(S.commentPostTagFriend);
                        List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeanses = new ArrayList<>();
                        for (int j = 0; j < commentPostTagFriend.length(); j++) {
                            JSONObject friendJson = commentPostTagFriend.getJSONObject(j);
                            DashboardCommentPostTagFriendsBeans dashboardCommentPostTagFriendsBeans = new DashboardCommentPostTagFriendsBeans();
                            dashboardCommentPostTagFriendsBeans.set_id(friendJson.has(S._id)?friendJson.getString(S._id):"");
                            dashboardCommentPostTagFriendsBeans.setUserId(friendJson.has(S.userId)?friendJson.getString(S.userId):"");
                            dashboardCommentPostTagFriendsBeans.setPostId(friendJson.has(S.postId)?friendJson.getString(S.postId):"");
                            Log.e("tag","commentPostTagFriend");

                            FriendIdBean friendIdBean = new FriendIdBean();
                            JSONObject frinedIdJson = friendJson.getJSONObject(S.friendId);
                            friendIdBean.set_id(frinedIdJson.has(S._id)?frinedIdJson.getString(S._id):"");
                            friendIdBean.setUsername(frinedIdJson.has(S.username)?frinedIdJson.getString(S.username):"");
                            friendIdBean.setEmail(frinedIdJson.has(S.email)?frinedIdJson.getString(S.email):"");;

                            PersonalInfo personalInfo1 = new PersonalInfo();
                            JSONObject personalInfoJsonInner = frinedIdJson.getJSONObject(S.personalInfo);
                            personalInfo.setLanguage(personalInfoJsonInner.has(S.language_response)?personalInfoJson.getString(S.language_response):"");
                            //personalInfo1.setLanguage(personalInfoJsonInner.getString(S.language_response));

                            personalInfo1.setCoverImage(personalInfoJsonInner.has(S.coverImage)?personalInfoJsonInner.getString(S.coverImage):"");
                            personalInfo1.setImage(personalInfoJsonInner.has(S.image)?personalInfoJsonInner.getString(S.image):"");
                            personalInfo1.setMobileNumber(personalInfoJsonInner.has(S.mobileNumber_response)?personalInfoJsonInner.getString(S.mobileNumber_response):"");
                            personalInfo1.setDateOfBirth(personalInfoJsonInner.has(S.dateOfBirth_response)?personalInfoJsonInner.getString(S.dateOfBirth_response):"");
                            personalInfo1.setVoucherCode(personalInfoJsonInner.has(S.voucherCode_response)?personalInfoJsonInner.getString(S.voucherCode_response):"");
                            personalInfo1.setLastName(personalInfoJsonInner.has(S.lastName_response)?personalInfoJsonInner.getString(S.lastName_response):"");
                            personalInfo1.setFirstName(personalInfoJsonInner.has(S.firstName_response)?personalInfoJsonInner.getString(S.firstName_response):"");
                            friendIdBean.setPersonalInfo(personalInfo1);
                            dashboardCommentPostTagFriendsBeans.setFriendIdBean(friendIdBean);
                            dashboardCommentPostTagFriendsBeanses.add(dashboardCommentPostTagFriendsBeans);
                        }
                        dashboardPostListBean.setDashboardCommentPostTagFriendsBeanses(dashboardCommentPostTagFriendsBeanses);
                        dashboardPostListBean.setDashboardPostImagesBeans(dashboardPostImagesBeanses);
                        dashboardPostListBean.setDashboardPostShareUserModals(dashboardPostShareUserModals);

                        dashboardPostListBean.setLikedPost(jsonObject1.has(S.likedPost)?jsonObject1.getString(S.likedPost):"");
                        dashboardPostListBean.setLikedPostCount(jsonObject1.has(S.likedPostCount)?jsonObject1.getString(S.likedPostCount):"");
                        dashboardPostListBean.setCommentPostCount(jsonObject1.has(S.commentPostCount)?jsonObject1.getString(S.commentPostCount):"");
                        dashboardPostListBean.setModified(jsonObject1.has(S.modified)?jsonObject1.getString(S.modified):"");
                        dashboardPostListBean.setCreated(jsonObject1.has(S.created)?jsonObject1.getString(S.created):"");
                        mPostListBeen.add(dashboardPostListBean);
                    }
                    else {
                        Log.e("tag","String");
                    }

                }
                dashBoardPostsAdapter.notifyDataSetChanged();
                loading = true;
                isLoading = false;
                if (mPostListBeen.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    postListview.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    postListview.setVisibility(View.GONE);
                }

            } else {
                if (mPostListBeen.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    postListview.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    postListview.setVisibility(View.GONE);
                }
                //Util.showAlertDialog(context, getString(R.string.alert), msg);
            }

        } catch (Exception e) {
            Log.e("tag","exception e");
            Log.e(TAG, e.toString());
            //no_data_tv.setVisibility(View.VISIBLE);
            //postListview.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClickOnLike(int position, String status) {

        mAuthAPI.postLikeByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mPostListBeen.get(position).get_id(), "", status);
    }


    public void onClickOnshare(int position) {
        mAuthAPI.postShareByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mPostListBeen.get(position).get_id());
    }

    public void onClickOnDelete(int position) {
        deletePost(mPostListBeen.get(position).get_id());
    }

    private void deletePost(String post_id) {
        mAuthAPI.deletePost(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), post_id);
    }

    public void onCLickOnFb(final int position) {
        if (mPostListBeen.get(position).getDashboardPostImagesBeans().size() == 0) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, mPostListBeen.get(position).getDescription());
            startActivity(Intent.createChooser(intent, "Share with"));
        } else {
            List<String> permissionList= Arrays.asList(("publish_actions"));
            LoginManager.getInstance().logInWithPublishPermissions(this,permissionList);
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    List<Bitmap> temp = null;
                    if (mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getType().equalsIgnoreCase(S.videoType))
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getThumbName(), ImageLoader.getInstance().getMemoryCache());
                    else
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getFileName(), ImageLoader.getInstance().getMemoryCache());

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(temp.get(0))
                            .build();

                    ArrayList<SharePhoto> photos = new ArrayList<>();
                    photos.add(photo);
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .setPhotos(photos)
                            .build();

                    ShareDialog shareDialog = new ShareDialog(DashboardActivity.this);
                    if (ShareDialog.canShow(SharePhotoContent.class)) {
                        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                    }
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Log.d("DashBoard", "====SUCCESS");
                        }

                        @Override
                        public void onCancel() {
                            Log.d("DashBoard", "====CANCEL");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d("DashBoard", "====ERROR");
                        }
                    });
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Log.e(TAG, error.toString());

                }
            });
            }
    }


    /*  user like post response*/
    public void postLikeByUserWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    // Post share webservice response
    public void sharedPostWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                startPostListingFirstPosition();
                //Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postShared_api);
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void advertisementListWebserviceResponse(String response) {
        advertisementArrayList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {
                JSONArray jsonArray = new JSONArray();
                jsonArray = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1 = jsonArray.getJSONObject(i);

                    if (jsonObject1.getString(S.type).compareToIgnoreCase("Horizontal") == 0) {
                        BeanAdvertisement beanAdvertisement = new BeanAdvertisement();
                        beanAdvertisement.setId(jsonObject1.getString(S._id));
                        beanAdvertisement.setStatus(jsonObject1.getString(S.status));
                        beanAdvertisement.setExpireDate(jsonObject1.getString(S.expiredDate));
                        beanAdvertisement.setAddURL(jsonObject1.getString(S.url));
                        beanAdvertisement.setAddImage(jsonObject1.getString(S.image));
                        beanAdvertisement.setAddDesc(jsonObject1.getString(S.description));
                        beanAdvertisement.setAddName(jsonObject1.getString(S.name));
                        beanAdvertisement.setSponsered(jsonObject1.getString("sponsered"));

                        beanAdvertisement.setCompanyLogo(jsonObject1.getString(S.companyLogo));
                        beanAdvertisement.setCompanyName(jsonObject1.getString(S.companyName));
                        beanAdvertisement.setLikedPost(jsonObject1.getString(S.likedAdvertisement));
                        beanAdvertisement.setLikeCount(jsonObject1.getString(S.likedAdvertisementCount));
                        beanAdvertisement.setCommentCount(jsonObject1.getString(S.commentAdvertisementCount));
                        advertisementArrayList.add(beanAdvertisement);
                    }
                }

                dashBoardPostsAdapter.notifyDataSetChanged();

            }/* else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,


                    }, 1);
        } else {
            FastLapApplication.location = LocationServices.FusedLocationApi.getLastLocation(FastLapApplication.mGoogleApiClient);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkRequestPermission();
                //  GoNext();
            } else {
                Util.showAlertDialog(context, getString(R.string.alert), getString(R.string.permission_message));
                finish();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == I.COMMENT_ACTIVITY_RESULT_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                String position = data.getStringExtra("postPosition");
                String comment_count = data.getStringExtra("total_comment");
                String like_count = data.getStringExtra("total_likes");
                String isLike = data.getStringExtra("isLike");
                if (isLike != null) {
                    mPostListBeen.get(Integer.parseInt(position)).setLikedPost(isLike);
                }

                mPostListBeen.get(Integer.parseInt(position)).setCommentPostCount(comment_count);
                mPostListBeen.get(Integer.parseInt(position)).setLikedPostCount(like_count);
                dashBoardPostsAdapter.notifyItemChanged(Integer.parseInt(position));
                //postEdittext.setText(postEdittext.getText().toString() + " " + vehicle_name);
            }

        }
        else if (requestCode == I.ADDPOSTACTIVTYCALLBACK && resultCode == RESULT_OK) {
            startPostListingFirstPosition();
        }
        else if (requestCode == I.ADCOMMENT_ACTIVITY_RESULT_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                String position = data.getStringExtra("postPosition");
                String comment_count = data.getStringExtra("total_comment");

                advertisementArrayList.get(Integer.parseInt(position)).setCommentCount(comment_count);
                dashBoardPostsAdapter.notifyItemChanged(Integer.parseInt(position));
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void startPostListingFirstPosition() {
        currentPage = 1;
        callRefreshDashboard(true, currentPage);
        mAuthAPI.getAdvertisementData(context, MySharedPreferences.getPreferences(context, S.user_id));
    }

    public void userSubscribeList(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = new JSONArray();
                jsonArray = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1 = jsonArray.getJSONObject(i);
                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2 = jsonObject1.getJSONObject(S.subscriptionPackageId);

                    MySharedPreferences.setPreferences(context, jsonObject2.getString(S.name), S.userSubscriptionName);
                    MySharedPreferences.setPreferences(context, jsonObject2.getString(S._id), S.userSubscriptionId);
                    MySharedPreferences.setPreferences(context, jsonObject2.getString(S.comparison), S.userSubscriptionComparison);
                    MySharedPreferences.setPreferences(context, jsonObject2.getString(S.sessions), S.userSubscriptionSessions);
                    MySharedPreferences.setPreferences(context, jsonObject2.getString(S.laps), S.userSubscriptionLaps);
                    MySharedPreferences.setPreferences(context, jsonObject2.has(S.garage)?jsonObject2.getString(S.garage):"", S.userSubscriptionGarage);
                    MySharedPreferences.setPreferences(context, jsonObject2.has(S.media)?jsonObject2.getString(S.media):"", S.userSubscriptionMedia);
                    MySharedPreferences.setPreferences(context, jsonObject1.getString(S.subscriptionEndDate), S.subscriptionEndDate);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void postRemoveResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postRemove_api);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    @Override
    public void preferCrossClick(int postion) {
        authAPI.changeHomeTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), preferedTrackList.get(postion).getTrackId(), "0");
    }

    @Override
    public void preferFilterClick(int postion,final JSONObject filterData) {
        preferedTrackFilter(postion,filterData);

    }

    private void preferedTrackFilter(final int postion,final JSONObject jsonObject)
    {
       // JSONObject jsonObject=null;
        try {
           // jsonObject = new JSONObject(filterData);
            Log.e("tag","filter object"+jsonObject);
            if(jsonObject!=null){
                vehicleTypeId=jsonObject.getString("vehicleTypeId");
                vehicleBrandID=jsonObject.getString("vehicleBrandId");
                fromVehicleYearId=jsonObject.getString("vehicleYearFrom");
                toVehicleYearId=jsonObject.getString("vehicleYearTo");
                trackCondition=jsonObject.getString("weather");
            }
            else {
                vehicleTypeId="";
                vehicleBrandID="";
                fromVehicleYearId="";
                toVehicleYearId="";
                trackCondition="";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prefer_track_filter_dialog);


        ImageView closeBtn;
       // TextViewPlayBold closeTv;
        TextViewPlayBold okTv;
        ImageView cross_iv;

        classTv = (TextViewPlayRegular) dialog.findViewById(R.id.class_tv);
        categoryTv = (TextViewPlayRegular) dialog.findViewById(R.id.category_tv);
        brandTv = (TextViewPlayRegular) dialog.findViewById(R.id.brand_tv);
        fromYearTv = (TextViewPlayRegular) dialog.findViewById(R.id.fromYearTv);
        toYearTv = (TextViewPlayRegular) dialog.findViewById(R.id.toYearTv);
        trackConditionTv = (TextViewPlayRegular) dialog.findViewById(R.id.track_condition_tv);
        closeBtn = (ImageView) dialog.findViewById(R.id.close_btn);
        //closeTv = (TextViewPlayBold) dialog.findViewById(R.id.close_tv);
        okTv = (TextViewPlayBold) dialog.findViewById(R.id.ok_tv);
        cross_iv = (ImageView) dialog.findViewById(R.id.cross_iv);
        if(jsonObject!=null){
            fromYearTv.setText(fromVehicleYearId);
            toYearTv.setText(toVehicleYearId);
            trackConditionTv.setText(trackCondition);
            getVehicleType(context,classTv);
            getVehicleBrand(context,vehicleTypeId,brandTv);
        }
        vehicleTypes_list = new ArrayList<>();
        vehicleBrand_list = new ArrayList<>();
        vehicleYear_list = new ArrayList<>();
        trackCondition_list = new ArrayList<>();

        classTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.getVehicleType(context);
            }
        });

        brandTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.getVehicleBrand(context, vehicleTypeId);
            }
        });

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeHomeTrackStatus(postion,dialog);

            }
        });
        cross_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        fromYearTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vehicleYear_list.size() > 0)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_year), vehicleYear_list, I.VEHICLE_YEAR);
            }
        });

        toYearTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vehicleYear_list.size() > 0)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_year), vehicleYear_list, I.VEHICLE_YEAR2);
            }
        });

        trackConditionTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.getTrackCondition(context, MySharedPreferences.getPreferences(context, user_id));
            }
        });


        okTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.getPreferedTrackDataFilter(context, MySharedPreferences.getPreferences(context, S.user_id),
                        vehicleTypeId, vehicleBrandID, fromVehicleYearId, toVehicleYearId, trackCondition, preferedTrackList.get(postion).getPreferredTrackId(),
                        postion);
                dialog.cancel();
            }
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    public void getVehicleType(final Context context, final TextViewPlayRegular classTv) {
       // mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleType()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Response<ResponseBody> responseBodyResponse) {
                        try {

                            JSONObject jsonObject = new JSONObject(Util.convertRetrofitResponce(responseBodyResponse));
                            String msg = jsonObject.getString(S.message);
                            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                                vehicleTypes_list.clear();
                                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    if(vehicleTypeId.equalsIgnoreCase(jsonObject1.getString(S._id))) {
                                        classTv.setText(jsonObject1.getString(S.name));
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                       // handleResponse(responseBodyResponse, S.vehicleTypeList_api, context);
                       // mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                       // mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    // show preefill data
    public void getVehicleBrand(final Context context, String type_id, final TextViewPlayRegular brandTv) {
       // mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleBrand(type_id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }
                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Response<ResponseBody> responseBodyResponse) {
                        try {
                            JSONObject jsonObject = new JSONObject(Util.convertRetrofitResponce(responseBodyResponse));
                            String msg = jsonObject.getString(S.message);
                            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    if(vehicleBrandID.equalsIgnoreCase(jsonObject1.getString(S._id))){
                                        brandTv.setText(jsonObject1.getString(S.vehicleBrandName));
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                       // handleResponse(responseBodyResponse, S.vehicleBrandList_api, context);
                       // mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                       // mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void preferRowClick(int postion) {
        preferedUserData.clear();
        preferedUserData.addAll(preferedTrackList.get(postion).getUserList());
        preferedTrackUserAdapter.notifyDataSetChanged();
    }
    // changes by mukeshs
    private void changeHomeTrackStatus(int postion,Dialog dialog) {
        authAPI.changeHomeTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), preferedTrackList.get(postion).getTrackId(), "0");
        dialog.cancel();
    }


    /// Vehicle Type Response
    public void getVehicleTypeResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    vehicleTypes_list.add(beanVehicleType);
                }
                if (vehicleTypes_list.size() > 0)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_type), vehicleTypes_list, I.VEHICLE_TYPE);
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /// Vehicle Brand Response
    public void getVehicleBrandResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleBrand_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehicleBrandName));
                    vehicleBrand_list.add(beanVehicleType);
                }
                if (vehicleBrand_list.size() > 0)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_brand), vehicleBrand_list, I.VEHICLE_BRAND);
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Vehicle Year Response
    public void getVehicleYearResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleYear_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    beanVehicleType.setId(jsonArray.getString(i));
                    beanVehicleType.setName(jsonArray.getString(i));
                    vehicleYear_list.add(beanVehicleType);
                }

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // track condition in prefertrack filter
    public void getTrackConditionResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                trackCondition_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    beanVehicleType.setId(jsonArray.getString(i));
                    beanVehicleType.setName(jsonArray.getString(i));
                    trackCondition_list.add(beanVehicleType);
                }
                if (trackCondition_list.size() > 0)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_track_condition), trackCondition_list, I.TRACK_CONDITION);

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // unHome track webservice response
    public void trackHomeStatus(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                preferedUserData.clear();
                preferedTrackUserAdapter.notifyDataSetChanged();

                mAuthAPI.getPreferedTrackData(context, MySharedPreferences.getPreferences(context, S.user_id));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }
    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        if (vehicleList.size() != 0) {
            final Dialog dialog1 = new Dialog(context);
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.spin_dialog);
            TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
            RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
            ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
            TextView done_tv = dialog1.findViewById(R.id.done_tv);
            done_tv.setVisibility(View.GONE);
            base_toggle_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog1.dismiss();
                }
            });
            heading.setText(dialog_title);

            VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(spinDialogAdapter);

            dialog1.setCancelable(true);
            dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog1.getWindow().setGravity(Gravity.CENTER);
            dialog1.show();
        } else {
            if (type == I.VEHICLE_BRAND)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.VEHICLE_TYPE)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_type));
            if (type == I.VEHICLE_MODEL)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
            if (type == I.VEHICLE_VERSION)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_version));
            if (type == I.VEHICLE_YEAR)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_year));
            if (type == I.TYRE_BRAND)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.TYRE_MODEL)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.VEHICLE_TYPE:
                classTv.setText(value);
                vehicleTypeId = id;

                brandTv.setText("");
                vehicleBrandID = "";
                vehicleBrand_list.clear();

                fromYearTv.setText("");
                toYearTv.setText("");
                fromVehicleYearId = "";
                toVehicleYearId = "";
                vehicleYear_list.clear();
                break;
            case I.VEHICLE_BRAND:

                fromYearTv.setText("");
                toYearTv.setText("");
                fromVehicleYearId = "";
                toVehicleYearId = "";
                vehicleYear_list.clear();

                brandTv.setText(value);
                vehicleBrandID = id;

                authAPI.getVehicleYear1(context);
                break;

            case I.VEHICLE_YEAR:
                fromYearTv.setText(value);
                fromVehicleYearId = id;
                break;

            case I.VEHICLE_YEAR2:
                toYearTv.setText(value);
                toVehicleYearId = id;
                break;

            case I.TRACK_CONDITION:
                trackConditionTv.setText(value);
                trackCondition = id;
                break;

        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }

    public void preferedTrackWebserviceResponse(String response) {
        Log.e("tag_octal","preferredTrackNew"+response);
        preferedTrackList.clear();
        preferedTracksAdapter.notifyDataSetChanged();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {
                JSONArray jsonArray = new JSONArray();
                jsonArray = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject mainObject=jsonArray.getJSONObject(i);
                    BeanPreferedTrack beanPreferedTrack = new BeanPreferedTrack();

                    beanPreferedTrack.setPreferredTrackId(mainObject.has(S._id)?mainObject.getString(S._id):"");
                    beanPreferedTrack.setFilter_data(mainObject.has(S.filterData)?mainObject.getJSONObject(S.filterData):null);
                    JSONObject trakcObject=mainObject.getJSONObject(S.trackId);
                    if(trakcObject!=null&&trakcObject.length()>0){
                        beanPreferedTrack.setTrackId(trakcObject.has(S._id)?trakcObject.getString(S._id):"");
                        beanPreferedTrack.setTrackName(trakcObject.has(S.name)?trakcObject.getString(S.name):"");
                    }
                    JSONArray userLapDataArray=mainObject.getJSONArray(S.userLapData);
                    ArrayList<BeanPreferedTrack.BeanTrackUserData> userlist = new ArrayList<>();
                    for (int j = 0; j < userLapDataArray.length(); j++) {
                        JSONObject userObject=userLapDataArray.getJSONObject(j);
                        BeanPreferedTrack.BeanTrackUserData trackUserData = new BeanPreferedTrack().new BeanTrackUserData();
                        JSONArray userInfoArray=userObject.has(S.userInfo)?userObject.getJSONArray(S.userInfo):null  ;
                        if(userInfoArray!=null && userInfoArray.length()>0){
                            trackUserData.setUserId(userInfoArray.getJSONObject(0).getString(S._id));
                            trackUserData.setUserName(userInfoArray.getJSONObject(0).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userObject.getJSONArray(S.userInfo).getJSONObject(0).getJSONObject(S.personalInfo).getString(S.lastName_response));
                        }
                        trackUserData.setUserRank(j + 1 + "");
                        trackUserData.setUserTime(userObject.getString(S.time));
                        trackUserData.setUserTimeLap("0:00.000");
                        userlist.add(trackUserData);
                    }
                    beanPreferedTrack.setUserList(userlist);
                    preferedTrackList.add(beanPreferedTrack);
                }
                preferedTracksAdapter.notifyDataSetChanged();
                preferedUserData.clear();
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    //call on save btn for saving data
    public void getPreferredTrackNewResponse(String response, int position) {
        Log.e("tag_octal","preferredFilterSave"+response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {
                JSONArray jsonArray = new JSONArray();
                jsonArray = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject mainObject=jsonArray.getJSONObject(i);
                    BeanPreferedTrack beanPreferedTrack = new BeanPreferedTrack();

                    beanPreferedTrack.setPreferredTrackId(mainObject.has(S._id)?mainObject.getString(S._id):"");
                    beanPreferedTrack.setFilter_data(mainObject.has(S.filterData)?mainObject.getJSONObject(S.filterData):null);
                    JSONObject trakcObject=mainObject.getJSONObject(S.trackId);
                    if(trakcObject!=null&&trakcObject.length()>0){
                        beanPreferedTrack.setTrackId(trakcObject.has(S._id)?trakcObject.getString(S._id):"");
                        beanPreferedTrack.setTrackName(trakcObject.has(S.name)?trakcObject.getString(S.name):"");
                    }
                    JSONArray userLapDataArray=mainObject.getJSONArray(S.userLapData);
                    ArrayList<BeanPreferedTrack.BeanTrackUserData> userlist = new ArrayList<>();
                    for (int j = 0; j < userLapDataArray.length(); j++) {
                        JSONObject userObject=jsonArray.getJSONObject(j);
                        BeanPreferedTrack.BeanTrackUserData trackUserData = new BeanPreferedTrack().new BeanTrackUserData();
                        trackUserData.setUserId(userObject.getJSONArray(S.userInfo).getJSONObject(0).getString(S._id));
                        trackUserData.setUserName(userObject.getJSONArray(S.userInfo).getJSONObject(0).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " +
                                userObject.getJSONArray(S.userInfo).getJSONObject(0).getJSONObject(S.personalInfo).getString(S.lastName_response));
                        trackUserData.setUserRank(j + 1 + "");
                        trackUserData.setUserTime(userObject.getString(S.time));
                        trackUserData.setUserTimeLap("0:00.000");
                        userlist.add(trackUserData);
                    }
                    beanPreferedTrack.setUserList(userlist);
                    preferedTrackList.add(beanPreferedTrack);
                }
                preferedTracksAdapter.notifyDataSetChanged();
                preferedUserData.clear();
                preferedUserData.addAll(preferedTrackList.get(position).getUserList());
                preferedTrackUserAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        /*try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);

            if (jsonObject.getInt(status) == S.adi_status_success) {
                //mAuthAPI.getPreferedTrackData(context, MySharedPreferences.getPreferences(context, S.user_id));

                //commited because no used this data for now its was used  in previous time thats why i commited this code
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1 = jsonObject.getJSONObject(S.data);
                BeanPreferedTrack beanPreferedTrack = new BeanPreferedTrack();
                beanPreferedTrack.setTrackId(jsonObject1.getJSONObject(S.trackId).getString(S._id));
                beanPreferedTrack.setTrackName(jsonObject1.getJSONObject(S.trackId).getString(S.name));

                beanPreferedTrack.setTrackId(jsonObject1.getString(S.trackId));
                beanPreferedTrack.setTrackName(jsonObject1.getJSONObject(S.trackId).getString(S.name));

                JSONArray userArray = new JSONArray();
                userArray = jsonObject1.getJSONArray(S.userLapData);

                ArrayList<BeanPreferedTrack.BeanTrackUserData> userlist = new ArrayList<>();
                for (int j = 0; j < userArray.length() && j < 5; j++) {
                    JSONObject jsonObject11 = new JSONObject();
                    jsonObject11 = userArray.getJSONObject(j);

                    BeanPreferedTrack.BeanTrackUserData trackUserData = new BeanPreferedTrack().new BeanTrackUserData();
                    trackUserData.setUserId(jsonObject11.getJSONArray(S.userInfo).getJSONObject(0).getString(S._id));
                    trackUserData.setUserName(jsonObject11.getJSONArray(S.userInfo).getJSONObject(0).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject11.getJSONArray(S.userInfo).getJSONObject(0).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    trackUserData.setUserRank(j + 1 + "");
                    trackUserData.setUserTime(jsonObject11.getString(S.time));
                    trackUserData.setUserTimeLap("0:00.000");

                    userlist.add(trackUserData);
                }

                beanPreferedTrack.setUserList(userlist);
                preferedTrackList.set(position, beanPreferedTrack);
                preferedTracksAdapter.notifyDataSetChanged();
                preferedUserData.clear();
                preferedUserData.addAll(preferedTrackList.get(position).getUserList());
                preferedTrackUserAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            //mAuthAPI.getPreferedTrackData(context, MySharedPreferences.getPreferences(context, S.user_id));
        }*/

    }

    public void reportAbuseResponce(String s) {
        JSONObject jsonObject1 = null;
        try {
            jsonObject1 = new JSONObject(s);
            Util.showToast(mContext,jsonObject1.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkUserSubcription(){
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).checkUserSubcription(MySharedPreferences.getPreferences(context, S.user_id))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }
                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Response<ResponseBody> responseBodyResponse) {
                        String responce=Util.convertRetrofitResponce(responseBodyResponse);
                        try {
                            JSONObject jsonObject=new JSONObject(responce);
                            Log.e("tag","check subs responce"+jsonObject.getString("isSubscribed"));
                            MySharedPreferences.setBooleanPreferences(DashboardActivity.this,jsonObject.getBoolean("isSubscribed"),S.checkSubscription);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        //mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
