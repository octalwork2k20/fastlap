package com.os.fastlap.activity.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.profile.AddVehicleActivity;
import com.os.fastlap.adapter.dashboard.DashboardVehicleListAdapter;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.beans.gragemodals.VehicleBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTyreBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTyreModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleVersionIdModal;
import com.os.fastlap.beans.gragemodals.VehicleYearIdModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 8/16/2017.
 */

public class DashboardVehicleListActivity extends BaseActivity implements DashboardVehicleListAdapter.OnItemClickListener {
    Context context;
    private AppCompatImageView baseToggleIcon;
    private View view;
    private RecyclerView recyclerView;
    private TextView no_data_tv;
    private DashboardVehicleListAdapter dashboardVehicleListAdapter;
    private ArrayList<BeanVehicleList> mGarageList;
    AuthAPI authAPI;
    private String TAG = DashboardVehicleListActivity.class.getSimpleName();
    private ImageView add_album;
    private boolean isShowAddButton=false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_vehiclelist_layout);
        context = this;
        if(getIntent().getExtras()!=null){
            isShowAddButton=getIntent().getBooleanExtra("isShowAddButton",false);
        }
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();
        getVehicleList();
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        view = (View) findViewById(R.id.view);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        add_album=(ImageView)findViewById(R.id.add_album);
        if(isShowAddButton){
            add_album.setVisibility(View.VISIBLE);
            add_album.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AddVehicleActivity.class);
                    intent.putExtra("class_name","DashboardVehicleListActivity");
                    startActivityForResult(intent, 1001);
                }
            });
        }
        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mGarageList = new ArrayList<>();
        mGarageList.clear();

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        dashboardVehicleListAdapter = new DashboardVehicleListAdapter(context, mGarageList);
        recyclerView.setAdapter(dashboardVehicleListAdapter);
        dashboardVehicleListAdapter.setOnItemClickListener(this);
    }

    public void getVehicleList() {
        authAPI.getVehicleList(context, MySharedPreferences.getPreferences(context, S.user_id));
    }

    // show Vehicle list webservice response
    public void getVehicleListResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            mGarageList.clear();
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        BeanVehicleList beanVehicleList = new BeanVehicleList();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        beanVehicleList.set_id(jsonObject1.getString(S._id));
                        beanVehicleList.setDescription(jsonObject1.getString(S.description));

                        JSONObject jsonObject2 = jsonObject1.getJSONObject(S.vehicleTypeId);
                        VehicleTypeIdModal vehicleTypeIdModal = new VehicleTypeIdModal();
                        vehicleTypeIdModal.set_id(jsonObject2.getString(S._id));
                        vehicleTypeIdModal.setName(jsonObject2.getString(S.name));
                        beanVehicleList.setVehicleTypeIdModal(vehicleTypeIdModal);

                        JSONObject jsonObject3 = jsonObject1.getJSONObject(S.vehicleBrandId);
                        VehicleBrandIdModal vehicleBrandIdModal = new VehicleBrandIdModal();
                        vehicleBrandIdModal.set_id(jsonObject3.getString(S._id));
                        vehicleBrandIdModal.setVehicleBrandName(jsonObject3.getString(S.vehicleBrandName));
                        vehicleBrandIdModal.setVehicleTypeId(jsonObject3.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleBrandIdModal(vehicleBrandIdModal);

                        JSONObject jsonObject4 = jsonObject1.getJSONObject(S.vehicleModelId);
                        VehicleModelIdModal vehicleModelIdModal = new VehicleModelIdModal();
                        vehicleModelIdModal.set_id(jsonObject4.getString(S._id));
                        vehicleModelIdModal.setVehicleTypeId(jsonObject4.getString(S.vehicleTypeId));
                        vehicleModelIdModal.setVehicleBrandId(jsonObject4.getString(S.vehicleBrandId));
                        vehicleModelIdModal.setVehicleModelName(jsonObject4.getString(S.vehicleModelName));
                        beanVehicleList.setVehicleModelIdModal(vehicleModelIdModal);


                        JSONObject jsonObject5 = jsonObject1.getJSONObject(S.vehicleVersionId);
                        VehicleVersionIdModal vehicleVersionIdModal = new VehicleVersionIdModal();
                        vehicleVersionIdModal.set_id(jsonObject5.getString(S._id));
                        vehicleVersionIdModal.setVehicleVersionName(jsonObject5.getString(S.vehicleVersionName));
                        vehicleVersionIdModal.setVehicleModelId(jsonObject5.getString(S.vehicleModelId));
                        vehicleVersionIdModal.setVehicleBrandId(jsonObject5.getString(S.vehicleBrandId));
                        vehicleVersionIdModal.setVehicleTypeId(jsonObject5.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleVersionIdModal(vehicleVersionIdModal);

                        JSONObject jsonObject6 = jsonObject1.getJSONObject(S.vehicleTyreBrandId);
                        VehicleTyreBrandIdModal vehicleTyreBrandIdModal = new VehicleTyreBrandIdModal();
                        vehicleTyreBrandIdModal.set_id(jsonObject6.getString(S._id));
                        vehicleTyreBrandIdModal.setBrandName(jsonObject6.getString(S.brandName));
                        vehicleTyreBrandIdModal.setVehicleTypeId(jsonObject6.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleTyreBrandIdModal(vehicleTyreBrandIdModal);

                        JSONObject jsonObject7 = jsonObject1.getJSONObject(S.vehicleTyreModelId);
                        VehicleTyreModelIdModal vehicleTyreModelIdModal = new VehicleTyreModelIdModal();
                        vehicleTyreModelIdModal.set_id(jsonObject7.getString(S._id));
                        vehicleTyreModelIdModal.setVehicleTypeId(jsonObject7.getString(S.vehicleTypeId));
                        vehicleTyreModelIdModal.setVehichlemodelName(jsonObject7.getString(S.vehichleTyreModelName));
                        vehicleTyreModelIdModal.setVehicleTyreBrandId(jsonObject7.getString(S.vehicleTyreBrandId));
                        beanVehicleList.setVehicleTyreModelIdModal(vehicleTyreModelIdModal);

                        JSONObject jsonObject8 = jsonObject1.getJSONObject(S.vehicleYearId);
                        VehicleYearIdModal vehicleYearIdModal = new VehicleYearIdModal();
                        vehicleYearIdModal.set_id(jsonObject8.getString(S._id));
                        vehicleYearIdModal.setVehicleYear(jsonObject8.getString(S.vehicleYear));
                        vehicleYearIdModal.setVehicleVersionId(jsonObject8.getString(S.vehicleVersionId));
                        vehicleYearIdModal.setVehicleModelId(jsonObject8.getString(S.vehicleModelId));
                        vehicleYearIdModal.setVehicleBrandId(jsonObject8.getString(S.vehicleBrandId));
                        vehicleYearIdModal.setVehicleTypeId(jsonObject8.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleYearIdModal(vehicleYearIdModal);

                        JSONArray imagesJarray = new JSONArray();
                        imagesJarray = jsonObject1.getJSONArray(S.userVehicleImage);
                        ArrayList<PagerBean> imageArray = new ArrayList<>();

                        for (int j = 0; j < imagesJarray.length(); j++)
                        {
                            imageArray.add(new PagerBean(null, null, null, false, true, imagesJarray.getJSONObject(j).getString(S.fileName), false, imagesJarray.getJSONObject(j).getString(S._id)));
                        }
                        beanVehicleList.setImagesList(imageArray);
                        mGarageList.add(beanVehicleList);
                    } catch (Exception e) {

                    }
                }
            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                //Util.showAlertDialog(context, getString(R.string.alert), msg);
            }

            dashboardVehicleListAdapter.notifyDataSetChanged();
            if (mGarageList.size() > 0) {
                no_data_tv.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            no_data_tv.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemVehicleList(int position) {
        mGarageList.get(position).get_id();

        Intent intent = new Intent();
        intent.putExtra(S.vehiclename, mGarageList.get(position).getVehicleBrandIdModal().getVehicleBrandName() + " " + mGarageList.get(position).getVehicleModelIdModal().getVehicleModelName() + " " + mGarageList.get(position).getVehicleVersionIdModal().getVehicleVersionName());
        intent.putExtra(S.vehicleId, mGarageList.get(position).get_id());
        intent.putExtra(S.vehicleBrandName, mGarageList.get(position).getVehicleTyreBrandIdModal().getBrandName());
        intent.putExtra(S.vehicleBrandId, mGarageList.get(position).getVehicleTyreBrandIdModal().get_id());
        intent.putExtra(S.vehicleModelName, mGarageList.get(position).getVehicleTyreModelIdModal().getVehichleModelName());
        intent.putExtra(S.vehicleModelId, mGarageList.get(position).getVehicleTyreModelIdModal().get_id());
        intent.putExtra(S.vehicleTypeId, mGarageList.get(position).getVehicleTypeIdModal().get_id());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FastLapApplication.mCurrentContext = context;
        setToolbarHeading(getString(R.string.select_vehicle));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1001){
            getVehicleList();
        }
    }
}