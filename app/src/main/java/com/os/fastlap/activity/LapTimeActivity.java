package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.adapter.DashboardLapTimeAdapter;
import com.os.fastlap.beans.LapTime;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LapTimeActivity extends BaseActivity implements View.OnClickListener,DashboardLapTimeAdapter.OnItemClick {

    private Context mContext;
    private AuthAPI mAuthAPI;
    private RelativeLayout actionbar;
    private AppCompatImageView baseToggleIcon;
    private View view;
    private RecyclerView lapTimeRecyclerView;
    private TextView no_data_tv;
    private DashboardLapTimeAdapter dashboardLapTimeAdapter;
    private ArrayList<LapTime> lapTimeArrayList;
    private final String TAG = TracksListingActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_tracklisting_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        mAuthAPI = new AuthAPI(mContext);
        initView();
    }
    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.lap_time));
        FastLapApplication.mCurrentContext = mContext;
        mAuthAPI.lapTimeList(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
    }
    private void initView() {
        actionbar = (RelativeLayout) findViewById(R.id.actionbar);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        view = findViewById(R.id.view);
        lapTimeArrayList = new ArrayList<>();
        dashboardLapTimeAdapter = new DashboardLapTimeAdapter(lapTimeArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        lapTimeRecyclerView = (RecyclerView) findViewById(R.id.track_recyclerView);
        lapTimeRecyclerView.setLayoutManager(layoutManager);
        lapTimeRecyclerView.setAdapter(dashboardLapTimeAdapter);
        clickListner();
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }

    @Override
    public void onClick(int position) {
        Intent intent = new Intent();
        intent.putExtra(S.lapTime, lapTimeArrayList.get(position).getLap_time());
        intent.putExtra(S.lapId, lapTimeArrayList.get(position).getId());
        setResult(RESULT_OK, intent);
        finish();

    }
    public void lapTimeListingAllWebserviceResponse(String response) {
        Log.e("tag","responce:"+response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataJson.length(); i++) {
                    try {
                        JSONObject jsonObject1 = dataJson.getJSONObject(i);
                        LapTime lapTime = new LapTime();
                        lapTime.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                        lapTime.setLap_time(jsonObject1.has(S.time)?jsonObject1.getString(S.time):"");
                        lapTime.setLap_speed(jsonObject1.has(S.maxSpeed)?jsonObject1.getString(S.maxSpeed):"");
                        lapTime.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                        lapTime.setTitle(jsonObject1.getJSONObject(S.trackId).getString(S.name));
                        lapTime.setDate(jsonObject1.getJSONObject(S.trackId).getString(S.created));
                        JSONObject lapTimeObject=jsonObject1.getJSONObject(S.lapTimeId);
                        JSONObject userVehicleObject=lapTimeObject.getJSONObject(S.userVehicleId);
                        lapTime.setLap_vehicle(userVehicleObject.getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName)+" "+userVehicleObject.getJSONObject(S.vehicleModelId).getString(S.vehicleModelName));
                        lapTimeArrayList.add(lapTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                dashboardLapTimeAdapter.notifyDataSetChanged();
                if (lapTimeArrayList.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    lapTimeRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    lapTimeRecyclerView.setVisibility(View.GONE);
                }

            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                lapTimeRecyclerView.setVisibility(View.GONE);
                //Util.showAlertDialog(mContext, getString(R.string.alert), msg);
            }
        } catch (Exception e) {
            no_data_tv.setVisibility(View.VISIBLE);
            lapTimeRecyclerView.setVisibility(View.GONE);
            Log.e(TAG, e.toString());
        }
    }
}
