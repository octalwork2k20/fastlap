package com.os.fastlap.activity.payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.beans.BeanTicketPurchase;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.ButtonPlayBold;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONObject;


public class AddCardDetail extends BaseActivity implements View.OnClickListener {

    Button button_click;
    private Card cardToSave;
    Context context;
    private AppCompatImageView baseToggleIcon;
    private CardInputWidget cardInputWidget;
    private ButtonPlayBold payBtn;
    String paymentType = "";
    String subscriptionPackageId = "";
    String voucherCodeId = "";
    String payAmount = "";
    String payMonth = "";
    String subscriptionEndDate = "";
    AuthAPI authAPI;
    private MyProgressDialog mProgressDialog;
    private String TAG = AddCardDetail.class.getSimpleName();
    BeanTicketPurchase beanTicketPurchase;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_form_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        mProgressDialog = new MyProgressDialog(context);
        initView();
        hideSoftKeyboard();
        beanTicketPurchase = new BeanTicketPurchase();

        paymentType = getIntent().getStringExtra(S.type);
        if (Integer.parseInt(paymentType) == I.PAYMENT_SUBSCRIPTION) {
            subscriptionPackageId = getIntent().getStringExtra(S.subscriptionPackageId);
            voucherCodeId = getIntent().getStringExtra(S.voucherCodeId);
            payAmount = getIntent().getStringExtra(S.amount);
            payMonth=getIntent().getStringExtra(S.month);
            subscriptionEndDate = getIntent().getStringExtra(S.subscriptionEndDate);

            payBtn.setText(getString(R.string.pay) + " $" + payAmount);
        } else if (Integer.parseInt(paymentType) == I.ALBUMPAYMET) {
            payAmount = getIntent().getStringExtra(S.amount);
            payBtn.setText(getString(R.string.pay) + " $" + payAmount);
        } else if (Integer.parseInt(paymentType) == I.EVENT_TICKET_PAYMET) {
            beanTicketPurchase = (BeanTicketPurchase) getIntent().getSerializableExtra(S.data);
            payAmount = beanTicketPurchase.getAmount();
            payBtn.setText(getString(R.string.pay) + " $" + payAmount);
        }
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        cardInputWidget = (CardInputWidget) findViewById(R.id.card_input_widget);
        payBtn = (ButtonPlayBold) findViewById(R.id.pay_btn);

        clickListner();
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
        payBtn.setOnClickListener(this);
    }


    public void hideSoftKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        ((Activity) context).setResult(Activity.RESULT_OK, returnIntent);
        ((Activity) context).finish();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pay_btn:
                cardToSave = cardInputWidget.getCard();
                if (cardToSave == null) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_card_data), Toast.LENGTH_LONG);
                } else {
                    mProgressDialog.show();
                    Stripe stripe = new Stripe(getApplicationContext(), getString(R.string.strip_key));
                    stripe.createToken(
                            cardToSave,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    mProgressDialog.dismiss();
                                    String tokenId = token.getId();
                                    Log.e("succes", token.getId());
                                    if (Integer.parseInt(paymentType) == I.ALBUMPAYMET) {
                                        Intent returnIntent = new Intent();
                                        returnIntent.putExtra(S.stripeToken, tokenId);
                                        returnIntent.putExtra(S.amount,payAmount);
                                        setResult(Activity.RESULT_OK, returnIntent);
                                        finish();
                                    } else if (Integer.parseInt(paymentType) == I.EVENT_TICKET_PAYMET) {
                                        getEventTicketPurchase(token.getId());
                                    }
                                    else
                                        getSubscription(token.getId());
                                }

                                public void onError(Exception error) {
                                    mProgressDialog.dismiss();
                                    // Show localized error message
                                    Toast.makeText(getApplicationContext(),
                                            error.getLocalizedMessage(),
                                            Toast.LENGTH_LONG
                                    ).show();

                                    Log.e("error", error.getLocalizedMessage());
                                }
                            }
                    );
                }

                break;

            case R.id.base_toggle_icon:
                Intent returnIntent = new Intent();
                ((Activity) context).setResult(Activity.RESULT_OK, returnIntent);
                ((Activity) context).finish();
                break;

        }

    }
    private void getEventTicketPurchase(String tokenId) {
        authAPI.getEventTicketPurchase(context, MySharedPreferences.getPreferences(context, S.user_id), beanTicketPurchase, tokenId);

    }
    private void getSubscription(String tokenId) {
        authAPI.getSubscription(context, MySharedPreferences.getPreferences(context, S.user_id), subscriptionPackageId, voucherCodeId, payAmount, subscriptionEndDate, tokenId,payMonth);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.payment));
        FastLapApplication.mCurrentContext = context;
    }

    public void userSubscriptionWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                MySharedPreferences.setBooleanPreferences(context,true,S.checkSubscription);
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void eventTicketPurchaseResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "eventTicketPurchase");
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
