package com.os.fastlap.activity.profile;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.LanguageAdapter;
import com.os.fastlap.adapter.profile.AddVehicleImageAdapter;
import com.os.fastlap.beans.AboutInfoFamilyModal;
import com.os.fastlap.beans.LanguageBeans;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;


/*
 * Created by abhinava on 8/14/2017.
 */

public class AddAboutInfoActivity extends BaseActivity implements View.OnClickListener, OnImagePickerDialogSelect, AddVehicleImageAdapter.OnItemClickListener, LanguageAdapter.OnItemClickListener, DatePickerDialog.OnDateSetListener {
    Context context;
    private AppCompatImageView baseToggleIcon;
    private EditTextPlayRegular editName;
    private EditTextPlayRegular editNickname;
    private EditTextPlayRegular editEmail;
    private EditTextPlayRegular editContact;
    private EditTextPlayRegular editWebsite;
    private EditTextPlayRegular editFacebook;
    private EditTextPlayRegular editInstragram;
    private EditTextPlayRegular editAbout;
    private TextView tvLanguage;
    private TextViewPlayRegular tvDateBirth;

    private RecyclerView recyclerView;
    private TextViewPlayBold saveBtn;
    private ArrayList<AboutInfoFamilyModal> mFamilyArrayList;

    OnImagePickerDialogSelect onImagePickerDialogSelect;
    AddVehicleImageAdapter mAddVehicleImageAdapter;
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    private ArrayList<PagerBean> family_photo_pagerBeanArrayList;
    private String TAG = AddAboutInfoActivity.class.getSimpleName();
    private String removeString = "";
    private ArrayList<LanguageBeans> mLanguageList;
    private LanguageAdapter mLanguageAdapter;
    private AuthAPI mAuthAPI;
    private Dialog mBottomSheetDialog;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    ArrayList<String> stringArrayList=new ArrayList<>();
    SpinnerDialog locationSpinnerDialog;
    SpinnerDialog nationalitySpinnerDialog;
    String nationality="";
    String location="";
    private TextView selectNationalityEt;
    private TextView locationEt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_about_info);
        context = this;
        FastLapApplication.mCurrentContext = context;
        mAuthAPI = new AuthAPI(context);
        mAuthAPI.getLanguage(context);
        mAuthAPI.getNationality(context);
        initView();
        mLanguageList = new ArrayList<>();
        mLanguageAdapter = new LanguageAdapter(context, mLanguageList);
        mLanguageAdapter.setItemClickListener(this);
        onDateSetListener = this;
        onImagePickerDialogSelect = this;
    }

    private void initView() {

        tvLanguage=(TextView)findViewById(R.id.tvLanguage) ;
        selectNationalityEt=(TextView)findViewById(R.id.tvNationality) ;
        locationEt=(TextView)findViewById(R.id.tvLocation) ;
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        tvDateBirth = (TextViewPlayRegular) findViewById(R.id.tvDateBirth);
        editName = (EditTextPlayRegular) findViewById(R.id.editName);
        editNickname = (EditTextPlayRegular) findViewById(R.id.editNickname);

        editContact = (EditTextPlayRegular) findViewById(R.id.editContact);
        editEmail = (EditTextPlayRegular) findViewById(R.id.editEmail);
        editWebsite = (EditTextPlayRegular) findViewById(R.id.editWebsite);
        editFacebook = (EditTextPlayRegular) findViewById(R.id.editFacebook);
        editInstragram = (EditTextPlayRegular) findViewById(R.id.editInstragram);
        editAbout = (EditTextPlayRegular) findViewById(R.id.editAboutUS);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        saveBtn = (TextViewPlayBold) findViewById(R.id.save_btn);

        family_photo_pagerBeanArrayList = new ArrayList<>();
        family_photo_pagerBeanArrayList.add(new PagerBean(null, null, null, true, false, ""));
        mAddVehicleImageAdapter = new AddVehicleImageAdapter(family_photo_pagerBeanArrayList, context);
        mAddVehicleImageAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAddVehicleImageAdapter);


        clickListner();

        autofillData();
    }

    private void autofillData() {
        editName.setText(getIntent().getStringExtra(S.username));
        editNickname.setText(getIntent().getStringExtra(S.nickname));
        tvLanguage.setText(getIntent().getStringExtra(S.language_response));
        tvLanguage.setTag(getIntent().getStringExtra(S.language_id));

        selectNationalityEt.setText(getIntent().getStringExtra(S.nationality));
        locationEt.setText(getIntent().getStringExtra(S.location_response));

        editContact.setText(getIntent().getStringExtra(S.phone));
        editEmail.setText(getIntent().getStringExtra(S.email));
        editWebsite.setText(getIntent().getStringExtra(S.website));
        editFacebook.setText(getIntent().getStringExtra(S.facebook));
       // userNicknameTv.setText(getIntent().getStringExtra(S.NickName));
        tvDateBirth.setText(getIntent().getStringExtra(S.DateBirth));

        editInstragram.setText(getIntent().getStringExtra(S.instagram));;
        editAbout.setText(getIntent().getStringExtra(S.aboutDescritpion));
        mFamilyArrayList = (ArrayList<AboutInfoFamilyModal>) getIntent().getSerializableExtra(S.images);




        for (int i = 0; i < mFamilyArrayList.size(); i++) {
            PagerBean pagerBean = new PagerBean(null, null, null, false, true, mFamilyArrayList.get(i).getUrl(), mFamilyArrayList.get(i).getId());
            family_photo_pagerBeanArrayList.add(pagerBean);
        }

    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
        tvLanguage.setOnClickListener(this);
        tvDateBirth.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        selectNationalityEt.setOnClickListener(this);
        locationEt.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FastLapApplication.mCurrentContext = context;
        setToolbarHeading(getString(R.string.about_me));
    }

    public void getNationalityWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                /*JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                nationality_array = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    nationality_array[i] = jsonObject1.getString(S.name);
                }
                spinnerArrayAdapter = new ArrayAdapter<>(context, R.layout.spinner_iten_gray, nationality_array);
                mNationalitySpinner.setAdapter(spinnerArrayAdapter);

                spinnerArrayAdapter = new ArrayAdapter<>(context, R.layout.spinner_iten_gray, nationality_array);
                mLocationSpinner.setAdapter(spinnerArrayAdapter);

                try {
                    int index = Arrays.asList(nationality_array).indexOf((mLocation).replaceAll("%20", " "));
                    int index1 = Arrays.asList(nationality_array).indexOf((mNationality).replaceAll("%20", " "));
                    mLocationSpinner.setSelection(index);
                    mNationalitySpinner.setSelection(index1);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                stringArrayList=new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    stringArrayList.add(jsonObject1.getString(S.name));
                }
                nationalitySpinnerDialog=new SpinnerDialog(this,stringArrayList,getResources().getString(R.string.search_nationality_and_select),getResources().getString(R.string.cancel));
                locationSpinnerDialog=new SpinnerDialog(this,stringArrayList,getResources().getString(R.string.search_location_and_select),getResources().getString(R.string.cancel));

                nationalitySpinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String item, int position) {
                        nationality=item;
                        selectNationalityEt.setText(item);
                    }
                });

                locationSpinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String item, int position) {
                        location=item;
                        locationEt.setText(item);
                    }
                });

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.tvLanguage:
                selectLanguageDialog();
                break;
            case R.id.tvNationality:
                nationalitySpinnerDialog.showSpinerDialog();
                break;
            case R.id.tvLocation:
                locationSpinnerDialog.showSpinerDialog();
                break;
            case R.id.tvDateBirth:
                DatepickerFragment datePickerFragment = DatepickerFragment.newInstance(onDateSetListener);
                datePickerFragment.show(getFragmentManager(), "date");
                break;
            case R.id.save_btn:
                if (updateAboutMeValidation(saveBtn, context)) {
                    mAuthAPI.updateAbout(context,
                            location,
                            nationality,
                            tvDateBirth.getText().toString(),
                            editNickname.getText().toString(),
                            editAbout.getText().toString(),
                            editContact.getText().toString(),
                            editWebsite.getText().toString(),
                            editFacebook.getText().toString(),
                            editInstragram.getText().toString(),
                            MySharedPreferences.getPreferences(context, S.user_id),
                            tvLanguage.getTag().toString(),
                            editEmail.getText().toString() /*family_photo_pagerBeanArrayList,
                            getImageCount(), removeString*/);

                }
                //   selectLanguageDialog();
                break;
        }
    }
    public static class DatepickerFragment extends DialogFragment {

        DatePickerDialog.OnDateSetListener onDateSetListener;

        public static DatepickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener) {
            DatepickerFragment datepickerFragment = new DatepickerFragment();
            datepickerFragment.onDateSetListener = onDateSetListener;
            return datepickerFragment;
        }

        public Dialog onCreateDialog(Bundle savedInstance) {
            // create Calendar Instance from Calendar class
            final Calendar calender = Calendar.getInstance();
            int year = calender.get(Calendar.YEAR);
            int month = calender.get(Calendar.MONTH);
            int day = calender.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dateFragment = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_DARK, onDateSetListener, year, month, day);
            dateFragment.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            return dateFragment;
        }

    }


    public  boolean updateAboutMeValidation(View saveBtn, Context context) {
        boolean result = false;
        if (TextUtils.isEmpty(editName.getText().toString().trim()))
            Util.showSnackBar(saveBtn,  getString(R.string.first_name_empty_valid));
        else if (TextUtils.isEmpty(editNickname.getText().toString().trim()))
            Util.showSnackBar(saveBtn, getString(R.string.nick_name_empty_valid));
        else if (TextUtils.isEmpty(editContact.getText().toString().trim()))
            Util.showSnackBar(saveBtn, context.getString(R.string.mobile_empty_valid));
        else if (TextUtils.isEmpty(tvDateBirth.getText().toString().trim()))
            Util.showSnackBar(saveBtn, context.getString(R.string.dob_valid_error));
        if(!Util.dobDiff(tvDateBirth.getText().toString().trim()))
            Util.showSnackBar(saveBtn, context.getString(R.string.dob_validation_error));
        else if (TextUtils.isEmpty(editEmail.getText().toString().trim()))
            Util.showSnackBar(saveBtn, context.getString(R.string.email_empty_valid));
        else if (TextUtils.isEmpty(tvLanguage.getText().toString().trim()))
            Util.showSnackBar(saveBtn, context.getString(R.string.choose_language));
        else if (TextUtils.isEmpty(selectNationalityEt.getText().toString().trim()))
            Util.showSnackBar(saveBtn, context.getString(R.string.select_nationality));
        else if (TextUtils.isEmpty(locationEt.getText().toString().trim()))
            Util.showSnackBar(saveBtn, context.getString(R.string.select_location));
        else if(!Patterns.WEB_URL.matcher(editWebsite.getText().toString().trim()).matches())
            Util.showSnackBar(saveBtn, context.getString(R.string.website_validation));
        else if(!Patterns.WEB_URL.matcher(editFacebook.getText().toString().trim()).matches())
            Util.showSnackBar(saveBtn, context.getString(R.string.facebook_validation));
        else if(!Patterns.WEB_URL.matcher(editInstragram.getText().toString().trim()).matches())
            Util.showSnackBar(saveBtn, context.getString(R.string.instragram_validation));
        else
            result = true;
        return result;
    }
    /* Click events in RecyclerView items  add image*/
    @Override
    public void onAddImageCLick(View v, int position) {
        if (family_photo_pagerBeanArrayList.get(position).isDefault()) {
            if (family_photo_pagerBeanArrayList.size() <= 4) {
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    //CHECKING PERMISSION FOR MARSHMALLOW
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
            } else {
                Util.showAlertDialog(context, getString(R.string.app_name), getResources().getString(R.string.choose_only_4_pics));
            }
        }
    }

    /* Click events on RecyclerView item delete image */
    @Override
    public void onDeleteImageClick(View v, int position) {
        if (family_photo_pagerBeanArrayList.get(position).getImage_id() != null) {
            removeString += family_photo_pagerBeanArrayList.get(position).getImage_id() + ",";
        }
        family_photo_pagerBeanArrayList.remove(position);
        mAddVehicleImageAdapter.notifyDataSetChanged();
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {
        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        //chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }
    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }
    @Override
    public void OnGallerySelect() {
        EasyImage.openGallery(this, SELECT_FILE_PROFILE);
    }
    @Override
    public void onVideoSelect() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == I.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
               // Place place = PlaceAutocomplete.getPlace(context, data);
               // userCityTv.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(context, data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, AddAboutInfoActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;

                if (family_photo_pagerBeanArrayList.size() <= 5) {

                    family_photo_pagerBeanArrayList.add(new PagerBean(imageFile, imageFile.getAbsolutePath(), imageFile.getName(), false, false, ""));
                    mAddVehicleImageAdapter.notifyDataSetChanged();

                }
            }


        });
    }
    private void selectLanguageDialog() {
        mBottomSheetDialog = new Dialog(context, R.style.AlertDialogBottomSlide);

        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.select_language_dialog, null);

        RecyclerView recycler_view = dialogView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setAdapter(mLanguageAdapter);

        mBottomSheetDialog.setContentView(dialogView); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    /* update user webservice response*/
    public void updateUserInfo(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    private int getImageCount() {
        int cnt = 0;
        for (int i = 1; i < family_photo_pagerBeanArrayList.size(); i++) {
            if (!family_photo_pagerBeanArrayList.get(i).isEdit())
                cnt++;
        }
        return cnt;
    }

    // LanguageListing webservice response
    public void getLanguage(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    LanguageBeans languageBeans = new LanguageBeans();
                    languageBeans.set_id(jsonObject1.getString(S._id));
                    languageBeans.setName(jsonObject1.getString(S.name));
                    languageBeans.setCode(jsonObject1.getString(S.code));
                    mLanguageList.add(languageBeans);
                }
                mLanguageAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onItemClick(int position) {
        tvLanguage.setText(mLanguageList.get(position).getName());
        tvLanguage.setTag(mLanguageList.get(position).get_id());
        mBottomSheetDialog.dismiss();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
        setDate(cal);
    }
    public void setDate(Calendar calender) {
        Date current = calender.getTime();
        int diff1 = new Date().compareTo(current);
        if (diff1 < 0) {
            Util.showSnackBar(tvDateBirth, getString(R.string.dob_valid_error));
        } else {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            tvDateBirth.setText(dateFormat.format(calender.getTime()));
        }
    }
}
