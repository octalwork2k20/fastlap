package com.os.fastlap.activity.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.adapter.profile.AwardProfileAdapter;
import com.os.fastlap.adapter.profile.FamilyProfileAdapter;
import com.os.fastlap.adapter.profile.FavoriteTrackAdapter;
import com.os.fastlap.beans.AboutInfoFamilyModal;
import com.os.fastlap.beans.UserTrackBeans;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/12/2017.
 */

public class InfoActivity extends BaseActivity implements View.OnClickListener {

    private AppCompatImageView toggle_icon;
    private ImageView img_bg;
    private CircleImageView user_icon;
    TextView txt_username,txtEmail;
    private RelativeLayout parent_about;
    private TextView edit_iv;
    private TextView txt_name;
    private TextView txt_dob;
    private ImageView img_user_name;
    private TextView txt_language_name;
    private TextView txt_city_name;
    private TextView txt_lap_upload_name;
    private TextView txt_phone_number_name;
    private TextView txt_phone_email_name;
    private TextView txt_website_name;
    private TextView txt_facebook_name;
    private TextView txt_instagram_name;
    private RecyclerView recycler_view_family;
    private RecyclerView recycler_view_tracks;
    private RelativeLayout parent_award;
    private RecyclerView recycler_view_award;
    private View abouteme_layout;
    private View award_layout;
    private ImageView img_arrow_about;
    private ImageView img_arrow_clothing;
    private ImageView level_icon;
    private TextView txt_aboutme;
    private AwardProfileAdapter mAwardProfileAdapter;
    private Context mContext;
    private ArrayList<String> mAwardList;
    private ImageView img_arrow_award;
    private FamilyProfileAdapter familyProfileAdapter;
    private FavoriteTrackAdapter favoriteTrackAdapter;
    private ArrayList<AboutInfoFamilyModal> mFamilyArrayList;
    private ArrayList<UserTrackBeans> mFavArrayList;
    private AuthAPI authAPI;
    private final String TAG = InfoActivity.class.getSimpleName();
    private TextView txt_favcount;
    TextView menu_menu;
    private String dateOfBirth="",nickName="",surName="",nationality;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_info_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading("Info");
        FastLapApplication.mCurrentContext = mContext;
        //authAPI.userClothList(mContext, ProfileActivity.profileUserId);
        authAPI.getUserAbout(mContext, ProfileActivity.profileUserId,MySharedPreferences.getPreferences(mContext,S.user_id));
    }

    private void initView() {
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        txt_favcount = (TextView) findViewById(R.id.txt_favcount);
        img_arrow_about = (ImageView) findViewById(R.id.img_arrow_about);
        toggle_icon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        txt_username = (TextView) findViewById(R.id.txt_username);
        txtEmail=(TextView)findViewById(R.id.txt_email);
        img_bg = (ImageView) findViewById(R.id.img_bg);
        user_icon = (CircleImageView) findViewById(R.id.user_icon);
        parent_about = (RelativeLayout) findViewById(R.id.parent_about);
        edit_iv = (TextView) findViewById(R.id.edit_iv);
        txt_name = (TextView) findViewById(R.id.txt_name);
        img_user_name = (ImageView) findViewById(R.id.img_user_name);
        txt_language_name = (TextView) findViewById(R.id.txt_language_name);
        txt_city_name = (TextView) findViewById(R.id.txt_city_name);
        txt_lap_upload_name = (TextView) findViewById(R.id.txt_lap_upload_name);
       // txt_best_lap_name = (TextView) findViewById(R.id.txt_best_lap_name);
        level_icon = (ImageView) findViewById(R.id.level_icon);
        txt_phone_number_name = (TextView) findViewById(R.id.txt_phone_number_name);
        txt_phone_email_name = (TextView) findViewById(R.id.txt_phone_email_name);
        txt_dob = (TextView) findViewById(R.id.txt_dob);
        txt_website_name = (TextView) findViewById(R.id.txt_website_name);
        txt_facebook_name = (TextView) findViewById(R.id.txt_facebook_name);
        txt_instagram_name = (TextView) findViewById(R.id.txt_instagram_name);
        recycler_view_family = (RecyclerView) findViewById(R.id.recycler_view_family);
        recycler_view_tracks = (RecyclerView) findViewById(R.id.recycler_view_tracks);
        parent_award = (RelativeLayout) findViewById(R.id.parent_award);
        recycler_view_award = (RecyclerView) findViewById(R.id.recycler_view_award);
        abouteme_layout = findViewById(R.id.abouteme_layout);
        award_layout = findViewById(R.id.award_layout);
        img_arrow_clothing = (ImageView) findViewById(R.id.img_arrow_clothing);
        recycler_view_family = (RecyclerView) findViewById(R.id.recycler_view_family);
        img_arrow_award = (ImageView) findViewById(R.id.img_arrow_award);
        txt_aboutme = (TextView) findViewById(R.id.txt_aboutme);
        edit_iv.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recycler_view_family.setLayoutManager(mLayoutManager);
        mFamilyArrayList = new ArrayList<AboutInfoFamilyModal>();

        mAwardList = new ArrayList<>();

        mAwardProfileAdapter = new AwardProfileAdapter(mContext, mAwardList);
        RecyclerView.LayoutManager mAwardLayoutManager = new LinearLayoutManager(mContext);
        recycler_view_award.setLayoutManager(mAwardLayoutManager);
        recycler_view_award.setAdapter(mAwardProfileAdapter);

        mFavArrayList = new ArrayList<>();
        favoriteTrackAdapter = new FavoriteTrackAdapter(mContext, mFavArrayList);
        RecyclerView.LayoutManager mFavLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recycler_view_tracks.setLayoutManager(mFavLayoutManager);
        recycler_view_tracks.setAdapter(favoriteTrackAdapter);

        setOnCLickListener();
        setProfileData();

        if (MySharedPreferences.getPreferences(mContext, S.user_id).compareTo(ProfileActivity.profileUserId) == 0) {
            edit_iv.setVisibility(View.VISIBLE);
        } else {
            edit_iv.setVisibility(View.GONE);
        }
        openAbount();

    }

    private void setOnCLickListener() {
        parent_about.setOnClickListener(this);
        parent_award.setOnClickListener(this);
        toggle_icon.setOnClickListener(this);
        edit_iv.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }

    private void setProfileData() {

        try {
            if(!ProfileActivity.profileUserCoverImage.equalsIgnoreCase("img/cover.png")){
                Util.setImageLoader(S.IMAGE_BASE_URL + ProfileActivity.profileUserCoverImage, img_bg, getApplicationContext());
            }
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + ProfileActivity.profileUserProfileImage, user_icon, Util.getImageLoaderOption(mContext));
            txt_username.setText(ProfileActivity.profileUserName);
            txtEmail.setText("("+ProfileActivity.profileEmail+")");
        }
        catch (Exception e){e.printStackTrace();}


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        edit_iv.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.parent_about:
                openAbount();
                break;
            case R.id.parent_award:
                if (award_layout.getVisibility() == View.VISIBLE) {
                    award_layout.setVisibility(View.GONE);
                    img_arrow_award.setImageResource(R.drawable.new_right_back_red);
                } else {
                    img_arrow_award.setImageResource(R.drawable.new_down_back_red);
                    img_arrow_clothing.setImageResource(R.drawable.new_right_back_red);
                    img_arrow_about.setImageResource(R.drawable.new_right_back_red);
                    abouteme_layout.setVisibility(View.VISIBLE);
                    award_layout.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.base_toggle_icon:
                finish();
                //overridePendingTransition(R.anim.slide_out_right, R.anim.slide_out_right);

                break;
            case R.id.edit_iv:
                Intent intent = new Intent(InfoActivity.this, AddAboutInfoActivity.class);
                intent.putExtra(S.language_response, txt_language_name.getText().toString());
                intent.putExtra(S.username, txt_name.getText().toString());
                intent.putExtra(S.language_id, txt_language_name.getTag().toString());
                intent.putExtra(S.phone, txt_phone_number_name.getText().toString());
                intent.putExtra(S.email, txt_phone_email_name.getText().toString());
                intent.putExtra(S.location_response, txt_city_name.getText().toString());
                intent.putExtra(S.nationality, nationality);
                intent.putExtra(S.nickname, nickName);
                intent.putExtra(S.DateBirth, dateOfBirth);
                intent.putExtra(S.website, txt_website_name.getText().toString());
                intent.putExtra(S.facebook, txt_facebook_name.getText().toString());
                intent.putExtra(S.instagram, txt_instagram_name.getText().toString());
                intent.putExtra(S.aboutDescritpion, txt_aboutme.getText().toString());
                intent.putExtra(S.images, mFamilyArrayList);
                intent.putExtra(S.isEdit, true);
                startActivity(intent);
                //Util.startNewActivity(InfoActivity.this, AddAboutInfoActivity.class, false);
                break;
            case R.id.menu_menu:
                Intent intent2 = new Intent(this, MenuActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                finish();
                break;
        }
    }

    private void openAbount() {
        if (abouteme_layout.getVisibility() == View.VISIBLE) {
            //abouteme_layout.setVisibility(View.GONE);
            img_arrow_about.setImageResource(R.drawable.new_right_back_red);
        } else {
            img_arrow_about.setImageResource(R.drawable.new_down_back_red);
            img_arrow_clothing.setImageResource(R.drawable.new_right_back_red);
            img_arrow_award.setImageResource(R.drawable.new_right_back_red);
            abouteme_layout.setVisibility(View.VISIBLE);
            //clothing_layout.setVisibility(View.GONE);
            //award_layout.setVisibility(View.GONE);
        }
    }

    /* user about us webservice response */
    public void getUserAboutResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                JSONObject userJson = jsonObject1.getJSONObject(S.userdata);
                txt_aboutme.setText(userJson.getString(S.aboutDescritpion));
                txt_phone_email_name.setText(userJson.getString(S.email));
                nickName=userJson.has(S.nickname)?userJson.getString(S.nickname):"";
                txt_name.setText(nickName);
                JSONObject personInfo = userJson.has(S.personalInfo)?userJson.getJSONObject(S.personalInfo):null;
                if (personInfo!=null) {
                    if(nickName.equalsIgnoreCase("")){
                        nickName=personInfo.has(S.firstName_response)?personInfo.getString(S.firstName_response):"";
                        txt_name.setText(nickName);
                    }
                    txt_language_name.setText(personInfo.getJSONObject(S.language_response).has(S.name)?personInfo.getJSONObject(S.language_response).getString(S.name):"");
                    txt_language_name.setTag(personInfo.getJSONObject(S.language_response).has(S._id)?personInfo.getJSONObject(S.language_response).getString(S._id):"");
                    txt_dob.setText(personInfo.getString(S.dateOfBirth_response));
                    dateOfBirth=personInfo.getString(S.dateOfBirth_response);
                }

                JSONObject addressJson = userJson.has(S.address)?userJson.getJSONObject(S.address):null;
                if (addressJson!=null){
                    txt_city_name.setText(addressJson.has(S.location_response)?addressJson.getString(S.location_response):"");
                    nationality=addressJson.has(S.nationality_response)?addressJson.getString(S.nationality_response):"";

                }
                JSONObject contactsJson = userJson.has(S.contacts)?userJson.getJSONObject(S.contacts):null;
                if(contactsJson!=null){
                    txt_phone_number_name.setText(contactsJson.has(S.phone)?contactsJson.getString(S.phone):"");
                    txt_instagram_name.setText(contactsJson.has(S.instagram)?contactsJson.getString(S.instagram):"");
                    txt_website_name.setText(contactsJson.has(S.website)?contactsJson.getString(S.website):"");
                    txt_facebook_name.setText(contactsJson.has(S.facebook)?contactsJson.getString(S.facebook):"");
                }
                txt_lap_upload_name.setText(jsonObject1.has(S.lapUploaded)?jsonObject1.getString(S.lapUploaded):"");
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + jsonObject1.getString(S.levelIcon), level_icon, Util.getImageLoaderOption(mContext));
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + jsonObject1.getString(S.locationIcon), img_user_name, Util.getImageLoaderOption(mContext));

                JSONArray userTrack = jsonObject1.getJSONArray(S.userTrack);
                mFavArrayList.clear();
                for (int i = 0; i < userTrack.length(); i++) {
                    JSONObject jsonObject2 = userTrack.getJSONObject(i);
                    UserTrackBeans userTrackBeans = new UserTrackBeans();
                    userTrackBeans.setUser_track_id(jsonObject2.has(S._id)?jsonObject2.getString(S._id):"");
                    JSONObject tracjIdJson = jsonObject2.has(S.TrackId)?jsonObject2.getJSONObject(S.TrackId):null;
                    if(tracjIdJson!=null&&tracjIdJson.length()>0){
                        userTrackBeans.setTrack_id(tracjIdJson.has(S._id)?tracjIdJson.getString(S._id):"");
                        userTrackBeans.setImage(tracjIdJson.has(S.image)?tracjIdJson.getString(S.image):"");
                    }
                    mFavArrayList.add(userTrackBeans);
                }
                if (mFavArrayList.size() != 0) {
                    txt_favcount.setVisibility(View.VISIBLE);
                    txt_favcount.setText(" ("+String.valueOf(mFavArrayList.size())+")");
                } else
                    txt_favcount.setVisibility(View.GONE);

                familyProfileAdapter = new FamilyProfileAdapter(mContext, mFamilyArrayList);
                recycler_view_family.setAdapter(familyProfileAdapter);
                favoriteTrackAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    public void trackManageStatus(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                authAPI.getUserAbout(mContext, ProfileActivity.profileUserId,MySharedPreferences.getPreferences(mContext,S.user_id));
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
