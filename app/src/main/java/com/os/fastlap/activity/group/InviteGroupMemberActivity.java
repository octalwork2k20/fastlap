package com.os.fastlap.activity.group;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.InviteFriendAdapter;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 12/20/2017.
 */

public class InviteGroupMemberActivity extends BaseActivity implements InviteFriendAdapter.OnItemClickListener {
    Context context;
    AuthAPI authAPI;

    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    InviteFriendAdapter inviteFriendAdapter;
    private String TAG = InviteGroupMemberActivity.class.getSimpleName();
    private AppCompatImageView baseToggleIcon;
    private EditTextPlayRegular etSearch;
    private RecyclerView recyclerView;
    private TextViewPlayRegular noDataTv;
    private TextViewPlayBold doneTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spin_dialog);
        context = this;
        authAPI = new AuthAPI(context);
        initView();

        authAPI.myFriendListforGroup(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId);
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        etSearch = (EditTextPlayRegular) findViewById(R.id.etSearch);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        noDataTv = (TextViewPlayRegular) findViewById(R.id.no_data_tv);
        doneTv = (TextViewPlayBold) findViewById(R.id.done_tv);

        mMyFriendsLists = new ArrayList<>();
        mMyFriendsLists.clear();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        inviteFriendAdapter = new InviteFriendAdapter(context, mMyFriendsLists, false);
        recyclerView.setAdapter(inviteFriendAdapter);
        inviteFriendAdapter.addMyFriendsListAll(mMyFriendsLists);
        inviteFriendAdapter.setItemClickListener(this);

        doneTv.setVisibility(View.GONE);
        etSearch.setVisibility(View.GONE);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                inviteFriendAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.invite_friends));
    }

    /* myFriendList webservice response */
    public void myFriendListWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                mMyFriendsLists.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.getString(S.id));
                    profileMyFriendsList.setUserId(jsonObject1.getString(S.userId));
                    profileMyFriendsList.setStatus(jsonObject1.getString(S.status));
                    profileMyFriendsList.setEmail(jsonObject1.getString(S.email));
                    profileMyFriendsList.setInvitedGroup(jsonObject1.getString(S.invitedGroup));

                    PersonalInfo personalInfo = new PersonalInfo();
                    JSONObject personalInfoJson = jsonObject1.getJSONObject(S.personalInfo);
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    profileMyFriendsList.setPersonalInfo(personalInfo);

                    if (jsonObject1.getString(S.joinedGroup).equals("0")) {
                        mMyFriendsLists.add(profileMyFriendsList);
                    }
                }
                inviteFriendAdapter.notifyDataSetChanged();

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onInviteClick(int position) {
        authAPI.sentInviteRequest(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId, mMyFriendsLists.get(position).getUserId(), position);
    }

    public void getInviteFriend(int postion, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                mMyFriendsLists.get(postion).setInvitedGroup("1");
                inviteFriendAdapter.notifyDataSetChanged();

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
