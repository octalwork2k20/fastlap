package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.os.fastlap.R;
import com.os.fastlap.activity.profile.AddAlbumActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.adapter.community.CommunityVechiceListAdapter;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.beans.BeanVehicle;
import com.os.fastlap.beans.UserDataBeans;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.ActivityCommunicator;
import com.os.fastlap.fragment.gallery.PhotosFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by abhinava on 7/14/2017.
 */

public class GalleryActivity extends BaseActivity implements ActivityCommunicator, View.OnClickListener {
    private android.support.design.widget.TabLayout TabLayout;
    private ViewPager viewpager;
    private Context context;
    private FragmentManager mFragmentManager;
    private ViewPagerAdapter adapter;
    ImageView base_toggle_icon;
    public static int pos = 0;
    AuthAPI authAPI;
    private int[] tabIcons =
            {
                    R.mipmap.photo,
                    R.mipmap.video
            };
    private String TAG = GalleryActivity.class.getSimpleName();
    public static ArrayList<BeanAlbum> mPhotoList;
    public static ArrayList<BeanAlbum> mPhotoListMY;
    public static ArrayList<BeanAlbum> mPhotoListOffical;
    public static ArrayList<BeanAlbum> mVideoList;
    public static ArrayList<BeanAlbum> mVideoListMY;
    public static ArrayList<BeanAlbum> mVideoListOffical;

    BeanAlbum beanAlbum;
    CallbackManager callbackManager;
    ImageView add_album;

    RecyclerView groupRecyclerView;
    private CommunityVechiceListAdapter communityVechiceListAdapter;
    private ArrayList<BeanVehicle> mArrayVechineList;
    EditText searchEt;
    TextView menu_menu;
    private LoginManager loginManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.gallery_activity);
        initView();
        addEventListeners();
        getPhotsData();

    }
    @Override
    protected void onStart() {
        super.onStart();
        getVideoData();
    }

    private void getPhotsData() {
        authAPI.getGalleryPhoto(context, MySharedPreferences.getPreferences(context, S.user_id), 1);
        authAPI.getGalleryPhoto(context, MySharedPreferences.getPreferences(context, S.user_id), 4);
        authAPI.getGalleryPhoto(context, MySharedPreferences.getPreferences(context, S.user_id), 3);
    }
    private void getVideoData() {
        authAPI.getGalleryVideo(context, MySharedPreferences.getPreferences(context, S.user_id), 1);
        authAPI.getGalleryVideo(context, MySharedPreferences.getPreferences(context, S.user_id), 4);
        authAPI.getGalleryVideo(context, MySharedPreferences.getPreferences(context, S.user_id), 3);
    }

    private void downloadGalleryData() {
        authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
    }

    private void initView() {
        context = this;

        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        mFragmentManager = getSupportFragmentManager();

        menu_menu=(TextView)findViewById(R.id.menu_menu);
        TabLayout = (TabLayout) findViewById(R.id.TabLayout);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        add_album = (ImageView) findViewById(R.id.add_album);
        groupRecyclerView = (RecyclerView) findViewById(R.id.groupRecyclerView);
        searchEt = findViewById(R.id.searchEt);

        mPhotoList = new ArrayList<>();
        mPhotoList.clear();

        mPhotoListOffical = new ArrayList<>();
        mPhotoListOffical.clear();


        mPhotoListMY = new ArrayList<>();
        mPhotoListMY.clear();

        mVideoList = new ArrayList<>();
        mVideoList.clear();

        mVideoListMY = new ArrayList<>();
        mVideoListMY.clear();

        mVideoListOffical = new ArrayList<>();
        mVideoListOffical.clear();

        setUpViewPager(viewpager);
        TabLayout.setupWithViewPager(viewpager);
        setupTabIcons();
        clickListner();
        Util.changeTabsFont(TabLayout, context);

        mArrayVechineList = new ArrayList<>();
        mArrayVechineList.clear();
        communityVechiceListAdapter = new CommunityVechiceListAdapter(context, mArrayVechineList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        groupRecyclerView.setLayoutManager(layoutManager);
        groupRecyclerView.setAdapter(communityVechiceListAdapter);

        searchEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(S.type, I.GALLERYSEARCH);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        add_album.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
        add_album.setVisibility(View.VISIBLE);
    }

    private void setupTabIcons() {
        TabLayout.getTabAt(0).setIcon(tabIcons[0]);
        TabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void addEventListeners() {
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                hideKeyboard();
                pos = position;
                getCurrentFragment();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setUpViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(mFragmentManager);
        adapter.addFragment(new PhotosFragment(I.PHOTOGALLERY, 0), getString(R.string.photo));
        adapter.addFragment(new PhotosFragment(I.VIDEOGALLERY, 0), getString(R.string.video));
        viewPager.setAdapter(adapter);
    }

    public Fragment getCurrentFragment() {
        Fragment mCurrentFragment = null;
        if (adapter != null && viewpager != null) {
            mCurrentFragment = adapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((PhotosFragment) mCurrentFragment).getData(1);
        }

        return mCurrentFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.add_album:
                Intent intent = new Intent(mContext, AddAlbumActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_menu:
                Intent intent2 = new Intent(this, MenuActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                finish();
                break;
        }

    }

    public void getGalleryPhotoWebserviceResponse(String response, int type)
    {
        try {
            if (type == 1) {
                mPhotoListMY.clear();
            }
            else if(type==3) {
                mPhotoList.clear();
            }
            else {
                mPhotoListOffical.clear();
            }
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataJson.length(); i++)
                {
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);
                    BeanAlbum beanAlbum = new BeanAlbum();
                    beanAlbum.set_id(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanAlbum.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");
                    beanAlbum.setDateOfEvent(jsonObject1.has(S.dateOfEvent)?jsonObject1.getString(S.dateOfEvent):"");
                    beanAlbum.setCreated(jsonObject1.has(S.created)?jsonObject1.getString(S.created):"");
                    beanAlbum.setDescription(jsonObject1.has(S.description)?jsonObject1.getString(S.description):"");
                    beanAlbum.setAlbumPrice(jsonObject1.has(S.albumPrice)?jsonObject1.getString(S.albumPrice):"");
                    beanAlbum.setLikedPost(jsonObject1.getString(S.likedAlbum));
                    beanAlbum.setCommentPostCount(jsonObject1.getString(S.commentAlbumCount));
                    beanAlbum.setLikedPostCount(jsonObject1.getString(S.likedAlbumCount));
                    beanAlbum.setMediaType(1);

                    JSONArray typejsonArray = jsonObject1.getJSONArray(S.groupTypeId);
                    if (typejsonArray.length() > 0) {
                        beanAlbum.setGroupTypeId(typejsonArray.getJSONObject(0).getString(S._id));
                        beanAlbum.setGroupTypeName(typejsonArray.getJSONObject(0).getString(S.name));
                    }

                    try {
                        JSONObject userIdObject=jsonObject1.getJSONObject(S.userId);
                        beanAlbum.setUserId(userIdObject.getString(S._id));
                        beanAlbum.setUserName(userIdObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userIdObject.getJSONObject(S.personalInfo).getString(S.lastName_response));
                        beanAlbum.setUserImage(userIdObject.getJSONObject(S.personalInfo).getString(S.image));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    if (jsonObject1.getJSONArray(S.trackId).length() > 0) {
                        beanAlbum.setTrackId(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S._id));
                        beanAlbum.setTrackName(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S.name));
                    }

                    if (jsonObject1.getJSONArray(S.eventId).length() > 0) {
                        beanAlbum.setEventId(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S._id));
                        beanAlbum.setEventName(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S.name));
                    }
                    JSONArray shareAlbumUser = jsonObject1.getJSONArray(S.albumShareUser);
                    List<DashboardPostShareUserModal> albumSharePost = new ArrayList<>();
                    for (int j = 0; j < shareAlbumUser.length(); j++)
                    {
                        JSONObject shareAlbumUserJson = shareAlbumUser.getJSONObject(j);
                        DashboardPostShareUserModal dashboardPostShareUserModal = new DashboardPostShareUserModal();
                        dashboardPostShareUserModal.setId(shareAlbumUserJson.has(S._id)?shareAlbumUserJson.getString(S._id):"");
                        dashboardPostShareUserModal.setPostId(shareAlbumUserJson.has(S.albumId)?shareAlbumUserJson.getString(S.albumId):"");
                        dashboardPostShareUserModal.setModified(shareAlbumUserJson.has(S.modified)?shareAlbumUserJson.getString(S.modified):"");
                        dashboardPostShareUserModal.setCreated(shareAlbumUserJson.has(S.created)?shareAlbumUserJson.getString(S.created):"");
                        dashboardPostShareUserModal.setStatus(shareAlbumUserJson.has(S.status)?shareAlbumUserJson.getString(S.status):"");

                        try {
                            JSONObject userJson = shareAlbumUserJson.getJSONObject(S.userId);
                            UserDataBeans userDataBeans = new UserDataBeans();
                            userDataBeans.set_id(userJson.has(S._id)?userJson.getString(S._id):"");
                            userDataBeans.setEmail(userJson.has(S.email)?userJson.getString(S.email):"");
                            userDataBeans.setUsername(userJson.has(S.username)?userJson.getString(S.username):"");

                            JSONObject personalInfoJson = userJson.getJSONObject(S.personalInfo);
                            PersonalInfo personalInfo = new PersonalInfo();
                            personalInfo.setLanguage(personalInfoJson.has(S.language_response)?personalInfoJson.getString(S.language_response):"");
                            personalInfo.setCoverImage(personalInfoJson.has(S.coverImage)?personalInfoJson.getString(S.coverImage):"");
                            personalInfo.setImage(personalInfoJson.has(S.image)?personalInfoJson.getString(S.image):"");
                            personalInfo.setMobileNumber(personalInfoJson.has(S.mobileNumber_response)?personalInfoJson.getString(S.mobileNumber_response):"");
                            personalInfo.setDateOfBirth(personalInfoJson.has(S.dateOfBirth_response)?personalInfoJson.getString(S.dateOfBirth_response):"");
                            personalInfo.setVoucherCode(personalInfoJson.has(S.voucherCode_response)?personalInfoJson.getString(S.voucherCode_response):"");
                            personalInfo.setLastName(personalInfoJson.has(S.lastName_response)?personalInfoJson.getString(S.lastName_response):"");
                            personalInfo.setFirstName(personalInfoJson.has(S.firstName_response)?personalInfoJson.getString(S.firstName_response):"");
                            userDataBeans.setPersonalInfo(personalInfo);
                            dashboardPostShareUserModal.setUserDataBeans(userDataBeans);
                            albumSharePost.add(dashboardPostShareUserModal);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                    beanAlbum.setAlbumPostShareUserModals(albumSharePost);

                    JSONArray friendArray = new JSONArray();
                    friendArray = jsonObject1.getJSONArray(S.albumFriend);
                    ArrayList<BeanAlbum.BeanFriend> friendList = new ArrayList<>();
                    for (int j = 0; j < friendArray.length(); j++) {
                        BeanAlbum.BeanFriend beanFriend = new BeanAlbum().new BeanFriend();
                        beanFriend.setUserFriendId(friendArray.getJSONObject(j).getString(S._id));
                        beanFriend.setFriendId(friendArray.getJSONObject(j).getJSONObject(S.friendId).getString(S._id));
                        beanFriend.setFriendName(friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                        beanFriend.setFriendImage(friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.image));
                        friendList.add(beanFriend);
                    }
                    beanAlbum.setFriendList(friendList);

                    JSONArray filesArray = new JSONArray();
                    filesArray = jsonObject1.getJSONArray(S.filesAll);
                    if(filesArray!=null){
                        ArrayList<BeanAlbum.BeanFiles> filesList = new ArrayList<>();
                        for (int j = 0; j < filesArray.length(); j++)
                        {
                            BeanAlbum.BeanFiles beanFiles = new BeanAlbum().new BeanFiles();
                            beanFiles.setFileId(filesArray.getJSONObject(j).has(S._id)?filesArray.getJSONObject(j).getString(S._id):"");
                            beanFiles.setCreated(filesArray.getJSONObject(j).has(S.created)?filesArray.getJSONObject(j).getString(S.created):"");
                            beanFiles.setFileName(filesArray.getJSONObject(j).has(S.fileName)?filesArray.getJSONObject(j).getString(S.fileName):"");
                            beanFiles.setThumbName(filesArray.getJSONObject(j).has(S.thumbName)?filesArray.getJSONObject(j).getString(S.thumbName):"");
                            beanFiles.setType(filesArray.getJSONObject(j).getString(S.type));
                            filesList.add(beanFiles);
                        }
                        beanAlbum.setFilesList(filesList);
                    }

                    JSONObject pricesPresetsObject=null;
                    JSONObject watermarksPresetsObject = null;
                    try {
                        pricesPresetsObject=jsonObject1.has("pricesPresets")?jsonObject1.getJSONObject("pricesPresets"):null;
                        watermarksPresetsObject=jsonObject1.has("watermarksPresets")?jsonObject1.getJSONObject("watermarksPresets"):null;
                    }
                    catch (Exception e){
                        Log.e("tag","error");
                        e.printStackTrace();
                    }

                    if(watermarksPresetsObject!=null){
                        beanAlbum.setPresetImage(watermarksPresetsObject.has("image")?watermarksPresetsObject.getString("image"):"");
                        beanAlbum.setPresetName(watermarksPresetsObject.has("name")?watermarksPresetsObject.getString("name"):"");
                        beanAlbum.setPresetOpacity(watermarksPresetsObject.has("opacity")?watermarksPresetsObject.getString("opacity"):"");
                    }
                    if(pricesPresetsObject!=null)
                    {
                        BeanAlbum.pricesPresetsModel pricesPresetsModel=new BeanAlbum().new pricesPresetsModel();
                        pricesPresetsModel.set_id(pricesPresetsObject.has("_id")?pricesPresetsObject.getString("_id"):"");
                        pricesPresetsModel.setName(pricesPresetsObject.has("name")?pricesPresetsObject.getString("name"):"");
                        pricesPresetsModel.setStatus(pricesPresetsObject.has("status")?pricesPresetsObject.getString("status"):"");

                        JSONArray photoArray = new JSONArray();
                        photoArray = pricesPresetsObject.has("photos")?pricesPresetsObject.getJSONArray("photos"):null;
                        if(photoArray!=null)
                        {
                            ArrayList<BeanAlbum.PhotoMOdel> photoList = new ArrayList<>();
                            for(int j=0;j<photoArray.length();j++)
                            {
                                JSONObject photoObject=photoArray.getJSONObject(j);
                                BeanAlbum.PhotoMOdel photoMOdel=new BeanAlbum().new PhotoMOdel();

                                photoMOdel.set_id(photoObject.has("_id")?photoObject.getString("_id"):"");
                                photoMOdel.setNumberOfPhotos(photoObject.has("numberOfPhotos")?photoObject.getString("numberOfPhotos"):"");
                                photoMOdel.setPrice(photoObject.has("price")?photoObject.getString("price"):"");
                                photoList.add(photoMOdel);

                            }
                            beanAlbum.setPhotoList(photoList);
                        }

                    }

                    if (type == 1) {
                        mPhotoListMY.add(beanAlbum);
                    }
                    else if (type == 3) {
                        mPhotoList.add(beanAlbum);
                    }
                    else {
                        mPhotoListOffical.add(beanAlbum);
                    }

                }
                getCurrentFragment();

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void getGalleryVideoWebserviceResponse(String response, int type) {
        try {
            if (type == 1) {
                mVideoListMY.clear();
            }
            else if(type==3){
                mVideoList.clear();
            }
            else {
                mVideoListOffical.clear();
            }
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataJson.length(); i++) {
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);
                    BeanAlbum beanAlbum = new BeanAlbum();
                    beanAlbum.set_id(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanAlbum.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");
                    beanAlbum.setDateOfEvent(jsonObject1.has(S.dateOfEvent)?jsonObject1.getString(S.dateOfEvent):"");
                    beanAlbum.setCreated(jsonObject1.has(S.created)?jsonObject1.getString(S.created):"");
                    beanAlbum.setDescription(jsonObject1.has(S.description)?jsonObject1.getString(S.description):"");
                    beanAlbum.setAlbumPrice(jsonObject1.has(S.albumPrice)?jsonObject1.getString(S.albumPrice):"");
                    beanAlbum.setMediaType(2);

                    JSONArray typejsonArray = jsonObject1.getJSONArray(S.groupTypeId);

                    if (typejsonArray.length() > 0) {
                        beanAlbum.setGroupTypeId(typejsonArray.getJSONObject(0).getString(S._id));
                        beanAlbum.setGroupTypeName(typejsonArray.getJSONObject(0).getString(S.name));
                    }

                    beanAlbum.setLikedPost(jsonObject1.getString(S.likedAlbum));
                    beanAlbum.setCommentPostCount(jsonObject1.getString(S.commentAlbumCount));
                    beanAlbum.setLikedPostCount(jsonObject1.getString(S.likedAlbumCount));
                    try {
                        JSONObject userIdObject=jsonObject1.getJSONObject(S.userId);
                        beanAlbum.setUserId(userIdObject.getString(S._id));
                        beanAlbum.setUserName(userIdObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userIdObject.getJSONObject(S.personalInfo).getString(S.lastName_response));
                        beanAlbum.setUserImage(userIdObject.getJSONObject(S.personalInfo).getString(S.image));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    if (jsonObject1.getJSONArray(S.trackId).length() > 0) {
                        beanAlbum.setTrackId(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S._id));
                        beanAlbum.setTrackName(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S.name));
                    }

                    if (jsonObject1.getJSONArray(S.eventId).length() > 0) {
                        beanAlbum.setEventId(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S._id));
                        beanAlbum.setEventName(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S.name));
                    }
                    JSONArray shareAlbumUser = jsonObject1.getJSONArray(S.albumShareUser);
                    List<DashboardPostShareUserModal> albumSharePost = new ArrayList<>();
                    for (int j = 0; j < shareAlbumUser.length(); j++)
                    {
                        JSONObject shareAlbumUserJson = shareAlbumUser.getJSONObject(j);
                        DashboardPostShareUserModal dashboardPostShareUserModal = new DashboardPostShareUserModal();

                        dashboardPostShareUserModal.setId(shareAlbumUserJson.has(S._id)?shareAlbumUserJson.getString(S._id):"");
                        dashboardPostShareUserModal.setPostId(shareAlbumUserJson.has(S.albumId)?shareAlbumUserJson.getString(S.albumId):"");
                        dashboardPostShareUserModal.setModified(shareAlbumUserJson.has(S.modified)?shareAlbumUserJson.getString(S.modified):"");
                        dashboardPostShareUserModal.setCreated(shareAlbumUserJson.has(S.created)?shareAlbumUserJson.getString(S.created):"");
                        dashboardPostShareUserModal.setStatus(shareAlbumUserJson.has(S.status)?shareAlbumUserJson.getString(S.status):"");

                        try {
                            JSONObject userJson = shareAlbumUserJson.getJSONObject(S.userId);
                            UserDataBeans userDataBeans = new UserDataBeans();
                            userDataBeans.set_id(userJson.getString(S._id));
                            userDataBeans.setEmail(userJson.getString(S.email));
                            userDataBeans.setUsername(userJson.getString(S.username));
                            JSONObject personalInfoJson = userJson.getJSONObject(S.personalInfo);
                            PersonalInfo personalInfo = new PersonalInfo();
                            personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                            personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                            personalInfo.setImage(personalInfoJson.getString(S.image));
                            personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                            personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                            personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                            personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                            personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                            userDataBeans.setPersonalInfo(personalInfo);
                            dashboardPostShareUserModal.setUserDataBeans(userDataBeans);
                            albumSharePost.add(dashboardPostShareUserModal);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }




                    }
                    beanAlbum.setAlbumPostShareUserModals(albumSharePost);
                    JSONArray friendArray;
                    friendArray = jsonObject1.getJSONArray(S.albumFriend);
                    ArrayList<BeanAlbum.BeanFriend> friendList = new ArrayList<>();
                    for (int j = 0; j < friendArray.length(); j++) {
                        BeanAlbum.BeanFriend beanFriend = new BeanAlbum().new BeanFriend();
                        beanFriend.setUserFriendId(friendArray.getJSONObject(j).getString(S._id));
                        beanFriend.setFriendId(friendArray.getJSONObject(j).getJSONObject(S.friendId).getString(S._id));
                        beanFriend.setFriendName(friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                        beanFriend.setFriendImage(friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.image));
                        friendList.add(beanFriend);
                    }
                    beanAlbum.setFriendList(friendList);

                    JSONArray filesArray;
                    filesArray = jsonObject1.getJSONArray(S.filesAll);
                    ArrayList<BeanAlbum.BeanFiles> filesList = new ArrayList<>();
                    for (int j = 0; j < filesArray.length(); j++) {
                        BeanAlbum.BeanFiles beanFiles = new BeanAlbum().new BeanFiles();
                        beanFiles.setFileId(filesArray.getJSONObject(j).has(S._id)?filesArray.getJSONObject(j).getString(S._id):"");
                        beanFiles.setCreated(filesArray.getJSONObject(j).has(S.created)?filesArray.getJSONObject(j).getString(S.created):"");
                        beanFiles.setFileName(filesArray.getJSONObject(j).has(S.fileName)?filesArray.getJSONObject(j).getString(S.fileName):"");
                        beanFiles.setThumbName(filesArray.getJSONObject(j).has(S.thumbName)?filesArray.getJSONObject(j).getString(S.thumbName):"");
                        beanFiles.setType(filesArray.getJSONObject(j).getString(S.type));
                        filesList.add(beanFiles);
                    }

                    ArrayList<BeanAlbum.PhotoMOdel> photoList = new ArrayList<>();
                    JSONObject pricesPresetsObject=null;
                    JSONObject watermarksPresetsObject = null;
                    try {
                        pricesPresetsObject=jsonObject1.has("pricesPresets")?jsonObject1.getJSONObject("pricesPresets"):null;
                        watermarksPresetsObject=jsonObject1.has("watermarksPresets")?jsonObject1.getJSONObject("watermarksPresets"):null;
                    }
                    catch (Exception e){
                        Log.e("tag","error");
                        e.printStackTrace();
                    }

                    if(watermarksPresetsObject!=null){
                        beanAlbum.setPresetImage(watermarksPresetsObject.has("image")?watermarksPresetsObject.getString("image"):"");
                        beanAlbum.setPresetName(watermarksPresetsObject.has("name")?watermarksPresetsObject.getString("name"):"");
                        beanAlbum.setPresetOpacity(watermarksPresetsObject.has("opacity")?watermarksPresetsObject.getString("opacity"):"");
                    }
                    if(pricesPresetsObject!=null){
                        BeanAlbum.pricesPresetsModel pricesPresetsModel=new BeanAlbum().new pricesPresetsModel();
                        pricesPresetsModel.set_id(pricesPresetsObject.has("_id")?pricesPresetsObject.getString("_id"):"");
                        pricesPresetsModel.setName(pricesPresetsObject.has("name")?pricesPresetsObject.getString("name"):"");
                        pricesPresetsModel.setStatus(pricesPresetsObject.has("status")?pricesPresetsObject.getString("status"):"");
                        JSONArray photoArray = new JSONArray();

                        photoArray = pricesPresetsObject.has("photos")?pricesPresetsObject.getJSONArray("photos"):null;
                        if(photoArray!=null) {
                            for(int j=0;j<photoArray.length();j++){
                                JSONObject photoObject=photoArray.getJSONObject(j);
                                BeanAlbum.PhotoMOdel photoMOdel=new BeanAlbum().new PhotoMOdel();
                                photoMOdel.set_id(photoObject.has("_id")?photoObject.getString("_id"):"");
                                photoMOdel.setNumberOfPhotos(photoObject.has("numberOfPhotos")?photoObject.getString("numberOfPhotos"):"");
                                photoMOdel.setPrice(photoObject.has("price")?photoObject.getString("price"):"");
                                photoList.add(photoMOdel);
                            }
                        }

                    }
                    if (filesArray.length() != 0) {
                        beanAlbum.setFilesList(filesList);
                        beanAlbum.setPhotoList(photoList);
                        if (type == 1) {
                            mVideoListMY.add(beanAlbum);
                        }
                        if(type==3){
                            mVideoList.add(beanAlbum);
                        }
                        else {
                            mVideoListOffical.add(beanAlbum);
                        }

                    }
                }
                getCurrentFragment();

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void passDataToActivity(String someValue) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        FastLapApplication.mCurrentContext = context;
        downloadGalleryData();
    }


    public void onClickOnshare(int position, int mediaType) {
        if (mediaType == 1) {
            authAPI.albumShareByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mPhotoList.get(position).get_id());
        } else {
            authAPI.albumShareByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mVideoList.get(position).get_id());
        }

    }

    public void onCLickOnFb(final int position, final int mediaType) {
        beanAlbum = new BeanAlbum();
        if (mediaType == 1) {
            beanAlbum = mPhotoList.get(position);
        } else {
            beanAlbum = mVideoList.get(position);
        }
        String url=null;
        if (beanAlbum.getFilesList().size()>0) {
            if (mediaType == 2){
                url=S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getThumbName();
            }
            else{
                url=S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getFileName();
            }
            shareMediaFacebook(url);
        }
        else {
            Toast.makeText(this, "Media content not found", Toast.LENGTH_SHORT).show();
        }
        /*beanAlbum = new BeanAlbum();
        if (mediaType == 1) {
            beanAlbum = mPhotoList.get(position);
        } else {
            beanAlbum = mVideoList.get(position);
        }
        if (beanAlbum.getFilesList().size() == 0) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, beanAlbum.getName());
            startActivity(Intent.createChooser(intent, "Share with"));
        }
        else {
            List<String> permissionNeeds = Arrays.asList("publish_actions");
            LoginManager.getInstance().logInWithPublishPermissions(this, permissionNeeds);
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult)
                {
                    List<Bitmap> temp = null;
                    try {
                        if (mediaType == 1)
                        {
                            temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL +
                                    beanAlbum.getFilesList().get(0).getFileName(), ImageLoader.getInstance().
                                    getMemoryCache());
                            }
                        else{
                            temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL +
                                    beanAlbum.getFilesList().get(0).getThumbName(), ImageLoader.getInstance().
                                    getMemoryCache());
                        }
                        SharePhoto photo = new SharePhoto.Builder()
                                .setBitmap(temp.get(0))
                                .build();

                        ArrayList<SharePhoto> photos = new ArrayList<>();
                        photos.add(photo);
                        SharePhotoContent content = new SharePhotoContent.Builder()
                                .setPhotos(photos)
                                .build();

                        ShareDialog shareDialog = new ShareDialog(GalleryActivity.this);
                        if (ShareDialog.canShow(SharePhotoContent.class)) {
                            shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                        }
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                Toast.makeText(context, "Media share sucessfully.", Toast.LENGTH_SHORT).show();
                                Log.e("DashBoard", "====SUCCESS");
                            }

                            @Override
                            public void onCancel() {
                                Log.e("DashBoard", "====CANCEL");
                            }

                            @Override
                            public void onError(FacebookException error) {
                                Log.e("DashBoard", "====ERROR");
                            }
                        });
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Log.e(TAG, error.toString());
                }
            });

        }*/
    }


    // Post share webservice response
    public void sharedPostWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                downloadGalleryData();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    public void albumRemoveWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                downloadGalleryData();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public void callDeleteAlbum(String albumId) {
        authAPI.deleteAbluum(context, albumId, MySharedPreferences.getPreferences(context, S.user_id));
    }

    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                mArrayVechineList.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicle beanVehicle = new BeanVehicle();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicle.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanVehicle.setImage(jsonObject1.has(S.image)?jsonObject1.getString(S.image):"");
                    beanVehicle.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");
                    mArrayVechineList.add(beanVehicle);
                }
                communityVechiceListAdapter.notifyDataSetChanged();

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void getVehicleTypeClick(int pos, String select_id) {
        String id = mArrayVechineList.get(pos).getId();

        Fragment mCurrentFragment = null;
        if (adapter != null && viewpager != null) {
            mCurrentFragment = adapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((PhotosFragment) mCurrentFragment).filterData(id);
        }

    }

    private void shareMediaFacebook(String url){
        Log.e("tag","url"+url);
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        loginManager = LoginManager.getInstance();
        loginManager.logInWithPublishPermissions(this, permissionNeeds);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult) {
                sharePhotoToFacebook(url);
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception)
            {
                System.out.println("onError");
            }
        });
    }
    private void sharePhotoToFacebook(String url){
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.car_img);
        ShareDialog shareDialog = new ShareDialog(GalleryActivity.this);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setImageUrl(Uri.parse(url))
                    .setContentTitle("Fast lap")
                    .setContentDescription("Hello this is fast lap sharing media.")
                    //.setContentUrl(Uri.parse(url))
                    .build();
            shareDialog.show(linkContent);
        }
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(context, "Media share sucessfully.", Toast.LENGTH_SHORT).show();
                Log.e("DashBoard", "====SUCCESS");
            }

            @Override
            public void onCancel() {
                Log.e("DashBoard", "====CANCEL");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("DashBoard", "====ERROR");
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }
}
