package com.os.fastlap.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIChart;
import com.highsoft.highcharts.Common.HIChartsClasses.HICredits;
import com.highsoft.highcharts.Common.HIChartsClasses.HIDateTimeLabelFormats;
import com.highsoft.highcharts.Common.HIChartsClasses.HIEvents;
import com.highsoft.highcharts.Common.HIChartsClasses.HIExporting;
import com.highsoft.highcharts.Common.HIChartsClasses.HILabels;
import com.highsoft.highcharts.Common.HIChartsClasses.HILegend;
import com.highsoft.highcharts.Common.HIChartsClasses.HIOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIPlotOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIPoint;
import com.highsoft.highcharts.Common.HIChartsClasses.HISeries;
import com.highsoft.highcharts.Common.HIChartsClasses.HISpline;
import com.highsoft.highcharts.Common.HIChartsClasses.HIStyle;
import com.highsoft.highcharts.Common.HIChartsClasses.HITitle;
import com.highsoft.highcharts.Common.HIChartsClasses.HITooltip;
import com.highsoft.highcharts.Common.HIChartsClasses.HIXAxis;
import com.highsoft.highcharts.Common.HIChartsClasses.HIYAxis;
import com.highsoft.highcharts.Common.HIColor;
import com.highsoft.highcharts.Core.HIChartView;
import com.highsoft.highcharts.Core.HIFunction;
import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsRaceActivity;
import com.os.fastlap.adapter.paddock.CompareLapAdapter;
import com.os.fastlap.beans.BeanCompareData;
import com.os.fastlap.beans.BeanLocation;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FullScreenChartActivity extends BaseActivity {
    HIChartView splineView;
    TextView mTitle;
    private boolean isSpeed=false;
    private boolean isAccCalc=false;
    private boolean isAccOn=false;
    private boolean isBrakeCalc=false;
    private boolean isBrakeOn=false;
    private boolean isXView=false;
    private boolean isYView=false;
    private boolean isZView=false;
    ArrayList<BeanTimeLap> mUserList;
    ArrayList<Integer> selectIds;
    PaddockMyLapsParentModel paddockMyLapsParentModel;
    AuthAPI authAPI;
    Context context;
    private AppCompatImageView baseToggleIcon;
    ArrayList<BeanCompareData> compareList;
    MyProgressDialog myProgressDialog;
    TextViewPlayRegular mUsername,mAverageSpeed,mMaxSpeed;
    TextView colorTv;
    TextView backTitle;
    RecyclerView mRecyclerView;
    private String chartType="";
    private CompareLapAdapter compareLapAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_graph);
        myProgressDialog=new MyProgressDialog(this);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        mRecyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        backTitle = (TextView) findViewById(R.id.backTitle);
        backTitle.setVisibility(View.VISIBLE);
        colorTv=(TextView)findViewById(R.id.colorTv);
        mUsername=(TextViewPlayRegular)findViewById(R.id.tv_user_name);
        mMaxSpeed=(TextViewPlayRegular)findViewById(R.id.tv_max_speed);
        mAverageSpeed=(TextViewPlayRegular)findViewById(R.id.tv_average_speed);
        context = this;
        authAPI = new AuthAPI(context);
        paddockMyLapsParentModel = new PaddockMyLapsParentModel();
        selectIds = getIntent().getIntegerArrayListExtra("selectIds");

        paddockMyLapsParentModel = (PaddockMyLapsParentModel) getIntent().getSerializableExtra("grouplist");
        compareList = new ArrayList<>();
        isSpeed= MyLapsRaceActivity.isSpeed;
        isAccCalc=MyLapsRaceActivity.isAccCalc;
        isAccOn=MyLapsRaceActivity.isAccOn;
        isBrakeCalc=MyLapsRaceActivity.isBrakeCalc;
        isBrakeOn=MyLapsRaceActivity.isBrakeOn;
        isXView=MyLapsRaceActivity.isXView;
        isYView=MyLapsRaceActivity.isYView;
        isZView=MyLapsRaceActivity.isZView;
        chartType=getIntent().getStringExtra("CHART_TYPE");
        mUserList= (ArrayList<BeanTimeLap>) getIntent().getSerializableExtra("mUserList");

        splineView = (HIChartView) findViewById(R.id.hichart);
        mTitle=(TextView)findViewById(R.id.toolbarheading);
        mTitle.setText("Graph View");
        mUserList = new ArrayList<>();
        mUserList.clear();
        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        downloadFiles();
        setUserList();
    }
    private void setUserList(){
        compareLapAdapter=new CompareLapAdapter(mContext,mUserList,false);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(compareLapAdapter);
    }

    private void downloadFiles() {
        for (int i = 0; i < selectIds.size(); i++) {
            String filePath = "";
            mUserList.add(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)));
            // mArrayList_user.add(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)));
            filePath = paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getFileUrl();
            int color = paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getLapColor();
            mUserList.get(i).setUserName(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getUserName());
            mUserList.get(i).setUserImage(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getUserImage());
            long milisecond = Util.TimeToMiliseconds(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getTime());
            Log.e("tag_octal","file path"+filePath);
            authAPI.readJsonFileData(this, filePath, color, milisecond);
        }
    }
    public void getUrlData(String response, int color, Long milisecond) {
        //Log.e("tag_octal","json responce: "+response.toString());
        try {
            JSONArray jsonArray = new JSONArray(response);
            readFileandDrowPathDemo(jsonArray, color, milisecond);
        } catch (Exception e) {
            Log.e("tag", e.toString());
        }
    }
    private void readFileandDrowPathDemo(JSONArray jsonArray, int color, long milisecond) {
        myProgressDialog.show();
        ArrayList<BeanLocation> chartArraylist = new ArrayList<>();
        PolylineOptions line = new PolylineOptions();
        BeanCompareData beanCompareData = new BeanCompareData();
        ArrayList<LatLng> latlagList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++)
        {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject != null) {
                    BeanLocation beanLocation = new BeanLocation();
                    LatLng latLng;
                    if(!jsonObject.optString("accel_x").equalsIgnoreCase("null")&&!jsonObject.optString("accel_x").equals(null)){
                        beanLocation.setAccel_x(jsonObject.has("accel_x")?jsonObject.optString("accel_x"):"0");
                    }
                    if(!jsonObject.optString("accel_y").equalsIgnoreCase("null")&&!jsonObject.optString("accel_y").equals(null)){
                        beanLocation.setAccel_y(jsonObject.has("accel_y")?jsonObject.optString("accel_y"):"0");
                    }
                    if(!jsonObject.optString("accel_z").equalsIgnoreCase("null")&&!jsonObject.optString("accel_z").equals(null)){
                        beanLocation.setAccel_z(jsonObject.has("accel_z")?jsonObject.optString("accel_z"):"0");
                    }
                    if(!jsonObject.optString("brake_calc").equalsIgnoreCase("null")&&!jsonObject.optString("brake_calc").equals(null)){
                        beanLocation.setBrake_calc(jsonObject.has("brake_calc")?jsonObject.optString("brake_calc"):"0");
                    }
                    if(!jsonObject.optString("acc_calc").equalsIgnoreCase("null")&&!jsonObject.optString("acc_calc").equals(null)){
                        beanLocation.setAcc_calc(jsonObject.has("acc_calc")?jsonObject.optString("acc_calc"):"0");
                    }
                    if(!jsonObject.optString("brake_on").equalsIgnoreCase("null")&&!jsonObject.optString("brake_on").equals(null)){
                        beanLocation.setBrake_on(jsonObject.has("brake_on")?jsonObject.optString("brake_on"):"0");
                    }
                    if(!jsonObject.optString("acc_on").equalsIgnoreCase("null")&&!jsonObject.optString("acc_on").equals(null)){
                        beanLocation.setAcc_on(jsonObject.has("acc_on")?jsonObject.optString("acc_on"):"0");
                    }
                    if(!jsonObject.optString("lat").equalsIgnoreCase("null")&&jsonObject.optString("lat").equals(null)){
                        beanLocation.setLat(jsonObject.has("lat")?jsonObject.optString("lat"):"0");
                    }
                    if(!jsonObject.optString("lng").equalsIgnoreCase("null")&&!jsonObject.optString("lng").equals(null)){
                        beanLocation.setLng(jsonObject.has("lng")?jsonObject.optString("lng"):"0");
                    }
                    if(!jsonObject.optString("speed").equalsIgnoreCase("null")&&!jsonObject.optString("speed").equals(null)){
                        beanLocation.setSpeed(jsonObject.has("speed")?jsonObject.optString("speed"):"0");
                    }
                    if(!jsonObject.optString("time").equalsIgnoreCase("null")&&!jsonObject.optString("time").equals(null)){
                        beanLocation.setTime(jsonObject.has("time")?jsonObject.optString("time"):"0");
                    }
                    if(!jsonObject.optString("dist").equalsIgnoreCase("null")&&!jsonObject.optString("dist").equals(null)){
                        beanLocation.setDist(jsonObject.has("dist")?jsonObject.optString("dist"):"0");
                    }

                    chartArraylist.add(beanLocation);
                    latLng = new LatLng(Double.parseDouble(jsonObject.has("lat")?jsonObject.optString("lat"):"0"), Double.parseDouble(jsonObject.has("lng")?jsonObject.optString("lng"):"0"));
                    line.add(new LatLng(Double.parseDouble(jsonObject.has("lat")?jsonObject.optString("lat"):"0"), Double.parseDouble(jsonObject.has("lng")?jsonObject.optString("lng"):"0")));
                    latlagList.add(latLng);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int speed = (int) milisecond / chartArraylist.size();
        beanCompareData.setLocationArrayList(chartArraylist);
        beanCompareData.setRunSpeed(speed);
        beanCompareData.setId(1 + "");
        beanCompareData.setRouteLatLng(latlagList);
        beanCompareData.setColor(color);
        line.width(2).color(color);
        compareList.add(beanCompareData);
       if(chartType.equalsIgnoreCase("Time"))
           setChartData();
       else
           setChartDataWithDistance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        backTitle.setVisibility(View.GONE);
    }

    private void setChartData()
    {
        ArrayList<HIYAxis>arrYAxis=new ArrayList<>();
        ArrayList<HISpline>dataSets=new ArrayList<>();
        HashMap axisDic = new HashMap();

        HIOptions options = new HIOptions();
        HIChart chart = new HIChart();
        chart.setZoomType("x");
        chart.setSpacingBottom(8);
        chart.setSpacingLeft(2);
        chart.setSpacingRight(2);
        chart.setSpacingTop(8);
        chart.setBackgroundColor(HIColor.initWithHexValue("FFFFFF"));

        chart.setBorderRadius(6);
        chart.setType("spline");

        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setSeries(new HISeries());
        plotOptions.getSeries().setColor(HIColor.initWithRGBA(255, 255, 255, 0.6));
        plotOptions.getSeries().setPoint(new HIPoint());
        plotOptions.getSeries().getPoint().setEvents(new HIEvents());

        HITitle title = new HITitle();
        title.setText("");

        HITooltip tooltip = new HITooltip();
        tooltip.setShared(true);
        tooltip.setHeaderFormat("");
        tooltip.setFormatter(new HIFunction("function () {if (this.points[0] && this.points[0].point && this.points[0].point.index && this.points[0].series && this.points[0].series.userOptions && this.points[0].series.userOptions.data && this.points[0].series.userOptions.data[this.points[0].point.index] && this.points[0].series.userOptions.data[this.points[0].point.index][2]){var customData = this.points[0].series.userOptions.data[this.points[0].point.index][2];var html = '<b>Name:</b> ' + customData.user + '<br /><b>Distance:</b> ' + customData.distance + '<br />'+'<br /><b>Time:</b> ' + customData.time + '<br />';for (var key in customData){if (customData.hasOwnProperty(key)) {if(key == 'user' || key == 'distance'||key == 'time') {}else if (key == 'speed'){if (("+isSpeed+")) { html += '<b>Speed:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_x') { if ("+isXView+") { html += '<b>Accel X:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_y') { if ("+isYView+") { html += '<b>Accel Y:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_z') { if ("+isZView+") { html += '<b>Accel Z:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_calc') { if ("+isBrakeCalc+") { html += '<b>Brake Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_on') { if ("+isBrakeOn+") { html += '<b>Brake On:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_calc') { if ("+isAccCalc+") { html += '<b>Accel Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_on') { if ("+isAccOn+") { html += '<b>Accel On:</b> ' + customData[key] + '<br />'; } } }}}return html;}"));
        HILegend legend=new HILegend();
        legend.setEnabled(false);

        HICredits credits = new HICredits();
        credits.setEnabled(false);

        HIExporting exporting = new HIExporting();
        exporting.setEnabled(false);

        if (isSpeed) {
            arrYAxis.add(getSpeedYAxis());
        }
        if (isAccCalc) {
            arrYAxis.add(getAccelCalcYAxis());
        }
        if (isAccOn) {
            arrYAxis.add(getAccelOnYAxis());
        }
        if (isBrakeCalc) {
            arrYAxis.add(getBrakeCalcYAxis());
        }
        if (isBrakeOn) {
            arrYAxis.add(getBrakeOnYAxis());
        }
        if (isXView) {
            arrYAxis.add(getXAxis());
        }
        if (isYView) {
            arrYAxis.add(getYAxis());
        }
        if (isZView) {
            arrYAxis.add(getZAxis());
        }

        if (arrYAxis.size()>0) {
            for( int i=0;i<arrYAxis.size();i++) {
                HIYAxis yAxis=arrYAxis.get(i);
                axisDic.put(yAxis.getTitle().getText(),i);
            }
        }
        Log.e("tag","compareList1  size"+compareList.size());
        for(int i=0;i<compareList.size();i++){
            int color=compareList.get(i).getColor();
            String defaultColor = String.format("%06X", (0xFFFFFF & color));
            ArrayList<BeanLocation> locationList = new ArrayList<>();
            BeanCompareData beanCompareData = new BeanCompareData();
            beanCompareData = compareList.get(i);
            locationList = beanCompareData.getLocationArrayList();

            ArrayList<Object[]> speedData=new ArrayList<>();
            ArrayList<Object[]> brakeCalcData =new ArrayList<>();
            ArrayList<Object[]> brakeOnData =new ArrayList<>();
            ArrayList<Object[]> accelCalcData =new ArrayList<>();
            ArrayList<Object[]> accelOnData =new ArrayList<>();

            ArrayList<Object[]> xData =new ArrayList<>();
            ArrayList<Object[]> yData =new ArrayList<>();
            ArrayList<Object[]> zData =new ArrayList<>();

            ArrayList<Object[]> tooltipData =new ArrayList<>();

            for(int index=0;index<locationList.size();index++){
                HashMap dataDic = new HashMap();
                dataDic.put("speed",locationList.get(index).getSpeed());
                dataDic.put("distance",locationList.get(index).getDist());
                dataDic.put("time",Util.getTimeFormat(locationList.get(index).getTime()));
                dataDic.put("user",mUserList.get(i).getUserName());
                dataDic.put("accel_x",locationList.get(index).getAccel_x());
                dataDic.put("accel_y",locationList.get(index).getAccel_y());
                dataDic.put("accel_z",locationList.get(index).getAccel_z());
                dataDic.put("brake_calc",locationList.get(index).getBrake_calc());
                dataDic.put("acc_calc",locationList.get(index).getAcc_calc());
                dataDic.put("brake_on",locationList.get(index).getBrake_on());
                dataDic.put("acc_on",locationList.get(index).getAcc_on());

                speedData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getSpeed()),dataDic} );
                brakeCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getBrake_calc()),dataDic} );
                brakeOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getBrake_on()),dataDic} );
                accelCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAcc_calc()),dataDic} );
                accelOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAcc_on()),dataDic} );
                xData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAccel_x()),dataDic} );
                yData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAccel_y()),dataDic} );
                zData.add(new Object[] {Double.parseDouble(locationList.get(index).getTime())*1000, Double.parseDouble(locationList.get(index).getAccel_z()),dataDic} );
                tooltipData.add(new Object[]{dataDic});
            }

            if(isSpeed){
                HISpline speedLine = new HISpline();
                speedLine.setTooltip(new HITooltip());
                speedLine.setData(speedData);
                speedLine.setName("Speed");
                speedLine.setYAxis(axisDic.get("Speed(km/h)"));
                speedLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(speedLine);

            }
            if(isBrakeCalc){
                HISpline brakeCalcLine = new HISpline();
                brakeCalcLine.setTooltip(new HITooltip());
                brakeCalcLine.setData(brakeCalcData);
                brakeCalcLine.setName("Brake Calc");
                brakeCalcLine.setYAxis(axisDic.get("Brake Calc"));
                brakeCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeCalcLine);

            }
            if(isBrakeOn){
                HISpline brakeOnLine = new HISpline();
                brakeOnLine.setTooltip(new HITooltip());
                brakeOnLine.setData(brakeOnData);
                brakeOnLine.setName("Brake On");
                brakeOnLine.setYAxis(axisDic.get("Brake On"));
                brakeOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeOnLine);
            }
            if(isAccCalc){
                HISpline accelCalcLine = new HISpline();
                accelCalcLine.setTooltip(new HITooltip());
                accelCalcLine.setData(accelCalcData);
                accelCalcLine.setName("Accel Calc");
                accelCalcLine.setYAxis(axisDic.get("Accel Calc"));
                accelCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelCalcLine);

            }
            if(isAccOn){
                HISpline accelOnLine = new HISpline();
                accelOnLine.setTooltip(new HITooltip());
                accelOnLine.setData(accelOnData);
                accelOnLine.setName("Accel On");
                accelOnLine.setYAxis(axisDic.get("Accel On"));
                accelOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelOnLine);

            }

        }
        options.setChart(chart);
        options.setXAxis(new ArrayList<HIXAxis>(){{add(getTimeXAxis());}});
        options.setYAxis(new ArrayList<>(arrYAxis));
        options.setSeries(new ArrayList<HISeries>(dataSets));
        options.setLegend(legend);
        options.setTitle(title);
        options.setExporting(exporting);
        options.setPlotOptions(plotOptions);
        options.setTooltip(tooltip);
        options.setCredits(credits);
        splineView.setOptions(options);
        splineView.setVisibility(View.VISIBLE);
        myProgressDialog.dismiss();

    }

    private HIXAxis getTimeXAxis(){
        HIXAxis xaxis = new HIXAxis();
        xaxis.setLabels(new HILabels());
        xaxis.getLabels().setStyle(new HIStyle());
        xaxis.getLabels().getStyle().setColor("#000000");
        xaxis.setTitle(new HITitle());
        xaxis.setAllowDecimals(true);
        xaxis.getTitle().setText("Time");
        xaxis.setDateTimeLabelFormats(new HIDateTimeLabelFormats());
        xaxis.getDateTimeLabelFormats().setMillisecond("%M:%S.%L");
        xaxis.getDateTimeLabelFormats().setSecond("%M:%S");
        xaxis.getDateTimeLabelFormats().setMinute("%M:%S");
        xaxis.getDateTimeLabelFormats().setHour("");
        xaxis.getDateTimeLabelFormats().setDay("");
        xaxis.getDateTimeLabelFormats().setWeek("");
        xaxis.getDateTimeLabelFormats().setMonth("");
        xaxis.getDateTimeLabelFormats().setYear("");
        xaxis.setType("datetime");
        xaxis.setMinRange(10);

        xaxis.getTitle().setStyle(new HIStyle());
        xaxis.getTitle().getStyle().setColor(xaxis.getLabels().getStyle().getColor());
        return xaxis ;

    }
    private HIYAxis getSpeedYAxis(){
        final HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("rgb(255, 255, 255");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Speed(km/h)");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor("rgb(255, 255, 255");
        yaxis.setOpposite(false);
        yaxis.setTickColor(HIColor.initWithRGBA(255, 255, 255, 0.0));
        yaxis.setLineColor(HIColor.initWithRGBA(255, 255, 255, 0.3));
        return yaxis ;
    }
    private HIYAxis getBrakeCalcYAxis(){
        final HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#90ed7d");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Brake Calc");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor("#90ed7d");
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
    private HIYAxis getBrakeOnYAxis(){
        final HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#90ed7d");
        yaxis.setTitle(new HITitle());
        yaxis.setAlignTicks(true);
        yaxis.setMax(1);
        yaxis.getTitle().setText("Brake On");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor("#90ed7d");
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }

    private HIYAxis getAccelCalcYAxis(){
        final HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#90ed7d");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Accel Calc");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor("#90ed7d");
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }

    private HIYAxis getAccelOnYAxis(){
        final HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#90ed7d");
        yaxis.setTitle(new HITitle());
        yaxis.setAlignTicks(true);
        yaxis.setMax(1);
        yaxis.getTitle().setText("Accel On");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor("#90ed7d");
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
    private HIYAxis getXAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("X");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
    private HIYAxis getYAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Y");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }
    private HIYAxis getZAxis(){
        HIYAxis yaxis = new HIYAxis();
        yaxis.setLabels(new HILabels());
        yaxis.getLabels().setStyle(new HIStyle());
        yaxis.getLabels().getStyle().setColor("#000000");
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("Y");
        yaxis.getTitle().setStyle(new HIStyle());
        yaxis.getTitle().getStyle().setColor(yaxis.getLabels().getStyle().getColor());
        yaxis.setOpposite(true);
        yaxis.setVisible(false);
        return yaxis ;
    }


    private HIXAxis getDistanceXAxis(){
        HIXAxis xaxis = new HIXAxis();
        xaxis.setLabels(new HILabels());
        xaxis.getLabels().setStyle(new HIStyle());
        xaxis.getLabels().getStyle().setColor("#000000");
        xaxis.setTitle(new HITitle());
        xaxis.getTitle().setText("Distance");
        //xaxis.getLabels().setDistance(15);
        xaxis.getLabels().setFormat("{value} m");
        xaxis.setMinRange(10);
        xaxis.getTitle().setStyle(new HIStyle());
        xaxis.getTitle().getStyle().setColor(xaxis.getLabels().getStyle().getColor());
        return xaxis ;

    }

    private void setChartDataWithDistance()
    {
        ArrayList<HIYAxis>arrYAxis=new ArrayList<>();
        ArrayList<HISpline>dataSets=new ArrayList<>();
        HashMap axisDic = new HashMap();

        HIOptions options = new HIOptions();
        HIChart chart = new HIChart();
        chart.setZoomType("x");
        chart.setSpacingBottom(8);
        chart.setSpacingLeft(2);
        chart.setSpacingRight(2);
        chart.setSpacingTop(8);
        chart.setBackgroundColor(HIColor.initWithHexValue("FFFFFF"));

        chart.setBorderRadius(6);
        chart.setType("spline");

        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setSeries(new HISeries());
        plotOptions.getSeries().setColor(HIColor.initWithRGBA(255, 255, 255, 0.6));
        plotOptions.getSeries().setPoint(new HIPoint());
        plotOptions.getSeries().getPoint().setEvents(new HIEvents());

        HITitle title = new HITitle();
        title.setText("");

        HITooltip tooltip = new HITooltip();
        tooltip.setShared(true);
        tooltip.setHeaderFormat("");
        tooltip.setFormatter(new HIFunction("function () {if (this.points[0] && this.points[0].point && this.points[0].point.index && this.points[0].series && this.points[0].series.userOptions && this.points[0].series.userOptions.data && this.points[0].series.userOptions.data[this.points[0].point.index] && this.points[0].series.userOptions.data[this.points[0].point.index][2]){var customData = this.points[0].series.userOptions.data[this.points[0].point.index][2];var html = '<b>Name:</b> ' + customData.user + '<br /><b>Distance:</b> ' + customData.distance + '<br />'+'<br /><b>Time:</b> ' + customData.time + '<br />';for (var key in customData){if (customData.hasOwnProperty(key)) {if(key == 'user' || key == 'distance'||key == 'time') {}else if (key == 'speed'){if (("+isSpeed+")) { html += '<b>Speed:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_x') { if ("+isXView+") { html += '<b>Accel X:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_y') { if ("+isYView+") { html += '<b>Accel Y:</b> ' + customData[key] + '<br />'; } }else if (key == 'accel_z') { if ("+isZView+") { html += '<b>Accel Z:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_calc') { if ("+isBrakeCalc+") { html += '<b>Brake Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'brake_on') { if ("+isBrakeOn+") { html += '<b>Brake On:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_calc') { if ("+isAccCalc+") { html += '<b>Accel Calc:</b> ' + customData[key] + '<br />'; } }else if (key == 'acc_on') { if ("+isAccOn+") { html += '<b>Accel On:</b> ' + customData[key] + '<br />'; } } }}}return html;}"));
        HILegend legend=new HILegend();
        legend.setEnabled(false);

        HICredits credits = new HICredits();
        credits.setEnabled(false);

        HIExporting exporting = new HIExporting();
        exporting.setEnabled(false);

        if (isSpeed) {
            arrYAxis.add(getSpeedYAxis());
        }
        if (isAccCalc) {
            arrYAxis.add(getAccelCalcYAxis());
        }
        if (isAccOn) {
            arrYAxis.add(getAccelOnYAxis());
        }
        if (isBrakeCalc) {
            arrYAxis.add(getBrakeCalcYAxis());
        }
        if (isBrakeOn) {
            arrYAxis.add(getBrakeOnYAxis());
        }
        if (isXView) {
            arrYAxis.add(getXAxis());
        }
        if (isYView) {
            arrYAxis.add(getYAxis());
        }
        if (isZView) {
            arrYAxis.add(getZAxis());
        }

        if (arrYAxis.size()>0) {
            for( int i=0;i<arrYAxis.size();i++) {
                HIYAxis yAxis=arrYAxis.get(i);
                axisDic.put(yAxis.getTitle().getText(),i);
            }
        }
        Log.e("tag","compareList1  size"+compareList.size());
        for(int i=0;i<compareList.size();i++){
            int color=compareList.get(i).getColor();
            String defaultColor = String.format("%06X", (0xFFFFFF & color));
            ArrayList<BeanLocation> locationList = new ArrayList<>();
            BeanCompareData beanCompareData = new BeanCompareData();
            beanCompareData = compareList.get(i);
            locationList = beanCompareData.getLocationArrayList();

            ArrayList<Object[]> speedData=new ArrayList<>();
            ArrayList<Object[]> brakeCalcData =new ArrayList<>();
            ArrayList<Object[]> brakeOnData =new ArrayList<>();
            ArrayList<Object[]> accelCalcData =new ArrayList<>();
            ArrayList<Object[]> accelOnData =new ArrayList<>();

            ArrayList<Object[]> xData =new ArrayList<>();
            ArrayList<Object[]> yData =new ArrayList<>();
            ArrayList<Object[]> zData =new ArrayList<>();

            ArrayList<Object[]> tooltipData =new ArrayList<>();

            for(int index=0;index<locationList.size();index++){
                HashMap dataDic = new HashMap();
                dataDic.put("speed",locationList.get(index).getSpeed());
                dataDic.put("distance",locationList.get(index).getDist());
                dataDic.put("time",Util.getTimeFormat(locationList.get(index).getTime()));
                dataDic.put("user",mUserList.get(i).getUserName());
                dataDic.put("accel_x",locationList.get(index).getAccel_x());
                dataDic.put("accel_y",locationList.get(index).getAccel_y());
                dataDic.put("accel_z",locationList.get(index).getAccel_z());
                dataDic.put("brake_calc",locationList.get(index).getBrake_calc());
                dataDic.put("acc_calc",locationList.get(index).getAcc_calc());
                dataDic.put("brake_on",locationList.get(index).getBrake_on());
                dataDic.put("acc_on",locationList.get(index).getAcc_on());

                speedData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getSpeed()),dataDic} );
                brakeCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getBrake_calc()),dataDic} );
                brakeOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getBrake_on()),dataDic} );
                accelCalcData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAcc_calc()),dataDic} );
                accelOnData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAcc_on()),dataDic} );
                xData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAccel_x()),dataDic} );
                yData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAccel_y()),dataDic} );
                zData.add(new Object[] {Double.parseDouble(locationList.get(index).getDist()), Double.parseDouble(locationList.get(index).getAccel_z()),dataDic} );
                tooltipData.add(new Object[]{dataDic});
            }

            if(isSpeed){
                HISpline speedLine = new HISpline();
                speedLine.setTooltip(new HITooltip());
                speedLine.setData(speedData);
                speedLine.setName("Speed");
                speedLine.setYAxis(axisDic.get("Speed(km/h)"));
                speedLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(speedLine);

            }
            if(isBrakeCalc){
                HISpline brakeCalcLine = new HISpline();
                brakeCalcLine.setTooltip(new HITooltip());
                brakeCalcLine.setData(brakeCalcData);
                brakeCalcLine.setName("Brake Calc");
                brakeCalcLine.setYAxis(axisDic.get("Brake Calc"));
                brakeCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeCalcLine);

            }
            if(isBrakeOn){
                HISpline brakeOnLine = new HISpline();
                brakeOnLine.setTooltip(new HITooltip());
                brakeOnLine.setData(brakeOnData);
                brakeOnLine.setName("Brake On");
                brakeOnLine.setYAxis(axisDic.get("Brake On"));
                brakeOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(brakeOnLine);
            }
            if(isAccCalc){
                HISpline accelCalcLine = new HISpline();
                accelCalcLine.setTooltip(new HITooltip());
                accelCalcLine.setData(accelCalcData);
                accelCalcLine.setName("Accel Calc");
                accelCalcLine.setYAxis(axisDic.get("Accel Calc"));
                accelCalcLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelCalcLine);

            }
            if(isAccOn){
                HISpline accelOnLine = new HISpline();
                accelOnLine.setTooltip(new HITooltip());
                accelOnLine.setData(accelOnData);
                accelOnLine.setName("Accel On");
                accelOnLine.setYAxis(axisDic.get("Accel On"));
                accelOnLine.setColor(HIColor.initWithHexValue(defaultColor));
                dataSets.add(accelOnLine);

            }

        }
        options.setChart(chart);
        options.setXAxis(new ArrayList<HIXAxis>(){{add(getDistanceXAxis());}});
        options.setYAxis(new ArrayList<>(arrYAxis));
        options.setSeries(new ArrayList<HISeries>(dataSets));
        options.setLegend(legend);
        options.setTitle(title);
        options.setExporting(exporting);
        options.setPlotOptions(plotOptions);
        options.setTooltip(tooltip);
        options.setCredits(credits);
        splineView.setOptions(options);
        splineView.setVisibility(View.VISIBLE);
        myProgressDialog.dismiss();

    }

}
