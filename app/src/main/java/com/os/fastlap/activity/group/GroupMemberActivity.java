package com.os.fastlap.activity.group;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.adapter.dashboard.LikesUserAdapter;
import com.os.fastlap.beans.dashboardmodals.LikeUserBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 7/12/2017.
 */

public class GroupMemberActivity extends BaseActivity implements View.OnClickListener, LikesUserAdapter.ClickListner {


    private RecyclerView memberRecyclerView;
    ArrayList<LikeUserBean> member_list = new ArrayList<>();
    ArrayList<LikeUserBean> member_listAll = new ArrayList<>();
    LikesUserAdapter memberUserAdapter;
    Context context;
    ImageView base_toggle_icon;
    AuthAPI authAPI;
    private String TAG = GroupMemberActivity.class.getSimpleName();
    TextView all_tab_tv;
    TextView friends_tab_tv;
    ImageView add_album;

    TextView no_data_tv;
    String ownerId = "";
    boolean isAdmin = false;
    boolean isSubAdmin = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_member_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();

        authAPI.getGroupMembersList(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId);

        ownerId = getIntent().getStringExtra(S.ownerId);


    }

    private void initView() {
        memberRecyclerView = (RecyclerView) findViewById(R.id.likes_recyclerView);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        all_tab_tv = (TextView) findViewById(R.id.all_tab_tv);
        friends_tab_tv = (TextView) findViewById(R.id.friends_tab_tv);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        add_album = (ImageView) findViewById(R.id.add_album);
        clickListner();

        member_list = new ArrayList<>();
        member_list.clear();
        member_listAll = new ArrayList<>();
        member_listAll.clear();

        memberUserAdapter = new LikesUserAdapter(member_list, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        memberRecyclerView.setLayoutManager(layoutManager);
        memberRecyclerView.setItemAnimator(new DefaultItemAnimator());
        memberRecyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        memberRecyclerView.setAdapter(memberUserAdapter);
        memberUserAdapter.setOnclickListner(this);


    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        all_tab_tv.setOnClickListener(this);
        friends_tab_tv.setOnClickListener(this);
        add_album.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.all_tab_tv:
                allMemberList();
                break;
            case R.id.friends_tab_tv:
                friendsList();
                break;
            case R.id.add_album:
                startActivity(new Intent(context, InviteGroupMemberActivity.class));
                break;
        }
    }

    private void friendsList() {
        all_tab_tv.setBackgroundColor(ContextCompat.getColor(GroupMemberActivity.this, R.color.white_color));
        friends_tab_tv.setBackgroundColor(ContextCompat.getColor(GroupMemberActivity.this, R.color.light_gray_color));

        member_list.clear();

        for (int i = 0; i < member_listAll.size(); i++) {
            if (member_listAll.get(i).getLike_user_friend_status().compareTo("1") == 0) {
                member_list.add(member_listAll.get(i));
            }
        }


        memberUserAdapter.notifyDataSetChanged();
    }

    private void allMemberList() {
        all_tab_tv.setBackgroundColor(ContextCompat.getColor(GroupMemberActivity.this, R.color.light_gray_color));
        friends_tab_tv.setBackgroundColor(ContextCompat.getColor(GroupMemberActivity.this, R.color.white_color));

        member_list.clear();
        member_list.addAll(member_listAll);

        memberUserAdapter.notifyDataSetChanged();
        if (member_list.size() > 0) {
            no_data_tv.setVisibility(View.GONE);
            memberRecyclerView.setVisibility(View.VISIBLE);
        } else {
            no_data_tv.setVisibility(View.VISIBLE);
            memberRecyclerView.setVisibility(View.GONE);
        }


        if (isAdmin) {
            add_album.setVisibility(View.VISIBLE);
        } else {
            add_album.setVisibility(View.GONE);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.DASHBOARD_SCREEN);
        FastLapApplication.mCurrentContext = context;
        setToolbarHeading(getString(R.string.members));
    }

    public void groupMemberWebserviceResponse(String response) {
        member_listAll.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < dataJson.length(); i++) {
                    LikeUserBean likeUserBean = new LikeUserBean();
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);

                    likeUserBean.set_id(jsonObject1.getString(S._id));
                    likeUserBean.setLike_user_friend_status(jsonObject1.getString(S.alreadyFriend));
                    likeUserBean.setLike_user_id(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    likeUserBean.setLike_user_name(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    likeUserBean.setLike_user_image(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));

                    likeUserBean.setIsAdmin(jsonObject1.getString(S.admin));
                    likeUserBean.setIsSubAdmin(jsonObject1.getString(S.subAdmin));
                    likeUserBean.setGroupId(jsonObject1.getJSONObject(S.groupId).getString(S._id));

                    if (MySharedPreferences.getPreferences(context, S.user_id).compareToIgnoreCase(jsonObject1.getJSONObject(S.userId).getString(S._id)) == 0) {
                        if (jsonObject1.getString(S.admin).equalsIgnoreCase("1")) {
                            isAdmin = true;
                        }
                        if (jsonObject1.getString(S.subAdmin).equalsIgnoreCase("1")) {
                            isSubAdmin = true;
                        }
                    }

                    member_listAll.add(likeUserBean);
                }
                allMemberList();
            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                memberRecyclerView.setVisibility(View.GONE);
                //  Util.showAlertDialog(context, getString(R.string.alert), msg);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            no_data_tv.setVisibility(View.VISIBLE);
            memberRecyclerView.setVisibility(View.GONE);
        }
    }


    public void getRequestStatusResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                authAPI.getGroupMembersList(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        }
    }

    @Override
    public void requestForAddFriend(int postion) {

    }

    @Override
    public void requestForAddFriend(final int postion, View view) {
        if (!MySharedPreferences.getPreferences(context, S.user_id).equalsIgnoreCase(member_listAll.get(postion).getLike_user_id())) {
            final PopupMenu popup = new PopupMenu(context, view);
            //inflating menu from xml resource
            popup.inflate(R.menu.group_member);
            Menu menu = popup.getMenu();
            if (!isAdmin) {
                menu.removeItem(R.id.make_admin);
                if (!isSubAdmin) {
                    menu.removeItem(R.id.remove_member);
                } else {
                    if (member_listAll.get(postion).getIsAdmin().equalsIgnoreCase("1")) {
                        menu.removeItem(R.id.remove_member);
                    }
                }
            } else {
                if (member_listAll.get(postion).getIsSubAdmin().equalsIgnoreCase("1")) {
                    menu.removeItem(R.id.make_admin);
                }
            }

            //adding click listener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.view_profile:

                            Intent intent3 = new Intent(context, ProfileActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt(S.page, I.MENU_SCREEN);
                            bundle.putString(S.user_id, member_listAll.get(postion).getLike_user_id());
                            bundle.putString("status", member_listAll.get(postion).getLike_user_friend_status());
                            intent3.putExtras(bundle);
                            context.startActivity(intent3);

                            popup.dismiss();
                            break;
                        case R.id.make_admin:
                            authAPI.makeGroupAdmin(context, MySharedPreferences.getPreferences(context, S.user_id), member_listAll.get(postion).getGroupId(), member_listAll.get(postion).getLike_user_id(), "1");
                            popup.dismiss();
                            break;
                        case R.id.remove_member:
                            authAPI.removeFromGroup(context, MySharedPreferences.getPreferences(context, S.user_id), member_listAll.get(postion).getGroupId(), member_listAll.get(postion).getLike_user_id());
                            popup.dismiss();
                            break;
                        case R.id.cancel:
                            popup.dismiss();
                            break;
                    }
                    return false;
                }
            });
            //displaying the popup
            popup.show();
        }

    }
}
