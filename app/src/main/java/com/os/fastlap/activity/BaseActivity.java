package com.os.fastlap.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONObject;

/**
 * Created by abhinava on 7/11/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private TextView dashboardMenu;
    private TextView chatMenu;
    private TextView recordMenu;
    private TextView notificationMenu;
    private TextView notification_badge_tv;
    private TextView menuMenu;
    private TextView toolbarheading;
    Context mContext;
    int current_activity = I.DASHBOARD_SCREEN;
    AuthAPI authAPI;
    private String TAG = BaseActivity.class.getSimpleName();
    public static final int VEHICLE_ACTIVITY_RESULT_CODE = 0;
    public static final int TRACK_ACTIVITY_RESULT_CODE = 1;
    public static final int LAP_TIME_ACTIVITY_RESULT_CODE = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        authAPI = new AuthAPI(mContext);
        FastLapApplication.mCurrentContext = mContext;
    }

    public void chatActivity(View view) {
        if (current_activity != I.CHAT_SCREEN) {
            Intent intent = new Intent(this, ChatListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    public void DashboardActitivity(View view) {
        if (current_activity != I.DASHBOARD_SCREEN) {
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    public void recordActivity(View view) {
        if (current_activity != I.RECORD_SCREEN) {
            Intent intent = new Intent(this, RecordCameraActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
         // checkGps();
        }
    }

    public void notificationActivity(View view) {
        if (current_activity != I.NOTIFICATION_SCREEN) {
            Intent intent = new Intent(this, NotificationListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
           // overridePendingTransition(R.anim.slide_up, R.anim.stay);
            finish();
        }
    }

    public void menuActivity(View view) {
        if (current_activity != I.MENU_SCREEN) {
            Intent intent = new Intent(this, MenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    private void initview() {
        dashboardMenu = (TextView) findViewById(R.id.dashboard_menu);
        chatMenu = (TextView) findViewById(R.id.chat_menu);
        recordMenu = (TextView) findViewById(R.id.record_menu);
        notificationMenu = (TextView) findViewById(R.id.notification_menu);
        menuMenu = (TextView) findViewById(R.id.menu_menu);
        notification_badge_tv = (TextView) findViewById(R.id.notification_badge_tv);
    }

    public void setToolbarHeading(String heading) {
        toolbarheading = (TextView) findViewById(R.id.toolbarheading);
        toolbarheading.setText(heading);
    }

    public void setBottomMenu(int status) {
        initview();
        authAPI.getGeneralInformationData(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
        current_activity = status;

        setBadgeCount();

        switch (status) {
            case I.DASHBOARD_SCREEN:
                dashboardMenu.setTextColor(ContextCompat.getColor(this, R.color.white_color));
                dashboardMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.dashboardselect, 0, 0);

                chatMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                chatMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.chatunselect, 0, 0);

                recordMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                recordMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.recordunselect, 0, 0);

                notificationMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                notificationMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.notificationunselect, 0, 0);

                menuMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                menuMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.menuunselect, 0, 0);
                break;
            case I.CHAT_SCREEN:
                dashboardMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                dashboardMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.dashboardunselect, 0, 0);

                chatMenu.setTextColor(ContextCompat.getColor(this, R.color.white_color));
                chatMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.chatselect, 0, 0);

                recordMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                recordMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.recordunselect, 0, 0);

                notificationMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                notificationMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.notificationunselect, 0, 0);

                menuMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                menuMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.menuunselect, 0, 0);
                break;
            case I.RECORD_SCREEN:
                dashboardMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                dashboardMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.dashboardunselect, 0, 0);

                chatMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                chatMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.chatunselect, 0, 0);

                recordMenu.setTextColor(ContextCompat.getColor(this, R.color.white_color));
                recordMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.recordselect, 0, 0);

                notificationMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                notificationMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.notificationunselect, 0, 0);

                menuMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                menuMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.menuunselect, 0, 0);
                break;
            case I.NOTIFICATION_SCREEN:
                dashboardMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                dashboardMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.dashboardunselect, 0, 0);

                chatMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                chatMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.chatunselect, 0, 0);

                recordMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                recordMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.recordunselect, 0, 0);

                notificationMenu.setTextColor(ContextCompat.getColor(this, R.color.white_color));
                notificationMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.notificationselect, 0, 0);

                menuMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                menuMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.menuunselect, 0, 0);
                break;
            case I.MENU_SCREEN:
                dashboardMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                dashboardMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.dashboardunselect, 0, 0);

                chatMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                chatMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.chatunselect, 0, 0);

                recordMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                recordMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.recordunselect, 0, 0);

                notificationMenu.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                notificationMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.notificationunselect, 0, 0);

                menuMenu.setTextColor(ContextCompat.getColor(this, R.color.white_color));
                menuMenu.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.menuselect, 0, 0);
                break;
        }
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void hideKeyboard(View view) {
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void generalInformationDataResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                MySharedPreferences.setPreferences(mContext, jsonObject.getJSONObject(S.data).getString(S.unreadNotificationCount), S.unread_notification);
                setBadgeCount();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void setBadgeCount() {
        String noticount = MySharedPreferences.getPreferences(mContext, S.unread_notification);

        if (!TextUtils.isEmpty(noticount)) {
            int count = Integer.parseInt(noticount);
            if (count > 0) {
                notification_badge_tv.setVisibility(View.VISIBLE);
            } else {
                notification_badge_tv.setVisibility(View.GONE);
            }
            notification_badge_tv.setText(noticount + "");
        } else {
            notification_badge_tv.setVisibility(View.GONE);
        }
    }


    //This method leads you to the alert dialog box.
    void checkGps() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlertToUser();
        }
        else {
            Intent intent = new Intent(this, RecordCameraActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    //This method configures the Alert Dialog box.
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.enable_gps_service))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.enable_gps),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

}
