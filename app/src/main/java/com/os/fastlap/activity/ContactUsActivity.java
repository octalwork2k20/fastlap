package com.os.fastlap.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.os.fastlap.R;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class ContactUsActivity extends BaseActivity implements View.OnClickListener, SpinnerSelectorInterface {
    Context context;
    AuthAPI authAPI;
    private String TAG = ContactUsActivity.class.getSimpleName();
    private AppCompatImageView baseToggleIcon;
    private TextViewPlayRegular requestTrackFormTv;
    private TextViewPlayRegular otherRequestFormTv;
    private static final int PICK_FILE_REQUEST = 1;
    private Uri filePath;
    String picturePath = "";
    TextViewPlayRegular chooseTv;
    String tracktypeId = "";
    private ArrayList<BeanVehicleType> groupTypes_list;
    SpinnerSelectorInterface spinnerSelectorInterface;
    TextViewPlayRegular tracktypeTv;
    String fileUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        context = this;
        authAPI = new AuthAPI(context);
        spinnerSelectorInterface = this;
        groupTypes_list = new ArrayList<>();
        groupTypes_list.clear();

        initView();

        fileUrl = getIntent().getStringExtra(S.fileName);
        if (!TextUtils.isEmpty(fileUrl)) {
            filePath = Uri.fromFile(new File(fileUrl));
            RequestFormDialog();
            chooseTv.setText(fileUrl);
        }
    }

    private void initView() {
        baseToggleIcon = findViewById(R.id.base_toggle_icon);
        requestTrackFormTv = findViewById(R.id.request_track_form_tv);
        otherRequestFormTv = findViewById(R.id.other_request_form_tv);

        setonclick();
    }

    private void setonclick() {
        baseToggleIcon.setOnClickListener(this);
        requestTrackFormTv.setOnClickListener(this);
        otherRequestFormTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                Intent intent = new Intent(mContext, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.request_track_form_tv:
                RequestFormDialog();
                break;
            case R.id.other_request_form_tv:
                ContactusDialog();
                break;
        }

    }

    private void ContactusDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.contact_us_dialog);

        final EditText etTitle;
        final EditText etMessage;
        TextView submitBtn;

        etTitle = (EditText) dialog.findViewById(R.id.etTitle);
        etMessage = (EditText) dialog.findViewById(R.id.etMessage);
        submitBtn = (TextView) dialog.findViewById(R.id.submit_btn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = etTitle.getText().toString().trim();
                String message = etMessage.getText().toString().trim();

                if (TextUtils.isEmpty(title)) {
                    Toast.makeText(context, getString(R.string.contact_us_title_valid), Toast.LENGTH_LONG);
                    return;
                }
                if (TextUtils.isEmpty(message)) {
                    Toast.makeText(context, getString(R.string.contact_us_message_valid), Toast.LENGTH_LONG);
                    return;
                }
                dialog.dismiss();
                authAPI.sendContactUs(context, title, message, MySharedPreferences.getPreferences(context, S.user_id));
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    private void RequestFormDialog() {
        filePath=null;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.contact_us_request_dialog);

        final EditTextPlayRegular trackNameTv;
        final EditTextPlayRegular addressTv;
        final EditTextPlayRegular websiteTv;
        final EditTextPlayRegular descriptionEt;
        final TextViewPlayBold submitBtn;
        final TextViewPlayBold toolbarheading;
        final TextViewPlayRegular notesText;
        final EditTextPlayRegular emailTv;
        final EditTextPlayRegular contactTv;


        trackNameTv = dialog.findViewById(R.id.track_name_tv);
        toolbarheading = dialog.findViewById(R.id.toolbarheading);
        addressTv = dialog.findViewById(R.id.address_tv);
        websiteTv = dialog.findViewById(R.id.website_tv);
        chooseTv = dialog.findViewById(R.id.choose_tv);
        descriptionEt = dialog.findViewById(R.id.description_et);
        submitBtn = dialog.findViewById(R.id.submit_btn);

        notesText = dialog.findViewById(R.id.notes_text);
        emailTv = dialog.findViewById(R.id.email_tv);
        contactTv = dialog.findViewById(R.id.contact_tv);
        tracktypeTv = dialog.findViewById(R.id.tracktypeTv);

        toolbarheading.setText(getString(R.string.request_track_form));
        if (TextUtils.isEmpty(fileUrl)) {
            notesText.setVisibility(View.GONE);
            chooseTv.setEnabled(true);
        } else {
            notesText.setVisibility(View.VISIBLE);
            chooseTv.setEnabled(false);
        }

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String trackName = trackNameTv.getText().toString().trim();
                String address = addressTv.getText().toString().trim();
                String website = websiteTv.getText().toString().trim();
                String description = descriptionEt.getText().toString().trim();
                String email = emailTv.getText().toString().trim();
                String contact = contactTv.getText().toString().trim();
                String trackType = tracktypeTv.getText().toString().trim();

                if (TextUtils.isEmpty(trackName)) {
                    Util.showSnackBar(submitBtn, getString(R.string.please_enter_track_name));
                    return;
                }
                if (TextUtils.isEmpty(address)) {
                    Util.showSnackBar(submitBtn, getString(R.string.please_enter_track_location_address));
                    return;
                }
                if (TextUtils.isEmpty(website)) {
                    Util.showSnackBar(submitBtn, getString(R.string.please_enter_website_url));
                    return;
                }
                if (!Util.isValidEmail(email)) {
                    Util.showSnackBar(submitBtn, getString(R.string.email_valid));
                    return;
                }
                if (TextUtils.isEmpty(contact)) {
                    Util.showSnackBar(submitBtn, context.getString(R.string.mobile_empty_valid));
                    return;
                }
                if (TextUtils.isEmpty(trackType)) {
                    Util.showSnackBar(submitBtn, context.getString(R.string.track_type_empty_error));
                    return;
                }
                if (filePath == null || TextUtils.isEmpty(filePath.toString())) {
                    Util.showSnackBar(submitBtn, getString(R.string.choose_a_file));
                    return;
                }
                if (TextUtils.isEmpty(description)) {
                    Util.showSnackBar(submitBtn, context.getString(R.string.description_empty_error));
                    return;
                }
                dialog.dismiss();
                authAPI.sendRequestForm(context, trackName, address, website, filePath, description, MySharedPreferences.getPreferences(context, S.user_id), email, contact, tracktypeId);
            }
        });

        chooseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showFileChooser();
                showFileChooserDialog();
            }
        });

        tracktypeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                groupTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    groupTypes_list.add(beanVehicleType);
                }
                showVehicleTypeDialog(getString(R.string.select_group_type), groupTypes_list, I.GROUPTYPE);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {

    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {
        switch (type) {
            case I.GROUPTYPE:
                tracktypeTv.setText(value);
                tracktypeId = id;

                break;
        }
    }

    public void getContactUsResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(context, getString(R.string.alert), msg);
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {

            Log.e(TAG, e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.contactus));
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a File"), PICK_FILE_REQUEST);
    }

    private void showFileChooserDialog() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(I.PICK_FILE_REQUEST)
                .withFilter(Pattern.compile(".*")) // Filtering files and directories by file name using regexp
                .withFilterDirectories(true) // Set directories filterable (false by default)
                .withHiddenFiles(false) // Show hidden files and folders
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case I.PICK_FILE_REQUEST:
                    picturePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);

                    File file = new File(picturePath);
                    long file_length = file.length();
                    long file_maxSize = 5 * 1024 * 1024;
                    if (file_length <= file_maxSize) {
                        chooseTv.setText(picturePath);
                        filePath = Uri.fromFile(new File(picturePath));
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.file_not_allowed_should_be_5_MB), Toast.LENGTH_LONG).show();
                        picturePath = "";
                        chooseTv.setText(picturePath);
                    }
                    break;
           /* case PICK_FILE_REQUEST:
                if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                    filePath = data.getData();
                    try {
                        try {
                            String[] filePathColumn = {MediaStore.MediaColumns.DATA};
                            Cursor cursor = getContentResolver().query(filePath,
                                    filePathColumn, null, null, null);
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            picturePath = cursor.getString(columnIndex);
                        } catch (Exception e) {
                            e.printStackTrace();
                            picturePath = filePath.getPath();
                            File file = new File(picturePath);
                            if (!file.getName().contains(".")) {
                                picturePath = "";
                                filePath = null;
                            }
                            //Toast.makeText(context,"Invalid File",Toast.LENGTH_SHORT).show();
                        }

                        if (picturePath.compareTo("") != 0) {
                            String type = picturePath.substring(picturePath.lastIndexOf(".") + 1);
                            boolean f = Check(type);
                            if (f) {


                                File file = new File(picturePath);
                                long file_length = file.length();
                                long file_maxSize = 5 * 1024 * 1024;
                                if (file_length <= file_maxSize) {
                                    chooseTv.setText(picturePath);
                                } else {
                                    Toast.makeText(context, "File should not be allowed more than 5 MB", Toast.LENGTH_LONG).show();
                                    picturePath = "";
                                    chooseTv.setText(picturePath);
                                }
                            } else {
                                Toast.makeText(context, "You are not allowed to upload file in " + type + " format", Toast.LENGTH_LONG).show();
                                picturePath = "";
                                chooseTv.setText(picturePath);
                            }


                        } else {
                            Toast.makeText(context, "Invalid File", Toast.LENGTH_SHORT).show();
                            picturePath = "";
                            chooseTv.setText(picturePath);
                        }

                        //cursor.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Invalid File", Toast.LENGTH_SHORT).show();
                        filePath = null;
                    }
                }
                break;*/
            }
        }

    }

    public boolean check(String type) {
        boolean flag;

        flag = type.compareToIgnoreCase("jpg") == 0 || type.compareToIgnoreCase("jpeg") == 0
                || type.compareToIgnoreCase("gif") == 0 || type.compareToIgnoreCase("png") == 0
                || type.compareToIgnoreCase("JPG") == 0 || type.compareToIgnoreCase("JPEG") == 0
                || type.compareToIgnoreCase("GIF") == 0 || type.compareToIgnoreCase("PNG") == 0
                || type.compareToIgnoreCase("mp3") == 0 || type.compareToIgnoreCase("aaf") == 0
                || type.compareToIgnoreCase("wav") == 0 || type.compareToIgnoreCase("mpeg") == 0
                || type.compareToIgnoreCase("docx") == 0 || type.compareToIgnoreCase("doc") == 0
                || type.compareToIgnoreCase("xls") == 0 || type.compareToIgnoreCase("pdf") == 0
                || type.compareToIgnoreCase("mp4") == 0;

        return flag;
    }

    public void newTrackRequestResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(context, getString(R.string.alert), msg);
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {

            Log.e(TAG, e.toString());
        }
    }
}
