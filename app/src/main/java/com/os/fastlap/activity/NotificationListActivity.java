package com.os.fastlap.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.adapter.NotificationAdapter;
import com.os.fastlap.beans.NotificationBeans;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhinava on 7/13/2017
 */

public class NotificationListActivity extends BaseActivity implements NotificationAdapter.NotificationInterface {
    Context mContext;
    private RecyclerView notificationRecyclerView;
    ArrayList<NotificationBeans> notification_list;
    NotificationAdapter notificationAdapter;
    private CircleImageView toolbarUserPic;
    private TextViewPlayRegular toolbarTxtUserName;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public final static String TAG = NotificationListActivity.class.getSimpleName();
    private AuthAPI mAuthAPI;
    TextView unreadBtn;
    String status = "0";
    TextView no_data_tv;
    private ImageView mylap_icon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_listing);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        mAuthAPI = new AuthAPI(mContext);
        initView();
        callNotificationListing(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.NOTIFICATION_SCREEN);
        FastLapApplication.mCurrentContext = mContext;
        setToolbarHeading(getString(R.string.notification));

        toolbarTxtUserName.setText(MySharedPreferences.getPreferences(mContext, S.username));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(mContext, S.image), toolbarUserPic, Util.getImageLoaderOption(mContext));
        callNotificationListing(true);

    }

    public void callNotificationListing(boolean prograssStatus) {
        mAuthAPI.userNotificationList(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), prograssStatus);
    }

    private void initView() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        notificationRecyclerView = (RecyclerView) findViewById(R.id.notification_recyclerView);
        toolbarUserPic = (CircleImageView) findViewById(R.id.toolbar_user_pic);
        toolbarTxtUserName = (TextViewPlayRegular) findViewById(R.id.toolbar_txt_user_name);
        unreadBtn = (TextView) findViewById(R.id.unreadBtn);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);

        mylap_icon = (ImageView) findViewById(R.id.mylap_icon);
        notification_list = new ArrayList<>();

        notificationAdapter = new NotificationAdapter(notification_list, mContext);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        notificationRecyclerView.setLayoutManager(layoutManager);
        notificationRecyclerView.setItemAnimator(new DefaultItemAnimator());
        notificationRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
        notificationRecyclerView.setAdapter(notificationAdapter);
        notificationAdapter.OnClickListner(this);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                callNotificationListing(false);
            }
        });


        unreadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuthAPI.setNotificationSettingsChanged(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
            }
        });

        mylap_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.startNewActivity(NotificationListActivity.this, MyLapsActivity.class, false);
            }
        });
    }

    /* here receive userNotificationList webservice response
    *  parse the response and nofity adapter
    */
    public void userNotificationList(String response) {
        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            notification_list.clear();
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                int unreadCount = 0;
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        NotificationBeans notificationBeans = new NotificationBeans();
                        notificationBeans.set_id(jsonObject1.getString(S._id));
                        notificationBeans.setSenderId(jsonObject1.getJSONObject(S.senderId).getString(S._id));
                        notificationBeans.setSenderEmail(jsonObject1.getJSONObject(S.senderId).getString(S.email));
                        notificationBeans.setSenderImage(jsonObject1.getJSONObject(S.senderId).getJSONObject(S.personalInfo).getString(S.image));
                        notificationBeans.setSenderName(jsonObject1.getJSONObject(S.senderId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.senderId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                        notificationBeans.setRecieverId(jsonObject1.getString(S.recieverId));
                        notificationBeans.setModified(jsonObject1.getString(S.modified));
                        notificationBeans.setCreated(jsonObject1.getString(S.created));
                        notificationBeans.setStatus(jsonObject1.getInt(S.status));
                        notificationBeans.setNickName(jsonObject1.getJSONObject(S.senderId).has(S.username)?jsonObject1.getJSONObject(S.senderId).getString(S.username):"");
                        notificationBeans.setMessage(jsonObject1.getString(S.message));
                        notificationBeans.setType(jsonObject1.getString(S.type));
                        if (jsonObject1.has(S.customData))
                            notificationBeans.setCustomData(jsonObject1.getString(S.customData));
                        notification_list.add(notificationBeans);
                        if (jsonObject1.getInt(S.status) == 0) {
                            unreadCount++;
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                        }
                }

                if (notification_list.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    notificationRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    notificationRecyclerView.setVisibility(View.GONE);
                }

                MySharedPreferences.setPreferences(mContext, unreadCount + "", S.unread_notification);
                setBottomMenu(I.NOTIFICATION_SCREEN);
                notificationAdapter.notifyDataSetChanged();
            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                notificationRecyclerView.setVisibility(View.GONE);
            }
            //Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            no_data_tv.setVisibility(View.VISIBLE);
            notificationRecyclerView.setVisibility(View.GONE);
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void ItemClickListner(int position) {
        mAuthAPI.setNotificationStatusChanged(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), notification_list.get(position).get_id());

    }

    public void removeNotificationApi(String id) {
        mAuthAPI.removeNotification(mContext, id, MySharedPreferences.getPreferences(mContext, S.user_id));
    }

    public void updateNotificationWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                setBottomMenu(I.NOTIFICATION_SCREEN);
                callNotificationListing(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeNotification(String responce) {
        try {
            JSONObject jsonObject = new JSONObject(responce);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, S.removeNotification_api);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
