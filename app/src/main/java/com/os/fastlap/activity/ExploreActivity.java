package com.os.fastlap.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.adapter.community.CommunityVechiceListAdapter;
import com.os.fastlap.beans.BeanExplore;
import com.os.fastlap.beans.BeanVehicle;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.ActivityCommunicator;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.fragment.explore.ExploreAllFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by abhinava on 7/19/2017.
 */

public class ExploreActivity extends BaseActivity implements OnDateSelectedListener, SpinnerSelectorInterface, OnMonthChangedListener, View.OnClickListener, ActivityCommunicator {
    Context context;
    private RecyclerView carsRecyclerView;
    private android.support.design.widget.TabLayout TabLayout;
    private ImageView calenderIv;
    private ImageView filterIv;
    private ViewPager viewpager;
    private com.os.fastlap.adapter.community.CommunityVechiceListAdapter communityVechiceListAdapter;
    private ArrayList<BeanVehicle> mArrayVechineList;
    MaterialCalendarView calendarView;
    SpinnerSelectorInterface spinnerSelectorInterface;
    private FragmentManager mFragmentManager;
    private ViewPagerAdapter adapter;
    public static int pos = 0;
    ImageView base_toggle_icon;
    AuthAPI authAPI;
    private String TAG = ExploreActivity.class.getSimpleName();
    public static ArrayList<BeanExplore> exploreTrackList;
    public static ArrayList<BeanExplore> exploreTrackListAll;
    Dialog cal_dialog;
    private ArrayList<BeanVehicleType> countryName;
    EditText searchEt;
    TextView menu_menu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.explore_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        spinnerSelectorInterface = this;
        initView();
        addEventListeners();
        getData("");
        authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));

    }

    private void initView() {
        mFragmentManager = getSupportFragmentManager();
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        carsRecyclerView = (RecyclerView) findViewById(R.id.cars_recyclerView);
        TabLayout = (TabLayout) findViewById(R.id.TabLayout);
        calenderIv = (ImageView) findViewById(R.id.calender_iv);
        filterIv = (ImageView) findViewById(R.id.filter_iv);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        searchEt = findViewById(R.id.searchEt);

        mArrayVechineList = new ArrayList<>();
        mArrayVechineList.clear();
        exploreTrackList = new ArrayList<>();
        exploreTrackList.clear();
        exploreTrackListAll = new ArrayList<>();
        exploreTrackListAll.clear();
        countryName = new ArrayList<>();
        countryName.clear();

        communityVechiceListAdapter = new CommunityVechiceListAdapter(context, mArrayVechineList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        carsRecyclerView.setLayoutManager(layoutManager);
        carsRecyclerView.setAdapter(communityVechiceListAdapter);

        clickListner();

        setUpViewPager();
        TabLayout.setupWithViewPager(viewpager);
        Util.changeTabsFont(TabLayout, context);

        searchEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(S.type, I.EXPLORESEARCH);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

    }

    private void setUpViewPager() {
        adapter = new ViewPagerAdapter(mFragmentManager);
        adapter.addFragment(new ExploreAllFragment(I.ALL), getString(R.string.all));
        adapter.addFragment(new ExploreAllFragment(I.COUNTRY), getString(R.string.country));
        adapter.getItem(viewpager.getCurrentItem());
        viewpager.setAdapter(adapter);
    }

    private void clickListner() {
        calenderIv.setOnClickListener(this);
        base_toggle_icon.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }

    private void addEventListeners() {

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                hideKeyboard();
                pos = position;
                if (position == 1) {
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_country), countryName, I.COUNTRYNAME);
                } else {
                    filter("");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        TabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // hideKeyboard();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // hideKeyboard();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                hideKeyboard();
                pos = tab.getPosition();
                if (pos == 1) {
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_country), countryName, I.COUNTRYNAME);
                } else {
                    filter("");
                }
            }
        });


    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.COUNTRYNAME:
                filter(value);
                break;
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        FastLapApplication.mCurrentContext = context;
        // setToolbarHeading(getString(R.string.explore));
    }

    private void getCalanderDates() {
        cal_dialog = new Dialog(context);
        cal_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        cal_dialog.setContentView(R.layout.explore_calenderview_layout);

        calendarView = (MaterialCalendarView) cal_dialog.findViewById(R.id.calendarView);
        ImageView done_iv = (ImageView) cal_dialog.findViewById(R.id.done_iv);
        ImageView cross_iv = (ImageView) cal_dialog.findViewById(R.id.cross_iv);

        Calendar instance1 = Calendar.getInstance();
        instance1.set(instance1.get(Calendar.YEAR), instance1.get(Calendar.MONTH), instance1.get(Calendar.DATE));

        Calendar instance2 = Calendar.getInstance();
        instance2.set(instance2.get(Calendar.YEAR), instance1.get(Calendar.MONTH), instance1.get(Calendar.DATE) + 29);
        calendarView.state().edit()
                .setMinimumDate(instance1.getTime())
                .commit();

        calendarView.state().edit()
                .setMaximumDate(instance2.getTime())
                .commit();

        calendarView.setOnDateChangedListener(this);
        calendarView.setOnMonthChangedListener(this);
        calendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        calendarView.setDynamicHeightEnabled(true);
        calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);

        cal_dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        cal_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = cal_dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        cal_dialog.show();

        done_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cal_dialog.cancel();
                CalendarDay date = calendarView.getSelectedDate();
                if(date!=null){
                    String dates = date.getYear() + "-" + pad((date.getMonth() + 1)) + "-" + pad(date.getDay());
                    authAPI.getExploreByDate(context, MySharedPreferences.getPreferences(context, S.user_id), dates,"");
                }
                else {
                    Util.showAlertDialog(mContext,getResources().getString(R.string.select_date),getResources().getString(R.string.please_select_date));
                    //Toast.makeText(context, getResources().getString(R.string.please_select_date), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cross_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal_dialog.cancel();
            }
        });
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {


    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.calender_iv:
                getCalanderDates();
                break;

            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.menu_menu:
                Intent intent = new Intent(this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void passDataToActivity(String someValue) {

    }

    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                mArrayVechineList.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicle beanVehicle = new BeanVehicle();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicle.setId(jsonObject1.getString(S._id));
                    beanVehicle.setName(jsonObject1.getString(S.name));
                    beanVehicle.setImage(jsonObject1.has(S.image)?jsonObject1.getString(S.image):"");
                    mArrayVechineList.add(beanVehicle);
                }
                communityVechiceListAdapter.notifyDataSetChanged();

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void getExploreDataWebserviceResponse(String response) {
        exploreTrackList.clear();
        exploreTrackListAll.clear();
        countryName.clear();
        ArrayList<String> countryList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            Log.d("tag","getExploreDataWebserviceResponse:"+jsonObject);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray mainArray = jsonObject.getJSONArray(S.data);
                for(int i=0;i<mainArray.length();i++)
                {
                    JSONObject indexObject=mainArray.getJSONObject(i);
                    JSONObject contactDetailsObj=indexObject.getJSONObject(S.contactDetails);
                    BeanExplore beanExplore = new BeanExplore();
                    beanExplore.setId(indexObject.getString(S._id));
                    beanExplore.setTrackURL(contactDetailsObj.getString(S.url));
                    beanExplore.setTrackMobile(contactDetailsObj.getString(S.mobileNumber_response));
                    beanExplore.setTrackEmail(contactDetailsObj.getString(S.email));

                    beanExplore.setTrackId(indexObject.getString(S._id));
                    beanExplore.setTrackName(indexObject.getString(S.name));
                    beanExplore.setTrackStatus(indexObject.getString(S.status));

                    JSONArray positionArray = indexObject.getJSONArray(S.position);
                    if (positionArray.length() > 0) {
                        beanExplore.setTrackLatPos(positionArray.getString(1));
                        beanExplore.setTrackLongPos(positionArray.getString(0));
                    }
                    beanExplore.setTrackLocation(indexObject.getString(S.location_response));
                    beanExplore.setAboutTrack(indexObject.getString(S.aboutTrack));
                    beanExplore.setTrackLength(indexObject.getString(S.trackLength));
                    beanExplore.setTrackType(indexObject.getString(S.type));
                    beanExplore.setTrackOpenedDate(indexObject.getString(S.trackOpenedDate));
                    beanExplore.setTrackCoverImage(indexObject.getString(S.coverImage));
                    beanExplore.setTrackImage(indexObject.has(S.image)?indexObject.getString(S.image):"");

                    beanExplore.setTotalLapRecords(indexObject.has(S.totalLapRecords)?indexObject.getString(S.totalLapRecords):"");
                    beanExplore.setTrackCountry(indexObject.getString(S.country));
                    if (!countryList.contains(indexObject.getString(S.country))) {
                        countryList.add(indexObject.getString(S.country));
                        BeanVehicleType beanVehicleType = new BeanVehicleType();
                        beanVehicleType.setId(indexObject.getString(S.country));
                        beanVehicleType.setName(indexObject.getString(S.country));
                        countryName.add(beanVehicleType);
                    }
                    //beanExplore.setTotalTurns(indexObject.getJSONObject(S.turns).getString(S.totalTurns));
                    exploreTrackList.add(beanExplore);
                    exploreTrackListAll.add(beanExplore);

                }
                   getCurrentFragment();

            } else {
                getCurrentFragment();
                /*Util.showAlertDialog(context, getString(R.string.alert), msg);*/
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public Fragment getCurrentFragment() {
        Fragment mCurrentFragment = null;
        if (adapter != null && viewpager != null) {
            mCurrentFragment = adapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((ExploreAllFragment) mCurrentFragment).getExploreData();
        }
        return mCurrentFragment;

    }

    public void getWeatherDataWebserviceResponse(String response) {
        getCurrentWeatherFragment(response);
    }

    public Fragment getCurrentWeatherFragment(String response) {
        Fragment mCurrentFragment = null;
        if (adapter != null && viewpager != null) {
            mCurrentFragment = adapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((ExploreAllFragment) mCurrentFragment).getWeatherData(response);
        }

        return mCurrentFragment;

    }

    public void getVehicleTypeClick(int pos, String select_id) {
        String id = mArrayVechineList.get(pos).getId();
        getData(id);
    }

    // search class
    public void filter(String ch) {
        exploreTrackList.clear();
        if (ch.isEmpty())
            exploreTrackList.addAll(exploreTrackListAll);
        else {
            for (BeanExplore wp : exploreTrackListAll) {
                if (wp.getTrackCountry().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())) ||
                        wp.getTrackCountry().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())))
                    exploreTrackList.add(wp);
            }
        }
        getCurrentFragment();
    }
    public void getData(String groupTypeId) {
        authAPI.getExploreData(context, MySharedPreferences.getPreferences(context, S.user_id), groupTypeId);
    }
}