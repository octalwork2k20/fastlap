package com.os.fastlap.activity.group;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.DashboardActivity;
import com.os.fastlap.adapter.dashboard.DashBoardPostsAdapter;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.UserDataBeans;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardFeelingIdBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostImagesBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostListBean;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.beans.dashboardmodals.FriendIdBean;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.TrackData;
import com.os.fastlap.beans.gragemodals.VehicleBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.os.fastlap.util.constants.S.status;


/**
 * Created by anandj on 7/17/2017.
 */

public class GroupDiaryActivity extends BaseActivity implements View.OnClickListener, DashBoardPostsAdapter.OnItemClickListener {

    private Context mContext;
    private AppCompatImageView baseToggleIcon;
    private ImageView imgBg;
    private TextViewPlayRegular txtUsername;
    private ImageView imgAddfrd;
    private CircleImageView userIcon;
    private RecyclerView postListview;
    private LinearLayout footer;
    AuthAPI authAPI;
    private String TAG = GroupDiaryActivity.class.getSimpleName();
    private ArrayList<DashboardPostListBean> mPostListBeen = new ArrayList<>();
    private DashBoardPostsAdapter dashBoardPostsAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private SharingDeleget sharingDeleget;
    private int currentPage = 1;
    private ProgressBar mProgressBar;
    private boolean loading = true;
    private AuthAPI mAuthAPI;
    TextView no_data_tv;
    CallbackManager callbackManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.track_diary_layout);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();
        callRefreshDashboard();
    }

    private void initView() {
        mAuthAPI = new AuthAPI(mContext);
        mProgressBar = new ProgressBar(mContext);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        imgBg = (ImageView) findViewById(R.id.img_bg);
        txtUsername = (TextViewPlayRegular) findViewById(R.id.txt_username);
        imgAddfrd = (ImageView) findViewById(R.id.img_addfrd);
        userIcon = (CircleImageView) findViewById(R.id.user_icon);
        postListview = (RecyclerView) findViewById(R.id.post_listview);
        footer = (LinearLayout) findViewById(R.id.footer);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);

        dashBoardPostsAdapter = new DashBoardPostsAdapter(mPostListBeen, DashboardActivity.advertisementArrayList, mContext, sharingDeleget, this, this);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        postListview.setLayoutManager(mLinearLayoutManager);
        postListview.setItemAnimator(new DefaultItemAnimator());
        postListview.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
        postListview.setAdapter(dashBoardPostsAdapter);

        postListview.setNestedScrollingEnabled(false);

        clickListner();
        setProfileData();


        setProfileData();
    }

    private void callRefreshDashboard() {
        mAuthAPI.getGroupDiaryData(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), GroupProfileActivity.profileGroupId, "1");
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
    }

    private void setProfileData() {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + GroupProfileActivity.profileGroupCoverImage, imgBg, Util.getImageLoaderOption(mContext));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + GroupProfileActivity.profileGroupProfileImage, userIcon, Util.getImageLoaderOption(mContext));
        txtUsername.setText(GroupProfileActivity.profileGroupName);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        FastLapApplication.mCurrentContext = mContext;
        setToolbarHeading(getString(R.string.group));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();

                break;
        }
    }

    /*  postListing webservice response
    *  here we show user post
    */
    public void postListingWebserviceResponse(String response) {

        mPostListBeen.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    DashboardPostListBean dashboardPostListBean = new DashboardPostListBean();
                    dashboardPostListBean.set_id(jsonObject1.getString(S._id));

                    JSONObject userJson = jsonObject1.getJSONObject(S.userId);
                    UserDataBeans userDataBeans = new UserDataBeans();
                    userDataBeans.set_id(userJson.getString(S._id));
                    userDataBeans.setEmail(userJson.getString(S.email));
                    userDataBeans.setUsername(userJson.getString(S.username));

                    JSONObject personalInfoJson = userJson.getJSONObject(S.personalInfo);
                    PersonalInfo personalInfo = new PersonalInfo();
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    userDataBeans.setPersonalInfo(personalInfo);

                    dashboardPostListBean.setUserDataBeans(userDataBeans);
                    dashboardPostListBean.setStatus(jsonObject1.getInt(S.status));
                    dashboardPostListBean.setDescription(jsonObject1.getString(S.description));
                    dashboardPostListBean.setPosition_text(jsonObject1.getString(S.position_text));
                    dashboardPostListBean.setSpeed(jsonObject1.getString(S.speed));
                    dashboardPostListBean.setWeather(jsonObject1.getString(S.weather));

                    JSONArray feelingJsonArray = jsonObject1.getJSONArray(S.feelingId);
                    for (int j = 0; j < feelingJsonArray.length(); j++) {
                        JSONObject feelingJson = feelingJsonArray.getJSONObject(j);
                        DashboardFeelingIdBeans dashboardFeelingIdBeans = new DashboardFeelingIdBeans();
                        dashboardFeelingIdBeans.set_id(feelingJson.getString(S._id));
                        dashboardFeelingIdBeans.setStatus(feelingJson.getInt(S.status));
                        dashboardFeelingIdBeans.setUnicodes(feelingJson.getString(S.unicodes));
                        dashboardFeelingIdBeans.setName(feelingJson.getString(S.name));


                        dashboardPostListBean.setDashboardFeelingIdBeans(dashboardFeelingIdBeans);
                    }

                    JSONArray trackIdJsonArray = jsonObject1.getJSONArray(S.trackId);
                    for (int j = 0; j < trackIdJsonArray.length(); j++) {
                        JSONObject trackIdJson = trackIdJsonArray.getJSONObject(j);
                        TrackData trackData = new TrackData();
                        trackData.setId(trackIdJson.getString(S._id));
                        trackData.setOwnerId(trackIdJson.getString(S.ownerId));
                        trackData.setName(trackIdJson.getString(S.name));
                        trackData.setImage(trackIdJson.getString(S.image));
                        trackData.setTrackLength(trackIdJson.getInt(S.trackLength));
                        trackData.setCountry(trackIdJson.has(S.country)?trackIdJson.getString(S.country):"");

                        dashboardPostListBean.setTrackData(trackData);
                    }

                    JSONArray userVehicleIdJsonArray = jsonObject1.getJSONArray(S.userVehicleId);
                    for (int j = 0; j < userVehicleIdJsonArray.length(); j++) {
                        JSONObject vehicleJson = userVehicleIdJsonArray.getJSONObject(j);
                        BeanVehicleList beanVehicleList = new BeanVehicleList();
                        beanVehicleList.set_id(vehicleJson.getString(S._id));

                        VehicleTypeIdModal vehicleTypeIdModal = new VehicleTypeIdModal();
                        JSONObject vehicleTypeJson = vehicleJson.getJSONObject(S.vehicleTypeId);
                        vehicleTypeIdModal.set_id(vehicleTypeJson.getString(S._id));
                        vehicleTypeIdModal.setName(vehicleTypeJson.getString(S.name));

                        beanVehicleList.setVehicleTypeIdModal(vehicleTypeIdModal);

                        VehicleBrandIdModal vehicleBrandIdModal = new VehicleBrandIdModal();
                        JSONObject vehicleBrandJson = vehicleJson.getJSONObject(S.vehicleBrandId);
                        vehicleBrandIdModal.set_id(vehicleBrandJson.getString(S._id));
                        vehicleBrandIdModal.setVehicleBrandName(vehicleBrandJson.getString(S.vehicleBrandName));
                        vehicleBrandIdModal.setVehicleTypeId(vehicleBrandJson.getString(S.vehicleTypeId));

                        beanVehicleList.setVehicleBrandIdModal(vehicleBrandIdModal);

                        VehicleModelIdModal vehicleModelIdModal = new VehicleModelIdModal();
                        JSONObject vehicleModalJson = vehicleJson.getJSONObject(S.vehicleModelId);
                        vehicleModelIdModal.set_id(vehicleModalJson.getString(S._id));
                        vehicleModelIdModal.setVehicleBrandId(vehicleModalJson.getString(S.vehicleBrandId));
                        vehicleModelIdModal.setVehicleTypeId(vehicleModalJson.getString(S.vehicleTypeId));
                        vehicleModelIdModal.setVehicleModelName(vehicleModalJson.getString(S.vehicleModelName));

                        beanVehicleList.setVehicleModelIdModal(vehicleModelIdModal);
                        beanVehicleList.setDescription(vehicleJson.getString(S.description));

                        dashboardPostListBean.setBeanVehicleList(beanVehicleList);
                    }
                    JSONArray postImagesJsonArray = jsonObject1.getJSONArray(S.postImages);
                    List<DashboardPostImagesBeans> dashboardPostImagesBeanses = new ArrayList<>();
                    for (int j = 0; j < postImagesJsonArray.length(); j++) {
                        JSONObject postImageJson = postImagesJsonArray.getJSONObject(j);
                        DashboardPostImagesBeans dashboardPostImagesBeans = new DashboardPostImagesBeans();
                        dashboardPostImagesBeans.set_id(postImageJson.getString(S._id));
                        dashboardPostImagesBeans.setUserId(postImageJson.getString(S.userId));
                        dashboardPostImagesBeans.setPostId(postImageJson.getString(S.postId));
                        dashboardPostImagesBeans.setStatus(postImageJson.getInt(S.status));
                        dashboardPostImagesBeans.setType(postImageJson.getString(S.type));
                        dashboardPostImagesBeans.setFileName(postImageJson.getString(S.fileName));
                        dashboardPostImagesBeans.setThumbName(postImageJson.getString(S.thumbName));
                        dashboardPostImagesBeanses.add(dashboardPostImagesBeans);
                    }

                    JSONArray postShareUser = jsonObject1.getJSONArray(S.postShareUser);
                    List<DashboardPostShareUserModal> dashboardPostShareUserModals = new ArrayList<>();
                    for (int j = 0; j < postShareUser.length(); j++) {
                        JSONObject postShareUserJson = postShareUser.getJSONObject(j);
                        DashboardPostShareUserModal dashboardPostShareUserModal = new DashboardPostShareUserModal();
                        dashboardPostShareUserModal.setId(postShareUserJson.getString(S._id));
                        dashboardPostShareUserModal.setPostId(postShareUserJson.getString(S.postId));
                        dashboardPostShareUserModal.setModified(postShareUserJson.getString(S.modified));
                        dashboardPostShareUserModal.setCreated(postShareUserJson.getString(S.created));
                        JSONObject userIdJson = postShareUserJson.getJSONObject(S.userId);

                        UserDataBeans userDataBeans1 = new UserDataBeans();
                        userDataBeans1.set_id(userIdJson.getString(S._id));
                        userDataBeans1.setEmail(userIdJson.getString(S.email));
                        userDataBeans1.setUsername(userIdJson.getString(S.username));
                        JSONObject personalInfoJsonShare = userIdJson.getJSONObject(S.personalInfo);
                        PersonalInfo personalInfo1 = new PersonalInfo();
                        personalInfo1.setLanguage(personalInfoJsonShare.getString(S.language_response));
                        personalInfo1.setCoverImage(personalInfoJsonShare.getString(S.coverImage));
                        personalInfo1.setImage(personalInfoJsonShare.getString(S.image));
                        personalInfo1.setMobileNumber(personalInfoJsonShare.getString(S.mobileNumber_response));
                        personalInfo1.setDateOfBirth(personalInfoJsonShare.getString(S.dateOfBirth_response));
                        personalInfo1.setVoucherCode(personalInfoJsonShare.getString(S.voucherCode_response));
                        personalInfo1.setLastName(personalInfoJsonShare.getString(S.lastName_response));
                        personalInfo1.setFirstName(personalInfoJsonShare.getString(S.firstName_response));

                        userDataBeans1.setPersonalInfo(personalInfo1);
                        dashboardPostShareUserModal.setUserDataBeans(userDataBeans1);
                        dashboardPostShareUserModals.add(dashboardPostShareUserModal);
                    }

                    JSONArray commentPostTagFriend = jsonObject1.getJSONArray(S.commentPostTagFriend);
                    List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeanses = new ArrayList<>();
                    for (int j = 0; j < commentPostTagFriend.length(); j++) {
                        JSONObject friendJson = commentPostTagFriend.getJSONObject(j);
                        DashboardCommentPostTagFriendsBeans dashboardCommentPostTagFriendsBeans = new DashboardCommentPostTagFriendsBeans();
                        dashboardCommentPostTagFriendsBeans.set_id(friendJson.getString(S._id));
                        dashboardCommentPostTagFriendsBeans.setUserId(friendJson.getString(S.userId));
                        dashboardCommentPostTagFriendsBeans.setPostId(friendJson.getString(S.postId));

                        FriendIdBean friendIdBean = new FriendIdBean();
                        JSONObject frinedIdJson = friendJson.getJSONObject(S.friendId);
                        friendIdBean.set_id(frinedIdJson.getString(S._id));
                        friendIdBean.setUsername(frinedIdJson.getString(S.username));
                        friendIdBean.setEmail(frinedIdJson.getString(S.email));

                        PersonalInfo personalInfo1 = new PersonalInfo();
                        JSONObject personalInfoJsonInner = frinedIdJson.getJSONObject(S.personalInfo);
                        personalInfo1.setLanguage(personalInfoJsonInner.getString(S.language_response));
                        personalInfo1.setCoverImage(personalInfoJsonInner.getString(S.coverImage));
                        personalInfo1.setImage(personalInfoJsonInner.getString(S.image));
                        personalInfo1.setMobileNumber(personalInfoJsonInner.getString(S.mobileNumber_response));
                        personalInfo1.setDateOfBirth(personalInfoJsonInner.getString(S.dateOfBirth_response));
                        personalInfo1.setVoucherCode(personalInfoJsonInner.getString(S.voucherCode_response));
                        personalInfo1.setLastName(personalInfoJsonInner.getString(S.lastName_response));
                        personalInfo1.setFirstName(personalInfoJsonInner.getString(S.firstName_response));

                        friendIdBean.setPersonalInfo(personalInfo1);
                        dashboardCommentPostTagFriendsBeans.setFriendIdBean(friendIdBean);

                        dashboardCommentPostTagFriendsBeanses.add(dashboardCommentPostTagFriendsBeans);

                    }

                    dashboardPostListBean.setDashboardCommentPostTagFriendsBeanses(dashboardCommentPostTagFriendsBeanses);
                    dashboardPostListBean.setDashboardPostImagesBeans(dashboardPostImagesBeanses);
                    dashboardPostListBean.setDashboardPostShareUserModals(dashboardPostShareUserModals);
                    dashboardPostListBean.setLikedPost(jsonObject1.getString(S.likedPost));
                    dashboardPostListBean.setLikedPostCount(jsonObject1.getString(S.likedPostCount));
                    dashboardPostListBean.setCommentPostCount(jsonObject1.getString(S.commentPostCount));
                    dashboardPostListBean.setModified(jsonObject1.getString(S.modified));
                    dashboardPostListBean.setCreated(jsonObject1.getString(S.created));

                    mPostListBeen.add(dashboardPostListBean);
                }
                dashBoardPostsAdapter.notifyDataSetChanged();
                if (mPostListBeen.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    postListview.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    postListview.setVisibility(View.GONE);
                }

            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                postListview.setVisibility(View.GONE);
                // Util.showAlertDialog(mContext, getString(R.string.alert), msg);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            no_data_tv.setVisibility(View.VISIBLE);
            postListview.setVisibility(View.GONE);

        }
    }

    @Override
    public void onClickOnLike(int position, String status) {

        mAuthAPI.postLikeByUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mPostListBeen.get(position).get_id(), "", status);
    }

    public void onClickOnshare(int position) {
        mAuthAPI.postShareByUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mPostListBeen.get(position).get_id());
    }

    /*  user like post response*/
    public void postLikeByUserWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == I.COMMENT_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK)
            {
                if(data!=null)
                {
                    String position = data.getStringExtra("postPosition");
                    String comment_count = data.getStringExtra("total_comment");
                    String like_count = data.getStringExtra("total_likes");
                    String isLike = data.getStringExtra("isLike");
                    if (isLike != null) {
                        mPostListBeen.get(Integer.parseInt(position)).setLikedPost(isLike);
                    }


                    mPostListBeen.get(Integer.parseInt(position)).setCommentPostCount(comment_count);
                    dashBoardPostsAdapter.notifyItemChanged(Integer.parseInt(position));
                    //postEdittext.setText(postEdittext.getText().toString() + " " + vehicle_name);
                }

            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onClickOnDelete(int position) {
        deletePost(mPostListBeen.get(position).get_id());
    }

    private void deletePost(String post_id) {
        mAuthAPI.deletePost(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), post_id);
    }

    public void postRemoveResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, S.postRemove_api);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void onCLickOnFb(final int position) {
        if (mPostListBeen.get(position).getDashboardPostImagesBeans().size() == 0) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, mPostListBeen.get(position).getDescription());
            startActivity(Intent.createChooser(intent, "Share with"));
        } else {
            List<String> permissionNeeds = Arrays.asList("publish_actions");
            LoginManager.getInstance().logInWithPublishPermissions(this, permissionNeeds);
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    List<Bitmap> temp = null;
                    if (mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getType().equalsIgnoreCase(S.videoType))
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getThumbName(), ImageLoader.getInstance().getMemoryCache());
                    else
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getFileName(), ImageLoader.getInstance().getMemoryCache());

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(temp.get(0))
                            .build();

                    ArrayList<SharePhoto> photos = new ArrayList<>();
                    photos.add(photo);
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .setPhotos(photos)
                            .build();

                    ShareDialog shareDialog = new ShareDialog(GroupDiaryActivity.this);
                    if (ShareDialog.canShow(SharePhotoContent.class)) {
                        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                    }
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Log.d("DashBoard", "====SUCCESS");
                        }

                        @Override
                        public void onCancel() {
                            Log.d("DashBoard", "====CANCEL");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d("DashBoard", "====ERROR");
                        }
                    });
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Log.e(TAG, error.toString());
                }
            });


           /* ShareDialog shareDialog = new ShareDialog(this);
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(getString(R.string.app_name))
                        .setImageUrl(Uri.parse(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getFileName()))
                        .setContentDescription(mPostListBeen.get(position).getDescription())
                        .setContentUrl(Uri.parse("http://182.156.245.85:3000/home"))
                        .build();
                shareDialog.show(linkContent);  // Show facebook ShareDialog
            }*/


        }
/*
            ShareDialog shareDialog = new ShareDialog(this);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setImageUrl(Uri.parse("https://www.studytutorial.in/wp-content/uploads/2017/02/FacebookLoginButton-min-300x136.png"))
                    .setContentDescription(
                            "This tutorial explains how to integrate Facebook and Login through Android Application")
                    .setContentUrl(Uri.parse("https://www.studytutorial.in/roid-facebook-integration-and-login-tutorial"))
                    .build();
            shareDialog.show(linkContent);  // Show facebook ShareDialog
        }*/
    }
}
