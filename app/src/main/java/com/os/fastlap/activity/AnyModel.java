package com.os.fastlap.activity;

public class AnyModel {
    public int time;
    public double speed=0.0;
    public double dist=0.0;
    public double lat=0.0;
    public double lng=0.0;
    public double brake_calc=0.0;
    public double acc_calc=0.0;
    public double brake_on=0.0;
    public double acc_on=0.0;
}
