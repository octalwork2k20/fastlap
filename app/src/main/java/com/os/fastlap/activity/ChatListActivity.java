package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.group.CreateGroupActivity;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.adapter.ChatListAdapter;
import com.os.fastlap.beans.BeanMessage;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.data.database.ChatDbHelper;
import com.os.fastlap.data.database.DatabaseManager;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.os.fastlap.R.id.user_img;

/**
 * Created by abhinava on 7/12/2017
 */

public class ChatListActivity extends BaseActivity implements View.OnClickListener, ChatListAdapter.Onclick {
    private EditText edtSearch;
    private TextView createGroupBtn;
    private RecyclerView chatsRecyclerView;
    private ImageView createChatFab;
    private TextView no_data_tv;
    private ChatListAdapter chatListAdapter;
    ArrayList<BeanMessage> chats_list = new ArrayList<>();
    ArrayList<BeanMessage> newChatList = new ArrayList<>();
    Context context;
    private View view;
    private Paint p = new Paint();
    private ImageView exploreIcon;

    private CircleImageView toolbarUserPic;
    private TextViewPlayRegular toolbarTxtUserName;
    DatabaseManager databaseManager;
    Cursor cursor;
    private ImageView mylap_icon;
    private AuthAPI mAuthAPI;
    BeanMessage beanMessage ;
    StringBuilder userIdBuilder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_list_activity);
        context = this;
         mAuthAPI=new AuthAPI(context);
        initView();
        databaseManager = DatabaseManager.getInstance(this);
    }

    private void initView() {
        edtSearch = findViewById(R.id.searchEt);
        createGroupBtn = findViewById(R.id.create_group_btn);
        chatsRecyclerView = findViewById(R.id.chats_recyclerView);
        createChatFab = (ImageView) findViewById(R.id.create_chat_fab);
        toolbarUserPic = (CircleImageView) findViewById(R.id.user_img);
        exploreIcon = (ImageView) findViewById(R.id.explore_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        mylap_icon = (ImageView) findViewById(R.id.mylap_icon);

        chats_list = new ArrayList<>();
        chats_list.clear();

        chatListAdapter = new ChatListAdapter(chats_list, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        chatsRecyclerView.setLayoutManager(layoutManager);
        chatsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        chatsRecyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        chatsRecyclerView.setAdapter(chatListAdapter);
        chatListAdapter.addChatListAll(chats_list);
        chatListAdapter.setOnclickListner(this);

        initSwipe();

        clickListner();

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                chatListAdapter.filter(arg0.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });

    }

    private void clickListner() {
        createGroupBtn.setOnClickListener(this);
        createChatFab.setOnClickListener(this);
        toolbarUserPic.setOnClickListener(this);
        exploreIcon.setOnClickListener(this);
        mylap_icon.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.CHAT_SCREEN);
        FastLapApplication.mCurrentContext = context;
        // toolbarTxtUserName.setText(MySharedPreferences.getPreferences(context, S.username));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image), toolbarUserPic, Util.getImageLoaderOption(context));

        new AsyncTaskRunner().execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_group_btn:
                Intent intent=new Intent(this,CreateGroupActivity.class);
                intent.putExtra(S.showGroupDetails,false);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                startActivity(intent);
                break;

            case R.id.create_chat_fab:
                Intent intent1 = new Intent(context, AllUsersActivity.class);
                startActivity(intent1);

                break;
            case R.id.explore_icon:
                Util.startNewActivity(ChatListActivity.this, ExploreActivity.class, false);
                break;
            case user_img:
                Util.startNewActivity(ChatListActivity.this, MyLapsActivity.class, false);
                break;
            case R.id.mylap_icon:
                Util.startNewActivity(ChatListActivity.this, MyLapsActivity.class, false);
                break;
        }
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.LEFT) {
                    Util.showAlertDialogWithTwoButton(context, getString(R.string.chat_delete_warning_msg), getString(R.string.cancel), getString(R.string.remove), position);

                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (!(dX > 0)) {
                        p.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.mipmap.delete);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(chatsRecyclerView);
    }

    private void removeView() {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    @Override
    public void onRowClick(int position) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(S.friendId, chats_list.get(position).getChannelId());
        intent.putExtra(S.userId, chats_list.get(position).getMessageSenderId());
        intent.putExtra(S.username, chats_list.get(position).getMessageSenderName());
        intent.putExtra(S.image, chats_list.get(position).getMessageSenderImage());
        intent.putExtra(S.CHAT_TYPE, chats_list.get(position).getChatType());
        intent.putExtra(S.ONLINE_STATUS, chats_list.get(position).getOnLineStatus());
        context.startActivity(intent);

        databaseManager.updateUnreadCount(chats_list.get(position).getChannelId());

    }

    /* Callback of chat delete dialog */
    public void removeChatSpecificUser(int pos) {
        databaseManager.deleteMessageHistorySpecificUser(chats_list.get(pos).getChannelId(), chats_list.get(pos).getMessageSenderId());
        chatListAdapter.removeItem(pos);
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        AsyncTaskRunner() {
            userIdBuilder=new StringBuilder();
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                cursor = databaseManager.queryDistinctById(ChatDbHelper.TABLE_LAST_MESSAGE, ChatDbHelper.CHANNEL_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            /*set adapter*/
            if (cursor != null) {
                int count = cursor.getCount();
                //   Toast.makeText(context, count + " message", Toast.LENGTH_LONG).show();
                if (cursor.moveToFirst())
                {
                    chats_list.clear();
                    do {
                        beanMessage = new BeanMessage();
                        beanMessage.setChannelId(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_CHANNEL_ID)));
                        beanMessage.setMessageText(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_MESSAGE)));
                        beanMessage.setMessageSenderId(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_USER_ID)));
                        beanMessage.setMessageSenderName(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_USER_NAME)));
                        beanMessage.setMessageSenderImage(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_USER_IMAGE)));
                        beanMessage.setMessageDate(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_MESSAGE_DATE)));
                        beanMessage.setUnReadCount(cursor.getInt(cursor.getColumnIndex(ChatDbHelper.UNREAD_COUNT)));
                        beanMessage.setChatType(cursor.getString(cursor.getColumnIndex(ChatDbHelper.MESSAGE_TYPE)));
                        beanMessage.setLastSendUserType(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_SEND_USER_TYPE)));
                        beanMessage.setOnLineStatus(false);
                        userIdBuilder.append(cursor.getString(cursor.getColumnIndex(ChatDbHelper.LAST_USER_ID)));
                        userIdBuilder.append(",");
                        chats_list.add(beanMessage);
                        }
                    while (cursor.moveToNext());
                }
                Log.e("tag","fggfdgfdg");
                callOnLineStatus();
                //chatListAdapter.notifyDataSetChanged();
                cursor.close();

                if (chats_list.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    chatsRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    chatsRecyclerView.setVisibility(View.GONE);
                }
            }
        }
    }


    public void RefreshData() {
        new AsyncTaskRunner().execute();
    }

    private void callOnLineStatus(){
        mAuthAPI.getOnlineStatusByUserID(context,userIdBuilder.toString());
        Log.e("tag","userId"+userIdBuilder);
    }




    public void getUserStatus(String responce){
        try {
            newChatList.addAll(chats_list);
            chats_list.clear();
            JSONArray jsonArray=new JSONArray(responce);
            Log.e("tag","mChat"+responce);
            for(int index=0;index<jsonArray.length();index++){
                JSONObject jsonObject=jsonArray.getJSONObject(index);
                BeanMessage beanMessage=new BeanMessage();
                beanMessage.setOnLineStatus(jsonObject.getBoolean("isOnline"));
                beanMessage.setChannelId(newChatList.get(index).getChannelId());
                beanMessage.setMessageText(newChatList.get(index).getMessageText());
                beanMessage.setMessageSenderId(newChatList.get(index).getMessageSenderId());
                beanMessage.setMessageSenderName(newChatList.get(index).getMessageSenderName());
                beanMessage.setMessageSenderImage(newChatList.get(index).getMessageSenderImage());
                beanMessage.setMessageDate(newChatList.get(index).getMessageDate());
                beanMessage.setUnReadCount(newChatList.get(index).getUnReadCount());
                beanMessage.setChatType(newChatList.get(index).getChatType());
                beanMessage.setLastSendUserType(newChatList.get(index).getLastSendUserType());
                chats_list.add(beanMessage);
            }
            chatListAdapter.notifyDataSetChanged();


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("tag","mChat"+responce);
    }

}
