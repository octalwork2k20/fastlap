package com.os.fastlap.activity.community;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.os.fastlap.R;
import com.os.fastlap.activity.dashboard.DashboardVehicleListActivity;
import com.os.fastlap.activity.payment.AddCardDetail;
import com.os.fastlap.adapter.EventTicketListAdapter;
import com.os.fastlap.beans.BeanTicketPurchase;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by abhinava on 9/13/2017.
 */

public class EventTicketPurchase extends AppCompatActivity implements EventTicketListAdapter.CheckedListner {
    Context context;
    EventsBean eventsBean = new EventsBean();
    private static final int VEHICLE_ACTIVITY_RESULT_CODE = 0;
    ImageView crossIv;
    TextViewPlayRegular endDateTv;
    TextViewPlayRegular myVehicleTv;
    AppCompatCheckBox rentCheckbox;
    TextViewPlayRegular rentVehicleTv;
    TextViewPlayBold orderTv;
    RecyclerView ticketRecyclerView;

    String vehicle_name = "";
    String vehicle_id = "";
    String eventId = "";
    ArrayList<BeanVehicleType> numberList;
    BeanTicketPurchase beanTicketPurchase;
    EventTicketListAdapter eventTicketListAdapter;
    private String TAG = EventTicketPurchase.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_purchase_layout);
        context = this;
        FastLapApplication.mCurrentContext = context;
        eventsBean = (EventsBean) getIntent().getExtras().getSerializable(S.data);
        eventId = eventsBean.get_id();
        beanTicketPurchase = new BeanTicketPurchase();
        beanTicketPurchase.setEventId(eventId);
        ticketPurchase();
        fillNumberArray();
    }

    private void ticketPurchase() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.event_purchase_dialog);


        crossIv = (ImageView) dialog.findViewById(R.id.cross_iv);
        endDateTv = (TextViewPlayRegular) dialog.findViewById(R.id.end_date_tv);
        myVehicleTv = (TextViewPlayRegular) dialog.findViewById(R.id.my_vehicle_tv);
        rentCheckbox = (AppCompatCheckBox) dialog.findViewById(R.id.rent_checkbox);
        rentVehicleTv = (TextViewPlayRegular) dialog.findViewById(R.id.rent_vehicle_tv);
        ticketRecyclerView = (RecyclerView) dialog.findViewById(R.id.ticketRecyclerView);
        orderTv = (TextViewPlayBold) dialog.findViewById(R.id.order_tv);

        eventTicketListAdapter = new EventTicketListAdapter(eventsBean.getPresetsList(), context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        ticketRecyclerView.setLayoutManager(layoutManager);
        ticketRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ticketRecyclerView.setAdapter(eventTicketListAdapter);
        eventTicketListAdapter.setOnItemClickListener(this);

        crossIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        orderTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean flag = false;

                for (int i = 0; i < eventsBean.getPresetsList().size(); i++) {
                    if (eventsBean.getPresetsList().get(i).isSelected()) {
                        flag = true;
                        beanTicketPurchase.setQuantity(eventsBean.getPresetsList().get(i).getTotalUser());
                        beanTicketPurchase.setAmount(eventsBean.getPresetsList().get(i).getPrice());
                        beanTicketPurchase.setPresetsId(eventsBean.getPresetsList().get(i).get_id());
                        break;
                    }
                }

                if (flag) {
                    dialog.dismiss();
                    beanTicketPurchase.setVehicleId(vehicle_id);
                    Intent intent = new Intent(context, AddCardDetail.class);
                    intent.putExtra(S.type, I.EVENT_TICKET_PAYMET + "");
                    intent.putExtra(S.data, beanTicketPurchase);
                    startActivityForResult(intent, I.ADDPOSTACTIVTYCALLBACK);
                } else {
                    Util.showSnackBar(orderTv, getString(R.string.please_select_a_ticket_type));
                    return;
                }
            }
        });

        myVehicleTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DashboardVehicleListActivity.class);
                startActivityForResult(intent, VEHICLE_ACTIVITY_RESULT_CODE);
            }
        });

        endDateTv.setText(getString(R.string.sales_end_on) + " " + Util.ConvertDateTimeZoneDateMonth(eventsBean.getEndDate()));

        dialog.setCancelable(false);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VEHICLE_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                vehicle_name = data.getStringExtra(S.vehiclename);
                vehicle_id = data.getStringExtra(S.vehicleId);

                myVehicleTv.setText(vehicle_name);
            }
        } else if (requestCode == I.ADDPOSTACTIVTYCALLBACK)
        {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

    private void fillNumberArray() {

        numberList = new ArrayList<>();
        numberList.clear();

        for (int i = 0; i < 4; i++) {
            BeanVehicleType beanVehicleType = new BeanVehicleType();
            beanVehicleType.setId((i + 1) + "");
            beanVehicleType.setName((i + 1) + "");

            numberList.add(beanVehicleType);
        }
    }


    public void goToEvent() {
        finish();
    }


    @Override
    public void CheckChange(int postion) {
        for (int i = 0; i < eventsBean.getPresetsList().size(); i++) {
            if (i == postion) {
                eventsBean.getPresetsList().get(i).setSelected(true);
            } else {
                eventsBean.getPresetsList().get(i).setSelected(false);
            }
        }

        eventTicketListAdapter.notifyDataSetChanged();

    }
}
