package com.os.fastlap.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.opencsv.CSVWriter;
import com.os.fastlap.R;
import com.os.fastlap.adapter.FileSpinnerAdpater;
import com.os.fastlap.beans.BeanTrackVersion;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.geofancing.Constants;
import com.os.fastlap.geofancing.GeofenceController;
import com.os.fastlap.geofancing.NamedGeofence;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.Compass;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

public class RecordCameraActivity extends BaseActivity implements View.OnClickListener, SensorEventListener, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GpsStatus.Listener {

    public static String TAG = RecordCameraActivity.class.getSimpleName();
    public static Context context;

    Handler handler;
    Handler timerHandler;
    private TextViewPlayRegular distanceTv;


    private ImageView myLocationIv;
    private TextViewPlayRegular speedTv;
    private TextViewPlayRegular timeTv;
    private TextViewPlayRegular switchTv;
    private TextViewPlayRegular speedMesureTv;
    //  private ImageView startStopIv;
    boolean isStart = false;
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private Sensor senGyro;
    private Sensor sensorMagneticField;
    private long lastUpdate = 0;
    private float last_x=0, last_y=0, last_z=0;
    private float[] valuesAccelerometer;
    private float[] valuesMagneticField;

    private float[] matrixR;
    private float[] matrixI;
    private float[] matrixValues;

    private long mLastTime;
    private long mStartTime;
    long MillisecondTime, StartTime, UpdateTime = 0L;

    String distanceValue = "0";
    String fileName = "";
    File filePath;

    AuthAPI authAPI;
    BeanTrackVersion beanTrackVersion;
    int lapCount = 0;
    NamedGeofence geofence;
    CSVWriter writer = null;
    Compass myCompass;
    ImageView gpsImg;
    TextView gpsTv;
    String speedValue = "0";
    float gyro_x = 0, gyro_y = 0, gyro_z = 0;
    int batteryVoltage = 0;
    int gpsUpdate;
    double distance = 0;
    double totalDistance = 0;
    boolean status = true;
    double tempLat = 0;
    double tempLong = 0;

    private static final long INTERVAL = 1000 * 2;
    private static final long FASTEST_INTERVAL = 1000 * 1;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation, lStart, lEnd;
    double speed;

    //for new work
    Button mUploadButton;
    Spinner mFileSpinner;
    String newFilePath;
    RelativeLayout circularLyt;
    RelativeLayout spinnerLyt;
    Location oldLocation;
    private boolean goToNext = false;
    public  static RecordCameraActivity mInstance;
    private PowerManager.WakeLock mWackLock;
    private String gpsAccuracy ="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_record_layout);
        try {
            PowerManager pm= (PowerManager) this.getSystemService(Context.POWER_SERVICE);
            mWackLock=pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,"MY TAG");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        mInstance=this;
        context = this;
        authAPI = new AuthAPI(context);
        beanTrackVersion = new BeanTrackVersion();
        initView();
        setonclickListner();
        //autoOnGps();
        GeofenceController.getInstance().init(this);
        checkPermission();
        // SpeedCalc();
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(mWackLock!=null)
        mWackLock.acquire();

    }
    @Override
    protected void onStop() {
        super.onStop();
        if(mWackLock!=null)
        mWackLock.release();
    }

    private void initView() {
        handler = new Handler();
        timerHandler = new Handler();
        distanceTv = (TextViewPlayRegular) findViewById(R.id.distance_tv);
        myLocationIv = (ImageView) findViewById(R.id.my_location_iv);
        speedTv = (TextViewPlayRegular) findViewById(R.id.speed_tv);
        timeTv = (TextViewPlayRegular) findViewById(R.id.time_tv);
        switchTv = (TextViewPlayRegular) findViewById(R.id.switch_tv);
        speedMesureTv = (TextViewPlayRegular) findViewById(R.id.speedMesureTv);
        //  startStopIv = (ImageView) findViewById(R.id.start_stop_iv);
        myCompass = (Compass) findViewById(R.id.mycompass);
        gpsImg = (ImageView) findViewById(R.id.gpsIv);
        gpsTv = (TextView) findViewById(R.id.gpsTv);


        mUploadButton = (Button) findViewById(R.id.btn_upload);
        mFileSpinner = (Spinner) findViewById(R.id.spinner_file);
        circularLyt = (RelativeLayout) findViewById(R.id.circularLyt);
        spinnerLyt = (RelativeLayout) findViewById(R.id.spinnerLyt);


        valuesAccelerometer = new float[3];
        valuesMagneticField = new float[3];

        matrixR = new float[9];
        matrixI = new float[9];
        matrixValues = new float[3];

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senGyro = senSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorMagneticField = senSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        switchTv.setText(getString(R.string.start));
        try {
            String speed = MySharedPreferences.getPreferences(context, S.speed);
            if (!TextUtils.isEmpty(speed)) {
                if (!speed.equalsIgnoreCase("MILES")) {
                    speedMesureTv.setText(getString(R.string.KMPH));

                } else {
                    speedMesureTv.setText(getString(R.string.MPH));
                }
            } else {
                speedMesureTv.setText(getString(R.string.KMPH));
            }
        } catch (Exception e) {
            Log.e("tag1", "speedMesureTv Exception" + e);
        }
    }

    private void setonclickListner() {
        circularLyt.setOnClickListener(this);
        mUploadButton.setOnClickListener(this);
        mFileSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                newFilePath = String.valueOf(adapterView.getItemAtPosition(i));
                newFilePath = Util.path + "/" + newFilePath;
//                if(newFilePath!=null){
//                    Util.deleteUploadedFile(context,newFilePath);
//                }
                Log.e("tag1", "item" + newFilePath);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        setSpinnerAdapter(Util.getAllFileFromStorage());
        mFileSpinner.setSelection(0);
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(RecordCameraActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(RecordCameraActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(RecordCameraActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(RecordCameraActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RecordCameraActivity.this, new String[]
                    {Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            // finish();
        }
    }

    private void autoOnGps()
    {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS) {
            createLocationRequest();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();
        } else {
            showGPSDisabledAlertToUser();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        autoOnGps();
        setBottomMenu(I.RECORD_SCREEN);
        FastLapApplication.mCurrentContext = context;
        CheckGpsStatus();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, senGyro, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, sensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);

        int googlePlayServicesCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // Log.i(AllGeofencesActivity.class.getSimpleName(), "googlePlayServicesCode = " + googlePlayServicesCode);

        if (googlePlayServicesCode == 1 || googlePlayServicesCode == 2 || googlePlayServicesCode == 3) {
            GooglePlayServicesUtil.getErrorDialog(googlePlayServicesCode, this, 0).show();
        }
    }

    public void CheckGpsStatus() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS) {
            gpsImg.setImageResource(R.mipmap.gps);
            gpsTv.setText(getString(R.string.gps_accuracy));
            distanceTv.setVisibility(View.VISIBLE);
            circularLyt.setBackgroundResource(R.drawable.circular_button);
            switchTv.setTextColor(getResources().getColor(R.color.white));
            //switchTv.setText(getString(R.string.start));
        } else {
            gpsImg.setImageResource(R.mipmap.gps_red);
            gpsTv.setText(getString(R.string.no_gps_found));
            distanceTv.setVisibility(View.GONE);
            // switchTv.setTextColor(getResources().getColor(R.color.gray_text_color ));
            circularLyt.setBackgroundResource(R.drawable.circular_button_gray);
            switchTv.setTextColor(getResources().getColor(R.color.white));

            //showGPSDisabledAlertToUser();
        }

    }

    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this,
                senAccelerometer);
        senSensorManager.unregisterListener(this,
                senGyro);
        senSensorManager.unregisterListener(this,
                sensorMagneticField);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.circularLyt:
                if (Util.isNetworkAvailable(context)) {
                    goforRecording();
                } else {
                    showInternetAlertDialog();
                }
                break;
            case R.id.btn_upload:
                if (Util.isNetworkAvailable(context)) {
                    if (mFileSpinner.getSelectedItemPosition() != 0) {
                        if (lStart != null) {
                            authAPI.getTrackDetailByLatLong(context, MySharedPreferences.getPreferences(context, S.user_id), lStart.getLatitude(), lStart.getLongitude());
                        }

                    } else {
                        Toast.makeText(context, getResources().getString(R.string.Please_select_file_from_drop_down), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    showInternetAlertDialogForUpload();
                }
                break;
        }
    }

    private void finalUploading(String filePath) {
        if (TextUtils.isEmpty(beanTrackVersion.getTrackId())) {
            Intent intent = new Intent(context, ContactUsActivity.class);
            intent.putExtra(S.fileName, filePath);
            intent.putExtra(S.lapTime, MillisecondTime + "");
            intent.putExtra(S.userTrack, beanTrackVersion);
            startActivity(intent);
        } else {
            Intent intent = new Intent(context, UploadLapTimeActivity.class);
            intent.putExtra(S.fileName, filePath);
            intent.putExtra(S.lapTime, MillisecondTime + "");
            intent.putExtra(S.userTrack, beanTrackVersion);
            startActivity(intent);
            finish();
        }
    }

    private void goforRecording() {
        if (!isStart) {
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (statusOfGPS) {
                try {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    oldLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if(oldLocation!=null){
                        startRecording();
                        Log.e("service",""+":>"+oldLocation.getLatitude()+":->"+oldLocation.getLongitude());
                    }
                    else {
                        showGPSDisabledAlertInCaseLocationIsNull();
                    }

                }
                catch (NullPointerException e){
                    createLocationRequest();
                    mGoogleApiClient = new GoogleApiClient.Builder(this)
                            .addApi(LocationServices.API)
                            .addConnectionCallbacks(this)
                            .addOnConnectionFailedListener(this)
                            .build();
                    mGoogleApiClient.connect();
                }

            } else {
                showGPSDisabledAlertToUser();
               // Util.showAlertDialog(context, getString(R.string.alert), getString(R.string.please_enable_location_services_to_continue));
            }
        } else {
            stopRecording();
        }
    }

    protected void startRecording()
    {
        Log.e("tag0","startRecording");
        if (lStart != null)
        {
            if (dataIsValid(lStart.getLatitude(), lStart.getLongitude(), "0.05")) {
                geofence = new NamedGeofence();
                geofence.name = "Lap Found";
                geofence.latitude = lStart.getLatitude();
                geofence.longitude = lStart.getLongitude();
                geofence.radius = Float.parseFloat("0.05") * 1000.0f;
                GeofenceController.getInstance().addGeofence(geofence, geofenceControllerListener);
            } else {
                showValidationErrorToast();
            }
            UpdateTime = 0L;
            distance = 0;
            isStart = true;
          //  startStopIv.setVisibility(View.VISIBLE);
            timeTv.setText("0:00:00");
            switchTv.setText(getString(R.string.stop));
            String fileDate=Util.getDate(System.currentTimeMillis());
            fileName = "Fastlap" + fileDate + ".csv";
            filePath = new File(context.getExternalFilesDir(null), fileName);
            saveExcelFile();
            MillisecondTime = 0;
            //   timeInSecond = "";
            StartTime = SystemClock.uptimeMillis();
            handler.postDelayed(runnable, 0);

            timerHandler.postDelayed(timerRunnable, 0);

            // SpeedCalc();
        } else {
            Toast.makeText(context, getString(R.string.please_try_again), Toast.LENGTH_LONG);
        }
    }



    private void stopRecording() {
        Log.e("tag0","stopRecording");
        if (geofence != null) {
            List<NamedGeofence> namedGeofences = new ArrayList<>();
            namedGeofences.add(geofence);
            GeofenceController.getInstance().removeGeofences(namedGeofences, geofenceControllerListener);
        }

        isStart = false;
        switchTv.setText(getString(R.string.start));
        //  timeTv.setText("0:00:00");
        handler.removeCallbacks(runnable);
        timerHandler.removeCallbacks(timerRunnable);
        SaveFile(filePath);
    }
    public void SaveFile(File file){
        boolean success = false;
        // Create a path where we will place our List of objects on external storage
        FileOutputStream os = null;
        try {
            writer.close();
            mUploadButton.setVisibility(View.VISIBLE);
            setSpinnerAdapter(Util.getAllFileFromStorage());
        } catch (IOException e) {
            Log.w("FileUtils", "Error writing " + file, e);
        } catch (Exception e) {
            Log.w("FileUtils", "Failed to save file", e);
        } finally {

            try {
                if (null != os)
                    os.close();
            } catch (Exception ex) {
            }
        }
    }

    private GeofenceController.GeofenceControllerListener geofenceControllerListener = new GeofenceController.GeofenceControllerListener() {
        @Override
        public void onGeofencesUpdated() {
        }

        @Override
        public void onError() {
            showErrorToast();
        }
    };

    private void showErrorToast() {
        Toast.makeText(context, getResources().getString(R.string.some_error_occur_message), Toast.LENGTH_SHORT).show();
    }
    private boolean dataIsValid(double lat, double lon, String radis) {
        boolean validData = true;


        String latitudeString = lat + "";
        String longitudeString = lon + "";
        String radiusString = radis;

        if (TextUtils.isEmpty(latitudeString) || TextUtils.isEmpty(longitudeString) || TextUtils.isEmpty(radiusString)) {
            validData = false;
        } else {
            double latitude = lat;
            double longitude = lon;
            float radius = Float.parseFloat(radiusString);
            if ((latitude < Constants.Geometry.MinLatitude || latitude > Constants.Geometry.MaxLatitude)
                    || (longitude < Constants.Geometry.MinLongitude || longitude > Constants.Geometry.MaxLongitude)
                    || (radius < Constants.Geometry.MinRadius || radius > Constants.Geometry.MaxRadius)) {
                validData = false;
            }
        }

        return validData;
    }

    private void showValidationErrorToast() {
        Toast.makeText(context, getResources().getString(R.string.please_enter_valid_field_data), Toast.LENGTH_SHORT).show();
    }
    private boolean saveExcelFile() {
        boolean success = true;
        // check if available and not read only
        try {
            if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
                Log.e(TAG, "Storage not available or read only");
                return false;
            }
            success = false;
            writer = new CSVWriter(new FileWriter(filePath));
            createExcelSheet();
            Log.e("tag0","save file sucessfully");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }


    public Runnable runnable = new Runnable() {
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            double t = MillisecondTime / 1000.0;

            // timeInSecond = new DecimalFormat("0.0").format(t);

            timeTv.setText(Util.MilisecondsToTime(MillisecondTime));

            handler.postDelayed(this, 0);
        }
    };

    public Runnable timerRunnable = new Runnable() {

        public void run() {
            Log.e("tag","mCurrentLocation A"+ lStart);
            if(lStart !=null)
            {
                batteryVoltage = getBatteryPercentage(context);
                if (status) {
                    gpsUpdate = 1;
                    tempLat = lStart.getLatitude();
                    tempLong = lStart.getLongitude();
                }
                if (!status) {
                    if (tempLat == lStart.getLatitude() && tempLong == lStart.getLongitude()) {
                        gpsUpdate = 0;
                    } else {
                        gpsUpdate = 1;
                    }
                    tempLat = lStart.getLatitude();
                    tempLong = lStart.getLongitude();
                }
                status = false;
                String[] data = new String[]{lStart.getLatitude() + "", lStart.getLongitude()
                        + "", lStart.getAltitude() + "", new DecimalFormat("##.0").format(MillisecondTime / 1000.0)
                        + "", speedValue, totalDistance + "", last_x + "", last_y + "", last_z + "", lapCount + "",
                        gyro_x + "", gyro_y + "", gyro_z + "", batteryVoltage + "", gpsUpdate + "",gpsAccuracy + ""};

                DisplayRecording();
                writer.writeNext(data);
                timerHandler.postDelayed(this, 100);
            }
            /*if(mCurrentLocation !=null)
            {
                batteryVoltage = getBatteryPercentage(context);
                if (status) {
                    gpsUpdate = 1;
                    tempLat = mCurrentLocation.getLatitude();
                    tempLong = mCurrentLocation.getLongitude();
                }
                if (!status) {
                    if (tempLat == mCurrentLocation.getLatitude() && tempLong == mCurrentLocation.getLongitude()) {
                        gpsUpdate = 0;
                    } else {
                        gpsUpdate = 1;
                    }
                    tempLat = mCurrentLocation.getLatitude();
                    tempLong = mCurrentLocation.getLongitude();
                }
                status = false;
                Log.e("tag", "update"+String.valueOf(gpsUpdate));
                String[] data = new String[]{mCurrentLocation.getLatitude() + "", mCurrentLocation.getLongitude()
                        + "", mCurrentLocation.getAltitude() + "", new DecimalFormat("##.0").format(MillisecondTime / 1000.0)
                        + "", speedValue, totalDistance + "", last_x + "", last_y + "", last_z + "", lapCount + "",
                        gyro_x + "", gyro_y + "", gyro_z + "", batteryVoltage + "", gpsUpdate + ""};

                Log.e("dd","last_x"+last_x);
                Log.e("dd","last_y"+last_y);
                Log.e("dd","last_z"+last_z);

                DisplayRecording();

                writer.writeNext(data);


                timerHandler.postDelayed(this, 100);
            }*/

        }
    };

   /* public void setLapData(String lapLatuitude, String lapLongitude) {
        if (MillisecondTime > 10000) {
            lapCount = lapCount + 1;
        }
    }*/

    /*public void SpeedCalc() {
        if (mCurrentLocation != null) {
            distanceTv.setText(new DecimalFormat("0").format((mCurrentLocation.getAccuracy())) + "m");
        } else if (mCurrentLocation != null) {
            distanceTv.setText(new DecimalFormat("0").format((mCurrentLocation.getAccuracy())) + "m");
        } else {
            if(mCurrentLocation!=null) {
                distanceTv.setText(new DecimalFormat("0").format((mCurrentLocation.getAccuracy())) + "m");
            }
        }

    }*/
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
       // Log.e("tag1111","onSensorChanged");
        Sensor mySensor = sensorEvent.sensor;
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            last_x = x;
            last_y = y;
            last_z = z;

            for (int i = 0; i < 3; i++) {
                valuesAccelerometer[i] = sensorEvent.values[i];
            }

        }
      //  Log.e("aa","senser type"+mySensor.getType());
        if (mySensor.getType() == Sensor.TYPE_GYROSCOPE) {
            gyro_x = sensorEvent.values[0];
            gyro_y = sensorEvent.values[1];
            gyro_z = sensorEvent.values[2];
        }

        if (mySensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            for (int i = 0; i < 3; i++) {
                valuesMagneticField[i] = sensorEvent.values[i];
            }
        }

        boolean success = SensorManager.getRotationMatrix(
                matrixR,
                matrixI,
                valuesAccelerometer,
                valuesMagneticField);

        if (success) {
            SensorManager.getOrientation(matrixR, matrixValues);
            // myCompass.update(matrixValues[0]);
            myCompass.update(last_x, last_y);
        }

       // Log.e("cc","last_x"+last_x);
      //  Log.e("cc","last_y"+last_y);
      //  Log.e("cc","last_z"+last_z);


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
      //  Log.e("tag11","onAccuracyChanged");
    }
    private void createExcelSheet() {
        String[] data = new String[]{"Latitude", "Longitude", "Altitude", "Time", "Speed", "Distance",
                "AccelerometerX", "AccelerometerY", "AccelerometerZ", "LapCount", "GyroX", "GyroY",
                "GyroZ", "Int Batt Voltage", "GPS_Update", "GPS_Accuracy"};

        writer.writeNext(data);
    }
    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }
    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }
    public void trackDetailByLatLongWebserviceResponse(String response)
    {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                JSONObject dataJson = jsonObject.getJSONObject(S.data);

                beanTrackVersion = new BeanTrackVersion();


                JSONObject trackDataJSON;
                trackDataJSON = dataJson.getJSONObject(S.trackId);
                beanTrackVersion.setTrackId(trackDataJSON.getString(S._id));
                beanTrackVersion.setTrackName(trackDataJSON.getString(S.name));
                beanTrackVersion.setTrackImage(trackDataJSON.getString(S.image));
                beanTrackVersion.setTrackCountry(trackDataJSON.getString(S.country));
                beanTrackVersion.setTrackLocation(trackDataJSON.getString(S.location_response));

                JSONArray versionArray = new JSONArray();
                versionArray = dataJson.getJSONArray(S.version);
                ArrayList<BeanTrackVersion.Version> versionList = new ArrayList<>();
                for (int i = 0; i < versionArray.length(); i++) {
                    JSONObject versionObject = versionArray.getJSONObject(i);
                    BeanTrackVersion.Version version = new BeanTrackVersion().new Version();
                    version.setVersionId(versionObject.getString(S._id));
                    version.setVersionName(versionObject.getString(S.name));
                    version.setCreated(versionObject.getString(S.created));
                    version.setStartLeftLat(versionObject.getJSONArray(S.startLeft).getString(0));
                    version.setStartLeftLong(versionObject.getJSONArray(S.startLeft).getString(1));
                    version.setStartRightLat(versionObject.getJSONArray(S.startRight).getString(0));
                    version.setStartRightLong(versionObject.getJSONArray(S.startRight).getString(1));
                    versionList.add(version);
                }
                beanTrackVersion.setVersionArrayList(versionList);
                finalUploading(newFilePath);
            }/* else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // unregisterReceiver(mGpsSwitchStateReceiver);
        stopLocationUpdates();
        if (geofence != null) {
            List<NamedGeofence> namedGeofences = new ArrayList<>();
            namedGeofences.add(geofence);
            GeofenceController.getInstance().removeGeofences(namedGeofences, geofenceControllerListener);
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        Log.e("tag0","current location: "+lStart);
        if (lStart == null) {
            lStart = mCurrentLocation;
            lEnd = mCurrentLocation;
        } else
            lEnd = mCurrentLocation;

        //DisplayRecording();
        speed = location.getSpeed() * 18 / 5;
        Log.e("tag11","speed"+speed);
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermission();
        try {
            lStart = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            Log.e("tag0","start location: "+lStart);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {
            Log.e("tag","SecurityException :"+e);
        }
    }
    @Override
    public void onConnectionSuspended(int i) {
        Log.e("tag","location disable");
      //  finish();
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
       // finish();
    }



    public void DisplayRecording()
    {
        if (lEnd != null)
        {
            distanceTv.setText(new DecimalFormat("0").format((mCurrentLocation.getAccuracy())) + "m");
            try {
                gpsAccuracy= new DecimalFormat("0").format((mCurrentLocation.getAccuracy()));
            }
            catch (Exception e){
                e.printStackTrace();
            }
            if (isStart) {
                totalDistance = totalDistance + lStart.distanceTo(lEnd);
                lStart = lEnd;
                Log.e("tag11","total distance :"+totalDistance);
            }
            if(mCurrentLocation!=null && oldLocation!=null)
            {
                float distance=oldLocation.distanceTo(mCurrentLocation);
                if (distance > 40 && !goToNext) {
                    goToNext = true;
                }
                if (goToNext && distance < 20) {
                    goToNext = false;
                    lapCount += 1;
                }
                Log.e("tag11","lapCount :"+lapCount);
            }
            /*distance = lStart.distanceTo(lEnd);
            totalDistance=totalDistance+distance;
            Log.e("tag11","total distance :"+totalDistance);
            if(distance>40 &&!goToNext){
                goToNext=true;
            }
            if(goToNext && distance <20){
                goToNext=false;
                lapCount+=1;
            }*/
            Formatter fmt = new Formatter(new StringBuilder());
            fmt.format(Locale.US, "%5.1f", speed);
            String strCurrentSpeed = fmt.toString();
            strCurrentSpeed = strCurrentSpeed.replace(' ', '0');
            speedValue = strCurrentSpeed;
            Log.e("tag11","speedValue :"+speedValue);

            String speed = MySharedPreferences.getPreferences(context, S.speed);

            if (!TextUtils.isEmpty(speed))
            {
                if (!speed.equalsIgnoreCase("MILES")) {
                    speedMesureTv.setText(getString(R.string.KMPH));
                    if (isStart)
                        speedTv.setText(new DecimalFormat("0.00").format(Double.parseDouble(strCurrentSpeed)));

                } else {
                    speedMesureTv.setText(getString(R.string.MPH));
                    double d = Double.parseDouble(strCurrentSpeed);
                    d = d / 1.61;
                    if (isStart)
                        speedTv.setText(new DecimalFormat("0.00").format(d));
                }
            } else {
                /*speedMesureTv.setText(getString(R.string.KMPH));
                if (isStart)
                    speedTv.setText(strCurrentSpeed);*/
                speedMesureTv.setText(getString(R.string.MPH));
                double d = Double.parseDouble(strCurrentSpeed);
                d = d / 1.61;
                if (isStart)
                    speedTv.setText(new DecimalFormat("0.00").format(d));
            }
        }

    }
    public static int getBatteryPercentage(Context context) {
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    protected void stopLocationUpdates() {
        if(mGoogleApiClient!=null && mGoogleApiClient.isConnected()){
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            distance = 0;
            if (mGoogleApiClient.isConnected())
                mGoogleApiClient.disconnect();
            lStart = null;
            lEnd = null;
            distance = 0;
        }

    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.gps_alert_messsage))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.enable_gps),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showGPSDisabledAlertInCaseLocationIsNull() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.location_not_found_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                goforRecording();
                            }
                        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showInternetAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.internat_track_validation))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                goforRecording();
                            }
                        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void showInternetAlertDialogForUpload() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.internat_session_validation))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void setSpinnerAdapter(ArrayList<String> allFileFromStorage) {
        if(!allFileFromStorage.isEmpty()){
            spinnerLyt.setVisibility(View.VISIBLE);
            FileSpinnerAdpater fileSpinnerAdpater=new FileSpinnerAdpater(getApplicationContext(),  R.layout.spinner_layout, allFileFromStorage);
            mFileSpinner.setAdapter(fileSpinnerAdpater);
        }
    }
    @Override
    public void onGpsStatusChanged(int i) {
        Log.e("tag","onGpsStatusChanged"+i);
    }
    public static RecordCameraActivity getRecordInstance(){
        return mInstance;
    }
}
