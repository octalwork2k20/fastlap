package com.os.fastlap.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.os.fastlap.R;
import com.os.fastlap.activity.payment.AddCardDetail;
import com.os.fastlap.activity.profile.AddAlbumActivity;
import com.os.fastlap.adapter.AlbumAdapter;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.listener.RecyclerItemClickListener;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhinava on 7/17/2017
 */
public class AlbumActivity extends BaseActivity implements View.OnClickListener,OnImagePickerDialogSelect {

    private TextView albumNameTv;
    private TextView albumTitleTv;
    private TextView raceNameTv;
    private TextView circuitNameTv;
    private TextView dateTimeTv;
    private TextView addUserCountTv;
    private TextView bikeCountTv;
    private TextView dateTv;
    BeanAlbum beanAlbum;
    AlbumAdapter albumAdapter;
    RecyclerView albumRecyclerView;
    LinearLayout shareLayout;
    private TextView no_data_tv;
    private CircleImageView userProfileIv;
    TextView buyPhotosLayout;
    LinearLayout mLytCountPrice;
    ArrayList<BeanAlbum.BeanFiles> images_list;
    TextView mTvPrice2,mTvPrice5,mTvPhoto2,mTvPhoto5;
    int mFragmentType;
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private AuthAPI mAuthAPI;
    CallbackManager callbackManager;
    ImageView base_toggle_icon;
    static ArrayList<String>photoGrapherIds=new ArrayList<>();
    ImageView add_album;
    TextView menu_menu;
    LinearLayout buy_photos_parent_layout;
    String type="";
    private LoginManager loginManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_activity);
        FacebookSdk.sdkInitialize(getApplicationContext());
        onImagePickerDialogSelect = this;
        callbackManager = CallbackManager.Factory.create();
        beanAlbum = (BeanAlbum) getIntent().getExtras().getSerializable(S.data);
        type=getIntent().getStringExtra("mediyaType");
        initilization();


    }
    private void initilization(){
        add_album = findViewById(R.id.add_album);
        add_album.setImageResource(R.mipmap.edit);
        add_album.setVisibility(View.VISIBLE);
        mAuthAPI = new AuthAPI(mContext);
        circuitNameTv = (TextView) findViewById(R.id.circuit_name_tv);
        albumNameTv = (TextView) findViewById(R.id.album_name_tv);
        dateTimeTv = (TextView) findViewById(R.id.date_time_tv);
        albumTitleTv = (TextView) findViewById(R.id.album_title_tv);
        raceNameTv = (TextView) findViewById(R.id.race_name_tv);
        dateTv = (TextView) findViewById(R.id.date_tv);
        addUserCountTv = (TextView) findViewById(R.id.add_user_count_tv);
        bikeCountTv = (TextView) findViewById(R.id.bike_count_tv);
        albumRecyclerView = (RecyclerView) findViewById(R.id.album_recyclerView);
        shareLayout = (LinearLayout) findViewById(R.id.share_layout);
        userProfileIv = findViewById(R.id.user_profile_iv);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        buyPhotosLayout = (TextView) findViewById(R.id.buy_photos_layout);
        mLytCountPrice=(LinearLayout)findViewById(R.id.lytPhotoCountPrice);
        mTvPrice2 = (TextView) findViewById(R.id.tvPrice2);
        mTvPrice5 = (TextView) findViewById(R.id.tvPrice5);
        mTvPhoto2 = (TextView) findViewById(R.id.tvPhoto2);
        mTvPhoto5 = (TextView) findViewById(R.id.tvPhoto5);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        buy_photos_parent_layout=(LinearLayout)findViewById(R.id.buy_photos_parent_layout);

        shareLayout.setOnClickListener(this);
        base_toggle_icon.setOnClickListener(this);
        buyPhotosLayout.setOnClickListener(this);
        add_album.setOnClickListener(this);
        menu_menu.setOnClickListener(this);

        if(mFragmentType==4){
            mLytCountPrice.setVisibility(View.VISIBLE);
        }
        if(beanAlbum!=null){
            if (!beanAlbum.getUserId().equalsIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id))){
                add_album.setVisibility(View.GONE);
            }
            setData();
        }
        albumRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, albumRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                multi_select(position);
            }
           /* @Override
            public void onItemLongClick(View view, int position) {

            }*/
        }));
    }

    public void multi_select(int position) {
        try {

            if(images_list.get(position).isSelected()){
                images_list.get(position).setSelected(false);
            }
            else {
                images_list.get(position).setSelected(true);

            }
            albumAdapter.notifyDataSetChanged();
            Log.e("tag","position "+position);
        }
        catch (Exception e){e.printStackTrace();}


    }
    private void setData() {
        albumNameTv.setText(beanAlbum.getName());
        albumTitleTv.setText(beanAlbum.getUserName());
        raceNameTv.setText(beanAlbum.getEventName());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanAlbum.getUserImage(), userProfileIv, Util.getImageLoaderOption(mContext));
        try {
            dateTimeTv.setText(Util.calculateTimeDiffFromNow(beanAlbum.getCreated(), mContext));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(beanAlbum.getDateOfEvent())) {
            dateTv.setVisibility(View.GONE);
        } else {
            dateTv.setVisibility(View.VISIBLE);
            dateTv.setText(Util.ConvertDateTimeZoneDate(beanAlbum.getDateOfEvent()));
        }
        if(beanAlbum.getPhotoList().size()>0){
            buyPhotosLayout.setVisibility(View.VISIBLE);
            mLytCountPrice.setVisibility(View.VISIBLE);
            mTvPhoto2.setText(beanAlbum.getPhotoList().get(0).getNumberOfPhotos()+"x Photos");
            mTvPrice2.setText("$"+beanAlbum.getPhotoList().get(0).getPrice());
            mTvPhoto5.setText(beanAlbum.getPhotoList().get(0).getNumberOfPhotos()+"x Photos");
            mTvPrice5.setText("$"+beanAlbum.getPhotoList().get(0).getPrice());
        }
        else {
            if (TextUtils.isEmpty(beanAlbum.getTrackName())) {
                circuitNameTv.setVisibility(View.GONE);
            } else {
                circuitNameTv.setVisibility(View.GONE);
                circuitNameTv.setText(beanAlbum.getTrackName());
            }

        }

        addUserCountTv.setText(beanAlbum.getFriendList().size() + "");
        bikeCountTv.setText("0");
        setImageToAdapter();
    }
    private void setImageToAdapter(){
        Log.e("tag420","watermark image"+beanAlbum.getPresetImage());
        images_list = beanAlbum.getFilesList();
        albumAdapter = new AlbumAdapter(mContext, images_list,beanAlbum.getPresetImage());
        albumRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        albumRecyclerView.setItemAnimator(new DefaultItemAnimator());
        albumRecyclerView.setAdapter(albumAdapter);

        if(beanAlbum.getPresetImage().equalsIgnoreCase("")){
            buy_photos_parent_layout.setVisibility(View.GONE);
        }
        if (images_list.size() > 0) {
            no_data_tv.setVisibility(View.GONE);
            albumRecyclerView.setVisibility(View.VISIBLE);
        }
        else {
            no_data_tv.setVisibility(View.VISIBLE);
            albumRecyclerView.setVisibility(View.GONE);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading(getString(R.string.open_album));
        FastLapApplication.mCurrentContext = mContext;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.share_layout:
                sharePostDialog();
                break;
            case R.id.buy_photos_layout:
                int numberOfPhotos=0;
                photoGrapherIds.clear();
                double totalAmount=0.0;
                for(int i=0;i<images_list.size();i++){
                    if(images_list.get(i).isSelected()){
                        numberOfPhotos++;
                        photoGrapherIds.add(images_list.get(i).getFileId());
                    }
                }
                if(numberOfPhotos==0){
                   Util.showAlertDialog(mContext,getResources().getString(R.string.app_name),"Please select at least one photo");
                   return;
                }
                else if(numberOfPhotos<=2){
                    double price= Integer.parseInt(beanAlbum.getPhotoList().get(0).getPrice());
                    double numberPhoto= Integer.parseInt(beanAlbum.getPhotoList().get(0).getNumberOfPhotos());
                    double onePhotoPrice=price/numberPhoto;
                    totalAmount=numberOfPhotos*onePhotoPrice;
                }
                else {
                    double price= Integer.parseInt(beanAlbum.getPhotoList().get(1).getPrice());
                    double numberPhoto= Integer.parseInt(beanAlbum.getPhotoList().get(1).getNumberOfPhotos());
                    double onePhotoPrice=(price/numberPhoto);
                    totalAmount=(numberOfPhotos*onePhotoPrice);
                }
                Log.e("tag","count "+numberOfPhotos);
                Log.e("tag","count "+totalAmount);
                Intent intent = new Intent(AlbumActivity.this, AddCardDetail.class);
                intent.putExtra(S.amount, totalAmount+"");
                intent.putExtra(S.type, I.ALBUMPAYMET + "");
                startActivityForResult(intent, 1);
                break;
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.add_album:
                Intent intentAlbum = new Intent(mContext, AddAlbumActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(S.data, beanAlbum);
                intentAlbum.putExtras(bundle);
                startActivityForResult(intentAlbum, I.ALBUM_ACTIVITY_REQUEST);
                break;
            case R.id.menu_menu:
                Intent intent2 = new Intent(this, MenuActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                finish();
                break;
        }
    }
    private void sharePostDialog() {
        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(mContext, onImagePickerDialogSelect);
        //chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        chooseImagePickerOptionDialog.changeText();
        // Util.showAlertDialogWithTwoButton(mContext, mContext.getString(R.string.are_you_sure_you_want_to_share_this_post), mContext.getString(R.string.cancel), mContext.getString(R.string.confirm), adapterPosition);
    }
    @Override
    public void OnCameraSelect() {
        authAPI.albumShareByUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), beanAlbum.get_id());
    }
    @Override
    public void OnGallerySelect() {
        onCLickOnFb();

    }
    @Override
    public void onVideoSelect() {

    }
    public void onCLickOnFb() {
        String url=null;
        final int mediaType = beanAlbum.getMediaType();
        if (beanAlbum.getFilesList().size() !=0) {
            if (mediaType == 2){
                url=S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getThumbName();
            }
            else{
                url=S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getFileName();
            }
            shareMediaFacebook(url);
        }
        else {
            Toast.makeText(this, "Media content not found", Toast.LENGTH_SHORT).show();
        }
        /*final int mediaType = beanAlbum.getMediaType();
        if (beanAlbum.getFilesList().size() == 0) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, beanAlbum.getName());
            startActivity(Intent.createChooser(intent, "Share with"));
        } else {
            List<String> permissionNeeds = Arrays.asList("publish_actions");
            LoginManager.getInstance().logInWithPublishPermissions(this, permissionNeeds);
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    try {
                        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.car_img);
                        List<Bitmap> temp = null;
                        if (mediaType == 2)
                            temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getThumbName(), ImageLoader.getInstance().getMemoryCache());
                        else
                            temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + beanAlbum.getFilesList().get(0).getFileName(), ImageLoader.getInstance().getMemoryCache());

                        SharePhoto photo = new SharePhoto.Builder()
                                .setBitmap(*//*temp.get(0)*//*image)
                                .build();

                        ArrayList<SharePhoto> photos = new ArrayList<>();
                        photos.add(photo);
                        SharePhotoContent content = new SharePhotoContent.Builder()
                                .setPhotos(photos)
                                .build();

                        ShareDialog shareDialog = new ShareDialog(AlbumActivity.this);
                        if (ShareDialog.canShow(SharePhotoContent.class)) {
                            shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                        }
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                Toast.makeText(mContext, "share sucessfully", Toast.LENGTH_SHORT).show();
                                Log.d("DashBoard", "====SUCCESS");
                            }

                            @Override
                            public void onCancel() {
                                Log.d("DashBoard", "====CANCEL");
                            }

                            @Override
                            public void onError(FacebookException error) {
                                Log.d("DashBoard", "====ERROR");
                            }
                        });
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Log.e("Tag", error.toString());
                }
            });
        }*/
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(requestCode==1){
                if(data.getExtras()!=null){
                    String token=data.getStringExtra(S.stripeToken);
                    String amount=data.getStringExtra(S.amount);
                    bugPhotoAftergettingToken(amount,token);
                }
            }
            else if (requestCode == I.ALBUM_ACTIVITY_REQUEST) {
                finish();
            }
        }
    }
    private void bugPhotoAftergettingToken(final  String amount,final String token){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("userId",MySharedPreferences.getPreferences(mContext,S.user_id));
            JSONArray jsonElements=new JSONArray();
            for(int i=0;i<photoGrapherIds.size();i++){
                jsonElements.put(photoGrapherIds.get(i));
            }
            jsonObject.put("photoGrapherIds",jsonElements);
            jsonObject.put("token",token);
            jsonObject.put("amount",amount);
            JsonParser jsonParser=new JsonParser();
           JsonObject jsonObject1= (JsonObject) jsonParser.parse(jsonObject.toString());
            //Log.e("tag",""+jsonObject1);
            mAuthAPI.getPurchasePhotoGraphers(mContext,jsonObject1);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    public void getBuyPhotoResponse(String response) {
        JSONObject jsonObject= null;
        try {
            jsonObject = new JSONObject(response.toString());
            setImageToAdapter();
            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
       Log.e("tag","response "+response);
    }

    private void shareMediaFacebook(String url){
        Log.e("tag","url"+url);
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        loginManager = LoginManager.getInstance();
        loginManager.logInWithPublishPermissions(this, permissionNeeds);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult) {
                sharePhotoToFacebook(url);
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception)
            {
                System.out.println("onError");
            }
        });
    }
    private void sharePhotoToFacebook(String url){
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.car_img);
        ShareDialog shareDialog = new ShareDialog(AlbumActivity.this);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(url))
                    .build();
            shareDialog.show(linkContent);
        }
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(AlbumActivity.this, "Media share sucessfully.", Toast.LENGTH_SHORT).show();
                Log.e("DashBoard", "====SUCCESS");
            }

            @Override
            public void onCancel() {
                Log.e("DashBoard", "====CANCEL");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("DashBoard", "====ERROR");
            }
        });

    }
}

