package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.adapter.settings.SettingBlockUserSuggestionListAdapter;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.ActivityCommunicator;
import com.os.fastlap.delegates.FragmentCommunicator;
import com.os.fastlap.delegates.FragmentCommunicatorTwo;
import com.os.fastlap.fragment.AppUserFragment;
import com.os.fastlap.fragment.ContactFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by riyazudinp on 6/17/2017.
 */

public class AllUsersActivity extends BaseActivity implements ActivityCommunicator, SettingBlockUserSuggestionListAdapter.OnItemClickListener {

    private ImageView back_iv;
    private EditText edtSearch;
    private TextView no_data_tv;
    private android.support.design.widget.TabLayout TabLayout;
    private Context context;
    private FragmentManager mFragmentManager;
    private ViewPagerAdapter adapter;
    private ViewPager viewpager;
    public FragmentCommunicator fragmentCommunicator;
    public FragmentCommunicatorTwo fragmentCommunicatorTwo;
    public static int pos = 0;
    private RecyclerView recycler_view;
    private SettingBlockUserSuggestionListAdapter spinDialogAdapter;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    AuthAPI authAPI;
    String TAG = AllUsersActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_users_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);

        initView();
        addEventListeners();
        Util.changeTabsFont(TabLayout, context);
        // authAPI.getAppUser(context,MySharedPreferences.getPreferences(context, S.user_id));
        authAPI.myChatRooms(context, MySharedPreferences.getPreferences(context, S.user_id));
    }


    private void initView() {

        mFragmentManager = getSupportFragmentManager();
        back_iv = (ImageView) findViewById(R.id.back_iv);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        TabLayout = (android.support.design.widget.TabLayout) findViewById(R.id.TabLayout);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        setUpViewPager(viewpager);

        TabLayout.setupWithViewPager(viewpager);
        mMyFriendsLists = new ArrayList<>();
        mMyFriendsLists.clear();

        spinDialogAdapter = new SettingBlockUserSuggestionListAdapter(context, mMyFriendsLists, false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recycler_view.setAdapter(spinDialogAdapter);
        spinDialogAdapter.addMyFriendsListAll(mMyFriendsLists);
        spinDialogAdapter.setItemClickListener(this);
    }

    private void addEventListeners() {
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                spinDialogAdapter.filter(arg0.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                edtSearch.setText("");
                hideKeyboard();
                pos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private void setUpViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(mFragmentManager);
        adapter.addFragment(new AppUserFragment(context), getString(R.string.app_user));
        adapter.addFragment(new ContactFragment(context), getString(R.string.contacts));
        adapter.getItem(viewPager.getCurrentItem());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void passDataToActivity(String someValue) {

    }

    /* myFriendList webservice response */
    public void myFriendListWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                mMyFriendsLists.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.getString(S.channelId));
                    profileMyFriendsList.setUserId(jsonObject1.getString(S.id));
                    profileMyFriendsList.setStatus(jsonObject1.getString(S.type));

                    PersonalInfo personalInfo = new PersonalInfo();
                    personalInfo.setFirstName(jsonObject1.getString(S.name));
                    personalInfo.setImage(jsonObject1.getString(S.image));
                    profileMyFriendsList.setPersonalInfo(personalInfo);

                    mMyFriendsLists.add(profileMyFriendsList);
                }
                spinDialogAdapter.addMyFriendsListAll(mMyFriendsLists);
                spinDialogAdapter.notifyDataSetChanged();

                if(mMyFriendsLists.size()>0)
                {
                    recycler_view.setVisibility(View.VISIBLE);
                    no_data_tv.setVisibility(View.GONE);
                }
                else
                {
                    recycler_view.setVisibility(View.GONE);
                    no_data_tv.setVisibility(View.VISIBLE);
                }
                //showMyFriendsList();
            } else {
                recycler_view.setVisibility(View.GONE);
                no_data_tv.setVisibility(View.VISIBLE);

                //Util.showAlertDialog(context, getString(R.string.alert), msg);
            }
        } catch (Exception e) {
            recycler_view.setVisibility(View.GONE);
            no_data_tv.setVisibility(View.VISIBLE);
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(S.friendId, mMyFriendsLists.get(position).getId());
        intent.putExtra(S.userId, mMyFriendsLists.get(position).getUserId());
        intent.putExtra(S.username, mMyFriendsLists.get(position).getPersonalInfo().getFirstName());
        intent.putExtra(S.image, mMyFriendsLists.get(position).getPersonalInfo().getImage());
        intent.putExtra(S.CHAT_TYPE, mMyFriendsLists.get(position).getStatus());
        startActivity(intent);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        public final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Fragment currentFragment = adapter.mFragmentList.get(1);
        if (currentFragment instanceof ContactFragment) {
            ContactFragment contactFragment = (ContactFragment) currentFragment;
            contactFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.CHAT_SCREEN);
        FastLapApplication.mCurrentContext = context;
    }
}
