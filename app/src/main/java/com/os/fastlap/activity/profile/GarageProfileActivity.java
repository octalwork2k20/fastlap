package com.os.fastlap.activity.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.adapter.profile.ClothProfileAdapter;
import com.os.fastlap.adapter.profile.GarageProfileAdapter;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.beans.clothmodals.ClothColorIdModal;
import com.os.fastlap.beans.clothmodals.ClothModelIdModal;
import com.os.fastlap.beans.clothmodals.ClothModelListModal;
import com.os.fastlap.beans.clothmodals.ClothNameIdModal;
import com.os.fastlap.beans.clothmodals.ClothYearIdModal;
import com.os.fastlap.beans.gragemodals.VehicleBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTyreBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTyreModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleVersionIdModal;
import com.os.fastlap.beans.gragemodals.VehicleYearIdModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.ButtonPlayBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.os.fastlap.R.id.base_toggle_icon;
import static com.os.fastlap.R.id.img_bg;
import static com.os.fastlap.R.id.txt_username;
import static com.os.fastlap.R.id.user_icon;

/**
 * Created by anandj on 7/14/2017.
 */

public class GarageProfileActivity extends BaseActivity implements View.OnClickListener,GarageProfileAdapter.OnItemClickListener,ClothProfileAdapter.OnItemClickListener {

    private AppCompatImageView toggle_icon;
    private RecyclerView recyclerview_garage;
    private ImageView imgBg;
    private TextView txtUsername,txtEmail;
    private CircleImageView userIcon;
    private TextView addVehicleBtn;
    private TextView no_data_tv;
    private Context mContext;
    private GarageProfileAdapter garageProfileAdapter;
    private ArrayList<BeanVehicleList> mGarageList;
    AuthAPI authAPI;
    private String TAG = GarageProfileActivity.class.getSimpleName();
    private View vehicle_layout;
    private View clothing_layout;
    private RelativeLayout parent_about;
    private RelativeLayout parent_clothing;
    private ImageView img_arrow_about;
    private ImageView img_arrow_clothing;
    private ClothProfileAdapter mClothProfileAdapter;
    private ArrayList<ClothModelListModal> mClothList;
    private RecyclerView recycler_view_clothing;
    private Button add_cloth_btn;
    TextView menu_menu;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_garage_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();

    }


    private void initView() {
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        recycler_view_clothing = (RecyclerView) findViewById(R.id.recycler_view_clothing);

        no_data_tv = findViewById(R.id.no_data_tv);
        toggle_icon = (AppCompatImageView) findViewById(base_toggle_icon);
        recyclerview_garage = (RecyclerView) findViewById(R.id.recyclerview_garage);
        imgBg = (ImageView) findViewById(img_bg);
        txtUsername = (TextView) findViewById(txt_username);
        txtEmail=(TextView)findViewById(R.id.txt_email);
        userIcon = (CircleImageView) findViewById(user_icon);
        addVehicleBtn = (ButtonPlayBold) findViewById(R.id.add_vehicle_btn);
        parent_about = (RelativeLayout) findViewById(R.id.parent_about);

        vehicle_layout = findViewById(R.id.vehicle_layout);
        clothing_layout = findViewById(R.id.clothing_layout);
        img_arrow_about = (ImageView) findViewById(R.id.img_arrow_about);
        parent_clothing = (RelativeLayout) findViewById(R.id.parent_clothing);
        img_arrow_clothing = (ImageView) findViewById(R.id.img_arrow_clothing);
        add_cloth_btn = (Button) findViewById(R.id.add_cloth_btn);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerview_garage.setLayoutManager(layoutManager);
        mGarageList = new ArrayList<>();
        garageProfileAdapter = new GarageProfileAdapter(mContext, mGarageList);
        recyclerview_garage.setAdapter(garageProfileAdapter);
        garageProfileAdapter.setOnItemClickListener(this);
        recyclerview_garage.setNestedScrollingEnabled(false);

        mClothList = new ArrayList<>();
        mClothProfileAdapter = new ClothProfileAdapter(mContext, mClothList);
        RecyclerView.LayoutManager mClothLayoutManager = new LinearLayoutManager(mContext);
        recycler_view_clothing.setLayoutManager(mClothLayoutManager);
        recycler_view_clothing.setAdapter(mClothProfileAdapter);
        mClothProfileAdapter.setOnItemClickListener(this);

        clickListner();
        setProfileData();

        if (ProfileActivity.profileUserId.compareToIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)) == 0) {
            addVehicleBtn.setVisibility(View.VISIBLE);
            add_cloth_btn.setVisibility(View.VISIBLE);
        } else {
            addVehicleBtn.setVisibility(View.GONE);
            add_cloth_btn.setVisibility(View.GONE);
        }
        openAbount();
    }

    private void clickListner() {
        menu_menu.setOnClickListener(this);
        toggle_icon.setOnClickListener(this);
        addVehicleBtn.setOnClickListener(this);
        parent_clothing.setOnClickListener(this);
        parent_about.setOnClickListener(this);
        add_cloth_btn.setOnClickListener(this);

    }

    private void setProfileData() {
        try {

            if(!ProfileActivity.profileUserCoverImage.equalsIgnoreCase("img/cover.png")){
                Util.setImageLoader(S.IMAGE_BASE_URL + ProfileActivity.profileUserCoverImage, imgBg, getApplicationContext());
            }
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + ProfileActivity.profileUserProfileImage, userIcon, Util.getImageLoaderOption(mContext));
            txtUsername.setText(ProfileActivity.profileUserName);
            txtEmail.setText("("+ProfileActivity.profileEmail+")");
        }
        catch (Exception e){e.printStackTrace();
        }

    }


    @Override
    public void onEditClothList(int position) {
    }
    @Override
    public void onDeleteClothList(int position) {
        showDialogForDeleteItem(mContext, getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.cancel), getString(R.string.delete_small), position,false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading(getString(R.string.garage));
        getVehicleList();
        FastLapApplication.mCurrentContext = mContext;
        authAPI.userClothList(mContext, ProfileActivity.profileUserId);
    }

    public void getVehicleList() {
        authAPI.getVehicleList(mContext, ProfileActivity.profileUserId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.add_vehicle_btn:
                Intent intent = new Intent(mContext, AddVehicleActivity.class);
                intent.putExtra("class_name","GarageProfileActivity");
                mContext.startActivity(intent);
                break;
            case R.id.parent_clothing:
                if (clothing_layout.getVisibility() == View.VISIBLE) {
                    clothing_layout.setVisibility(View.GONE);
                    img_arrow_clothing.setImageResource(R.drawable.new_right_back_red);
                }
                else {
                    img_arrow_clothing.setImageResource(R.drawable.new_down_back_red);
                    img_arrow_about.setImageResource(R.drawable.new_right_back_red);
                    vehicle_layout.setVisibility(View.GONE);
                    clothing_layout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.parent_about:
                openAbount();
                break;
            case R.id.add_cloth_btn:
                Util.startNewActivity(GarageProfileActivity.this, AddClothesActivity.class, false);
                break;
            case R.id.menu_menu:
                Intent intent2 = new Intent(this, MenuActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                finish();
                break;


        }
    }
    private void openAbount() {
        if (vehicle_layout.getVisibility() == View.VISIBLE) {
            vehicle_layout.setVisibility(View.GONE);
            img_arrow_about.setImageResource(R.drawable.new_right_back_red);
        } else {
            img_arrow_about.setImageResource(R.drawable.new_down_back_red);
            img_arrow_clothing.setImageResource(R.drawable.new_right_back_red);
            vehicle_layout.setVisibility(View.VISIBLE);
            clothing_layout.setVisibility(View.GONE);

        }
    }

    // show Vehicle list webservice response
    public void getVehicleListResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            mGarageList.clear();
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                if (jsonArray.length() == 0) {
                    recyclerview_garage.setVisibility(View.GONE);
                    no_data_tv.setVisibility(View.VISIBLE);
                } else {
                    recyclerview_garage.setVisibility(View.VISIBLE);
                    no_data_tv.setVisibility(View.GONE);
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {

                        BeanVehicleList beanVehicleList = new BeanVehicleList();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        beanVehicleList.set_id(jsonObject1.getString(S._id));
                        beanVehicleList.setDescription(jsonObject1.getString(S.description));

                        JSONObject jsonObject2 = jsonObject1.getJSONObject(S.vehicleTypeId);
                        VehicleTypeIdModal vehicleTypeIdModal = new VehicleTypeIdModal();
                        vehicleTypeIdModal.set_id(jsonObject2.getString(S._id));
                        vehicleTypeIdModal.setName(jsonObject2.getString(S.name));
                        beanVehicleList.setVehicleTypeIdModal(vehicleTypeIdModal);

                        JSONObject jsonObject3 = jsonObject1.getJSONObject(S.vehicleBrandId);
                        VehicleBrandIdModal vehicleBrandIdModal = new VehicleBrandIdModal();
                        vehicleBrandIdModal.set_id(jsonObject3.getString(S._id));
                        vehicleBrandIdModal.setVehicleBrandName(jsonObject3.getString(S.vehicleBrandName));
                        vehicleBrandIdModal.setVehicleTypeId(jsonObject3.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleBrandIdModal(vehicleBrandIdModal);

                        JSONObject jsonObject4 = jsonObject1.getJSONObject(S.vehicleModelId);
                        VehicleModelIdModal vehicleModelIdModal = new VehicleModelIdModal();
                        vehicleModelIdModal.set_id(jsonObject4.getString(S._id));
                        vehicleModelIdModal.setVehicleTypeId(jsonObject4.getString(S.vehicleTypeId));
                        vehicleModelIdModal.setVehicleBrandId(jsonObject4.getString(S.vehicleBrandId));
                        vehicleModelIdModal.setVehicleModelName(jsonObject4.getString(S.vehicleModelName));
                        beanVehicleList.setVehicleModelIdModal(vehicleModelIdModal);

                        JSONObject jsonObject5 = jsonObject1.getJSONObject(S.vehicleVersionId);
                        VehicleVersionIdModal vehicleVersionIdModal = new VehicleVersionIdModal();
                        vehicleVersionIdModal.set_id(jsonObject5.getString(S._id));
                        vehicleVersionIdModal.setVehicleVersionName(jsonObject5.getString(S.vehicleVersionName));
                        vehicleVersionIdModal.setVehicleModelId(jsonObject5.getString(S.vehicleModelId));
                        vehicleVersionIdModal.setVehicleBrandId(jsonObject5.getString(S.vehicleBrandId));
                        vehicleVersionIdModal.setVehicleTypeId(jsonObject5.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleVersionIdModal(vehicleVersionIdModal);

                        JSONObject jsonObject6 = jsonObject1.getJSONObject(S.vehicleTyreBrandId);
                        VehicleTyreBrandIdModal vehicleTyreBrandIdModal = new VehicleTyreBrandIdModal();
                        vehicleTyreBrandIdModal.set_id(jsonObject6.getString(S._id));
                        vehicleTyreBrandIdModal.setBrandName(jsonObject6.getString(S.brandName));
                        vehicleTyreBrandIdModal.setVehicleTypeId(jsonObject6.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleTyreBrandIdModal(vehicleTyreBrandIdModal);

                        JSONObject jsonObject7 = jsonObject1.getJSONObject(S.vehicleTyreModelId);
                        VehicleTyreModelIdModal vehicleTyreModelIdModal = new VehicleTyreModelIdModal();
                        vehicleTyreModelIdModal.set_id(jsonObject7.getString(S._id));
                        vehicleTyreModelIdModal.setVehicleTypeId(jsonObject7.getString(S.vehicleTypeId));
                        vehicleTyreModelIdModal.setVehichlemodelName(jsonObject7.getString(S.vehichleTyreModelName));
                        vehicleTyreModelIdModal.setVehicleTyreBrandId(jsonObject7.getString(S.vehicleTyreBrandId));
                        beanVehicleList.setVehicleTyreModelIdModal(vehicleTyreModelIdModal);

                        JSONObject jsonObject8 = jsonObject1.getJSONObject(S.vehicleYearId);
                        VehicleYearIdModal vehicleYearIdModal = new VehicleYearIdModal();
                        vehicleYearIdModal.set_id(jsonObject8.getString(S._id));
                        vehicleYearIdModal.setVehicleYear(jsonObject8.getString(S.vehicleYear));
                        vehicleYearIdModal.setVehicleVersionId(jsonObject8.getString(S.vehicleVersionId));
                        vehicleYearIdModal.setVehicleModelId(jsonObject8.getString(S.vehicleModelId));
                        vehicleYearIdModal.setVehicleBrandId(jsonObject8.getString(S.vehicleBrandId));
                        vehicleYearIdModal.setVehicleTypeId(jsonObject8.getString(S.vehicleTypeId));
                        beanVehicleList.setVehicleYearIdModal(vehicleYearIdModal);


                        beanVehicleList.setUploadedLapCount(jsonObject1.getString(S.userLapCount));
                        beanVehicleList.setBestRanking(jsonObject1.getString(S.bestRanking));
                        beanVehicleList.setBestSpeed(jsonObject1.getString(S.bestSpeed));
                        beanVehicleList.setTrackVisitedCount(jsonObject1.getString(S.userTrackRoundCount));


                        JSONArray imagesJarray = new JSONArray();
                        imagesJarray = jsonObject1.getJSONArray(S.userVehicleImage);
                        ArrayList<PagerBean> imageArray = new ArrayList<>();

                        for (int j = 0; j < imagesJarray.length(); j++)
                        {
                            imageArray.add(new PagerBean(null, null, null, false, true, imagesJarray.getJSONObject(j).getString(S.fileName), false, imagesJarray.getJSONObject(j).getString(S._id)));
                        }

                        beanVehicleList.setImagesList(imageArray);
                        mGarageList.add(beanVehicleList);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                recyclerview_garage.setVisibility(View.GONE);
                no_data_tv.setVisibility(View.VISIBLE);
            }

            ///   Util.showAlertDialog(mContext, getString(R.string.alert), msg);

            garageProfileAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Delete Vehicle list webservice response
    public void deleteVehicleListResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, "");
                // getVehicleList();
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // edit vehicle list webservice calling here
    @Override
    public void onEditVehicleList(int position) {
        Intent intent = new Intent(mContext, AddVehicleActivity.class);
        intent.putExtra("class_name","GarageProfileActivity");
        intent.putExtra(S.vehicleModal, mGarageList.get(position));
        startActivity(intent);
    }

    // delete vehicle list webservice calling here
    @Override
    public void onDeleteVehicleList(int position) {
        showDialogForDeleteItem(mContext, getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.cancel), getString(R.string.delete_small), position,true);

        //Util.showAlertDialogWithTwoButton(mContext, getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.cancel), getString(R.string.delete_small), position);
        //authAPI.deleteVechicleItem(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mGarageList.get(position).get_id());
    }


    private void showDialogForDeleteItem(final Context context, final String message, String positive_button, String negative_button, final int position,final boolean isVehhicle){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.setCancelable(false);
        TextView txt_msg, cancel_btn, logout_btn;
        txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        cancel_btn = (TextView) dialog.findViewById(R.id.cancel_btn);
        logout_btn = (TextView) dialog.findViewById(R.id.logout_btn);
        txt_msg.setText(message);

        cancel_btn.setText(positive_button);
        logout_btn.setText(negative_button);

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(isVehhicle){
                    callDeleteVechicleListItem(position);
                }
                else {
                    callDeleteClothListItem(position);
                }
            }
        });


        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
        dialog.show();
    }


    // user response delete confirm dialog
    // if user click on yes than call delete vehicle webservice
    public void callDeleteVechicleListItem(int position) {
        authAPI.deleteVechicleItem(mContext, ProfileActivity.profileUserId, mGarageList.get(position).get_id());
    }
    public void callDeleteClothListItem(int position) {
        authAPI.deleteClothItem(mContext, ProfileActivity.profileUserId, mClothList.get(position).get_id());
    }
    public void getDeleteClothResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, "");
                authAPI.userClothList(mContext, ProfileActivity.profileUserId);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onItemVehicleList(int position) {

    }


    // user cloth list webservice response
    public void getClothList(String response) {
        mClothList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ClothModelListModal clothModelListModal = new ClothModelListModal();
                    clothModelListModal.set_id(jsonObject1.getString(S._id));
                    clothModelListModal.setClothName(jsonObject1.getString(S.clothName));
                    clothModelListModal.setClothImage(jsonObject1.getString(S.images1));

                    JSONObject jsonObject2 = jsonObject1.getJSONObject(S.clothBrandId);
                    ClothNameIdModal clothNameIdModal = new ClothNameIdModal();
                    clothNameIdModal.set_id(jsonObject2.getString(S._id));
                    clothNameIdModal.setName(jsonObject2.getString(S.name));
                    clothModelListModal.setClothNameIdModal(clothNameIdModal);

                    JSONObject jsonObject3 = jsonObject1.has(S.vehicleTypeId)?jsonObject1.getJSONObject(S.vehicleTypeId):null;
                    if(jsonObject3!=null){
                        VehicleTypeIdModal vehicleTypeIdModal=new VehicleTypeIdModal();
                        vehicleTypeIdModal.set_id(jsonObject3.getString(S._id));
                        vehicleTypeIdModal.setName(jsonObject3.getString(S.name));
                        clothModelListModal.setVehicleTypeIdModal(vehicleTypeIdModal);
                    }
                    ClothModelIdModal clothModelIdModal = new ClothModelIdModal();
                    clothModelIdModal.setName(jsonObject1.getString(S.clothModelName));
                    clothModelListModal.setClothModelIdModal(clothModelIdModal);

                    ClothYearIdModal clothYearIdModal = new ClothYearIdModal();
                    clothYearIdModal.setYears(jsonObject1.getString(S.clothYearName));
                    clothModelListModal.setClothYearIdModal(clothYearIdModal);

                    ClothColorIdModal clothColorIdModal = new ClothColorIdModal();
                    clothColorIdModal.setName(jsonObject1.getString(S.clothColorName));
                    clothModelListModal.setClothColorIdModal(clothColorIdModal);
                    mClothList.add(clothModelListModal);

                }
                recycler_view_clothing.setVisibility(View.VISIBLE);
                mClothProfileAdapter.notifyDataSetChanged();
            }
            else {
                recycler_view_clothing.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        }
    }
}
