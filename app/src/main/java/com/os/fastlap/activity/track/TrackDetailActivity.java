package com.os.fastlap.activity.track;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.community.EventActivity;
import com.os.fastlap.activity.profile.FriendsProfileActivity;
import com.os.fastlap.activity.profile.GalleryProfileActivity;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by anandj on 7/11/2017.
 */

public class TrackDetailActivity extends BaseActivity implements View.OnClickListener {

    private Context mContext;
    private AppCompatImageView toggle_icon;
    private TextView toolbarheading;
    private ImageView img_bg;
    private ImageView img_addfrd;
    private ImageView img_arrow_info;
    private ImageView img_arrow_diary;
    private ImageView img_arrow_event;
    private ImageView img_arrow_laps;
    private ImageView img_arrow_gallery;
    private RelativeLayout rr;
    private CircleImageView user_icon;
    private CoordinatorLayout coordinator_layout;
    private ImageView img_diary;
    private ImageView trackHomeIconIv;
    private ImageView trackFavIconIv;
    private TextView txt_diary;
    private RelativeLayout parent_diary;
    private ImageView img_info;
    private TextView txt_info;
    private RelativeLayout parent_info;
    private ImageView img_event;
    private TextView txt_event;
    private RelativeLayout parent_event;
    private ImageView img_laps;
    private TextView txt_laps;
    private RelativeLayout parent_laps;
    private ImageView img_gallery;
    private TextView txt_gallery;
    private RelativeLayout parent_gallery;
    private TextView txt_username,txtEmail;
    AuthAPI authAPI;
    Bundle bundle;
    private String TAG = TrackDetailActivity.class.getSimpleName();
    String friendStatus;

    public static String trackId = "";
    public static String trackName = "";
    public static String trackEmail = "";
    public static String trackImage = "";
    public static String trackCoverImage = "";

    int homeStatus = 0;
    int favStatus = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_detail_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();
        bundle = getIntent().getExtras();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.track));
        FastLapApplication.mCurrentContext = mContext;

        if (bundle != null) {
            if (bundle.getInt(S.page) == I.DASHBOARD_SCREEN) {
                setBottomMenu(I.DASHBOARD_SCREEN);
            } else if (bundle.getInt(S.page) == I.MENU_SCREEN) {
                setBottomMenu(I.MENU_SCREEN);
            }

            trackId = bundle.getString(S.user_id);
            setProfileUser();
        }
    }

    private void setProfileUser() {
        img_addfrd.setVisibility(View.GONE);
        friendStatus = bundle.getString("status");
        if (friendStatus != null) {
            if (friendStatus.compareTo("0") == 0) {
                img_addfrd.setVisibility(View.VISIBLE);
                img_addfrd.setImageResource(R.mipmap.adduser_white);
            } else {
                img_addfrd.setVisibility(View.GONE);
            }
        }

        authAPI.getTrackInfo(mContext, trackId, MySharedPreferences.getPreferences(mContext, S.user_id));
    }

    private void setProfileData() {

        if(!trackImage.equalsIgnoreCase("")&&trackImage!=null)
            Util.setImageLoader(S.IMAGE_BASE_URL + TrackDetailActivity.trackImage, user_icon, getApplicationContext());
        else
            user_icon.setImageResource(R.drawable.nopic);

        Util.setImageLoader(S.IMAGE_BASE_URL + TrackDetailActivity.trackCoverImage, img_bg, getApplicationContext());


        txt_username.setText(trackName);
        txtEmail.setText("("+ trackEmail+")");


        if (homeStatus == 0) {
            trackHomeIconIv.setImageResource(R.mipmap.homeplus);
        } else {
            trackHomeIconIv.setImageResource(R.mipmap.homeminus);
        }

        if (favStatus == 0) {
            trackFavIconIv.setImageResource(R.mipmap.starplus);
        } else {
            trackFavIconIv.setImageResource(R.mipmap.starminus);
        }
    }

    private void initView() {
        txt_username = (TextView) findViewById(R.id.txt_username);
        txtEmail=(TextView)findViewById(R.id.txt_email);
        img_addfrd = (ImageView) findViewById(R.id.img_addfrd);
        toggle_icon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        toolbarheading = (TextView) findViewById(R.id.toolbarheading);
        img_bg = (ImageView) findViewById(R.id.img_bg);
        rr = (RelativeLayout) findViewById(R.id.rr);
        user_icon = (CircleImageView) findViewById(R.id.user_icon);
        coordinator_layout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        img_diary = (ImageView) findViewById(R.id.img_diary);
        txt_diary = (TextView) findViewById(R.id.txt_diary);
        parent_diary = (RelativeLayout) findViewById(R.id.parent_diary);
        img_info = (ImageView) findViewById(R.id.img_info);
        txt_info = (TextView) findViewById(R.id.txt_info);
        parent_info = (RelativeLayout) findViewById(R.id.parent_info);
        img_event = (ImageView) findViewById(R.id.img_event);
        txt_event = (TextView) findViewById(R.id.txt_event);
        parent_event = (RelativeLayout) findViewById(R.id.parent_event);
        img_laps = (ImageView) findViewById(R.id.img_laps);
        txt_laps = (TextView) findViewById(R.id.txt_laps);
        parent_laps = (RelativeLayout) findViewById(R.id.parent_laps);
        img_gallery = (ImageView) findViewById(R.id.img_gallery);
        txt_gallery = (TextView) findViewById(R.id.txt_gallery);

        parent_gallery = (RelativeLayout) findViewById(R.id.parent_gallery);

        img_arrow_info = (ImageView) findViewById(R.id.img_arrow_info);
        img_arrow_diary = (ImageView) findViewById(R.id.img_arrow_diary);
        img_arrow_event = (ImageView) findViewById(R.id.img_arrow_event);
        img_arrow_laps = (ImageView) findViewById(R.id.img_arrow_laps);
        img_arrow_gallery = (ImageView) findViewById(R.id.img_arrow_gallery);
        trackFavIconIv = (ImageView) findViewById(R.id.track_fav_icon_iv);
        trackHomeIconIv = (ImageView) findViewById(R.id.track_home_icon_iv);

        setOnclickListener();

        trackFavIconIv.setVisibility(View.VISIBLE);
        trackHomeIconIv.setVisibility(View.VISIBLE);
    }

    private void setOnclickListener() {
        parent_info.setOnClickListener(this);
        parent_diary.setOnClickListener(this);
        parent_event.setOnClickListener(this);
        parent_laps.setOnClickListener(this);
        parent_gallery.setOnClickListener(this);
        toggle_icon.setOnClickListener(this);
        img_addfrd.setOnClickListener(this);
        trackFavIconIv.setOnClickListener(this);
        trackHomeIconIv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_addfrd:
                /*
                 if user want to add friends
                     than status =  0
                 if user want to accepts friends request
                         than status  = 1
                 if user want to reject friends request
                         than status = 2
                */

                if (friendStatus.compareTo("0") == 0) {
                    String status = "0";
                    authAPI.userFriend(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), trackId, status, "");
                } else if (friendStatus.compareTo("1") == 0) {
                   /* String status = "0";
                    authAPI.userFriend(mContext, "", MySharedPreferences.getPreferences(mContext, S.user_id), status,"");*/
                }
                break;
            case R.id.parent_info:
                setSelectedInfo();
                Util.startNewActivity(TrackDetailActivity.this, TrackInfoActivity.class, true);
                break;
            case R.id.parent_diary:
                setSelectedDiary();
                Util.startNewActivity(TrackDetailActivity.this, TrackDiaryActivity.class, true);
                break;
            case R.id.parent_event:
                setSelectedevent();
                Intent intent = new Intent(mContext, EventActivity.class);
                intent.putExtra(S.type, I.TRACKEVENT);
                intent.putExtra(S._id, trackId);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);

                break;
            case R.id.parent_laps:
                setSelectedLaps();

                Intent intentTrackTimeLaps = new Intent(mContext, TrackTimeLaps.class);
                intentTrackTimeLaps.putExtra(S.trackId, trackId);
                startActivity(intentTrackTimeLaps);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);

               // Util.startNewActivity(TrackDetailActivity.this, TrackTimeLaps.class, true);
                break;
            case R.id.parent_friends:
                setSelectedFriends();
                Util.startNewActivity(TrackDetailActivity.this, FriendsProfileActivity.class, true);
                break;
            case R.id.parent_gallery:
                setSelectedGallery();
                Intent intent1 = new Intent(mContext, GalleryProfileActivity.class);
                intent1.putExtra(S.type, I.TRACKGALLERY);
                startActivity(intent1);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.track_fav_icon_iv:
                if (favStatus == 1) {
                    authAPI.changeFavTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), trackId, "", "0");
                } else {
                    authAPI.changeFavTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), trackId, "", "1");
                }

                break;
            case R.id.track_home_icon_iv:
                if (homeStatus == 1)
                {
                    authAPI.changeHomeTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), trackId, "0");
                } else {
                    authAPI.changeHomeTrackStatus(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), trackId, "1");
                }

                break;
        }
    }

    private void setSelectedInfo() {
        /*parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_info.setImageResource(R.mipmap.infoselect);
        img_arrow_info.setImageResource(R.drawable.arrowright_white);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_laps.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_laps.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_laps.setImageResource(R.mipmap.laps_unselect);
        img_arrow_laps.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.friends_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);*/

    }

    private void setSelectedDiary() {
        /*parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_diary.setImageResource(R.mipmap.diaryselect);
        img_arrow_diary.setImageResource(R.drawable.arrowright_white);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_laps.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_laps.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_laps.setImageResource(R.mipmap.laps_unselect);
        img_arrow_laps.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.friends_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);*/

    }

    private void setSelectedevent() {
        /*parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_event.setImageResource(R.mipmap.event_select);
        img_arrow_event.setImageResource(R.drawable.arrowright_white);

        parent_laps.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_laps.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_laps.setImageResource(R.mipmap.laps_unselect);
        img_arrow_laps.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.friends_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);*/
    }

    private void setSelectedLaps() {
        /*parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_laps.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_laps.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_laps.setImageResource(R.mipmap.laps_select);
        img_arrow_laps.setImageResource(R.drawable.arrowright_white);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.friends_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);*/

    }

    private void setSelectedFriends() {
        /*parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_laps.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_laps.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_laps.setImageResource(R.mipmap.laps_unselect);
        img_arrow_laps.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_gallery.setImageResource(R.mipmap.friends_select);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);*/
    }

    private void setSelectedGallery() {
        /*parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_laps.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_laps.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_laps.setImageResource(R.mipmap.laps_unselect);
        img_arrow_laps.setImageResource(R.mipmap.arrowright);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.gallery_select);
        img_arrow_gallery.setImageResource(R.drawable.arrowright_white);*/
    }

    /* track info webservice response */
    public void trackInfoResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                trackName = jsonObject1.has(S.name)?jsonObject1.getString(S.name):"";
                trackEmail = jsonObject1.getJSONObject(S.contactDetails).getString(S.email);
                trackCoverImage = jsonObject1.has(S.coverImage)?jsonObject1.getString(S.coverImage):"";
                trackImage = jsonObject1.has(S.image)?jsonObject1.getString(S.image):"";
                homeStatus = jsonObject1.getInt(S.home);
                favStatus = jsonObject1.getInt(S.favorite);

                setProfileData();

            } /*else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // unHome track webservice response
    public void trackHomeStatus(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                setProfileUser();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    // unfavorite track webservice response
    public void trackManageStatus(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                setProfileUser();
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
