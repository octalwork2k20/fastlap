package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by abhinava on 2/25/2016.
 */
public class OTPActivity extends AppCompatActivity implements View.OnClickListener {
    Context context;
    EditText otp_et;
    TextView verify_btn;
    LinearLayout regenrate_otp;
    String verify_otp = "";
    String usertype = "";
    ImageView toggle_icon;
    JSONObject sign_up_data;
    public static String TAG = "OTPActivity.java";
    AuthAPI authAPI;
    TextViewPlayBold remainigTime;
    TextViewPlayBold txt_regenerate_otp;
    CountDownTimer countDownTimer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        otp_et = (EditText) findViewById(R.id.edt_otp);
        verify_btn = (TextView) findViewById(R.id.btn_submit);
        regenrate_otp = (LinearLayout) findViewById(R.id.regenrate_otp);
        toggle_icon = (ImageView) findViewById(R.id.toggle_icon);
        remainigTime=(TextViewPlayBold) findViewById(R.id.remainingTimer);
        txt_regenerate_otp=(TextViewPlayBold)findViewById(R.id.txt_regenerate_otp);
        verify_btn.setOnClickListener(this);
        regenrate_otp.setOnClickListener(this);
        toggle_icon.setOnClickListener(this);
        retrieveOtp();
    }

    private void retrieveOtp() {
        try {
            txt_regenerate_otp.setTextColor(getResources().getColor(R.color.gray_text_color));
            regenrate_otp.setEnabled(false);
            sign_up_data = new JSONObject(getIntent().getStringExtra(S.sign_up_data));
            setOtp(sign_up_data.getString(S.otp));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void startTimer(){
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_regenerate_otp.setTextColor(getResources().getColor(R.color.gray_text_color));
                regenrate_otp.setEnabled(false);
                remainigTime.setText("00:00:"+ millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                txt_regenerate_otp.setTextColor(getResources().getColor(R.color.colorPrimary));
                regenrate_otp.setEnabled(true);
                remainigTime.setText("");
            }

        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                if (Validation.otpScreenValidation(otp_et, verify_otp, verify_btn, context)) {
                    try {
                        authAPI.verifyOtp(context, sign_up_data.getString(S._id), verify_otp);
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                }
                break;
            case R.id.toggle_icon:
                onBackPressed();
                break;
            case R.id.regenrate_otp:
                try {
                    authAPI.resendOtp(context, sign_up_data.getString(S._id));
                } catch (JSONException e) {
                    Log.e(TAG, e.toString());
                }
                break;
        }
    }

    // opt verify api response
    public void getResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                finish();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // resend otp webservice response
    public void getResendOtpResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = dataJson.getJSONObject(0);
                startTimer();
                setOtp(jsonObject1.getString(S.otp));
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void setOtp(String otp) {
        startTimer();
        verify_otp = otp;
        otp_et.setText(verify_otp);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
