package com.os.fastlap.activity.community;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.beans.dashboardmodals.LikeUserBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.fragment.community.InfoFragment;
import com.os.fastlap.fragment.community.PilotsFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/20/2017.
 */

public class CommunityMoreInfoActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    private AppCompatImageView baseToggleIcon;
    private CoordinatorLayout htabMaincontent;
    private AppBarLayout htabAppbar;
    private CoordinatorLayout coordinatorLayout;
    private ImageView imgBg;
    private TextView txtName;
    private Button participateBtn;
    private ImageView privacyImg;
    private TextView privacyTv;
    private ImageView approvedIv;
    private TextView approvedTv;
    private LinearLayout shareLl;
    private CircleImageView userIcon;
    ViewPager viewpager;
    private TabLayout tabHost;
    private ViewPagerAdapter viewPagerAdapter;
    public static ArrayList<LikeUserBean> member_list;
    EventsBean eventsBean = new EventsBean();
    AuthAPI authAPI;
    String eventId = "";
    private String TAG = CommunityMoreInfoActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_fragment_event_moreinfo);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();

        eventId = getIntent().getStringExtra(S._id);

        authAPI.getEventMembersList(context, MySharedPreferences.getPreferences(context, S.user_id), eventId);
        authAPI.getEventInfo(context, MySharedPreferences.getPreferences(context, S.user_id), eventId);
    }


    private void setupViewPager(ViewPager view) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new InfoFragment(), getString(R.string.info));
        viewPagerAdapter.addFragment(new PilotsFragment(), getString(R.string.participants_text));

        view.setAdapter(viewPagerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        FastLapApplication.mCurrentContext = context;
        setToolbarHeading(getString(R.string.events));
    }

    private void initView() {

        member_list = new ArrayList<>();
        member_list.clear();
        eventsBean = new EventsBean();
        //    scrollView.setFillViewport(true);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        htabMaincontent = (CoordinatorLayout) findViewById(R.id.htab_maincontent);
        htabAppbar = (AppBarLayout) findViewById(R.id.htab_appbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        imgBg = (ImageView) findViewById(R.id.img_bg);
        txtName = (TextView) findViewById(R.id.txt_name);
        participateBtn = (Button) findViewById(R.id.participate_btn);
        privacyImg = (ImageView) findViewById(R.id.privacy_img);
        privacyTv = (TextView) findViewById(R.id.privacy_tv);
        approvedIv = (ImageView) findViewById(R.id.approved_iv);
        approvedTv = (TextView) findViewById(R.id.approved_tv);
        shareLl = (LinearLayout) findViewById(R.id.share_ll);
        userIcon = (CircleImageView) findViewById(R.id.user_icon);
        tabHost = (TabLayout) findViewById(R.id.tab_host);
        viewpager = (ViewPager) findViewById(R.id.viewpager);

        setupViewPager(viewpager);
        tabHost.setupWithViewPager(viewpager);
        viewpager.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                viewpager.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        Util.changeTabsFont(tabHost, this);

        clickListner();
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
        participateBtn.setOnClickListener(this);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                getCurrentPilotFragment();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.participate_btn:
                Intent intent = new Intent(context, EventTicketPurchase.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(S.data, eventsBean);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }


    public void EventMembersWebserviceResponse(String response) {
        member_list.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < dataJson.length(); i++) {
                    LikeUserBean likeUserBean = new LikeUserBean();
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);

                    likeUserBean.set_id(jsonObject1.getString(S._id));
                    likeUserBean.setLike_user_friend_status(jsonObject1.getString(S.alreadyFriend));
                    likeUserBean.setLike_user_id(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    likeUserBean.setLike_user_name(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    likeUserBean.setLike_user_image(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));

                    member_list.add(likeUserBean);
                }
                getCurrentFragment();
                getCurrentPilotFragment();
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void EventInfoWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = dataJson.getJSONObject(0);
                eventsBean = new EventsBean();

                eventsBean.set_id(jsonObject1.getString(S._id));
                eventsBean.setName(jsonObject1.getString(S.name));
                eventsBean.setDescription(jsonObject1.getString(S.description));
                eventsBean.setLocation(jsonObject1.getString(S.location_response));
                eventsBean.setCoverImage(jsonObject1.getString(S.coverImage));
                eventsBean.setImage(jsonObject1.getString(S.image));
                eventsBean.setCreated(jsonObject1.getString(S.created));
                eventsBean.setDays(jsonObject1.getString(S.days));
                eventsBean.setEndDate(jsonObject1.getString(S.endDate));
                eventsBean.setEndTime(jsonObject1.getString(S.endTime));
                eventsBean.setStartTime(jsonObject1.getString(S.startTime));

                JSONArray groupjsonarray = jsonObject1.getJSONArray(S.groupId);
                if (groupjsonarray.length()>0)
                {
                    eventsBean.setGroupDescription(groupjsonarray.getJSONObject(0).getString(S.description));
                    eventsBean.setGroupId(groupjsonarray.getJSONObject(0).getString(S._id));
                    eventsBean.setGroupImage(groupjsonarray.getJSONObject(0).getString(S.image));
                    eventsBean.setGroupName(groupjsonarray.getJSONObject(0).getString(S.name));
                }
                eventsBean.setPrivacy(jsonObject1.getString(S.privacy));
                //eventsBean.setRemainTicket(jsonObject1.getString(S.remainTicket));
                eventsBean.setStartDate(jsonObject1.getString(S.startDate));
                eventsBean.setStatus(jsonObject1.getString(S.status));
                // eventsBean.setTicketPrice(jsonObject1.getString(S.ticketPrice));
                // eventsBean.setTotalTicket(jsonObject1.getString(S.totalTicket));
                eventsBean.setTrackId(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S._id));
                eventsBean.setTrackName(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S.name));
                eventsBean.setTrackImage("");
                eventsBean.setTrackLocaion("");
             //   eventsBean.setType(jsonObject1.getInt(S.type));
                eventsBean.setUserId(jsonObject1.getJSONObject(S.userId).getString(S._id));
                eventsBean.setUserImage(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));
                eventsBean.setUserName(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));

                JSONArray jsonArray1 = new JSONArray();
                jsonArray1 = jsonObject1.getJSONArray(S.presets);

                ArrayList<EventsBean.Presets> presetsList = new ArrayList<>();
                for (int j = 0; j < jsonArray1.length(); j++) {
                    JSONObject jsonObject3 = new JSONObject();
                    jsonObject3 = jsonArray1.getJSONObject(j);
                    EventsBean.Presets presets = new EventsBean().new Presets();
                    presets.setTypes(jsonObject3.getString(S.types));
                    presets.setPrice(jsonObject3.getString(S.price));
                    presets.setTotalTicket(jsonObject3.getString(S.totalTicket));
                    presets.setTotalUser(jsonObject3.getString(S.totalUser));
                    presets.setRemainTicket(jsonObject3.getString(S.remainTicket));
                    presets.set_id(jsonObject3.getString(S._id));

                    presetsList.add(presets);
                }
                eventsBean.setPresetsList(presetsList);

                //eventsBean.setVehicleTypeId(jsonObject1.getString(S.vehicleTypeId));
                getCurrentFragment();

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public Fragment getCurrentFragment() {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && viewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(viewpager.getCurrentItem());
            if (mCurrentFragment instanceof InfoFragment) {
                ((InfoFragment) mCurrentFragment).setinfoData(eventsBean);
                setEventData();
            }
        }

        return mCurrentFragment;

    }

    public Fragment getCurrentPilotFragment() {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && viewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(viewpager.getCurrentItem());
            if (mCurrentFragment instanceof PilotsFragment) {
                ((PilotsFragment) mCurrentFragment).setinfoData();
                setEventData();
            }
        }
        return mCurrentFragment;

    }
    public void setEventData() {
        txtName.setText(eventsBean.getName());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + eventsBean.getCoverImage(), imgBg, Util.getImageLoaderOption(context));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + eventsBean.getImage(), userIcon, Util.getImageLoaderOption(context));
        if (eventsBean.getPrivacy().compareTo("1") == 0) {
            privacyTv.setText(getString(R.string.public_text));
        } else if (eventsBean.getPrivacy().compareTo("2") == 0) {
            privacyTv.setText(getString(R.string.private_text));
        } else {
            privacyTv.setText(getString(R.string.friends));
        }
    }
}
