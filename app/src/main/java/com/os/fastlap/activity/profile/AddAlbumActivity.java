package com.os.fastlap.activity.profile;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.TracksListingActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.profile.AddVehicleImageAdapter;
import com.os.fastlap.beans.BeanAddAlbum;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.libs.Gallery;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by abhinava on 11/21/2017
 */

public class AddAlbumActivity extends BaseActivity implements View.OnClickListener, OnImagePickerDialogSelect, SpinnerSelectorInterface, AddVehicleImageAdapter.OnItemClickListener, DatePickerDialog.OnDateSetListener {

    Context context;
    private ImageView backIv;
    private EditTextPlayRegular albumNameTv;
    private EditTextPlayRegular descriptionEt;
    private RecyclerView recyclerView;
    private TextViewPlayBold submitBtn;
    AddVehicleImageAdapter mAddVehicleImageAdapter;
    AuthAPI authAPI;
    TextView trackNameTv;
    SpinnerSelectorInterface spinnerSelectorInterface;
    TextView eventDateTv;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    private static final int TRACK_ACTIVITY_RESULT_CODE = 1;
    private ArrayList<PagerBean> session1_pagerBeanArrayList;
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private String TAG = AddAlbumActivity.class.getSimpleName();
    String removeImages = "";
    String track_name = "";
    String track_id = "";
    BeanAddAlbum beanAddAlbum;
    private Uri videoFileUri;
    BeanAlbum beanEditAlbum = null;
    String removeList = "";
    TextView txtTitle;
    ArrayList<BeanAlbum.BeanFiles> filesList = new ArrayList<>();
    private ArrayList<BeanVehicleType> groupTypes_list;
    TextViewPlayRegular groupTypeTv;
    String groupTypeID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_album_layout);
        context = this;
        authAPI = new AuthAPI(context);
        onImagePickerDialogSelect = this;
        spinnerSelectorInterface = this;
        onDateSetListener = this;
        beanAddAlbum = new BeanAddAlbum();
        initView();
    }

    private void initView() {
        txtTitle = findViewById(R.id.txtTitle);
        backIv = findViewById(R.id.back_iv);
        albumNameTv = findViewById(R.id.album_name_tv);
        descriptionEt = findViewById(R.id.description_et);
        recyclerView = findViewById(R.id.recycler_view);
        submitBtn = findViewById(R.id.submit_btn);
        trackNameTv = findViewById(R.id.track_name_tv);
        eventDateTv = findViewById(R.id.event_date_tv);
        groupTypeTv = findViewById(R.id.group_type_tv);

        groupTypes_list = new ArrayList<>();
        groupTypes_list.clear();

        session1_pagerBeanArrayList = new ArrayList<>();
        session1_pagerBeanArrayList.add(new PagerBean(null, null, null, true, false, ""));

        mAddVehicleImageAdapter = new AddVehicleImageAdapter(session1_pagerBeanArrayList, context);
        mAddVehicleImageAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAddVehicleImageAdapter);

        clickListner();

        if (getIntent().getExtras() != null) {
            beanEditAlbum = (BeanAlbum) getIntent().getExtras().getSerializable(S.data);
            if (beanEditAlbum != null) {
                txtTitle.setText(getText(R.string.edit_album));
                albumNameTv.setText(beanEditAlbum.getName());
                albumNameTv.setSelection(beanEditAlbum.getName().length());
                trackNameTv.setText(beanEditAlbum.getTrackName());
                descriptionEt.setText(beanEditAlbum.getDescription());
                track_id = beanEditAlbum.getTrackId();
                groupTypeID = beanEditAlbum.getGroupTypeId();
                groupTypeTv.setText(beanEditAlbum.getGroupTypeName());
                eventDateTv.setText(Util.ConvertDateTimeZoneDate(beanEditAlbum.getDateOfEvent()));
                filesList = beanEditAlbum.getFilesList();
                for (int i = 0; i < filesList.size(); i++) {
                    PagerBean pagerBean = new PagerBean();
                    pagerBean.setImage_id(filesList.get(i).getFileId());
                    pagerBean.setEdit(true);
                    if (!filesList.get(i).getType().equalsIgnoreCase(S.videoType))
                        pagerBean.setImage(filesList.get(i).getFileName());
                    else
                        pagerBean.setImage(filesList.get(i).getThumbName());
                    pagerBean.setFileType(filesList.get(i).getType());
                    session1_pagerBeanArrayList.add(pagerBean);
                }

            }
        }

    }

    private void clickListner() {
        backIv.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        trackNameTv.setOnClickListener(this);
        eventDateTv.setOnClickListener(this);
        groupTypeTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.track_name_tv:
                Intent trackIntent = new Intent(context, TracksListingActivity.class);
                startActivityForResult(trackIntent, TRACK_ACTIVITY_RESULT_CODE);
                break;
            case R.id.event_date_tv:
                DatepickerFragment datePickerFragment = DatepickerFragment.newInstance(onDateSetListener);
                datePickerFragment.show(getFragmentManager(), "date");
                break;

            case R.id.submit_btn:
                String albumName = albumNameTv.getText().toString().trim();
                String albumDescription = descriptionEt.getText().toString().trim();
                String grouptype = groupTypeTv.getText().toString().trim();

                if (TextUtils.isEmpty(albumName)) {
                    Util.showSnackBar(submitBtn, getString(R.string.please_enter_album_name));
                    return;
                }
                if (TextUtils.isEmpty(grouptype)) {
                    Util.showSnackBar(submitBtn, getString(R.string.please_enter_group_type));
                    return;
                }

                if (beanEditAlbum == null) {
                    beanAddAlbum.setAlbumName(albumName);
                    beanAddAlbum.setDescription(albumDescription);
                    beanAddAlbum.setDateOfEvent(eventDateTv.getText().toString().trim());
                    beanAddAlbum.setEventId("");
                    beanAddAlbum.setTrackId(track_id);
                    beanAddAlbum.setMediaArrayList(session1_pagerBeanArrayList);
                    beanAddAlbum.setPhotoCount(getImageCount());
                    beanAddAlbum.setVideoCount(getThumbnailCount());
                    beanAddAlbum.setGroupTypeId(groupTypeID);

                    authAPI.addAlbum(context, MySharedPreferences.getPreferences(context, S.user_id), beanAddAlbum);
                } else {
                    beanAddAlbum.setAlbumName(albumName);
                    beanAddAlbum.setDescription(albumDescription);
                    beanAddAlbum.setDateOfEvent(eventDateTv.getText().toString().trim());
                    beanAddAlbum.setEventId(beanEditAlbum.get_id());
                    beanAddAlbum.setTrackId(track_id);
                    beanAddAlbum.setMediaArrayList(session1_pagerBeanArrayList);
                    beanAddAlbum.setPhotoCount(getImageCount());
                    beanAddAlbum.setVideoCount(getThumbnailCount());
                    beanAddAlbum.setGroupTypeId(groupTypeID);

                    authAPI.editAlbum(context, MySharedPreferences.getPreferences(context, S.user_id), beanAddAlbum, beanEditAlbum, removeImages);
                }


                break;

            case R.id.group_type_tv:

                authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));

                break;
        }
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {
        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        chooseImagePickerOptionDialog.makeVisible();
        //pagerBean = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }

    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {
        // EasyImage.openGallery(this, SELECT_FILE_PROFILE);
        Intent intent = new Intent(this, Gallery.class);
        //Set the title
        intent.putExtra("title", "Select media");
        startActivityForResult(intent, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {
        // create new Intentwith with Standard Intent action that can be
        // sent to have the camera application capture an video and return it.
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // create a file to save the video
        videoFileUri = Util.getOutputMediaFileUri(I.MEDIA_TYPE_VIDEO, context);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoFileUri);

        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // set permission
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        // start the Video Capture Intent
        startActivityForResult(intent, I.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onAddImageCLick(View v, int position) {
        if (session1_pagerBeanArrayList.get(position).isDefault()) {

            if (android.os.Build.VERSION.SDK_INT >= 23) {
                //CHECKING PERMISSION FOR MARSHMALLOW
                checkRequestPermission();
            } else {
                showImageDialog();
            }
        }
    }

    @Override
    public void onDeleteImageClick(View v, int position) {
        if (session1_pagerBeanArrayList.get(position).getImage_id() != null && !session1_pagerBeanArrayList.get(position).getImage_id().isEmpty()) {
            if (removeImages.isEmpty())
                removeImages = session1_pagerBeanArrayList.get(position).getImage_id();
            else
                removeImages = removeImages + "," + session1_pagerBeanArrayList.get(position).getImage_id();
        }


        /* check before delete image is for edit or add*/
        if (!session1_pagerBeanArrayList.get(position).isEdit())
            session1_pagerBeanArrayList.remove(position);
        else {
            session1_pagerBeanArrayList.remove(position);
        }

        // refresh adapter
        mAddVehicleImageAdapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TRACK_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                track_name = data.getStringExtra(S.trackname);
                track_id = data.getStringExtra(S.trackId);

                trackNameTv.setText(track_name);
            }
        } else if (requestCode == I.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                ArrayList<PagerBean> listPagerBeen = new ArrayList<>();
                PagerBean pagerBean = new PagerBean();
                File file = new File(Util.getFilePathFromURI(context, data.getData()));

                pagerBean.setFile(file);
                pagerBean.setFileName(file.getName());
                pagerBean.setFileURl(data.getData().toString());
                pagerBean.setFileType(Util.isImageOrVideoFile(file.getPath()), file.getPath(), context);
                listPagerBeen.add(pagerBean);
                session1_pagerBeanArrayList.addAll(listPagerBeen);
                mAddVehicleImageAdapter.notifyDataSetChanged();
                //  SetShowMediaFiles(listPagerBeen);
            } else {
                ArrayList<PagerBean> listPagerBeen = new ArrayList<>();
                PagerBean pagerBean = new PagerBean();
                File file = new File(Util.getFilePathFromURI(context, videoFileUri));

                pagerBean.setFile(file);
                pagerBean.setFileName(file.getName());
                pagerBean.setFileURl(videoFileUri.getPath());
                pagerBean.setFileType(Util.isImageOrVideoFile(file.getPath()), file.getPath(), context);
                listPagerBeen.add(pagerBean);
                session1_pagerBeanArrayList.addAll(listPagerBeen);
                mAddVehicleImageAdapter.notifyDataSetChanged();
                //  SetShowMediaFiles(listPagerBeen);
            }

            //Log.e("filepathvideo", data.getData().getPath());
        } else if (requestCode == SELECT_FILE_PROFILE) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
                int i0 = selectionResult.size();

                //    selectedImage.clear();
                ArrayList<PagerBean> listPagerBeen = new ArrayList<>();
                for (int i = 0; i < selectionResult.size(); i++) {
                    PagerBean pagerBean = new PagerBean();
                    File file = new File(selectionResult.get(i));

                    pagerBean.setFile(file);
                    pagerBean.setFileName(file.getName());
                    pagerBean.setFileURl(Uri.fromFile(file).toString());
                    pagerBean.setFileType(Util.isImageOrVideoFile(file.getPath()), file.getPath(), context);
                    listPagerBeen.add(pagerBean);
                }
                session1_pagerBeanArrayList.addAll(listPagerBeen);
                mAddVehicleImageAdapter.notifyDataSetChanged();
            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;

                if (session1_pagerBeanArrayList.size() <= 5) {
                    PagerBean pagerBean = new PagerBean(imageFile, imageFile.getAbsolutePath(), imageFile.getName(), false, false, "", true, "");
                    pagerBean.setFileType(Util.isImageOrVideoFile(file.getPath()), file.getPath(), context);
                    session1_pagerBeanArrayList.add(pagerBean);
                    mAddVehicleImageAdapter.notifyDataSetChanged();
                }
            }


        });
    }

    private int getImageCount() {
        int length = 0;
        for (int i = 0; i < session1_pagerBeanArrayList.size(); i++) {
            if (!session1_pagerBeanArrayList.get(i).isDefault()) {
                if (!session1_pagerBeanArrayList.get(i).isEdit()) {
                    if (session1_pagerBeanArrayList.get(i).getFileType().equalsIgnoreCase(S.image))
                        length++;
                }
            }
        }
        return length;
    }

    private int getThumbnailCount() {
        int length = 0;
        for (int i = 0; i < session1_pagerBeanArrayList.size(); i++) {
            if (!session1_pagerBeanArrayList.get(i).isDefault()) {
                if (!session1_pagerBeanArrayList.get(i).isEdit()) {
                    if (session1_pagerBeanArrayList.get(i).getFileType().equalsIgnoreCase(S.video))
                        length++;
                }
            }
        }
        return length;
    }

    /* add album response */
    public void addAlbumResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.addalbum);
                // finish();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* add album response callback */
    public void addAlbumResponseCallback() {
        finish();
    }

    /* edit album response callback */
    public void editAlbumResponseCallback() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    /* edit album response*/
    public void editAlbumResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.updateAlbum_api);
                // finish();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public static class DatepickerFragment extends DialogFragment {

        DatePickerDialog.OnDateSetListener onDateSetListener;

        public static DatepickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener) {
            DatepickerFragment datepickerFragment = new DatepickerFragment();
            datepickerFragment.onDateSetListener = onDateSetListener;
            return datepickerFragment;
        }

        public Dialog onCreateDialog(Bundle savedInstance) {

            // create Calendar Instance from Calendar class
            final Calendar calender = Calendar.getInstance();
            int year = calender.get(Calendar.YEAR);
            int month = calender.get(Calendar.MONTH);
            int day = calender.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dateFragment = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_DARK, onDateSetListener, year, month, day);
            dateFragment.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            return dateFragment;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
        setDate(cal);
    }

    public void setDate(Calendar calender) {
        Date current = calender.getTime();
        int diff1 = new Date().compareTo(current);
        if (diff1 < 0) {
            Util.showSnackBar(eventDateTv, getString(R.string.please_enter_valid_date));
        } else {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            eventDateTv.setText(dateFormat.format(calender.getTime()));
        }
    }


    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                groupTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    groupTypes_list.add(beanVehicleType);
                }
                showVehicleTypeDialog(getString(R.string.select_group_type), groupTypes_list, I.GROUPTYPE);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {

    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {
        switch (type) {
            case I.GROUPTYPE:
                groupTypeTv.setText(value);
                groupTypeID = id;
                break;
        }
    }
}
