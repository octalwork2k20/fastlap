package com.os.fastlap.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.community.EventActivity;
import com.os.fastlap.activity.community.UserGroupsActivity;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.activity.mylaps.MyLapsUploadActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.data.database.DatabaseManager;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhinava on 7/13/2017.
 */

public class MenuActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout paddockMenuTv;
    private RelativeLayout profileMenuTv;
    private RelativeLayout galleryMenuTv;
    private RelativeLayout eventsMenuTv;
    private RelativeLayout groupsMenuTv;
    private RelativeLayout exploreMenuTv;
    private RelativeLayout settingsMenuTv;
    private RelativeLayout contactusMenuTv;
    private RelativeLayout logoutMenuTv,upload_menu_tv;
    TextView user_name_tv;
    private AuthAPI authAPI;
    String TAG = "MenuActivity.java";
    Context context;
    private CircleImageView toolbarUserPic;
    private TextViewPlayRegular toolbarTxtUserName;
    private DatabaseManager databaseManager;
    private ImageView mylap_icon;
    private ImageView exploreIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FastLapApplication.mCurrentContext = context;
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading(getString(R.string.menu));
        toolbarTxtUserName.setText(MySharedPreferences.getPreferences(context, S.username));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image), toolbarUserPic, Util.getImageLoaderOption(context));
    }

    private void initView() {
        authAPI = new AuthAPI(context);
        databaseManager = DatabaseManager.getInstance(this);
        exploreIcon = (ImageView) findViewById(R.id.explore_icon);
        mylap_icon = (ImageView) findViewById(R.id.mylap_icon);
        paddockMenuTv = (RelativeLayout) findViewById(R.id.paddock_menu_tv);
        profileMenuTv = (RelativeLayout) findViewById(R.id.profile_menu_tv);
        galleryMenuTv = (RelativeLayout) findViewById(R.id.gallery_menu_tv);
        eventsMenuTv = (RelativeLayout) findViewById(R.id.events_menu_tv);
        groupsMenuTv = (RelativeLayout) findViewById(R.id.groups_menu_tv);
        exploreMenuTv = (RelativeLayout) findViewById(R.id.explore_menu_tv);
        settingsMenuTv = (RelativeLayout) findViewById(R.id.settings_menu_tv);
        contactusMenuTv = (RelativeLayout) findViewById(R.id.contactus_menu_tv);
        logoutMenuTv = (RelativeLayout) findViewById(R.id.logout_menu_tv);
        upload_menu_tv = (RelativeLayout) findViewById(R.id.upload_menu_tv);

        toolbarUserPic = (CircleImageView) findViewById(R.id.toolbar_user_pic);
        toolbarTxtUserName = (TextViewPlayRegular) findViewById(R.id.toolbar_txt_user_name);
        user_name_tv = (TextView) findViewById(R.id.user_name_tv);
        exploreIcon.setVisibility(View.VISIBLE);

        clicklistner();

        user_name_tv.setText(MySharedPreferences.getPreferences(context,S.firstName_response)+" "+MySharedPreferences.getPreferences(context,S.lastName_response));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        exploreIcon.setVisibility(View.GONE);
    }

    private void clicklistner() {
        profileMenuTv.setOnClickListener(this);
        paddockMenuTv.setOnClickListener(this);
        galleryMenuTv.setOnClickListener(this);
        groupsMenuTv.setOnClickListener(this);
        eventsMenuTv.setOnClickListener(this);
        exploreMenuTv.setOnClickListener(this);
        settingsMenuTv.setOnClickListener(this);
        contactusMenuTv.setOnClickListener(this);
        logoutMenuTv.setOnClickListener(this);
        mylap_icon.setOnClickListener(this);
        upload_menu_tv.setOnClickListener(this);
        exploreIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.profile_menu_tv:
                Intent intent = new Intent(context, ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(S.page, I.MENU_SCREEN);
                bundle.putString(S.user_id, MySharedPreferences.getPreferences(context, S.user_id));
                bundle.putString("status", "0");
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.paddock_menu_tv:
                Util.startNewActivity(MenuActivity.this, MyLapsActivity.class, true);
                break;
            case R.id.upload_menu_tv:
                Util.startNewActivity(MenuActivity.this, MyLapsUploadActivity.class, true);
                break;
            case R.id.gallery_menu_tv:
                Util.startNewActivity(MenuActivity.this, GalleryActivity.class, true);
                break;
            case R.id.events_menu_tv:
                Intent intent1 = new Intent(mContext, EventActivity.class);
                intent1.putExtra(S.type, I.PROFILEEVENT);
                intent1.putExtra(S._id, MySharedPreferences.getPreferences(context, S.user_id));
                startActivity(intent1);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.groups_menu_tv:
                Util.startNewActivity(MenuActivity.this, UserGroupsActivity.class, true);
                break;
            case R.id.explore_menu_tv:
                Util.startNewActivity(MenuActivity.this, ExploreActivity.class, true);
                break;
            case R.id.settings_menu_tv:
                Util.startNewActivity(MenuActivity.this, SettingsActivity.class, true);
                break;
            case R.id.contactus_menu_tv:
                Util.startNewActivity(MenuActivity.this, ContactUsActivity.class, true);
                break;
            case R.id.logout_menu_tv:
                logoutDialog();
                break;
            case R.id.mylap_icon:
                Util.startNewActivity(this, MyLapsActivity.class, true);
                break;
            case R.id.explore_icon:
                Util.startNewActivity(MenuActivity.this, ExploreActivity.class, true);
                break;
        }
    }
    private void logoutDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);

        TextView cancelBtn;
        TextView logoutBtn;

        cancelBtn = (TextView) dialog.findViewById(R.id.cancel_btn);
        logoutBtn = (TextView) dialog.findViewById(R.id.logout_btn);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authAPI.logout(context, MySharedPreferences.getPreferences(context, S.user_id), Util.getDeviceToken(context));
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }


    // logout webservice response
    public void getLogoutWebserviceResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                databaseManager.clearAllDatabaseTable();

                String isFirstTime=MySharedPreferences.getPreferences(mContext,S.isFirstTime);
                String language=MySharedPreferences.getPreferences(mContext,S.language);

                MySharedPreferences.clearAll(context);
                FacebookSdk.sdkInitialize(getApplicationContext());
                LoginManager.getInstance().logOut();

                MySharedPreferences.setPreferences(mContext,isFirstTime,S.isFirstTime);
                MySharedPreferences.setPreferences(mContext,language,S.language);
                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
