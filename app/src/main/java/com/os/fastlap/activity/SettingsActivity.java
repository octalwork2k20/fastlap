package com.os.fastlap.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.ActivityCommunicator;
import com.os.fastlap.delegates.FragmentCommunicator;
import com.os.fastlap.delegates.FragmentCommunicatorTwo;
import com.os.fastlap.firebase.MyFirebaseMessagingService;
import com.os.fastlap.fragment.settings.SettingsFragment;
import com.os.fastlap.fragment.settings.SubscriptionFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by abhinava on 7/18/2017.
 */

public class SettingsActivity extends BaseActivity implements ActivityCommunicator, View.OnClickListener {
    private android.support.design.widget.TabLayout TabLayout;
    private Context context;
    private FragmentManager mFragmentManager;
    private ViewPagerAdapter adapter;
    private ViewPager viewpager;
    public FragmentCommunicator fragmentCommunicator;
    public FragmentCommunicatorTwo fragmentCommunicatorTwo;
    public static int pos = 0;
    ImageView base_toggle_icon;
    private String TAG = SettingsActivity.class.getSimpleName();
    AuthAPI mAuthAPI;
    String notification_type;
    TextView menu_menu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
        if(getIntent().getExtras()!=null){
            notification_type=getIntent().getStringExtra("Notification_type");
            if(notification_type.equalsIgnoreCase("USER_SUBSCRIPTION"))
                pos=1;
        }
        initView();
        addEventListeners();

    }

    BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if(intent.getExtras()!=null){
                    notification_type=intent.getStringExtra("Notification_type");
                    if(notification_type.equalsIgnoreCase("USER_SUBSCRIPTION")){
                        pos=1;
                        initView();
                        addEventListeners();
                    }

                }
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    };

    private void initView() {
        context = this;
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        mAuthAPI = new AuthAPI(context);
        FastLapApplication.mCurrentContext = context;
        mFragmentManager = getSupportFragmentManager();

        TabLayout = (TabLayout) findViewById(R.id.TabLayout);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);

        setUpViewPager(viewpager);
        TabLayout.setupWithViewPager(viewpager);
        clickListner();
        Util.changeTabsFont(TabLayout, context);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver!=null){
            unregisterReceiver(broadcastReceiver);
        }

    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading(getString(R.string.settings));
        FastLapApplication.mCurrentContext = context;
        registerReceiver(broadcastReceiver,new IntentFilter(MyFirebaseMessagingService.BROADCAST_FOR_SUBCRIPTION_PLAN));
        mAuthAPI.userSubscribeList(context, MySharedPreferences.getPreferences(context, S.user_id));
    }

    private void addEventListeners() {
        viewpager.setCurrentItem(pos);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                hideKeyboard();
                pos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    // LanguageListing webservice response
    public void getLanguage(String response) {
        Fragment settingFrag = adapter.getItem(0);
        if (settingFrag instanceof SettingsFragment)
            ((SettingsFragment) settingFrag).getLanguage(response);
    }

    private void setUpViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(mFragmentManager);
        adapter.addFragment(new SettingsFragment(context), getString(R.string.settings));
        adapter.addFragment(new SubscriptionFragment(), getString(R.string.subscription));
        adapter.getItem(viewPager.getCurrentItem());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void passDataToActivity(String someValue) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.menu_menu:
                Intent intent = new Intent(this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }
    public Fragment subscriptionListWebserviceResponse(String response) {
        Fragment mCurrentFragment = null;
        if (adapter != null && viewpager != null) {
            mCurrentFragment = adapter.getRegisteredFragment(1);
            ((SubscriptionFragment) mCurrentFragment).getsubscriptionList(response);

        }

        return mCurrentFragment;

    }


    public void voucherCodeWebserviceResponse(String response) {
        Fragment mCurrentFragment = null;
        if (adapter != null && viewpager != null) {
            mCurrentFragment = adapter.getRegisteredFragment(1);
            ((SubscriptionFragment) mCurrentFragment).getVoucherCode(response);

        }
    }

    public void userSubscribeList(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = new JSONArray();
                jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1 = jsonArray.getJSONObject(i);
                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2 = jsonObject1.has(S.subscriptionPackageId)?jsonObject1.getJSONObject(S.subscriptionPackageId):null;
                    if(jsonObject2!=null&&jsonObject2.length()>0){
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S._id)?jsonObject2.getString(S._id):"", S.userSubscriptionId);
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S.name)?jsonObject2.getString(S.name):"", S.userSubscriptionName);
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S.comparison)?jsonObject2.getString(S.comparison):"", S.userSubscriptionComparison);
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S.sessions)?jsonObject2.getString(S.sessions):"", S.userSubscriptionSessions);
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S.laps)?jsonObject2.getString(S.laps):"", S.userSubscriptionLaps);
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S.garage)?jsonObject2.getString(S.garage):"", S.userSubscriptionGarage);
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S.media)?jsonObject2.getString(S.media):"", S.userSubscriptionMedia);
                        MySharedPreferences.setPreferences(context, jsonObject2.has(S.subscriptionEndDate)?jsonObject2.getString(S.subscriptionEndDate):"", S.subscriptionEndDate);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
