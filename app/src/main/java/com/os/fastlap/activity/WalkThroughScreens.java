package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.fragment.WalkThroughFragment;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;


/**
 * Created by abhinava on 1/11/2017.
 */

public class WalkThroughScreens extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    private TextView skip, done;
    private ViewPager mPager;
    private DotsIndicator mIndicator;

    private ImagePagerAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.walkthrough);

        initializeView();
    }

    private void initializeView() {

        mIndicator = (DotsIndicator) findViewById(R.id.mIndicator);
        mPager = (ViewPager) findViewById(R.id.mPager);
        skip = (TextView) findViewById(R.id.skip);
        done = (TextView) findViewById(R.id.done);

        skip.setOnClickListener(this);
        done.setOnClickListener(this);

        mAdapter = new ImagePagerAdapter(getSupportFragmentManager(),3);
        mPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mPager);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==2) {
                    done.setVisibility(View.VISIBLE);
                }
                else {
                    done.setVisibility(View.INVISIBLE);
                }
                }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

       // Fragment mCurrentFragment = tabsPageradapter.getRegisteredFragment(pager.getCurrentItem());

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.skip:
                Intent intentSkip = new Intent(WalkThroughScreens.this, LoginActivity.class);
                startActivity(intentSkip);
                finish();
                break;
            case R.id.done:
                Intent intentDone = new Intent(WalkThroughScreens.this, LoginActivity.class);
                startActivity(intentDone);
                finish();
                break;
        }

    }

    private class ImagePagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public ImagePagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int index) {

            return WalkThroughFragment.newInstance(index);
        }

        @Override
        public int getCount() {
            // get item count - equal to number of tabs
            return mNumOfTabs;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
