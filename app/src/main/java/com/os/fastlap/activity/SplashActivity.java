
package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.os.fastlap.R;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * Created by abhinava on 7/10/2017.
 */
public class SplashActivity extends AppCompatActivity {
    Context mContext;
    int SPLASH_HOLD_TIME = 3000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        keyHash();
        holdSplashScreen();
        Util.setLocale(mContext);
    }

    private void holdSplashScreen() {
        Thread timerThread = new Thread() {

            public void run() {
                try {
                    sleep(SPLASH_HOLD_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                GoNext();

            }
        };
        timerThread.start();
    }
    public void GoNext() {
        Intent intent = null;
        if (MySharedPreferences.getPreferences(mContext, S.isFirstTime).equals("1"))
        {
            if (MySharedPreferences.getPreferences(mContext, S.user_id).equals("")) {
                intent = new Intent(mContext,
                        LoginActivity.class);
            } else {
                intent = new Intent(mContext,
                        DashboardActivity.class);
            }
        } else {
            intent = new Intent(mContext, WalkThroughScreens.class);
            MySharedPreferences.setPreferences(mContext, "1", S.isFirstTime);
        }
        startActivity(intent);
        finish();

    }
    public void keyHash() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.os.fastlap", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something1 = new String(Base64.encode(md.digest(), 0));
                String something2 = new String(Base64.encode(md.digest(),1));
                Log.e("hash key1", something1);
                Log.e("hash key2", something2);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }
}
