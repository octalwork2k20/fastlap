package com.os.fastlap.activity.profile;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPagerCustomDuration;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.profile.AddVehicleImageAdapter;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import me.crosswall.lib.coverflow.core.LinkageCoverTransformer;
import me.crosswall.lib.coverflow.core.LinkagePagerContainer;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;


/**
 * Created by abhinava on 8/3/2017.
 */

public class AddVehicleActivity extends BaseActivity implements View.OnClickListener, SpinnerSelectorInterface, OnImagePickerDialogSelect, AddVehicleImageAdapter.OnItemClickListener {

    Context context;
    private AppCompatImageView baseToggleIcon;
    private TextViewPlayRegular vehicleTypeTv;
    private TextViewPlayRegular vehicleBrandTv;
    private TextViewPlayRegular vehicleModelTv;
    private TextViewPlayRegular vehicleVersionTv;
    private TextViewPlayRegular vehicleYearTv;
    private TextViewPlayRegular tyreBrandTv;
    private TextViewPlayRegular tyreModelTv;
    private EditTextPlayRegular descriptionEt;
    private LinearLayout vehiclePhotoLl;
    private LinkagePagerContainer vehiclePhotoPagerContainer;
    private TextViewPlayBold addVehicleBtn;

    String getImagePathProfile = "";
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    private ArrayList<PagerBean> vehicle_photo_pagerBeanArrayList;
    private int beanId = 0;
    private VehiclePhotoMyPagerAdapter vehicle_photo_myPagerAdapter;
    private ViewPagerCustomDuration vehicle_photo_pager;

    private ArrayList<BeanVehicleType> vehicleTypes_list;
    private ArrayList<BeanVehicleType> vehicleBrand_list;
    private ArrayList<BeanVehicleType> vehicleModel_list;
    private ArrayList<BeanVehicleType> vehicleVersion_list;
    private ArrayList<BeanVehicleType> vehicleYear_list;
    private ArrayList<BeanVehicleType> vehicleTyreBrand_list;
    private ArrayList<BeanVehicleType> vehicleTyreModel_list;
    AuthAPI authAPI;
    private String TAG = AddVehicleActivity.class.getSimpleName();
    SpinnerSelectorInterface spinnerSelectorInterface;

    String vehicleTypeId = "";
    String vehicleBrandID = "";
    String vehicleModelId = "";
    String vehicleVersionId = "";
    String vehicleYearId = "";
    String vehicleTyreBrandId = "";
    String vehicleTyreModelId = "";

    OnImagePickerDialogSelect onImagePickerDialogSelect;
    RecyclerView recycler_view;
    AddVehicleImageAdapter mAddVehicleImageAdapter;
    BeanVehicleList beanVehicleList = null;
    boolean isEdit = false;
    String userVehicleId = "";
    String removeImage = "";
    String className="";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_add_vehicle);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        spinnerSelectorInterface = this;
        onImagePickerDialogSelect = this;
        if(getIntent().getExtras()!=null){
            className=getIntent().getStringExtra("class_name");
        }
        initView();
    }

    private void initView() {
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        vehicleTypeTv = (TextViewPlayRegular) findViewById(R.id.vehicle_type_tv);
        vehicleBrandTv = (TextViewPlayRegular) findViewById(R.id.vehicle_brand_tv);
        vehicleModelTv = (TextViewPlayRegular) findViewById(R.id.vehicle_model_tv);
        vehicleVersionTv = (TextViewPlayRegular) findViewById(R.id.vehicle_version_tv);
        vehicleYearTv = (TextViewPlayRegular) findViewById(R.id.vehicle_year_tv);
        tyreBrandTv = (TextViewPlayRegular) findViewById(R.id.tyre_brand_tv);
        tyreModelTv = (TextViewPlayRegular) findViewById(R.id.tyre_model_tv);
        descriptionEt = (EditTextPlayRegular) findViewById(R.id.description_et);
        vehiclePhotoLl = (LinearLayout) findViewById(R.id.vehicle_photo_ll);
        vehiclePhotoPagerContainer = (LinkagePagerContainer) findViewById(R.id.vehicle_photo_pager_container);
        addVehicleBtn = (TextViewPlayBold) findViewById(R.id.add_vehicle_btn);


        vehicleTypes_list = new ArrayList<>();
        vehicleTypes_list.clear();
        vehicleBrand_list = new ArrayList<>();
        vehicleBrand_list.clear();
        vehicleModel_list = new ArrayList<>();
        vehicleModel_list.clear();
        vehicleVersion_list = new ArrayList<>();
        vehicleVersion_list.clear();
        vehicleYear_list = new ArrayList<>();
        vehicleYear_list.clear();
        vehicleTyreBrand_list = new ArrayList<>();
        vehicleTyreBrand_list.clear();
        vehicleTyreModel_list = new ArrayList<>();
        vehicleTyreModel_list.clear();

        vehicle_photo_pagerBeanArrayList = new ArrayList<>();
        vehicle_photo_pagerBeanArrayList.add(new PagerBean(null, null, null, true, false, ""));
        mAddVehicleImageAdapter = new AddVehicleImageAdapter(vehicle_photo_pagerBeanArrayList, context);
        mAddVehicleImageAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setAdapter(mAddVehicleImageAdapter);

        vehicle_photo_pager = vehiclePhotoPagerContainer.getViewPager();
        vehicle_photo_myPagerAdapter = new VehiclePhotoMyPagerAdapter();
        vehicle_photo_pager.setAdapter(vehicle_photo_myPagerAdapter);
        vehicle_photo_pager.setOffscreenPageLimit(4);
        vehicle_photo_pager.setPageTransformer(false, new LinkageCoverTransformer(0f, 0f, 0f, 0f));
        vehicle_photo_pager.setClipChildren(false);
        vehicle_photo_pager.setCurrentItem(0);

        clickListner();


        autoFillVehicleData();
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
        vehicleTypeTv.setOnClickListener(this);
        vehicleBrandTv.setOnClickListener(this);
        vehicleModelTv.setOnClickListener(this);
        vehicleVersionTv.setOnClickListener(this);
        vehicleYearTv.setOnClickListener(this);
        tyreBrandTv.setOnClickListener(this);
        tyreModelTv.setOnClickListener(this);
        addVehicleBtn.setOnClickListener(this);
    }

    private void autoFillVehicleData() {
        beanVehicleList = (BeanVehicleList) getIntent().getSerializableExtra(S.vehicleModal);
        if (beanVehicleList != null) {
            userVehicleId = beanVehicleList.get_id();
            isEdit = true;
            addVehicleBtn.setText(getString(R.string.edit_vehicle));
            setToolbarHeading(getString(R.string.edit_vehicle));
            vehicleTypeTv.setText(beanVehicleList.getVehicleTypeIdModal().getName());
            vehicleBrandTv.setText(beanVehicleList.getVehicleBrandIdModal().getVehicleBrandName());
            vehicleModelTv.setText(beanVehicleList.getVehicleModelIdModal().getVehicleModelName());
            vehicleVersionTv.setText(beanVehicleList.getVehicleVersionIdModal().getVehicleVersionName());
            vehicleYearTv.setText(beanVehicleList.getVehicleYearIdModal().getVehicleYear());
            tyreBrandTv.setText(beanVehicleList.getVehicleTyreBrandIdModal().getBrandName());
            tyreModelTv.setText(beanVehicleList.getVehicleTyreModelIdModal().getVehichleModelName());
            descriptionEt.setText(beanVehicleList.getDescription());
            descriptionEt.setSelection(descriptionEt.length());

            vehicleTypeId = beanVehicleList.getVehicleTypeIdModal().get_id();
            vehicleBrandID = beanVehicleList.getVehicleBrandIdModal().get_id();
            vehicleModelId = beanVehicleList.getVehicleModelIdModal().get_id();
            vehicleVersionId = beanVehicleList.getVehicleVersionIdModal().get_id();
            vehicleYearId = beanVehicleList.getVehicleYearIdModal().get_id();
            vehicleTyreBrandId = beanVehicleList.getVehicleTyreBrandIdModal().get_id();
            vehicleTyreModelId = beanVehicleList.getVehicleTyreModelIdModal().get_id();

            vehicle_photo_pagerBeanArrayList.addAll(beanVehicleList.getImagesList());
            vehicle_photo_myPagerAdapter.notifyDataSetChanged();
        } else {
            authAPI.getVehicleType(context);
            setToolbarHeading(getString(R.string.add_vehicle));
        }


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;

            case R.id.vehicle_type_tv:
                if (beanVehicleList != null) {
                    authAPI.getVehicleType(context);
                } else
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_type), vehicleTypes_list, I.VEHICLE_TYPE);
                break;

            case R.id.vehicle_brand_tv:
                if (beanVehicleList != null) {
                    authAPI.getVehicleBrand(context, vehicleTypeId);
                    //  authAPI.getVehicleTyreBrand(context, vehicleTypeId);
                } else
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_brand), vehicleBrand_list, I.VEHICLE_BRAND);
                break;

            case R.id.vehicle_model_tv:
                if (beanVehicleList != null)
                    authAPI.getVehicleModel(context, vehicleTypeId, vehicleBrandID);
                else
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_model), vehicleModel_list, I.VEHICLE_MODEL);
                break;

            case R.id.vehicle_version_tv:
                if (beanVehicleList != null)
                    authAPI.getVehicleVersion(context, vehicleTypeId, vehicleBrandID, vehicleModelId);
                else
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_version), vehicleVersion_list, I.VEHICLE_VERSION);
                break;

            case R.id.vehicle_year_tv:
                if (beanVehicleList == null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_year), vehicleYear_list, I.VEHICLE_YEAR);
                else
                    authAPI.getVehicleYear(context, vehicleTypeId, vehicleBrandID, vehicleModelId, vehicleVersionId);
                break;

            case R.id.tyre_brand_tv:
                if (beanVehicleList != null)
                    authAPI.getVehicleTyreBrand(context, vehicleTypeId);
                else
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_brand), vehicleTyreBrand_list, I.TYRE_BRAND);
                break;

            case R.id.tyre_model_tv:
                if (beanVehicleList != null)
                    authAPI.getVehicleTyreModel(context, vehicleTypeId, vehicleTyreBrandId);
                else
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_model), vehicleTyreModel_list, I.TYRE_MODEL);
                break;

            case R.id.add_vehicle_btn:
                String isSubscribed="";
                if(MySharedPreferences.getBooleanPreferences(context,S.checkSubscription)){
                    isSubscribed="1";
                }
                else {
                    isSubscribed="0";
                }
                Util.hideKeyboard(getCurrentFocus(), context);
                try {
                    if (Validation.addVehicleValidation(vehicleTypeId, vehicleBrandID, vehicleModelId, vehicleVersionId, vehicleYearId, vehicleTyreBrandId, vehicleTyreModelId,
                            vehicle_photo_pagerBeanArrayList.size(), addVehicleBtn, descriptionEt, context)) {
                        if (isEdit) {
                            authAPI.editVehicleList(context, vehicleTypeId, vehicleBrandID, vehicleModelId, vehicleVersionId, MySharedPreferences.getPreferences(context, S.user_id), vehicleTyreBrandId,
                                    vehicleTyreModelId, vehicleYearId, descriptionEt.getText().toString(),isSubscribed, userVehicleId, removeImage, getNewFileArray());
                        } else{
                            userVehicleId= "New";
                            authAPI.addVehicle(context,userVehicleId, vehicleTypeId, vehicleBrandID, vehicleModelId, vehicleVersionId, MySharedPreferences.getPreferences(context, S.user_id), vehicleTyreBrandId,
                                    vehicleTyreModelId, vehicleYearId, descriptionEt.getText().toString(),isSubscribed, getNewFileArray());
                        }

                    }

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FastLapApplication.mCurrentContext = context;
        // setToolbarHeading(getString(R.string.add_vehicle));
    }

    /// Vehicle Type Response
    public void getVehicleTypeResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    vehicleTypes_list.add(beanVehicleType);
                }
                if (beanVehicleList != null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_type), vehicleTypes_list, I.VEHICLE_TYPE);
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /// Vehicle Brand Response
    public void getVehicleBrandResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleBrand_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehicleBrandName));
                    vehicleBrand_list.add(beanVehicleType);
                }
                if (beanVehicleList != null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_brand), vehicleBrand_list, I.VEHICLE_BRAND);
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /// Vehicle Model Response
    public void getVehicleModelResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleModel_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehicleModelName));
                    vehicleModel_list.add(beanVehicleType);
                }
                if (beanVehicleList != null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_model), vehicleModel_list, I.VEHICLE_MODEL);
            }/* else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /// Vehicle Version Response
    public void getVehicleVersionResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleVersion_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehicleVersionName));
                    vehicleVersion_list.add(beanVehicleType);
                }
                if (beanVehicleList != null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_version), vehicleVersion_list, I.VEHICLE_VERSION);

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Vehicle Year Response
    public void getVehicleYearResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleYear_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehicleYear));
                    vehicleYear_list.add(beanVehicleType);
                }
                if (beanVehicleList != null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_year), vehicleYear_list, I.VEHICLE_YEAR);
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Vehicle Tyre Brand Response
    public void getVehicleTyreBrandResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTyreBrand_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.brandName));
                    vehicleTyreBrand_list.add(beanVehicleType);
                }
                if (beanVehicleList != null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_brand), vehicleTyreBrand_list, I.TYRE_BRAND);

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Vehicle Tyre Model Response
    public void getVehicleTyreModelResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTyreModel_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehichleTyreModelName));
                    vehicleTyreModel_list.add(beanVehicleType);
                }
                if (beanVehicleList != null)
                    showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_model), vehicleTyreModel_list, I.TYRE_MODEL);
            }/* else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Add Vehicle Webservice Response
    public void getAddVehicleWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");
            else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Edit Vehicle Webservice Response
    public void getEditVehicleWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");
            else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        if (vehicleList.size() != 0)
        {
            final Dialog dialog1 = new Dialog(context);
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.spin_dialog);
            TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
            RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
            ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
            TextView done_tv = dialog1.findViewById(R.id.done_tv);
            done_tv.setVisibility(View.GONE);
            base_toggle_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog1.dismiss();
                }
            });
            heading.setText(dialog_title);

            VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(spinDialogAdapter);

            dialog1.setCancelable(true);
            dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog1.getWindow().setGravity(Gravity.CENTER);
            dialog1.show();
        } else {
            if (type == I.VEHICLE_BRAND)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.VEHICLE_TYPE)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_type));
            if (type == I.VEHICLE_MODEL)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
            if (type == I.VEHICLE_VERSION)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_version));
            if (type == I.VEHICLE_YEAR)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_year));
            if (type == I.TYRE_BRAND)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.TYRE_MODEL)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.VEHICLE_TYPE:
                vehicleTypeTv.setText(value);
                vehicleTypeId = id;

                vehicleBrandTv.setText("");
                vehicleBrandID = "";
                vehicleBrand_list.clear();

                vehicleModelTv.setText("");
                vehicleModelId = "";
                vehicleModel_list.clear();

                vehicleVersionTv.setText("");
                vehicleVersionId = "";
                vehicleVersion_list.clear();

                vehicleYearTv.setText("");
                vehicleYearId = "";
                vehicleYear_list.clear();

                tyreBrandTv.setText("");
                vehicleTyreBrandId = "";
                vehicleTyreBrand_list.clear();

                tyreModelTv.setText("");
                vehicleTyreModelId = "";
                vehicleTyreModel_list.clear();

                authAPI.getVehicleBrand(context, vehicleTypeId);
                authAPI.getVehicleTyreBrand(context, vehicleTypeId);
                beanVehicleList = null;
                break;
            case I.VEHICLE_BRAND:
                vehicleModelTv.setText("");
                vehicleModelId = "";
                vehicleModel_list.clear();

                vehicleVersionTv.setText("");
                vehicleVersionId = "";
                vehicleVersion_list.clear();

                vehicleYearTv.setText("");
                vehicleYearId = "";
                vehicleYear_list.clear();

                vehicleBrandTv.setText(value);
                vehicleBrandID = id;

                authAPI.getVehicleModel(context, vehicleTypeId, vehicleBrandID);
                beanVehicleList = null;
                break;
            case I.VEHICLE_MODEL:
                vehicleVersionTv.setText("");
                vehicleVersionId = "";
                vehicleVersion_list.clear();

                vehicleYearTv.setText("");
                vehicleYearId = "";
                vehicleYear_list.clear();

                vehicleModelTv.setText(value);
                vehicleModelId = id;
                beanVehicleList = null;
                authAPI.getVehicleVersion(context, vehicleTypeId, vehicleBrandID, vehicleModelId);
                break;
            case I.VEHICLE_VERSION:
                vehicleYearTv.setText("");
                vehicleYearId = "";
                vehicleYear_list.clear();

                vehicleVersionTv.setText(value);
                vehicleVersionId = id;

                beanVehicleList = null;
                authAPI.getVehicleYear(context, vehicleTypeId, vehicleBrandID, vehicleModelId, vehicleVersionId);
                break;
            case I.VEHICLE_YEAR:
                vehicleYearTv.setText(value);
                vehicleYearId = id;
                break;
            case I.TYRE_BRAND:
                tyreModelTv.setText("");
                vehicleTyreModelId = "";
                vehicleTyreModel_list.clear();

                tyreBrandTv.setText(value);
                vehicleTyreBrandId = id;
                beanVehicleList = null;
                authAPI.getVehicleTyreModel(context, vehicleTypeId, vehicleTyreBrandId);
                break;
            case I.TYRE_MODEL:
                tyreModelTv.setText(value);
                vehicleTyreModelId = id;
                break;
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }


    /* Click events in RecyclerView items  add image*/
    @Override
    public void onAddImageCLick(View v, int position) {
        if (vehicle_photo_pagerBeanArrayList.get(position).isDefault()) {
            if (vehicle_photo_pagerBeanArrayList.size() <= 4) {
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    //CHECKING PERMISSION FOR MARSHMALLOW
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
            } else {
                Util.showAlertDialog(context, getString(R.string.app_name), getResources().getString(R.string.choose_only_4_pics));
            }
        }
    }

    /* Click events on RecyclerView item delete image */
    @Override
    public void onDeleteImageClick(View v, int position) {
        /* check before delete image is for edit or add*/


        if (vehicle_photo_pagerBeanArrayList.get(position).getImage_id() != null && !vehicle_photo_pagerBeanArrayList.get(position).getImage_id().isEmpty()) {
            if (removeImage.isEmpty())
                removeImage = vehicle_photo_pagerBeanArrayList.get(position).getImage_id();
            else
                removeImage = removeImage + "," + vehicle_photo_pagerBeanArrayList.get(position).getImage_id();
        }

        if (!vehicle_photo_pagerBeanArrayList.get(position).isEdit())
            vehicle_photo_pagerBeanArrayList.remove(position);
        else {
            vehicle_photo_pagerBeanArrayList.remove(position);
        }

        // refresh adapter
        mAddVehicleImageAdapter.notifyDataSetChanged();

    }


    private class VehiclePhotoMyPagerAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;

        VehiclePhotoMyPagerAdapter() {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            final View view1;

            view1 = mLayoutInflater.inflate(R.layout.pager_row_layout, container, false);

            ImageView imageViewPagerImage = (ImageView) view1.findViewById(R.id.imgPagerImage);
            ImageView imageViewPagerDelete = (ImageView) view1.findViewById(R.id.imgPagerDelete);

            imageViewPagerDelete.setTag(position);

            if (vehicle_photo_pagerBeanArrayList.get(position).isDefault()) {
                imageViewPagerDelete.setVisibility(View.GONE);
                imageViewPagerImage.setImageResource(R.drawable.ic_image_add);
            } else {
                imageViewPagerDelete.setVisibility(View.VISIBLE);

                String uri = Uri.fromFile(vehicle_photo_pagerBeanArrayList.get(position).getFile()).toString();
                String decoded = Uri.decode(uri);
                ImageLoader.getInstance().displayImage(decoded, imageViewPagerImage);
                //imageViewPagerImage.setImageURI(Uri.parse(pagerBeanArrayList.get(position).getFileURl()));
            }

            imageViewPagerDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "onclick", Toast.LENGTH_SHORT).show();
                    //   container.removeView(view1);
                    int pos = (int) v.getTag();
                    vehicle_photo_pagerBeanArrayList.remove(pos);
                    notifyDataSetChanged();
                  /*  vehicle_photo_myPagerAdapter = new VehiclePhotoMyPagerAdapter();
                    vehicle_photo_pager.setAdapter(vehicle_photo_myPagerAdapter);*/

                }
            });

            imageViewPagerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (vehicle_photo_pagerBeanArrayList.get(position).isDefault()) {
                        if (vehicle_photo_pagerBeanArrayList.size() <= 4) {
                            if (android.os.Build.VERSION.SDK_INT >= 23) {
                                //CHECKING PERMISSION FOR MARSHMALLOW
                                checkRequestPermission();
                            } else {
                                showImageDialog();
                            }
                        } else {
                            Util.showAlertDialog(context, getString(R.string.app_name), getResources().getString(R.string.choose_only_4_pics));
                        }
                    }
                }
            });

            container.addView(view1);
            return view1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return vehicle_photo_pagerBeanArrayList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {

        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        //chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }

    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {
        EasyImage.openGallery(this, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, AddVehicleActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;

                if (vehicle_photo_pagerBeanArrayList.size() <= 5) {
                    vehicle_photo_pagerBeanArrayList.add(new PagerBean(imageFile, imageFile.getAbsolutePath(), imageFile.getName(), false, false, "", true, ""));
                    mAddVehicleImageAdapter.notifyDataSetChanged();

                }
            }


        });
    }

    @Override
    public void onBackPressed() {
        if(className.equalsIgnoreCase("DashboardVehicleListActivity")){
            Intent intent=new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
        else {
            finish();
        }

    }

    private ArrayList<PagerBean> getNewFileArray() {
        ArrayList<PagerBean> list = new ArrayList<>();
        for (int i = 0; i < vehicle_photo_pagerBeanArrayList.size(); i++) {
            if (vehicle_photo_pagerBeanArrayList.get(i).isNew()) {
                list.add(vehicle_photo_pagerBeanArrayList.get(i));
            }
        }

        return list;
    }
}