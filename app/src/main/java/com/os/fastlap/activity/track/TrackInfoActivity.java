package com.os.fastlap.activity.track;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.mylaps.MyLapsRaceActivity;
import com.os.fastlap.adapter.RecordLapAdater;
import com.os.fastlap.beans.TrackRecord;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.libs.WorkaroundMapFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/12/2017.
 */

public class TrackInfoActivity extends BaseActivity implements View.OnClickListener {
    private Context mContext;
    private AuthAPI authAPI;
    private final String TAG = TrackInfoActivity.class.getSimpleName();

    @BindView(R.id.base_toggle_icon)
    AppCompatImageView baseToggleIcon;

    @BindView(R.id.img_bg)
    ImageView imgBg;

    @BindView(R.id.txt_username)
    TextView txtUsername;

    @BindView(R.id.img_addfrd)
    ImageView imgAddfrd;

    @BindView(R.id.user_icon)
    CircleImageView userIcon;

    @BindView(R.id.txt_aboutme)
    TextView txtAboutme;

    @BindView(R.id.txt_name)
    TextView txtName;

    @BindView(R.id.img_user_name)
    ImageView imgUserName;

    @BindView(R.id.txt_location)
    TextView txtLocation;

    @BindView(R.id.ll_length)
    RelativeLayout llLength;

    @BindView(R.id.txt_length)
    TextView txtLength;

    @BindView(R.id.ll_turns)
    RelativeLayout llTurns;

    @BindView(R.id.txt_turns)
    TextView txtTurns;

    @BindView(R.id.ll_opened)
    RelativeLayout llOpened;

    @BindView(R.id.txt_opened)
    TextView txtOpened;

    @BindView(R.id.txt_lap_uploaded)
    TextView txtLapUploaded;

    @BindView(R.id.ll_phone_number)
    RelativeLayout llPhoneNumber;

    @BindView(R.id.txt_phone_number)
    TextView txtPhoneNumber;

    @BindView(R.id.txt_phone_number_name)
    TextView txtPhoneNumberName;

    @BindView(R.id.ll_website)
    RelativeLayout llWebsite;


    @BindView(R.id.txt_email)
    TextView txtEmail;

    @BindView(R.id.txt_phone_email_name)
    TextView txtPhoneEmailName;

    @BindView(R.id.txt_website)
    TextView txtWebsite;


    @BindView(R.id.txt_website_name)
    TextView txtWebsiteName;


    @BindView(R.id.txt_fav_group)
    TextView txtFavGroup;

    @BindView(R.id.rr_recyclerview)
    RelativeLayout rr_recyclerview;

    @BindView(R.id.recycler_view_groups)
    RecyclerView recyclerViewGroups;

    @BindView(R.id.recycler_view_photographer)
    RecyclerView recyclerViewPhotographer;

    @BindView(R.id.recycle_lap_record)
    RecyclerView recycle_lap_record;

    @BindView(R.id.nestedscrollview)
    NestedScrollView scrollview;

    @BindView(R.id.google_tv)
    Button google_tv;

    @BindView(R.id.track_tv)
    Button track_tv;

    String  trackRxLng="";
    String  trackRxLat="";

    String  trackLxLng="";
    String  trackLxLat="";

    String  overlayIimage="";


    GoogleMap googleMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_info_activity);
        ButterKnife.bind(this);
        mContext = this;
        setToolbarHeading(getString(R.string.track_info));
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();
        authAPI.getTrackInfo(mContext, TrackDetailActivity.trackId, MySharedPreferences.getPreferences(mContext, S.user_id));
        initlitaionTrckMap();
    }

    private void initlitaionTrckMap() {
        try {
            ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.compare_map_frame)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    String pos = MySharedPreferences.getPreferences(mContext, S.defaultmapView);
                    if (pos.compareTo("4") == 0) {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                        google_tv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                        track_tv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

                    } else {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        google_tv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                        track_tv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                    }
                }
            });

            ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.compare_map_frame))
                    .setListener(new WorkaroundMapFragment.OnTouchListener() {
                        @Override
                        public void onTouch() {
                            scrollview.requestDisallowInterceptTouchEvent(true);
                        }
                    });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.DASHBOARD_SCREEN);
        FastLapApplication.mCurrentContext = mContext;
    }

    private void initView() {
        setOnCLickListener();
        setProfileData();
    }

    private void setOnCLickListener() {
        google_tv.setOnClickListener(this);
        track_tv.setOnClickListener(this);
        baseToggleIcon.setOnClickListener(this);
    }

    private void setProfileData() {
        Util.setImageLoader(S.IMAGE_BASE_URL + TrackDetailActivity.trackCoverImage, imgBg, getApplicationContext());
        Util.setImageLoader(S.IMAGE_BASE_URL + TrackDetailActivity.trackImage, userIcon, getApplicationContext());
        txtUsername.setText(TrackDetailActivity.trackName);
        txtEmail.setText("("+TrackDetailActivity.trackEmail+")");

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.google_tv:
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                google_tv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                track_tv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                break;
            case R.id.track_tv:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                google_tv.setTextColor(ContextCompat.getColor(mContext, R.color.theme_graycolor));
                track_tv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                break;
        }
    }

    /* track info webservice response */
    public void trackInfoResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                txtAboutme.setText(jsonObject1.getString(S.aboutTrack));
                txtName.setText(jsonObject1.getString(S.name));
                txtLocation.setText(jsonObject1.getString(S.location_response));
                txtLength.setText(jsonObject1.getString(S.trackLength) /*" km"*/);
                txtTurns.setText(jsonObject1.getJSONObject(S.turns).getString(S.totalTurns) + "");
                txtOpened.setText(Util.ConvertDateTimeZoneDate(jsonObject1.getString(S.trackOpenedDate)));
                txtLapUploaded.setText(jsonObject1.getString("lapUploaded"));
                txtPhoneNumberName.setText(jsonObject1.getJSONObject(S.contactDetails).getString(S.mobileNumber_response));
                txtPhoneEmailName.setText(jsonObject1.getJSONObject(S.contactDetails).getString(S.email));
                txtWebsiteName.setText(jsonObject1.getJSONObject(S.contactDetails).getString(S.url));

                ArrayList<TrackRecord> trackRecordsList=new ArrayList<>();
                JSONObject leftRecord=new JSONObject(String.valueOf(jsonObject1.getJSONObject("leftRecord")));
                TrackRecord trackRecord=new TrackRecord();
                trackRecord.name=leftRecord.getString("name");
                trackRecord.time_lap=leftRecord.getString("time_lap");
                trackRecord.pilot=leftRecord.getString("pilot");
                trackRecord.vehicle_type=leftRecord.getString("vehicle_type");
                trackRecord.vehicle=leftRecord.getString("vehicle");
                trackRecord.date=Util.getDateFormate(leftRecord.getString("date"));
                trackRecordsList.add(trackRecord);

                TrackRecord trackRecord1=new TrackRecord();
                trackRecord1.name="Fast-Lap Record";
                JSONObject bestLap=jsonObject1.getJSONObject("bestLap");
                JSONObject lapTimeData=bestLap.getJSONObject("lapTimeData");
                JSONObject userVehicleId=lapTimeData.getJSONObject("userVehicleId");

                String firstName=lapTimeData.getJSONObject("userId").getJSONObject("personalInfo").getString("firstName");
                String lastName=lapTimeData.getJSONObject("userId").getJSONObject("personalInfo").getString("lastName");
                trackRecord1.time_lap=bestLap.getString("time");
                trackRecord1.pilot=firstName+" "+lastName;
                trackRecord1.vehicle_type=userVehicleId.has("vehicleTypeId")?userVehicleId.getJSONObject("vehicleTypeId").getString("name"):"Car";
                String modelName=userVehicleId.getJSONObject("vehicleBrandId").getString("vehicleBrandName");
                String brandName=userVehicleId.getJSONObject("vehicleModelId").getString("vehicleModelName");
                trackRecord1.vehicle=brandName+" "+modelName;
                trackRecord1.date=Util.ConvertDateTimeZoneDate(jsonObject1.getJSONObject("bestLap").getString("created"));
                trackRecordsList.add(trackRecord1);


                JSONObject rightRecord=new JSONObject(String.valueOf(jsonObject1.getJSONObject("rightRecord")));
                TrackRecord trackRecord2=new TrackRecord();
                trackRecord2.name=rightRecord.getString("name");
                trackRecord2.time_lap=rightRecord.getString("time_lap");
                trackRecord2.pilot=rightRecord.getString("pilot");
                trackRecord2.vehicle_type=rightRecord.getString("vehicle_type");
                trackRecord2.vehicle=rightRecord.getString("vehicle");
                trackRecord2.date=Util.getDateFormate(rightRecord.getString("date"));;
                trackRecordsList.add(trackRecord2);
                recycle_lap_record.setLayoutManager(new LinearLayoutManager(this));
                RecordLapAdater recordLapAdater=new RecordLapAdater(this,trackRecordsList);
                recycle_lap_record.setAdapter(recordLapAdater);

                JSONObject trackversionData=jsonObject1.has("trackversionData")?jsonObject1.getJSONObject("trackversionData"):null;
                if(trackversionData!=null){
                    try {
                        JSONArray  rxArray = trackversionData.has(S.right_top)?trackversionData.getJSONArray(S.right_top):null;
                        if (rxArray.length() > 0&&rxArray!=null) {
                             trackRxLng=rxArray.getString(0);
                             trackRxLat=rxArray.getString(1);
                        }
                        //JSONArray lxArray = new JSONArray();
                        JSONArray lxArray=trackversionData.has(S.left_bottom)?trackversionData.getJSONArray(S.left_bottom):null;
                        if (lxArray.length() > 0&&lxArray!=null) {
                             trackLxLng=lxArray.getString(0);
                             trackLxLat=lxArray.getString(1);
                        }
                        overlayIimage=trackversionData.has(S.overlay_image)?trackversionData.getString(S.overlay_image):"";
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                recordLapAdater.notifyDataSetChanged();
                drawGoogleMap();


            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    private void drawGoogleMap(){
        new AddGroundOverlay().execute(overlayIimage);
        PolylineOptions line = new PolylineOptions();
        line.add(new LatLng(Double.parseDouble(trackLxLat),Double.parseDouble(trackRxLng)));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(line.getPoints().get(0)).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
    public class AddGroundOverlay extends AsyncTask<String, Integer, BitmapDescriptor> {

        BitmapDescriptor bitmapDescriptor;
        String imageUrl;
        @Override
        protected BitmapDescriptor doInBackground(String... url) {
            imageUrl = url[0];
            try {
                bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(Picasso.with(mContext).load(S.IMAGE_BASE_URL+imageUrl).get());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmapDescriptor;
        }
        protected void onPostExecute(BitmapDescriptor icon) {
            try {
                LatLngBounds newarkBounds = new LatLngBounds(
                        new LatLng(Double.parseDouble(trackLxLat), Double.parseDouble(trackLxLng)),       // South west corner
                        new LatLng(Double.parseDouble(trackRxLat), Double.parseDouble(trackRxLng)));      // North east corner
                GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                        .image(icon)
                        .positionFromBounds(newarkBounds);
                googleMap.addGroundOverlay(newarkMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
