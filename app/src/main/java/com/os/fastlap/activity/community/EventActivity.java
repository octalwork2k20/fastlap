package com.os.fastlap.activity.community;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.SearchActivity;
import com.os.fastlap.activity.group.GroupProfileActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.adapter.community.CommunityVechiceListAdapter;
import com.os.fastlap.beans.BeanVehicle;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.fragment.community.EventFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by anandj on 7/18/2017.
 */

public class EventActivity extends BaseActivity implements View.OnClickListener {

    private TabLayout tab_host;
    private ViewPager viewpager;
    private ViewPagerAdapter viewPagerAdapter;
    Context context;
    ImageView base_toggle_icon;
    public static ArrayList<EventsBean> mArrayListSignedUp;
    public static ArrayList<EventsBean> mArrayListFavoite;
    public static ArrayList<EventsBean> mArrayListFriends;
    public static ArrayList<EventsBean> mArrayListAll;
    EditText searchEt;
    AuthAPI authAPI;
    private String TAG = EventActivity.class.getSimpleName();
    String getEventId;
    String searchText = "";

    private CommunityVechiceListAdapter communityVechiceListAdapter;
    private ArrayList<BeanVehicle> mArrayVechineList;
    private RecyclerView recycler_view;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_fragment_events);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();

        getEventId = getIntent().getExtras().getString(S._id);
        int type = getIntent().getExtras().getInt(S.type);

        if (type == I.TRACKEVENT) {
            authAPI.getTrackEvents(context, MySharedPreferences.getPreferences(context, S.user_id), getEventId, I.SIGNEDUPEVENT);
            authAPI.getTrackEvents(context, MySharedPreferences.getPreferences(context, S.user_id), getEventId, I.FAVORITEEVENT);
            authAPI.getTrackEvents(context, MySharedPreferences.getPreferences(context, S.user_id), getEventId, I.FRIENDSEVENT);
            authAPI.getTrackEvents(context, MySharedPreferences.getPreferences(context, S.user_id), getEventId, I.ALLEVENT);
        } else if (type == I.GROUPEVENT) {
            authAPI.getGroupEvents(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId, I.SIGNEDUPEVENT);
            authAPI.getGroupEvents(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId, I.FAVORITEEVENT);
            authAPI.getGroupEvents(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId, I.FRIENDSEVENT);
            authAPI.getGroupEvents(context, MySharedPreferences.getPreferences(context, S.user_id), GroupProfileActivity.profileGroupId, I.ALLEVENT);
        } else if (type == I.PROFILEEVENT) {
            authAPI.getUserEvents(context, MySharedPreferences.getPreferences(context, S.user_id), I.SIGNEDUPEVENT);
            authAPI.getUserEvents(context, MySharedPreferences.getPreferences(context, S.user_id), I.FAVORITEEVENT);
            authAPI.getUserEvents(context, MySharedPreferences.getPreferences(context, S.user_id), I.FRIENDSEVENT);
            authAPI.getUserEvents(context, MySharedPreferences.getPreferences(context, S.user_id), I.ALLEVENT);
        }

        authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
    }

    private void initView() {
        tab_host = (TabLayout) findViewById(R.id.tab_host);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        searchEt = (EditText) findViewById(R.id.searchEt);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        recycler_view = (RecyclerView) findViewById(R.id.vechice_list);

        mArrayListSignedUp = new ArrayList<>();
        mArrayListSignedUp.clear();

        mArrayListFavoite = new ArrayList<>();
        mArrayListFavoite.clear();

        mArrayListFriends = new ArrayList<>();
        mArrayListFriends.clear();

        mArrayListAll = new ArrayList<>();
        mArrayListAll.clear();

        mArrayVechineList = new ArrayList<>();
        mArrayVechineList.clear();
        communityVechiceListAdapter = new CommunityVechiceListAdapter(context, mArrayVechineList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setAdapter(communityVechiceListAdapter);

        setUpViewPager(viewpager);
        tab_host.setupWithViewPager(viewpager);
        Util.changeTabsFont(tab_host, context);

        clickListner();

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getCurrentFragment();
                getCurrentFragmentFilter(searchText);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        searchEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(S.type,I.EVENTSSEARCH);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
    }

    private void setUpViewPager(ViewPager viewpager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new EventFragment(I.SIGNEDUPEVENT), "Signed Event");
        viewPagerAdapter.addFragment(new EventFragment(I.FAVORITEEVENT), getString(R.string.favorites));
        viewPagerAdapter.addFragment(new EventFragment(I.FRIENDSEVENT), getString(R.string.friends));
        viewPagerAdapter.addFragment(new EventFragment(I.ALLEVENT), getString(R.string.all));
        viewpager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //  setToolbarHeading(getString(R.string.events));
        FastLapApplication.mCurrentContext = context;
    }

    public void EventWebserviceResponse(String response, int type) {
        if (type == I.SIGNEDUPEVENT) {
            mArrayListSignedUp.clear();
        } else if (type == I.FAVORITEEVENT) {
            mArrayListFavoite.clear();
        } else if (type == I.FRIENDSEVENT) {
            mArrayListFriends.clear();
        } else {
            mArrayListAll.clear();
        }

        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    EventsBean eventsBean = new EventsBean();
                    JSONObject jsonObject1;
                    jsonObject1 = jsonArray.getJSONObject(i);
                    eventsBean.set_id(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    eventsBean.setTrackId(jsonObject1.has(S.trackId)?jsonObject1.getString(S.trackId):"");
                    eventsBean.setGroupId(jsonObject1.has(S.groupId)?jsonObject1.getString(S.groupId):"");
                    eventsBean.setCreated(jsonObject1.has(S.created)?jsonObject1.getString(S.created):"");
                    eventsBean.setStatus(jsonObject1.has(S.status)?jsonObject1.getString(S.status):"");
                    eventsBean.setCoverImage(jsonObject1.has(S.coverImage)?jsonObject1.getString(S.coverImage):"");
                    eventsBean.setImage(jsonObject1.has(S.image)?jsonObject1.getString(S.image):"");
                    eventsBean.setDays(jsonObject1.has(S.days)?jsonObject1.getString(S.days):"");
                    eventsBean.setEndDate(jsonObject1.has(S.endDate)?jsonObject1.getString(S.endDate):"");
                    eventsBean.setStartDate(jsonObject1.has(S.startDate)?jsonObject1.getString(S.startDate):"");
                    eventsBean.setUserId(jsonObject1.has(S.userId)?jsonObject1.getString(S.userId):"");
                    if(jsonObject1.getString(S.type).equalsIgnoreCase("null")){
                        eventsBean.setType(1);
                    }
                    else {
                        eventsBean.setType(jsonObject1.has(S.type)?jsonObject1.getInt(S.type):1);
                    }

                    eventsBean.setPrivacy(jsonObject1.has(S.privacy)?jsonObject1.getString(S.privacy):"");
                    eventsBean.setLocation(jsonObject1.has(S.location_response)?jsonObject1.getString(S.location_response):"");
                    eventsBean.setDescription(jsonObject1.has(S.description)?jsonObject1.getString(S.description):"");
                    eventsBean.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");

                    JSONArray jsonArray1 = new JSONArray();
                    jsonArray1 = jsonObject1.getJSONArray(S.presets);

                    ArrayList<EventsBean.Presets> presetsList = new ArrayList<>();
                    for (int j = 0; j < jsonArray1.length(); j++) {
                        JSONObject jsonObject2 = new JSONObject();
                        jsonObject2 = jsonArray1.getJSONObject(j);
                        EventsBean.Presets presets = new EventsBean().new Presets();
                        presets.setTypes(jsonObject2.has(S.types)?jsonObject2.getString(S.types):"1");
                        presets.setPrice(jsonObject2.has(S.price)?jsonObject2.getString(S.price):"1");
                        presets.setTotalTicket(jsonObject2.has(S.totalTicket)?jsonObject2.getString(S.totalTicket):"1");
                        presets.setTotalUser(jsonObject2.has(S.totalUser)?jsonObject2.getString(S.totalUser):"1");
                        presets.setRemainTicket(jsonObject2.has(S.remainTicket)?jsonObject2.getString(S.remainTicket):"1");
                        presets.set_id(jsonObject2.has(S._id)?jsonObject2.getString(S._id):"1");
                        presetsList.add(presets);
                    }
                    eventsBean.setPresetsList(presetsList);

                    if (type == I.SIGNEDUPEVENT) {
                        mArrayListSignedUp.add(eventsBean);
                    } else if (type == I.FAVORITEEVENT) {
                        mArrayListFavoite.add(eventsBean);
                    } else if (type == I.FRIENDSEVENT) {
                        mArrayListFriends.add(eventsBean);
                    } else {
                        mArrayListAll.add(eventsBean);
                    }

                    /*JSONArray groupArray = jsonObject1.has(S.groupId)?jsonObject1.getJSONArray(S.groupId):null;
                    if (groupArray!=null && groupArray.length() > 0) {
                        eventsBean.setVehicleTypeId(groupArray.getJSONObject(0).getString(S.groupTypeId));
                    }*/
                }
                getCurrentFragment();


            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void getCurrentFragment() {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && viewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((EventFragment) mCurrentFragment).getListData();
        }

        //    return mCurrentFragment;

    }

    public void getCurrentFragmentFilter(String ch) {
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && viewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((EventFragment) mCurrentFragment).SearchFilter(ch);
        }

        // return mCurrentFragment;

    }

    public void getVehicleTypeClick(int pos, String select_id) {
        String id = mArrayVechineList.get(pos).getId();
        Fragment mCurrentFragment = null;
        if (viewPagerAdapter != null && viewpager != null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((EventFragment) mCurrentFragment).getVehicleTypeClick(id, select_id);
        }
    }

    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                mArrayVechineList.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicle beanVehicle = new BeanVehicle();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicle.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanVehicle.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");
                    beanVehicle.setImage(jsonObject1.has(S.image)?jsonObject1.getString(S.image):"");
                    mArrayVechineList.add(beanVehicle);
                }
                communityVechiceListAdapter.notifyDataSetChanged();

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
