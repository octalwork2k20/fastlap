package com.os.fastlap.activity.community;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.SearchActivity;
import com.os.fastlap.activity.group.CreateGroupActivity;
import com.os.fastlap.activity.profile.PendingGroupRequestActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.adapter.community.CommunityOfficialsAdapter;
import com.os.fastlap.adapter.community.CommunityVechiceListAdapter;
import com.os.fastlap.beans.BeanGroup;
import com.os.fastlap.beans.BeanVehicle;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anandj on 7/18/2017
 */

public class UserGroupsActivity extends BaseActivity implements View.OnClickListener, CommunityOfficialsAdapter.ClickListner {

    Context context;
    private RelativeLayout create_group_rl;
    private RecyclerView vechice_list;
    private RecyclerView recycler_view;
    private CommunityVechiceListAdapter communityVechiceListAdapter;
    private ArrayList<BeanVehicle> mArrayVechineList;
    private CommunityOfficialsAdapter communityOfficialsAdapter;
    private ArrayList<BeanGroup> groupArrayList;
    private ViewPagerAdapter viewPagerAdapter;
    private NestedScrollView nestedscrollview;
    ImageView base_toggle_icon;
    AuthAPI authAPI;
    EditText searchEt;
    TextView no_data_tv;
    int user_type = 5;
    ImageView option_icon;
    private String TAG = UserGroupsActivity.class.getSimpleName();
    ImageView request_icon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_fragment_user_groups);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();

        authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
        authAPI.getGroupList(context, MySharedPreferences.getPreferences(context, S.user_id), user_type,MySharedPreferences.getPreferences(context, S.vehicleTypeId));
    }

    private void initView() {
        option_icon = findViewById(R.id.option_icon);
        nestedscrollview = findViewById(R.id.nestedscrollview);
        recycler_view = findViewById(R.id.recycler_view);
        vechice_list = findViewById(R.id.vechice_list);
        base_toggle_icon = findViewById(R.id.base_toggle_icon);
        create_group_rl = findViewById(R.id.create_group_rl);
        searchEt = findViewById(R.id.searchEt);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        request_icon = findViewById(R.id.request_icon);

        mArrayVechineList = new ArrayList<>();
        mArrayVechineList.clear();
        communityVechiceListAdapter = new CommunityVechiceListAdapter(context, mArrayVechineList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        vechice_list.setLayoutManager(layoutManager);
        vechice_list.setAdapter(communityVechiceListAdapter);

        groupArrayList = new ArrayList<>();
        groupArrayList.clear();

        communityOfficialsAdapter = new CommunityOfficialsAdapter(context, groupArrayList);
        recycler_view.setLayoutManager(new LinearLayoutManager(context));
        recycler_view.setAdapter(communityOfficialsAdapter);
        recycler_view.setNestedScrollingEnabled(false);
        nestedscrollview.setFillViewport(true);
        recycler_view.setHasFixedSize(true);
        communityOfficialsAdapter.setonclickListner(this);
        clickListner();

        searchEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(S.type, I.GROUPSSEARCH);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        create_group_rl.setOnClickListener(this);
        option_icon.setOnClickListener(this);
        request_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.create_group_rl:
                //Util.startNewActivity(UserGroupsActivity.this, CreateGroupActivity.class, false);
                Intent intent=new Intent(this,CreateGroupActivity.class);
                intent.putExtra(S.showGroupDetails,false);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                startActivity(intent);
                break;
            case R.id.option_icon:
                final PopupMenu popup = new PopupMenu(context, option_icon);
                //inflating menu from xml resource
                popup.inflate(R.menu.post_option_menu);
                if (user_type == 5)
                    popup.getMenu().getItem(0).setTitle(getString(R.string.my_group));
                else
                    popup.getMenu().getItem(0).setTitle(getString(R.string.all_group));
                Menu m = popup.getMenu();
                m.removeItem(R.id.report_post);
                m.removeItem(R.id.hide_post);
                m.removeItem(R.id.delete_post);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.annoing_text:
                                if (user_type == 5) {
                                    user_type = 6;
                                    authAPI.getGroupList(context, MySharedPreferences.getPreferences(context, S.user_id), user_type,MySharedPreferences.getPreferences(context, S.vehicleTypeId));
                                } else {
                                    user_type = 5;
                                    authAPI.getGroupList(context, MySharedPreferences.getPreferences(context, S.user_id), user_type,MySharedPreferences.getPreferences(context, S.vehicleTypeId));
                                }
                                popup.dismiss();
                                break;
                            case R.id.cancel:
                                popup.dismiss();
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
                break;

            case R.id.request_icon:
                startActivity(new Intent(context, PendingGroupRequestActivity.class));
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        // setToolbarHeading(getString(R.string.groups));
        FastLapApplication.mCurrentContext = context;
    }

    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                mArrayVechineList.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicle beanVehicle = new BeanVehicle();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    beanVehicle.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanVehicle.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");
                    beanVehicle.setImage(jsonObject1.has(S.image)?jsonObject1.getString(S.image):"");

                    mArrayVechineList.add(beanVehicle);
                }
                communityVechiceListAdapter.notifyDataSetChanged();

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void joinedGroup(int position) {
        authAPI.joinedGroup(context, MySharedPreferences.getPreferences(context, S.user_id), groupArrayList.get(position).getId(), "1", position);
    }

    public void groupjoinWebserviceResponse(int position, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                groupArrayList.get(position).setJoinedCount("1");
                groupArrayList.get(position).setAdduserStatus("1");
                communityOfficialsAdapter.notifyItemChanged(position);
                // Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void groupsWebserviceResponse(String response) {
        try {
            groupArrayList.clear();
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1;
                    jsonObject1 = jsonArray.getJSONObject(i);
                    BeanGroup beanGroup = new BeanGroup();

                    beanGroup.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanGroup.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");
                    beanGroup.setDescription(jsonObject1.has(S.description)?jsonObject1.getString(S.description):"");
                    beanGroup.setImage(jsonObject1.has(S.image)?jsonObject1.getString(S.image):"");
                    beanGroup.setAdduserStatus(jsonObject1.has(S.addedGroup)?jsonObject1.getString(S.addedGroup):"");
                    beanGroup.setJoinedCount(jsonObject1.has(S.joinedUser)?jsonObject1.getString(S.joinedUser):"");
                    beanGroup.setGroupTypeId(jsonObject1.has(S.groupTypeId)?jsonObject1.getString(S.groupTypeId):"");
                    beanGroup.setGroupOwnerId(jsonObject1.has(S.userId)?jsonObject1.getString(S.userId):"");
                    groupArrayList.add(beanGroup);

                }
                communityOfficialsAdapter.notifyDataSetChanged();
                communityOfficialsAdapter.addCloneList(groupArrayList);

                if (groupArrayList.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    recycler_view.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    recycler_view.setVisibility(View.GONE);
                }

            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                recycler_view.setVisibility(View.GONE);

                //Util.showAlertDialog(context, getString(R.string.alert), msg);
            }
        } catch (Exception e) {
            no_data_tv.setVisibility(View.VISIBLE);
            recycler_view.setVisibility(View.GONE);
            Log.e(TAG, e.toString());
        }
    }

    public void getVehicleTypeClick(int pos, String select_id) {
        String id = mArrayVechineList.get(pos).getId();
        if (select_id.isEmpty())
            communityOfficialsAdapter.filterById("");
        else
            communityOfficialsAdapter.filterById(id);
    }
}
