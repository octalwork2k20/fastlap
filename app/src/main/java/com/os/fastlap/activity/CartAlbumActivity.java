package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.os.fastlap.R;
import com.os.fastlap.activity.payment.AddCardDetail;
import com.os.fastlap.adapter.CartAlbumAdpter;
import com.os.fastlap.beans.AlbumIdBeans;
import com.os.fastlap.beans.CartAlbumBeans;
import com.os.fastlap.beans.CartAlbumImagesBeans;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.os.fastlap.R.id.view;

/**
 * Created by anandj on 10/5/2017.
 */

public class CartAlbumActivity extends BaseActivity implements View.OnClickListener {

    private AppCompatImageView mBaseToggleIcon;
    private TextViewPlayBold mToolbarheading;
    private View mView;
    private RelativeLayout mActionBar;
    private ImageView mImgEmptycart;
    private RecyclerView mRecylerview;
    private TextViewPlayBold mBtnPlaceOrder;
    private TextViewPlayBold mTxtCarttotal;
    private RelativeLayout mRrBottom;
    private Context mContext;
    private AuthAPI mAuthAPI;
    private List<CartAlbumBeans> mAlbumBeansList;
    private CartAlbumAdpter mCartAlbumAdpter;
    private double cart_total = 0;
    private static final String TAG = CartAlbumActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_my_cart);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        initView();
        callViewCartAblum();
    }

    private void initView() {
        mAuthAPI = new AuthAPI(mContext);

        mBaseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        mToolbarheading = (TextViewPlayBold) findViewById(R.id.toolbarheading);
        mView = (View) findViewById(view);
        mActionBar = (RelativeLayout) findViewById(R.id.action_bar);
        mImgEmptycart = (ImageView) findViewById(R.id.img_emptycart);
        mRecylerview = (RecyclerView) findViewById(R.id.recylerview);
        mBtnPlaceOrder = (TextViewPlayBold) findViewById(R.id.btn_place_order);
        mTxtCarttotal = (TextViewPlayBold) findViewById(R.id.txt_carttotal);
        mRrBottom = (RelativeLayout) findViewById(R.id.rr_bottom);
        mAlbumBeansList = new ArrayList<>();
        mCartAlbumAdpter = new CartAlbumAdpter(mContext, mAlbumBeansList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecylerview.setLayoutManager(layoutManager);
        mRecylerview.setAdapter(mCartAlbumAdpter);


        mBaseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mRrBottom.setOnClickListener(this);


        mBaseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        mToolbarheading.setText(getString(R.string.mycart));

    }

    public void callViewCartAblum() {
        mAuthAPI.viewAlbumCart(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
    }

    /* viewAlbumCart webservice response */
    public void viewAlbumCartWebservcieResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            mAlbumBeansList.clear();
            cart_total = 0;
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);


                mRecylerview.setVisibility(View.VISIBLE);
                mRrBottom.setVisibility(View.VISIBLE);
                mImgEmptycart.setVisibility(View.GONE);


                if (dataJson.length() != 0) {
                    mRecylerview.setVisibility(View.GONE);
                    mRrBottom.setVisibility(View.GONE);
                    mImgEmptycart.setVisibility(View.VISIBLE);
                } else {
                    mRecylerview.setVisibility(View.VISIBLE);
                    mRrBottom.setVisibility(View.VISIBLE);
                    mImgEmptycart.setVisibility(View.GONE);
                }

                if (dataJson.length() == 0) {
                    mRecylerview.setVisibility(View.GONE);
                    mRrBottom.setVisibility(View.GONE);
                    mImgEmptycart.setVisibility(View.VISIBLE);
                } else {
                    mRecylerview.setVisibility(View.VISIBLE);
                    mRrBottom.setVisibility(View.VISIBLE);
                    mImgEmptycart.setVisibility(View.GONE);
                }

                for (int i = 0; i < dataJson.length(); i++) {
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);
                    CartAlbumBeans cartAlbumBeans = new CartAlbumBeans();
                    cartAlbumBeans.set_id(jsonObject1.getString(S._id));
                    cartAlbumBeans.setUserId(jsonObject1.getString(S.userId));

                    AlbumIdBeans albumIdBeans = new AlbumIdBeans();
                    JSONObject albumIdJson = jsonObject1.getJSONObject(S.albumId);
                    albumIdBeans.set_id(albumIdJson.getString(S._id));
                    albumIdBeans.setUserId(albumIdJson.getString(S.userId));
                    albumIdBeans.setTrackId(albumIdJson.getString(S.trackId));
                    albumIdBeans.setEventId(albumIdJson.getString(S.eventId));
                    albumIdBeans.setModified(albumIdJson.getString(S.modified));
                    albumIdBeans.setCreated(albumIdJson.getString(S.created));
                    albumIdBeans.setStatus(albumIdJson.getString(S.status));
                    albumIdBeans.setAlbumPrice(albumIdJson.getString(S.albumPrice));
                    albumIdBeans.setDateOfEvent(albumIdJson.getString(S.dateOfEvent));
                    albumIdBeans.setName(albumIdJson.getString(S.name));
                    cart_total += Double.parseDouble(albumIdJson.getString("albumPrice"));


                    cartAlbumBeans.setAlbumIdBeans(albumIdBeans);

                    JSONArray albumImageJson = jsonObject1.getJSONArray(S.userAlbumImage);
                    List<CartAlbumImagesBeans> cartAlbumImagesBeansList = new ArrayList<>();
                    for (int j = 0; j < albumImageJson.length(); j++) {
                        JSONObject jsonObject2 = albumImageJson.getJSONObject(j);
                        CartAlbumImagesBeans cartAlbumImagesBeans = new CartAlbumImagesBeans();
                        cartAlbumImagesBeans.set_id(jsonObject2.getString(S._id));
                        cartAlbumImagesBeans.setUserId(jsonObject2.getString(S.userId));
                        cartAlbumImagesBeans.setAlbumId(jsonObject2.getString(S.albumId));
                        cartAlbumImagesBeans.setStatus(jsonObject2.getString(S.status));
                        cartAlbumImagesBeans.setType(jsonObject2.getString(S.type));
                        cartAlbumImagesBeans.setThumbName(jsonObject2.getString(S.thumbName));
                        cartAlbumImagesBeans.setFileName(jsonObject2.getString(S.fileName));
                        cartAlbumImagesBeansList.add(cartAlbumImagesBeans);
                    }
                    cartAlbumBeans.setCartAlbumImagesBeanses(cartAlbumImagesBeansList);
                    mAlbumBeansList.add(cartAlbumBeans);
                }
                mCartAlbumAdpter.notifyDataSetChanged();
                mTxtCarttotal.setText(getString(R.string.dollar) + Util.DisplayAmount(cart_total + ""));
            } else {
                mRecylerview.setVisibility(View.GONE);
                mRrBottom.setVisibility(View.GONE);
                mImgEmptycart.setVisibility(View.VISIBLE);
                //Util.showAlertDialog(mContext, getString(R.string.alert), msg);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void deleteCartAlbum(int pos) {
        mAuthAPI.removeCartAlbum(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mAlbumBeansList.get(pos).getAlbumIdBeans().get_id(), mAlbumBeansList.get(pos).get_id());
    }

    /* removeCartAlbum webservice response  */
    public void removeCartAlbumWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            mAlbumBeansList.clear();
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, S.removeCartAlbum_api);
                //callViewCartAblum();

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null && data.getStringExtra(S.stripeToken) != null) {
            String stripeToken = data.getStringExtra(S.stripeToken);
            albumCheckout(stripeToken);
        }
    }

    // Call webservice albumCheckout
    public void albumCheckout(String stripeToken) {
        mAuthAPI.albumCheckout(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), String.valueOf(cart_total), stripeToken);
    }

    /* albumCheckout webservice response */
    public void albumCheckoutWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg,S.albumCheckout_api);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rr_bottom:
                Intent intent = new Intent(CartAlbumActivity.this, AddCardDetail.class);
                intent.putExtra(S.amount, String.valueOf(cart_total));
                intent.putExtra(S.type, I.ALBUMPAYMET + "");
                startActivityForResult(intent, 1);
                break;
        }
    }
}