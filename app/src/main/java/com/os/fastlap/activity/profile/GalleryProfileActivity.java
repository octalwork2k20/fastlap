package com.os.fastlap.activity.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.activity.group.GroupProfileActivity;
import com.os.fastlap.activity.track.TrackDetailActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.beans.BeanGallery;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.fragment.gallery.PhotoFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/13/2017
 */

public class GalleryProfileActivity extends BaseActivity implements View.OnClickListener {

    private TabLayout tab_layout;
    private ViewPager viewpager;
    private ViewPagerAdapter adapter;
    ImageView base_toggle_icon;
    Context mContext;
    private AppCompatImageView baseToggleIcon;
    private View view;
    private CoordinatorLayout htabMaincontent;
    private AppBarLayout htabAppbar;
    private CoordinatorLayout coordinatorLayout;
    private RelativeLayout rr;
    private ImageView imgBg;
    private CircleImageView userIcon;
    private RelativeLayout parentAbout;
    private TextViewPlayRegular txtInfo;
    private TabLayout tabHost;
    private LinearLayout footer;
    ImageView add_album;
    TextView txtUsername,txtEmail;
    public static ArrayList<BeanGallery> galleryList;
    AuthAPI authAPI;
    int pageType;
    String profileID = "";
    String profileName = "";
    String profileImage = "";
    String profileCoverImage = "";
    private String TAG = GalleryProfileActivity.class.getSimpleName();
    TextView menu_menu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_gallery_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();

        pageType = getIntent().getIntExtra(S.type, 0);

    }

    private void initView() {
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        view = (View) findViewById(R.id.view);
        add_album = (ImageView) findViewById(R.id.add_album);
        htabMaincontent = (CoordinatorLayout) findViewById(R.id.htab_maincontent);
        htabAppbar = (AppBarLayout) findViewById(R.id.htab_appbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        rr = (RelativeLayout) findViewById(R.id.rr);
        imgBg = (ImageView) findViewById(R.id.img_bg);
        userIcon = (CircleImageView) findViewById(R.id.user_icon);
        parentAbout = (RelativeLayout) findViewById(R.id.parent_about);
        txtInfo = (TextViewPlayRegular) findViewById(R.id.txt_info);
        footer = (LinearLayout) findViewById(R.id.footer);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        tab_layout = (TabLayout) findViewById(R.id.tab_host);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        //txtEmail=(TextView)findViewById(R.id.txt_email);
        galleryList = new ArrayList<>();
        galleryList.clear();

        setupViewPager(viewpager);
        tab_layout.setupWithViewPager(viewpager);

        clickListner();
        Util.changeTabsFont(tab_layout, this);

    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
        add_album.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }

    private void setProfileData() {
        if (pageType == I.GROUPGALLERY) {
            profileID = GroupProfileActivity.profileGroupId;
            profileName = GroupProfileActivity.profileGroupName;
            profileImage = GroupProfileActivity.profileGroupProfileImage;
            profileCoverImage = GroupProfileActivity.profileGroupCoverImage;
            authAPI.getGroupGallery(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), profileID);
        } else if (pageType == I.TRACKGALLERY) {
            profileID = TrackDetailActivity.trackId;
            profileName = TrackDetailActivity.trackName;
            profileImage = TrackDetailActivity.trackImage;
            profileCoverImage = TrackDetailActivity.trackCoverImage;
            authAPI.getTrackGallery(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), profileID);
        } else if (pageType == I.USERGALLERY) {
            profileID = ProfileActivity.profileUserId;
            profileName = ProfileActivity.profileUserName;
            profileImage = ProfileActivity.profileUserProfileImage;
            profileCoverImage = ProfileActivity.profileUserCoverImage;
            authAPI.getUserGallery(mContext, profileID);

            if (profileID.compareToIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)) == 0) {
                add_album.setVisibility(View.VISIBLE);
            }
        }

        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + profileCoverImage, imgBg, Util.getImageLoaderOption(mContext));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + profileImage, userIcon, Util.getImageLoaderOption(mContext));
        txtUsername.setText(profileName);
        //txtEmail.setText(ProfileActivity.profileEmail);

    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading(getString(R.string.gallery));
        FastLapApplication.mCurrentContext = mContext;
        setProfileData();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager()); //create adapter for viewpager
        adapter.addFragment(new PhotoFragment(I.PHOTOGALLERY), getString(R.string.photo)); //add fragments to adapter
        adapter.addFragment(new PhotoFragment(I.VIDEOGALLERY), getString(R.string.video));

        viewPager.setAdapter(adapter); //and finally set adapter to viewpager
        //  viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getCurrentFragment();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public Fragment getCurrentFragment() {
        Fragment mCurrentFragment = null;
        if (adapter != null && viewpager != null) {
            mCurrentFragment = adapter.getRegisteredFragment(viewpager.getCurrentItem());
            ((PhotoFragment) mCurrentFragment).setGalleryData();
        }

        return mCurrentFragment;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.add_album:
                Intent intent = new Intent(mContext, AddAlbumActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_menu:
                Intent intent2 = new Intent(this, MenuActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                finish();
                break;
        }
    }

    public void galleryListWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                galleryList.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanGallery beanGallery = new BeanGallery();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanGallery.setId(jsonObject1.getString(S._id));
                    beanGallery.setUserId(jsonObject1.getString(S.userId));
                    beanGallery.setPostId(jsonObject1.getString(S.postId));
                    beanGallery.setCreated(jsonObject1.getString(S.created));
                    beanGallery.setStatus(jsonObject1.getString(S.status));
                    beanGallery.setType(jsonObject1.getString(S.type));
                    beanGallery.setThumbName(jsonObject1.getString(S.thumbName));
                    beanGallery.setFileName(jsonObject1.getString(S.fileName));
                    galleryList.add(beanGallery);
                }
                getCurrentFragment();
            }/* else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
