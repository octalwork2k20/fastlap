package com.os.fastlap.activity.dashboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.dashboard.ShareFriendsUserAdapter;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

/**
 * Created by anandj on 11/15/2017.
 */

public class ShareFriendsListActivity extends BaseActivity implements View.OnClickListener {

    private AppCompatImageView mBaseToggleIcon;
    private TextViewPlayBold mToolbarheading;
    private View mView;
    private RelativeLayout mActionBar;
    private RecyclerView mLikesRecyclerView;
    private TextViewPlayRegular mDashboardMenu;
    private TextViewPlayRegular mChatMenu;
    private TextViewPlayRegular mRecordMenu;
    private TextViewPlayRegular mNotificationMenu;
    private TextViewPlayRegular mMenuMenu;
    private LinearLayout mBottomLl;
    ShareFriendsUserAdapter likesUserAdapter;
    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.likes_user_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        initView();
    }

    private void initView() {
        mBaseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        mToolbarheading = (TextViewPlayBold) findViewById(R.id.toolbarheading);
        mView = (View) findViewById(R.id.view);
        mActionBar = (RelativeLayout) findViewById(R.id.action_bar);
        mLikesRecyclerView = (RecyclerView) findViewById(R.id.likes_recyclerView);
        mDashboardMenu = (TextViewPlayRegular) findViewById(R.id.dashboard_menu);
        mChatMenu = (TextViewPlayRegular) findViewById(R.id.chat_menu);
        mRecordMenu = (TextViewPlayRegular) findViewById(R.id.record_menu);
        mNotificationMenu = (TextViewPlayRegular) findViewById(R.id.notification_menu);
        mMenuMenu = (TextViewPlayRegular) findViewById(R.id.menu_menu);
        mBottomLl = (LinearLayout) findViewById(R.id.bottom_ll);


        likesUserAdapter = new ShareFriendsUserAdapter((ArrayList<DashboardPostShareUserModal>) getIntent().getSerializableExtra(S.user_list), mContext);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        mLikesRecyclerView.setLayoutManager(layoutManager);
        mLikesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLikesRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
        mLikesRecyclerView.setAdapter(likesUserAdapter);

        clickListner();
    }

    private void clickListner() {
        mBaseToggleIcon.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.DASHBOARD_SCREEN);
        setToolbarHeading(getString(R.string.shared_friends));
        FastLapApplication.mCurrentContext = mContext;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }
}
