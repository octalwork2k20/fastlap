package com.os.fastlap.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.os.fastlap.R;
import com.os.fastlap.adapter.ChatAdapter;
import com.os.fastlap.beans.BeanMessage;
import com.os.fastlap.beans.Bean_UserDetail;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.database.ChatDbHelper;
import com.os.fastlap.data.database.DatabaseManager;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by abhinava on 7/12/2017
 */

public class ChatActivity extends BaseActivity implements View.OnClickListener {


    private RecyclerView chatRecyclerView;
    private EditText msgEt;
    private TextView typing_tv;
    private ImageButton smileyIconBtn;
    private ImageButton sendImageBtn;
    private ChatAdapter mChatUserAdapter;
    private Context context;
    public ArrayList<BeanMessage> messageList = new ArrayList<>();
    ImageView base_toggle_icon;
    private Boolean isConnected = true;

    private boolean mTyping = false;
    private static final int TYPING_TIMER_LENGTH = 600;
    private Handler mTypingHandler = new Handler();

    private Socket mSocket;
    private static final String TAG = ChatActivity.class.getSimpleName();
    public String chatChannelId = "";
    private String chatUserId = "";
    private String chatUserName = "";
    private String chatUserImage = "";
    private String chatType = "";
    private boolean isOnLine =false;
    private ArrayList<Bean_UserDetail> joinUserList;
    private DatabaseManager databaseManager;
    private Cursor cursor;
    private static final String INSERTION_COMPLETE_BROADCAST = "com.os.fastlap.activity.INSERTION_COMPLETE_BROADCAST";
    private InsertItemBroadcastReceiver insertItemBroadcastReceiver;
    private TextView userStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        chatChannelId = getIntent().getStringExtra(S.friendId);
        chatUserId = getIntent().getStringExtra(S.userId);
        chatUserName = getIntent().getStringExtra(S.username);
        chatUserImage = getIntent().getStringExtra(S.image);
        chatType = getIntent().getStringExtra(S.CHAT_TYPE);
        isOnLine = getIntent().getBooleanExtra(S.ONLINE_STATUS,false);
        socketInit();
        initView();
        insertItemBroadcastReceiver = new InsertItemBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(insertItemBroadcastReceiver, new IntentFilter(INSERTION_COMPLETE_BROADCAST));
        databaseManager = DatabaseManager.getInstance(this);
        new AsyncTaskRunner().execute();
    }

    private void socketInit() {
        FastLapApplication app = (FastLapApplication) getApplication();
        mSocket = app.getSocket();
        if (mSocket.connected() && mSocket!=null){
            mSocket.emit(S.JOIN_EVENT, chatChannelId, MySharedPreferences.getPreferences(context, S.user_id));
            mSocket.on(S.MESSAGE_RECIEVE_EVENT, onAddMessage);
            mSocket.on(S.NEWUSER_JOIN_EVENT, onUserJoined);
            mSocket.on(S.USER_LEFT_EVENT, onUserLeft);
            mSocket.on(S.USER_TYPING, onTyping);
            mSocket.on(S.USER_STOP_TYPING, onStopTyping);
            mSocket.on(S.USER_REMOVE, removeUser);
            mSocket.on(S.UPDATE_USERS_LIST, onUpdateUserList);
            mSocket.on(S.messageDelivered, onMessageDelivered);
        }

        /*mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);*/
        //  mSocket.connect();
        // mSocket.disconnect();
    }

    private void initView() {
        userStatus = findViewById(R.id.userStatus);
        if(isOnLine){
            userStatus.setVisibility(View.VISIBLE);
            userStatus.setText("Online");
        }
        else {
            userStatus.setVisibility(View.GONE);
            userStatus.setText("Offline");
        }
        chatRecyclerView = findViewById(R.id.chat_recyclerView);
        msgEt = findViewById(R.id.msg_et);
        typing_tv = findViewById(R.id.typing_tv);
        smileyIconBtn = findViewById(R.id.smiley_icon_btn);
        sendImageBtn = (ImageButton) findViewById(R.id.send_image_btn);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        joinUserList = new ArrayList<>();
        joinUserList.clear();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setStackFromEnd(true);
        chatRecyclerView.setLayoutManager(linearLayoutManager);
        chatRecyclerView.setHasFixedSize(true);
        mChatUserAdapter = new ChatAdapter(mContext, messageList);
        chatRecyclerView.setAdapter(mChatUserAdapter);
        chatRecyclerView.scrollToPosition(messageList.size() - 1);
        clickListner();
        if (isConnected) {
            mSocket.emit(S.JOIN_EVENT, MySharedPreferences.getPreferences(context, S.user_id));
        }
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        sendImageBtn.setOnClickListener(this);

        msgEt.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (null == chatChannelId) return;
                if (!mSocket.connected()) return;

                if (!mTyping) {
                    mTyping = true;
                    mSocket.emit(S.USER_TYPING, chatChannelId, MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
                }

                mTypingHandler.removeCallbacks(onTypingTimeout);
                mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);
            }

            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(chatUserName);
        FastLapApplication.activityResumed();
        FastLapApplication.mCurrentContext = context;
    }

    @Override
    protected void onPause() {
        super.onPause();
        FastLapApplication.activityPaused();
    }

    private void addNewMessage(JSONObject jsonObject) {
        try {
            if (!databaseManager.checkMessageAlreayExitOrNot(jsonObject.getString(S._id))) {
                BeanMessage beanMessage = new BeanMessage();
                beanMessage.setMessageText(jsonObject.getString(S.content));
                beanMessage.setMessageDate(jsonObject.getString(S.date));
                beanMessage.setMessageSenderId(MySharedPreferences.getPreferences(context, S.user_id));
                beanMessage.setMessageSenderName(MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
                beanMessage.setMessageSenderImage(MySharedPreferences.getPreferences(context, S.image));
                beanMessage.setChannelId(jsonObject.getString(S.channelId));
                beanMessage.setMessageId(jsonObject.getString(S._id));
                beanMessage.setChatType(jsonObject.getString(S.CHAT_TYPE));
                if (jsonObject.getString(S.CHAT_TYPE).compareToIgnoreCase("G") == 0) {
                    beanMessage.setMessageReciverId(jsonObject.getString(S.groupId));
                    beanMessage.setMessageReciverImage(jsonObject.getString(S.groupImage));
                    beanMessage.setMessageReciverName(jsonObject.getString(S.groupName));
                } else {
                    beanMessage.setMessageReciverId(chatUserId);
                    beanMessage.setMessageReciverImage(chatUserImage);
                    beanMessage.setMessageReciverName(chatUserName);
                }

                databaseManager.chatSaveInDB(beanMessage);
                Intent customBroadcastIntent = new Intent(INSERTION_COMPLETE_BROADCAST);
                Bundle bundle = new Bundle();
                bundle.putSerializable("value", beanMessage);
                customBroadcastIntent.putExtras(bundle);
                LocalBroadcastManager.getInstance(this).sendBroadcast(customBroadcastIntent);
                databaseManager.chatSaveLastMessageInDB(jsonObject.getString(S.channelId), jsonObject.getString(S.content), chatUserId, chatUserName, chatUserImage, jsonObject.getString(S.date) + "", 0, jsonObject.getString(S.CHAT_TYPE), "M");
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.send_image_btn:
                String content = msgEt.getText().toString().trim();

                if (!content.isEmpty()) {
                    /*byte[] textdata = new byte[0];
                    try {
                        textdata = content.getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    content = Base64.encodeToString(textdata, Base64.NO_WRAP);*/

                    long date = System.currentTimeMillis();

                    try {
                        Log.e("size", messageList.size() + "");
                        JSONObject data = new JSONObject();
                        data.put(S.CHAT_SENDER_NAME, MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
                        data.put(S.CHAT_MESSAGE_TEXT, content);
                        data.put(S.CHAT_SENDER_IMAGE, MySharedPreferences.getPreferences(context, S.image));
                        data.put(S.CHAT_SENDER_ID, MySharedPreferences.getPreferences(context, S.user_id));
                        data.put(S.CHAT_CHANNEL_ID, chatChannelId);
                        data.put(S.CHAT_DATE, date + "");
                        data.put(S.CHAT_TIMEBIN, chatChannelId);
                        data.put(S.CHAT_TYPE, chatType);

                        if (chatType.compareToIgnoreCase("G") == 0) {
                            data.put(S.CHAT_GROUP_ID, chatUserId);
                            data.put(S.CHAT_GROUP_NAME, chatUserName);
                            data.put(S.CHAT_GROUP_IMAGE, chatUserImage);
                        } else {
                            data.put(S.CHAT_GROUP_ID, "");
                            data.put(S.CHAT_GROUP_NAME, "");
                            data.put(S.CHAT_GROUP_IMAGE, "");
                        }

                        mSocket.emit(S.MESSAGE_SEND_EVENT, chatChannelId, data);
                        msgEt.setText("");
                    } catch (JSONException e) {
                        Log.e(TAG, e.toString());
                    }
                }
                break;
        }
    }

    private Emitter.Listener onMessageDelivered = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("json", args[0] + "");
            try {
                JSONObject jsonObject = (JSONObject) args[0];
                addNewMessage(jsonObject);

                //   refreshList();

            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
    };

   /* private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = true;
                    Toast.makeText(mContext, "connected", Toast.LENGTH_SHORT).show();
                    mSocket.emit(S.JOIN_EVENT, chatChannelId, MySharedPreferences.getPreferences(context, S.user_id));
                    //  mSocket.emit(S.JOIN_EVENT, "59d478b564df51258c26aa78", MySharedPreferences.getPreferences(context, S.user_id));
                }
            });
        }
    };*/

    private Emitter.Listener onUpdateUserList = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "onUpdateUserList" + " " + args[0]);

                    try {
                        JSONArray jsonArray = new JSONArray();
                        jsonArray = (JSONArray) args[0];
                        Log.e("sChat",""+jsonArray.toString());
                        joinUserList.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject = jsonArray.getJSONObject(i);
                            Bean_UserDetail bean_userDetail = new Bean_UserDetail();
                            bean_userDetail.setUser_id(jsonObject.getString(S._id));
                            bean_userDetail.setEmail(jsonObject.getString(S.email));
                            bean_userDetail.setUser_name(jsonObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject.getJSONObject(S.personalInfo).getString(S.lastName_response));
                            bean_userDetail.setProfile_image(jsonObject.getJSONObject(S.personalInfo).getString(S.image));
                            joinUserList.add(bean_userDetail);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }

                }
            });
        }
    };

    /*  private Emitter.Listener onDisconnect = new Emitter.Listener() {
          @Override
          public void call(Object... args) {
              runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      Log.i(TAG, "diconnected");
                      isConnected = false;
                      Toast.makeText(getApplicationContext(),
                              R.string.disconnect, Toast.LENGTH_LONG).show();
                  }
              });
          }
      };

      private Emitter.Listener onConnectError = new Emitter.Listener() {
          @Override
          public void call(Object... args) {
              runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      Log.e(TAG, "Error connecting");
                      Toast.makeText(getApplicationContext(),
                              R.string.error_connect, Toast.LENGTH_LONG).show();
                  }
              });
          }
      };*/
    private Emitter.Listener onAddMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];

                    onRecieveMsg(data);
                    // Log.d("chatmessage", data + "");

                    // removeTyping(username);
                    //addMessage(username, message);
                }
            });
        }
    };


    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString(S.CHAT_SENDER_NAME);
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }

                   /* addLog(getResources().getString(R.string.message_user_joined, username));
                    addParticipantsLog(numUsers);*/
                }
            });
        }
    };
    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString(S.CHAT_SENDER_NAME);
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }

                  /*  addLog(getResources().getString(R.string.message_user_left, username));
                    addParticipantsLog(numUsers);
                    removeTyping(username);*/
                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        if (data.getString(S.roomId).equalsIgnoreCase(chatChannelId)) {
                            username = data.getString(S.CHAT_SENDER_USERNAME);
                            typing_tv.setText(username + " " + getString(R.string.is_typing));
                            typing_tv.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        // return;
                    }
                    // addTyping(username);
                }
            });
        }
    };
    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        if (data.getString(S.roomId).equalsIgnoreCase(chatChannelId)) {
                            username = data.getString(S.CHAT_SENDER_USERNAME);
                            typing_tv.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    //removeTyping(username);
                }
            });
        }
    };

    private Emitter.Listener removeUser=new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("tag","removeUser :"+args.toString());
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            stopTyping();

        }
    };

    private void stopTyping() {
        if (!mTyping) return;

        mTyping = false;
        mSocket.emit(S.USER_STOP_TYPING, chatChannelId, MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
    }

    public void refreshList() {
      /*  runOnUiThread(new Runnable() {
            @Override
            public void run() {*/
        mChatUserAdapter.notifyDataSetChanged();
        chatRecyclerView.smoothScrollToPosition(mChatUserAdapter.getItemCount());
//stuff that updates ui
/*
            }
        });*/

    }
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        AsyncTaskRunner() {
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                cursor = databaseManager.queryInsectsById(ChatDbHelper.TABLE_MESSAGE, ChatDbHelper.CHANNEL_ID, chatChannelId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            /*set adapter*/
            if (cursor != null) {
                //int count = cursor.getCount();
                if (cursor.moveToFirst()) {
                    messageList.clear();
                    do {

                        BeanMessage beanMessage = new BeanMessage();
                        beanMessage.setMessageId(cursor.getString(cursor.getColumnIndex(ChatDbHelper._ID)));
                        beanMessage.setMessageText(cursor.getString(cursor.getColumnIndex(ChatDbHelper.MESSAGE)));
                        beanMessage.setMessageDate(cursor.getString(cursor.getColumnIndex(ChatDbHelper.DATETIME)));
                        beanMessage.setIsReadStatus(cursor.getInt(cursor.getColumnIndex(ChatDbHelper.IS_READ)));
                        beanMessage.setMessageSenderId(cursor.getString(cursor.getColumnIndex(ChatDbHelper.SENDER_ID)));
                        beanMessage.setMessageReciverId(cursor.getString(cursor.getColumnIndex(ChatDbHelper.RECIVER_ID)));
                        beanMessage.setChannelId(cursor.getString(cursor.getColumnIndex(ChatDbHelper.CHANNEL_ID)));
                        beanMessage.setChatType(cursor.getString(cursor.getColumnIndex(ChatDbHelper.MESSAGE_TYPE)));

                        Cursor userCursor = databaseManager.queryInsectsById(ChatDbHelper.TABLE_USER, ChatDbHelper.USER_ID, cursor.getString(cursor.getColumnIndex(ChatDbHelper.SENDER_ID)));
                        userCursor.moveToFirst();

                        beanMessage.setMessageSenderName(userCursor.getString(userCursor.getColumnIndex(ChatDbHelper.USER_NAME)));
                        beanMessage.setMessageSenderImage(userCursor.getString(userCursor.getColumnIndex(ChatDbHelper.USER_IMAGE)));

                        userCursor = databaseManager.queryInsectsById(ChatDbHelper.TABLE_USER, ChatDbHelper.USER_ID, cursor.getString(cursor.getColumnIndex(ChatDbHelper.RECIVER_ID)));
                        userCursor.moveToFirst();

                        beanMessage.setMessageReciverImage(userCursor.getString(userCursor.getColumnIndex(ChatDbHelper.USER_IMAGE)));
                        beanMessage.setMessageReciverName(userCursor.getString(userCursor.getColumnIndex(ChatDbHelper.USER_NAME)));
                        messageList.add(beanMessage);
                    } while (cursor.moveToNext());
                }

                mChatUserAdapter.notifyDataSetChanged();
                cursor.close();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
     /*   mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);*/
        mSocket.off(S.MESSAGE_RECIEVE_EVENT, onAddMessage);
        mSocket.off(S.NEWUSER_JOIN_EVENT, onUserJoined);
        mSocket.off(S.USER_LEFT_EVENT, onUserLeft);
        mSocket.off(S.USER_STOP_TYPING, onStopTyping);
        mSocket.off(S.UPDATE_USERS_LIST, onUpdateUserList);
        mSocket.off(S.messageDelivered, onMessageDelivered);
    }

    public class InsertItemBroadcastReceiver extends BroadcastReceiver {
        private String intentAction;

        @Override
        public void onReceive(Context context, Intent intent) {
            intentAction = intent.getAction();
            if (intentAction == INSERTION_COMPLETE_BROADCAST) {
                Bundle bundle = intent.getExtras();
                messageList.add((BeanMessage) bundle.getSerializable("value"));
                refreshList();

            }
        }
    }

    public void onRecieveMsg(JSONObject data) {
        try {
            String roomId = data.getString(S.CHAT_CHANNEL_ID);

            String username = data.getString(S.CHAT_SENDER_NAME);
            String content = data.getString(S.CHAT_MESSAGE_TEXT);
            String image = data.getString(S.CHAT_SENDER_IMAGE);
            String userId = data.getString(S.CHAT_SENDER_ID);
            String date = data.getString(S.CHAT_DATE);
            String timebin = data.getString(S.CHAT_TIMEBIN);
            String chattype = data.getString(S.CHAT_TYPE);
            String msgId = data.getString(S._id);
            String notificationTitle = data.getString("notificationTitle");

            if (!databaseManager.checkMessageAlreayExitOrNot(msgId)) {
                BeanMessage beanMessage = new BeanMessage();
                beanMessage.setChannelId(roomId);
                beanMessage.setMessageText(content);
                beanMessage.setMessageDate(date);
                beanMessage.setMessageTimebin(timebin);
                beanMessage.setMessageSenderId(userId);
                beanMessage.setMessageSenderName(username);
                beanMessage.setMessageSenderImage(image);
                beanMessage.setMessageReciverId(MySharedPreferences.getPreferences(context, S.user_id));
                beanMessage.setMessageReciverImage(MySharedPreferences.getPreferences(context, S.image));
                beanMessage.setMessageReciverName(MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
                beanMessage.setChatType(chattype);
                beanMessage.setMessageId(msgId);

                databaseManager.chatSaveInDB(beanMessage);

                if (chattype.compareToIgnoreCase("G") == 0) {
                    userId = data.getString(S.CHAT_GROUP_ID);
                    username = data.getString(S.CHAT_GROUP_NAME);
                    image = data.getString(S.CHAT_GROUP_IMAGE);
                }

                if (roomId.compareTo(chatChannelId) == 0)
                {
                    if (FastLapApplication.isActivityVisible())
                    {
                        Intent customBroadcastIntent = new Intent(INSERTION_COMPLETE_BROADCAST);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("value", beanMessage);
                        customBroadcastIntent.putExtras(bundle);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(customBroadcastIntent);

                        databaseManager.chatSaveLastMessageInDB(roomId, content, userId, username, image, date, 0, chattype, "O");
                    } else {
                                   /* FastLapApplication fastLapApplication = (FastLapApplication) getApplication();
                                     fastLapApplication.openInnerPNDialog(beanMessage);*/
                        databaseManager.chatSaveLastMessageInDB(roomId, content, userId, username, image, date, 1, chattype, "O");
                        SendNotification(roomId, userId, username, image, chattype, content, notificationTitle);
                    }

                } else {
                             /*   FastLapApplication fastLapApplication = (FastLapApplication) getApplication();
                                  fastLapApplication.openInnerPNDialog(beanMessage);*/
                    databaseManager.chatSaveLastMessageInDB(roomId, content, userId, username, image, date, 1, chattype, "O");
                    SendNotification(roomId, userId, username, image, chattype, content, notificationTitle);
                }
            }


        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

    }

    public void SendNotification(String roomId, String userId, String username, String image, String chattype, String content, String notificationTitle) {
        int id = (int) (System.currentTimeMillis() * (int) (Math.random() * 100));

        Intent notificationIntent = new Intent(this, ChatActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(DashboardActivity.class);
        Bundle notification_bundle = new Bundle();
        notification_bundle.putString(S.friendId, roomId);
        notification_bundle.putString(S.userId, userId);
        notification_bundle.putString(S.username, username);
        notification_bundle.putString(S.image, image);
        notification_bundle.putString(S.CHAT_TYPE, chattype);
        notificationIntent.putExtras(notification_bundle);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder;

        String notificationMessage = content;

        notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.noti_icon)
                .setContentTitle(notificationTitle)
                .setContentText(Html.fromHtml(notificationMessage))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setSubText("1" + " " + getString(R.string.new_message))
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(notificationMessage)))
                .setContentIntent(resultPendingIntent);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
    }
}
