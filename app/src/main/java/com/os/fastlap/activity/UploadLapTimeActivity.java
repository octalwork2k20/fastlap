package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.beans.BeanTrackVersion;
import com.os.fastlap.beans.BeanUploadData;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.delegates.NavigationDelegate;
import com.os.fastlap.fragment.uploadlaptime.UploadLapTimeStep1Fragment;
import com.os.fastlap.fragment.uploadlaptime.UploadLapTimeStep2Fragment;
import com.os.fastlap.fragment.uploadlaptime.UploadLapTimeStep3Fragment;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONObject;

/**
 * Created by abhinava on 7/17/2017.
 */

public class UploadLapTimeActivity extends BaseActivity implements NavigationDelegate, View.OnClickListener {
    private FrameLayout step1Fl;
    private TextView step1Tv;
    private FrameLayout step2Fl;
    private TextView step2Tv;
    private FrameLayout step3Fl;
    private TextView step3Tv;
    private FrameLayout frameLayout;
    public FragmentManager mFragmentManager = null;
    FragmentTransaction fragmentTransaction = null;
    public NavigationDelegate mNavigationDelegate;
    Fragment mFragment = null;
    public Context context;
    ImageView base_toggle_icon;
    public static BeanUploadData beanUploadData;
    private String TAG=UploadLapTimeActivity.class.getSimpleName();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_lap_time_activity);
        initView();

        mFragmentManager = getSupportFragmentManager();
        mNavigationDelegate = (NavigationDelegate) this;
        context = this;

        beanUploadData=new BeanUploadData();
        FastLapApplication.mCurrentContext = context;

        String fileName = getIntent().getStringExtra(S.fileName);
        String lapTime = getIntent().getStringExtra(S.lapTime);

        BeanTrackVersion beanTrackVersion= (BeanTrackVersion) getIntent().getSerializableExtra(S.userTrack);
        if (fileName != null)
        {
            beanUploadData.setFilePath(fileName);
            beanUploadData.setLapTime(lapTime);
            beanUploadData.setTrackId(beanTrackVersion.getTrackId());
            beanUploadData.setTrackName(beanTrackVersion.getTrackName());
            beanUploadData.setTrackImage(beanTrackVersion.getTrackImage());
            beanUploadData.setTrackLocation(beanTrackVersion.getTrackLocation());
            displayView(FastLapConstant.UPLOADLAPTIMESTEP1FRAGMENT, beanTrackVersion.getVersionArrayList(), null);
        }
    }

    private void initView() {

        step1Fl = (FrameLayout) findViewById(R.id.step1_fl);
        step1Tv = (TextView) findViewById(R.id.step1_tv);
        step2Fl = (FrameLayout) findViewById(R.id.step2_fl);
        step2Tv = (TextView) findViewById(R.id.step2_tv);
        step3Fl = (FrameLayout) findViewById(R.id.step3_fl);
        step3Tv = (TextView) findViewById(R.id.step3_tv);
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);

        clickListner();
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
    }

    public void displayView(String fragmentName, Object obj, Object list) {
        fragmentTransaction = mFragmentManager.beginTransaction();

        if (fragmentName.equals(FastLapConstant.UPLOADLAPTIMESTEP1FRAGMENT)) {
            mFragment = UploadLapTimeStep1Fragment.newInstance(context, obj);
        }

        if (fragmentName.equals(FastLapConstant.UPLOADLAPTIMESTEP2FRAGMENT)) {
            mFragment = UploadLapTimeStep2Fragment.newInstance(context, obj);
        }
        if (fragmentName.equals(FastLapConstant.UPLOADLAPTIMESTEP3FRAGMENT)) {
            mFragment = UploadLapTimeStep3Fragment.newInstance(context, obj);
        }
        if (mFragment != null) {
            try {
                fragmentTransaction.replace(R.id.frame_layout, mFragment).addToBackStack(fragmentName);
                fragmentTransaction.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.RECORD_SCREEN);
        FastLapApplication.mCurrentContext = context;
        setToolbarHeading(getString(R.string.uploaded_lap_time));
    }

    @Override
    public void executeFragment(String fragmentName, Object obj) {

    }

    @Override
    public void goBack() {

    }

    public void setStep1Fragment() {
        step1Fl.setBackgroundResource(R.drawable.step_circle_background);
        step1Tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        step2Fl.setBackgroundResource(R.drawable.step_darkgray_circle_background);
        step2Tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));

        step3Fl.setBackgroundResource(R.drawable.step_darkgray_circle_background);
        step3Tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
    }

    public void setStep2Fragment() {
        step1Fl.setBackgroundResource(R.drawable.step_darkgray_circle_background);
        step1Tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));

        step2Fl.setBackgroundResource(R.drawable.step_circle_background);
        step2Tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        step3Fl.setBackgroundResource(R.drawable.step_darkgray_circle_background);
        step3Tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
    }

    public void setStep3Fragment() {
        step1Fl.setBackgroundResource(R.drawable.step_darkgray_circle_background);
        step1Tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));

        step2Fl.setBackgroundResource(R.drawable.step_darkgray_circle_background);
        step2Tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));

        step3Fl.setBackgroundResource(R.drawable.step_circle_background);
        step3Tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
    }

    @Override
    public void onBackPressed() {
        OnBackCross();
        }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                OnBackCross();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {

            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            if (currentFragment instanceof UploadLapTimeStep1Fragment)
            {
                ((UploadLapTimeStep1Fragment)currentFragment).onActivityResult(requestCode,resultCode,data);
            }

            if (currentFragment instanceof UploadLapTimeStep3Fragment)
            {
                ((UploadLapTimeStep3Fragment)currentFragment).onActivityResult(requestCode,resultCode,data);
            }
        }
    }

    public void getVehicleTyreBrandResponse(String response) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (currentFragment instanceof UploadLapTimeStep2Fragment) {
            ((UploadLapTimeStep2Fragment)currentFragment).getVehicleTyreBrandResponse(response);
        }
    }

    public void getVehicleTyreModelResponse(String response)
    {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (currentFragment instanceof UploadLapTimeStep2Fragment)
        {
            ((UploadLapTimeStep2Fragment)currentFragment).getVehicleTyreModelResponse(response);
        }
    }

    public void OnBackCross()
    {
        hideKeyboard();
        if (getSupportFragmentManager().getBackStackEntryCount() > 1)
        {
            getSupportFragmentManager().popBackStack();
        }
        else {
            Intent intent=new Intent(context,RecordCameraActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void getYoutubeURLData(String response,int position)
    {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (currentFragment instanceof UploadLapTimeStep3Fragment)
        {
            ((UploadLapTimeStep3Fragment)currentFragment).getYoutubeURLData(response,position);
        }
    }

    public void getUploadLapTimeDataResponse(String response)
    {
        if(UploadLapTimeActivity.beanUploadData.getFilePath()!=null){
            Util.deleteUploadedFile(context,UploadLapTimeActivity.beanUploadData.getFilePath());
        }
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(context,getString(R.string.alert), msg,"6");
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    //get responce from Base api
    public void checkLapTimeCSV(String response)
    {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (currentFragment instanceof UploadLapTimeStep1Fragment)
        {
            ((UploadLapTimeStep1Fragment)currentFragment).checkLapTimeCSV(response);
        }
    }

    public void vehicleTypeWebserviceResponse(String response)
    {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (currentFragment instanceof UploadLapTimeStep1Fragment)
        {
            ((UploadLapTimeStep1Fragment)currentFragment).vehicleTypeWebserviceResponse(response);
        }
    }

    public void lapNotFound() {
        finish();
        Intent intent = new Intent(this, RecordCameraActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void trackNotfound()
    {

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (currentFragment instanceof UploadLapTimeStep1Fragment)
        {
            ((UploadLapTimeStep1Fragment)currentFragment).trackNotfound();
        }


    }
}
