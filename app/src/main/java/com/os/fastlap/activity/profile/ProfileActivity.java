package com.os.fastlap.activity.profile;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.DashboardActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.adapter.dashboard.DashBoardPostsAdapter;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.beans.UserDataBeans;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardFeelingIdBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostImagesBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostListBean;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.beans.dashboardmodals.FriendIdBean;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.TrackData;
import com.os.fastlap.beans.gragemodals.VehicleBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.os.fastlap.util.constants.S.status;

/*
 * Created by anandj on 7/11/2017.
 */

public class ProfileActivity extends BaseActivity implements View.OnClickListener, OnImagePickerDialogSelect, DashBoardPostsAdapter.OnItemClickListener {

    private Context mContext;
    private AppCompatImageView toggle_icon;
    public static ImageView img_bg;
    private ImageView img_addfrd;
    private CircleImageView user_icon;
    private RelativeLayout parent_diary;
    private RelativeLayout parent_info;
    private RelativeLayout parent_garage;
    private RelativeLayout parent_laps;
    private RelativeLayout parent_friends;
    private RelativeLayout parent_gallery;
    private TextView txt_username,txtEmail;
    ImageView profile_edit_iv;
    ImageView cover_edit_iv;
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    PagerBean pagerBean = new PagerBean();
    int SELECT_IMAGE_TYPE = 0;
    AuthAPI authAPI;
    private RelativeLayout diaryView;
    private RecyclerView postListview;
    Bundle bundle;
    private String TAG = ProfileActivity.class.getSimpleName();
    public static String profileUserId = "";
    public static String profileUserName = "";
    public static String profileEmail = "";
    public static String profileUserProfileImage = "";
    public static String profileUserCoverImage = "";
    DashBoardPostsAdapter dashBoardPostsAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    String friendStatus;
    String userfriendId;
    private int currentPage = 1;
    private ArrayList<DashboardPostListBean> mPostListBeen = new ArrayList<>();
    private SharingDeleget sharingDeleget;
    private CallbackManager callbackManager;
    TextView menu_menu;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;

        onImagePickerDialogSelect = this;
        authAPI = new AuthAPI(mContext);
        initView();
        bundle = getIntent().getExtras();

        setToolbarHeading(getString(R.string.profile));
        FastLapApplication.mCurrentContext = mContext;

        if (bundle != null) {
            if (bundle.getInt(S.page) == I.DASHBOARD_SCREEN) {
                setBottomMenu(I.DASHBOARD_SCREEN);
            } else if (bundle.getInt(S.page) == I.MENU_SCREEN) {
                setBottomMenu(I.MENU_SCREEN);
            }
            profileUserId = bundle.getString(S.user_id);
            setProfileUser();

        }
        callRefreshDashboard(false, currentPage);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setProfileUser() {
        if (profileUserId.compareToIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)) == 0) {
            profileUserId = MySharedPreferences.getPreferences(mContext, S.user_id);
            setProfileData();
            img_addfrd.setVisibility(View.GONE);
            profile_edit_iv.setVisibility(View.VISIBLE);
            cover_edit_iv.setVisibility(View.VISIBLE);
        } else {
            profileUserId = bundle.getString(S.user_id);
            img_addfrd.setVisibility(View.VISIBLE);
            profile_edit_iv.setVisibility(View.GONE);
            cover_edit_iv.setVisibility(View.GONE);
            friendStatus = bundle.getString("status");
            userfriendId = bundle.getString(S.friendId);
            if (userfriendId == null) {
                userfriendId = "";
            }

            friendStatus();

        }

        authAPI.getUserAbout(mContext, profileUserId, MySharedPreferences.getPreferences(mContext, S.user_id));
    }

    public void friendStatus() {
        if (friendStatus != null) {
            if (friendStatus.compareTo("0") == 0) {
                img_addfrd.setVisibility(View.VISIBLE);
                img_addfrd.setImageResource(R.mipmap.adduser_white);
            } else if (friendStatus.compareTo("2") == 0) {
                img_addfrd.setVisibility(View.VISIBLE);
                img_addfrd.setImageResource(R.mipmap.adduser_white);
            } else if (friendStatus.compareTo("1") == 0) {
                img_addfrd.setVisibility(View.VISIBLE);
                img_addfrd.setImageResource(R.mipmap.removefriend);
            } else {
                img_addfrd.setVisibility(View.GONE);
            }
        }
    }

    private void setProfileData() {


        try {
            if(!profileUserCoverImage.equalsIgnoreCase("img/cover.png")){
                Util.setImageLoader(S.IMAGE_BASE_URL + ProfileActivity.profileUserCoverImage, img_bg, getApplicationContext());
            }
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + ProfileActivity.profileUserProfileImage, user_icon, Util.getImageLoaderOption(mContext));
            txt_username.setText(ProfileActivity.profileUserName);
            txtEmail.setText("("+ProfileActivity.profileEmail+")");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        /*try {
            if(!profileUserCoverImage.equalsIgnoreCase("img/cover.png"))
            Util.setImageLoader(S.IMAGE_BASE_URL + profileUserCoverImage, img_bg, getApplicationContext());
        }
        catch (Exception e){e.printStackTrace();
        }
       // Glide.with(mContext).load(S.IMAGE_BASE_URL + profileUserCoverImage).placeholder(getResources().getDrawable(R.drawable.image_placeholder)).into(img_bg);
        if(!profileUserProfileImage.equalsIgnoreCase("")&&profileUserProfileImage!=null)
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + profileUserProfileImage, user_icon, Util.getImageLoaderOption(mContext));
        else
            user_icon.setImageResource(R.drawable.nopic);

        txt_username.setText(profileUserName);
        txtEmail.setText("("+profileEmail+")");*/
    }

    private void initView() {
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        txt_username = (TextView) findViewById(R.id.txt_username);
        txtEmail=(TextView)findViewById(R.id.txt_email);
        img_addfrd = (ImageView) findViewById(R.id.img_addfrd);
        toggle_icon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        img_bg = (ImageView) findViewById(R.id.img_bg);
        user_icon = (CircleImageView) findViewById(R.id.user_icon);
        parent_diary = (RelativeLayout) findViewById(R.id.parent_diary);
        parent_info = (RelativeLayout) findViewById(R.id.parent_info);
        parent_garage = (RelativeLayout) findViewById(R.id.parent_garage);
        parent_laps = (RelativeLayout) findViewById(R.id.parent_laps);
        parent_friends = (RelativeLayout) findViewById(R.id.parent_friends);
        parent_gallery = (RelativeLayout) findViewById(R.id.parent_gallery);
        profile_edit_iv = (ImageView) findViewById(R.id.profile_edit_iv);
        cover_edit_iv = (ImageView) findViewById(R.id.cover_edit_iv);
        postListview = (RecyclerView) findViewById(R.id.post_listview);
        diaryView=(RelativeLayout)findViewById(R.id.diaryView);
        setOnclickListener();
        profile_edit_iv.setVisibility(View.VISIBLE);
        cover_edit_iv.setVisibility(View.VISIBLE);
        diaryView.setVisibility(View.VISIBLE);
        setPostListAdapter();
    }

    private void setOnclickListener() {
        parent_info.setOnClickListener(this);
        parent_diary.setOnClickListener(this);
        parent_garage.setOnClickListener(this);
        parent_laps.setOnClickListener(this);
        parent_friends.setOnClickListener(this);
        parent_gallery.setOnClickListener(this);
        toggle_icon.setOnClickListener(this);
        profile_edit_iv.setOnClickListener(this);
        cover_edit_iv.setOnClickListener(this);
        img_addfrd.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }
    private void setPostListAdapter(){
        dashBoardPostsAdapter = new DashBoardPostsAdapter(mPostListBeen, DashboardActivity.advertisementArrayList, mContext, sharingDeleget, this, this);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        postListview.setLayoutManager(mLinearLayoutManager);
        postListview.setNestedScrollingEnabled(false);
        postListview.setAdapter(dashBoardPostsAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_addfrd:
                if (friendStatus.compareTo("0") == 0) {
                    String status = "0";
                    authAPI.userFriend(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), profileUserId, status, "");
                } else if (friendStatus.compareTo("2") == 0) {
                    Util.showSnackBar(img_addfrd, getString(R.string.you_already_send_request));
                } else if (friendStatus.compareTo("1") == 0) {
                    String status = "4";
                    authAPI.userFriend(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), profileUserId, status, userfriendId);
                }
                break;
            case R.id.parent_info:
                Util.startNewActivity(ProfileActivity.this, InfoActivity.class, true);
                break;

            case R.id.parent_garage:
                Util.startNewActivity(ProfileActivity.this, GarageProfileActivity.class, true);
                break;
            case R.id.parent_laps:
                Util.startNewActivity(ProfileActivity.this, LapsProfileActivity.class, true);
                break;
            case R.id.parent_friends:
                Util.startNewActivity(ProfileActivity.this, FriendsProfileActivity.class, true);
                break;
            case R.id.parent_gallery:
                Intent intent1 = new Intent(mContext, GalleryProfileActivity.class);
                intent1.putExtra(S.type, I.USERGALLERY);
                startActivity(intent1);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;

            case R.id.profile_edit_iv:
                SELECT_IMAGE_TYPE = 0;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
                break;
            case R.id.cover_edit_iv:
                SELECT_IMAGE_TYPE = 1;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }

                break;
            case R.id.base_toggle_icon:
                finish();
                break;
            /*case R.id.parent_diary:
                Util.startNewActivity(ProfileActivity.this, DiaryActivity.class, true);
                break;*/
            case R.id.parent_diary:
                if(diaryView.getVisibility()==View.VISIBLE){
                    diaryView.setVisibility(View.GONE);
                }
                else {
                    diaryView.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.menu_menu:
                Intent intent = new Intent(this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;

        }
    }
    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {

        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(mContext, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        pagerBean = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }

    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {
        EasyImage.openGallery(this, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, ProfileActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i)
            {
                File imageFile = file;
                String imageAbsolutePath = imageFile.getAbsolutePath();
                String imageFileName = imageFile.getName();

                pagerBean = new PagerBean(imageFile, imageAbsolutePath, imageFileName, false, false, "");

                String uri = Uri.fromFile(imageFile).toString();
                String decoded = Uri.decode(uri);

                if (SELECT_IMAGE_TYPE == 0) {
                    ImageLoader.getInstance().displayImage(decoded, user_icon);
                    authAPI.UpdateImage(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), pagerBean, SELECT_IMAGE_TYPE);
                } else {
                   // img_bg.setImageBitmap(Util.loadBitmap(uri));
                    ImageLoader.getInstance().displayImage(decoded, img_bg);
                    authAPI.UpdateImage(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), pagerBean, SELECT_IMAGE_TYPE);
                }
            }


        });
    }

    /* update image webservice response*/
    public void updateImages(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
                if (SELECT_IMAGE_TYPE == 0) {
                    profileUserProfileImage=jsonObject.getString(S.img);
                    MySharedPreferences.setPreferences(mContext, jsonObject.getString(S.img), S.image);

                } else {
                    profileUserCoverImage=jsonObject.getString(S.coverImage);
                    MySharedPreferences.setPreferences(mContext, jsonObject.getString(S.coverImage), S.coverImage);
                }
                setProfileData();
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* user friends webservice response */
    public void userFriendsWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                friendStatus = "2";
                friendStatus();
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
              //  Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, S.userFriend_api);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* Response from user here you update ui according to status */
    public void callbackFromSuccessDialog() {

    }

    /* user about us webservice response */
    public void getUserAboutResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                JSONObject userJson = jsonObject1.getJSONObject(S.userdata);

                JSONObject personInfo = userJson.getJSONObject(S.personalInfo);

                profileUserName = personInfo.getString(S.firstName_response) + " " + personInfo.getString(S.lastName_response);
                profileEmail=userJson.has(S.email)?userJson.getString(S.email):"";
                profileUserProfileImage = personInfo.getString(S.image);
                profileUserCoverImage = personInfo.getString(S.coverImage);

                setProfileData();

                if (profileUserId.compareToIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)) != 0) {
                    friendStatus = jsonObject1.getJSONObject(S.friendData).getString(S.status);
                    friendStatus();
                } else {
                    if (userJson.has(S.groupTypeId))
                        MySharedPreferences.setPreferences(mContext, userJson.getString(S.groupTypeId), S.vehicleTypeId);
                }

            } /*else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void callRefreshDashboard(boolean refreshstatus, int currentPage) {
        authAPI.myPostListing(mContext, profileUserId, refreshstatus, String.valueOf(currentPage));
    }

    public void postListingWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success)
            {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    DashboardPostListBean dashboardPostListBean = new DashboardPostListBean();
                    dashboardPostListBean.set_id(jsonObject1.getString(S._id));

                    JSONObject userJson = jsonObject1.getJSONObject(S.userId);
                    UserDataBeans userDataBeans = new UserDataBeans();
                    userDataBeans.set_id(userJson.getString(S._id));
                    userDataBeans.setEmail(userJson.getString(S.email));
                    userDataBeans.setUsername(userJson.getString(S.nickname));

                    JSONObject personalInfoJson = userJson.getJSONObject(S.personalInfo);
                    PersonalInfo personalInfo = new PersonalInfo();
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    userDataBeans.setPersonalInfo(personalInfo);

                    dashboardPostListBean.setUserDataBeans(userDataBeans);
                    dashboardPostListBean.setStatus(jsonObject1.getInt(S.status));
                    dashboardPostListBean.setDescription(jsonObject1.getString(S.description));
                    dashboardPostListBean.setPosition_text(jsonObject1.getString(S.position_text));
                    dashboardPostListBean.setSpeed(jsonObject1.getString(S.speed));
                    dashboardPostListBean.setWeather(jsonObject1.getString(S.weather));

                    JSONArray feelingJsonArray = jsonObject1.getJSONArray(S.feelingId);
                    for (int j = 0; j < feelingJsonArray.length(); j++) {
                        JSONObject feelingJson = feelingJsonArray.getJSONObject(j);
                        DashboardFeelingIdBeans dashboardFeelingIdBeans = new DashboardFeelingIdBeans();
                        dashboardFeelingIdBeans.set_id(feelingJson.getString(S._id));
                        dashboardFeelingIdBeans.setStatus(feelingJson.getInt(S.status));
                        dashboardFeelingIdBeans.setUnicodes(feelingJson.getString(S.unicodes));
                        dashboardFeelingIdBeans.setName(feelingJson.getString(S.name));

                        dashboardPostListBean.setDashboardFeelingIdBeans(dashboardFeelingIdBeans);
                    }

                    JSONArray trackIdJsonArray = jsonObject1.getJSONArray(S.trackId);
                    for (int j = 0; j < trackIdJsonArray.length(); j++) {
                        JSONObject trackIdJson = trackIdJsonArray.getJSONObject(j);
                        TrackData trackData = new TrackData();
                        trackData.setId(trackIdJson.getString(S._id));
                        trackData.setOwnerId(trackIdJson.getString(S.ownerId));
                        trackData.setName(trackIdJson.getString(S.name));
                        trackData.setImage(trackIdJson.getString(S.image));
                        trackData.setTrackLength(trackIdJson.getInt(S.trackLength));
                        trackData.setCountry(trackIdJson.has(S.country)?trackIdJson.getString(S.country):"");

                        dashboardPostListBean.setTrackData(trackData);
                    }

                    JSONArray userVehicleIdJsonArray = jsonObject1.getJSONArray(S.userVehicleId);
                    for (int j = 0; j < userVehicleIdJsonArray.length(); j++) {
                        JSONObject vehicleJson = userVehicleIdJsonArray.getJSONObject(j);
                        BeanVehicleList beanVehicleList = new BeanVehicleList();
                        beanVehicleList.set_id(vehicleJson.getString(S._id));

                        VehicleTypeIdModal vehicleTypeIdModal = new VehicleTypeIdModal();
                        JSONObject vehicleTypeJson = vehicleJson.getJSONObject(S.vehicleTypeId);
                        vehicleTypeIdModal.set_id(vehicleTypeJson.getString(S._id));
                        vehicleTypeIdModal.setName(vehicleTypeJson.getString(S.name));

                        beanVehicleList.setVehicleTypeIdModal(vehicleTypeIdModal);

                        VehicleBrandIdModal vehicleBrandIdModal = new VehicleBrandIdModal();
                        JSONObject vehicleBrandJson = vehicleJson.getJSONObject(S.vehicleBrandId);
                        vehicleBrandIdModal.set_id(vehicleBrandJson.getString(S._id));
                        vehicleBrandIdModal.setVehicleBrandName(vehicleBrandJson.getString(S.vehicleBrandName));
                        vehicleBrandIdModal.setVehicleTypeId(vehicleBrandJson.getString(S.vehicleTypeId));

                        beanVehicleList.setVehicleBrandIdModal(vehicleBrandIdModal);

                        VehicleModelIdModal vehicleModelIdModal = new VehicleModelIdModal();
                        JSONObject vehicleModalJson = vehicleJson.getJSONObject(S.vehicleModelId);
                        vehicleModelIdModal.set_id(vehicleModalJson.getString(S._id));
                        vehicleModelIdModal.setVehicleBrandId(vehicleModalJson.getString(S.vehicleBrandId));
                        vehicleModelIdModal.setVehicleTypeId(vehicleModalJson.getString(S.vehicleTypeId));
                        vehicleModelIdModal.setVehicleModelName(vehicleModalJson.getString(S.vehicleModelName));

                        beanVehicleList.setVehicleModelIdModal(vehicleModelIdModal);
                        beanVehicleList.setDescription(vehicleJson.getString(S.description));

                        dashboardPostListBean.setBeanVehicleList(beanVehicleList);
                    }
                    JSONArray postImagesJsonArray = jsonObject1.getJSONArray(S.postImages);
                    List<DashboardPostImagesBeans> dashboardPostImagesBeanses = new ArrayList<>();
                    for (int j = 0; j < postImagesJsonArray.length(); j++) {
                        JSONObject postImageJson = postImagesJsonArray.getJSONObject(j);
                        DashboardPostImagesBeans dashboardPostImagesBeans = new DashboardPostImagesBeans();
                        dashboardPostImagesBeans.set_id(postImageJson.getString(S._id));
                        dashboardPostImagesBeans.setUserId(postImageJson.getString(S.userId));
                        dashboardPostImagesBeans.setPostId(postImageJson.getString(S.postId));
                        dashboardPostImagesBeans.setStatus(postImageJson.getInt(S.status));
                        dashboardPostImagesBeans.setType(postImageJson.getString(S.type));
                        dashboardPostImagesBeans.setFileName(postImageJson.getString(S.fileName));
                        dashboardPostImagesBeans.setThumbName(postImageJson.getString(S.thumbName));
                        dashboardPostImagesBeanses.add(dashboardPostImagesBeans);
                    }

                    JSONArray postShareUser = jsonObject1.getJSONArray(S.postShareUser);
                    List<DashboardPostShareUserModal> dashboardPostShareUserModals = new ArrayList<>();
                    for (int j = 0; j < postShareUser.length(); j++) {
                        JSONObject postShareUserJson = postShareUser.getJSONObject(j);
                        DashboardPostShareUserModal dashboardPostShareUserModal = new DashboardPostShareUserModal();
                        dashboardPostShareUserModal.setId(postShareUserJson.getString(S._id));
                        dashboardPostShareUserModal.setPostId(postShareUserJson.getString(S.postId));
                        dashboardPostShareUserModal.setModified(postShareUserJson.getString(S.modified));
                        dashboardPostShareUserModal.setCreated(postShareUserJson.getString(S.created));
                        JSONObject userIdJson = postShareUserJson.getJSONObject(S.userId);

                        UserDataBeans userDataBeans1 = new UserDataBeans();
                        userDataBeans1.set_id(userIdJson.getString(S._id));
                        userDataBeans1.setEmail(userIdJson.getString(S.email));
                        userDataBeans1.setUsername(userIdJson.getString(S.username));
                        JSONObject personalInfoJsonShare = userIdJson.getJSONObject(S.personalInfo);
                        PersonalInfo personalInfo1 = new PersonalInfo();
                        personalInfo1.setLanguage(personalInfoJsonShare.getString(S.language_response));
                        personalInfo1.setCoverImage(personalInfoJsonShare.getString(S.coverImage));
                        personalInfo1.setImage(personalInfoJsonShare.getString(S.image));
                        personalInfo1.setMobileNumber(personalInfoJsonShare.getString(S.mobileNumber_response));
                        personalInfo1.setDateOfBirth(personalInfoJsonShare.getString(S.dateOfBirth_response));
                        personalInfo1.setVoucherCode(personalInfoJsonShare.getString(S.voucherCode_response));
                        personalInfo1.setLastName(personalInfoJsonShare.getString(S.lastName_response));
                        personalInfo1.setFirstName(personalInfoJsonShare.getString(S.firstName_response));

                        userDataBeans1.setPersonalInfo(personalInfo1);
                        dashboardPostShareUserModal.setUserDataBeans(userDataBeans1);
                        dashboardPostShareUserModals.add(dashboardPostShareUserModal);
                    }

                    JSONArray commentPostTagFriend = jsonObject1.getJSONArray(S.commentPostTagFriend);
                    List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeanses = new ArrayList<>();
                    for (int j = 0; j < commentPostTagFriend.length(); j++) {
                        JSONObject friendJson = commentPostTagFriend.getJSONObject(j);
                        DashboardCommentPostTagFriendsBeans dashboardCommentPostTagFriendsBeans = new DashboardCommentPostTagFriendsBeans();
                        dashboardCommentPostTagFriendsBeans.set_id(friendJson.getString(S._id));
                        dashboardCommentPostTagFriendsBeans.setUserId(friendJson.getString(S.userId));
                        dashboardCommentPostTagFriendsBeans.setPostId(friendJson.getString(S.postId));

                        FriendIdBean friendIdBean = new FriendIdBean();
                        JSONObject frinedIdJson = friendJson.getJSONObject(S.friendId);
                        friendIdBean.set_id(frinedIdJson.getString(S._id));
                        friendIdBean.setUsername(frinedIdJson.getString(S.username));
                        friendIdBean.setEmail(frinedIdJson.getString(S.email));

                        PersonalInfo personalInfo1 = new PersonalInfo();
                        JSONObject personalInfoJsonInner = frinedIdJson.getJSONObject(S.personalInfo);
                        personalInfo1.setLanguage(personalInfoJsonInner.getString(S.language_response));
                        personalInfo1.setCoverImage(personalInfoJsonInner.getString(S.coverImage));
                        personalInfo1.setImage(personalInfoJsonInner.getString(S.image));
                        personalInfo1.setMobileNumber(personalInfoJsonInner.getString(S.mobileNumber_response));
                        personalInfo1.setDateOfBirth(personalInfoJsonInner.getString(S.dateOfBirth_response));
                        personalInfo1.setVoucherCode(personalInfoJsonInner.getString(S.voucherCode_response));
                        personalInfo1.setLastName(personalInfoJsonInner.getString(S.lastName_response));
                        personalInfo1.setFirstName(personalInfoJsonInner.getString(S.firstName_response));

                        friendIdBean.setPersonalInfo(personalInfo1);
                        dashboardCommentPostTagFriendsBeans.setFriendIdBean(friendIdBean);

                        dashboardCommentPostTagFriendsBeanses.add(dashboardCommentPostTagFriendsBeans);

                    }
                    dashboardPostListBean.setDashboardCommentPostTagFriendsBeanses(dashboardCommentPostTagFriendsBeanses);
                    dashboardPostListBean.setDashboardPostImagesBeans(dashboardPostImagesBeanses);
                    dashboardPostListBean.setDashboardPostShareUserModals(dashboardPostShareUserModals);
                    dashboardPostListBean.setLikedPost(jsonObject1.getString(S.likedPost));
                    dashboardPostListBean.setLikedPostCount(jsonObject1.getString(S.likedPostCount));
                    dashboardPostListBean.setCommentPostCount(jsonObject1.getString(S.commentPostCount));
                    dashboardPostListBean.setModified(jsonObject1.getString(S.modified));
                    dashboardPostListBean.setCreated(jsonObject1.getString(S.created));
                    mPostListBeen.add(dashboardPostListBean);
                }
                dashBoardPostsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickOnLike(int position, String status) {
        authAPI.postLikeByUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mPostListBeen.get(position).get_id(), "", status);
    }
    public void onClickOnshare(int position) {
        authAPI.postShareByUser(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), mPostListBeen.get(position).get_id());
    }
    public void sharedPostWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                callRefreshDashboard(false, currentPage);
                //Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postShared_api);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void postLikeByUserWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void onCLickOnFb(final int position) {
        if (mPostListBeen.get(position).getDashboardPostImagesBeans().size() == 0) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, mPostListBeen.get(position).getDescription());
            startActivity(Intent.createChooser(intent, "Share with"));
        } else {
            List<String> permissionList= Arrays.asList(("publish_actions"));
            LoginManager.getInstance().logInWithPublishPermissions(this,permissionList);
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    List<Bitmap> temp = null;
                    if (mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getType().equalsIgnoreCase(S.videoType))
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getThumbName(), ImageLoader.getInstance().getMemoryCache());
                    else
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getFileName(), ImageLoader.getInstance().getMemoryCache());

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(temp.get(0))
                            .build();

                    ArrayList<SharePhoto> photos = new ArrayList<>();
                    photos.add(photo);
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .setPhotos(photos)
                            .build();

                    ShareDialog shareDialog = new ShareDialog(ProfileActivity.this);
                    if (ShareDialog.canShow(SharePhotoContent.class)) {
                        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                    }
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Log.d("DashBoard", "====SUCCESS");
                        }

                        @Override
                        public void onCancel() {
                            Log.d("DashBoard", "====CANCEL");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d("DashBoard", "====ERROR");
                        }
                    });
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Log.e(TAG, error.toString());

                }
            });
        }
    }

}
