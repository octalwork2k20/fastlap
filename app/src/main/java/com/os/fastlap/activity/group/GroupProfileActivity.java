package com.os.fastlap.activity.group;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.community.EventActivity;
import com.os.fastlap.activity.profile.GalleryProfileActivity;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONObject;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/*
 * Created by anandj on 7/11/2017.
 */

public class GroupProfileActivity extends BaseActivity implements View.OnClickListener, OnImagePickerDialogSelect {

    private Context mContext;
    private AppCompatImageView toggle_icon;
    private TextView toolbarheading;
    private ImageView img_bg;
    private ImageView img_addfrd;
    private ImageView img_arrow_info;
    private ImageView img_arrow_diary;
    private ImageView img_arrow_event;
    private ImageView img_arrow_members;
    private ImageView img_arrow_gallery;
    private RelativeLayout rr;
    private CircleImageView user_icon;
    private CoordinatorLayout coordinator_layout;
    private ImageView img_diary;
    private TextView txt_diary;
    private RelativeLayout parent_diary;
    private ImageView img_info;
    private TextView txt_info;
    private RelativeLayout parent_info;
    private ImageView img_event;
    private TextView txt_event;
    private RelativeLayout parent_event;
    private ImageView img_members;
    private TextView txt_members;
    private RelativeLayout parent_members;
    private ImageView img_gallery;
    private TextView txt_gallery;
    private RelativeLayout parent_gallery;
    private TextView txt_username;
    ImageView profile_edit_iv;
    ImageView cover_edit_iv;

    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    PagerBean pagerBean = new PagerBean();
    int SELECT_IMAGE_TYPE = 0;
    AuthAPI authAPI;

    Bundle bundle;
    private String TAG = GroupProfileActivity.class.getSimpleName();
    public static String profileGroupId = "";
    public static String profileGroupName = "";
    public static String profileGroupProfileImage = "";
    public static String profileGroupCoverImage = "";
    String joinStatus;
    String ownerId="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_profile_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        onImagePickerDialogSelect = this;
        authAPI = new AuthAPI(mContext);
        initView();
        bundle = getIntent().getExtras();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.group));
        FastLapApplication.mCurrentContext = mContext;
        setBottomMenu(I.MENU_SCREEN);
        if (bundle != null)
        {
            profileGroupId = bundle.getString(S.groupId);
            ownerId = bundle.getString(S.ownerId);
            setProfileUser();
        }
    }

    private void setProfileUser() {

        profileGroupId = bundle.getString(S.groupId);
        profile_edit_iv.setVisibility(View.GONE);
        cover_edit_iv.setVisibility(View.GONE);
        joinStatus = bundle.getString("status");
        if (joinStatus != null) {
            if (joinStatus.compareTo("0") == 0) {
                img_addfrd.setVisibility(View.VISIBLE);
                img_addfrd.setImageResource(R.mipmap.adduser_white);
            } else {
                img_addfrd.setVisibility(View.GONE);
            }
        }

        authAPI.getGroupAbout(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), profileGroupId);
    }

    private void setProfileData() {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + profileGroupCoverImage, img_bg, Util.getImageLoaderOption(mContext));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + profileGroupProfileImage, user_icon, Util.getImageLoaderOption(mContext));
        txt_username.setText(profileGroupName);

    }

    private void initView() {
        txt_username = (TextView) findViewById(R.id.txt_username);
        img_addfrd = (ImageView) findViewById(R.id.img_addfrd);
        toggle_icon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        toolbarheading = (TextView) findViewById(R.id.toolbarheading);
        img_bg = (ImageView) findViewById(R.id.img_bg);
        rr = (RelativeLayout) findViewById(R.id.rr);
        user_icon = (CircleImageView) findViewById(R.id.user_icon);
        coordinator_layout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        img_diary = (ImageView) findViewById(R.id.img_diary);
        txt_diary = (TextView) findViewById(R.id.txt_diary);
        parent_diary = (RelativeLayout) findViewById(R.id.parent_diary);
        img_info = (ImageView) findViewById(R.id.img_info);
        txt_info = (TextView) findViewById(R.id.txt_info);
        parent_info = (RelativeLayout) findViewById(R.id.parent_info);
        img_event = (ImageView) findViewById(R.id.img_event);
        txt_event = (TextView) findViewById(R.id.txt_event);
        parent_event = (RelativeLayout) findViewById(R.id.parent_event);
        img_members = (ImageView) findViewById(R.id.img_members);
        txt_members = (TextView) findViewById(R.id.txt_members);
        parent_members = (RelativeLayout) findViewById(R.id.parent_members);
        img_gallery = (ImageView) findViewById(R.id.img_gallery);
        txt_gallery = (TextView) findViewById(R.id.txt_gallery);

        parent_gallery = (RelativeLayout) findViewById(R.id.parent_gallery);

        img_arrow_info = (ImageView) findViewById(R.id.img_arrow_info);
        img_arrow_diary = (ImageView) findViewById(R.id.img_arrow_diary);
        img_arrow_event = (ImageView) findViewById(R.id.img_arrow_event);
        img_arrow_members = (ImageView) findViewById(R.id.img_arrow_members);
        img_arrow_gallery = (ImageView) findViewById(R.id.img_arrow_gallery);
        profile_edit_iv = (ImageView) findViewById(R.id.profile_edit_iv);
        cover_edit_iv = (ImageView) findViewById(R.id.cover_edit_iv);

        setOnclickListener();

        profile_edit_iv.setVisibility(View.GONE);
        cover_edit_iv.setVisibility(View.GONE);
    }

    private void setOnclickListener() {
        parent_info.setOnClickListener(this);
        parent_diary.setOnClickListener(this);
        parent_members.setOnClickListener(this);
        parent_gallery.setOnClickListener(this);
        parent_event.setOnClickListener(this);

        toggle_icon.setOnClickListener(this);
        profile_edit_iv.setOnClickListener(this);
        cover_edit_iv.setOnClickListener(this);
        img_addfrd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_addfrd:
                img_addfrd.setVisibility(View.GONE);
                authAPI.joinedGroup(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), profileGroupId, "1", 0);
                //authAPI.joinedGroup(mContext, MySharedPreferences.getPreferences(mContext, S.user_id),profileGroupId);
                break;
            case R.id.parent_info:
                //setSelectedInfo();
                Util.startNewActivity(GroupProfileActivity.this, GroupInfoActivity.class, true);
                break;
            case R.id.parent_diary:
                //setSelectedDiary();
                Util.startNewActivity(GroupProfileActivity.this, GroupDiaryActivity.class, true);
                break;
            case R.id.parent_event:
                //setSelectedevent();
                Intent intent = new Intent(mContext, EventActivity.class);
                intent.putExtra(S.type, I.GROUPEVENT);
                intent.putExtra(S._id, profileGroupId);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.parent_members:
                //setSelectedMembers();
                Intent intent2=new Intent(mContext,GroupMemberActivity.class);
                intent2.putExtra(S.ownerId,ownerId);
                startActivity(intent2);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.parent_gallery:
                //setSelectedGallery();
                Intent intent1 = new Intent(mContext, GalleryProfileActivity.class);
                intent1.putExtra(S.type, I.GROUPGALLERY);
                startActivity(intent1);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;

            case R.id.profile_edit_iv:
                SELECT_IMAGE_TYPE = 0;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
                break;
            case R.id.cover_edit_iv:
                SELECT_IMAGE_TYPE = 1;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }

                break;
            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }

    private void setSelectedInfo() {
        parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_info.setImageResource(R.mipmap.infoselect);
        img_arrow_info.setImageResource(R.drawable.arrowright_white);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_members.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_members.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_members.setImageResource(R.mipmap.friends_unselect);
        img_arrow_members.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.gallery_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);

    }

    private void setSelectedDiary() {
        parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_diary.setImageResource(R.mipmap.diaryselect);
        img_arrow_diary.setImageResource(R.drawable.arrowright_white);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_members.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_members.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_members.setImageResource(R.mipmap.friends_unselect);
        img_arrow_members.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.gallery_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);

    }

    private void setSelectedevent() {
        parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_event.setImageResource(R.mipmap.event_select);
        img_arrow_event.setImageResource(R.drawable.arrowright_white);

        parent_members.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_members.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_members.setImageResource(R.mipmap.friends_unselect);
        img_arrow_members.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.gallery_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);
    }

    private void setSelectedMembers() {
        parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_members.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        txt_members.setTextColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_members.setImageResource(R.mipmap.friends_select);
        img_arrow_members.setImageResource(R.drawable.arrowright_white);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.gallery_unselect);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        img_arrow_gallery.setImageResource(R.mipmap.arrowright);

    }

    private void setSelectedGallery() {
        parent_info.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_info.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_info.setImageResource(R.mipmap.infounselect);
        img_arrow_info.setImageResource(R.mipmap.arrowright);

        parent_diary.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_diary.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_diary.setImageResource(R.mipmap.diaryunselect);
        img_arrow_diary.setImageResource(R.mipmap.arrowright);

        parent_event.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_event.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_event.setImageResource(R.mipmap.event_unselect);
        img_arrow_event.setImageResource(R.mipmap.arrowright);

        parent_members.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white_color));
        txt_members.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_members.setImageResource(R.mipmap.friends_unselect);
        img_arrow_members.setImageResource(R.mipmap.arrowright);

        txt_gallery.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_gray_dark));
        img_gallery.setImageResource(R.mipmap.gallery_select);

        parent_gallery.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        img_arrow_gallery.setImageResource(R.drawable.arrowright_white);
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {

        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(mContext, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        pagerBean = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }

    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {
        EasyImage.openGallery(this, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, GroupProfileActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;
                String imageAbsolutePath = imageFile.getAbsolutePath();
                String imageFileName = imageFile.getName();

                pagerBean = new PagerBean(imageFile, imageAbsolutePath, imageFileName, false, false, "");

                String uri = Uri.fromFile(imageFile).toString();
                String decoded = Uri.decode(uri);

                if (SELECT_IMAGE_TYPE == 0) {
                    ImageLoader.getInstance().displayImage(decoded, user_icon);
                    authAPI.UpdateImage(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), pagerBean, SELECT_IMAGE_TYPE);
                } else {
                    ImageLoader.getInstance().displayImage(decoded, img_bg);
                    authAPI.UpdateImage(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), pagerBean, SELECT_IMAGE_TYPE);
                }

            }

        });
    }

    /* update image webservice response*/
    public void updateImages(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
                if (SELECT_IMAGE_TYPE == 0) {
                    MySharedPreferences.setPreferences(mContext, jsonObject.getString(S.img), S.image);

                } else {
                    MySharedPreferences.setPreferences(mContext, jsonObject.getString(S.coverImage), S.coverImage);
                }
                setProfileData();
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void groupjoinWebserviceResponse(int position, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                joinStatus = "1";
                if (joinStatus != null) {
                    if (joinStatus.compareTo("0") == 0) {
                        img_addfrd.setVisibility(View.VISIBLE);
                        img_addfrd.setImageResource(R.mipmap.adduser_white);
                    } else {
                        img_addfrd.setVisibility(View.GONE);
                    }
                }
                //Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* user friends webservice response */
    public void userFriendsWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, S.userFriend_api);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* Response from user here you update ui according to status */
    public void callbackFromSuccessDialog() {

    }

    /* Group about us webservice response */
    public void groupInfoWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONObject jsonObject1 = jsonObject.getJSONObject(S.data);
/*
                JSONObject userJson = jsonObject1.getJSONObject(S.userId);
                JSONObject personInfo = userJson.getJSONObject(S.personalInfo);

                JSONObject vehicleJson = jsonObject1.getJSONObject(S.userVehicleId);

                JSONObject groupTypeJson = jsonObject1.getJSONObject(S.groupTypeId);

                JSONObject trackJson = jsonObject1.getJSONObject(S.trackId);*/

                profileGroupName = jsonObject1.getString(S.name);
                profileGroupProfileImage = jsonObject1.getString(S.image);
                profileGroupCoverImage = jsonObject1.getString(S.coverImage);

                setProfileData();


            } /*else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
