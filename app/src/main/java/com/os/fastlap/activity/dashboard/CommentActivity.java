package com.os.fastlap.activity.dashboard;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.dashboard.DashBoardPostsAdapter;
import com.os.fastlap.adapter.dashboard.DashboardCommentAdapter;
import com.os.fastlap.beans.BeanAdvertisement;
import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.Bean_UserDetail;
import com.os.fastlap.beans.UserDataBeans;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.beans.dashboardmodals.CommentBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardCommentPostTagFriendsBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardFeelingIdBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostImagesBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostListBean;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.beans.dashboardmodals.FriendIdBean;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.TrackData;
import com.os.fastlap.beans.gragemodals.VehicleBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.os.fastlap.util.constants.S.status;

/*
 * Created by monikab on 1/8/2016.
 */
public class CommentActivity extends BaseActivity implements SharingDeleget, View.OnClickListener, DashboardCommentAdapter.CommentInterFace, DashBoardPostsAdapter.OnItemClickListener {

    ImageView base_toggle_icon;
    RecyclerView commentRecyclerView;
    RecyclerView postListview;
    DashboardCommentAdapter dashboardCommentAdapter;
    public EditText add_comment_et;
    ImageButton send_btn;
    String post_id = "";
    String comment = "";
    Context context;
    TextView no_data_tv;
    MyProgressDialog myProgressDialog;
    Bean_UserDetail user_data;
    private AuthAPI mAuthAPI;
    private List<CommentBeans> mCommentBeansList;
    private final String TAG = CommentActivity.class.getSimpleName();
    DashboardCommentAdapter.CommentInterFace commentInterFace;
    AuthAPI authAPI;
    String postPosition = "0";
    private ArrayList<DashboardPostListBean> mPostListBeen = new ArrayList<>();
    private DashBoardPostsAdapter dashBoardPostsAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    public static ArrayList<BeanAdvertisement> advertisementArrayList;
    private SharingDeleget sharingDeleget;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.dashboard_layout_comment);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();
    }

    private void initView() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            post_id = extras.getString(S.postId);
            postPosition = extras.getString(S.position);
            Log.e("post", post_id);
        }
        mAuthAPI = new AuthAPI(context);
        mCommentBeansList = new ArrayList<>();
        advertisementArrayList = new ArrayList<>();
        advertisementArrayList.clear();
        mPostListBeen = new ArrayList<>();
        mPostListBeen.clear();
        commentRecyclerView = (RecyclerView) findViewById(R.id.comment_recyclerView);
        postListview = (RecyclerView) findViewById(R.id.post_listview);
        add_comment_et = (EditText) findViewById(R.id.add_comment_et);
        send_btn = (ImageButton) findViewById(R.id.send_btn);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);

        dashboardCommentAdapter = new DashboardCommentAdapter(mCommentBeansList, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        commentRecyclerView.setLayoutManager(layoutManager);
        commentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        commentRecyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        commentRecyclerView.setAdapter(dashboardCommentAdapter);
        dashboardCommentAdapter.setOnItemClickListener(this);
        commentRecyclerView.setNestedScrollingEnabled(false);

        dashBoardPostsAdapter = new DashBoardPostsAdapter(mPostListBeen, advertisementArrayList, context, sharingDeleget, this, this);
        mLinearLayoutManager = new LinearLayoutManager(context);
        postListview.setLayoutManager(mLinearLayoutManager);
        postListview.setItemAnimator(new DefaultItemAnimator());
        postListview.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        postListview.setAdapter(dashBoardPostsAdapter);
        postListview.setNestedScrollingEnabled(false);


        clickListner();
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        send_btn.setOnClickListener(this);

        /*add_comment_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    comment = add_comment_et.getText().toString().trim();
                    byte[] data = new byte[0];
                    try {
                        data = comment.getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    comment = Base64.encodeToString(data, Base64.NO_WRAP);
                    if (comment.length() > 0) {
                        add_comment_et.setText("");
                        mAuthAPI.postComment(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment, "", "0");
                    }
                    handled = true;
                }
                return handled;
            }
        });*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                onBack();
                break;
            case R.id.send_btn:
                comment = add_comment_et.getText().toString().trim();
                byte[] data = new byte[0];
                try {
                    data = comment.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment.length() > 0) {
                    add_comment_et.setText("");
                    mAuthAPI.postComment(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment, "", "0");
                }
                break;
        }
    }


    private void onBack() {
        Intent returnIntent = new Intent();
        if (mPostListBeen.size() > 0) {
            returnIntent.putExtra("result", "");
            returnIntent.putExtra("total_likes", mPostListBeen.get(0).getLikedPostCount());
            returnIntent.putExtra("isLike", mPostListBeen.get(0).getLikedPost());
            int list_size = mCommentBeansList.size();
            returnIntent.putExtra("total_comment", list_size + "");
            returnIntent.putExtra("postPosition", postPosition);
        }
        setResult(Activity.RESULT_OK, returnIntent);
        hideKeyboard();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.comments));
        postCommentListing();
        FastLapApplication.mCurrentContext = context;
    }

    @Override
    public void facebookSharing(String title, String desc, String image) {

    }

    @Override
    public void gmailSharing(String title, String desc, String image) {

    }

    @Override
    public void twitterSharing(String title, String desc, String image) {

    }

    @Override
    public void onDeletePost(int pos) {
        mCommentBeansList.remove(pos);
        dashboardCommentAdapter.notifyDataSetChanged();
    }

    /*  postCommentListing webservice response
    *   here we show listing of comments
    */
    public void postCommentListingWebserviceResponse(String response) {

        mCommentBeansList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    CommentBeans commentBeans = new CommentBeans();
                    commentBeans.setComment_like_count(jsonObject1.getString(S.countLikeData));
                    commentBeans.setComment_reply_count(jsonObject1.getString(S.countReplyCommentData));
                    commentBeans.setComment_like_status(jsonObject1.getString(S.likedComment));

                    commentBeans.setComment_id(jsonObject1.getString(S._id));
                    commentBeans.setPost_id(jsonObject1.getString(S.postId));
                    commentBeans.setComment_date(jsonObject1.getString(S.created));
                    commentBeans.setComment_text(jsonObject1.getString(S.comment));

                    commentBeans.setComment_user_id(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    commentBeans.setComment_user_name(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    commentBeans.setComment_user_image(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));

                    mCommentBeansList.add(commentBeans);
                }
                dashboardCommentAdapter.notifyDataSetChanged();

                if (mCommentBeansList.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    commentRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    commentRecyclerView.setVisibility(View.GONE);
                }

            } else {
                //Util.showAlertDialog(context, getString(R.string.alert), msg);
                mCommentBeansList.clear();
                dashboardCommentAdapter.notifyDataSetChanged();
                no_data_tv.setVisibility(View.VISIBLE);
                commentRecyclerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            mCommentBeansList.clear();
            dashboardCommentAdapter.notifyDataSetChanged();
            no_data_tv.setVisibility(View.VISIBLE);
            commentRecyclerView.setVisibility(View.GONE);

        }
    }

    /* postCommentRemove webservice response
     *  here we remove comment and refresh comment list
    */
    public void postCommentRemoveWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postCommentRemove_api);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /*  Calling postCommentListing webservice*/
    public void postCommentListing() {
        authAPI.postInfo(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);
        mAuthAPI.postCommentListing(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);
        // mAuthAPI.postCommentListing(context, "599d1c7462535205ac85f5c9", "59a41a517f30ef15cc373679");
    }

    @Override
    public void onBackPressed() {

        onBack();
    }

    /* postCommentLike webservice response*/
    public void postCommentLikeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickCommentRowReply(int postion) {
        Intent i = new Intent(context, ReplyActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        startActivity(i);
    }

    @Override
    public void onClickReplyViewCount(int postion) {
        Intent i = new Intent(context, ReplyActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        startActivity(i);
    }

    @Override
    public void onClickCommentRowLikeCount(int postion) {
        Intent i = new Intent(context, LikesUserActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        i.putExtra(S.likeType, I.COMMENTLIKE);
        startActivity(i);
    }

    @Override
    public void onClickCommentRowLike(int postion, String status) {
        authAPI.postCommentLike(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), mCommentBeansList.get(postion).getComment_id(), status);
    }

    @Override
    public void onClickCommentDelete(int postion) {
        Util.showAlertDialogWithTwoButton(context, getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.cancel), getString(R.string.delete_small), postion);
        //
    }

    public void callDeleteComment(int postion) {
        authAPI.postCommentRemove(context, MySharedPreferences.getPreferences(context, S.user_id), mCommentBeansList.get(postion).getComment_id());
    }

    // Post Comment response
    public void postCommentResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickCommentEdit(final int postion) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dashboard_comment_edit);

        AppCompatImageView baseToggleIcon;
        ImageView commetRowUserIv;
        TextViewPlayBold usernameTv;
        TextViewPlayRegular commentTv;
        final EditTextPlayRegular etComment;
        TextViewPlayBold updateBtn;
        TextViewPlayBold cancelBtn;

        baseToggleIcon = (AppCompatImageView) dialog.findViewById(R.id.base_toggle_icon);
        commetRowUserIv = (ImageView) dialog.findViewById(R.id.commet_row_user_iv);
        usernameTv = (TextViewPlayBold) dialog.findViewById(R.id.username_tv);
        commentTv = (TextViewPlayRegular) dialog.findViewById(R.id.comment_tv);
        etComment = (EditTextPlayRegular) dialog.findViewById(R.id.etComment);
        updateBtn = (TextViewPlayBold) dialog.findViewById(R.id.update_btn);
        cancelBtn = (TextViewPlayBold) dialog.findViewById(R.id.cancel_btn);

        usernameTv.setText(MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image), commetRowUserIv, Util.getImageLoaderOption(context));

        byte[] data = Base64.decode(mCommentBeansList.get(postion).getComment_text(), Base64.NO_WRAP);
        try {
            String text = new String(data, "UTF-8");
            commentTv.setText(text);
            etComment.setText(text);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = etComment.getText().toString().trim();
                byte[] data = new byte[0];
                try {
                    data = comment.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment.length() > 0) {
                    etComment.setText("");
                    mAuthAPI.postComment(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment, mCommentBeansList.get(postion).getComment_id(), "1");
                    dialog.cancel();
                }
            }
        });

        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    @Override
    public void onClickOnLike(int position, String status) {
        mAuthAPI.postLikeByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mPostListBeen.get(position).get_id(), "", status);
    }

    public void postDetailResponse(String response) {
        mPostListBeen.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    DashboardPostListBean dashboardPostListBean;
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    dashboardPostListBean = new DashboardPostListBean();
                    dashboardPostListBean.set_id(jsonObject1.getString(S._id));

                    JSONObject userJson = jsonObject1.getJSONObject(S.userId);
                    UserDataBeans userDataBeans = new UserDataBeans();
                    userDataBeans.set_id(userJson.getString(S._id));
                    userDataBeans.setEmail(userJson.getString(S.email));
                    //userDataBeans.setUsername(userJson.getString(S.username));
                    userDataBeans.setUsername(userJson.getString(S.nickname));

                    JSONObject personalInfoJson = userJson.getJSONObject(S.personalInfo);
                    PersonalInfo personalInfo = new PersonalInfo();
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    userDataBeans.setPersonalInfo(personalInfo);

                    dashboardPostListBean.setUserDataBeans(userDataBeans);
                    dashboardPostListBean.setStatus(jsonObject1.getInt(S.status));
                    dashboardPostListBean.setDescription(jsonObject1.getString(S.description));
                    dashboardPostListBean.setPosition_text(jsonObject1.getString(S.position_text));
                    dashboardPostListBean.setSpeed(jsonObject1.getString(S.speed));
                    dashboardPostListBean.setWeather(jsonObject1.getString(S.weather));

                    JSONArray feelingJsonArray = jsonObject1.getJSONArray(S.feelingId);
                    for (int j = 0; j < feelingJsonArray.length(); j++) {
                        JSONObject feelingJson = feelingJsonArray.getJSONObject(j);
                        DashboardFeelingIdBeans dashboardFeelingIdBeans = new DashboardFeelingIdBeans();
                        dashboardFeelingIdBeans.set_id(feelingJson.getString(S._id));
                        dashboardFeelingIdBeans.setStatus(feelingJson.getInt(S.status));
                        dashboardFeelingIdBeans.setUnicodes(feelingJson.getString(S.unicodes));
                        dashboardFeelingIdBeans.setName(feelingJson.getString(S.name));

                        dashboardPostListBean.setDashboardFeelingIdBeans(dashboardFeelingIdBeans);
                    }


                    JSONArray trackIdJsonArray = jsonObject1.getJSONArray(S.trackId);
                    for (int j = 0; j < trackIdJsonArray.length(); j++) {
                        JSONObject trackIdJson = trackIdJsonArray.getJSONObject(j);
                        TrackData trackData = new TrackData();
                        trackData.setId(trackIdJson.has(S._id)?trackIdJson.getString(S._id):"");
                        trackData.setOwnerId(trackIdJson.has(S.ownerId)?trackIdJson.getString(S.ownerId):"");
                        trackData.setName(trackIdJson.has(S.name)?trackIdJson.getString(S.name):"");
                        trackData.setImage(trackIdJson.has(S.image)?trackIdJson.getString(S.image):"");
                        trackData.setTrackLength(trackIdJson.has(S.trackLength)?trackIdJson.getInt(S.trackLength):0);
                        trackData.setCountry(trackIdJson.has(S.country)?trackIdJson.getString(S.country):"");

                        dashboardPostListBean.setTrackData(trackData);
                    }

                    JSONArray userVehicleIdJsonArray = jsonObject1.getJSONArray(S.userVehicleId);
                    for (int j = 0; j < userVehicleIdJsonArray.length(); j++) {
                        JSONObject vehicleJson = userVehicleIdJsonArray.getJSONObject(j);
                        BeanVehicleList beanVehicleList = new BeanVehicleList();
                        beanVehicleList.set_id(vehicleJson.has(S._id)?vehicleJson.getString(S._id):"");

                        VehicleTypeIdModal vehicleTypeIdModal = new VehicleTypeIdModal();
                        JSONObject vehicleTypeJson = vehicleJson.getJSONObject(S.vehicleTypeId);
                        vehicleTypeIdModal.set_id(vehicleTypeJson.has(S._id)?vehicleTypeJson.getString(S._id):"");
                        vehicleTypeIdModal.setName(vehicleTypeJson.has(S.name)?vehicleTypeJson.getString(S.name):"");
                        beanVehicleList.setVehicleTypeIdModal(vehicleTypeIdModal);


                        VehicleBrandIdModal vehicleBrandIdModal = new VehicleBrandIdModal();
                        JSONObject vehicleBrandJson = vehicleJson.getJSONObject(S.vehicleBrandId);
                        vehicleBrandIdModal.set_id(vehicleBrandJson.getString(S._id));
                        vehicleBrandIdModal.setVehicleBrandName(vehicleBrandJson.getString(S.vehicleBrandName));
                        vehicleBrandIdModal.setVehicleTypeId(vehicleBrandJson.getString(S.vehicleTypeId));

                        beanVehicleList.setVehicleBrandIdModal(vehicleBrandIdModal);

                        VehicleModelIdModal vehicleModelIdModal = new VehicleModelIdModal();
                        JSONObject vehicleModalJson = vehicleJson.getJSONObject(S.vehicleModelId);
                        vehicleModelIdModal.set_id(vehicleModalJson.getString(S._id));
                        vehicleModelIdModal.setVehicleBrandId(vehicleModalJson.getString(S.vehicleBrandId));
                        vehicleModelIdModal.setVehicleTypeId(vehicleModalJson.getString(S.vehicleTypeId));
                        vehicleModelIdModal.setVehicleModelName(vehicleModalJson.getString(S.vehicleModelName));

                        beanVehicleList.setVehicleModelIdModal(vehicleModelIdModal);
                        beanVehicleList.setDescription(vehicleJson.getString(S.description));

                        dashboardPostListBean.setBeanVehicleList(beanVehicleList);
                    }
                    JSONArray postImagesJsonArray = jsonObject1.getJSONArray(S.postImages);
                    List<DashboardPostImagesBeans> dashboardPostImagesBeanses = new ArrayList<>();
                    for (int j = 0; j < postImagesJsonArray.length(); j++) {
                        JSONObject postImageJson = postImagesJsonArray.getJSONObject(j);
                        DashboardPostImagesBeans dashboardPostImagesBeans = new DashboardPostImagesBeans();
                        dashboardPostImagesBeans.set_id(postImageJson.getString(S._id));
                        dashboardPostImagesBeans.setUserId(postImageJson.getString(S.userId));
                        dashboardPostImagesBeans.setPostId(postImageJson.getString(S.postId));
                        dashboardPostImagesBeans.setStatus(postImageJson.getInt(S.status));
                        dashboardPostImagesBeans.setType(postImageJson.getString(S.type));
                        dashboardPostImagesBeans.setFileName(postImageJson.getString(S.fileName));
                        dashboardPostImagesBeans.setThumbName(postImageJson.getString(S.thumbName));
                        dashboardPostImagesBeanses.add(dashboardPostImagesBeans);
                    }

                    JSONArray postShareUser = jsonObject1.getJSONArray(S.postShareUser);
                    List<DashboardPostShareUserModal> dashboardPostShareUserModals = new ArrayList<>();
                    for (int j = 0; j < postShareUser.length(); j++) {
                        JSONObject postShareUserJson = postShareUser.getJSONObject(j);
                        DashboardPostShareUserModal dashboardPostShareUserModal = new DashboardPostShareUserModal();
                        dashboardPostShareUserModal.setId(postShareUserJson.getString(S._id));
                        dashboardPostShareUserModal.setPostId(postShareUserJson.getString(S.postId));
                        dashboardPostShareUserModal.setModified(postShareUserJson.getString(S.modified));
                        dashboardPostShareUserModal.setCreated(postShareUserJson.getString(S.created));
                        JSONObject userIdJson = postShareUserJson.getJSONObject(S.userId);

                        UserDataBeans userDataBeans1 = new UserDataBeans();
                        userDataBeans1.set_id(userIdJson.getString(S._id));
                        userDataBeans1.setEmail(userIdJson.getString(S.email));
                        //userDataBeans1.setUsername(userIdJson.getString(S.username));
                        userDataBeans1.setUsername(userIdJson.getString(S.nickname));
                        JSONObject personalInfoJsonShare = userIdJson.getJSONObject(S.personalInfo);
                        PersonalInfo personalInfo1 = new PersonalInfo();
                        personalInfo1.setLanguage(personalInfoJsonShare.getString(S.language_response));
                        personalInfo1.setCoverImage(personalInfoJsonShare.getString(S.coverImage));
                        personalInfo1.setImage(personalInfoJsonShare.getString(S.image));
                        personalInfo1.setMobileNumber(personalInfoJsonShare.getString(S.mobileNumber_response));
                        personalInfo1.setDateOfBirth(personalInfoJsonShare.getString(S.dateOfBirth_response));
                        personalInfo1.setVoucherCode(personalInfoJsonShare.getString(S.voucherCode_response));
                        personalInfo1.setLastName(personalInfoJsonShare.getString(S.lastName_response));
                        personalInfo1.setFirstName(personalInfoJsonShare.getString(S.firstName_response));

                        userDataBeans1.setPersonalInfo(personalInfo1);
                        dashboardPostShareUserModal.setUserDataBeans(userDataBeans1);
                        dashboardPostShareUserModals.add(dashboardPostShareUserModal);
                    }

                    JSONArray commentPostTagFriend = jsonObject1.getJSONArray(S.commentPostTagFriend);
                    List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeanses = new ArrayList<>();
                    for (int j = 0; j < commentPostTagFriend.length(); j++) {
                        JSONObject friendJson = commentPostTagFriend.getJSONObject(j);
                        DashboardCommentPostTagFriendsBeans dashboardCommentPostTagFriendsBeans = new DashboardCommentPostTagFriendsBeans();
                        dashboardCommentPostTagFriendsBeans.set_id(friendJson.getString(S._id));
                        dashboardCommentPostTagFriendsBeans.setUserId(friendJson.getString(S.userId));
                        dashboardCommentPostTagFriendsBeans.setPostId(friendJson.getString(S.postId));

                        FriendIdBean friendIdBean = new FriendIdBean();
                        JSONObject frinedIdJson = friendJson.getJSONObject(S.friendId);
                        friendIdBean.set_id(frinedIdJson.getString(S._id));
                        //friendIdBean.setUsername(frinedIdJson.getString(S.username));
                        friendIdBean.setUsername(frinedIdJson.getString(S.nickname));
                        friendIdBean.setEmail(frinedIdJson.getString(S.email));

                        PersonalInfo personalInfo1 = new PersonalInfo();
                        JSONObject personalInfoJsonInner = frinedIdJson.getJSONObject(S.personalInfo);
                        personalInfo1.setLanguage(personalInfoJsonInner.getString(S.language_response));
                        personalInfo1.setCoverImage(personalInfoJsonInner.getString(S.coverImage));
                        personalInfo1.setImage(personalInfoJsonInner.getString(S.image));
                        personalInfo1.setMobileNumber(personalInfoJsonInner.getString(S.mobileNumber_response));
                        personalInfo1.setDateOfBirth(personalInfoJsonInner.getString(S.dateOfBirth_response));
                        personalInfo1.setVoucherCode(personalInfoJsonInner.getString(S.voucherCode_response));
                        personalInfo1.setLastName(personalInfoJsonInner.getString(S.lastName_response));
                        personalInfo1.setFirstName(personalInfoJsonInner.getString(S.firstName_response));

                        friendIdBean.setPersonalInfo(personalInfo1);
                        dashboardCommentPostTagFriendsBeans.setFriendIdBean(friendIdBean);

                        dashboardCommentPostTagFriendsBeanses.add(dashboardCommentPostTagFriendsBeans);
                    }
                    dashboardPostListBean.setDashboardCommentPostTagFriendsBeanses(dashboardCommentPostTagFriendsBeanses);
                    dashboardPostListBean.setDashboardPostImagesBeans(dashboardPostImagesBeanses);
                    dashboardPostListBean.setDashboardPostShareUserModals(dashboardPostShareUserModals);
                    dashboardPostListBean.setLikedPost(jsonObject1.getString(S.likedPost));
                    dashboardPostListBean.setLikedPostCount(jsonObject1.getString(S.likedPostCount));
                    dashboardPostListBean.setCommentPostCount(jsonObject1.getString(S.commentPostCount));
                    dashboardPostListBean.setModified(jsonObject1.getString(S.modified));
                    dashboardPostListBean.setCreated(jsonObject1.getString(S.created));

                    mPostListBeen.add(dashboardPostListBean);
                }

                dashBoardPostsAdapter.notifyDataSetChanged();

            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void onClickOnshare(int position) {
        mAuthAPI.postShareByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mPostListBeen.get(position).get_id());
    }

    public void onCLickOnFb(final int position) {
        if (mPostListBeen.get(position).getDashboardPostImagesBeans().size() == 0) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, mPostListBeen.get(position).getDescription());
            startActivity(Intent.createChooser(intent, "Share with"));
        } else {
            List<String> permissionNeeds = Arrays.asList("publish_actions");
            LoginManager.getInstance().logInWithPublishPermissions(this, permissionNeeds);
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    List<Bitmap> temp = null;
                    if (mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getType().equalsIgnoreCase(S.videoType))
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getThumbName(), ImageLoader.getInstance().getMemoryCache());
                    else
                        temp = MemoryCacheUtils.findCachedBitmapsForImageUri(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getFileName(), ImageLoader.getInstance().getMemoryCache());

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(temp.get(0))
                            .build();

                    ArrayList<SharePhoto> photos = new ArrayList<>();
                    photos.add(photo);
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .setPhotos(photos)
                            .build();

                    ShareDialog shareDialog = new ShareDialog(CommentActivity.this);
                    if (ShareDialog.canShow(SharePhotoContent.class)) {
                        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                    }
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Log.d("DashBoard", "====SUCCESS");
                        }

                        @Override
                        public void onCancel() {
                            Log.d("DashBoard", "====CANCEL");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d("DashBoard", "====ERROR");
                        }
                    });
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Log.e(TAG, error.toString());
                }
            });


           /* ShareDialog shareDialog = new ShareDialog(this);
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(getString(R.string.app_name))
                        .setImageUrl(Uri.parse(S.IMAGE_BASE_URL + mPostListBeen.get(position).getDashboardPostImagesBeans().get(0).getFileName()))
                        .setContentDescription(mPostListBeen.get(position).getDescription())
                        .setContentUrl(Uri.parse("http://182.156.245.85:3000/home"))
                        .build();
                shareDialog.show(linkContent);  // Show facebook ShareDialog
            }*/


        }
/*
            ShareDialog shareDialog = new ShareDialog(this);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setImageUrl(Uri.parse("https://www.studytutorial.in/wp-content/uploads/2017/02/FacebookLoginButton-min-300x136.png"))
                    .setContentDescription(
                            "This tutorial explains how to integrate Facebook and Login through Android Application")
                    .setContentUrl(Uri.parse("https://www.studytutorial.in/roid-facebook-integration-and-login-tutorial"))
                    .build();
            shareDialog.show(linkContent);  // Show facebook ShareDialog
        }*/
    }

    public void reportAbuseResponce(String s) {
        JSONObject jsonObject1 = null;
        try {
            jsonObject1 = new JSONObject(s);
            Util.showToast(context,jsonObject1.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
