package com.os.fastlap.activity.mylaps;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIEvents;
import com.highsoft.highcharts.Common.HIChartsClasses.HIOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIPlotOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIPoint;
import com.highsoft.highcharts.Common.HIChartsClasses.HISeries;
import com.highsoft.highcharts.Common.HIColor;
import com.highsoft.highcharts.Core.HIChartContext;
import com.highsoft.highcharts.Core.HIChartView;
import com.highsoft.highcharts.Core.HIConsumer;
import com.highsoft.highcharts.Core.HIFunction;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.FullScreenChartActivity;
import com.os.fastlap.adapter.paddock.CompareLapAdapter;
import com.os.fastlap.adapter.paddock.PaddockRaceUsersAdapter;
import com.os.fastlap.beans.BeanCompareData;
import com.os.fastlap.beans.BeanLocation;
import com.os.fastlap.beans.BeanRunElement;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.libs.WorkaroundMapFragment;
import com.os.fastlap.util.ChartsManager;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anandj on 7/21/2017.
 */


public class MyLapsRaceActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    private AppCompatImageView baseToggleIcon;
    private ImageView playIv;
    private ImageView pauseIv;
    private ImageView previousIv;
    private ImageView nextIv;
    private RecyclerView usersRecyclerView;
    ArrayList<BeanTimeLap> mUserList;
    PaddockRaceUsersAdapter paddockRaceUsersAdapter;
    ArrayList<Integer> selectIds;
    PaddockMyLapsParentModel paddockMyLapsParentModel;
    private String TAG = MyLapsRaceActivity.class.getSimpleName();
    AuthAPI authAPI;
    ArrayList<BeanCompareData> compareList;
    private Circle driverMarker;
    GoogleMap googleMap;
    int width;
    Handler handler;
    int runStatus = 0;
    ArrayList<BeanRunElement> runElementList;
    Runnable runnable;
    TextView oneSpeedTv;
    TextView twoSpeedTv;
    TextView fourSpeedTv;
    ImageView editPolyLine;
    Button google_tv;
    Button track_tv;
    boolean isVisibleLine = true;
    NestedScrollView scrollview;
    private RecyclerView mRecyclerView;
    private CompareLapAdapter compareLapAdapter;
    private TextViewPlayRegular mFullViewGraph;
    static public boolean isSpeed=true,isAccCalc=false,isAccOn=false,
            isBrakeCalc=false,isBrakeOn=false,isXView=false,isYView=false,isZView=false;
    ChartsManager manager=null;
    ArrayList selectedItem=new ArrayList<>();
    ViewGroup chartDetailsSpline;
    MyProgressDialog myProgressDialog;
    TextView mSelectParamter;
    TextView mSelectChartType;
    boolean[] subcriotionTrueIndex={isSpeed,isBrakeOn,isAccOn,isBrakeCalc,isAccCalc,isXView,isYView,isZView};
    boolean[] subcriotionFalseIndex={isSpeed,isBrakeOn,isAccOn};
    final String[] subcriotionFalseItem = {S.Speed, S.brakeOn,S.accelerationOn};
    final String[] subcriotionTrueItem = {S.Speed, S.brakeOn,S.accelerationOn,S.brakeCalc,S.accelerationCalc,S.accelX, S.accelY, S.accelZ};
    private int selectedDialogIndex=0;
    private String chartType="Time";
    ArrayList<Marker> markersList=new ArrayList<>();
    String markerBrake="1";
    double markerBrakeCalc=0;
    double markerAccelCalc=0;
    String markerAccel="1";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mylaps_race_activity_layout);

       // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
       // StrictMode.setThreadPolicy(policy);
        myProgressDialog=new MyProgressDialog(this);
        context = this;
        authAPI = new AuthAPI(context);
        FastLapApplication.mCurrentContext = context;
        handler = new Handler();
        selectIds = new ArrayList<>();
        compareList = new ArrayList<>();
        selectIds = getIntent().getIntegerArrayListExtra("selectIds");
        initView();
        initilizeMap();
        runElementList = new ArrayList<>();
        paddockMyLapsParentModel = new PaddockMyLapsParentModel();
        selectIds = getIntent().getIntegerArrayListExtra("selectIds");
        paddockMyLapsParentModel = (PaddockMyLapsParentModel) getIntent().getSerializableExtra("grouplist");
        manager = new ChartsManager();
    }

    private void initView() {
        int spanCount;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        chartDetailsSpline=(ViewGroup)findViewById(R.id.chartDetailsSpline);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        playIv = (ImageView) findViewById(R.id.play_iv);
        pauseIv = (ImageView) findViewById(R.id.pause_iv);
        nextIv = (ImageView) findViewById(R.id.next_iv);
        previousIv = (ImageView) findViewById(R.id.previous_iv);
        usersRecyclerView = (RecyclerView) findViewById(R.id.users_recyclerView);
        oneSpeedTv = (TextView) findViewById(R.id.oneSpeedTv);
        twoSpeedTv = (TextView) findViewById(R.id.twoSpeedTv);
        fourSpeedTv = (TextView) findViewById(R.id.fourSpeedTv);
        google_tv = (Button) findViewById(R.id.google_tv);
        track_tv = (Button) findViewById(R.id.track_tv);
        editPolyLine = (ImageView) findViewById(R.id.editPolyLine);
        scrollview = (NestedScrollView) findViewById(R.id.scrollview);
        mRecyclerView = (RecyclerView) findViewById(R.id.compareRecycleview);
        mFullViewGraph=(TextViewPlayRegular)findViewById(R.id.tvFullViewGraph);
        mSelectParamter=(TextView)findViewById(R.id.tvSelectParamter);
        mSelectChartType=(TextView)findViewById(R.id.tvSelectChartType);
        mUserList = new ArrayList<>();
        mUserList.clear();
        if(selectIds.size()>1){
            spanCount=2;
        }
        else {
            spanCount=1;
        }
        GridLayoutManager gridLayoutManager=new GridLayoutManager(context,spanCount,GridLayoutManager.HORIZONTAL,false);
        paddockRaceUsersAdapter = new PaddockRaceUsersAdapter(context, mUserList, this, width);
        usersRecyclerView.setLayoutManager(gridLayoutManager);
        usersRecyclerView.setItemAnimator(new DefaultItemAnimator());
        usersRecyclerView.setAdapter(paddockRaceUsersAdapter);

        LinearLayoutManager vLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(vLayoutManager);
        compareLapAdapter=new CompareLapAdapter(this,mUserList,true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(compareLapAdapter);
        clickListner();
        usersRecyclerView.setNestedScrollingEnabled(false);
        usersRecyclerView.setHasFixedSize(true);
    }
    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
        playIv.setOnClickListener(this);
        pauseIv.setOnClickListener(this);
        nextIv.setOnClickListener(this);
        previousIv.setOnClickListener(this);
        oneSpeedTv.setOnClickListener(this);
        twoSpeedTv.setOnClickListener(this);
        fourSpeedTv.setOnClickListener(this);
        editPolyLine.setOnClickListener(this);
        track_tv.setOnClickListener(this);
        google_tv.setOnClickListener(this);
        mFullViewGraph.setOnClickListener(this);
        mSelectParamter.setOnClickListener(this);
        mSelectChartType.setOnClickListener(this);
        }

    private void initilizeMap() {
        try {
            ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.compare_map_frame)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    downloadFiles();
                    String pos = MySharedPreferences.getPreferences(context, S.defaultmapView);
                    if (pos.compareTo("4") == 0) {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                        google_tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                        track_tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

                    } else {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        google_tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        track_tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                    }
                }
            });
            ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.compare_map_frame))
                    .setListener(new WorkaroundMapFragment.OnTouchListener() {
                        @Override
                        public void onTouch() {
                            scrollview.requestDisallowInterceptTouchEvent(true);
                        }
                    });

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void getUrlData(String response, int color, Long milisecond) {
        JSONArray jsonArray=null;
        try {
            jsonArray = new JSONArray(response);
            readFileandDrowPathDemo(jsonArray, color, milisecond);
        } catch (Exception e) {
            Log.e("tag11", e.toString());
        }
    }
    private void readFileandDrowPathDemo(JSONArray jsonArray, int color, long milisecond)
    {
        selectedItem.clear();
        isSpeed=true;isBrakeOn=false;isAccOn=false;isBrakeCalc=false;isAccCalc=false;isXView=false;isYView=false;
        //markersArray.clear();
        ArrayList<BeanLocation> chartArraylist = new ArrayList<>();
        ArrayList<Double>timeList=new ArrayList<>();
        ArrayList<Double>distanceList=new ArrayList<>();
       /* Double[] timeList = new Double[jsonArray.length()];
        Double[] distanceList = new Double[jsonArray.length()];*/
        PolylineOptions line = new PolylineOptions();
        BeanCompareData beanCompareData = new BeanCompareData();
        ArrayList<LatLng> latlagList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++)
        {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject != null) {
                    BeanLocation beanLocation = new BeanLocation();
                    LatLng latLng;
                    if(!jsonObject.optString("accel_x").equalsIgnoreCase("null")&&!jsonObject.optString("accel_x").equals(null)){
                        beanLocation.setAccel_x(jsonObject.has("accel_x")?jsonObject.optString("accel_x"):"0");
                    }
                    if(!jsonObject.optString("accel_y").equalsIgnoreCase("null")&&!jsonObject.optString("accel_y").equals(null)){
                        beanLocation.setAccel_y(jsonObject.has("accel_y")?jsonObject.optString("accel_y"):"0");
                    }
                    if(!jsonObject.optString("accel_z").equalsIgnoreCase("null")&&!jsonObject.optString("accel_z").equals(null)){
                        beanLocation.setAccel_z(jsonObject.has("accel_z")?jsonObject.optString("accel_z"):"0");
                    }
                    if(!jsonObject.optString("brake_calc").equalsIgnoreCase("null")&&!jsonObject.optString("brake_calc").equals(null)){
                        beanLocation.setBrake_calc(jsonObject.has("brake_calc")?jsonObject.optString("brake_calc"):"0");
                    }
                    if(!jsonObject.optString("acc_calc").equalsIgnoreCase("null")&&!jsonObject.optString("acc_calc").equals(null)){
                        beanLocation.setAcc_calc(jsonObject.has("acc_calc")?jsonObject.optString("acc_calc"):"0");
                    }
                    if(!jsonObject.optString("brake_on").equalsIgnoreCase("null")&&!jsonObject.optString("brake_on").equals(null)){
                        beanLocation.setBrake_on(jsonObject.has("brake_on")?jsonObject.optString("brake_on"):"0");
                    }
                    if(!jsonObject.optString("acc_on").equalsIgnoreCase("null")&&!jsonObject.optString("acc_on").equals(null)){
                        beanLocation.setAcc_on(jsonObject.has("acc_on")?jsonObject.optString("acc_on"):"0");
                    }
                    if(!jsonObject.optString("lat").equalsIgnoreCase("null")&&!jsonObject.optString("lat").equals(null)){
                        beanLocation.setLat(jsonObject.has("lat")?jsonObject.optString("lat"):"0");
                    }
                    if(!jsonObject.optString("lng").equalsIgnoreCase("null")&&!jsonObject.optString("lng").equals(null)){
                        beanLocation.setLng(jsonObject.has("lng")?jsonObject.optString("lng"):"0");
                    }
                    if(!jsonObject.optString("speed").equalsIgnoreCase("null")&&!jsonObject.optString("speed").equals(null)){
                        beanLocation.setSpeed(jsonObject.has("speed")?jsonObject.optString("speed"):"0");
                    }
                    if(!jsonObject.optString("time").equalsIgnoreCase("null")&&!jsonObject.optString("time").equals(null)){
                        beanLocation.setTime(jsonObject.has("time")?jsonObject.optString("time"):"0");
                        timeList.add(Double.valueOf(jsonObject.has("time")?jsonObject.optString("time"):"0"));
                        //timeList[i]=Double.valueOf(jsonObject.has("time")?jsonObject.optString("time"):"0");
                    }
                    if(!jsonObject.optString("dist").equalsIgnoreCase("null")&&!jsonObject.optString("dist").equals(null)){
                        beanLocation.setDist(jsonObject.has("dist")?jsonObject.optString("dist"):"0");
                        distanceList.add(Double.valueOf(jsonObject.has("dist")?jsonObject.optString("dist"):"0"));
                       // distanceList[i]=Double.valueOf(jsonObject.has("dist")?jsonObject.optString("dist"):"0");
                    }

                    chartArraylist.add(beanLocation);

                    //markersArray.add(beanLocation);
                    latLng = new LatLng(Double.parseDouble(jsonObject.has("lat")?jsonObject.optString("lat"):"0"), Double.parseDouble(jsonObject.has("lng")?jsonObject.optString("lng"):"0"));
                    line.add(new LatLng(Double.parseDouble(jsonObject.has("lat")?jsonObject.optString("lat"):"0"), Double.parseDouble(jsonObject.has("lng")?jsonObject.optString("lng"):"0")));
                    latlagList.add(latLng);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int speed = (int) milisecond / chartArraylist.size();
        beanCompareData.setLocationArrayList(chartArraylist);
        beanCompareData.setTimeList(timeList);
        beanCompareData.setDistanceList(distanceList);
        beanCompareData.setRunSpeed(speed);
        beanCompareData.setId(1 + "");
        beanCompareData.setRouteLatLng(latlagList);
        beanCompareData.setColor(color);
        line.width(5).color(color);
        Polyline polylineFinal = googleMap.addPolyline(line);
        beanCompareData.setPolyline(polylineFinal);
        compareList.add(beanCompareData);
        setChartData();
        drowGoogleMap(line,color);
    }
    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        FastLapApplication.mCurrentContext = context;
        setToolbarHeading(getString(R.string.compare_lap_text));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.play_iv:
                pauseIv.setVisibility(View.VISIBLE);
                playIv.setVisibility(View.GONE);
                if (runStatus == 0) {
                    runStatus = 1;
                    for (int i = 0; i < compareList.size(); i++) {
                        runStatus = 1;
                        new AsyncCaller(i).execute();
                    }
                } else {
                    for (int i = 0; i < runElementList.size(); i++) {
                        handler.postDelayed(runElementList.get(i).getRunnable(), runElementList.get(i).getRunSpeed());
                    }

                }
                break;
            case R.id.pause_iv:
                if (runStatus == 1 || runStatus == 2) {
                    runStatus = 2;
                    playIv.setVisibility(View.VISIBLE);
                    pauseIv.setVisibility(View.GONE);

                    for (int i = 0; i < runElementList.size(); i++) {
                        handler.removeCallbacks(runElementList.get(i).getRunnable());
                    }
                }
                break;
            case R.id.next_iv:
                if (playIv.getVisibility() == View.VISIBLE) {
                    for (int i = 0; i < runElementList.size(); i++) {
                        driverMarker = runElementList.get(i).getCircle();
                        int runPos = runElementList.get(i).getRunningPosition();
                        runPos++;
                        List<LatLng> directionPoint = new ArrayList<>();
                        directionPoint = compareList.get(i).getRouteLatLng();
                        Projection proj = googleMap.getProjection();

                        if (runPos < directionPoint.size()) {
                            boolean contain = proj.getVisibleRegion().latLngBounds.contains(directionPoint.get(i));
                            if (!contain)
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(directionPoint.get(runPos), 15f));
                            driverMarker.setCenter(directionPoint.get(runPos));
                            runElementList.get(i).setRunningPosition(runPos);
                        }
                    }
                }
                break;
            case R.id.previous_iv:
                if (playIv.getVisibility() == View.VISIBLE) {
                    for (int i = 0; i < runElementList.size(); i++) {
                        driverMarker = runElementList.get(i).getCircle();
                        int runPos = runElementList.get(i).getRunningPosition();
                        runPos--;
                        List<LatLng> directionPoint = new ArrayList<>();
                        directionPoint = compareList.get(i).getRouteLatLng();
                        Projection proj = googleMap.getProjection();

                        if (0 < runPos) {
                            boolean contain = proj.getVisibleRegion().latLngBounds.contains(directionPoint.get(i));
                            if (!contain)
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(directionPoint.get(runPos), 15f));
                            driverMarker.setCenter(directionPoint.get(runPos));
                            runElementList.get(i).setRunningPosition(runPos);
                        }
                    }
                }
                break;
            case R.id.oneSpeedTv:
                for (int i = 0; i < runElementList.size(); i++) {
                    runElementList.get(i).setRunSpeed(runElementList.get(i).getOriginalSpeed());
                }
                // interval = 200;
                oneSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                twoSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                fourSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                break;
            case R.id.twoSpeedTv:
                // interval = 100;
                for (int i = 0; i < runElementList.size(); i++) {
                    runElementList.get(i).setRunSpeed(runElementList.get(i).getOriginalSpeed() / 2);
                }
                oneSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                twoSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                fourSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));

                break;
            case R.id.fourSpeedTv:
                for (int i = 0; i < runElementList.size(); i++) {
                    runElementList.get(i).setRunSpeed(runElementList.get(i).getOriginalSpeed() / 4);
                }
                //  interval = 50;
                oneSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                twoSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                fourSpeedTv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
            case R.id.editPolyLine:
                if (isVisibleLine) {
                    for (BeanCompareData beanCompareData : compareList) {
                        beanCompareData.getPolyline().setVisible(false);
                    }
                    isVisibleLine = false;
                } else {
                    for (BeanCompareData beanCompareData : compareList) {
                        beanCompareData.getPolyline().setVisible(true);
                    }
                    isVisibleLine = true;
                }
                break;
            case R.id.google_tv:
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                google_tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                track_tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                break;
            case R.id.track_tv:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                google_tv.setTextColor(ContextCompat.getColor(context, R.color.theme_graycolor));
                track_tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
            case R.id.tvFullViewGraph:
                Intent intent = new Intent(this, FullScreenChartActivity.class);
                intent.putExtra("grouplist", paddockMyLapsParentModel);
                intent.putIntegerArrayListExtra("selectIds", selectIds);
                intent.putExtra("mUserList", mUserList);
                intent.putExtra("CHART_TYPE", chartType);
                intent.putExtra("isSpeed", isSpeed);
                intent.putExtra("isAccCalc", isAccCalc);
                intent.putExtra("isAccOn", isAccOn);
                intent.putExtra("isBrakeCalc", isBrakeCalc);
                intent.putExtra("isBrakeOn", isBrakeOn);

                intent.putExtra("isXView", isXView);
                intent.putExtra("isYView", isYView);
                intent.putExtra("isZView", isZView);
                startActivity(intent);
                break;
            case R.id.tvSelectParamter:
                if(MySharedPreferences.getBooleanPreferences(context,S.checkSubscription)){
                    showSelectParameter(subcriotionTrueItem,subcriotionTrueIndex);
                }
                else {
                    showSelectParameter(subcriotionFalseItem,subcriotionFalseIndex);
                }
                break;
            case R.id.tvSelectChartType:
                showChartTypeDialog();
                break;

        }
    }

    private void showChartTypeDialog(){
        Log.e("tag","selectedDialogIndex"+selectedDialogIndex);
        final CharSequence[] type_radio = {"Time", "Distance"};
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setSingleChoiceItems(type_radio, selectedDialogIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedDialogIndex=i;
            }
        });
        builder.setTitle("Select Chart");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(type_radio[selectedDialogIndex].toString().equalsIgnoreCase("Time")){
                    chartType=type_radio[selectedDialogIndex].toString();
                    setChartData();
                }
                else{
                    chartType=type_radio[selectedDialogIndex].toString();
                    setChartData();
                }
            }
        });
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }

    private void showSelectParameter(String[] items,boolean [] index) {
        selectedItem.clear();
        for(int i=0;i<index.length;i++){
            if(index[i]==true){
                selectedItem.add(i);
            }
        }
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.string_select_parameter));
        builder.setMultiChoiceItems(items,index , new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int selectedItemId, boolean isSelected) {
                if (isSelected) {
                    selectedItem.add(selectedItemId);
                } else if (selectedItem.contains(selectedItemId)) {
                    selectedItem.remove(Integer.valueOf(selectedItemId));
                }
            }
        });
        builder.setPositiveButton(getResources().getString(R.string.select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                updateChart(selectedItem);
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    public void getTrackProfileResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONObject jsonObject1 = jsonObject.getJSONObject(S.data);
                //lenghtTv.setText(jsonObject1.getString("trackLength") + " " + getString(R.string.km));
                //turnsTv.setText(jsonObject1.getJSONObject("turns").getString("totalTurns"));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }
    private class AsyncCaller extends AsyncTask<Void, Void, Void> {
        int position;

        public AsyncCaller(int postion) {
            position = postion;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            List<LatLng> routeLatLng = new ArrayList<>();
            routeLatLng = compareList.get(position).getRouteLatLng();
            final int runSpeed = compareList.get(position).getRunSpeed();
            runElementList.get(position).setRunSpeed(runSpeed);
            runElementList.get(position).setOriginalSpeed(runSpeed);

            final List<LatLng> finalRouteLatLng = routeLatLng;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setAnimation(googleMap, finalRouteLatLng, position);
                }
            });
            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }
    public void setAnimation(GoogleMap myMap, final List<LatLng> directionPoint, int postion) {
        driverMarker = runElementList.get(postion).getCircle();
        animateMarker(myMap, directionPoint, driverMarker, postion);
    }
    private void animateMarker(final GoogleMap myMap, final List<LatLng> directionPoint, final Circle driverMarker, final int postion) {

        final Projection proj = myMap.getProjection();

        runnable = new Runnable() {
            @Override
            public void run() {
                int i = runElementList.get(postion).getRunningPosition();
                int interval = runElementList.get(postion).getRunSpeed();

                if (i < directionPoint.size())
                {
                    boolean contain = proj.getVisibleRegion().latLngBounds.contains(directionPoint.get(i));
                    if (!contain)
                        myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(directionPoint.get(i), 15f));
                    driverMarker.setCenter(directionPoint.get(i));

                    //mUserList.get(postion).setBestLapTime(compareList.get(postion).getLocationArrayList().get(i).getTime());
                    mUserList.get(postion).setBestLapTime(Util.getTimeFormat(compareList.get(postion).getLocationArrayList().get(i).getTime()));
                    mUserList.get(postion).setMaxSpeed(compareList.get(postion).getLocationArrayList().get(i).getSpeed());
                    mUserList.get(postion).setDistance(compareList.get(postion).getLocationArrayList().get(i).getDist());

                    //new changes
                    mUserList.get(postion).setAccel_x(compareList.get(postion).getLocationArrayList().get(i).getAccel_x());
                    mUserList.get(postion).setAccel_y(compareList.get(postion).getLocationArrayList().get(i).getAccel_y());
                    mUserList.get(postion).setAccel_z(compareList.get(postion).getLocationArrayList().get(i).getAccel_z());


                    mUserList.get(postion).setBrake_calc(compareList.get(postion).getLocationArrayList().get(i).getBrake_calc());
                    mUserList.get(postion).setBrake_on(compareList.get(postion).getLocationArrayList().get(i).getBrake_on());
                    mUserList.get(postion).setAcc_calc(compareList.get(postion).getLocationArrayList().get(i).getAcc_calc());
                    mUserList.get(postion).setAcc_on(compareList.get(postion).getLocationArrayList().get(i).getAcc_on());

                    paddockRaceUsersAdapter.notifyDataSetChanged();
                    i++;
                    runElementList.get(postion).setRunningPosition(i);
                    handler.postDelayed(this, interval);
                } else {
                    runStatus = 0;
                    playIv.setVisibility(View.VISIBLE);
                    pauseIv.setVisibility(View.GONE);
                    handler.removeCallbacks(runElementList.get(postion).getRunnable());
                    runElementList.get(postion).setRunningPosition(0);
                }
            }
        };
        handler.post(runnable);
        runElementList.get(postion).setRunnable(runnable);
    }
    private void downloadFiles() {
        mUserList.clear();
        runElementList.clear();
        for (int i = 0; i < selectIds.size(); i++) {
            String filePath = "";
            mUserList.add(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)));
            filePath = paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getFileUrl();
            int color = paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getLapColor();
          //  mUserList.get(i).setLapColor(color);
            mUserList.get(i).setUserName(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getUserName());
            mUserList.get(i).setUserImage(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getUserImage());
            long milisecond = Util.TimeToMiliseconds(paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(i)).getTime());
            authAPI.readJsonFileData(context, filePath, color, milisecond);
            paddockRaceUsersAdapter.notifyDataSetChanged();
            compareLapAdapter.notifyDataSetChanged();
        }

    }
    public void drowGoogleMap(PolylineOptions polylineOptions, int color) {
        if (googleMap != null) {
            isVisibleLine = true;
            driverMarker = googleMap.addCircle(new CircleOptions().center(polylineOptions.getPoints().get(0)).radius(5).zIndex(1.0f).strokeColor(color).fillColor(color));
            BeanRunElement beanRunElement = new BeanRunElement();
            beanRunElement.setCircle(driverMarker);
            runElementList.add(beanRunElement);
            if(!paddockMyLapsParentModel.getTrackRxLat().equalsIgnoreCase("0")&& !paddockMyLapsParentModel.getTrackRxLng().equalsIgnoreCase("0")){
                new AddGroundOverlay().execute(paddockMyLapsParentModel.getOverlay_image());
            }
            CameraPosition cameraPosition = new CameraPosition.Builder().target(polylineOptions.getPoints().get(0)).zoom(15).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            //formatGoogleMap();
        }
    }
    private void formatGoogleMap(){
        if(markersList.size()>0){
            for(int i=0;i<markersList.size();i++){
                markersList.get(i).remove();
            }
        }
        for (int j = 0; j < selectIds.size(); j++)
        {
            int color = paddockMyLapsParentModel.getMyLapsChildList().get(selectIds.get(j)).getLapColor();
            //Random rnd = new Random();
            /*int new_color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            while(new_color==color){
                new_color=Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            }*/
            Log.e("tag420","Add color"+color);
            BeanCompareData beanCompareData = new BeanCompareData();
            beanCompareData = compareList.get(j);
            ArrayList<BeanLocation> locationList = new ArrayList<>();
            locationList = beanCompareData.getLocationArrayList();
            for(int i = 0 ; i < locationList.size() ; i++)
            {
                if(isBrakeOn && isBrakeLineShow(Double.parseDouble(locationList.get(i).getBrake_on()),
                        Double.parseDouble(locationList.get(i).getBrake_calc()))) {
                    createMarker(locationList.get(i).getLat(), locationList.get(i).getLng(),color,"is_brake");
                }
                if(isAccOn && isAccelLineShow(Double.parseDouble(locationList.get(i).getAcc_on()),
                        Double.parseDouble(locationList.get(i).getAcc_calc()))){
                    createMarker(locationList.get(i).getLat(), locationList.get(i).getLng(),color,"is_accel");
                }
                if(isBrakeCalc && isBrakeCalcLineShow(Double.parseDouble(locationList.get(i).getBrake_calc()))) {
                    createMarker(locationList.get(i).getLat(), locationList.get(i).getLng(),color,"is_brake");
                }
                if(isAccCalc && isAccelCalcLineShow(Double.parseDouble(locationList.get(i).getAcc_calc()))) {
                    createMarker(locationList.get(i).getLat(), locationList.get(i).getLng(),color,"is_accel");
                }

            }
        }
    }
    protected void createMarker(String latitude, String longitude,int color,String isFor) {

        if(!latitude.equalsIgnoreCase("0")&&!longitude.equalsIgnoreCase("0")){
            MarkerOptions markerOptions=new MarkerOptions();
            if(isFor.equalsIgnoreCase("is_brake")){
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Util.getMarkerBitmapFromView(R.drawable.hexagonal,this, color)));
            }
            else{
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Util.getMarkerBitmapFromView(R.drawable.square,this, color)));
            }
            markerOptions.anchor(0.5f, 0.5f).position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)));
            markersList.add(googleMap.addMarker(markerOptions));

        }
    }
    private void updateChart(ArrayList selectedData){
        myProgressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                myProgressDialog.dismiss();
            }
        },2000);

        isSpeed=false;isBrakeOn=false;isAccOn=false;isBrakeCalc=false;isAccCalc=false;
        isXView=false;isYView=false;isZView=false;
        if(!selectedData.isEmpty()){
            if (selectedData.contains(0)) {
                isSpeed=true;
            }
            if (selectedData.contains(1)) {
                isBrakeOn=true;
            }
            if (selectedData.contains(2)) {
                isAccOn=true;
            }
            if (selectedData.contains(3)) {
                isBrakeCalc=true;
            }
            if (selectedData.contains(4)) {
                isAccCalc=true;
            }
            if (selectedData.contains(5)) {
                isXView=true;
            }
            if (selectedData.contains(6)) {
                isYView=true;
            }
            if (selectedData.contains(7)) {
                isZView=true;
            }
        }
        formatGoogleMap();
        setChartData();
    }
    private void setChartData() {
        HIOptions hiOptions=null;
        chartDetailsSpline.removeAllViews();
        HIChartView chartView=new HIChartView(this);
        chartDetailsSpline.addView(chartView);
        float height = getResources().getDisplayMetrics().heightPixels;
        chartView.getLayoutParams().height = Math.round(height*2/5);
        chartView.setWillNotDraw(true);
        if(chartType.equalsIgnoreCase("Time")){
            hiOptions=manager.setChart(chartView,compareList,mUserList,
                    isSpeed, isAccCalc,isAccOn,isBrakeCalc,
                    isBrakeOn,isXView,isYView,isZView);
        }
        else {
            hiOptions=manager.setChartWithDistance(chartView,compareList,mUserList,
                    isSpeed, isAccCalc,isAccOn,isBrakeCalc,
                    isBrakeOn,isXView,isYView,isZView);
        }
        chartView.setWillNotDraw(false);
        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setSeries(new HISeries());
        plotOptions.getSeries().setColor(HIColor.initWithRGBA(255, 255, 255, 0.6));
        plotOptions.getSeries().setPoint(new HIPoint());
        plotOptions.getSeries().getPoint().setEvents(new HIEvents());
        hiOptions.setPlotOptions(plotOptions);
        hiOptions.getPlotOptions().getSeries().getPoint().getEvents().setMouseOver(new HIFunction(new HIConsumer<HIChartContext>()
        {
            @Override
            public void accept(HIChartContext hiChartContext) {
                try {
                    Double index= (Double) hiChartContext.getProperty("index");
                    Double x= (Double) hiChartContext.getProperty("x");
                    String color= (String) hiChartContext.getProperty("color");
                    changeDataPosition(index.intValue(),color);
                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        }, new String[] {"index","x","color"}));

        myProgressDialog.dismiss();

        paddockRaceUsersAdapter.notifyDataSetChanged();


    }
    private void changeDataPosition(int xIndex,String color) {
        int selectedLapIndex = 0;
        for (int i = 0; i < compareList.size(); i++) {
            if (xIndex < compareList.get(i).getRouteLatLng().size()) {
                runElementList.get(i).getCircle().setCenter(compareList.get(i).getRouteLatLng().get(xIndex));
                runElementList.get(i).setRunningPosition(xIndex + 1);

                String hexColor = String.format("#%06X", (0xFFFFFF & compareList.get(i).getColor()));
                if(hexColor.equalsIgnoreCase(color)){
                   // myNewFunction(xIndex,color);
                    selectedLapIndex=i;

                    break;
                }

            }
        }
        myNewFunction(xIndex,color,selectedLapIndex);
        //setDataToCompare(selectedLapIndex,xIndex);

       // setDataToCompare(selectedLapIndex,xIndex);
    }

    private void myNewFunction(int mouseIndex,String chartColor,int selectedLapIndex) {
        ArrayList<BeanCompareData>dataToCompare = compareList;
        double targetValue=compareList.get(selectedLapIndex).getTimeList().get(mouseIndex);
        Log.e("tag","if"+targetValue);
        for(int i=0;i<compareList.size();i++)
        {
            //String hexColor = String.format("#%06X", (0xFFFFFF & compareList.get(i).getColor()));
            if(i==selectedLapIndex){
                mUserList.get(i).setBestLapTime(Util.getTimeFormat(compareList.get(i).getLocationArrayList().get(mouseIndex).getTime()));
                mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(mouseIndex).getDist());

                mUserList.get(i).setMaxSpeed(compareList.get(i).getLocationArrayList().get(mouseIndex).getSpeed());
                mUserList.get(i).setAccel_x(compareList.get(i).getLocationArrayList().get(mouseIndex).getAccel_x());
                mUserList.get(i).setAccel_y(compareList.get(i).getLocationArrayList().get(mouseIndex).getAccel_y());
                mUserList.get(i).setAccel_z(compareList.get(i).getLocationArrayList().get(mouseIndex).getAccel_z());

                mUserList.get(i).setBrake_calc(compareList.get(i).getLocationArrayList().get(mouseIndex).getBrake_calc());
                mUserList.get(i).setBrake_on(compareList.get(i).getLocationArrayList().get(mouseIndex).getBrake_on());
                mUserList.get(i).setAcc_calc(compareList.get(i).getLocationArrayList().get(mouseIndex).getAcc_calc());
                mUserList.get(i).setAcc_on(compareList.get(i).getLocationArrayList().get(mouseIndex).getAcc_on());

               // mUserList.get(i).setLapColor(Color.parseColor(chartColor));


            }
            else {
                //double targetValue;
                if(chartType.equalsIgnoreCase("Time")){
                    targetValue=compareList.get(selectedLapIndex).getTimeList().get(mouseIndex);
                }
                else {
                    targetValue=compareList.get(selectedLapIndex).getDistanceList().get(mouseIndex);
                }
                Log.e("tag","else"+targetValue);
                int closest=getClosestValue(targetValue,chartType,i);
                Log.e("tag","closest"+closest);


                if(mouseIndex >= compareList.get(i).getLocationArrayList().size()){
                    int diff = mouseIndex - compareList.get(i).getLocationArrayList().size();
                    int toAdd = diff + 1;
                    for(int j=0;j< toAdd;j++) {
                        BeanLocation beanLocation=dataToCompare.get(i).getLocationArrayList().get(closest);
                        dataToCompare.get(i).getLocationArrayList().add(beanLocation);
                    }
                }
                else {
                    dataToCompare.get(i).getLocationArrayList().set(mouseIndex,compareList.get(i).getLocationArrayList().get(closest));

                }


                mUserList.get(i).setBestLapTime(Util.getTimeFormat(dataToCompare.get(i).getLocationArrayList().get(closest).getTime()));
                mUserList.get(i).setDistance(dataToCompare.get(i).getLocationArrayList().get(closest).getDist());

                mUserList.get(i).setMaxSpeed(dataToCompare.get(i).getLocationArrayList().get(closest).getSpeed());
                mUserList.get(i).setAccel_x(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_x());
                mUserList.get(i).setAccel_y(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_y());
                mUserList.get(i).setAccel_z(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_z());

                mUserList.get(i).setBrake_calc(dataToCompare.get(i).getLocationArrayList().get(closest).getBrake_calc());
                mUserList.get(i).setBrake_on(dataToCompare.get(i).getLocationArrayList().get(closest).getBrake_on());
                mUserList.get(i).setAcc_calc(dataToCompare.get(i).getLocationArrayList().get(closest).getAcc_calc());
                mUserList.get(i).setAcc_on(dataToCompare.get(i).getLocationArrayList().get(closest).getAcc_on());

                //mUserList.get(i).setLapColor(Color.parseColor(chartColor));

            }
            Log.e("tag","selectedLapIndex"+selectedLapIndex);
            paddockRaceUsersAdapter.notifyDataSetChanged();

        }
    }




/*
    private void myNewFunction(int mouseIndex,String chartColor,int selectedLapIndex) {
        int selectedLapIndex = 0;
        ArrayList<BeanCompareData>dataToCompare = compareList;
        for(int i=0;i<compareList.size();i++){
            String hexColor = String.format("#%06X", (0xFFFFFF & compareList.get(i).getColor()));
            if(hexColor.equalsIgnoreCase(chartColor)){
                selectedLapIndex=i;
                mUserList.get(i).setBestLapTime(Util.getTimeFormat(compareList.get(i).getLocationArrayList().get(mouseIndex).getTime()));
                mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(mouseIndex).getDist());

                mUserList.get(i).setMaxSpeed(compareList.get(i).getLocationArrayList().get(mouseIndex).getSpeed());
                mUserList.get(i).setAccel_x(compareList.get(i).getLocationArrayList().get(mouseIndex).getAccel_x());
                mUserList.get(i).setAccel_y(compareList.get(i).getLocationArrayList().get(mouseIndex).getAccel_y());
                mUserList.get(i).setAccel_z(compareList.get(i).getLocationArrayList().get(mouseIndex).getAccel_z());

                mUserList.get(i).setBrake_calc(compareList.get(i).getLocationArrayList().get(mouseIndex).getBrake_calc());
                mUserList.get(i).setBrake_on(compareList.get(i).getLocationArrayList().get(mouseIndex).getBrake_on());
                mUserList.get(i).setAcc_calc(compareList.get(i).getLocationArrayList().get(mouseIndex).getAcc_calc());
                mUserList.get(i).setAcc_on(compareList.get(i).getLocationArrayList().get(mouseIndex).getAcc_on());

                mUserList.get(i).setLapColor(Color.parseColor(chartColor));
                double targetValue=compareList.get(selectedLapIndex).getTimeList().get(mouseIndex);
                Log.e("tag","if"+targetValue);

            }
            else {
                double targetValue;
                if(chartType.equalsIgnoreCase("Time")){
                    targetValue=compareList.get(selectedLapIndex).getTimeList().get(mouseIndex);
                }
                else {
                    targetValue=compareList.get(selectedLapIndex).getDistanceList().get(mouseIndex);
                }
                Log.e("tag","else"+targetValue);
                int closest=getClosestValue(targetValue,chartType,i);
                Log.e("tag","closest"+closest);


                if(mouseIndex >= compareList.get(i).getLocationArrayList().size()){
                    int diff = mouseIndex - compareList.get(i).getLocationArrayList().size();
                    int toAdd = diff + 1;
                    for(int j=0;j< toAdd;j++) {
                        BeanLocation beanLocation=dataToCompare.get(i).getLocationArrayList().get(closest);
                        dataToCompare.get(i).getLocationArrayList().add(beanLocation);
                    }
                }
                else {
                    dataToCompare.get(i).getLocationArrayList().set(mouseIndex,compareList.get(i).getLocationArrayList().get(closest));

                }


                mUserList.get(i).setBestLapTime(Util.getTimeFormat(dataToCompare.get(i).getLocationArrayList().get(closest).getTime()));
                mUserList.get(i).setDistance(dataToCompare.get(i).getLocationArrayList().get(closest).getDist());

                mUserList.get(i).setMaxSpeed(dataToCompare.get(i).getLocationArrayList().get(closest).getSpeed());
                mUserList.get(i).setAccel_x(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_x());
                mUserList.get(i).setAccel_y(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_y());
                mUserList.get(i).setAccel_z(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_z());

                mUserList.get(i).setBrake_calc(dataToCompare.get(i).getLocationArrayList().get(closest).getBrake_calc());
                mUserList.get(i).setBrake_on(dataToCompare.get(i).getLocationArrayList().get(closest).getBrake_on());
                mUserList.get(i).setAcc_calc(dataToCompare.get(i).getLocationArrayList().get(closest).getAcc_calc());
                mUserList.get(i).setAcc_on(dataToCompare.get(i).getLocationArrayList().get(closest).getAcc_on());

                //mUserList.get(i).setLapColor(Color.parseColor(chartColor));

            }
            Log.e("tag","selectedLapIndex"+selectedLapIndex);
            paddockRaceUsersAdapter.notifyDataSetChanged();

        }
    }
*/





    private int getClosestValue(double targetValue,String tag, int index){
        Double [] numbers;
        if(tag.equalsIgnoreCase("Time")){
            numbers = compareList.get(index).getTimeList().toArray(new Double[compareList.get(index).getTimeList().size()]);
        }
        else {
            numbers = compareList.get(index).getDistanceList().toArray(new Double[compareList.get(index).getDistanceList().size()]);
        }
       // int myNumber = 490;
        double distance =  Math.abs(numbers[0] - targetValue);
        int idx = 0;
        for(int c = 1; c < numbers.length; c++)
        {
            double cdistance =  Math.abs(numbers[c] - targetValue);
            if(cdistance < distance){
                idx = c;
                distance = cdistance;
            }
        }
        double theNumber = numbers[idx];
        Log.e("tag",""+theNumber);
        return idx;

    }
    private boolean isBrakeLineShow(double mBrakeOn,double mBrakeCalc){
        boolean isShow;
        if(mBrakeOn==1 && mBrakeCalc>0) {
            if(markerBrake.equalsIgnoreCase("1")){
                isShow= false;
            }
            else {
                markerBrake="1";
                isShow= true;
            }
        }
        else {
            markerBrake="0";
            isShow= false;
        }
        return isShow;
    }
    private boolean isAccelLineShow(double mAccelOn,double mAccelCalc){
        boolean isShow;
        if(mAccelOn==1 && mAccelCalc>0){
            if(markerAccel.equalsIgnoreCase("1")){
                isShow= false;
            }
            else {
                markerAccel="1";
                isShow= true;
            }
        }
        else {
            markerAccel="0";
            isShow= false;
        }
        return isShow;
    }
    private boolean isBrakeCalcLineShow(double mBrakeCalc){
        boolean isShow;
        if(mBrakeCalc>0) {
            if(markerBrakeCalc>0){
                isShow= false;
            }
            else {
                markerBrakeCalc=mBrakeCalc;
                isShow= true;
            }
        }
        else {
            markerBrakeCalc=0;
            isShow= false;
        }
        return isShow;
    }
    private boolean isAccelCalcLineShow(double mAccelCalc){
        boolean isShow;
        if(mAccelCalc>0) {
            if(markerAccelCalc>0){
                isShow= false;
            }
            else {
                markerAccelCalc=mAccelCalc;
                isShow= true;
            }
        }
        else {
            markerAccelCalc=0;
            isShow= false;
        }
        return isShow;
    }
    public class AddGroundOverlay extends AsyncTask<String, Integer, BitmapDescriptor> {

        BitmapDescriptor bitmapDescriptor;
        String imageUrl;
        @Override
        protected BitmapDescriptor doInBackground(String... url) {
            imageUrl = url[0];
            try {
                bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(Picasso.with(context).load(S.IMAGE_BASE_URL+imageUrl).get());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmapDescriptor;
        }
        protected void onPostExecute(BitmapDescriptor icon) {
            try {
                LatLngBounds newarkBounds = new LatLngBounds(
                        new LatLng(Double.parseDouble(paddockMyLapsParentModel.getTrackLxLat()), Double.parseDouble(paddockMyLapsParentModel.getTrackLxLng())),       // South west corner
                        new LatLng(Double.parseDouble(paddockMyLapsParentModel.getTrackRxLat()), Double.parseDouble(paddockMyLapsParentModel.getTrackRxLng())));      // North east corner
                GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                       .image(icon)
                        .positionFromBounds(newarkBounds);
                googleMap.addGroundOverlay(newarkMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /*private void setDataToCompare(int lapIndex,int index){
        ArrayList<BeanCompareData>dataToCompare = compareList;
        double targetValue;
        if(chartType.equalsIgnoreCase("Time")){
            targetValue=compareList.get(lapIndex).getTimeList().get(index);
        }
        else {
            targetValue=compareList.get(lapIndex).getDistanceList().get(index);
        }
        // double targetValue= Double.parseDouble(compareList.get(lapIndex).getLocationArrayList().get(index).getTime());
        for(int i = 0; i < compareList.size(); i++ )
        {
            if(i==lapIndex){
                mUserList.get(i).setBestLapTime(Util.getTimeFormat(dataToCompare.get(i).getLocationArrayList().get(i).getTime()));
                mUserList.get(i).setDistance(dataToCompare.get(i).getLocationArrayList().get(i).getDist());
                mUserList.get(i).setMaxSpeed(dataToCompare.get(i).getLocationArrayList().get(i).getSpeed());
                mUserList.get(i).setAccel_x(dataToCompare.get(i).getLocationArrayList().get(i).getAccel_x());
                mUserList.get(i).setAccel_y(dataToCompare.get(i).getLocationArrayList().get(i).getAccel_y());
                mUserList.get(i).setAccel_z(dataToCompare.get(i).getLocationArrayList().get(i).getAccel_z());

                mUserList.get(i).setBrake_calc(dataToCompare.get(i).getLocationArrayList().get(i).getBrake_calc());
                mUserList.get(i).setBrake_on(dataToCompare.get(i).getLocationArrayList().get(i).getBrake_on());
                mUserList.get(i).setAcc_calc(dataToCompare.get(i).getLocationArrayList().get(i).getAcc_calc());
                mUserList.get(i).setAcc_on(dataToCompare.get(i).getLocationArrayList().get(i).getAcc_on());
            }
            else {
                int closest=getClosestValue(targetValue,chartType,i);
                mUserList.get(i).setBestLapTime(Util.getTimeFormat(dataToCompare.get(i).getLocationArrayList().get(closest).getTime()));
                mUserList.get(i).setDistance(dataToCompare.get(i).getLocationArrayList().get(closest).getDist());
                mUserList.get(i).setMaxSpeed(dataToCompare.get(i).getLocationArrayList().get(closest).getSpeed());
                mUserList.get(i).setAccel_x(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_x());
                mUserList.get(i).setAccel_y(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_y());
                mUserList.get(i).setAccel_z(dataToCompare.get(i).getLocationArrayList().get(closest).getAccel_z());

                mUserList.get(i).setBrake_calc(dataToCompare.get(i).getLocationArrayList().get(closest).getBrake_calc());
                mUserList.get(i).setBrake_on(dataToCompare.get(i).getLocationArrayList().get(closest).getBrake_on());
                mUserList.get(i).setAcc_calc(dataToCompare.get(i).getLocationArrayList().get(closest).getAcc_calc());
                mUserList.get(i).setAcc_on(dataToCompare.get(i).getLocationArrayList().get(closest).getAcc_on());
               *//* if(index >= compareList.get(i).getLocationArrayList().size()){
                    int diff = index - compareList.get(i).getLocationArrayList().size();
                    int toAdd = diff + 1;
                    for(int j=0;j< toAdd;j++) {
                        BeanLocation beanLocation=dataToCompare.get(i).getLocationArrayList().get(closest);
                        dataToCompare.get(i).getLocationArrayList().add(beanLocation);
                    }
                }
                else {
                    dataToCompare.get(i).getLocationArrayList().set(index,compareList.get(i).getLocationArrayList().get(closest));
                    Log.e("tag","call else"+closest);

                }*//*
            }

            paddockRaceUsersAdapter.notifyDataSetChanged();

             *//*mUserList.get(i).setBestLapTime(Util.getTimeFormat(compareList.get(i).getLocationArrayList().get(index).getTime()));
            mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(index).getDist());

            Double [] arrDistance = compareList.get(i).getDistanceList().toArray(new Double[compareList.get(i).getDistanceList().size()]);
            Arrays.sort(arrDistance);

            Double finalDistance=findClosestForDistance(arrDistance,compareList.get(lapIndex).getDistanceList().get(index));
            mUserList.get(i).setBestLapTime(Util.getTimeFormat(finalTarget+""));
            mUserList.get(i).setDistance(finalDistance+"");*//*

        }


    }*/




    //targetDistance=compareList.get(i).getDistanceList().get(xIndex);
               /* if(compareList.get(i).getLocationArrayList().get(xIndex).getTime().equalsIgnoreCase(time/1000+"")){
                    Log.e("tag","target match"+targetTime);
                    mUserList.get(i).setBestLapTime(Util.getTimeFormat(compareList.get(i).getLocationArrayList().get(xIndex).getTime()));
                    mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(xIndex).getDist());
                    targetTime=compareList.get(i).getTimeList().get(xIndex);
                }
                else {
                    Double [] arrTime = compareList.get(i).getTimeList().toArray(new Double[compareList.get(i).getTimeList().size()]);
                    Arrays.sort(arrTime);
                    Double [] arrDistance = compareList.get(i).getDistanceList().toArray(new Double[compareList.get(i).getDistanceList().size()]);
                    Arrays.sort(arrDistance);
                    Log.e("tag","don't match hhh-"+targetTime);
                    Double finalTarget=findClosest(arrTime,targetTime);
                    Double finalDistance=findClosestForDistance(arrDistance,targetDistance);
                    Log.e("tag","don't match"+finalTarget);
                    mUserList.get(i).setBestLapTime(Util.getTimeFormat(finalTarget+""));
                    mUserList.get(i).setDistance(finalDistance+"");
                }*/
                /*if(String.valueOf(compareList.get(i).getLocationArrayList().get(xIndex).getTime()).equalsIgnoreCase(String.valueOf(time))){
                    Log.e("tag","match"+compareList.get(i).getColor());

                    *//*Double [] arrTime = compareList.get(i).getTimeList().toArray(new Double[compareList.get(i).getTimeList().size()]);
                    Double [] arrDis = compareList.get(i).getDistanceList().toArray(new Double[compareList.get(i).getDistanceList().size()]);
                    Arrays.sort(arrTime);
                    Arrays.sort(arrDis);

                    Double time=findClosest(arrTime, compareList.get(i).getTimeList().get(i));
                    mUserList.get(i).setBestLapTime();
                    mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(xIndex).getDist());*//*
                }
                else {
                    Log.e("tag","don't match");
                    *//*mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(xIndex).getDist());
                    mUserList.get(i).setBestLapTime(compareList.get(i).getLocationArrayList().get(xIndex).getTime());*//*
                }*/
               /* if(chartType.equalsIgnoreCase("Time")){
                    mUserList.get(i).setBestLapTime(xAxis);
                    mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(xIndex).getDist());
                    Double [] arr = compareList.get(i).getTimeList().toArray(new Double[compareList.get(i).getTimeList().size()]);
                    Arrays.sort(arr);
                    //System.out.println(findClosest(arr, compareList.get(i).getTimeList().get(xIndex)));
                    //Log.e("tag","dddd"+Util.getTimeFormat(String.valueOf(findClosest(arr, compareList.get(i).getTimeList().get(xIndex)))));
                    Log.e("tag","target value"+findClosest(arr,23.200));
                    Log.e("tag","target"+compareList.get(i).getTimeList().get(xIndex));
                    //mUserList.get(i).setBestLapTime(Util.getTimeFormat(compareList.get(i).getLocationArrayList().get(xIndex).getTime()));
                }
                else {
                    mUserList.get(i).setBestLapTime(Util.getTimeFormat(compareList.get(i).getLocationArrayList().get(xIndex).getTime()));
                    mUserList.get(i).setDistance(xAxis);
                    //mUserList.get(i).setDistance(compareList.get(i).getLocationArrayList().get(xIndex).getDist());
                }*/

    //mUserList.get(i).setBestLapTime(Util.getTimeFormat(compareList.get(i).getLocationArrayList().get(xIndex).getTime()));






}
