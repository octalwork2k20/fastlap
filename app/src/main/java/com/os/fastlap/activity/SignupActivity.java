package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.os.fastlap.R;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.fragment.settings.TermsConditionFragment;
import com.os.fastlap.fragment.signup.SignupUserOtherDetailFragment;
import com.os.fastlap.fragment.signup.SignupUserPersonalDetailFragment;
import com.os.fastlap.fragment.signup.SignupUsertypeFragment;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by abhinava on 7/10/2017.
 */

public class SignupActivity extends BaseActivity implements SignupUserPersonalDetailFragment.MyInterface {
    public String TAG = "SignupActivity.java";
    public FragmentManager mFragmentManager = null;
    FragmentTransaction fragmentTransaction = null;
    Fragment mFragment = null;
    public Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        mFragmentManager = getSupportFragmentManager();
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        displayView(FastLapConstant.SIGNUPUSERPERSONALDETAILFRAGMENT, null, null);
    }

    public void displayView(String fragmentName, Object obj, Object list) {
        fragmentTransaction = mFragmentManager.beginTransaction();

        if (fragmentName.equals(FastLapConstant.SIGNUPUSERTYPEFRAGMENT)) {
            mFragment = SignupUsertypeFragment.newInstance(mContext, obj);
        } else if (fragmentName.equals(FastLapConstant.SIGNUPUSERPERSONALDETAILFRAGMENT)) {
            mFragment = SignupUserPersonalDetailFragment.newInstance(mContext, obj);
        } else if (fragmentName.equals(FastLapConstant.SIGNUPUSEROTHERDETAILFRAGMENT)) {
            mFragment = SignupUserOtherDetailFragment.newInstance(mContext, obj);
        } else if (fragmentName.equals(FastLapConstant.TERMSCONDITIONFRAGMENT)) {
            mFragment = new TermsConditionFragment();
        }
        if (mFragment != null) {
            try {
                fragmentTransaction.replace(R.id.container_fl, mFragment).addToBackStack(fragmentName);
                fragmentTransaction.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // signup  webservice success response
    public void getResponse(String response) {
        try {
            JSONObject jsonObject1 = new JSONObject(response);
            String msg = jsonObject1.getString(S.message);
            if (jsonObject1.getInt(S.status) == S.adi_status_success) {
                JSONObject jsonObject = jsonObject1.getJSONObject(S.data);
                startOtpScreen(jsonObject);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // getNationality webservice success
    public void getNationalityWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                if (mFragment instanceof SignupUserOtherDetailFragment)
                    ((SignupUserOtherDetailFragment) mFragment).receiveNationalityResponse(jsonArray);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // LanguageListing webservice response
    public void getLanguage(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                if (mFragment instanceof SignupUserOtherDetailFragment)
                    ((SignupUserOtherDetailFragment) mFragment).getLanguageResponse(jsonArray);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void startOtpScreen(JSONObject data) {
        Intent intent = new Intent(mContext, OTPActivity.class);
        intent.putExtra(S.sign_up_data, data.toString());
        startActivity(intent);
        finish();
    }


    @Override
    public void onBackPressed()
    {
        hideKeyboard();
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            Intent intent = new Intent(mContext, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void transferData(String fullnmae, String lastName, String nickName, String email, String mobile, String password, String confirm_password,String countryCode) {
        ((SignupUserOtherDetailFragment) mFragment).getUserPersonalFragmentData(fullnmae, lastName, nickName, email, mobile, password, confirm_password,countryCode);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    public void OnBackCross()
    {
        hideKeyboard();
        if (getSupportFragmentManager().getBackStackEntryCount() > 1)
        {
            getSupportFragmentManager().popBackStack();
        } else {
            Intent intent = new Intent(mContext, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

}
