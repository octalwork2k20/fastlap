package com.os.fastlap.activity.profile;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by anandj on 8/11/2017.
 */

public class AddClothesActivity extends BaseActivity implements View.OnClickListener, SpinnerSelectorInterface, OnImagePickerDialogSelect {

    Context context;
    private AppCompatImageView base_toggle_icon;
    private TextViewPlayBold toolbarheading;
    private Context mContext;
    private SpinnerSelectorInterface spinnerSelectorInterface;
    private RelativeLayout actionBar;
    private AppCompatImageView baseToggleIcon;
    private View view;

    private TextViewPlayRegular clothBrandTv;
    private TextViewPlayRegular clothVehicleTv;
    private EditText clothModelTv;
    private EditText clothYearTv;
    private EditText clothesColorEt;
    private TextViewPlayRegular btnBrowse;
    private TextViewPlayBold addClothBtn;
    private TextViewPlayRegular image_name_tv;
    AuthAPI authAPI;
    private String TAG = AddClothesActivity.class.getSimpleName();
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private ArrayList<BeanVehicleType> clothBrand_list;
    private ArrayList<BeanVehicleType> vehicleTypes_list;
    String clothBrandID = "";
    String clothBrandName = "";
    String clothVehicleID = "";
    String clothVehicleName = "";
    String clothName = "";
    String clothModelName = "";
    String clothYear = "";
    String clothColor = "";
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    public PagerBean pagerBean;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_add_clothing);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        spinnerSelectorInterface = this;
        onImagePickerDialogSelect = this;
        initView();
    }

    private void initView() {
        base_toggle_icon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        clothBrandTv = (TextViewPlayRegular) findViewById(R.id.cloth_brand_tv);
        clothVehicleTv=(TextViewPlayRegular)findViewById(R.id.cloth_vehicle_tv);
        clothModelTv = (EditText) findViewById(R.id.cloth_model_tv);
        clothYearTv = (EditText) findViewById(R.id.cloth_year_tv);
        clothesColorEt = (EditText) findViewById(R.id.clothes_color_et);
        btnBrowse = (TextViewPlayRegular) findViewById(R.id.btn_browse);
        addClothBtn = (TextViewPlayBold) findViewById(R.id.add_cloth_btn);
        image_name_tv = (TextViewPlayRegular) findViewById(R.id.image_name_tv);

        clothBrand_list = new ArrayList<>();
        vehicleTypes_list=new ArrayList<>();
        clothBrand_list.clear();
        vehicleTypes_list.clear();

        clothBrandTv.setOnClickListener(this);
        addClothBtn.setOnClickListener(this);
        btnBrowse.setOnClickListener(this);
        base_toggle_icon.setOnClickListener(this);
        clothVehicleTv.setOnClickListener(this);

        authAPI.getClothBrand(context, MySharedPreferences.getPreferences(context, S.user_id));
        authAPI.getVehicleType(context);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.cloth_brand_tv:
                showVehicleTypeDialog(context.getResources().getString(R.string.select_clothing_brand), clothBrand_list, I.CLOTH_BRAND);
                break;
            case R.id.cloth_vehicle_tv:
                showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_type), vehicleTypes_list, I.CLOTH_VEHICLE);
                break;
            case R.id.btn_browse:
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
                break;
            case R.id.add_cloth_btn:
                Util.hideKeyboard(getCurrentFocus(), context);
                clothModelName = clothModelTv.getText().toString().trim();
                clothYear = clothYearTv.getText().toString().trim();
                clothColor = clothesColorEt.getText().toString().trim();
                clothName = clothBrandName + " " + clothModelName + " " + clothYear;
                try {
                    if (Validation.addClothValidation(clothName, clothBrandID, clothModelName, clothYear, clothColor, pagerBean, addClothBtn, context))
                        authAPI.addCloth(context, clothName, clothBrandID, clothModelName, clothYear, clothColor,clothVehicleID, MySharedPreferences.getPreferences(context, S.user_id), pagerBean);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }

                break;
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.add_cloth));
        FastLapApplication.mCurrentContext = context;
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.CLOTH_BRAND:
                clothBrandTv.setText(value);
                clothBrandID = id;
                clothBrandName = value;
                break;
            case I.CLOTH_VEHICLE:
                clothVehicleTv.setText(value);
                clothVehicleID = id;
                clothVehicleName = value;
                break;
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }

    // Add Vehicle Webservice Response
    public void getAddClothResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");
            else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* Cloth brand webservice response */
    public void getClothBrand(String response) {
        clothBrand_list.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    clothBrand_list.add(beanVehicleType);
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void getVehicleTypeResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    vehicleTypes_list.add(beanVehicleType);
                }

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
   /* *//* cloth model webservice response *//*
    public void getClothModel(String response)
    {
        clothModel_list.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    clothModel_list.add(beanVehicleType);
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    *//* cloth year webservice response*//*
    public void getClothYear(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.years));
                    clothYear_list.add(beanVehicleType);
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    *//* cloth color webservice response *//*
    public void getClothColor(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    clothColor_list.add(beanVehicleType);
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }*/

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {

        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();

        pagerBean = null;
        image_name_tv.setText(getString(R.string.clothing_picture));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }

    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {
        EasyImage.openGallery(this, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, AddClothesActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;
                String imageAbsolutePath = imageFile.getAbsolutePath();
                String imageFileName = imageFile.getName();
                image_name_tv.setText(imageFileName);

                pagerBean = new PagerBean(imageFile, imageAbsolutePath, imageFileName, false, false, "");
                image_name_tv.setText(pagerBean.getFileName());
            }


        });
    }



}
