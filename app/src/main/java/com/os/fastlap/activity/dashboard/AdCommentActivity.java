package com.os.fastlap.activity.dashboard;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.dashboard.AdCommentAdapter;
import com.os.fastlap.adapter.dashboard.DashboardCommentAdapter;
import com.os.fastlap.beans.Bean_UserDetail;
import com.os.fastlap.beans.dashboardmodals.CommentBeans;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by monikab on 1/8/2016.
 */
public class AdCommentActivity extends BaseActivity implements SharingDeleget,View.OnClickListener, AdCommentAdapter.CommentInterFace {

    ImageView base_toggle_icon;
    RecyclerView commentRecyclerView;
    AdCommentAdapter dashboardCommentAdapter;
    public EditText add_comment_et;
    ImageButton send_btn;
    String post_id = "";
    String comment = "";
    Context context;
    TextView no_data_tv;
    MyProgressDialog myProgressDialog;
    Bean_UserDetail user_data;
    private AuthAPI mAuthAPI;
    private List<CommentBeans> mCommentBeansList;
    private final String TAG = AdCommentActivity.class.getSimpleName();
    DashboardCommentAdapter.CommentInterFace commentInterFace;
    AuthAPI authAPI;
    String postPosition = "0";
    private LinearLayoutManager mLinearLayoutManager;
    private SharingDeleget sharingDeleget;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.ad_comment_layout);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();
    }

    private void initView() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            post_id = extras.getString(S.postId);
            postPosition = extras.getString(S.position);
            Log.e("post", post_id);
        }
        mAuthAPI = new AuthAPI(context);
        mCommentBeansList = new ArrayList<>();

        commentRecyclerView = (RecyclerView) findViewById(R.id.comment_recyclerView);
        add_comment_et = (EditText) findViewById(R.id.add_comment_et);
        send_btn = (ImageButton) findViewById(R.id.send_btn);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);

        dashboardCommentAdapter = new AdCommentAdapter(mCommentBeansList, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        commentRecyclerView.setLayoutManager(layoutManager);
        commentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        commentRecyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        commentRecyclerView.setAdapter(dashboardCommentAdapter);
        dashboardCommentAdapter.setOnItemClickListener(this);
        commentRecyclerView.setNestedScrollingEnabled(false);

        clickListner();
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        send_btn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                onBack();
                break;
            case R.id.send_btn:
                comment = add_comment_et.getText().toString().trim();
                byte[] data = new byte[0];
                try {
                    data = comment.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment.length() > 0) {
                    add_comment_et.setText("");
                    mAuthAPI.adComment(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment, "", "0");
                }
                break;
        }
    }

    private void onBack()
    {
        Intent returnIntent = new Intent();

        returnIntent.putExtra("result", "");
        returnIntent.putExtra("total_likes", "");
        returnIntent.putExtra("isLike","");
        int list_size = mCommentBeansList.size();
        returnIntent.putExtra("total_comment", list_size + "");
        returnIntent.putExtra("postPosition", postPosition);

        setResult(Activity.RESULT_OK, returnIntent);
        hideKeyboard();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.comments));
        postCommentListing();
        FastLapApplication.mCurrentContext = context;
    }

    @Override
    public void facebookSharing(String title, String desc, String image) {

    }

    @Override
    public void gmailSharing(String title, String desc, String image) {

    }

    @Override
    public void twitterSharing(String title, String desc, String image) {

    }

    @Override
    public void onDeletePost(int pos) {
        mCommentBeansList.remove(pos);
        dashboardCommentAdapter.notifyDataSetChanged();
    }

    /*  postCommentListing webservice response
    *   here we show listing of comments
    */
    public void postCommentListingWebserviceResponse(String response) {
        mCommentBeansList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    CommentBeans commentBeans = new CommentBeans();
                   /* commentBeans.setComment_like_count(jsonObject1.getString(S.countLikeData));
                    commentBeans.setComment_reply_count(jsonObject1.getString(S.countReplyCommentData));
                    commentBeans.setComment_like_status(jsonObject1.getString(S.likedComment));*/

                    commentBeans.setComment_id(jsonObject1.getString(S._id));
                    commentBeans.setPost_id(jsonObject1.getString(S.advertisementId));
                    commentBeans.setComment_date(jsonObject1.getString(S.created));
                    commentBeans.setComment_text(jsonObject1.getString(S.comment));

                    commentBeans.setComment_user_id(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    commentBeans.setComment_user_name(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    commentBeans.setComment_user_image(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));

                    mCommentBeansList.add(commentBeans);
                }
                dashboardCommentAdapter.notifyDataSetChanged();

                if (mCommentBeansList.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    commentRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    commentRecyclerView.setVisibility(View.GONE);
                }

            } else {
                //Util.showAlertDialog(context, getString(R.string.alert), msg);
                mCommentBeansList.clear();
                dashboardCommentAdapter.notifyDataSetChanged();
                no_data_tv.setVisibility(View.VISIBLE);
                commentRecyclerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            mCommentBeansList.clear();
            dashboardCommentAdapter.notifyDataSetChanged();
            no_data_tv.setVisibility(View.VISIBLE);
            commentRecyclerView.setVisibility(View.GONE);

        }
    }

    /* postCommentRemove webservice response
     *  here we remove comment and refresh comment list
    */
    public void postCommentRemoveWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postCommentRemove_api);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /*  Calling postCommentListing webservice*/
    public void postCommentListing() {
        mAuthAPI.adCommentListing(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);
    }

    @Override
    public void onBackPressed() {

        onBack();
    }

    /* postCommentLike webservice response*/
    public void postCommentLikeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickCommentRowReply(int postion) {
        Intent i = new Intent(context, ReplyActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        startActivity(i);
    }

    @Override
    public void onClickReplyViewCount(int postion) {
        Intent i = new Intent(context, ReplyActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        startActivity(i);
    }

    @Override
    public void onClickCommentRowLikeCount(int postion) {
        Intent i = new Intent(context, LikesUserActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        i.putExtra(S.likeType, I.COMMENTLIKE);
        startActivity(i);
    }

    @Override
    public void onClickCommentRowLike(int postion, String status) {
        authAPI.postCommentLike(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), mCommentBeansList.get(postion).getComment_id(), status);
    }

    @Override
    public void onClickCommentDelete(int postion) {
        Util.showAlertDialogWithTwoButton(context, getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.cancel), getString(R.string.delete_small), postion);
        //
    }

    public void callDeleteComment(int postion) {
        authAPI.adCommentRemove(context, MySharedPreferences.getPreferences(context, S.user_id), mCommentBeansList.get(postion).getComment_id());
    }

    // Post Comment response
    public void postCommentResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickCommentEdit(final int postion) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dashboard_comment_edit);

        AppCompatImageView baseToggleIcon;
        ImageView commetRowUserIv;
        TextViewPlayBold usernameTv;
        TextViewPlayRegular commentTv;
        final EditTextPlayRegular etComment;
        TextViewPlayBold updateBtn;
        TextViewPlayBold cancelBtn;

        baseToggleIcon = (AppCompatImageView) dialog.findViewById(R.id.base_toggle_icon);
        commetRowUserIv = (ImageView) dialog.findViewById(R.id.commet_row_user_iv);
        usernameTv = (TextViewPlayBold) dialog.findViewById(R.id.username_tv);
        commentTv = (TextViewPlayRegular) dialog.findViewById(R.id.comment_tv);
        etComment = (EditTextPlayRegular) dialog.findViewById(R.id.etComment);
        updateBtn = (TextViewPlayBold) dialog.findViewById(R.id.update_btn);
        cancelBtn = (TextViewPlayBold) dialog.findViewById(R.id.cancel_btn);

        usernameTv.setText(MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image), commetRowUserIv, Util.getImageLoaderOption(context));

        byte[] data = Base64.decode(mCommentBeansList.get(postion).getComment_text(), Base64.NO_WRAP);
        try {
            String text = new String(data, "UTF-8");
            commentTv.setText(text);
            etComment.setText(text);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = etComment.getText().toString().trim();
                byte[] data = new byte[0];
                try {
                    data = comment.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment.length() > 0) {
                    etComment.setText("");
                    mAuthAPI.adComment(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment, mCommentBeansList.get(postion).getComment_id(), "1");
                    dialog.cancel();
                }
            }
        });

        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }
}
