package com.os.fastlap.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.dashboard.LikesUserActivity;
import com.os.fastlap.adapter.AlbumCommentAdapter;
import com.os.fastlap.adapter.GalleryPostsAdapter;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.beans.Bean_UserDetail;
import com.os.fastlap.beans.UserDataBeans;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.beans.dashboardmodals.CommentBeans;
import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by monikab on 1/8/2016.
 */
public class AlbumCommentActivity extends BaseActivity implements SharingDeleget, View.OnClickListener, AlbumCommentAdapter.CommentInterFace, GalleryPostsAdapter.ClickListner {

    ImageView base_toggle_icon;
    RecyclerView commentRecyclerView;
    AlbumCommentAdapter dashboardCommentAdapter;
    public EditText add_comment_et;
    ImageButton send_btn;
    String post_id = "";
    String comment = "";
    Context context;
    TextView no_data_tv;
    MyProgressDialog myProgressDialog;
    Bean_UserDetail user_data;
    private AuthAPI mAuthAPI;
    private List<CommentBeans> mCommentBeansList;
    private final String TAG = AlbumCommentActivity.class.getSimpleName();
    AlbumCommentAdapter.CommentInterFace commentInterFace;
    AuthAPI authAPI;
    String postPosition = "0";

    RecyclerView recycler_view;
    private GalleryPostsAdapter galleryPostsAdapter;
    public ArrayList<BeanAlbum> mMediaList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_layout_comment);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();
    }

    private void initView() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            post_id = extras.getString(S.postId);
            postPosition = extras.getString(S.position);
            Log.e("post", post_id);
        }

        mAuthAPI = new AuthAPI(context);
        mCommentBeansList = new ArrayList<>();
        mMediaList = new ArrayList<>();
        mMediaList.clear();

        commentRecyclerView = (RecyclerView) findViewById(R.id.comment_recyclerView);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        add_comment_et = (EditText) findViewById(R.id.add_comment_et);
        send_btn = (ImageButton) findViewById(R.id.send_btn);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);

        galleryPostsAdapter = new GalleryPostsAdapter(mMediaList, context, AlbumCommentActivity.this,1);
        RecyclerView.LayoutManager mClothLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(mClothLayoutManager);
        recycler_view.setAdapter(galleryPostsAdapter);
        galleryPostsAdapter.setOnclickListner(this);
        recycler_view.setNestedScrollingEnabled(false);

        dashboardCommentAdapter = new AlbumCommentAdapter(mCommentBeansList, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        commentRecyclerView.setLayoutManager(layoutManager);
        commentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        commentRecyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        commentRecyclerView.setAdapter(dashboardCommentAdapter);
        dashboardCommentAdapter.setOnItemClickListener(this);
        commentRecyclerView.setNestedScrollingEnabled(false);

        clickListner();
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        send_btn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:

                onBack();


                break;

            case R.id.send_btn:
                comment = add_comment_et.getText().toString().trim();
                byte[] data = new byte[0];
                try {
                    data = comment.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment.length() > 0) {
                    add_comment_et.setText("");
                    mAuthAPI.albumComment(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment, "", "0");
                }
                break;
        }
    }

    private void onBack() {
        Intent returnIntent = new Intent();

        returnIntent.putExtra("result", "");
        returnIntent.putExtra("total_likes", "");
        returnIntent.putExtra("isLike", "");
        int list_size = mCommentBeansList.size();
        returnIntent.putExtra("total_comment", list_size + "");
        returnIntent.putExtra("postPosition", postPosition);

        setResult(Activity.RESULT_OK, returnIntent);
        hideKeyboard();
        finish();
    }

    @Override
    public void openAlbum(int position) {
        Intent intent = new Intent(context, AlbumActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(S.data, mMediaList.get(position));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onClickOnLike(int position, String status) {

        authAPI.albumLikeByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mMediaList.get(position).get_id(), "", status);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.comments));
        postCommentListing();
        FastLapApplication.mCurrentContext = context;
    }

    @Override
    public void facebookSharing(String title, String desc, String image) {

    }

    @Override
    public void gmailSharing(String title, String desc, String image) {

    }

    @Override
    public void twitterSharing(String title, String desc, String image) {

    }

    @Override
    public void onDeletePost(int pos) {
        mCommentBeansList.remove(pos);
        dashboardCommentAdapter.notifyDataSetChanged();
    }

    /*  postCommentListing webservice response
    *   here we show listing of comments
    */
    public void postCommentListingWebserviceResponse(String response) {

        mCommentBeansList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    CommentBeans commentBeans = new CommentBeans();
                    commentBeans.setComment_like_count(jsonObject1.getString(S.countLikeData));
                    commentBeans.setComment_reply_count(jsonObject1.getString(S.countReplyCommentData));
                    commentBeans.setComment_reply_count("0");
                    commentBeans.setComment_like_status(jsonObject1.getString(S.likedComment));
                    // commentBeans.setComment_like_status("0");

                    commentBeans.setComment_id(jsonObject1.getString(S._id));
                    commentBeans.setPost_id(jsonObject1.getString(S.albumId));
                    commentBeans.setComment_date(jsonObject1.getString(S.created));
                    commentBeans.setComment_text(jsonObject1.getString(S.comment));

                    commentBeans.setComment_user_id(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    commentBeans.setComment_user_name(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    commentBeans.setComment_user_image(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));

                    mCommentBeansList.add(commentBeans);
                }
                dashboardCommentAdapter.notifyDataSetChanged();

                if (mCommentBeansList.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    commentRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    commentRecyclerView.setVisibility(View.GONE);
                }

            } else {
                //Util.showAlertDialog(context, getString(R.string.alert), msg);
                mCommentBeansList.clear();
                dashboardCommentAdapter.notifyDataSetChanged();
                no_data_tv.setVisibility(View.VISIBLE);
                commentRecyclerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            mCommentBeansList.clear();
            dashboardCommentAdapter.notifyDataSetChanged();
            no_data_tv.setVisibility(View.VISIBLE);
            commentRecyclerView.setVisibility(View.GONE);

        }
    }

    /* postCommentRemove webservice response
     *  here we remove comment and refresh comment list
    */
    public void postCommentRemoveWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postCommentRemove_api);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /*  Calling postCommentListing webservice*/
    public void postCommentListing() {
        mAuthAPI.albumCommentListing(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);
        mAuthAPI.albumInfo(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);

    }

    @Override
    public void onBackPressed() {

        onBack();
    }

    /* postCommentLike webservice response*/
    public void postCommentLikeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickCommentRowReply(int postion) {
        Intent i = new Intent(context, ReplyAlbumActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        startActivity(i);
    }

    @Override
    public void onClickReplyViewCount(int postion) {
        Intent i = new Intent(context, ReplyAlbumActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        startActivity(i);
    }

    @Override
    public void onClickCommentRowLikeCount(int postion) {
        Intent i = new Intent(context, LikesUserActivity.class);
        i.putExtra(S.postId, post_id);
        i.putExtra(S.userPostCommentId, mCommentBeansList.get(postion).getComment_id());
        i.putExtra(S.likeType, I.ALBUMCOMMENTLIKE);
        startActivity(i);
    }

    @Override
    public void onClickCommentRowLike(int postion, String status) {
        authAPI.albumCommentLike(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), mCommentBeansList.get(postion).getComment_id(), status);
    }

    @Override
    public void onClickCommentDelete(int postion) {
        Util.showAlertDialogWithTwoButton(context, getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.cancel), getString(R.string.delete_small), postion);
        //
    }

    public void callDeleteComment(int postion) {
        authAPI.albumCommentRemove(context, MySharedPreferences.getPreferences(context, S.user_id), mCommentBeansList.get(postion).getComment_id());
    }

    // Post Comment response
    public void postCommentResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickCommentEdit(final int postion) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dashboard_comment_edit);

        AppCompatImageView baseToggleIcon;
        ImageView commetRowUserIv;
        TextViewPlayBold usernameTv;
        TextViewPlayRegular commentTv;
        final EditTextPlayRegular etComment;
        TextViewPlayBold updateBtn;
        TextViewPlayBold cancelBtn;

        baseToggleIcon = (AppCompatImageView) dialog.findViewById(R.id.base_toggle_icon);
        commetRowUserIv = (ImageView) dialog.findViewById(R.id.commet_row_user_iv);
        usernameTv = (TextViewPlayBold) dialog.findViewById(R.id.username_tv);
        commentTv = (TextViewPlayRegular) dialog.findViewById(R.id.comment_tv);
        etComment = (EditTextPlayRegular) dialog.findViewById(R.id.etComment);
        updateBtn = (TextViewPlayBold) dialog.findViewById(R.id.update_btn);
        cancelBtn = (TextViewPlayBold) dialog.findViewById(R.id.cancel_btn);

        usernameTv.setText(MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image), commetRowUserIv, Util.getImageLoaderOption(context));

        byte[] data = Base64.decode(mCommentBeansList.get(postion).getComment_text(), Base64.NO_WRAP);
        try {
            String text = new String(data, "UTF-8");
            commentTv.setText(text);
            etComment.setText(text);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = etComment.getText().toString().trim();
                byte[] data = new byte[0];
                try {
                    data = comment.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment.length() > 0) {
                    etComment.setText("");
                    mAuthAPI.albumComment(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment, mCommentBeansList.get(postion).getComment_id(), "1");
                    dialog.cancel();
                }
            }
        });

        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }


    public void getGalleryPhotoWebserviceResponse(String response) {
        try {

            mMediaList.clear();
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataJson.length(); i++) {
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);
                    BeanAlbum beanAlbum = new BeanAlbum();
                    beanAlbum.set_id(jsonObject1.getString(S._id));
                    beanAlbum.setName(jsonObject1.getString(S.name));
                    beanAlbum.setDateOfEvent(jsonObject1.getString(S.dateOfEvent));
                    beanAlbum.setCreated(jsonObject1.getString(S.created));
                    beanAlbum.setDescription(jsonObject1.getString(S.description));
                    beanAlbum.setMediaType(1);
                    beanAlbum.setAlbumPrice(jsonObject1.getString(S.albumPrice));
                    JSONArray typejsonArray = jsonObject1.getJSONArray(S.groupTypeId);

                    if (typejsonArray.length() > 0) {
                        beanAlbum.setGroupTypeId(typejsonArray.getJSONObject(0).getString(S._id));
                        beanAlbum.setGroupTypeName(typejsonArray.getJSONObject(0).getString(S.name));
                    }


                    beanAlbum.setLikedPost(jsonObject1.getString(S.likedAlbum));
                    beanAlbum.setCommentPostCount(jsonObject1.getString(S.commentAlbumCount));
                    beanAlbum.setLikedPostCount(jsonObject1.getString(S.likedAlbumCount));

                    beanAlbum.setUserId(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    beanAlbum.setUserName(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    beanAlbum.setUserImage(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));
                    if (jsonObject1.getJSONArray(S.trackId).length() > 0) {
                        beanAlbum.setTrackId(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S._id));
                        beanAlbum.setTrackName(jsonObject1.getJSONArray(S.trackId).getJSONObject(0).getString(S.name));
                    }

                    if (jsonObject1.getJSONArray(S.eventId).length() > 0) {
                        beanAlbum.setEventId(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S._id));
                        beanAlbum.setEventName(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S.name));
                    }
                    JSONArray shareAlbumUser = jsonObject1.getJSONArray(S.albumShareUser);
                    List<DashboardPostShareUserModal> albumSharePost = new ArrayList<>();
                    for (int j = 0; j < shareAlbumUser.length(); j++) {
                        JSONObject shareAlbumUserJson = shareAlbumUser.getJSONObject(j);
                        DashboardPostShareUserModal dashboardPostShareUserModal = new DashboardPostShareUserModal();
                        dashboardPostShareUserModal.setId(shareAlbumUserJson.getString(S._id));
                        dashboardPostShareUserModal.setPostId(shareAlbumUserJson.getString(S.albumId));
                        dashboardPostShareUserModal.setModified(shareAlbumUserJson.getString(S.modified));
                        dashboardPostShareUserModal.setCreated(shareAlbumUserJson.getString(S.created));
                        dashboardPostShareUserModal.setStatus(shareAlbumUserJson.getString(S.status));

                        JSONObject userJson = shareAlbumUserJson.getJSONObject(S.userId);
                        UserDataBeans userDataBeans = new UserDataBeans();
                        userDataBeans.set_id(userJson.getString(S._id));
                        userDataBeans.setEmail(userJson.getString(S.email));
                        userDataBeans.setUsername(userJson.getString(S.username));

                        JSONObject personalInfoJson = userJson.getJSONObject(S.personalInfo);
                        PersonalInfo personalInfo = new PersonalInfo();
                        personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                        personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                        personalInfo.setImage(personalInfoJson.getString(S.image));
                        personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                        personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                        personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                        personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                        personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                        userDataBeans.setPersonalInfo(personalInfo);

                        dashboardPostShareUserModal.setUserDataBeans(userDataBeans);
                        albumSharePost.add(dashboardPostShareUserModal);
                    }
                    beanAlbum.setAlbumPostShareUserModals(albumSharePost);

                    JSONArray friendArray = new JSONArray();
                    friendArray = jsonObject1.getJSONArray(S.albumFriend);
                    ArrayList<BeanAlbum.BeanFriend> friendList = new ArrayList<>();
                    for (int j = 0; j < friendArray.length(); j++) {
                        BeanAlbum.BeanFriend beanFriend = new BeanAlbum().new BeanFriend();
                        beanFriend.setUserFriendId(friendArray.getJSONObject(j).getString(S._id));
                        beanFriend.setFriendId(friendArray.getJSONObject(j).getJSONObject(S.friendId).getString(S._id));
                        beanFriend.setFriendName(friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                        beanFriend.setFriendImage(friendArray.getJSONObject(j).getJSONObject(S.friendId).getJSONObject(S.personalInfo).getString(S.image));
                        friendList.add(beanFriend);
                    }
                    beanAlbum.setFriendList(friendList);

                    JSONArray filesArray = new JSONArray();
                    filesArray = jsonObject1.getJSONArray(S.filesAll);
                    ArrayList<BeanAlbum.BeanFiles> filesList = new ArrayList<>();
                    for (int j = 0; j < filesArray.length(); j++) {
                        BeanAlbum.BeanFiles beanFiles = new BeanAlbum().new BeanFiles();
                        beanFiles.setFileId(filesArray.getJSONObject(j).getString(S._id));
                        beanFiles.setCreated(filesArray.getJSONObject(j).getString(S.created));
                        beanFiles.setFileName(filesArray.getJSONObject(j).getString(S.fileName));
                        beanFiles.setThumbName(filesArray.getJSONObject(j).getString(S.thumbName));
                        beanFiles.setType(filesArray.getJSONObject(j).getString(S.type));
                        filesList.add(beanFiles);
                    }

                    if (jsonObject1.getJSONArray(S.eventId).length() > 0) {
                        beanAlbum.setEventId(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S._id));
                        beanAlbum.setEventName(jsonObject1.getJSONArray(S.eventId).getJSONObject(0).getString(S.name));
                    }

                    if (filesArray.length() != 0) {
                        beanAlbum.setFilesList(filesList);
                        mMediaList.add(beanAlbum);
                    }


                }
                galleryPostsAdapter.notifyDataSetChanged();

            } /*else
                    Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

}
