package com.os.fastlap.activity.mylaps;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.activity.SearchActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.adapter.community.CommunityVechiceListAdapter;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.beans.BeanVehicle;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.fragment.paddock.PaddockMyLapsSubFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import static com.os.fastlap.R.id.viewpager;


/**
 * Created by anandj on 7/21/2017.
 */

public class MyLapsActivity extends BaseActivity implements View.OnClickListener, SpinnerSelectorInterface {

    private TabLayout mTabHost;
    private ViewPager mViewpager;
    Context context;
    ImageView base_toggle_icon;
    private ViewPagerAdapter viewPagerAdapter;
    private RecyclerView mVechiceList;
    private CommunityVechiceListAdapter communityVechiceListAdapter;
    private ArrayList<BeanVehicle> mArrayVechineList;
    private String TAG = MyLapsActivity.class.getSimpleName();
    public static ArrayList<PaddockMyLapsParentModel> myLapsList;
    public static ArrayList<PaddockMyLapsParentModel> myBsetList;
    public static ArrayList<PaddockMyLapsParentModel> myFriendList;
    AuthAPI authAPI;
    public static ArrayList<BeanVehicleType> countryName;
    SpinnerSelectorInterface spinnerSelectorInterface;
    EditText searchEt;
    String  trackVersionId="0";
    TextView menu_menu;
    public static ArrayList selectedItem=new ArrayList<>();
    JSONArray lapArray=new JSONArray();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mylaps_activity_main);
        context = this;
        spinnerSelectorInterface = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();
        Util.changeTabsFont(mTabHost, context);
        try {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty("userId",MySharedPreferences.getPreferences(context, S.user_id));
            Log.e("log","JSONObject"+jsonObject);
            authAPI.getMyLapsListingFriends(context, jsonObject);
        }
        catch (Exception e){
            Log.e("log","Exception"+e);
        }
        authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));

    }
    private void initView() {
        mTabHost = (TabLayout) findViewById(R.id.tab_host);
        mViewpager = (ViewPager) findViewById(viewpager);
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        searchEt = findViewById(R.id.searchEt);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        mVechiceList = (RecyclerView) findViewById(R.id.vechice_list);
        mArrayVechineList = new ArrayList<>();
        mArrayVechineList.clear();
        myLapsList = new ArrayList<>();
        myBsetList = new ArrayList<>();
        myFriendList = new ArrayList<>();
        myLapsList.clear();
        myBsetList.clear();
        selectedItem.clear();
        myFriendList.clear();
        countryName = new ArrayList<>();
        countryName.clear();

        communityVechiceListAdapter = new CommunityVechiceListAdapter(context, mArrayVechineList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mVechiceList.setLayoutManager(layoutManager);
        mVechiceList.setAdapter(communityVechiceListAdapter);

        setupViewPager(mViewpager);
        mTabHost.setupWithViewPager(mViewpager);
        mViewpager.setNestedScrollingEnabled(false);
        clickListner();
        searchEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(S.type, I.MYLAPSSEARCH);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });


        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    ((MyLapsActivity)context).showVehicleTypeDialog(context.getResources().getString(R.string.select_country),I.COUNTRYNAME);
                }else {
                    getCurrentFragment();
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    public Fragment getCurrentFragment()
    {
        Fragment mCurrentFragment=null;
        if(viewPagerAdapter!=null && mViewpager!=null)
        {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            ((PaddockMyLapsSubFragment)mCurrentFragment).getListData();
        }
        return mCurrentFragment;
    }

    public Fragment getCurrentFragment(String ch)
    {
        Fragment mCurrentFragment=null;
        if(viewPagerAdapter!=null && mViewpager!=null)
        {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            ((PaddockMyLapsSubFragment)mCurrentFragment).filter(ch);
        }

        return mCurrentFragment;
    }

    public Fragment getCurrentFragmentVehicle(String id) {
        Fragment mCurrentFragment=null;
        if(viewPagerAdapter!=null && mViewpager!=null) {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            ((PaddockMyLapsSubFragment)mCurrentFragment).filterVehicle(id);
        }
        return mCurrentFragment;
    }
    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        menu_menu.setOnClickListener(this);
    }

    private void setupViewPager(ViewPager viewpager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new PaddockMyLapsSubFragment(I.FAVORITETRACKS,trackVersionId), getString(R.string.favorites));
        viewPagerAdapter.addFragment(new PaddockMyLapsSubFragment(I.ALLTRACKS,trackVersionId), getString(R.string.all));
        viewPagerAdapter.addFragment(new PaddockMyLapsSubFragment(I.COUNTRYTRACKS,trackVersionId), getString(R.string.country));
        viewpager.setAdapter(viewPagerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        FastLapApplication.mCurrentContext = context;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.menu_menu:
                Intent intent = new Intent(this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }
    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                mArrayVechineList.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicle beanVehicle = new BeanVehicle();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicle.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanVehicle.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");
                    beanVehicle.setImage(jsonObject1.has(S.image)?jsonObject1.getString(S.image):"");
                    mArrayVechineList.add(beanVehicle);
                }
                communityVechiceListAdapter.notifyDataSetChanged();

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    public void getMyLapsWebserviceResponse(String response) {
        try {
            ArrayList<String> countryList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                myLapsList.clear();
                myBsetList.clear();
                countryName.clear();

                JSONArray dataArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataArray.length(); i++)
                {
                    JSONObject jsonObject1 = dataArray.getJSONObject(i);
                    PaddockMyLapsParentModel paddockMyLapsParentModel = new PaddockMyLapsParentModel();
                    paddockMyLapsParentModel.setMyLapCount(jsonObject1.has(S.myLapCount)?jsonObject1.getString(S.myLapCount):"");
                    paddockMyLapsParentModel.setBestPosition(jsonObject1.has(S.bestPosition)?jsonObject1.getString(S.bestPosition):"");
                    trackVersionId=jsonObject1.has(S._id)?jsonObject1.getString(S._id):"";
                    paddockMyLapsParentModel.setUserLapId(trackVersionId);
                    JSONObject indexObject = new JSONObject();
                    JSONObject trackObject=new JSONObject();
                    JSONArray tempArray=jsonObject1.has(S.trackVersionData)?jsonObject1.getJSONArray(S.trackVersionData):null;
                    if(tempArray!=null&&tempArray.length()>0)
                    {
                        indexObject = tempArray.getJSONObject(0);

                        paddockMyLapsParentModel.setIsFav(indexObject.has(S.isFav)?indexObject.getString(S.isFav):"");
                        paddockMyLapsParentModel.setIsHome(indexObject.has(S.isHome)?indexObject.getString(S.isHome):"");
                        paddockMyLapsParentModel.setTrackVersionName(indexObject.getString(S.name));
                        paddockMyLapsParentModel.setTrackVersionId(indexObject.getString(S._id));
                        //JSONArray rxArray = new JSONArray();
                        try {
                            JSONArray  rxArray = indexObject.has(S.right_top)?indexObject.getJSONArray(S.right_top):null;
                            if (rxArray.length() > 0&&rxArray!=null) {
                                paddockMyLapsParentModel.setTrackRxLng(rxArray.getString(0));
                                paddockMyLapsParentModel.setTrackRxLat(rxArray.getString(1));
                            }
                            //JSONArray lxArray = new JSONArray();
                            JSONArray lxArray=indexObject.has(S.left_bottom)?indexObject.getJSONArray(S.left_bottom):null;
                            if (lxArray.length() > 0&&lxArray!=null) {
                                paddockMyLapsParentModel.setTrackLxLng(lxArray.getString(0));
                                paddockMyLapsParentModel.setTrackLxLat(lxArray.getString(1));
                            }
                            paddockMyLapsParentModel.setOverlay_image(indexObject.has(S.overlay_image)?indexObject.getString(S.overlay_image):"");
                            if (indexObject.has(S.sector)) {
                                if (indexObject.getJSONObject(S.sector).has(S.noSector)) {
                                    if (indexObject.getJSONObject(S.sector).getJSONObject(S.noSector).has(S.image)) {
                                        paddockMyLapsParentModel.setTrackRouteImage(indexObject.getJSONObject(S.sector).getJSONObject(S.noSector).getString(S.image));
                                    }
                                }
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                        trackObject=indexObject.has(S.trackId)?indexObject.getJSONObject(S.trackId):null;
                        if(trackObject!=null) {
                            paddockMyLapsParentModel.setTrack_id(trackObject.has(S._id)?trackObject.getString(S._id):"");
                            paddockMyLapsParentModel.setTrackName(trackObject.has(S.name)?trackObject.getString(S.name):"");
                            paddockMyLapsParentModel.setTrackImage(trackObject.has(S.image)?trackObject.getString(S.image):"");
                            paddockMyLapsParentModel.setTracklocation(trackObject.has(S.location_response)?trackObject.getString(S.location_response):"");
                            paddockMyLapsParentModel.setTrackLength(trackObject.has(S.trackLength)?trackObject.getString(S.trackLength):"");
                            paddockMyLapsParentModel.setTotalTurns(trackObject.getJSONObject(S.turns).has(S.totalTurns)? trackObject.getJSONObject(S.turns).getString(S.totalTurns):"");
                            paddockMyLapsParentModel.setTrackurl(trackObject.getJSONObject(S.contactDetails).has(S.url)?trackObject.getJSONObject(S.contactDetails).getString(S.url):"");
                            paddockMyLapsParentModel.setTrackMobile(trackObject.getJSONObject(S.contactDetails).has(S.mobileNumber_response)?trackObject.getJSONObject(S.contactDetails).getString(S.mobileNumber_response):"");
                            paddockMyLapsParentModel.setTrackEmail(trackObject.getJSONObject(S.contactDetails).has(S.email)?trackObject.getJSONObject(S.contactDetails).getString(S.email):"");

                            paddockMyLapsParentModel.setTrackCountry(trackObject.has(S.country)?trackObject.getString(S.country):"");


                        }

                    }
                    JSONArray userArray = jsonObject1.getJSONArray(S.lapData);
                    ArrayList<BeanTimeLap> userList = new ArrayList<>();
                    for (int j = 0; j < userArray.length(); j++)
                    {
                        try {
                            JSONObject lapObject = userArray.getJSONObject(j);
                            JSONObject userJsonObject = lapObject.has(S.userId)?lapObject.getJSONObject(S.userId):null;
                            if(userJsonObject!=null&&userJsonObject.length()>0)
                            {
                                BeanTimeLap beanTimeLap = new BeanTimeLap();
                                beanTimeLap.setUserId(userJsonObject.has(S._id)?userJsonObject.getString(S._id):"");
                                if((userJsonObject.getString(S._id).equalsIgnoreCase(MySharedPreferences.getPreferences(context, S.user_id))&&lapObject.getString("status").equalsIgnoreCase("1"))) {
                                    beanTimeLap.setUserImage(userJsonObject.getJSONObject(S.personalInfo).has(S.image)?userJsonObject.getJSONObject(S.personalInfo).getString(S.image):"");
                                    beanTimeLap.setUserName(userJsonObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userJsonObject.getJSONObject(S.personalInfo).getString(S.lastName_response));
                                    beanTimeLap.setTimeLap_Id(lapObject.has(S._id)?lapObject.getString(S._id):"");
                                    beanTimeLap.setLapId(lapObject.has(S._id)?lapObject.getString(S._id):"");
                                    beanTimeLap.setTrackVersionId(lapObject.has(S.trackVersionId)?lapObject.getString(S.trackVersionId):"");
                                    beanTimeLap.setGroupTypeId(lapObject.getJSONObject(S.lapTimeId).has(S.groupTypeId)?lapObject.getJSONObject(S.lapTimeId).getString(S.groupTypeId):"");
                                    beanTimeLap.setLapDate(lapObject.getJSONObject(S.lapTimeId).getString(S.dateOfRecording));
                                    beanTimeLap.setVehicleModelName(lapObject.getJSONObject(S.lapTimeId).getJSONObject(S.userVehicleId).getJSONObject(S.vehicleModelId).getString(S.vehicleModelName));
                                    beanTimeLap.setVehicleBrandName(lapObject.getJSONObject(S.lapTimeId).getJSONObject(S.userVehicleId).getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName));
                                    beanTimeLap.setTime(lapObject.has(S.time)?lapObject.getString(S.time):"");
                                    beanTimeLap.setFileUrl(lapObject.has(S.fileUrl)?lapObject.getString(S.fileUrl):"");
                                    beanTimeLap.setMaxSpeed(lapObject.has(S.maxSpeed)?lapObject.getString(S.maxSpeed):"");
                                    beanTimeLap.setAvgSpeed(lapObject.has(S.averageSpeed)?lapObject.getString(S.averageSpeed):"");
                                    beanTimeLap.setAverageSpeedToShow(lapObject.has(S.averageSpeedToShow)?lapObject.getString(S.averageSpeedToShow):"");


                                    beanTimeLap.setRank((j + 1) + "");
                                    beanTimeLap.setBestLapTime(lapObject.has(S.time)?lapObject.getString(S.time):"");
                                    Random rnd = new Random();
                                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                                    beanTimeLap.setLapColor(color);
                                    userList.add(beanTimeLap);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                    paddockMyLapsParentModel.setMyLapsChildList(userList);
                    myLapsList.add(paddockMyLapsParentModel);
                    myBsetList.add(paddockMyLapsParentModel);
                    if (trackObject!=null && !countryList.contains(trackObject.getString(S.country))) {
                        countryList.add(indexObject.getJSONObject(S.trackId).getString(S.country));
                        BeanVehicleType beanVehicleType = new BeanVehicleType();
                        beanVehicleType.setId(indexObject.getJSONObject(S.trackId).getString(S.country));
                        beanVehicleType.setName(indexObject.getJSONObject(S.trackId).getString(S.country));
                        countryName.add(beanVehicleType);
                    }
                }
                getCurrentFragment();
            }

            getCurrentFragment();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void showVehicleTypeDialog(String dialog_title, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, countryName, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }
    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.COUNTRYNAME:
                getCurrentFragment(value);
                break;
        }
    }
    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {
    }
    public void getVehicleTypeClick(int pos, String select_id) {
        String id = mArrayVechineList.get(pos).getId();
        getCurrentFragmentVehicle(id);
    }

}
