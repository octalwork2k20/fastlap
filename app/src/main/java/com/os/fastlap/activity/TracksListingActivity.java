package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.adapter.dashboard.DashboardTrackListAdapter;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.ContactDetails;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.DashboardTrackListingAll;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.Position;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.TrackData;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.Turns;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.UserTrackData;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.os.fastlap.util.constants.S.userTrackData;

/*
 * Created by abhinava on 8/23/2017.
 */

public class TracksListingActivity extends BaseActivity implements View.OnClickListener, DashboardTrackListAdapter.OnItemClickListener {

    private Context mContext;
    private AuthAPI mAuthAPI;
    private RelativeLayout actionbar;
    private AppCompatImageView baseToggleIcon;
    private View view;
    private RecyclerView trackRecyclerView;
    private TextView no_data_tv;
    private DashboardTrackListAdapter mDashboardTrackListAdapter;
    private ArrayList<DashboardTrackListingAll> mDashboardTrackListingAllArrayList;
    private final String TAG = TracksListingActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_tracklisting_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        mAuthAPI = new AuthAPI(mContext);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.tracks));
        FastLapApplication.mCurrentContext = mContext;
        mAuthAPI.trackListingAll(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
    }

    private void initView() {
        actionbar = (RelativeLayout) findViewById(R.id.actionbar);
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        view = findViewById(R.id.view);
        mDashboardTrackListingAllArrayList = new ArrayList<>();
        mDashboardTrackListAdapter = new DashboardTrackListAdapter(mContext, mDashboardTrackListingAllArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        trackRecyclerView = (RecyclerView) findViewById(R.id.track_recyclerView);
        trackRecyclerView.setLayoutManager(layoutManager);
        trackRecyclerView.setAdapter(mDashboardTrackListAdapter);
        mDashboardTrackListAdapter.setOnItemClickListener(this);
        clickListner();
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }

    /*Track Listing webservice response */
    public void trackListingAllWebserviceResponse(String response) {
        mDashboardTrackListingAllArrayList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataJson.length(); i++) {
                    try {
                        JSONObject jsonObject1 = dataJson.getJSONObject(i);
                        DashboardTrackListingAll dashboardTrackListingAll = new DashboardTrackListingAll();

                        JSONObject trackDataJSON;
                        trackDataJSON = jsonObject1.getJSONObject(S.trackData);
                        TrackData trackData = new TrackData();
                        trackData.setId(trackDataJSON.getString(S._id));
                        //trackData.setOwnerId(trackDataJSON.getString(S.ownerId));
                        trackData.setName(trackDataJSON.getString(S.name));
                        trackData.setCreated(trackDataJSON.getString(S.created));
                        trackData.setStatus(trackDataJSON.getInt(S.status));
                        trackData.setAboutTrack(trackDataJSON.getString(S.aboutTrack));
                       // trackData.setTotalLapRecords(trackDataJSON.getInt(S.totalLapRecords));
                        trackData.setTrackOpenDate("");
                        trackData.setTrackLength(trackDataJSON.getInt(S.trackLength));
                        trackData.setLocation(trackDataJSON.getString(S.location_response));
                        trackData.setCoverImage(trackDataJSON.getString(S.coverImage));
                        trackData.setImage(trackDataJSON.getString(S.image));

                        Position position = new Position();
                        if (trackDataJSON.getJSONArray(S.position).length() != 0) {
                            position.setLat(trackDataJSON.getJSONArray(S.position).getString(0));
                            position.setLng(trackDataJSON.getJSONArray(S.position).getString(1));
                        }

                        trackData.setPosition(position);

                        ContactDetails contactDetails = new ContactDetails();
                        contactDetails.setEmail(trackDataJSON.getJSONObject(S.contactDetails).getString(S.email));
                        contactDetails.setUrl(trackDataJSON.getJSONObject(S.contactDetails).getString(S.url));
                        contactDetails.setMobileNumber(trackDataJSON.getJSONObject(S.contactDetails).getString(S.mobileNumber_response));
                        trackData.setContactDetails(contactDetails);

                      /*  Straights straights = new Straights();
                        straights.setLengthOfStraights(trackDataJSON.getJSONObject(S.straights).getInt(S.lengthOfStraights));
                        straights.setTotalStraights(trackDataJSON.getJSONObject(S.straights).getInt(S.totalStraights));
                        trackData.setStraights(straights);*/

                        Turns turns = new Turns();
                        turns.setSlowLeftTurns(trackDataJSON.getJSONObject(S.turns).getInt(S.slowLeftTurns));
                        turns.setSlowRightTurns(trackDataJSON.getJSONObject(S.turns).getInt(S.slowRightTurns));
                        turns.setRightTurns(trackDataJSON.getJSONObject(S.turns).getInt(S.rightTurns));
                        turns.setLeftTurns(trackDataJSON.getJSONObject(S.turns).getInt(S.leftTurns));
                        turns.setTotalTurns(trackDataJSON.getJSONObject(S.turns).getInt(S.totalTurns));
                        trackData.setTurns(turns);

                        dashboardTrackListingAll.setTrackData(trackData);

                        JSONObject userTrackJSON = new JSONObject();
                        userTrackJSON = jsonObject1.getJSONObject(userTrackData);
                        UserTrackData userTrackData = new UserTrackData();
                        if (userTrackJSON.has(S._id)) {
                            userTrackData.setId(userTrackJSON.getString(S._id));
                            userTrackData.setTrackId(userTrackJSON.getString(S.TrackId));
                            userTrackData.setModified(userTrackJSON.getString(S.modified));
                            userTrackData.setCreated(userTrackJSON.getString(S.created));
                            userTrackData.setStatus(userTrackJSON.getInt(S.status));
                        } else {
                            userTrackData = null;
                        }

                        dashboardTrackListingAll.setUserTrackData(userTrackData);

                        mDashboardTrackListingAllArrayList.add(dashboardTrackListingAll);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                mDashboardTrackListAdapter.notifyDataSetChanged();

                if (mDashboardTrackListingAllArrayList.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    trackRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    trackRecyclerView.setVisibility(View.GONE);
                }

            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                trackRecyclerView.setVisibility(View.GONE);
                //Util.showAlertDialog(mContext, getString(R.string.alert), msg);
            }
        } catch (Exception e) {
            no_data_tv.setVisibility(View.VISIBLE);
            trackRecyclerView.setVisibility(View.GONE);
            Log.e(TAG, e.toString());
        }
    }

    // unfavorite track webservice response
    public void trackManageStatus(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                mAuthAPI.trackListingAll(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onItemVehicleList(int position) {
        mDashboardTrackListingAllArrayList.get(position).getTrackData().getId();

        Intent intent = new Intent();
        intent.putExtra(S.trackname, mDashboardTrackListingAllArrayList.get(position).getTrackData().getName());
        intent.putExtra(S.trackId, mDashboardTrackListingAllArrayList.get(position).getTrackData().getId());
        setResult(RESULT_OK, intent);
        finish();
    }
}
