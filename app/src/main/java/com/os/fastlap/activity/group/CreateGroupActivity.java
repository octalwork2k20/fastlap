package com.os.fastlap.activity.group;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.TracksListingActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.settings.SettingBlockUserSuggestionListAdapter;
import com.os.fastlap.beans.BeanAddGroup;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by abhinava on 9/7/2017.
 */

public class CreateGroupActivity extends BaseActivity implements View.OnClickListener, SpinnerSelectorInterface, OnImagePickerDialogSelect {
    private AppCompatImageView baseToggleIcon;
    Context context;
    AuthAPI authAPI;
    Dialog step1Dialog;
    Dialog step2Dialog;
    Dialog step3Dialog;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    private SettingBlockUserSuggestionListAdapter spinDialogAdapter;
    private String TAG = CreateGroupActivity.class.getSimpleName();
    BeanAddGroup beanAddGroup;
    ArrayList<BeanVehicleType> privacy_list;
    private ArrayList<BeanVehicleType> groupTypes_list;
    private ArrayList<BeanVehicleType> vehicle_list;
    SpinnerSelectorInterface spinnerSelectorInterface;
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    private String vehicleId = "";
    private String groupTypeID = "";
    private String trackId = "";

    TextViewPlayRegular groupPrivacyTv;
    TextViewPlayRegular groupTypeTv;
    TextViewPlayRegular vehicleTypeTv;
    TextViewPlayRegular trackTypeTv;
    ImageView groupProfileIv;
    ImageView groupCoverIv;

    private static final int TRACK_ACTIVITY_RESULT_CODE = 1;
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    public PagerBean profilePagerBean;
    public PagerBean coverPagerBean;
    String privacy;
    ImageView post_image;
    TextView group_name_tv;

    int image_type;
    boolean isShowGroupDetails=false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_create_layout);
        context = this;
        FastLapApplication.mCurrentContext = context;
        isShowGroupDetails=getIntent().getBooleanExtra(S.showGroupDetails,false);
        authAPI = new AuthAPI(context);
        spinnerSelectorInterface = this;
        onImagePickerDialogSelect = this;
        initView();
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mMyFriendsLists = new ArrayList<>();
        mMyFriendsLists.clear();
        beanAddGroup = new BeanAddGroup();
        if(isShowGroupDetails)
            beanAddGroup.setShowAllData(true);
        else
            beanAddGroup.setShowAllData(false);
        privacy_list = new ArrayList<>();
        privacy_list.clear();

        groupTypes_list = new ArrayList<>();
        groupTypes_list.clear();
        vehicle_list = new ArrayList<>();
        vehicle_list.clear();

        privacy_list.add(new BeanVehicleType("1", getString(R.string.public_text)));
        privacy_list.add(new BeanVehicleType("2", getString(R.string.private_text)));
        privacy_list.add(new BeanVehicleType("3", getString(R.string.friends)));


        if(isShowGroupDetails){
            step1Dialog();
            step2Dialog();
            step3Dialog();
            step1Dialog.show();
        }
        else {
            stepHideDetailsDialog();
            step3Dialog();
            step2Dialog.show();
        }
        authAPI.myFriendList(context, MySharedPreferences.getPreferences(context, S.user_id), "");

    }


    private void step1Dialog() {
        step1Dialog = new Dialog(context);
        step1Dialog.setContentView(R.layout.group_step1_dialog);
        step1Dialog.setCancelable(false);
        step1Dialog.setCanceledOnTouchOutside(false);
        final EditTextPlayRegular groupNameEt;

        TextViewPlayRegular detailTv;
        TextViewPlayBold backBtn;
        final TextViewPlayBold nextBtn;

        groupNameEt = (EditTextPlayRegular) step1Dialog.findViewById(R.id.group_name_et);
        groupPrivacyTv = (TextViewPlayRegular) step1Dialog.findViewById(R.id.group_privacy_tv);
        groupTypeTv = (TextViewPlayRegular) step1Dialog.findViewById(R.id.group_type_tv);
        vehicleTypeTv = (TextViewPlayRegular) step1Dialog.findViewById(R.id.vehicle_type_tv);
        trackTypeTv = (TextViewPlayRegular) step1Dialog.findViewById(R.id.track_type_tv);
        detailTv = (TextViewPlayRegular) step1Dialog.findViewById(R.id.detail_tv);
        backBtn = (TextViewPlayBold) step1Dialog.findViewById(R.id.back_btn);
        nextBtn = (TextViewPlayBold) step1Dialog.findViewById(R.id.next_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step1Dialog.cancel();
                finish();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Util.hideKeyboard(getCurrentFocus(), context);
                try {

                    String groupname = groupNameEt.getText().toString().trim();

                    if (Validation.addGroupStep1Validation(groupname, groupPrivacyTv.getText().toString().trim(), groupTypeID, vehicleId, trackId, context, nextBtn)) {
                        beanAddGroup.setGroupName(groupname);
                        beanAddGroup.setVehicleTypeId(groupTypeID);
                        beanAddGroup.setVehicleBrandId(vehicleId);
                        beanAddGroup.setTrackId(trackId);
                        beanAddGroup.setPrivacy(privacy);

                        step1Dialog.cancel();
                        step2Dialog.show();
                        group_name_tv.setText(beanAddGroup.getGroupName());
                    }

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }

            }
        });

        groupPrivacyTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showVehicleTypeDialog(context.getResources().getString(R.string.select_privacy), privacy_list, I.PRIVACY);
            }
        });

        groupTypeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
            }
        });

        vehicleTypeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authAPI.getVehicleList(context, MySharedPreferences.getPreferences(context, S.user_id));
            }
        });

        trackTypeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent trackIntent = new Intent(context, TracksListingActivity.class);
                startActivityForResult(trackIntent, TRACK_ACTIVITY_RESULT_CODE);
            }
        });

        step1Dialog.show();
        step1Dialog.setCancelable(false);
        step1Dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        step1Dialog.getWindow().setGravity(Gravity.CENTER);

    }

    private void stepHideDetailsDialog() {

        step2Dialog = new Dialog(context);
        step2Dialog.setContentView(R.layout.group_new_step1_dialog);
        step2Dialog.setCancelable(false);
        step2Dialog.setCanceledOnTouchOutside(false);

        TextViewPlayBold profileUploadBtn;
        TextViewPlayBold coverUploadBtn;
        final EditTextPlayRegular descriptionEt;
        TextViewPlayBold backBtn;
        EditTextPlayRegular mGroupName;
        final TextViewPlayBold nextBtn;


        final TextView txt_desc_name;

        mGroupName=(EditTextPlayRegular)step2Dialog.findViewById(R.id.editGroupName);
        groupProfileIv = (ImageView) step2Dialog.findViewById(R.id.group_profile_iv);
        groupCoverIv = (ImageView) step2Dialog.findViewById(R.id.group_cover_iv);
        profileUploadBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.profile_upload_btn);
        coverUploadBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.cover_upload_btn);
        descriptionEt = (EditTextPlayRegular) step2Dialog.findViewById(R.id.description_et);
        backBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.back_btn);
        nextBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.next_btn);
        post_image = (ImageView) step2Dialog.findViewById(R.id.post_image);
        txt_desc_name = (TextView) step2Dialog.findViewById(R.id.txt_desc_name);
        group_name_tv = (TextView) step2Dialog.findViewById(R.id.group_name_et);




        profileUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = I.PROFILE;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
            }
        });

        coverUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = I.COVER;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
            }
        });

        descriptionEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                txt_desc_name.setText(editable.toString());
            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step2Dialog.cancel();
                finish();
                //step1Dialog.show();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.hideKeyboard(getCurrentFocus(), context);
                try {
                    String description = descriptionEt.getText().toString().trim();
                    if (Validation.addGroupStep2Validation(description, profilePagerBean, coverPagerBean, context, nextBtn)) {
                        beanAddGroup.setGroupName(mGroupName.getText().toString());
                        beanAddGroup.setGroupDesc(description);
                        beanAddGroup.setProfilePagerBean(profilePagerBean);
                        beanAddGroup.setCoverPagerBean(coverPagerBean);

                        step2Dialog.cancel();
                        step3Dialog.show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
        step2Dialog.setCancelable(false);
        step2Dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        step2Dialog.getWindow().setGravity(Gravity.CENTER);
    }

    private void step2Dialog() {

        step2Dialog = new Dialog(context);
        step2Dialog.setContentView(R.layout.group_step2_dialog);
        step2Dialog.setCancelable(false);
        step2Dialog.setCanceledOnTouchOutside(false);


        TextViewPlayBold profileUploadBtn;
        TextViewPlayBold coverUploadBtn;
        final EditTextPlayRegular descriptionEt;
        TextViewPlayBold backBtn;
        EditTextPlayRegular mGroupName;
        final TextViewPlayBold nextBtn;


        final TextView txt_desc_name;

       // mGroupName=(EditTextPlayRegular)step2Dialog.findViewById(R.id.editGroupName);
        groupProfileIv = (ImageView) step2Dialog.findViewById(R.id.group_profile_iv);
        groupCoverIv = (ImageView) step2Dialog.findViewById(R.id.group_cover_iv);
        profileUploadBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.profile_upload_btn);
        coverUploadBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.cover_upload_btn);
        descriptionEt = (EditTextPlayRegular) step2Dialog.findViewById(R.id.description_et);
        backBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.back_btn);
        nextBtn = (TextViewPlayBold) step2Dialog.findViewById(R.id.next_btn);
        post_image = (ImageView) step2Dialog.findViewById(R.id.post_image);
        txt_desc_name = (TextView) step2Dialog.findViewById(R.id.txt_desc_name);
        group_name_tv = (TextView) step2Dialog.findViewById(R.id.group_name_et);




        profileUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = I.PROFILE;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
            }
        });

        coverUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = I.COVER;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
            }
        });

        descriptionEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                txt_desc_name.setText(editable.toString());
            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step2Dialog.cancel();
                //step1Dialog.show();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.hideKeyboard(getCurrentFocus(), context);
                try {

                    String description = descriptionEt.getText().toString().trim();
                    if (Validation.addGroupStep2Validation(description, profilePagerBean, coverPagerBean, context, nextBtn)) {
                       // beanAddGroup.setGroupName(mGroupName.getText().toString());
                        beanAddGroup.setGroupDesc(description);
                        beanAddGroup.setProfilePagerBean(profilePagerBean);
                        beanAddGroup.setCoverPagerBean(coverPagerBean);

                        step2Dialog.cancel();
                        step3Dialog.show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
        step2Dialog.setCancelable(false);
        step2Dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        step2Dialog.getWindow().setGravity(Gravity.CENTER);
    }

    private void step3Dialog() {
        step3Dialog = new Dialog(context);
        step3Dialog.setContentView(R.layout.group_step3_dialog);
        step3Dialog.setCancelable(false);
        step3Dialog.setCanceledOnTouchOutside(false);
        RecyclerView recyclerView;
        TextViewPlayBold backBtn;
        final TextViewPlayBold nextBtn;

        recyclerView = (RecyclerView) step3Dialog.findViewById(R.id.recycler_view);
        backBtn = (TextViewPlayBold) step3Dialog.findViewById(R.id.back_btn);
        nextBtn = (TextViewPlayBold) step3Dialog.findViewById(R.id.next_btn);

        spinDialogAdapter = new SettingBlockUserSuggestionListAdapter(context, mMyFriendsLists, true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                step3Dialog.cancel();
                step2Dialog.show();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spinDialogAdapter.getSelectedFriendsList().length() > 0) {
                    beanAddGroup.setTag_friend(spinDialogAdapter.getSelectedFriendsList());
                    step3Dialog.cancel();
                    authAPI.createUserGroup(context, MySharedPreferences.getPreferences(context, S.user_id), beanAddGroup);

                } else {
                    Util.showSnackBar(nextBtn, context.getString(R.string.friends_select_empty));
                }
            }
        });

        step3Dialog.setCancelable(false);
        step3Dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        step3Dialog.getWindow().setGravity(Gravity.CENTER);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }

    /* myFriendList webservice response */
    public void myFriendListWebserviceResponse(String response) {
        mMyFriendsLists.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                mMyFriendsLists.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.getString(S.id));
                    profileMyFriendsList.setUserId(jsonObject1.getString(S.userId));
                    profileMyFriendsList.setStatus(jsonObject1.getString(S.status));

                    PersonalInfo personalInfo = new PersonalInfo();
                    JSONObject personalInfoJson = jsonObject1.getJSONObject(S.personalInfo);
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    profileMyFriendsList.setPersonalInfo(personalInfo);

                    mMyFriendsLists.add(profileMyFriendsList);

                }
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {

    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {
        switch (type) {
            case I.GROUPTYPE:
                groupTypeTv.setText(value);
                groupTypeID = id;

                break;
            case I.VEHICLE_MODEL:
                vehicleTypeTv.setText(value);
                vehicleId = id;
                break;
            case I.PRIVACY:
                privacy = id;
                groupPrivacyTv.setText(value);

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TRACK_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                trackTypeTv.setText(data.getStringExtra(S.trackname));
                trackId = data.getStringExtra(S.trackId);
            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, CreateGroupActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;
                String imageAbsolutePath = imageFile.getAbsolutePath();
                String imageFileName = imageFile.getName();
                if (image_type == I.PROFILE) {
                    profilePagerBean = new PagerBean(imageFile, imageAbsolutePath, imageFileName, false, false, "");

                    String uri = Uri.fromFile(imageFile).toString();
                    String decoded = Uri.decode(uri);
                    ImageLoader.getInstance().displayImage(decoded, post_image);
                    ImageLoader.getInstance().displayImage(decoded, groupProfileIv);

                } else {
                    coverPagerBean = new PagerBean(imageFile, imageAbsolutePath, imageFileName, false, false, "");
                    String uri = Uri.fromFile(imageFile).toString();
                    String decoded = Uri.decode(uri);
                    ImageLoader.getInstance().displayImage(decoded, groupCoverIv);

                }
            }


        });
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {

        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }

    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {
        EasyImage.openGallery(this, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {

    }


    public void createGroupWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, "");
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                groupTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    groupTypes_list.add(beanVehicleType);
                }
                showVehicleTypeDialog(getString(R.string.select_group_type), groupTypes_list, I.GROUPTYPE);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // show Vehicle list webservice response
    public void getVehicleListResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < jsonArray.length(); i++) {

                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));

                    JSONObject jsonObject3 = jsonObject1.getJSONObject(S.vehicleBrandId);
                    JSONObject jsonObject4 = jsonObject1.getJSONObject(S.vehicleModelId);

                    beanVehicleType.setName(jsonObject3.getString(S.vehicleBrandName) + " " + jsonObject4.getString(S.vehicleModelName));

                    vehicle_list.add(beanVehicleType);
                }

                showVehicleTypeDialog(getString(R.string.select_vehicle), vehicle_list, I.VEHICLE_MODEL);
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
