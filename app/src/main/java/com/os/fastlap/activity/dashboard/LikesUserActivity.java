package com.os.fastlap.activity.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.profile.ProfileActivity;
import com.os.fastlap.adapter.dashboard.LikesUserAdapter;
import com.os.fastlap.beans.dashboardmodals.LikeUserBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 7/12/2017.
 */

public class LikesUserActivity extends BaseActivity implements View.OnClickListener,LikesUserAdapter.OnItemClikcView {


    private RecyclerView likesRecyclerView;

    ArrayList<LikeUserBean> likes_list = new ArrayList<>();
    LikesUserAdapter likesUserAdapter;
    Context context;
    ImageView base_toggle_icon;
    AuthAPI authAPI;
    TextView no_data_tv;
    String post_id = "";
    String postComment_id = "";
    String userPostCommentReplyId = "";
    private String TAG = LikesUserActivity.class.getSimpleName();
    int likeType = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.likes_user_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();

        likeType = getIntent().getIntExtra(S.likeType, 0);

        if (likeType == I.POSTLIKE) {
            post_id = getIntent().getStringExtra(S.postId);
            authAPI.getPostLikeUserList(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);
        }else if (likeType == I.ADLIKE)
        {
            post_id = getIntent().getStringExtra(S.postId);
            authAPI.getAdLikeUserList(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);
        }  else if (likeType == I.ALBUMLIKE) {
            post_id = getIntent().getStringExtra(S.postId);
            authAPI.getAlbumLikeUserList(context, MySharedPreferences.getPreferences(context, S.user_id), post_id);
        } else if (likeType == I.COMMENTLIKE) {
            post_id = getIntent().getStringExtra(S.postId);
            postComment_id = getIntent().getStringExtra(S.userPostCommentId);
            authAPI.getCommentLikeUserList(context, MySharedPreferences.getPreferences(context, S.user_id), post_id, postComment_id);
        } else if (likeType == I.ALBUMCOMMENTLIKE) {
            post_id = getIntent().getStringExtra(S.postId);
            postComment_id = getIntent().getStringExtra(S.userPostCommentId);
            authAPI.getCommentAlbumLikeUserList(context, MySharedPreferences.getPreferences(context, S.user_id), postComment_id);
        } else if (likeType == I.REPLYLIKE) {
            post_id = getIntent().getStringExtra(S.postId);
            postComment_id = getIntent().getStringExtra(S.userPostCommentId);
            userPostCommentReplyId = getIntent().getStringExtra(S.userPostCommentReplyId);
            authAPI.getReplyLikeUserList(context, MySharedPreferences.getPreferences(context, S.user_id), userPostCommentReplyId);
        } else if (likeType == I.ALBUMREPLYLIKE) {
            post_id = getIntent().getStringExtra(S.postId);
            postComment_id = getIntent().getStringExtra(S.userPostCommentId);
            userPostCommentReplyId = getIntent().getStringExtra(S.userPostCommentReplyId);
            authAPI.getAlbumReplyLikeUserList(context, MySharedPreferences.getPreferences(context, S.user_id), userPostCommentReplyId);
        }
    }

    private void initView() {
        likesRecyclerView = (RecyclerView) findViewById(R.id.likes_recyclerView);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);
        clickListner();

        likes_list = new ArrayList<>();
        likes_list.clear();


        likesUserAdapter = new LikesUserAdapter(likes_list, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        likesRecyclerView.setLayoutManager(layoutManager);
        likesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        likesRecyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        likesRecyclerView.setAdapter(likesUserAdapter);
        likesUserAdapter.setOnItemClikcView(this);
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();

                break;
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.DASHBOARD_SCREEN);
        setToolbarHeading(getString(R.string.likes));
        FastLapApplication.mCurrentContext = context;
    }

    public void postLikeUserListingResponse(String response) {
        likes_list.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < dataJson.length(); i++) {
                    LikeUserBean likeUserBean = new LikeUserBean();
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);

                    likeUserBean.set_id(jsonObject1.getString(S._id));
                    likeUserBean.setLike_user_friend_status(jsonObject1.getString(S.alreadyFriend));
                    likeUserBean.setLike_user_id(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    likeUserBean.setLike_user_name(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    likeUserBean.setLike_user_image(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));

                    likes_list.add(likeUserBean);
                }

                likesUserAdapter.notifyDataSetChanged();

                if (likes_list.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    likesRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    likesRecyclerView.setVisibility(View.GONE);
                }


            } else {
                no_data_tv.setVisibility(View.VISIBLE);
                likesRecyclerView.setVisibility(View.GONE);
                //Util.showAlertDialog(context, getString(R.string.alert), msg);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            no_data_tv.setVisibility(View.VISIBLE);
            likesRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(String id) {
        Intent intentProfile = new Intent(context, ProfileActivity.class);
        Bundle bundleProfile = new Bundle();
        bundleProfile.putString(S.user_id, id);
        bundleProfile.putString("status", "0");
        /*bundleProfile.putInt(S.page, I.DASHBOARD_SCREEN);
        // bundleProfile.putString(S.user_id, mPostListBeen.get(getAdapterPosition()).getUserDataBeans().get_id());
        bundleProfile.putString("status", "0");*/
        intentProfile.putExtras(bundleProfile);
        startActivity(intentProfile);
    }
}
