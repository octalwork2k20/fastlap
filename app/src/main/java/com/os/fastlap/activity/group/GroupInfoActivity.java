package com.os.fastlap.activity.group;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.adapter.MemberListAdapter;
import com.os.fastlap.beans.dashboardmodals.LikeUserBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/12/2017.
 */

public class GroupInfoActivity extends BaseActivity implements View.OnClickListener {

    private AppCompatImageView baseToggleIcon;


    private Context mContext;
    private AuthAPI authAPI;
    private final String TAG = GroupInfoActivity.class.getSimpleName();
    private ImageView imgBg;
    private TextViewPlayRegular txtUsername;
    private CircleImageView userIcon;
    private TextViewPlayRegular txtAboutme;
    private TextViewPlayRegular txtTypeOfEvent;
    private RecyclerView recyclerViewPilots;
    ArrayList<LikeUserBean> memberList;
    MemberListAdapter memberAdapter;
    private TextView txt_pilots_count;
    private TextView txt_friends_count;
    private TextView txt_friends_out_of;
    private TextView invite_friends_tv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_info_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.DASHBOARD_SCREEN);
        setToolbarHeading(getString(R.string.group));
        FastLapApplication.mCurrentContext = mContext;
        authAPI.getGroupAbout(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), GroupProfileActivity.profileGroupId);
        authAPI.getGroupMembersList(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), GroupProfileActivity.profileGroupId);
    }

    private void initView() {
        baseToggleIcon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);

        imgBg = (ImageView) findViewById(R.id.img_bg);
        txtUsername = (TextViewPlayRegular) findViewById(R.id.txt_username);
        userIcon = (CircleImageView) findViewById(R.id.user_icon);
        txtAboutme = (TextViewPlayRegular) findViewById(R.id.txt_aboutme);
        txtTypeOfEvent = (TextViewPlayRegular) findViewById(R.id.txt_type_of_event);
        txt_pilots_count = (TextView) findViewById(R.id.txt_pilots_count);
        txt_friends_count = (TextView) findViewById(R.id.txt_friends_count);
        txt_friends_out_of = (TextView) findViewById(R.id.txt_friends_out_of);
        invite_friends_tv = (TextView) findViewById(R.id.invite_friends_tv);
        recyclerViewPilots = (RecyclerView) findViewById(R.id.recycler_view_pilots);

        memberList = new ArrayList<>();
        memberList.clear();

        memberAdapter = new MemberListAdapter(mContext, memberList);
        RecyclerView.LayoutManager mFavLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewPilots.setLayoutManager(mFavLayoutManager);
        recyclerViewPilots.setAdapter(memberAdapter);

        setOnCLickListener();
        setProfileData();



    }

    private void setOnCLickListener() {
        baseToggleIcon.setOnClickListener(this);
        invite_friends_tv.setOnClickListener(this);

    }

    private void setProfileData() {
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + GroupProfileActivity.profileGroupCoverImage, imgBg, Util.getImageLoaderOption(mContext));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + GroupProfileActivity.profileGroupProfileImage, userIcon, Util.getImageLoaderOption(mContext));
        txtUsername.setText(GroupProfileActivity.profileGroupName);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.invite_friends_tv:
                startActivity(new Intent(mContext, InviteGroupMemberActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
        }
    }

    /* group info webservice response */
    public void groupInfoWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONObject jsonObject1 = jsonObject.getJSONObject(S.data);
                txtAboutme.setText(jsonObject1.getString(S.description));

                JSONObject userJson = jsonObject1.getJSONObject(S.userId);
                JSONObject personInfo = userJson.getJSONObject(S.personalInfo);

                JSONObject groupTypeJson = jsonObject1.getJSONObject(S.groupTypeId);
                txtTypeOfEvent.setText(getString(R.string.qualify_for) + " " + groupTypeJson.getString(S.name) + " " + getString(R.string.events));

                if(userJson.getString(S._id).equalsIgnoreCase(MySharedPreferences.getPreferences(mContext,S.user_id)))
                {
                    invite_friends_tv.setVisibility(View.VISIBLE);
                }
                else
                {
                    invite_friends_tv.setVisibility(View.GONE);
                }
                JSONObject trackJson = jsonObject1.getJSONObject(S.trackId);
                JSONObject vehicleJson = jsonObject1.getJSONObject(S.userVehicleId);
                setProfileData();


            }/* else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void groupMemberWebserviceResponse(String response) {
        memberList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);

                for (int i = 0; i < dataJson.length(); i++) {
                    LikeUserBean likeUserBean = new LikeUserBean();
                    JSONObject jsonObject1 = dataJson.getJSONObject(i);

                    likeUserBean.set_id(jsonObject1.getString(S._id));
                    likeUserBean.setLike_user_friend_status(jsonObject1.getString(S.alreadyFriend));
                    likeUserBean.setLike_user_id(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    likeUserBean.setLike_user_name(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    likeUserBean.setLike_user_image(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));


                    memberList.add(likeUserBean);
                }


                int count=0;
                for(int i = 0; i< memberList.size(); i++)
                {
                    if(memberList.get(i).getLike_user_friend_status().compareTo("1")==0)
                    {
                        count++;
                    }
                }

                txt_friends_count.setText(count+"");

                txt_pilots_count.setText(memberList.size() + "");
                txt_friends_out_of.setText("/"+memberList.size());


                memberAdapter.notifyDataSetChanged();


            } /*else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
