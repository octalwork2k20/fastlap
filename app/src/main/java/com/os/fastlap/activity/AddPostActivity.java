package com.os.fastlap.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.BetterViewPager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.dashboard.DashboardVehicleListActivity;
import com.os.fastlap.adapter.AddPostPagerAdapter;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.settings.SettingBlockUserSuggestionListAdapter;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.beans.dashboardmodals.DashboardPostListBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.libs.Gallery;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;
import com.rw.keyboardlistener.KeyboardUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.os.fastlap.R.id.base_toggle_icon;
import static com.os.fastlap.R.id.speed_menu_ll;
import static com.os.fastlap.activity.BaseActivity.LAP_TIME_ACTIVITY_RESULT_CODE;
import static com.os.fastlap.activity.BaseActivity.TRACK_ACTIVITY_RESULT_CODE;
import static com.os.fastlap.activity.BaseActivity.VEHICLE_ACTIVITY_RESULT_CODE;


/*
 * Created by abhinava on 8/16/2017.
 */

public class AddPostActivity extends AppCompatActivity implements View.OnClickListener, OnImagePickerDialogSelect, SpinnerSelectorInterface {
    Context context;

    @BindView(R.id.txtNickname)
    TextView txtNickname;

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @BindView(R.id.arraowleft_iv)
    ImageView arraowleft_iv;

    @BindView(R.id.arraowright_iv)
    ImageView arraowright_iv;

    @BindView(R.id.txtSelectTrackName)
    TextView txtSelectTrackName;

    @BindView(R.id.txtSelectVehicleName)
    TextView txtSelectVehicleName;

    @BindView(R.id.txtBestPosition)
    TextView txtBestPosition;

    @BindView(R.id.txtSpeed)
    TextView txtSpeed;
    @BindView(R.id.txtWeather)
    TextView txtWeather;

    @BindView(R.id.txtTrackName)
    TextView txtTrackName;

    @BindView(R.id.action_bar)
    RelativeLayout actionBar;

    @BindView(R.id.base_toggle_icon)
    AppCompatImageView baseToggleIcon;

    @BindView(R.id.btnPost)
    TextView btnPost;

    @BindView(R.id.user_img)
    CircleImageView userImg;

    @BindView(R.id.user_name_tv)
    TextView userNameTv;

    @BindView(R.id.post_edittext)
    EditText postEdittext;

    @BindView(R.id.track_menu_ll)
    LinearLayout trackMenuLl;

    @BindView(R.id.media_Rl)
    RelativeLayout media_Rl;

    @BindView(R.id.vehicle_menu_ll)
    LinearLayout vehicleMenuLl;

    @BindView(R.id.position_menu_ll)
    LinearLayout positionMenuLl;

    @BindView(R.id.speed_menu_ll)
    LinearLayout speedMenuLl;

    @BindView(R.id.weather_menu_ll)
    LinearLayout weatherMenuLl;

    @BindView(R.id.image_menu_ll)
    LinearLayout imageMenuLl;


    @BindView(R.id.feeling_menu_ll)
    LinearLayout feelingMenuLl;

    @BindView(R.id.post_header_rl)
    RelativeLayout postHeaderRl;

    @BindView(R.id.bottom_attach_rl)
    LinearLayout bottomAttachRl;

    @BindView(R.id.tag_friends_menu_ll)
    LinearLayout tagFriendsMenuLl;

    @BindView(R.id.lap_time_menu_ll)
    LinearLayout lap_time_menu_ll;


    @BindView(R.id.home_image_ll)
    FrameLayout homeImageLl;

    @BindView(R.id.ll_feed_image)
    LinearLayout llFeedImage;

    @BindView(R.id.home_row_feed_image_iv1)
    ImageView homeRowFeedImageIv1;


    @BindView(R.id.home_row_feed_image_iv2)
    ImageView homeRowFeedImageIv2;

    @BindView(R.id.ll_feed_image2)
    LinearLayout llFeedImage2;

    @BindView(R.id.home_row_feed_image_iv3)
    ImageView homeRowFeedImageIv3;


    @BindView(R.id.ll_inner_home_row_feed_image_iv4)
    FrameLayout llInnerHomeRowFeedImageIv4;

    @BindView(R.id.home_row_feed_image_iv4)
    ImageView homeRowFeedImageIv4;

    @BindView(R.id.ll_alpha)
    LinearLayout llAlpha;

    @BindView(R.id.iv_points)
    TextView ivPoints;

    @BindView(R.id.keyboardToplayout)
    RelativeLayout keyboardToplayout;

    @BindView(R.id.expandLayout)
    LinearLayout expandLayout;

    @BindView(R.id.expand_icon)
    ImageView expand_icon;

    @BindView(R.id.txtLapTime)
    TextView txtLapTime;

    @BindView(R.id.txtFeeling)
    TextView txtFeeling;

    @BindView(R.id.txtFriendName)
    TextView txtFriendName;

    private static final int ADD_POST_ACTIVITY = 3;
    private OnImagePickerDialogSelect onImagePickerDialogSelect;
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    public PagerBean pagerBean;
    public ArrayList<PagerBean> imageList = new ArrayList<>();
    private String TAG = AddPostActivity.class.getSimpleName();
    private ArrayList<BeanVehicleType> feeling_array = new ArrayList<>();
    private ArrayList<BeanVehicleType> weather_array = new ArrayList<>();
    private AuthAPI authAPI;
    private SpinnerSelectorInterface spinnerSelectorInterface;
    private String vehicle_id = "", vehicle_name = "";
    private String track_name = "", track_id = "";
    private String lap_time = "", lap_id = "";
    private String feeling_name = "", feeling_id = "";
    private String weather_name = "", speed_name = "", position_name = "";
    private Dialog dialog1;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    private SettingBlockUserSuggestionListAdapter spinDialogAdapter;
    private ArrayList<PagerBean> selectedImage = new ArrayList<>();
    private String select_value = "";
    private String feelingSpan = "";
    private String tagfriendspan = "";
    private String deafultSpna = "";
    private SpannableStringBuilder deafult_spna;
    private Uri videoFileUri;
    private DashboardPostListBean post_details = null;
    private AddPostPagerAdapter customPagerAdapter;
    private String removeImage = "";
    int width;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_post_activity);
        ButterKnife.bind(this);
        context = this;
        FastLapApplication.mCurrentContext = context;
        onImagePickerDialogSelect = this;
        spinnerSelectorInterface = this;
        authAPI = new AuthAPI(context);
        initView();
        authAPI.getFeelings(context);

        userNameTv.setText(MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image), userImg, Util.getImageLoaderOption(context));
    }

    private void initView() {
        fillWeatherArray();
        deafultSpna = "<font color=#bd2436>" + MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response) + "</font>";
        mMyFriendsLists = new ArrayList<>();
        spinDialogAdapter = new SettingBlockUserSuggestionListAdapter(context, mMyFriendsLists, true);

        customPagerAdapter = new AddPostPagerAdapter(context, selectedImage);
        viewpager.setAdapter(customPagerAdapter);


        imageList.clear();
        clickListner();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;

        if (getIntent().getExtras() != null) {
            post_details = (DashboardPostListBean) getIntent().getExtras().getSerializable(S.data);
            if (post_details != null) {
                postEdittext.setText(post_details.getDescription());
                postEdittext.setSelection(postEdittext.getText().length());
                if (post_details.getTrackData() != null)
                {
                    track_name = post_details.getTrackData().getName();
                    track_id = post_details.getTrackData().getId();
                    txtSelectTrackName.setText(track_name);
                }
                if (post_details.getBeanVehicleList() != null) {
                    vehicle_name = post_details.getBeanVehicleList().getVehicleBrandIdModal().getVehicleBrandName() + " " + post_details.getBeanVehicleList().getVehicleModelIdModal().getVehicleModelName() + " " + post_details.getBeanVehicleList().getVehicleTypeIdModal().getName();
                    vehicle_id = post_details.getBeanVehicleList().get_id();
                    txtSelectVehicleName.setText(vehicle_name);
                }
                if (!post_details.getSpeed().isEmpty()) {
                    speed_name = post_details.getSpeed();
                    txtSpeed.setText(speed_name + " " + context.getString(R.string.km_per_hour));
                }
                if (!post_details.getPosition_text().isEmpty()) {
                    position_name = post_details.getPosition_text();
                    txtBestPosition.setText(position_name);
                }
                if (!post_details.getWeather().isEmpty()) {
                    weather_name = post_details.getWeather();
                    txtWeather.setText(weather_name);
                }
                if (post_details.getDashboardFeelingIdBeans() != null) {
                    feeling_name = post_details.getDashboardFeelingIdBeans().getName();
                    feeling_id = post_details.getDashboardFeelingIdBeans().get_id();
                    feelingSpan = "<font color=#414141> - Feeling </font><font color=#bd2436>" +
                            Util.getEmoticon(Integer.parseInt(post_details.getDashboardFeelingIdBeans().getUnicodes().replace("U+", "0x").substring(2), 16)) + " " + feeling_name + "</font>";
                    userNameTv.setText(Html.fromHtml(deafultSpna + feelingSpan + tagfriendspan));
                    userNameTv.setText(post_details.getUserDataBeans().getNickname());
                }
                if(post_details.getLapTime()!=null){
                    txtLapTime.setText(post_details.getLapTime().getLap_time());
                    lap_id=post_details.getLapTime().getId();
                }
                if (post_details.getDashboardCommentPostTagFriendsBeanses().size() != 0) {
                    if (mMyFriendsLists.size() == 0)
                        authAPI.myFriendList(context, MySharedPreferences.getPreferences(context, S.user_id), "");
                    else {
                        spinDialogAdapter.addMyFriendsListAll(mMyFriendsLists);
                        spinDialogAdapter.addSelectedTagFriends(post_details.getDashboardCommentPostTagFriendsBeanses());
                        tagfriendspan = " " + spinDialogAdapter.getSelectedFriendsNames();
                        userNameTv.setText((Html.fromHtml(deafultSpna + feelingSpan + tagfriendspan)));
                    }
                }
                if (post_details.getDashboardPostImagesBeans().size() != 0) {
                    selectedImage = new ArrayList<>();
                    for (int i = 0; i < post_details.getDashboardPostImagesBeans().size(); i++) {
                        PagerBean pagerBean = new PagerBean();
                        pagerBean.setImage_id(post_details.getDashboardPostImagesBeans().get(i).get_id());
                        pagerBean.setEdit(true);
                        if (!post_details.getDashboardPostImagesBeans().get(i).getType().equalsIgnoreCase(S.videoType))
                            pagerBean.setImage(post_details.getDashboardPostImagesBeans().get(i).getFileName());
                        else
                            pagerBean.setImage(post_details.getDashboardPostImagesBeans().get(i).getThumbName());
                        pagerBean.setFileType(post_details.getDashboardPostImagesBeans().get(i).getType());

                        selectedImage.add(pagerBean);
                    }
                    setShowMediaFiles(selectedImage);
                }
            }
        }

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int pos) {

                int imageSize = 0;
                imageSize = selectedImage.size();
                if (pos == 0) {
                    arraowleft_iv.setVisibility(View.GONE);
                } else if (imageSize > 1) {
                    arraowleft_iv.setVisibility(View.VISIBLE);
                }

                if ((imageSize - 1) == pos) {
                    arraowright_iv.setVisibility(View.GONE);
                } else if (imageSize > 1) {
                    arraowright_iv.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void fillWeatherArray() {

        BeanVehicleType beanVehicleType = new BeanVehicleType();
        beanVehicleType.setId("1");
        beanVehicleType.setName(getString(R.string.dry));

        weather_array.add(beanVehicleType);

        beanVehicleType = new BeanVehicleType();
        beanVehicleType.setId("1");
        beanVehicleType.setName(getString(R.string.cloud));

        weather_array.add(beanVehicleType);

        beanVehicleType = new BeanVehicleType();
        beanVehicleType.setId("1");
        beanVehicleType.setName(getString(R.string.rain));

        weather_array.add(beanVehicleType);

        beanVehicleType = new BeanVehicleType();
        beanVehicleType.setId("1");
        beanVehicleType.setName(getString(R.string.ice));

        weather_array.add(beanVehicleType);
    }

    private void clickListner() {
        baseToggleIcon.setOnClickListener(this);
        btnPost.setOnClickListener(this);
        vehicleMenuLl.setOnClickListener(this);
        trackMenuLl.setOnClickListener(this);
        positionMenuLl.setOnClickListener(this);
        speedMenuLl.setOnClickListener(this);
        weatherMenuLl.setOnClickListener(this);
        imageMenuLl.setOnClickListener(this);
        feelingMenuLl.setOnClickListener(this);
        tagFriendsMenuLl.setOnClickListener(this);
        arraowleft_iv.setOnClickListener(this);
        arraowright_iv.setOnClickListener(this);
        media_Rl.setOnClickListener(this);
        keyboardToplayout.setOnClickListener(this);
        expand_icon.setOnClickListener(this);
        lap_time_menu_ll.setOnClickListener(this);

        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                if (isVisible) {
                    keyboardToplayout.setVisibility(View.VISIBLE);
                    expandLayout.setVisibility(View.GONE);
                } else {
                    if (keyboardToplayout.getVisibility() == View.VISIBLE) {
                        keyboardToplayout.setVisibility(View.VISIBLE);
                        expandLayout.setVisibility(View.GONE);
                    } else {
                        keyboardToplayout.setVisibility(View.GONE);
                        expandLayout.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.arraowleft_iv:
                int last_pos = viewpager.getCurrentItem();
                int next_post = last_pos - 1;
                viewpager.setCurrentItem(next_post);
                break;
            case R.id.arraowright_iv:
                int lastpos = viewpager.getCurrentItem();
                int nextpost = lastpos + 1;
                viewpager.setCurrentItem(nextpost);
                break;
            case base_toggle_icon:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;

            case R.id.btnPost:
                String description = postEdittext.getText().toString().trim();
                if (Validation.addPostValidation(selectedImage, description, btnPost, context))
                {
                    if (post_details == null){
                        authAPI.postSave(context, MySharedPreferences.getPreferences(context, S.user_id), vehicle_id, track_id,lap_id,
                                txtBestPosition.getText().toString(), feeling_id, description, selectedImage,
                                spinDialogAdapter.getSelectedFriendsList().toString(), getThumbnailCount(),speed_name, weather_name);
                        JsonObject jsonObject=new JsonObject();
                        jsonObject.addProperty("1",MySharedPreferences.getPreferences(context, S.user_id));
                        jsonObject.addProperty("2",vehicle_id);
                        jsonObject.addProperty("3",track_id);
                        jsonObject.addProperty("4",lap_id);
                        jsonObject.addProperty("5",txtBestPosition.getText().toString());
                        jsonObject.addProperty("6",description);
                        jsonObject.addProperty("7",selectedImage.size());
                        jsonObject.addProperty("8",spinDialogAdapter.getSelectedFriendsList().toString());
                        jsonObject.addProperty("9",getThumbnailCount());
                        jsonObject.addProperty("10",speed_name);
                        jsonObject.addProperty("11",weather_name);
                        Log.e("tag","add"+jsonObject.toString());

                    }
                    else{
                        authAPI.updatePost(context, MySharedPreferences.getPreferences(context, S.user_id), vehicle_id,track_id,lap_id,
                                txtBestPosition.getText().toString(), feeling_id, description, selectedImage,
                                spinDialogAdapter.getSelectedFriendsListOnlyNew().toString(), getThumbnailCount(),
                                speed_name, weather_name,removeImage,spinDialogAdapter.getRemoveFriendTag(),
                                post_details.get_id());

                        JsonObject jsonObject=new JsonObject();
                        jsonObject.addProperty("1",MySharedPreferences.getPreferences(context, S.user_id));
                        jsonObject.addProperty("2",vehicle_id);
                        jsonObject.addProperty("3",track_id);
                        jsonObject.addProperty("4",lap_id);
                        jsonObject.addProperty("5",txtBestPosition.getText().toString());
                        jsonObject.addProperty("6",description);
                        jsonObject.addProperty("7",selectedImage.size());
                        jsonObject.addProperty("8",spinDialogAdapter.getSelectedFriendsList().toString());
                        jsonObject.addProperty("9",getThumbnailCount());
                        jsonObject.addProperty("10",speed_name);
                        jsonObject.addProperty("11",weather_name);
                        Log.e("tag","edit"+jsonObject.toString());
                    }


                }
                break;
            case R.id.vehicle_menu_ll:
                Intent intent = new Intent(this, DashboardVehicleListActivity.class);
                intent.putExtra("isShowAddButton",true);
                startActivityForResult(intent, VEHICLE_ACTIVITY_RESULT_CODE);
                break;

            case R.id.track_menu_ll:
                Intent trackIntent = new Intent(context, TracksListingActivity.class);
                startActivityForResult(trackIntent, TRACK_ACTIVITY_RESULT_CODE);
                break;
            case R.id.lap_time_menu_ll:
                Intent laTimeIntent = new Intent(context, LapTimeActivity.class);
                startActivityForResult(laTimeIntent, LAP_TIME_ACTIVITY_RESULT_CODE);
                break;

            case R.id.position_menu_ll:
                showPositionTextDialog();
                break;

            case speed_menu_ll:
                showSpeedDialog();
                break;

            case R.id.weather_menu_ll:
                showWeatherDialog();
                break;

            case R.id.image_menu_ll:
                if (Build.VERSION.SDK_INT >= 23)
                    checkRequestPermission();
                else
                    showImageDialog();
                break;

            case R.id.feeling_menu_ll:
                showFeelingDialog(context.getResources().getString(R.string.select_mood), feeling_array, I.FEELING);
                break;
            case R.id.tag_friends_menu_ll:
                if (mMyFriendsLists.size() == 0)
                    authAPI.myFriendList(context, MySharedPreferences.getPreferences(context, S.user_id), "");
                else
                    showMyFriendsList();
                break;
            case R.id.media_Rl:
                if (selectedImage.size() == 0) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                }
                break;

            case R.id.keyboardToplayout:
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                keyboardToplayout.setVisibility(View.GONE);
                expandLayout.setVisibility(View.VISIBLE);

                KeyboardUtils.forceCloseKeyboard(keyboardToplayout);

                break;

            case R.id.expand_icon:
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                keyboardToplayout.setVisibility(View.VISIBLE);
                expandLayout.setVisibility(View.GONE);
                break;
        }
    }

    private int getThumbnailCount() {
        int length = 0;
        for (int i = 0; i < selectedImage.size(); i++) {
            if (selectedImage.get(i).getFileType().equalsIgnoreCase(S.video))
                length++;
        }
        return length;
    }

    private void showSpeedDialog() {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_edit_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        final EditText edtSpeed = (EditText) dialog1.findViewById(R.id.edtSpeed);
        edtSpeed.setHint(getString(R.string.enter_speed));
        edtSpeed.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
        edtSpeed.setInputType(InputType.TYPE_CLASS_NUMBER);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        done_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String speed = edtSpeed.getText().toString().trim();
                if (speed.isEmpty())
                    Util.showAlertDialog(context, getString(R.string.alert), getString(R.string.please_enter_speed));
                else {
                    dialog1.dismiss();
                    speed_name = speed;
                    txtSpeed.setText(speed_name + " km/h");
                    // postEdittext.setText(postEdittext.getText().toString() + " " + speed_name);
                }
            }
        });
        heading.setText(getString(R.string.speed));

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    private void showPositionTextDialog() {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_edit_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        final EditText edtSpeed = (EditText) dialog1.findViewById(R.id.edtSpeed);
        edtSpeed.setHint(getString(R.string.enter_your_best_position));
        edtSpeed.setFilters(new InputFilter[]{new InputFilter.LengthFilter(7)});
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        done_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String speed = edtSpeed.getText().toString().trim();
                if (speed.isEmpty())
                    Util.showAlertDialog(context, getString(R.string.alert), getString(R.string.please_enter_speed));
                else {
                    dialog1.dismiss();
                    position_name = speed;
                    txtBestPosition.setText(position_name);
                }
            }
        });
        heading.setText(getString(R.string.best_position));
        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    /* Post Save webservice response */
    public void savePostWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                // JSONObject dataJson = jsonObject.getJSONObject(S.data);
                setResult(Activity.RESULT_OK);
                onBackPressed();
                //Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postSave_api);
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);


        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    private void showWeatherDialog() {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(getString(R.string.select_weather));

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, weather_array, spinnerSelectorInterface, I.WEATHER, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    public void showImageDialog() {
        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        //chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
        chooseImagePickerOptionDialog.makeVisible();
        pagerBean = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }

    /* myFriendList webservice response */
    public void myFriendListWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                mMyFriendsLists.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.getString(S.id));
                    profileMyFriendsList.setUserId(jsonObject1.getString(S.userId));
                    profileMyFriendsList.setStatus(jsonObject1.getString(S.status));

                    PersonalInfo personalInfo = new PersonalInfo();
                    JSONObject personalInfoJson = jsonObject1.getJSONObject(S.personalInfo);
                    personalInfo.setFirstName(personalInfoJson.getString(S.firstName_response));
                    personalInfo.setLastName(personalInfoJson.getString(S.lastName_response));
                    personalInfo.setVoucherCode(personalInfoJson.getString(S.voucherCode_response));
                    personalInfo.setDateOfBirth(personalInfoJson.getString(S.dateOfBirth_response));
                    personalInfo.setMobileNumber(personalInfoJson.getString(S.mobileNumber_response));
                    personalInfo.setImage(personalInfoJson.getString(S.image));
                    personalInfo.setCoverImage(personalInfoJson.getString(S.coverImage));
                    personalInfo.setLanguage(personalInfoJson.getString(S.language_response));
                    profileMyFriendsList.setPersonalInfo(personalInfo);

                    mMyFriendsLists.add(profileMyFriendsList);
                }
                spinDialogAdapter.notifyDataSetChanged();
                if (post_details == null)
                    showMyFriendsList();
                else {
                    spinDialogAdapter.addSelectedTagFriends(post_details.getDashboardCommentPostTagFriendsBeanses());
                    tagfriendspan = " " + spinDialogAdapter.getSelectedFriendsNames();
                    userNameTv.setText((Html.fromHtml(deafultSpna + feelingSpan + tagfriendspan)));
                }
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showMyFriendsList() {
        dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        EditText etSearch = (EditText) dialog1.findViewById(R.id.etSearch);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                spinDialogAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        done_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tagfriendspan = " " + spinDialogAdapter.getSelectedFriendsNames();
                userNameTv.setText((Html.fromHtml(deafultSpna + feelingSpan + tagfriendspan)));
                // userNameTv.setText(addClickablePart(), TextView.BufferType.SPANNABLE);
                txtFriendName.setText(Html.fromHtml(deafultSpna + tagfriendspan));
                dialog1.dismiss();
            }
        });
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(getString(R.string.select_friends));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);
        spinDialogAdapter.addMyFriendsListAll(mMyFriendsLists);
        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {/*
        Intent intent = new Intent(this, AlbumSelectActivity.class);
//set limit on number of images that can be selected, default is 10
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5);
        startActivityForResult(intent, SELECT_FILE_PROFILE);*/
        /*

        EasyImage.openGallery(this, SELECT_FILE_PROFILE, true);*/

        Intent intent = new Intent(this, Gallery.class);
        //Set the title
        intent.putExtra("title", "Select media");
        startActivityForResult(intent, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {
        // create new Intentwith with Standard Intent action that can be
        // sent to have the camera application capture an video and return it.
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // create a file to save the video
        videoFileUri = Util.getOutputMediaFileUri(I.MEDIA_TYPE_VIDEO, context);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoFileUri);

        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // set permission
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        // start the Video Capture Intent
        startActivityForResult(intent, I.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VEHICLE_ACTIVITY_RESULT_CODE)
        {
            if (resultCode == RESULT_OK) {
                vehicle_name = data.getStringExtra(S.vehiclename);
                vehicle_id = data.getStringExtra(S.vehicleId);
                txtSelectVehicleName.setText(vehicle_name);
                //postEdittext.setText(postEdittext.getText().toString() + " " + vehicle_name);
            }
        }
        else if (requestCode == TRACK_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                track_name = data.getStringExtra(S.trackname);
                track_id = data.getStringExtra(S.trackId);

                txtSelectTrackName.setText(track_name);
                //postEdittext.setText(postEdittext.getText().toString() + " " + track_name);

            }
        }
        else if (requestCode == LAP_TIME_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                lap_time = data.getStringExtra(S.lapTime);
                lap_id = data.getStringExtra(S.lapId);
                txtLapTime.setText(lap_time);
            }
        }
        else if (requestCode == SELECT_FILE_PROFILE) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
                int i0 = selectionResult.size();

                //  selectedImage.clear();

                for (int i = 0; i < selectionResult.size(); i++) {
                    PagerBean pagerBean = new PagerBean();
                    File file = new File(selectionResult.get(i));

                    pagerBean.setFile(file);
                    pagerBean.setEdit(false);
                    pagerBean.setFileName(file.getName());
                    pagerBean.setFileURl(Uri.fromFile(file).toString());
                    pagerBean.setFileType(Util.isImageOrVideoFile(file.getPath()), file.getPath(), context);
                    selectedImage.add(pagerBean);
                }

                setShowMediaFiles(selectedImage);

            }
        } else if (requestCode == I.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                PagerBean pagerBean = new PagerBean();
                File file = new File(Util.getFilePathFromURI(context, data.getData()));

                pagerBean.setFile(file);
                pagerBean.setEdit(false);
                pagerBean.setFileName(file.getName());
                pagerBean.setFileURl(data.getData().toString());
                pagerBean.setFileType(Util.isImageOrVideoFile(file.getPath()), file.getPath(), context);
                selectedImage.add(pagerBean);
                setShowMediaFiles(selectedImage);
            } else {
                PagerBean pagerBean = new PagerBean();
                pagerBean.setEdit(false);
                File file = new File(Util.getFilePathFromURI(context, videoFileUri));

                pagerBean.setFile(file);
                pagerBean.setFileName(file.getName());
                pagerBean.setFileURl(videoFileUri.getPath());
                pagerBean.setFileType(Util.isImageOrVideoFile(file.getPath()), file.getPath(), context);
                selectedImage.add(pagerBean);
                setShowMediaFiles(selectedImage);
            }

            //Log.e("filepathvideo", data.getData().getPath());
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, AddPostActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource imageSource, int i) {

                String imageAbsolutePath = imageFile.getAbsolutePath();
                String imageFileName = imageFile.getName();

            /*    pagerBean = new PagerBean(imageFile, imageAbsolutePath, imageFileName, false, false, "");
                imageList.add(pagerBean);
*/
                PagerBean pagerBean = new PagerBean();

                pagerBean.setFile(imageFile);
                pagerBean.setEdit(false);
                pagerBean.setFileName(imageFile.getName());
                pagerBean.setFileURl(imageFile.getPath());
                pagerBean.setFileType(S.image, imageFile.getPath(), context);
                selectedImage.add(pagerBean);
                setShowMediaFiles(selectedImage);
            }


        });
    }

    public void setRemoveImages(String id) {
        if (removeImage.isEmpty())
            removeImage = id;
        else
            removeImage = removeImage.concat("," + id);

        setLeftRight();
    }

    private void setLeftRight() {
        int imageSize = 0;
        imageSize = selectedImage.size() - 1;
        if (viewpager.getCurrentItem() == 0) {
            arraowleft_iv.setVisibility(View.GONE);
        } else if (imageSize > 1) {
            arraowleft_iv.setVisibility(View.VISIBLE);
        }

        if ((imageSize - 1) == viewpager.getCurrentItem()) {
            arraowright_iv.setVisibility(View.GONE);
        } else if (imageSize > 1) {
            arraowright_iv.setVisibility(View.VISIBLE);
        }
    }

    public void setShowMediaFiles(ArrayList<PagerBean> imageList) {
        media_Rl.setVisibility(View.VISIBLE);
        customPagerAdapter = new AddPostPagerAdapter(context, imageList);
        viewpager.setAdapter(customPagerAdapter);

        if (imageList.size() == 1) {
            arraowleft_iv.setVisibility(View.GONE);
            arraowright_iv.setVisibility(View.GONE);
        } else {
            arraowleft_iv.setVisibility(View.GONE);
            arraowright_iv.setVisibility(View.VISIBLE);
        }
    }
    public void showFeelingDialog(String string, ArrayList<BeanVehicleType> feeling_array, int vehicleBrand) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(string);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, feeling_array, spinnerSelectorInterface, vehicleBrand, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    public void feelingListWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                feeling_array.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanFeelings = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanFeelings.setId(jsonObject1.getString(S._id));
                    beanFeelings.setName(jsonObject1.getString(S.name));
                    beanFeelings.setUnicode(jsonObject1.getString(S.unicodes));
                    feeling_array.add(beanFeelings);
                }
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {
        if (type == I.FEELING) {
            feeling_name = value;
            feeling_id = id;
            feelingSpan = "<font color=#414141> - Feeling </font><font color=#bd2436>" +
                    Util.getEmoticon(Integer.parseInt(feeling_array.get(pos).getUnicode().replace("U+", "0x").substring(2), 16)) + " " + feeling_name + "</font>";
            userNameTv.setText(Html.fromHtml(deafultSpna + feelingSpan + tagfriendspan));
            txtFeeling.setText(feeling_name);
        } else if (type == I.WEATHER) {
            weather_name = value;
            txtWeather.setText(weather_name);
        }
    }
}
