package com.os.fastlap.activity.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.BaseActivity;
import com.os.fastlap.activity.MenuActivity;
import com.os.fastlap.adapter.paddock.PaddockMyLapAdapter;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandj on 7/14/2017
 */

public class LapsProfileActivity extends BaseActivity implements View.OnClickListener {
    private AppCompatImageView toggle_icon;
    private ImageView imgBg;
    private TextView txtUsername,txtEmail;
    private CircleImageView userIcon;
    private TextView no_data_tv;
    private Context mContext;
    private PaddockMyLapAdapter paddockMyLapsAdapter;
    private ArrayList<PaddockMyLapsParentModel> my_laps_list;
    private ExpandableListView expandable_list_my_laps;
    TextView menu_menu;

    AuthAPI authAPI;
    private String TAG = LapsProfileActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_lap_activity);
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        authAPI = new AuthAPI(mContext);
        initView();

        authAPI.getMyLapsListing(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
    }


    private void initView() {
        menu_menu=(TextView)findViewById(R.id.menu_menu);
        no_data_tv = findViewById(R.id.no_data_tv);
        toggle_icon = (AppCompatImageView) findViewById(R.id.base_toggle_icon);
        imgBg = (ImageView) findViewById(R.id.img_bg);
        txtUsername = (TextView) findViewById(R.id.txt_username);
        txtEmail=(TextView) findViewById(R.id.txt_email);
        userIcon = (CircleImageView) findViewById(R.id.user_icon);

        my_laps_list = new ArrayList<>();
        my_laps_list.clear();

        expandable_list_my_laps = findViewById(R.id.expandable_list_my_laps);

        paddockMyLapsAdapter = new PaddockMyLapAdapter(mContext, my_laps_list);
        expandable_list_my_laps.setAdapter(paddockMyLapsAdapter);


        expandable_list_my_laps.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                setListViewHeight(expandableListView, i);
                return false;
            }
        });

        expandable_list_my_laps.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandable_list_my_laps.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        clickListner();
        setProfileData();

    }
    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();
            if (group != -1) {
                if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                    for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                        View listItem = listAdapter.getChildView(i, j, false, null,
                                listView);
                        listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                        totalHeight += listItem.getMeasuredHeight();

                    }
                }
            }

        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    private void clickListner() {
        menu_menu.setOnClickListener(this);
        toggle_icon.setOnClickListener(this);
    }

    private void setProfileData() {
        try {
            Util.setImageLoader(S.IMAGE_BASE_URL + ProfileActivity.profileUserCoverImage, imgBg, getApplicationContext());
            ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + ProfileActivity.profileUserProfileImage, userIcon, Util.getImageLoaderOption(mContext));
            txtUsername.setText(ProfileActivity.profileUserName);
            txtEmail.setText("("+ProfileActivity.profileEmail+")");
        }
        catch (Exception e){e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setBottomMenu(I.MENU_SCREEN);
        setToolbarHeading(getString(R.string.laps));
        FastLapApplication.mCurrentContext = mContext;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
            case R.id.menu_menu:
                Intent intent = new Intent(this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void getMyLapsWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                my_laps_list.clear();
                JSONArray dataArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataArray.length(); i++)
                {
                    JSONObject jsonObject1 = dataArray.getJSONObject(i);
                    PaddockMyLapsParentModel paddockMyLapsParentModel = new PaddockMyLapsParentModel();
                    paddockMyLapsParentModel.setUserLapId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    paddockMyLapsParentModel.setMyLapCount(jsonObject1.has(S.myLapCount)?jsonObject1.getString(S.myLapCount):"");
                    paddockMyLapsParentModel.setBestPosition(jsonObject1.has(S.bestPosition)?jsonObject1.getString(S.bestPosition):"");
                    JSONObject indexObject = new JSONObject();
                    JSONObject trackObject=new JSONObject();
                    JSONArray tempArray=jsonObject1.has(S.trackVersionData)?jsonObject1.getJSONArray(S.trackVersionData):null;
                    if(tempArray!=null&&tempArray.length()>0)
                    {
                        indexObject = tempArray.getJSONObject(0);
                        trackObject=indexObject.has(S.trackId)?indexObject.getJSONObject(S.trackId):null;
                        if(trackObject!=null)
                        {
                            paddockMyLapsParentModel.setTrack_id(trackObject.has(S._id)?trackObject.getString(S._id):"");
                            paddockMyLapsParentModel.setTrackName(trackObject.has(S.name)?trackObject.getString(S.name):"");
                            paddockMyLapsParentModel.setTrackImage(trackObject.has(S.image)?trackObject.getString(S.image):"");
                            paddockMyLapsParentModel.setTracklocation(trackObject.has(S.location_response)?trackObject.getString(S.location_response):"");
                            paddockMyLapsParentModel.setTrackLength(trackObject.has(S.trackLength)?trackObject.getString(S.trackLength):"");
                            paddockMyLapsParentModel.setTotalTurns(trackObject.getJSONObject(S.turns).has(S.totalTurns)? trackObject.getJSONObject(S.turns).getString(S.totalTurns):"");
                            // paddockMyLapsParentModel.setTotalLapRecord(jsonObject2.getJSONObject(S.trackId).getString(S.totalLapRecords));
                            paddockMyLapsParentModel.setTrackurl(trackObject.getJSONObject(S.contactDetails).getString(S.url));
                            paddockMyLapsParentModel.setTrackMobile(trackObject.getJSONObject(S.contactDetails).getString(S.mobileNumber_response));
                            paddockMyLapsParentModel.setTrackEmail(trackObject.getJSONObject(S.contactDetails).getString(S.email));
                            paddockMyLapsParentModel.setIsFav(indexObject.has(S.isFav)?indexObject.getString(S.isFav):"");
                            paddockMyLapsParentModel.setIsHome(indexObject.has(S.isHome)?indexObject.getString(S.isHome):"");
                            paddockMyLapsParentModel.setTrackCountry(trackObject.getString(S.country));
                            paddockMyLapsParentModel.setTrackVersionName(indexObject.getString(S.name));
                            paddockMyLapsParentModel.setTrackVersionId(indexObject.getString(S._id));
                            JSONArray rxArray = new JSONArray();
                            rxArray = indexObject.has(S.startRight)?indexObject.getJSONArray(S.startRight):null;
                            if (rxArray.length() > 0&&rxArray!=null) {
                                paddockMyLapsParentModel.setTrackRxLng(rxArray.getString(0));
                                paddockMyLapsParentModel.setTrackRxLat(rxArray.getString(1));
                            }
                            JSONArray lxArray = new JSONArray();
                            lxArray=indexObject.has(S.startLeft)?indexObject.getJSONArray(S.startLeft):null;
                            if (lxArray.length() > 0&&lxArray!=null) {
                                paddockMyLapsParentModel.setTrackLxLng(lxArray.getString(0));
                                paddockMyLapsParentModel.setTrackLxLat(lxArray.getString(1));
                            }
                            if (indexObject.has(S.sector)) {
                                if (indexObject.getJSONObject(S.sector).has(S.noSector)) {
                                    if (indexObject.getJSONObject(S.sector).getJSONObject(S.noSector).has(S.image)) {
                                        paddockMyLapsParentModel.setTrackRouteImage(indexObject.getJSONObject(S.sector).getJSONObject(S.noSector).getString(S.image));
                                    }
                                }
                            }
                        }

                    }
                    JSONArray userArray = jsonObject1.getJSONArray(S.lapData);
                    ArrayList<BeanTimeLap> userList = new ArrayList<>();
                    for (int j = 0; j < userArray.length(); j++)
                    {
                        try {
                            JSONObject lapObject = userArray.getJSONObject(j);
                            JSONObject userJsonObject = lapObject.has(S.userId)?lapObject.getJSONObject(S.userId):null;
                            if(userJsonObject!=null&&userJsonObject.length()>0)
                            {
                                BeanTimeLap beanTimeLap = new BeanTimeLap();
                                beanTimeLap.setUserId(userJsonObject.has(S._id)?userJsonObject.getString(S._id):"");
                                if((userJsonObject.getString(S._id).equalsIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id))&&lapObject.getString("status").equalsIgnoreCase("1")))
                                {
                                    if (TextUtils.isEmpty(paddockMyLapsParentModel.getTrackBestPosition()) || paddockMyLapsParentModel.getTrackBestPosition().equalsIgnoreCase("0")) {
                                        if (userJsonObject.getString(S._id).compareToIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)) == 0) {
                                            paddockMyLapsParentModel.setTrackBestPosition((j + 1) + "");
                                        }
                                    }
                                    else {
                                        paddockMyLapsParentModel.setTrackBestPosition((paddockMyLapsParentModel.getTrackBestPosition()));
                                    }
                                    beanTimeLap.setUserImage(userJsonObject.getJSONObject(S.personalInfo).getString(S.image));
                                    beanTimeLap.setUserName(userJsonObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userJsonObject.getJSONObject(S.personalInfo).getString(S.lastName_response));

                                    beanTimeLap.setTimeLap_Id(lapObject.getString(S._id));
                                    beanTimeLap.setLapId(lapObject.getString(S._id));
                                    beanTimeLap.setTrackVersionId(lapObject.getString(S.trackVersionId));

                                    beanTimeLap.setGroupTypeId(lapObject.getJSONObject(S.lapTimeId).has(S.groupTypeId)?lapObject.getJSONObject(S.lapTimeId).getString(S.groupTypeId):"");

                                    beanTimeLap.setLapDate(lapObject.getJSONObject(S.lapTimeId).getString(S.dateOfRecording));

                                    beanTimeLap.setVehicleModelName(lapObject.getJSONObject(S.lapTimeId).getJSONObject(S.userVehicleId).getJSONObject(S.vehicleModelId).getString(S.vehicleModelName));
                                    beanTimeLap.setVehicleBrandName(lapObject.getJSONObject(S.lapTimeId).getJSONObject(S.userVehicleId).getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName));

                                    beanTimeLap.setTime(lapObject.getString(S.time));
                                    beanTimeLap.setFileUrl(lapObject.getString(S.fileUrl));
                                    beanTimeLap.setMaxSpeed(lapObject.getString(S.maxSpeed));
                                    beanTimeLap.setAvgSpeed(lapObject.getString(S.averageSpeed));
                                    beanTimeLap.setAverageSpeedToShow(lapObject.has(S.averageSpeedToShow)?lapObject.getString(S.averageSpeedToShow):"");

                                    beanTimeLap.setRank((j + 1) + "");
                                    beanTimeLap.setBestLapTime(lapObject.getString(S.time));
                                    Random rnd = new Random();
                                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                                    beanTimeLap.setLapColor(color);
                                    userList.add(beanTimeLap);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                    paddockMyLapsParentModel.setMyLapsChildList(userList);
                    my_laps_list.add(paddockMyLapsParentModel);
                    /*if (trackObject!=null && !countryList.contains(trackObject.getString(S.country))) {
                        countryList.add(indexObject.getJSONObject(S.trackId).getString(S.country));
                        BeanVehicleType beanVehicleType = new BeanVehicleType();
                        beanVehicleType.setId(indexObject.getJSONObject(S.trackId).getString(S.country));
                        beanVehicleType.setName(indexObject.getJSONObject(S.trackId).getString(S.country));
                        countryName.add(beanVehicleType);
                    }*/
                }
                getListData();

            }
            getListData();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void getListData() {
        if(my_laps_list.isEmpty()){
            no_data_tv.setVisibility(View.VISIBLE);
        }
        else {
            paddockMyLapsAdapter.notifyDataSetChanged();
            setListViewHeight(expandable_list_my_laps, -1);
            no_data_tv.setVisibility(View.GONE);
        }

    }
}
