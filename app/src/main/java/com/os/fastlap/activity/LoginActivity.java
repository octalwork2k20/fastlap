package com.os.fastlap.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.os.fastlap.R;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by abhinava on 7/10/2017.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private TextView signupTextTv;
    private ImageView crossIv;
    private EditText emailAddressEt;
    private EditText passwordEt;
    private TextView loginBtn;
    private TextView forgotPasswordTv;
    private TextView fbLoginBtn;
    private TextView gpLoginBtn;
    Context context;
    private CallbackManager callbackManager;
    String TAG = LoginActivity.class.getSimpleName();
    MyProgressDialog myProgressDialog;
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    AuthAPI authAPI;
    Dialog forgotDialog=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.login_activity);
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        checkRequestPermission();
        autoOnGps();
        initView();
        clickListner();

    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]
                    {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.READ_PHONE_STATE,


                    }, 1);
        } else {
            FastLapApplication.location = LocationServices.FusedLocationApi.getLastLocation(FastLapApplication.mGoogleApiClient);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkRequestPermission();
                //  GoNext();
            } else {
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.permission_message));
                finish();
            }
        }
    }

    private void autoOnGps() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS)
        {
            FastLapApplication.mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            FastLapApplication.mGoogleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
        } else {
            showGPSDisabledAlertToUser();
        }

    }
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.gps_alert_messsage))
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void initView() {
        signupTextTv = (TextView) findViewById(R.id.signup_text_tv);
        crossIv = (ImageView) findViewById(R.id.cross_iv);
        emailAddressEt = (EditText) findViewById(R.id.email_address_et);
        passwordEt = (EditText) findViewById(R.id.password_et);
        loginBtn = (TextView) findViewById(R.id.login_btn);
        forgotPasswordTv = (TextView) findViewById(R.id.forgot_password_tv);
        fbLoginBtn = (TextView) findViewById(R.id.fb_login_btn);
        gpLoginBtn = (TextView) findViewById(R.id.gp_login_btn);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        myProgressDialog = new MyProgressDialog(context);
        myProgressDialog.setCancelable(false);

    }

    private void clickListner() {
        signupTextTv.setOnClickListener(this);
        crossIv.setOnClickListener(this);
        forgotPasswordTv.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        fbLoginBtn.setOnClickListener(this);
        gpLoginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fb_login_btn:
                if(!Util.isNetworkAvailable(context)){
                    showInternetAlertDialog(context);
                    return;
                }
                fbLoginBtn.setEnabled(false);
                facebookLogin();
                break;
            case R.id.gp_login_btn:
                if(!Util.isNetworkAvailable(context)){
                    showInternetAlertDialog(context);
                    return;
                }
                gpLoginBtn.setEnabled(false);
                signIn();
                break;
            case R.id.signup_text_tv:
                if(!Util.isNetworkAvailable(context)){
                    showInternetAlertDialog(context);
                    return;
                }
                Intent intent = new Intent(context, SignupActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.cross_iv:
                finish();
                break;
            case R.id.login_btn:
                String email = emailAddressEt.getText().toString().trim();
                String password = passwordEt.getText().toString().trim();
                if (Validation.loginValidation(emailAddressEt, passwordEt, loginBtn, context))
                    try {
                        callManualLogin(email, password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                break;

            case R.id.forgot_password_tv:
                if(!Util.isNetworkAvailable(context)){
                    showInternetAlertDialog(context);
                    return;
                }
                ForgotpasswordDialog();
                break;
        }
    }

    private void ForgotpasswordDialog() {
        if(forgotDialog!=null && forgotDialog.isShowing())
            return;

        forgotDialog = new Dialog(context);
        forgotDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgotDialog.setContentView(R.layout.forgotpassword);

        ImageView backIv;
        final EditText emailEt;
        final TextView submitBtn;

        backIv = forgotDialog.findViewById(R.id.back_iv);
        emailEt = forgotDialog.findViewById(R.id.email_et);
        submitBtn = forgotDialog.findViewById(R.id.submit_btn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                String email = emailEt.getText().toString().trim();
                if (Validation.forgotPasswordValidation(emailEt, submitBtn, context)) {
                    //forgotDialog.dismiss();
                    authAPI.forgotPassword(context, email);

                }
            }
        });

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotDialog.dismiss();
            }
        });

        forgotDialog.setCancelable(true);
        forgotDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        forgotDialog.getWindow().setGravity(Gravity.CENTER);
        forgotDialog.show();
    }

    // call webservice for user login
    private void callManualLogin(String email, String password) throws JSONException {
        try {
            authAPI.login(context, email, password, S.android_provider, Util.getDeviceToken(context), Util.getDeviceSerial(mContext), Util.getDeviceModel(), Util.getDeviceManufacture(), Util.getDeviceBrand(), Util.getDeviceVersionCode(), S.android_provider, Util.getDeviceToken(context));
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void startOtpScreen(JSONObject data) {
        Intent intent = new Intent(context, OTPActivity.class);
        intent.putExtra(S.sign_up_data, data.toString());
        startActivity(intent);
        finish();
    }

    // forgot password webservice success response
    public void getForgotPasswordResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);

            if(jsonObject.getInt(S.status) == S.adi_status_success){
                if(forgotDialog!=null){
                    forgotDialog.dismiss();
                }
                Util.showAlertDialog(context, getString(R.string.app_name), msg);
            }
            else {
                Util.showAlertDialog(context, getString(R.string.app_name), msg);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // login webservice success response
    public void getResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            Log.e("tag","login response "+jsonObject);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = dataJson.getJSONObject(0);
                int mobile_verify_status = jsonObject1.getInt(S.mobile_verify);
                if (mobile_verify_status == I.MOBILE_VERIFY) {
                    Util.saveDataInLocalStorage(jsonObject1, context);
                    finish();
                } else
                    startOtpScreen(jsonObject1);
            } else
                Util.showAlertDialog(context, getString(R.string.app_name), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // social signup webservice response
    public void getSocialSignupWebserviceResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataJson = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = dataJson.getJSONObject(0);
                Util.saveDataInLocalStorage(jsonObject1, context);
                finish();
            } else
                Util.showAlertDialog(context, getString(R.string.app_name), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void facebookLogin() {
        if (AccessToken.getCurrentAccessToken() == null)
        {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday", "user_photos", "user_location"));
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            String accessToken = loginResult.getAccessToken().getToken();
                            Log.i("accessToken", accessToken);
                            fbLoginBtn.setEnabled(true);
                            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    Log.i(TAG, response.toString());
                                    Log.i(TAG, object + "");
                                    try {
                                        String id = object.getString("id");
                                        String birthday = "";
                                        if (object.has("birthday")) {
                                            birthday = object.getString("birthday");
                                        }
                                        String cover = "";
                                        if (object.has("cover")) {
                                            cover = object.getJSONObject("cover").getString("source");
                                        }
                                        String location = "";
                                        if (object.has("location")) {
                                            location = object.getJSONObject("location").getString("name");
                                        }
                                        String about = "";
                                        if (object.has("about")) {
                                            about = object.getString("about");
                                        }
                                        String email = "";
                                        if (object.has("email")) {
                                            email = object.getString("email");
                                        }
                                        callSocialLoginWebservice(object.getString("first_name"), object.getString("last_name"), email, Util.getFbProfileImageUrl(id), id, S.social_login_type_fb, birthday, cover, location, about);
                                    } catch (Exception e) {
                                        Log.e(TAG, e.toString());
                                    }

                                }
                            });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,about,birthday,cover,location,email,first_name,gender,last_name"); // Parámetros que pedimos a facebook
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            fbLoginBtn.setEnabled(true);
                            LoginManager.getInstance().logOut();
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            Log.e("exception", exception.toString());
                            fbLoginBtn.setEnabled(true);
                        }
                    });

        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                fbLoginBtn.setEnabled(true);

            }
        }).executeAsync();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        Log.d("statuscode", result.getStatus().toString());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //String name = "";
            if (acct != null) {
                if (acct.getDisplayName() != null) {
                    String[] name = acct.getDisplayName().split(" ");
                    String fName = name[0];
                    String lName = name[1];
                    String url = "";
                    if (acct.getPhotoUrl() != null) {
                        url = acct.getPhotoUrl().toString();
                    }
                    try {
                        callSocialLoginWebservice(fName, lName, acct.getEmail(), url, acct.getId(), S.social_login_type_google, "", "", "", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void callSocialLoginWebservice(final String fname, final String lname, String email, final String url, final String social_id, final String social_login_type, final String birthday, final String cover, final String location, final String about) throws JSONException {
        if (TextUtils.isEmpty(email)) {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.forgotpassword);

            ImageView backIv;
            final EditText emailEt;
            final TextView submitBtn;
            TextView titleTv;

            backIv = dialog.findViewById(R.id.back_iv);
            emailEt = dialog.findViewById(R.id.email_et);
            submitBtn = dialog.findViewById(R.id.submit_btn);
            titleTv = dialog.findViewById(R.id.titleTv);

            titleTv.setText(getString(R.string.enter_email));

            submitBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyboard(v);
                    String email = emailEt.getText().toString().trim();
                    if (Validation.forgotPasswordValidation(emailEt, submitBtn, context)) {
                        dialog.dismiss();
                        authAPI.socialLogin(context, fname, lname, email,fname, "599472df9eb8056c27000029", "", Util.getLatitude(), Util.getLongitude(), S.android_provider, Util.getDeviceToken(context), email, social_id, social_login_type, Util.getUniquePassword(), url, Util.getDeviceSerial(mContext), Util.getDeviceModel(), Util.getDeviceManufacture(), Util.getDeviceBrand(), Util.getDeviceVersionCode(), S.android_provider, Util.getDeviceToken(context), birthday, cover, location, about);

                    }
                }
            });
            backIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.setCancelable(true);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.show();

        } else {
            authAPI.socialLogin(context, fname, lname, email,fname, "599472df9eb8056c27000029", "", Util.getLatitude(), Util.getLongitude(), S.android_provider, Util.getDeviceToken(context), email, social_id, social_login_type, Util.getUniquePassword(), url, Util.getDeviceSerial(mContext), Util.getDeviceModel(), Util.getDeviceManufacture(), Util.getDeviceBrand(), Util.getDeviceVersionCode(), S.android_provider, Util.getDeviceToken(context), birthday, cover, location, about);
        }


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void showInternetAlertDialog(final Context context) {
        // Toast.makeText(context, "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(getResources().getString(R.string.please_check_internat_connection))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}
