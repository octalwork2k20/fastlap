package com.os.fastlap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.os.fastlap.R;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.data.database.DatabaseManager;
import com.os.fastlap.delegates.NavigationDelegate;
import com.os.fastlap.fragment.settings.BlockUserFragment;
import com.os.fastlap.fragment.settings.ChangeEmailFragment;
import com.os.fastlap.fragment.settings.ChangePasswordFragment;
import com.os.fastlap.fragment.settings.DeactivateAccountFragment;
import com.os.fastlap.fragment.settings.DefaultMapviewFragment;
import com.os.fastlap.fragment.settings.ManageDeviceFragment;
import com.os.fastlap.fragment.settings.MetricSystemFragment;
import com.os.fastlap.fragment.settings.SettingGeneralInfoFragment;
import com.os.fastlap.fragment.settings.TermsConditionFragment;
import com.os.fastlap.fragment.settings.TimeSystemFragment;
import com.os.fastlap.fragment.settings.VehicleSettingFragment;
import com.os.fastlap.fragment.settings.WeekStartDayFragment;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by abhinava on 7/27/2017.
 */

public class SettingMenuActivity extends BaseActivity implements NavigationDelegate, View.OnClickListener {
    public String TAG = "SettingMenuActivity.java";
    public FragmentManager mFragmentManager = null;
    FragmentTransaction fragmentTransaction = null;
    public NavigationDelegate mNavigationDelegate;
    Fragment mFragment = null;
    public Context mContext;
    ImageView base_toggle_icon;
    AuthAPI authAPI;
    private DatabaseManager databaseManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_menu_activity);

        databaseManager = DatabaseManager.getInstance(this);
        mFragmentManager = getSupportFragmentManager();
        mNavigationDelegate = (NavigationDelegate) this;
        mContext = this;
        FastLapApplication.mCurrentContext = mContext;
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);

        String getPage = getIntent().getStringExtra("getPage");

        if (getPage != null) {
            displayView(getPage, getIntent().getStringExtra("from"), null);
        } else {
            displayView(FastLapConstant.SETTINGGENERALINFOFRAGMENT, null, null);
        }
        authAPI = new AuthAPI(mContext);
        clickListner();
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
    }

    public void displayView(String fragmentName, Object obj, Object list) {
        fragmentTransaction = mFragmentManager.beginTransaction();

        if (fragmentName.equals(FastLapConstant.SETTINGGENERALINFOFRAGMENT)) {
            mFragment = SettingGeneralInfoFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.general_info));
        }

        if (fragmentName.equals(FastLapConstant.CHANGEPASSWORDFRAGMENT)) {
            mFragment = ChangePasswordFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.change_password));
        }

        if (fragmentName.equals(FastLapConstant.CHANGEEMAILFRAGMENT)) {
            mFragment = ChangeEmailFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.change_email));
        }

        if (fragmentName.equals(FastLapConstant.VEHICLESETTINGFRAGMENT)) {
            mFragment = VehicleSettingFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.vehicle_setting));
        }

        if (fragmentName.equals(FastLapConstant.BLOCKUSERFRAGMENT)) {
            mFragment = BlockUserFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.block_user));
        }
        if (fragmentName.equals(FastLapConstant.MANAGEDEVICEFRAGMENT)) {
            mFragment = ManageDeviceFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.manage_device_connected));
        }
        if (fragmentName.equals(FastLapConstant.DEFAULTMAPVIEWFRAGMENT)) {
            mFragment = DefaultMapviewFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.default_maps_view));
        }
        if (fragmentName.equals(FastLapConstant.TIMESYSTEMFRAGMENT)) {
            mFragment = TimeSystemFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.time_system));
        }
        if (fragmentName.equals(FastLapConstant.WEEKSTARTDAYFRAGMENT)) {
            mFragment = WeekStartDayFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.week_start_day));
        }
        if (fragmentName.equals(FastLapConstant.METRICSYSTEMFRAGMENT)) {
            mFragment = MetricSystemFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.metric_system));
        }
        if (fragmentName.equals(FastLapConstant.DEACTIVATEACCOUNTFRAGMENT)) {
            mFragment = DeactivateAccountFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.deactivate_your_account));
        }
        if (fragmentName.equals(FastLapConstant.TERMSCONDITIONFRAGMENT)) {
            mFragment = TermsConditionFragment.newInstance(mContext, obj);
            setToolbarHeading(getString(R.string.terms_and_conditions));
        }
        if (mFragment != null) {
            try {
                fragmentTransaction.replace(R.id.container_fl, mFragment).addToBackStack(fragmentName);
                fragmentTransaction.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public void executeFragment(String fragmentName, Object obj) {

    }
    @Override
    public void goBack() {

    }

    // here we receive change password webservice response
    public void getChangePasswordResponse(String response) {
        if (mFragment instanceof ChangePasswordFragment)
            ((ChangePasswordFragment) mFragment).getChangePasswordResponse(response);
    }

    // here we receive change email webservice response
    public void getChangeEmailResponse(String response) {
        if (mFragment instanceof ChangeEmailFragment)
            ((ChangeEmailFragment) mFragment).getChangeEmailResponse(response);
    }

    /* userDeviceConnected webservice response */
    public void userDeviceConnectedResponse(String response) {
        if (mFragment instanceof ManageDeviceFragment)
            ((ManageDeviceFragment) mFragment).userDeviceConnectedResponse(response);
    }

    /* deviceDisconnectById webservice response */
    public void deviceDisconnectByIdResponse(String response) {
        if (mFragment instanceof ManageDeviceFragment)
            ((ManageDeviceFragment) mFragment).deviceDisconnectByIdResponse(response);
    }

    /* deviceDisconnect webservice response */
    public void deviceDisconnect(String response) {
        if (mFragment instanceof ManageDeviceFragment)
            ((ManageDeviceFragment) mFragment).deviceDisconnect(response);
    }

    /* user friends list webservice response */
    public void myFriendListWebserviceResponse(String response) {
        if (mFragment instanceof BlockUserFragment)
            ((BlockUserFragment) mFragment).myFriendListWebserviceResponse(response);
    }

    /* Handle Callback for Disconnect device webservice  */
    public void callbackDisconnectedResponse() {
        if (mFragment instanceof ManageDeviceFragment)
            ((ManageDeviceFragment) mFragment).onDiscoonectedComplete();
    }

    /* Handle Callback for Disconnect device webservice  */
    public void callbackDisconnectedAllDeviceResponse() {
        if (mFragment instanceof ManageDeviceFragment)
            ((ManageDeviceFragment) mFragment).onDiscoonectedComplete();
    }

    /*  Handle Callback for  User Block status*/
    public void callbackUserBlockResponse() {
        if (mFragment instanceof BlockUserFragment)
            ((BlockUserFragment) mFragment).refreshUserList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                finish();
                break;
        }
    }

    // get Static Page Webservice response
    public void getStaticPageResponse(String response) {
        if (mFragment instanceof TermsConditionFragment)
            ((TermsConditionFragment) mFragment).getPageInformation(response);
    }

    // getNationality webservice success
    public void getNationalityWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                if (mFragment instanceof SettingGeneralInfoFragment)
                    ((SettingGeneralInfoFragment) mFragment).receiveNationalityResponse(jsonArray);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // get UserInfo webservice success
    public void getUserAboutResponse(String response) {
        if (mFragment instanceof SettingGeneralInfoFragment)
            ((SettingGeneralInfoFragment) mFragment).getUserAboutResponse(response);
    }

    /* myFriendBlockList webservice response */
    public void myFriendBlockListResponse(String response) {
        if (mFragment instanceof BlockUserFragment)
            ((BlockUserFragment) mFragment).myFriendBlockListResponse(response);
    }

    /* userBlockStatus webservice response */
    public void userBlockStatusResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(mContext, getString(R.string.alert), msg, S.userBlockStatus_api);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
      /*  if(mFragment instanceof BlockUserFragment)
            ((BlockUserFragment)mFragment)*/
    }

    // get update info webservice success
    public void getUpdateInfoResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(mContext, getString(R.string.alert), getResources().getString(R.string.updated_sucessfuly));
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // get update info webservice success
    public void getUpdatetimeSystemResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(mContext, getString(R.string.alert),getResources().getString(R.string.updated_sucessfuly));

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // get update metric info webservice success
    public void getUpdateMetricSystemResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // get update Week start day webservice success
    public void getUpdateWeekStartDayResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {

                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    public void accountDeactivate(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                authAPI.logout(mContext, MySharedPreferences.getPreferences(mContext, S.user_id), Util.getDeviceToken(mContext));

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // logout webservice response
    public void getLogoutWebserviceResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                databaseManager.clearAllDatabaseTable();
                //  Util.showAlertDialog(context, getString(R.string.alert), msg);
                String isFirstTime=MySharedPreferences.getPreferences(mContext,S.isFirstTime);
                String language=MySharedPreferences.getPreferences(mContext,S.language);
                MySharedPreferences.clearAll(mContext);

                MySharedPreferences.setPreferences(mContext,isFirstTime,S.isFirstTime);
                MySharedPreferences.setPreferences(mContext,language,S.language);
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void accountDeactivate() {
        authAPI.setAccountDeactivate(mContext, MySharedPreferences.getPreferences(mContext, S.user_id));
    }

    // get update info webservice success
    public void getDefaultMapViewResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {

                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void vehicleTypeWebserviceResponse(String response)
    {
        if (mFragment instanceof VehicleSettingFragment)
            ((VehicleSettingFragment) mFragment).vehicleTypeWebserviceResponse(response);
    }

    public void getUpdateVehicleTypeResponse(String responseBody)
    {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {

                Util.showAlertDialog(mContext, getString(R.string.alert), msg);

            } else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
