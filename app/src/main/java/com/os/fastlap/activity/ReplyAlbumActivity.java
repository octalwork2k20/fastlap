package com.os.fastlap.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.adapter.AlbumReplyAdapter;
import com.os.fastlap.beans.dashboardmodals.ReplyBeans;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SharingDeleget;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/*
 * Created by monikab on 1/8/2016.
 */
public class ReplyAlbumActivity extends BaseActivity implements SharingDeleget, View.OnClickListener, AlbumReplyAdapter.ReplyInterFace {

    RecyclerView replyRecclerView;
    AlbumReplyAdapter dashboardReplyAdapter;
    EditText add_comment_et;
    ImageButton send_btn;
    String comment_text = "";
    Context context;
    MyProgressDialog myProgressDialog;
    ArrayList<ReplyBeans> reply_list;
    private String image_id = "";
    public static SharingDeleget mSharingDeleget;
    ImageView base_toggle_icon;
    String post_id;
    String post_comment_id;
    AuthAPI authAPI;
    private String TAG = ReplyAlbumActivity.class.getSimpleName();
    TextView no_data_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layout_reply);
        mSharingDeleget = this;
        context = this;
        FastLapApplication.mCurrentContext = context;
        authAPI = new AuthAPI(context);
        initView();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            post_comment_id = extras.getString(S.userPostCommentId);
            post_id = extras.getString(S.postId);
        }
    }

    private void initView() {
        replyRecclerView = (RecyclerView) findViewById(R.id.reply_list);
        add_comment_et = (EditText) findViewById(R.id.add_comment_et);
        send_btn = (ImageButton) findViewById(R.id.send_btn);
        base_toggle_icon = (ImageView) findViewById(R.id.base_toggle_icon);
        no_data_tv = (TextView) findViewById(R.id.no_data_tv);

        myProgressDialog = new MyProgressDialog(context);
        myProgressDialog.setCancelable(false);

        reply_list = new ArrayList<>();
        reply_list.clear();

        dashboardReplyAdapter = new AlbumReplyAdapter(reply_list, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        replyRecclerView.setLayoutManager(layoutManager);
        replyRecclerView.setItemAnimator(new DefaultItemAnimator());
        replyRecclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        replyRecclerView.setAdapter(dashboardReplyAdapter);
        dashboardReplyAdapter.setOnItemClickListener(this);

        clickListner();
    }

    private void clickListner() {
        base_toggle_icon.setOnClickListener(this);
        send_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.base_toggle_icon:
                onBackPressed();
                break;

            case R.id.send_btn:
                comment_text = add_comment_et.getText().toString();
                byte[] data = new byte[0];
                try {
                    data = comment_text.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment_text = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment_text.length() > 0) {
                    add_comment_et.setText("");
                    authAPI.albumCommentReply(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment_text, post_comment_id, "", "0");

                }
                break;
        }
    }

    @Override
    public void facebookSharing(String title, String desc, String image) {

    }

    @Override
    public void gmailSharing(String title, String desc, String image) {

    }

    @Override
    public void twitterSharing(String title, String desc, String image) {

    }

    @Override
    public void onDeletePost(int pos) {
        reply_list.remove(pos);
        dashboardReplyAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbarHeading(getString(R.string.replies));
        postCommentReplyListing();
        FastLapApplication.mCurrentContext = context;

    }

    protected void postCommentReplyListing() {
        authAPI.getAlbumCommentReplyListing(context, MySharedPreferences.getPreferences(context, S.user_id), post_id, post_comment_id);
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "");
        setResult(Activity.RESULT_OK, returnIntent);
        super.onBackPressed();
    }

    // Post Comment Reply response
    public void postCommentReplyResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentReplyListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // post comment reply listing response
    public void postCommentReplyListingResponse(String response) {
        reply_list.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ReplyBeans replyBeans = new ReplyBeans();
                    replyBeans.setReplyId(jsonObject1.getString(S._id));
                    replyBeans.setReplyDateTime(jsonObject1.getString(S.created));
                    replyBeans.setPostId(jsonObject1.getString(S.albumId));
                    replyBeans.setPostCommentId(jsonObject1.getString(S.userAlbumCommentId));

                    replyBeans.setReplyText(jsonObject1.getString(S.comment));
                    replyBeans.setReplyStatus(jsonObject1.getString(S.status));
                    replyBeans.setReplyCountLikeData(jsonObject1.getString(S.countLikeData));
                    replyBeans.setReplyLikedCommentReply(jsonObject1.getString(S.likedCommentReply));

                    replyBeans.setReplyUserId(jsonObject1.getJSONObject(S.userId).getString(S._id));
                    replyBeans.setReplyUserName(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.lastName_response));
                    replyBeans.setReplyUserImage(jsonObject1.getJSONObject(S.userId).getJSONObject(S.personalInfo).getString(S.image));

                    //likedCommentReply
                    reply_list.add(replyBeans);
                }
                dashboardReplyAdapter.notifyDataSetChanged();

                if (reply_list.size() > 0) {
                    no_data_tv.setVisibility(View.GONE);
                    replyRecclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data_tv.setVisibility(View.VISIBLE);
                    replyRecclerView.setVisibility(View.GONE);
                }


            } else {
                //Util.showAlertDialog(context, getString(R.string.alert), msg);
                reply_list.clear();
                dashboardReplyAdapter.notifyDataSetChanged();
                no_data_tv.setVisibility(View.VISIBLE);
                replyRecclerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            reply_list.clear();
            dashboardReplyAdapter.notifyDataSetChanged();
            no_data_tv.setVisibility(View.VISIBLE);
            replyRecclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClickReplyRowLike(int postion, String status) {
        authAPI.albumCommentReplyLike(context, MySharedPreferences.getPreferences(context, S.user_id), post_id, post_comment_id, reply_list.get(postion).getReplyId(), status);
    }

    @Override
    public void onClickReplyDelete(int postion) {
        Util.showAlertDialogWithTwoButton(context, getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.cancel), getString(R.string.delete_small), postion);
    }

    public void postCommentReplyLikeResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentReplyListing();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void callDeleteReply(int position) {
        authAPI.albumReplyRemove(context, MySharedPreferences.getPreferences(context, S.user_id), reply_list.get(position).getReplyId());
    }

    public void postCommentReplyDeleteResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                postCommentReplyListing();
                Util.showAlertDialogWithAction(context, getString(R.string.alert), msg, S.postCommentRemove_api);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClickReplyEdit(final int postion) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dashboard_comment_edit);

        AppCompatImageView baseToggleIcon;
        ImageView commetRowUserIv;
        TextViewPlayBold usernameTv;
        TextViewPlayRegular commentTv;
        final EditTextPlayRegular etComment;
        TextViewPlayBold updateBtn;
        TextViewPlayBold cancelBtn;

        baseToggleIcon = (AppCompatImageView) dialog.findViewById(R.id.base_toggle_icon);
        commetRowUserIv = (ImageView) dialog.findViewById(R.id.commet_row_user_iv);
        usernameTv = (TextViewPlayBold) dialog.findViewById(R.id.username_tv);
        commentTv = (TextViewPlayRegular) dialog.findViewById(R.id.comment_tv);
        etComment = (EditTextPlayRegular) dialog.findViewById(R.id.etComment);
        updateBtn = (TextViewPlayBold) dialog.findViewById(R.id.update_btn);
        cancelBtn = (TextViewPlayBold) dialog.findViewById(R.id.cancel_btn);

        usernameTv.setText(MySharedPreferences.getPreferences(context, S.firstName_response) + " " + MySharedPreferences.getPreferences(context, S.lastName_response));
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + MySharedPreferences.getPreferences(context, S.image), commetRowUserIv, Util.getImageLoaderOption(context));

        byte[] data = Base64.decode(reply_list.get(postion).getReplyText(), Base64.NO_WRAP);
        try {
            String text = new String(data, "UTF-8");
            commentTv.setText(text);
            etComment.setText(text);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_text = etComment.getText().toString().trim();
                byte[] data = new byte[0];
                try {
                    data = comment_text.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                comment_text = Base64.encodeToString(data, Base64.NO_WRAP);
                if (comment_text.length() > 0) {
                    etComment.setText("");
                    authAPI.albumCommentReply(context, post_id, MySharedPreferences.getPreferences(context, S.user_id), comment_text, post_comment_id, reply_list.get(postion).getReplyId(), "1");
                    dialog.cancel();
                }
            }
        });

        baseToggleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }
}
