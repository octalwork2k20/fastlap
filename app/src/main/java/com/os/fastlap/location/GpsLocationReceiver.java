package com.os.fastlap.location;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.os.fastlap.activity.RecordCameraActivity;

public class GpsLocationReceiver extends BroadcastReceiver implements LocationListener {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        try {
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                Log.e("tag","current activity"+getCurrentActivity(context));
                if(getCurrentActivity(context).equalsIgnoreCase("com.os.fastlap.activity.RecordCameraActivity")){
                    RecordCameraActivity.getRecordInstance().CheckGpsStatus();
                }
                // react on GPS provider change action
                Log.e("tag","onProviderEnabled"+intent);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        Log.e("tag","onProviderEnabled"+s);
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.e("tag","onProviderDisabled"+s);
    }

    private String getCurrentActivity(Context context){
        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        return  cn.getClassName();
    }
}

