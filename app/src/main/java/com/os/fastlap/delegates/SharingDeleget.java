package com.os.fastlap.delegates;

/**
 * Created by jitendrav on 7/9/2015.
 */
public interface SharingDeleget {
    public void facebookSharing(String title, String desc, String image);

    public void gmailSharing(String title, String desc, String image);

    public void twitterSharing(String title, String desc, String image);

    public void onDeletePost(int pos);
}
