package com.os.fastlap.delegates;

public interface ActivityCommunicator {
    public void passDataToActivity(String someValue);
}
