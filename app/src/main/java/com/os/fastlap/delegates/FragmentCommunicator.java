package com.os.fastlap.delegates;

public interface FragmentCommunicator {
    public void passDataToFragment(String someValue);
}