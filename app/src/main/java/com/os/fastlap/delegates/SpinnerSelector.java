package com.os.fastlap.delegates;

/**
 * Created by abhinava on 8/4/2017.
 */

public abstract class SpinnerSelector implements SpinnerSelectorInterface
{
    public void vehicleTypeName(String someValue) {

    }

    public void vehicleBrandName(String someValue) {

    }

    public void vehicleModelName(String someValue) {

    }

    public void vehicleVersionName(String someValue) {

    }

    public void vehicleYearName(String someValue) {

    }

    public void TyreBrandName(String someValue) {

    }

    public void TyreModelName(String someValue) {

    }
}
