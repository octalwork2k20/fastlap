package com.os.fastlap.delegates;

public interface FragmentCommunicatorTwo {
    public void passDataToFragment(String someValue);
}