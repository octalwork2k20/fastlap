package com.os.fastlap.delegates;

/**
 * Created by jitendrav on 7/9/2015.
 */
public interface OnImagePickerDialogSelect
{
    public void OnCameraSelect();
    public void OnGallerySelect();
    void onVideoSelect();

}
