package com.os.fastlap.fragment.community;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.community.CommunityMoreInfoActivity;
import com.os.fastlap.adapter.MemberListAdapter;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.beans.dashboardmodals.LikeUserBean;
import com.os.fastlap.util.Util;

import java.util.ArrayList;

/**
 * Created by anandj on 7/20/2017.
 */

public class InfoFragment extends Fragment {
    Context context;
    private ImageView imgCalIcon;
    private TextView txtDate;
    private TextView txtDateDetails;
    private ImageView imgLocationIcon;
    private TextView txtCircuit;
    private TextView txtCircuitDetails;
    private ImageView imgGroupIcon;
    private TextView txtGroup;
    private TextView txtPilotsCount;
    private TextView txtDets;
    private TextView txtFriends;
    private TextView txtFriendsCount;
    private TextView txtFriendsOutOf;
    private RecyclerView recyclerView;
    private ImageView imgOrganizedIcon;
    private TextView txtOrganized;
    private TextView txtCarName;
    private TextView txtClubName;
    private TextView amountTv;
    private TextView detailTv;
    private RelativeLayout rrDollorAmt;
    private ImageView imgDollorIcon;
    private View view;
    MemberListAdapter memberAdapter;
    ArrayList<LikeUserBean> friend_list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.community_fragment_event_info, container, false);
        context = getActivity();
        initView(view);
        return view;
    }

    private void initView(View view) {
        imgCalIcon = (ImageView) view.findViewById(R.id.img_cal_icon);
        txtDate = (TextView) view.findViewById(R.id.txt_date);
        txtDateDetails = (TextView) view.findViewById(R.id.txt_date_details);
        imgLocationIcon = (ImageView) view.findViewById(R.id.img_location_icon);
        txtCircuit = (TextView) view.findViewById(R.id.txt_circuit);
        txtCircuitDetails = (TextView) view.findViewById(R.id.txt_circuit_details);
        imgGroupIcon = (ImageView) view.findViewById(R.id.img_group_icon);
        txtGroup = (TextView) view.findViewById(R.id.txt_group);
        txtPilotsCount = (TextView) view.findViewById(R.id.txt_pilots_count);
        txtDets = (TextView) view.findViewById(R.id.txt_dets);
        txtFriends = (TextView) view.findViewById(R.id.txt_friends);
        txtFriendsCount = (TextView) view.findViewById(R.id.txt_friends_count);
        txtFriendsOutOf = (TextView) view.findViewById(R.id.txt_friends_out_of);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        imgOrganizedIcon = (ImageView) view.findViewById(R.id.img_organized_icon);
        txtOrganized = (TextView) view.findViewById(R.id.txt_organized);
        txtCarName = (TextView) view.findViewById(R.id.txt_car_name);
        txtClubName = (TextView) view.findViewById(R.id.txt_club_name);
        amountTv = (TextView) view.findViewById(R.id.amount_tv);
        detailTv = (TextView) view.findViewById(R.id.detail_tv);
        rrDollorAmt = (RelativeLayout) view.findViewById(R.id.rr_dollor_amt);
        imgDollorIcon = (ImageView) view.findViewById(R.id.img_dollor_icon);

        friend_list=new ArrayList<>();



        memberAdapter = new MemberListAdapter(context,friend_list);
        RecyclerView.LayoutManager mFavLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mFavLayoutManager);
        recyclerView.setAdapter(memberAdapter);
    }

    public void setinfoData(EventsBean eventsBean) {
        txtDate.setText(Util.ConvertDateTimeZoneDateMonth(eventsBean.getStartDate()) + " - " + Util.ConvertDateTimeZoneDateMonth(eventsBean.getEndDate()));
        txtDateDetails.setText(Util.ConvertDateTimeZoneDateMonth(eventsBean.getStartDate()) + " at " + Util.ConvertDateTimeZoneTime(eventsBean.getStartTime()) + " to " +
                Util.ConvertDateTimeZoneDateMonth(eventsBean.getEndDate()) + " at " + Util.ConvertDateTimeZoneTime(eventsBean.getEndTime()));
        txtCircuit.setText(eventsBean.getTrackName());
        txtCircuitDetails.setText(eventsBean.getLocation());

        txtCarName.setText(eventsBean.getGroupName());
        txtClubName.setText(eventsBean.getGroupDescription());

        detailTv.setText(eventsBean.getDescription());

        txtPilotsCount.setText(CommunityMoreInfoActivity.member_list.size() + "");
        txtFriendsOutOf.setText("/" + CommunityMoreInfoActivity.member_list.size());

        int count = 0;
        friend_list.clear();
        for (int i = 0; i < CommunityMoreInfoActivity.member_list.size(); i++)
        {
            if (CommunityMoreInfoActivity.member_list.get(i).getLike_user_friend_status().compareTo("1") == 0)
            {
                friend_list.add(CommunityMoreInfoActivity.member_list.get(i));
                count++;
            }
        }

        txtFriendsCount.setText(count + "");

        if (eventsBean.getPresetsList().size() > 0) {
            if (eventsBean.getPresetsList().get(0).getTypes().equalsIgnoreCase("1")) {
                amountTv.setText("$" + eventsBean.getPresetsList().get(0).getPrice() + " " + getString(R.string.for_single_entrance));
            } else {
                amountTv.setText("$" + eventsBean.getPresetsList().get(0).getPrice() + " " + getString(R.string.for_double_entrance));
            }
        }

        memberAdapter.notifyDataSetChanged();

    }
}
