package com.os.fastlap.fragment.paddock;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import static com.os.fastlap.R.id.viewpager;

/**
 * Created by anandj on 7/18/2017.
 */

@SuppressLint("ValidFragment")
public class PaddockMyLapsFragment extends Fragment {

    Context context;
    private TabLayout mTabHost;
    private ViewPager mViewpager;
    private ViewPagerAdapter viewPagerAdapter;
    private String TAG = PaddockMyLapsFragment.class.getSimpleName();
    AuthAPI authAPI;
    String trackVersionId;

    @SuppressLint("ValidFragment")
    public PaddockMyLapsFragment(final String trackVersionId) {
        this.trackVersionId=trackVersionId;
    }
    public PaddockMyLapsFragment() {
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mylaps_my_laps_fragment, container, false);
        context = getActivity();
        initView(view);
        authAPI = new AuthAPI(context);
        authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initView(View view) {

        mTabHost = (TabLayout) view.findViewById(R.id.tab_host);
        mViewpager = (ViewPager) view.findViewById(viewpager);

        setupViewPager(mViewpager);
        mTabHost.setupWithViewPager(mViewpager);

        mViewpager.setNestedScrollingEnabled(false);
        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    ((MyLapsActivity)context).showVehicleTypeDialog(context.getResources().getString(R.string.select_country),I.COUNTRYNAME);
                }else {
                    getCurrentFragment();
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        Util.changeTabsFont(mTabHost, getActivity());
    }

    private void setupViewPager(ViewPager viewpager) {
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(new PaddockMyLapsSubFragment(I.FAVORITETRACKS,trackVersionId), getString(R.string.favorites));
        viewPagerAdapter.addFragment(new PaddockMyLapsSubFragment(I.ALLTRACKS,trackVersionId), getString(R.string.all));
        viewPagerAdapter.addFragment(new PaddockMyLapsSubFragment(I.COUNTRYTRACKS,trackVersionId), getString(R.string.country));

        viewpager.setAdapter(viewPagerAdapter);
    }


    public Fragment getCurrentFragment()
    {
        Fragment mCurrentFragment=null;
        if(viewPagerAdapter!=null && mViewpager!=null)
        {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            ((PaddockMyLapsSubFragment)mCurrentFragment).getListData();
        }

        return mCurrentFragment;
    }

    public Fragment getCurrentFragment(String ch)
    {
        Fragment mCurrentFragment=null;
        if(viewPagerAdapter!=null && mViewpager!=null)
        {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            ((PaddockMyLapsSubFragment)mCurrentFragment).filter(ch);
        }

        return mCurrentFragment;
    }

    public Fragment getCurrentFragmentVehicle(String id)
    {
        Fragment mCurrentFragment=null;
        if(viewPagerAdapter!=null && mViewpager!=null)
        {
            mCurrentFragment = viewPagerAdapter.getRegisteredFragment(mViewpager.getCurrentItem());
            ((PaddockMyLapsSubFragment)mCurrentFragment).filterVehicle(id);
        }

        return mCurrentFragment;
    }
}
