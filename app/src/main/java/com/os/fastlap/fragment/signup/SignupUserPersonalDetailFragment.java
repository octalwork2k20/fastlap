package com.os.fastlap.fragment.signup;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.os.fastlap.R;
import com.os.fastlap.activity.LoginActivity;
import com.os.fastlap.activity.SignupActivity;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;

/**
 * Created by abhinava on 7/10/2017.
 */

public class SignupUserPersonalDetailFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private TextView loginTextTv;
    private ImageView crossIv;
    private EditText fullNameEt;
    private EditText nickNameEt;
    private EditText emailAddressEt;
    private EditText passwordEt;
    private EditText confirmPasswordEt;
    private TextView nextBtn;
    private EditText last_name_et;
    private EditText mobile_number_et;
    private CountryCodePicker countryCodePicker;
    private MyInterface listener;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if (context instanceof SignupActivity) {
            activity = (SignupActivity) context;
            listener = (MyInterface) activity;
        }
    }

    public static SignupUserPersonalDetailFragment newInstance(Context contex, Object obj) {
        SignupUserPersonalDetailFragment f = new SignupUserPersonalDetailFragment();
        context = contex;
        object = obj;

        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.signup_user_personal_detail, container, false);
            context = getActivity();
        }
        initView(rootview);

        return rootview;
    }


    private void initView(View rootview) {
        countryCodePicker=(CountryCodePicker)rootview.findViewById(R.id.countryCode);
        loginTextTv = (TextView) rootview.findViewById(R.id.login_text_tv);
        crossIv = (ImageView) rootview.findViewById(R.id.cross_iv);
        fullNameEt = (EditText) rootview.findViewById(R.id.full_name_et);
        nickNameEt = (EditText) rootview.findViewById(R.id.nick_name_et);
        emailAddressEt = (EditText) rootview.findViewById(R.id.email_address_et);
        passwordEt = (EditText) rootview.findViewById(R.id.password_et);
        confirmPasswordEt = (EditText) rootview.findViewById(R.id.confirm_password_et);
        nextBtn = (TextView) rootview.findViewById(R.id.next_btn);
        last_name_et = (EditText) rootview.findViewById(R.id.last_name_et);
        mobile_number_et = (EditText) rootview.findViewById(R.id.mobile_number_et);
        mobile_number_et.setSelection(mobile_number_et.getText().length());
        clickListner();
    }

    private void clickListner() {
        nextBtn.setOnClickListener(this);
        loginTextTv.setOnClickListener(this);
        crossIv.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn:
                Util.hideKeyboard(getActivity().getCurrentFocus(), getActivity());
                if (Validation.signupUserPersonalDetailsValidation(fullNameEt, last_name_et, nickNameEt, emailAddressEt, mobile_number_et, passwordEt, confirmPasswordEt, nextBtn, getActivity())) {
                    ((SignupActivity) getActivity()).displayView(FastLapConstant.SIGNUPUSEROTHERDETAILFRAGMENT, null, null);
                    if (listener != null) {
                        String nickname = nickNameEt.getText().toString();
                        if (TextUtils.isEmpty(nickname)) {
                            nickname = fullNameEt.getText().toString() + last_name_et.getText().toString();
                        }

                        listener.transferData(fullNameEt.getText().toString(), last_name_et.getText().toString(), nickname, emailAddressEt.getText().toString(), mobile_number_et.getText().toString(),
                                passwordEt.getText().toString(), confirmPasswordEt.getText().toString(),countryCodePicker.getSelectedCountryCodeAsInt()+"");
                    }
                }
                break;
            case R.id.login_text_tv:
                Util.startNewActivity(getActivity(), LoginActivity.class, false);
                getActivity().finish();
                break;
            case R.id.cross_iv:
                Util.startNewActivity(getActivity(), LoginActivity.class, false);
                getActivity().finish();
                break;
        }
    }
    public interface MyInterface {
        void transferData(String fullnmae, String lastName, String nickName, String email, String mobile, String password, String confirm_password,String countryCode);
    }
}
