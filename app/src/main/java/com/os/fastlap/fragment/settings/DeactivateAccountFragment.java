package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.customclass.TextViewPlayBold;

/**
 * Created by abhinava on 7/27/2017.
 */

public class DeactivateAccountFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private TextViewPlayBold deactiveAccountTv;
    AuthAPI authAPI;

    public static DeactivateAccountFragment newInstance(Context contex, Object obj) {
        DeactivateAccountFragment f = new DeactivateAccountFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_deactivate_account, container, false);
            context = getActivity();
        }
        initView(rootview);
        authAPI=new AuthAPI(context);

        return rootview;
    }


    private void initView(View rootview) {
        deactiveAccountTv = (TextViewPlayBold) rootview.findViewById(R.id.deactive_account_tv);

        clickListner();
    }

    private void clickListner()
    {
        deactiveAccountTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.deactive_account_tv:
                Util.showAlertDialogWithTwoButton(context, getString(R.string.are_you_sure_you_want_to_deactivate_your_account), getString(R.string.cancel), getString(R.string.ok), 0);

                break;
        }
    }

}
