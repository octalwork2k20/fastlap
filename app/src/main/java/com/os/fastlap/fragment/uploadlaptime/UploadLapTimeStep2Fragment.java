package com.os.fastlap.fragment.uploadlaptime;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.UploadLapTimeActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.uploadlaptime.UploadLapTimeStep2Adapter;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 7/10/2017.
 */

public class UploadLapTimeStep2Fragment extends Fragment implements View.OnClickListener, UploadLapTimeStep2Adapter.OnItemClickListener, SpinnerSelectorInterface {
    static Context context;
    static Object object;
    View rootview;
    private RecyclerView recyclerView;
    private TextView backTv;
    private TextView nextTv;
    AuthAPI authAPI;
    UploadLapTimeStep2Adapter uploadLapTimeStep2Adapter;
    private ArrayList<BeanVehicleType> vehicleTyreBrand_list;
    private ArrayList<BeanVehicleType> vehicleTyreModel_list;
    SpinnerSelectorInterface spinnerSelectorInterface;

    private String TAG = UploadLapTimeStep2Fragment.class.getSimpleName();
    int itemPosition = 0;


    public static UploadLapTimeStep2Fragment newInstance(Context contex, Object obj) {
        UploadLapTimeStep2Fragment f = new UploadLapTimeStep2Fragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.upload_lap_time_step2, container, false);
            context = getActivity();
        }
        authAPI = new AuthAPI(context);
        spinnerSelectorInterface = this;
        initView(rootview);
        ((UploadLapTimeActivity) context).setStep2Fragment();
        UploadLapTimeActivity.beanUploadData.setLapWeather(getString(R.string.rain));

        //getSessionList();

        return rootview;
    }

    private void initView(View rootview) {
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerView);

        backTv = (TextView) rootview.findViewById(R.id.back_tv);
        nextTv = (TextView) rootview.findViewById(R.id.next_tv);

        vehicleTyreBrand_list = new ArrayList<>();
        vehicleTyreModel_list = new ArrayList<>();

        uploadLapTimeStep2Adapter = new UploadLapTimeStep2Adapter(context, UploadLapTimeActivity.beanUploadData.getSelectedValue());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(uploadLapTimeStep2Adapter);
        uploadLapTimeStep2Adapter.setItemClickListener(this);

        clickListner();
    }

    private void clickListner() {
        nextTv.setOnClickListener(this);
        backTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_tv:
                String notes = "";
                boolean isValid = false;
                try {
                    for (int i = 0; i < UploadLapTimeActivity.beanUploadData.getSelectedValue().length(); i++)
                    {
                        isValid = false;
                        JSONObject jsonObject = new JSONObject();
                        jsonObject = UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(i);
                        if (TextUtils.isEmpty(jsonObject.getString("weather")))
                        {
                            Util.showSnackBar(nextTv,  getString(R.string.condition_is_required));
                            break;
                        } else if (TextUtils.isEmpty(jsonObject.getString("vehicleTyreBrandId"))) {
                            Util.showSnackBar(nextTv, getString(R.string.type_brand_is_required));
                            break;

                        } else if (TextUtils.isEmpty(jsonObject.getString("vehicleTyreModelId"))) {
                            Util.showSnackBar(nextTv,  getString(R.string.type_model_is_required));
                            break;

                        } else {
                            isValid = true;
                        }
                    }
                } catch (Exception e) {

                }
                if (isValid) {
                    ((UploadLapTimeActivity) getActivity()).displayView(FastLapConstant.UPLOADLAPTIMESTEP3FRAGMENT, null, null);
                }

                /*if (UploadLapTimeActivity.beanUploadData.getLapTyreBrandId().length() > 0) {
                    if (UploadLapTimeActivity.beanUploadData.getLapTyreModelId().length() > 0) {
                        UploadLapTimeActivity.beanUploadData.setLapNotes(notes);
                        ((UploadLapTimeActivity) getActivity()).displayView(FastLapConstant.UPLOADLAPTIMESTEP3FRAGMENT, null, null);
                    } else {
                        Util.showToast(context, getString(R.string.vehicle_tyremodel_empty_error));
                    }
                } else {
                    Util.showToast(context, getString(R.string.vehicle_tyrebrand_empty_error));
                }*/
                break;

            case R.id.back_tv:
                ((UploadLapTimeActivity) getActivity()).OnBackCross();
                break;
        }
    }

    @Override
    public void onWeatherClick(int position, String weather) {
        try {
            UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(position).put("weather", weather);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        uploadLapTimeStep2Adapter.notifyDataSetChanged();
    }

    @Override
    public void onTyreBrandClick(int position) {
        itemPosition = position;
        if(UploadLapTimeActivity.beanUploadData.getRentVehicle().equalsIgnoreCase("0")){
            authAPI.getVehicleTyreBrand(context, UploadLapTimeActivity.beanUploadData.getVehicleTypeId());
        }
        else {
            authAPI.getVehicleTyreBrand(context, UploadLapTimeActivity.beanUploadData.getRentVehicleTypeId());
        }

    }

    @Override
    public void onTyreModelClick(int position) {
        itemPosition = position;
        showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_model), vehicleTyreModel_list, I.TYRE_MODEL);
    }

    // Vehicle Tyre Brand Response
    public void getVehicleTyreBrandResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTyreBrand_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.brandName));
                    vehicleTyreBrand_list.add(beanVehicleType);
                }
                showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_brand), vehicleTyreBrand_list, I.TYRE_BRAND);
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Vehicle Tyre Model Response
    public void getVehicleTyreModelResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTyreModel_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehichleTyreModelName));
                    vehicleTyreModel_list.add(beanVehicleType);
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.TYRE_BRAND:
                try {
                    UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(itemPosition).put("vehicleTyreBrandId", id);
                    if(UploadLapTimeActivity.beanUploadData.getRentVehicle().equalsIgnoreCase("0")){
                        UploadLapTimeActivity.beanUploadData.setVehicleBrandName(value);
                    }
                    else {
                        UploadLapTimeActivity.beanUploadData.setRentedVehicleBrandName(value);
                    }

                  //  UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(itemPosition).put("vehicleTyreBrandName", value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(UploadLapTimeActivity.beanUploadData.getRentVehicle().equalsIgnoreCase("0")){
                    authAPI.getVehicleTyreModel(context, UploadLapTimeActivity.beanUploadData.getVehicleTypeId(), id);
                }
                else {
                    authAPI.getVehicleTyreModel(context, UploadLapTimeActivity.beanUploadData.getRentVehicleTypeId(), id);
                }

                uploadLapTimeStep2Adapter.notifyDataSetChanged();
                break;
            case I.TYRE_MODEL:
                try {
                    UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(itemPosition).put("vehicleTyreModelId", id);

                    if(UploadLapTimeActivity.beanUploadData.getRentVehicle().equalsIgnoreCase("0")){
                        UploadLapTimeActivity.beanUploadData.setVehicleModelName(value);
                    }
                    else {
                        UploadLapTimeActivity.beanUploadData.setRentedVehicleModelName(value);
                    }
                 //   UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(itemPosition).put("vehicleTyreModelName", value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                UploadLapTimeActivity.beanUploadData.setLapTyreModelId(id + "");
                uploadLapTimeStep2Adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }
}
