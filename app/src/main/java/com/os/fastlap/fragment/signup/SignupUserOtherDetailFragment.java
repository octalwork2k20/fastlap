package com.os.fastlap.fragment.signup;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.iid.FirebaseInstanceId;
import com.os.fastlap.R;
import com.os.fastlap.activity.LoginActivity;
import com.os.fastlap.activity.SettingMenuActivity;
import com.os.fastlap.activity.SignupActivity;
import com.os.fastlap.adapter.LanguageAdapter;
import com.os.fastlap.beans.LanguageBeans;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/*
 * Created by abhinava on 7/10/2017.
 */

public class SignupUserOtherDetailFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, LanguageAdapter.OnItemClickListener {
    static Context context;
    static Object object;
    View rootview;
    private TextView loginTextTv;
    private ImageView crossIv;
    private RelativeLayout dobLayout;
    TextView dobEt;
    private RelativeLayout languageLayout;
    private TextView chooseLanguageEt;
    private TextView locationEt;
    private RelativeLayout nationalityLayout;
    private TextView selectNationalityEt;
    private RelativeLayout locationLayout;

    private EditText enterVoucherCodeEt;
    private AppCompatCheckBox termsCheckbox;
    private TextView signupBtn;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    private String fName, lastName, nickName, email, mobile, password, confirm_password,countryCode;
    private ArrayList<LanguageBeans> mLanguageList;
    private LanguageAdapter mLanguageAdapter;
    private Dialog mBottomSheetDialog;
    AuthAPI authAPI;
    public static String TAG = "SignupUserOtherDetailFragment.java";
    ArrayList<String> stringArrayList=new ArrayList<>();
    SpinnerDialog locationSpinnerDialog;
    SpinnerDialog nationalitySpinnerDialog;
    String nationality="";
    String location="";

    public static SignupUserOtherDetailFragment newInstance(Context contex, Object obj) {
        SignupUserOtherDetailFragment f = new SignupUserOtherDetailFragment();
        context = contex;
        object = obj;
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.signup_user_other_detail, container, false);
            context = getActivity();
        }
        authAPI = new AuthAPI(getActivity());
        mLanguageList = new ArrayList<>();
        mLanguageAdapter = new LanguageAdapter(getActivity(), mLanguageList);
        mLanguageAdapter.setItemClickListener(this);
        initView(rootview);

        return rootview;
    }

    private void initView(View rootview) {
        loginTextTv = rootview.findViewById(R.id.login_text_tv);
        crossIv = rootview.findViewById(R.id.cross_iv);
        dobLayout = rootview.findViewById(R.id.dob_layout);
        dobEt = rootview.findViewById(R.id.dob_et);
        languageLayout = rootview.findViewById(R.id.language_layout);
        chooseLanguageEt = rootview.findViewById(R.id.choose_language_et);
        nationalityLayout = rootview.findViewById(R.id.nationality_layout);
        selectNationalityEt = rootview.findViewById(R.id.select_nationality_et);
        locationLayout = rootview.findViewById(R.id.location_layout);
        locationEt = rootview.findViewById(R.id.location_et);
        enterVoucherCodeEt = rootview.findViewById(R.id.enter_voucher_code_et);
        termsCheckbox = rootview.findViewById(R.id.terms_checkbox);
        signupBtn = rootview.findViewById(R.id.signup_btn);
        onDateSetListener = this;
        clickListner();

        AuthAPI authAPI = new AuthAPI(getContext());
        authAPI.getNationality(getContext());
        authAPI.getLanguage(getContext());
    }

    public void receiveNationalityResponse(JSONArray jsonArray) {
        try {
            stringArrayList=new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                stringArrayList.add(jsonObject.getString(S.name));
            }
            nationalitySpinnerDialog=new SpinnerDialog(getActivity(),stringArrayList,getResources().getString(R.string.search_nationality_and_select),getResources().getString(R.string.cancel));
            locationSpinnerDialog=new SpinnerDialog(getActivity(),stringArrayList,getResources().getString(R.string.search_location_and_select),getResources().getString(R.string.cancel));

            nationalitySpinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                @Override
                public void onClick(String item, int position) {
                    nationality=item;
                    selectNationalityEt.setText(item);
                }
            });

            locationSpinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                @Override
                public void onClick(String item, int position) {
                    location=item;
                    locationEt.setText(item);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    public void getLanguageResponse(JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                LanguageBeans languageBeans = new LanguageBeans();
                languageBeans.set_id(jsonObject.getString(S._id));
                languageBeans.setName(jsonObject.getString(S.name));
                languageBeans.setCode(jsonObject.getString(S.code));

                mLanguageList.add(languageBeans);
            }
            mLanguageAdapter.notifyDataSetChanged();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void clickListner() {
        loginTextTv.setOnClickListener(this);
        crossIv.setOnClickListener(this);
        dobLayout.setOnClickListener(this);
        languageLayout.setOnClickListener(this);
        nationalityLayout.setOnClickListener(this);
        locationLayout.setOnClickListener(this);
        locationLayout.setOnClickListener(this);
        signupBtn.setOnClickListener(this);
        termsCheckbox.setOnClickListener(this);
        selectNationalityEt.setOnClickListener(this);
        locationEt.setOnClickListener(this);



    }

    @SuppressLint("LongLogTag")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_text_tv:
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;

            case R.id.cross_iv:
                //  ((SignupActivity) getActivity()).displayView(FastLapConstant.SIGNUPUSERPERSONALDETAILFRAGMENT, null, null);
                ((SignupActivity) context).OnBackCross();
                break;
            case R.id.dob_layout:
                DatepickerFragment datePickerFragment = DatepickerFragment.newInstance(onDateSetListener);
                datePickerFragment.show(getActivity().getFragmentManager(), "date");
                break;
            case R.id.language_layout:
                selectLanguageDialog();
                break;
            case R.id.signup_btn:
                Util.hideKeyboard(getActivity().getCurrentFocus(), getActivity());
                if (Validation.signupUserDetailsValidation(dobEt, chooseLanguageEt, nationality,location, enterVoucherCodeEt, termsCheckbox, signupBtn, getActivity()))
                    callsignup();
                break;
            case R.id.terms_checkbox:
                Intent intent9 = new Intent(context, SettingMenuActivity.class);
                intent9.putExtra("getPage", FastLapConstant.TERMSCONDITIONFRAGMENT);
                intent9.putExtra("from", "signup");
                startActivity(intent9);
                break;
            case R.id.select_nationality_et:
                nationalitySpinnerDialog.showSpinerDialog();
                break;
            case R.id.location_et:
                locationSpinnerDialog.showSpinerDialog();
                break;
        }
    }
    public void getUserPersonalFragmentData(String fnmae, String lastName, String nickName, String email, String mobile, String password, String confirm_password,String countryCode) {
        this.fName = fnmae;
        this.lastName = lastName;
        this.nickName = nickName;
        this.email = email;
        this.mobile = mobile;
        this.password = password;
        this.confirm_password = confirm_password;
        this.countryCode=countryCode;
    }

    private void callsignup() {
        String lat, lng, device_token;
        lat = lng = device_token = "";
        if (FastLapApplication.location != null) {
            lat = String.valueOf(FastLapApplication.location.getLatitude());
            lng = String.valueOf(FastLapApplication.location.getLongitude());
        }

        if (MySharedPreferences.getPreferences(getActivity(), S.user_device_token) == null || S.user_device_token.isEmpty()) {
            if (FirebaseInstanceId.getInstance().getToken() != null)
                device_token = FirebaseInstanceId.getInstance().getToken();
        }
        else {
            device_token = MySharedPreferences.getPreferences(getActivity(), S.user_device_token);
        }
        try {
            authAPI.signup(getActivity(), fName, lastName, nickName, email, password,countryCode+" "+mobile, dobEt.getText().toString(), chooseLanguageEt.getTag().toString(), nationality,location, enterVoucherCodeEt.getText().toString(), lat, lng, S.android_provider, device_token);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }


    private void selectLanguageDialog() {
        if (mLanguageList.size() > 0) {
            mBottomSheetDialog = new Dialog(context, R.style.AlertDialogBottomSlide);
            mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LayoutInflater inflater = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View dialogView = inflater.inflate(R.layout.select_language_dialog, null);


            RecyclerView recycler_view = dialogView.findViewById(R.id.recycler_view);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recycler_view.setLayoutManager(layoutManager);
            recycler_view.setAdapter(mLanguageAdapter);


            mBottomSheetDialog.setContentView(dialogView); // your custom view.
            mBottomSheetDialog.setCancelable(true);
            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
            mBottomSheetDialog.show();
        } else {
            Util.showToast(context, getString(R.string.netword_issue));
        }
    }

    @Override
    public void onItemClick(int position) {
        chooseLanguageEt.setText(mLanguageList.get(position).getName());
        chooseLanguageEt.setTag(mLanguageList.get(position).get_id());
        mBottomSheetDialog.dismiss();
    }


    public static class DatepickerFragment extends DialogFragment {

        DatePickerDialog.OnDateSetListener onDateSetListener;

        public static DatepickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener) {
            DatepickerFragment datepickerFragment = new DatepickerFragment();
            datepickerFragment.onDateSetListener = onDateSetListener;
            return datepickerFragment;
        }

        public Dialog onCreateDialog(Bundle savedInstance) {

            // create Calendar Instance from Calendar class
            final Calendar calender = Calendar.getInstance();
            int year = calender.get(Calendar.YEAR);
            int month = calender.get(Calendar.MONTH);
            int day = calender.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dateFragment = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_DARK, onDateSetListener, year, month, day);
            dateFragment.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            return dateFragment;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
        setDate(cal);
    }

    public void setDate(Calendar calender) {
        Date current = calender.getTime();
        int diff1 = new Date().compareTo(current);
        if (diff1 < 0) {
            Util.showSnackBar(dobEt, getString(R.string.dob_valid_error));
        } else {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dobEt.setText(dateFormat.format(calender.getTime()));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == I.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                locationEt.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
