package com.os.fastlap.fragment.community;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.community.CommunityMoreInfoActivity;
import com.os.fastlap.adapter.dashboard.LikesUserAdapter;

/**
 * Created by anandj on 7/20/2017.
 */

public class PilotsFragment extends Fragment {

    private RecyclerView recyclerView;
    public LikesUserAdapter likesUserAdapter;
    Context context;
    TextView no_data_tv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_member_list_fragment, container, false);
        context = getActivity();
        initView(view);
        return view;
    }

    public void setinfoData() {
        likesUserAdapter.notifyDataSetChanged();

        if (CommunityMoreInfoActivity.member_list.size() > 0) {
            no_data_tv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            no_data_tv.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void initView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        no_data_tv = (TextView) view.findViewById(R.id.no_data_tv);

        likesUserAdapter = new LikesUserAdapter(CommunityMoreInfoActivity.member_list, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(likesUserAdapter);

        setinfoData();

    }
}
