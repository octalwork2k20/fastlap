package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.adapter.settings.ManageDeviceConnectedAdapter;
import com.os.fastlap.beans.SettingManageDeviceConnectedModal;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/*
 * Created by abhinava on 7/27/2017.
 */

public class ManageDeviceFragment extends Fragment implements View.OnClickListener, ManageDeviceConnectedAdapter.OnItemClickListener {
    static Context context;
    static Object object;
    View rootview;
    private RecyclerView managedeviceRecyclerView;
    private TextViewPlayBold disconnectAllTv;
    private AuthAPI authAPI;
    private ArrayList<SettingManageDeviceConnectedModal> mConnectedDevice;
    private ManageDeviceConnectedAdapter mManageDeviceConnectedAdapter;
    private final String TAG = ManageDeviceFragment.class.getSimpleName();

    public static ManageDeviceFragment newInstance(Context contex, Object obj) {
        ManageDeviceFragment f = new ManageDeviceFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_manage_device, container, false);
            context = getActivity();
        }
        initView(rootview);
        authAPI = new AuthAPI(getActivity());
        onDiscoonectedComplete();
        return rootview;
    }

    private void initView(View rootview) {
        managedeviceRecyclerView = (RecyclerView) rootview.findViewById(R.id.managedevice_recyclerView);
        disconnectAllTv = (TextViewPlayBold) rootview.findViewById(R.id.disconnect_all_tv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        managedeviceRecyclerView.setLayoutManager(layoutManager);
        managedeviceRecyclerView.setNestedScrollingEnabled(false);
        clickListner();
    }

    private void clickListner() {
        disconnectAllTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.disconnect_all_tv:
                authAPI.disconnectAllTv(getActivity(), MySharedPreferences.getPreferences(getActivity(), S.user_id), Util.getDeviceToken(getActivity()));
                break;
        }
    }

    /* Connected device listing */
    public void userDeviceConnectedResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            mConnectedDevice = new ArrayList<>();
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    SettingManageDeviceConnectedModal settingManageDeviceConnectedModal = new SettingManageDeviceConnectedModal();
                    settingManageDeviceConnectedModal.setId(jsonObject1.getString(S._id));
                    settingManageDeviceConnectedModal.setUserId(jsonObject1.getString(S.userId));
                    settingManageDeviceConnectedModal.setStatus(jsonObject1.getString(S.status));
                    settingManageDeviceConnectedModal.setDeviceToken(jsonObject1.getString(S.deviceToken));
                    settingManageDeviceConnectedModal.setDeviceType(jsonObject1.getString(S.deviceType));
                    settingManageDeviceConnectedModal.setVersioncode(jsonObject1.getString(S.versioncode));
                    settingManageDeviceConnectedModal.setBrand(jsonObject1.getString(S.brand));
                    settingManageDeviceConnectedModal.setManufacture(jsonObject1.getString(S.Manufacture));
                    settingManageDeviceConnectedModal.setMODEL(jsonObject1.getString(S.MODEL));
                    settingManageDeviceConnectedModal.setSERIAL(jsonObject1.getString(S.SERIAL));
                    mConnectedDevice.add(settingManageDeviceConnectedModal);
                }
            } else
                Util.showAlertDialog(getContext(), getString(R.string.alert), msg);
            if (mConnectedDevice.size() == 0)
                disconnectAllTv.setEnabled(false);
            else
                disconnectAllTv.setEnabled(true);

            mManageDeviceConnectedAdapter = new ManageDeviceConnectedAdapter(getActivity(), mConnectedDevice, this);
            managedeviceRecyclerView.setAdapter(mManageDeviceConnectedAdapter);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* Disconnect device response */
    public void deviceDisconnectByIdResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(getActivity(), getString(R.string.alert), msg, S.deviceDisconnectById_api);
            } else
                Util.showAlertDialog(getContext(), getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /* deviceDisconnect response */
    public void deviceDisconnect(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                Util.showAlertDialogWithAction(getActivity(), getString(R.string.alert), msg, S.deviceDisconnect_api);
            } else
                Util.showAlertDialog(getContext(), getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onDisconnectedDevice(int position) {
        authAPI.deviceDisconnectById(getActivity(), mConnectedDevice.get(position).getId(), MySharedPreferences.getPreferences(getActivity(), S.user_id));
    }

    public void onDiscoonectedComplete() {
        authAPI.userDeviceConnected(MySharedPreferences.getPreferences(getActivity(), S.user_id), Util.getDeviceToken(getActivity()), getActivity());
    }
}
