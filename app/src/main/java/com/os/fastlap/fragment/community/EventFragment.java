package com.os.fastlap.fragment.community;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.community.CommunityMoreInfoActivity;
import com.os.fastlap.activity.community.EventActivity;
import com.os.fastlap.activity.community.EventTicketPurchase;
import com.os.fastlap.adapter.community.CommunityEventRecyclerAdapter;
import com.os.fastlap.beans.EventsBean;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;

/**
 * Created by anandj on 7/19/2017.
 */

@SuppressLint("ValidFragment")
public class EventFragment extends Fragment implements View.OnClickListener, CommunityEventRecyclerAdapter.ClickListner {

    private RecyclerView recycler_view;
    private CommunityEventRecyclerAdapter communityEventRecyclerAdapter;
    private int current_view = I.GRIDVIEW;
    private ImageView listview_gridview;
    int type;
    ArrayList<EventsBean> arrayList;
    TextView no_data_tv;
    Context context;
    private String TAG = EventFragment.class.getSimpleName();

    public EventFragment(int type) {
        this.type = type;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.community_fragment_events_all, container, false);
        context = getActivity();
        initView(view);
        listview_gridview.setOnClickListener(this);
        return view;
    }

    private void initView(View view) {
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        listview_gridview = (ImageView) view.findViewById(R.id.listview_gridview);
        no_data_tv = (TextView) view.findViewById(R.id.no_data_tv);
        arrayList = new ArrayList<>();
        arrayList.clear();

        communityEventRecyclerAdapter = new CommunityEventRecyclerAdapter(getContext(), arrayList, I.GRIDVIEW);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_view.setAdapter(communityEventRecyclerAdapter);
        recycler_view.setNestedScrollingEnabled(false);
        communityEventRecyclerAdapter.setOnclickListner(this);
        communityEventRecyclerAdapter.AddCloneList(arrayList);

        getListData();
    }

    public void getListData() {
        arrayList.clear();
        if (type == I.SIGNEDUPEVENT) {
            arrayList.addAll(EventActivity.mArrayListSignedUp);
            communityEventRecyclerAdapter.AddCloneList(arrayList);

        } else if (type == I.FAVORITEEVENT) {
            arrayList.addAll(EventActivity.mArrayListFavoite);
            communityEventRecyclerAdapter.AddCloneList(arrayList);
        } else if (type == I.FRIENDSEVENT) {
            arrayList.addAll(EventActivity.mArrayListFriends);
            communityEventRecyclerAdapter.AddCloneList(arrayList);
        } else {
            arrayList.addAll(EventActivity.mArrayListAll);
            communityEventRecyclerAdapter.AddCloneList(arrayList);
        }
        communityEventRecyclerAdapter.notifyDataSetChanged();

        if (arrayList.size() > 0) {
            no_data_tv.setVisibility(View.GONE);
            recycler_view.setVisibility(View.VISIBLE);
        } else {
            no_data_tv.setVisibility(View.VISIBLE);
            recycler_view.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.listview_gridview:
                if (current_view == I.LISTVIEW) {
                    communityEventRecyclerAdapter = new CommunityEventRecyclerAdapter(getContext(), arrayList, I.GRIDVIEW);
                    recycler_view.setAdapter(communityEventRecyclerAdapter);

                    listview_gridview.setImageResource(R.mipmap.gridview);
                    current_view = I.GRIDVIEW;
                } else {
                    communityEventRecyclerAdapter = new CommunityEventRecyclerAdapter(getContext(), arrayList, I.LISTVIEW);
                    recycler_view.setAdapter(communityEventRecyclerAdapter);

                    listview_gridview.setImageResource(R.mipmap.listview);
                    current_view = I.LISTVIEW;
                }

                break;
        }
    }

    @Override
    public void ticketInfo(int position) {
        Intent intent = new Intent(context, EventTicketPurchase.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(S.data, arrayList.get(position));
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void moreInfo(int position) {
        Intent intent = new Intent(context, CommunityMoreInfoActivity.class);
        intent.putExtra(S._id, arrayList.get(position).get_id());
        startActivity(intent);
    }

    public void SearchFilter(String ch) {
        communityEventRecyclerAdapter.filter(ch);
    }

    public void getVehicleTypeClick(String id, String select_id) {
        if (select_id.isEmpty())
            communityEventRecyclerAdapter.filterById("");
        else
            communityEventRecyclerAdapter.filterById(id);
    }
}
