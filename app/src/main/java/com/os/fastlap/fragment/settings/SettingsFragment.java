package com.os.fastlap.fragment.settings;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.SettingMenuActivity;
import com.os.fastlap.activity.SettingsActivity;
import com.os.fastlap.adapter.LanguageAdapter;
import com.os.fastlap.beans.LanguageBeans;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.ActivityCommunicator;
import com.os.fastlap.delegates.FragmentCommunicator;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;



/*
 * Created by riyazudinp on 6/17/2017.
 */

@SuppressLint("ValidFragment")
public class SettingsFragment extends Fragment implements FragmentCommunicator, View.OnClickListener, LanguageAdapter.OnItemClickListener {

    private Context mContext;
    private View mView;
    private ActivityCommunicator activityCommunicator;
    private TextView generalInfoTv;
    private TextView changePasswordTv;
    private TextView changeEmailTv;
    private TextView changeLanguageTv;
    private TextView vehicleSettingTv;
    private TextView blockUserTv;
    private TextView manageDeviceConnectedTv;
    private TextView defaultMapsViewTv;
    private TextView timeSystemTv;
    private TextView weekStartDayTv;
    private TextView metricSystemTv;
    private TextView deactivateYourAccountTv;
    private TextView termsAndConditionsTv;
    private ArrayList<LanguageBeans> mLanguageList;
    private LanguageAdapter mLanguageAdapter;
    private Dialog mBottomSheetDialog;
    AuthAPI authAPI;
    private String TAG = SettingsFragment.class.getSimpleName();

    @SuppressLint("ValidFragment")
    public SettingsFragment(Context context) {
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.setting_fragment, container, false);

        }
        mContext = getActivity();
        initView(mView);
        authAPI = new AuthAPI(getContext());
        authAPI.getLanguage(getContext());

        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCommunicator = (ActivityCommunicator) mContext;
        ((SettingsActivity) mContext).fragmentCommunicator = this;
    }

    // LanguageListing webservice response
    public void getLanguage(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    LanguageBeans languageBeans = new LanguageBeans();
                    languageBeans.set_id(jsonObject1.getString(S._id));
                    languageBeans.setName(jsonObject1.getString(S.name));
                    languageBeans.setCode(jsonObject1.getString(S.code));
                    if(MySharedPreferences.getPreferences(mContext,S.language).equalsIgnoreCase(jsonObject1.getString(S.code))){
                        languageBeans.setSelected(true);
                    }
                    else {
                        languageBeans.setSelected(false);
                    }
                    mLanguageList.add(languageBeans);
                }
                mLanguageAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private void initView(View mView) {

        generalInfoTv = (TextView) mView.findViewById(R.id.general_info_tv);
        changePasswordTv = (TextView) mView.findViewById(R.id.change_password_tv);
        changeEmailTv = (TextView) mView.findViewById(R.id.change_email_tv);
        changeLanguageTv = (TextView) mView.findViewById(R.id.change_language_tv);
        vehicleSettingTv = (TextView) mView.findViewById(R.id.vehicle_setting_tv);
        blockUserTv = (TextView) mView.findViewById(R.id.block_user_tv);
        manageDeviceConnectedTv = (TextView) mView.findViewById(R.id.manage_device_connected_tv);
        defaultMapsViewTv = (TextView) mView.findViewById(R.id.default_maps_view_tv);
        timeSystemTv = (TextView) mView.findViewById(R.id.time_system_tv);
        weekStartDayTv = (TextView) mView.findViewById(R.id.week_start_day_tv);
        metricSystemTv = (TextView) mView.findViewById(R.id.metric_system_tv);
        deactivateYourAccountTv = (TextView) mView.findViewById(R.id.deactivate_your_account_tv);
        termsAndConditionsTv = (TextView) mView.findViewById(R.id.terms_and_conditions_tv);

        mLanguageList = new ArrayList<>();
        mLanguageAdapter = new LanguageAdapter(getActivity(), mLanguageList);
        mLanguageAdapter.setItemClickListener(this);

        clickListner();
    }

    private void clickListner() {
        generalInfoTv.setOnClickListener(this);
        changePasswordTv.setOnClickListener(this);
        changeEmailTv.setOnClickListener(this);
        changeLanguageTv.setOnClickListener(this);
        vehicleSettingTv.setOnClickListener(this);
        blockUserTv.setOnClickListener(this);
        manageDeviceConnectedTv.setOnClickListener(this);
        defaultMapsViewTv.setOnClickListener(this);
        timeSystemTv.setOnClickListener(this);
        weekStartDayTv.setOnClickListener(this);
        metricSystemTv.setOnClickListener(this);
        deactivateYourAccountTv.setOnClickListener(this);
        termsAndConditionsTv.setOnClickListener(this);
    }


    @Override
    public void passDataToFragment(String someValue) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.general_info_tv:
                Intent intent = new Intent(mContext, SettingMenuActivity.class);
                intent.putExtra("getPage", FastLapConstant.SETTINGGENERALINFOFRAGMENT);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.change_password_tv:
                Intent intent1 = new Intent(mContext, SettingMenuActivity.class);
                intent1.putExtra("getPage", FastLapConstant.CHANGEPASSWORDFRAGMENT);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.change_email_tv:
                Intent intent0 = new Intent(mContext, SettingMenuActivity.class);
                intent0.putExtra("getPage", FastLapConstant.CHANGEEMAILFRAGMENT);
                startActivity(intent0);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.change_language_tv:
                selectLanguageDialog();
                break;
            case R.id.vehicle_setting_tv:
                Intent intent10 = new Intent(mContext, SettingMenuActivity.class);
                intent10.putExtra("getPage", FastLapConstant.VEHICLESETTINGFRAGMENT);
                startActivity(intent10);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.block_user_tv:
                Intent intent2 = new Intent(mContext, SettingMenuActivity.class);
                intent2.putExtra("getPage", FastLapConstant.BLOCKUSERFRAGMENT);
                startActivity(intent2);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.manage_device_connected_tv:
                Intent intent3 = new Intent(mContext, SettingMenuActivity.class);
                intent3.putExtra("getPage", FastLapConstant.MANAGEDEVICEFRAGMENT);
                startActivity(intent3);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.default_maps_view_tv:
                Intent intent4 = new Intent(mContext, SettingMenuActivity.class);
                intent4.putExtra("getPage", FastLapConstant.DEFAULTMAPVIEWFRAGMENT);
                startActivity(intent4);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.time_system_tv:
                Intent intent5 = new Intent(mContext, SettingMenuActivity.class);
                intent5.putExtra("getPage", FastLapConstant.TIMESYSTEMFRAGMENT);
                startActivity(intent5);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.week_start_day_tv:
                Intent intent6 = new Intent(mContext, SettingMenuActivity.class);
                intent6.putExtra("getPage", FastLapConstant.WEEKSTARTDAYFRAGMENT);
                startActivity(intent6);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.metric_system_tv:
                Intent intent7 = new Intent(mContext, SettingMenuActivity.class);
                intent7.putExtra("getPage", FastLapConstant.METRICSYSTEMFRAGMENT);
                startActivity(intent7);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.deactivate_your_account_tv:
                Intent intent8 = new Intent(mContext, SettingMenuActivity.class);
                intent8.putExtra("getPage", FastLapConstant.DEACTIVATEACCOUNTFRAGMENT);
                startActivity(intent8);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
            case R.id.terms_and_conditions_tv:
                Intent intent9 = new Intent(mContext, SettingMenuActivity.class);
                intent9.putExtra("getPage", FastLapConstant.TERMSCONDITIONFRAGMENT);
                startActivity(intent9);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                break;
        }

    }

    private void selectLanguageDialog() {
        Log.e("tag",""+MySharedPreferences.getPreferences(mContext,S.language));
        mBottomSheetDialog = new Dialog(getActivity(), R.style.AlertDialogBottomSlide);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.select_language_dialog, null);


        RecyclerView recycler_view = dialogView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setAdapter(mLanguageAdapter);


        mBottomSheetDialog.setContentView(dialogView); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    @Override
    public void onItemClick(int position) {
        MySharedPreferences.setPreferences(mContext, mLanguageList.get(position).getCode(), S.language);

        Util.setLocale(mContext);
        mBottomSheetDialog.dismiss();
        Intent intent = new Intent(mContext, SettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        ((Activity) mContext).finish();
    }
}
