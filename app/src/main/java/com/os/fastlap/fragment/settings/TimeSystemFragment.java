package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.RadioButtonPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;

/**
 * Created by abhinava on 7/27/2017.
 */

public class TimeSystemFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private RadioGroup timeSystemRadiogroup;
    private RadioButtonPlayRegular rb24Hour;
    private RadioButtonPlayRegular rb12Hour;
    private TextViewPlayBold timeSystemSaveTv;
    private TextViewPlayBold timeSystemDeleteTv;
    AuthAPI authAPI;

    public static TimeSystemFragment newInstance(Context contex, Object obj) {
        TimeSystemFragment f = new TimeSystemFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_time_system, container, false);
            context = getActivity();
        }
        initView(rootview);
        authAPI = new AuthAPI(context);

        return rootview;
    }

    private void initView(View rootview) {
        timeSystemRadiogroup = (RadioGroup) rootview.findViewById(R.id.time_system_radiogroup);
        rb24Hour = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_24_hour);
        rb12Hour = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_12_hour);
        timeSystemSaveTv = (TextViewPlayBold) rootview.findViewById(R.id.time_system_save_tv);
        timeSystemDeleteTv = (TextViewPlayBold) rootview.findViewById(R.id.time_system_delete_tv);
        clickListner();

        String time = MySharedPreferences.getPreferences(context, S.timeSystem);
        if (time.equalsIgnoreCase("12"))
        {
            rb12Hour.setChecked(true);
            rb24Hour.setChecked(false);
        } else {
            rb12Hour.setChecked(false);
            rb24Hour.setChecked(true);
        }
    }

    private void clickListner() {
        timeSystemSaveTv.setOnClickListener(this);
        timeSystemDeleteTv.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.time_system_save_tv:
                String timeformat = "";
                if (rb12Hour.isChecked()) {
                    timeformat = "12";
                } else if (rb24Hour.isChecked()) {
                    timeformat = "24";
                }

                MySharedPreferences.setPreferences(context, timeformat, S.timeSystem);
                authAPI.setTimeSetting(context, MySharedPreferences.getPreferences(context, S.user_id), timeformat);
                break;

            case R.id.time_system_delete_tv:

                break;
        }

    }

}
