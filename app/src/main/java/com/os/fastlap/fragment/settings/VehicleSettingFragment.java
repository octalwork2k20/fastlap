package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.adapter.settings.VehicleSettingAdapter;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by abhinava on 7/27/2017.
 */

public class VehicleSettingFragment extends Fragment implements View.OnClickListener, VehicleSettingAdapter.OnItemClickListener {
    static Context context;
    static Object object;
    View rootview;
    private TextViewPlayBold saveTv;
    private RecyclerView vehicleRecyclerView;
    private AuthAPI authAPI;
    private String TAG = "VehicleSettingFragment.java";
    private ArrayList<BeanVehicleType> vehicleTypes_list;
    VehicleSettingAdapter vehicleSettingAdapter;
    ArrayList<String> selectedId;

    public static VehicleSettingFragment newInstance(Context contex, Object obj) {
        VehicleSettingFragment f = new VehicleSettingFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_vehicle_setting, container, false);
            context = getActivity();
        }
        initView(rootview);
        selectedId = new ArrayList<>();
        selectedId.clear();

        authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
        selectedId = new ArrayList<String>(Arrays.asList(MySharedPreferences.getPreferences(context, S.vehicleTypeId).split(",")));
        return rootview;
    }


    private void initView(View rootview) {
        authAPI = new AuthAPI(getContext());
        vehicleRecyclerView = rootview.findViewById(R.id.vehicleRecyclerView);
        saveTv = rootview.findViewById(R.id.save_tv);

        vehicleTypes_list = new ArrayList<>();
        vehicleTypes_list.clear();

        vehicleSettingAdapter = new VehicleSettingAdapter(context, vehicleTypes_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        vehicleRecyclerView.setLayoutManager(layoutManager);
        vehicleRecyclerView.setItemAnimator(new DefaultItemAnimator());
        vehicleRecyclerView.setAdapter(vehicleSettingAdapter);
        vehicleSettingAdapter.setItemClickListener(this);

        clickListner();
    }

    private void clickListner() {
        saveTv.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_tv:
                Util.hideKeyboard(getActivity().getCurrentFocus(), getContext());
                StringBuilder vehicleIds=new StringBuilder();
                vehicleIds.setLength(0);
                for(int i=0;i<selectedId.size();i++){
                    if(!selectedId.get(i).equalsIgnoreCase("")){
                        vehicleIds.append(selectedId.get(i));
                        vehicleIds.append(",");
                    }

                }
                Log.e("tag420","selected vehicle ids "+Util.method(vehicleIds.toString()));
                //String ids = TextUtils.join(",",selectedId);
                MySharedPreferences.setPreferences(context, Util.method(vehicleIds.toString()), S.vehicleTypeId);
                authAPI.UpdateVehicleType(getContext(), MySharedPreferences.getPreferences(getContext(), S.user_id),Util.method(vehicleIds.toString()));
                break;
        }
    }

    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                vehicleTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    if (selectedId.contains(jsonObject1.getString(S._id)))
                    {
                        beanVehicleType.setSelected(true);
                    } else {
                        beanVehicleType.setSelected(false);
                    }

                    vehicleTypes_list.add(beanVehicleType);
                }
              //  selectedId.clear();
                vehicleSettingAdapter.notifyDataSetChanged();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onItemClick(int position) {
        if (vehicleTypes_list.get(position).isSelected()) {
            vehicleTypes_list.get(position).setSelected(false);
            selectedId.remove(selectedId.indexOf(vehicleTypes_list.get(position).getId()));

        } else {
            vehicleTypes_list.get(position).setSelected(true);
            selectedId.add(vehicleTypes_list.get(position).getId());
        }

        vehicleSettingAdapter.notifyDataSetChanged();
    }
}
