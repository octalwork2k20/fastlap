package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.RadioButtonPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;

/**
 * Created by abhinava on 7/27/2017.
 */

public class DefaultMapviewFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private RadioButtonPlayRegular rbGoogleNoSector;
    private RadioButtonPlayRegular rbGoogle3Sector;
    private RadioButtonPlayRegular rbGoogle4Sector;
    private RadioButtonPlayRegular rbTrackNoSector;
    private RadioButtonPlayRegular rbTrack3Sector;
    private RadioButtonPlayRegular rbTrack4Sector;
    private TextViewPlayBold mapsviewSaveTv;
    private TextViewPlayBold mapsviewDeleteTv;
    AuthAPI authAPI;
    RadioButtonPlayRegular selectedView;

    public static DefaultMapviewFragment newInstance(Context contex, Object obj) {
        DefaultMapviewFragment f = new DefaultMapviewFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_default_views, container, false);
            context = getActivity();
        }
        initView(rootview);


        return rootview;
    }

    private void initView(View rootview) {
        rbGoogleNoSector = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_google_no_sector);
        rbGoogle3Sector = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_google_3_sector);
        rbGoogle4Sector = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_google_4_sector);
        rbTrackNoSector = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_track_no_sector);
        rbTrack3Sector = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_track_3_sector);
        rbTrack4Sector = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_track_4_sector);
        mapsviewSaveTv = (TextViewPlayBold) rootview.findViewById(R.id.mapsview_save_tv);
        mapsviewDeleteTv = (TextViewPlayBold) rootview.findViewById(R.id.mapsview_delete_tv);

        authAPI = new AuthAPI(context);

        clicklistner();


        String pos = MySharedPreferences.getPreferences(context, S.defaultmapView);
        if (pos.compareTo("1") == 0) {
            setOnloadCheck(rbGoogleNoSector);
        } else if (pos.compareTo("2") == 0) {
            setOnloadCheck(rbGoogle3Sector);
        } else if (pos.compareTo("3") == 0) {
            setOnloadCheck(rbGoogle4Sector);
        } else if (pos.compareTo("4") == 0) {
            setOnloadCheck(rbTrackNoSector);
        } else if (pos.compareTo("5") == 0) {
            setOnloadCheck(rbTrack3Sector);
        } else if (pos.compareTo("6") == 0) {
            setOnloadCheck(rbTrack4Sector);
        }

    }

    private void clicklistner() {
        mapsviewSaveTv.setOnClickListener(this);
        mapsviewDeleteTv.setOnClickListener(this);

        rbGoogleNoSector.setOnClickListener(this);
        rbGoogle3Sector.setOnClickListener(this);
        rbGoogle4Sector.setOnClickListener(this);
        rbTrackNoSector.setOnClickListener(this);
        rbTrack3Sector.setOnClickListener(this);
        rbTrack4Sector.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mapsview_delete_tv:

                break;

            case R.id.mapsview_save_tv:


                String pos = "0";
                String textSend = "";
                if (selectedView.getId() == R.id.rb_google_no_sector) {
                    pos = "1";
                    textSend = "g0";
                } else if (selectedView.getId() == R.id.rb_google_3_sector) {
                    pos = "2";
                    textSend = "g3";
                } else if (selectedView.getId() == R.id.rb_google_4_sector) {
                    pos = "3";
                    textSend = "g4";
                } else if (selectedView.getId() == R.id.rb_track_no_sector) {
                    pos = "4";
                    textSend = "t0";
                } else if (selectedView.getId() == R.id.rb_track_3_sector) {
                    pos = "5";
                    textSend = "t3";
                } else if (selectedView.getId() == R.id.rb_track_4_sector) {
                    pos = "6";
                    textSend = "t4";
                }
                textSend = selectedView.getText().toString();

                authAPI.setDefaultMapview(context, MySharedPreferences.getPreferences(context, S.user_id), textSend);
                MySharedPreferences.setPreferences(context, pos, S.defaultmapView);
                break;
            case R.id.rb_google_no_sector:
                setCheck(rbGoogleNoSector);
                break;
            case R.id.rb_google_3_sector:
                setCheck(rbGoogle3Sector);
                break;
            case R.id.rb_google_4_sector:
                setCheck(rbGoogle4Sector);
                break;
            case R.id.rb_track_no_sector:
                setCheck(rbTrackNoSector);
                break;
            case R.id.rb_track_3_sector:
                setCheck(rbTrack3Sector);
                break;
            case R.id.rb_track_4_sector:
                setCheck(rbTrack4Sector);
                break;

        }
    }

    private void setCheck(RadioButtonPlayRegular rbGoogleNoSect) {
        if (rbGoogleNoSect.isChecked()) {
            rbGoogleNoSector.setChecked(false);
            rbGoogle3Sector.setChecked(false);
            rbGoogle4Sector.setChecked(false);
            rbTrackNoSector.setChecked(false);
            rbTrack3Sector.setChecked(false);
            rbTrack4Sector.setChecked(false);
            rbGoogleNoSect.setChecked(true);
            selectedView = rbGoogleNoSect;
        }
    }

    private void setOnloadCheck(RadioButtonPlayRegular rbGoogleNoSect) {

        rbGoogleNoSector.setChecked(false);
        rbGoogle3Sector.setChecked(false);
        rbGoogle4Sector.setChecked(false);
        rbTrackNoSector.setChecked(false);
        rbTrack3Sector.setChecked(false);
        rbTrack4Sector.setChecked(false);
        rbGoogleNoSect.setChecked(true);
        selectedView = rbGoogleNoSect;

    }
}
