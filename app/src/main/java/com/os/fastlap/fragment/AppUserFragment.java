package com.os.fastlap.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.activity.AllUsersActivity;
import com.os.fastlap.adapter.AppUserAdapter;
import com.os.fastlap.delegates.ActivityCommunicator;
import com.os.fastlap.delegates.FragmentCommunicator;



/*
 * Created by riyazudinp on 6/17/2017.
 */

@SuppressLint("ValidFragment")
public class AppUserFragment extends Fragment implements FragmentCommunicator {

    private Context mContext;
    private View mView;
    private RecyclerView recycler_view;
    private AppUserAdapter appUserAdapter;
    private ActivityCommunicator activityCommunicator;

    @SuppressLint("ValidFragment")
    public AppUserFragment(Context context) {
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.recycler_view_layout, container, false);
            initView();
        }

        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCommunicator = (ActivityCommunicator) mContext;
        ((AllUsersActivity) mContext).fragmentCommunicator = this;
    }

    private void initView() {
        recycler_view = (RecyclerView) mView.findViewById(R.id.recycler_view);
    }


    @Override
    public void passDataToFragment(String someValue) {

    }
}
