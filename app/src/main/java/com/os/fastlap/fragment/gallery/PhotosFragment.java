package com.os.fastlap.fragment.gallery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.AlbumActivity;
import com.os.fastlap.activity.GalleryActivity;
import com.os.fastlap.adapter.GalleryPostsAdapter;
import com.os.fastlap.beans.BeanAlbum;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import java.util.ArrayList;



/*
 * Created by riyazudinp on 6/17/2017.
 */


public class PhotosFragment extends Fragment implements GalleryPostsAdapter.ClickListner, View.OnClickListener {

    private Context context;
    private View mView;
    private RecyclerView recycler_view;
    TextView no_data_tv;
    private GalleryPostsAdapter galleryPostsAdapter;
    public ArrayList<BeanAlbum> mMediaList;
    public ArrayList<BeanAlbum> mMediaListAll;
    int type;
    int mediaType;
    AuthAPI authAPI;
    private String TAG = PhotosFragment.class.getSimpleName();
    RelativeLayout my_album_tv;
    RelativeLayout official_tv;
    RelativeLayout all_tv;
    View mViewAlbum,mViewOffical,mViewAll;

    @SuppressLint("ValidFragment")
    public PhotosFragment(int mediaType, int type) {
        this.type = type;
        this.mediaType = mediaType;
    }

    public PhotosFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.recycler_view_layout, container, false);
        context = getActivity();
        authAPI = new AuthAPI(context);
        initView(mView);
        getData(1);
        return mView;
    }

    private void initView(View mView) {
        mMediaList = new ArrayList<>();
        mMediaList.clear();
        mMediaListAll = new ArrayList<>();
        mMediaListAll.clear();

        recycler_view = (RecyclerView) mView.findViewById(R.id.recycler_view);
        no_data_tv = (TextView) mView.findViewById(R.id.no_data_tv);
        my_album_tv = (RelativeLayout) mView.findViewById(R.id.my_album_tv);
        official_tv = (RelativeLayout) mView.findViewById(R.id.officialTv);
        all_tv = (RelativeLayout) mView.findViewById(R.id.all_tv);
        mViewAlbum=(View)mView.findViewById(R.id.viewAlbum);
        mViewOffical=(View)mView.findViewById(R.id.viewOffical);
        mViewAll=(View)mView.findViewById(R.id.viewAll);


        galleryPostsAdapter = new GalleryPostsAdapter(mMediaList, getActivity(), getActivity(),type);
        RecyclerView.LayoutManager mClothLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(mClothLayoutManager);
        recycler_view.setAdapter(galleryPostsAdapter);
        galleryPostsAdapter.setOnclickListner(this);

        my_album_tv.setOnClickListener(this);
        all_tv.setOnClickListener(this);
        official_tv.setOnClickListener(this);
    }
    @Override
    public void openAlbum(int position) {
        Intent intent = new Intent(context, AlbumActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(S.data, mMediaList.get(position));
        intent.putExtra("mediyaType",type+"");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onClickOnLike(int position, String status) {
        authAPI.albumLikeByUser(context, MySharedPreferences.getPreferences(context, S.user_id), mMediaList.get(position).get_id(), "", status);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == I.COMMENT_ACTIVITY_RESULT_CODE && resultCode == getActivity().RESULT_OK) {

            String position = data.getStringExtra("postPosition");
            String comment_count = data.getStringExtra("total_comment");

            mMediaList.get(Integer.parseInt(position)).setCommentPostCount(comment_count);
            galleryPostsAdapter.notifyItemChanged(Integer.parseInt(position));;
        }
    }

    public void getData(int type) {

        mMediaList.clear();
        if (mediaType == I.PHOTOGALLERY) {
            if (type == 1) {
                mMediaList.addAll(GalleryActivity.mPhotoListMY);
            }
            else if(type==3) {
                mMediaList.addAll(GalleryActivity.mPhotoList);
            }
            else {
                mMediaList.addAll(GalleryActivity.mPhotoListOffical);
            }

        } else {
            if (type == 1) {
                mMediaList.addAll(GalleryActivity.mVideoListMY);
            }
            else if(type==3) {
                mMediaList.addAll(GalleryActivity.mVideoList);
            }
            else {
                mMediaList.addAll(GalleryActivity.mVideoListOffical);
            }
        }
        mMediaListAll.clear();
        mMediaListAll.addAll(mMediaList);
        galleryPostsAdapter = new GalleryPostsAdapter(mMediaList, getActivity(), getActivity(),type);
        RecyclerView.LayoutManager mClothLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(mClothLayoutManager);
        recycler_view.setAdapter(galleryPostsAdapter);
        galleryPostsAdapter.setOnclickListner(this);
        changeSelectedTabColor(type);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.my_album_tv:
                getData(1);
                break;

            case R.id.all_tv:
                getData(3);
                break;
            case R.id.officialTv:
                getData(4);
                break;

        }

    }

    private void changeSelectedTabColor(int type){
        switch (type){
            case 1:
                mViewAlbum.setBackgroundResource(R.color.colorAccent);
                mViewOffical.setBackgroundResource(R.color.gray_text_color);
                mViewAll.setBackgroundResource(R.color.gray_text_color);
                break;
            case 3:
                mViewAlbum.setBackgroundResource(R.color.gray_text_color);
                mViewOffical.setBackgroundResource(R.color.gray_text_color);
                mViewAll.setBackgroundResource(R.color.colorAccent);
                break;
            case 4:
                mViewAlbum.setBackgroundResource(R.color.gray_text_color);
                mViewOffical.setBackgroundResource(R.color.colorAccent);
                mViewAll.setBackgroundResource(R.color.gray_text_color);
                break;
        }
    }

    public void filterData(String id) {
        mMediaList.clear();
        if (id.isEmpty())
            mMediaList.addAll(mMediaListAll);
        for (BeanAlbum wp : mMediaListAll) {
            if (wp.getGroupTypeId().equalsIgnoreCase(id.toLowerCase()))
                mMediaList.add(wp);
        }
        galleryPostsAdapter.notifyDataSetChanged();

    }
}
