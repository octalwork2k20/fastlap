package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.RadioButtonPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;

/**
 * Created by abhinava on 7/27/2017.
 */

public class MetricSystemFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private RadioGroup metricSystemRadiogroup;
    private RadioButtonPlayRegular rbKm;
    private RadioButtonPlayRegular rbMiles;
    private TextViewPlayBold metricSystemSaveTv;
    private TextViewPlayBold metricSystemDeleteTv;
    AuthAPI authAPI;

    public static MetricSystemFragment newInstance(Context contex, Object obj) {
        MetricSystemFragment f = new MetricSystemFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_metric_system, container, false);
            context = getActivity();
        }
        initView(rootview);
        authAPI = new AuthAPI(context);

        return rootview;
    }

    private void initView(View rootview) {
        metricSystemRadiogroup = (RadioGroup) rootview.findViewById(R.id.metric_system_radiogroup);
        rbKm = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_km);
        rbMiles = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_miles);
        metricSystemSaveTv = (TextViewPlayBold) rootview.findViewById(R.id.metric_system_save_tv);
        metricSystemDeleteTv = (TextViewPlayBold) rootview.findViewById(R.id.metric_system_delete_tv);

        clickListner();

        String speed = MySharedPreferences.getPreferences(context, S.speed);
        if (speed.equalsIgnoreCase("KM")) {
            rbKm.setChecked(true);
            rbMiles.setChecked(false);
        } else {
            rbKm.setChecked(false);
            rbMiles.setChecked(true);
        }
    }

    private void clickListner() {
        metricSystemSaveTv.setOnClickListener(this);
        metricSystemDeleteTv.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.metric_system_delete_tv:

                break;

            case R.id.metric_system_save_tv:
                String metrics = "";
                if (rbKm.isChecked()) {
                    metrics = "KM";
                } else if (rbMiles.isChecked()) {
                    metrics = "MILES";
                }
                MySharedPreferences.setPreferences(context, metrics, S.speed);
                authAPI.setMetricSetting(context, MySharedPreferences.getPreferences(context, S.user_id), metrics);
                break;
        }
    }
}
