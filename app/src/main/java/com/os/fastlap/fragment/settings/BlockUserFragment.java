package com.os.fastlap.fragment.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.adapter.settings.BlockUserListAdapter;
import com.os.fastlap.adapter.settings.SettingBlockUserSuggestionListAdapter;
import com.os.fastlap.beans.BlockUserListingBean;
import com.os.fastlap.beans.ProfileMyFriendsList;
import com.os.fastlap.beans.chatmodels.PersonalInfo;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/*
 * Created by abhinava on 7/27/2017.
 */

public class BlockUserFragment extends Fragment implements View.OnClickListener, BlockUserListAdapter.OnItemClickListener, SettingBlockUserSuggestionListAdapter.OnItemClickListener {
    static Context context;
    static Object object;
    View rootview;
    private AutoCompleteTextView blockUserEmailEt;
    private TextViewPlayBold userBlockTv;
    private TextViewPlayBold blockDeleteTv;
    private RecyclerView block_users_recyclerView;
    private BlockUserListAdapter mBlockUserListAdapter;
    private ArrayList<BlockUserListingBean> mBlockUserListingBeen;
    private AuthAPI mAuthAPI;
    private ArrayList<ProfileMyFriendsList> mMyFriendsLists;
    private final String TAG = BlockUserFragment.class.getSimpleName();
    private Dialog dialog1;

    public static BlockUserFragment newInstance(Context contex, Object obj) {
        BlockUserFragment f = new BlockUserFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_block_user, container, false);
        }
        initView(rootview);
        return rootview;
    }

    private void initView(View rootview) {
        mMyFriendsLists = new ArrayList<>();
        blockUserEmailEt = (AutoCompleteTextView) rootview.findViewById(R.id.block_user_email_et);
        userBlockTv = (TextViewPlayBold) rootview.findViewById(R.id.user_block_tv);
        blockDeleteTv = (TextViewPlayBold) rootview.findViewById(R.id.block_delete_tv);
        block_users_recyclerView = (RecyclerView) rootview.findViewById(R.id.block_users_recyclerView);
        mAuthAPI = new AuthAPI(getActivity());

        clickListner();
        mBlockUserListingBeen = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        block_users_recyclerView.setLayoutManager(layoutManager);

        refreshUserList();
    }

    public void refreshUserList() {
        blockUserEmailEt.setText("");
        mAuthAPI.myFriendBlockList(getActivity(), MySharedPreferences.getPreferences(getActivity(), S.user_id));
        mAuthAPI.myFriendList(getActivity(), MySharedPreferences.getPreferences(getActivity(), S.user_id), "");
    }

    private void clickListner() {
        userBlockTv.setOnClickListener(this);
        blockDeleteTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_block_tv:
                if (!blockUserEmailEt.getText().toString().trim().isEmpty())
                    showMyFriendsList(blockUserEmailEt.getText().toString());
                else
                    Util.showAlertDialog(getActivity(), getString(R.string.alert), getResources().getString(R.string.block_user_empty));
                break;

            case R.id.block_delete_tv:
                getActivity().onBackPressed();
                break;
        }
    }

    public void myFriendListWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            mMyFriendsLists.clear();
            if (jsonObject.getInt(S.status) == S.adi_status_success) {

                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    ProfileMyFriendsList profileMyFriendsList = new ProfileMyFriendsList();
                    profileMyFriendsList.setId(jsonObject1.has(S.id)?jsonObject1.getString(S.id):"");
                    profileMyFriendsList.setUserId(jsonObject1.has(S.userId)?jsonObject1.getString(S.userId):"");
                    profileMyFriendsList.setStatus(jsonObject1.has(S.status)?jsonObject1.getString(S.status):"");
                    profileMyFriendsList.setEmail(jsonObject1.has(S.email)?jsonObject1.getString(S.email):"");
                    PersonalInfo personalInfo = new PersonalInfo();

                    JSONObject personalInfoJson = jsonObject1.has(S.personalInfo)?jsonObject1.getJSONObject(S.personalInfo):null;
                    if(personalInfoJson!=null){
                        personalInfo.setFirstName(personalInfoJson.has(S.firstName_response)?personalInfoJson.getString(S.firstName_response):"");
                        personalInfo.setLastName(personalInfoJson.has(S.lastName_response)?personalInfoJson.getString(S.lastName_response):"");
                        personalInfo.setVoucherCode(personalInfoJson.has(S.voucherCode_response)?personalInfoJson.getString(S.voucherCode_response):"");
                        personalInfo.setDateOfBirth(personalInfoJson.has(S.dateOfBirth_response)?personalInfoJson.getString(S.dateOfBirth_response):"");
                        personalInfo.setMobileNumber(personalInfoJson.has(S.mobileNumber_response)?personalInfoJson.getString(S.mobileNumber_response):"");
                        personalInfo.setImage(personalInfoJson.has(S.image)?personalInfoJson.getString(S.image):"");
                        personalInfo.setCoverImage(personalInfoJson.has(S.coverImage)?personalInfoJson.getString(S.coverImage):"");
                        personalInfo.setLanguage(personalInfoJson.has(S.language_response)?personalInfoJson.getString(S.language_response):"");

                        profileMyFriendsList.setPersonalInfo(personalInfo);
                    }
                    mMyFriendsLists.add(profileMyFriendsList);

                }
            } else
                Util.showAlertDialog(getActivity(), getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }


    private void showMyFriendsList(String name) {
        dialog1 = new Dialog(getActivity());
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        TextView no_data_tv = dialog1.findViewById(R.id.no_data_tv);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(getString(R.string.select_friends));

        if (mMyFriendsLists.size() == 0) {
            no_data_tv.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            no_data_tv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        SettingBlockUserSuggestionListAdapter spinDialogAdapter = new SettingBlockUserSuggestionListAdapter(getActivity(), mMyFriendsLists, false);
        spinDialogAdapter.setItemClickListener(this);
        recyclerView.setAdapter(spinDialogAdapter);

        spinDialogAdapter.filter(name);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    /* myFriendBlockList webservice response */
    public void myFriendBlockListResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                mBlockUserListingBeen.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    BlockUserListingBean blockUserListingBean = new BlockUserListingBean();
                    blockUserListingBean.set_id(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    blockUserListingBean.setUserFriendId(jsonObject1.has(S.userFriendId)?jsonObject1.getString(S.userFriendId):"");
                    JSONObject jsonObject2 = jsonObject1.getJSONObject(S.blockUserId);
                    blockUserListingBean.setBlockUserId__id(jsonObject2.has(S._id)?jsonObject1.getString(S._id):"");

                    JSONObject jsonObject3 = jsonObject2.has(S.personalInfo)?jsonObject2.getJSONObject(S.personalInfo):null;
                    if(jsonObject3!=null){
                        blockUserListingBean.setFirstName(jsonObject3.has(S.firstName_response)?jsonObject1.getString(S.firstName_response):"");
                        blockUserListingBean.setLastName(jsonObject3.has(S.lastName_response)?jsonObject1.getString(S.lastName_response):"");
                    }
                    mBlockUserListingBeen.add(blockUserListingBean);
                }
                mBlockUserListAdapter = new BlockUserListAdapter(getActivity(), mBlockUserListingBeen, this);
                block_users_recyclerView.setAdapter(mBlockUserListAdapter);
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /*  unblock user */
    @Override
    public void onUnBlockUser(int position) {
        mAuthAPI.userBlockStatus(getActivity(), mBlockUserListingBeen.get(position).getUserFriendId(), "1", MySharedPreferences.getPreferences(getActivity(), S.user_id),
                mBlockUserListingBeen.get(position).getBlockUserId__id(), mBlockUserListingBeen.get(position).get_id());
    }

    @Override
    public void onItemClick(int position) {
        blockUserEmailEt.setText(mMyFriendsLists.get(position).getPersonalInfo().getFirstName() + " " + mMyFriendsLists.get(position).getPersonalInfo().getLastName());
        blockUserEmailEt.setTag(mMyFriendsLists.get(position).getId());
        dialog1.dismiss();

        mAuthAPI.userBlockStatus(getActivity(), mMyFriendsLists.get(position).getId(), "3", MySharedPreferences.getPreferences(getActivity(), S.user_id), mMyFriendsLists.get(position).getUserId(), "");
    }
}
