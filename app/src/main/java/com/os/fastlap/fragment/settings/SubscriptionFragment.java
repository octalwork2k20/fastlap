package com.os.fastlap.fragment.settings;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.os.fastlap.R;
import com.os.fastlap.activity.payment.AddCardDetail;
import com.os.fastlap.adapter.SubscriptionAdapter;
import com.os.fastlap.beans.BeanSubscription;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.listener.CenterLockListener;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/*
 * Created by riyazudinp on 6/17/2017.
 */

@SuppressLint("ValidFragment")
public class SubscriptionFragment extends Fragment implements SubscriptionAdapter.Clicklistner, View.OnClickListener {

    private Context context;
    private View mView;

    private TextView textViewCenter;
    private RecyclerView recyclerViewFirst;
    private EditText voucherCodeEt;
    private TextView voucherCodeText;
    private TextView saveTv;
    private TextView deleteTv;
    ArrayList<BeanSubscription> subscription_list;
    SubscriptionAdapter subscriptionAdapter;
    AuthAPI authAPI;
    private String TAG = SubscriptionFragment.class.getSimpleName();
    String voucherId = "";
    double payAmount = 0.00;
    double totalAmount =0.00;
    String voucherType = "";
    double voucherAmount = 0.00;
    TextView already_subscribe_tv;
    @SuppressLint("ValidFragment")
    public SubscriptionFragment() {

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.setting_subscription_layout, container, false);
            context = getActivity();
            initView();
        }
        context = getActivity();
        authAPI = new AuthAPI(context);
        initView();

        authAPI.getSubscriptionList(context, MySharedPreferences.getPreferences(context, S.user_id));

        return mView;
    }

    private void initView() {

        textViewCenter = (TextView) mView.findViewById(R.id.textViewCenter);
        recyclerViewFirst = (RecyclerView) mView.findViewById(R.id.recyclerView_first);
        voucherCodeEt = (EditText) mView.findViewById(R.id.voucher_code_et);
        voucherCodeText = (TextView) mView.findViewById(R.id.voucher_code_text);
        saveTv = (TextView) mView.findViewById(R.id.save_tv);
        deleteTv = (TextView) mView.findViewById(R.id.delete_tv);
        already_subscribe_tv = (TextView) mView.findViewById(R.id.already_subscribe_tv);

        subscription_list = new ArrayList<>();
        subscription_list.clear();

        subscriptionAdapter = new SubscriptionAdapter(subscription_list, context, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
        recyclerViewFirst.setLayoutManager(layoutManager);
        recyclerViewFirst.setItemAnimator(new DefaultItemAnimator());
        //  recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerViewFirst.setAdapter(subscriptionAdapter);
        subscriptionAdapter.setOnclickListner(this);

        textViewCenter.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                try {
                    int center = (textViewCenter.getLeft() + textViewCenter.getRight()) / 2;
                    recyclerViewFirst.addOnScrollListener(new CenterLockListener(center));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        clickListner();

        if (!MySharedPreferences.getBooleanPreferences(context,S.checkSubscription)) {
            already_subscribe_tv.setText(getString(R.string.you_have_not_subscribe_any_plan));
        } else {
            already_subscribe_tv.setText(getString(R.string.you_have_subscribe) + " " + MySharedPreferences.getPreferences(context, S.userSubscriptionName) + " " + getString(R.string.plan_upto)
                    + " " + Util.ConvertDateTimeZoneDate(MySharedPreferences.getPreferences(context, S.subscriptionEndDate)));
        }

    }

    private void clickListner() {
        saveTv.setOnClickListener(this);
        deleteTv.setOnClickListener(this);
    }

    public void getsubscriptionList(String response) {
        subscription_list.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanSubscription beanSubscription = new BeanSubscription();
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1 = jsonArray.getJSONObject(i);

                    beanSubscription.setId(jsonObject1.has(S._id)?jsonObject1.getString(S._id):"");
                    beanSubscription.setStatus(jsonObject1.has(S.status)?jsonObject1.getString(S.status):"");
                    beanSubscription.setComparison(jsonObject1.has(S.comparison)?jsonObject1.getString(S.comparison):"");
                    beanSubscription.setSessions(jsonObject1.has(S.sessions)?jsonObject1.getString(S.sessions):"");
                    beanSubscription.setLaps(jsonObject1.has(S.laps)?jsonObject1.getString(S.laps):"");

                    beanSubscription.setTypeofFilters(jsonObject1.has("typeofFilters")?jsonObject1.getString("typeofFilters"):"");
                    beanSubscription.setDataComparisonCapability(jsonObject1.has("dataComparisonCapability")?jsonObject1.getString("dataComparisonCapability"):"");
                    beanSubscription.setVehicles(jsonObject1.has("vehicles")?jsonObject1.getString("vehicles"):"");

                    beanSubscription.setGarage(jsonObject1.has(S.garage)?jsonObject1.getString(S.garage):"");
                    beanSubscription.setMedia(jsonObject1.has(S.media)?jsonObject1.getString(S.media):"");
                    beanSubscription.setPrice(jsonObject1.has(S.price)?jsonObject1.getString(S.price):"");
                    beanSubscription.setName(jsonObject1.has(S.name)?jsonObject1.getString(S.name):"");

                    subscription_list.add(beanSubscription);
                }
                subscriptionAdapter.notifyDataSetChanged();
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void BuyClick(final int position) {
        payAmount = Double.parseDouble(subscription_list.get(position).getPrice());
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_month_dialog);

        final EditTextPlayRegular monthEt;
        TextViewPlayBold cancelBtn;
        TextViewPlayBold okBtn;
        TextViewPlayBold tvAmount;

        monthEt = (EditTextPlayRegular) dialog.findViewById(R.id.month_et);
        cancelBtn = (TextViewPlayBold) dialog.findViewById(R.id.cancel_btn);
        okBtn = (TextViewPlayBold) dialog.findViewById(R.id.ok_btn);
        tvAmount=(TextViewPlayBold)dialog.findViewById(R.id.tvAmount);

        String month = monthEt.getText().toString().trim();
        totalAmount = payAmount * Integer.parseInt(month);
        tvAmount.setText(String.valueOf("$"+totalAmount));

        monthEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()!=0){
                    totalAmount=0.0;
                    String month = charSequence.toString().trim();
                    totalAmount = payAmount * Integer.parseInt(month);
                    tvAmount.setText(String.valueOf("$"+totalAmount));
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String month = monthEt.getText().toString().trim();
                if (!month.isEmpty() && month != null) {
                    if (Integer.parseInt(month) > 0) {
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.MONTH, Integer.parseInt(month));
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Intent intent = new Intent(context, AddCardDetail.class);
                        intent.putExtra(S.type, I.PAYMENT_SUBSCRIPTION + "");
                        intent.putExtra(S.subscriptionPackageId, subscription_list.get(position).getId());
                        intent.putExtra(S.voucherCodeId, voucherId);
                        intent.putExtra(S.amount, totalAmount + "");
                        intent.putExtra(S.month,month);
                        intent.putExtra(S.subscriptionEndDate, dateFormat.format(cal.getTime()));
                        startActivity(intent);
                        dialog.cancel();
                    } else {
                        Toast.makeText(context, getString(R.string.month_error_message), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, getString(R.string.month_empty_message), Toast.LENGTH_LONG).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_tv:
                String voucherCode = voucherCodeEt.getText().toString().trim();
                if (!voucherCode.isEmpty()) {
                    authAPI.getVoucherDetail(context, voucherCode, MySharedPreferences.getPreferences(context, S.user_id));
                }
                break;

            case R.id.delete_tv:
                voucherId = "";
                voucherAmount = 0.0;
                voucherType = "";
                voucherCodeText.setText("");
                voucherCodeText.setVisibility(View.GONE);
                break;
        }

    }

    public void getVoucherCode(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);

                JSONObject jsonObject1 = new JSONObject();
                jsonObject1 = jsonArray.getJSONObject(0);

                String status = jsonObject1.getString(S.status);

                if (status.compareTo("0") == 0) {
                    Util.showAlertDialog(context, getString(R.string.alert), getString(R.string.you_can_not_apply_this_voucher_code));
                    voucherCodeText.setText(getString(R.string.you_can_not_apply_this_voucher_code));
                    voucherCodeText.setVisibility(View.VISIBLE);
                } else {

                    voucherId = jsonObject1.getString(S._id);
                    voucherType = jsonObject1.getString(S.discountType);
                    voucherAmount = Double.parseDouble(jsonObject1.getString(S.amount));

                    String text = "";
                    if (voucherType.compareToIgnoreCase("percentage") == 0) {
                        double disAmount = (payAmount * voucherAmount) / 100;
                        payAmount = payAmount - disAmount;
                        text = "Congratulation! You win $" + disAmount + " Discount";
                    } else {
                        text = "Congratulation! You win $" + voucherAmount + " Months free";
                    }
                    voucherCodeText.setText(text);
                    voucherCodeText.setVisibility(View.VISIBLE);
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
