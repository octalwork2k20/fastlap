package com.os.fastlap.fragment.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.LoginActivity;
import com.os.fastlap.activity.SignupActivity;
import com.os.fastlap.constant.FastLapConstant;

/**
 * Created by abhinava on 7/10/2017.
 */

public class SignupUsertypeFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private RelativeLayout actionBar;
    private TextView loginTextTv;
    private ImageView crossIv;
    private TextView userTypeTv;
    private TextView userChangeTv;
    private LinearLayout otherUserTypeLl;
    private TextView groupTypeTv;
    private TextView trackTypeTv;
    private TextView photographerTypeTv;

    public static SignupUsertypeFragment newInstance(Context contex, Object obj) {
        SignupUsertypeFragment f = new SignupUsertypeFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.signup_usertype, container, false);
            context = getActivity();
        }
        initView(rootview);

        return rootview;
    }


    private void initView(View rootview) {
        actionBar = (RelativeLayout) rootview.findViewById(R.id.action_bar);
        loginTextTv = (TextView) rootview.findViewById(R.id.login_text_tv);
        crossIv = (ImageView) rootview.findViewById(R.id.cross_iv);
        userTypeTv = (TextView) rootview.findViewById(R.id.user_type_tv);
        userChangeTv = (TextView) rootview.findViewById(R.id.user_change_tv);
        otherUserTypeLl = (LinearLayout) rootview.findViewById(R.id.other_user_type_ll);
        groupTypeTv = (TextView) rootview.findViewById(R.id.group_type_tv);
        trackTypeTv = (TextView) rootview.findViewById(R.id.track_type_tv);
        photographerTypeTv = (TextView) rootview.findViewById(R.id.photographer_type_tv);

        clickListner();
    }

    private void clickListner() {
        loginTextTv.setOnClickListener(this);
        crossIv.setOnClickListener(this);
        userTypeTv.setOnClickListener(this);
        userChangeTv.setOnClickListener(this);
        groupTypeTv.setOnClickListener(this);
        trackTypeTv.setOnClickListener(this);
        photographerTypeTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == loginTextTv) {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        } else if (v == crossIv) {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        } else if (v == userChangeTv) {
            if (otherUserTypeLl.getVisibility() == View.VISIBLE) {
                otherUserTypeLl.setVisibility(View.GONE);
            } else {
                otherUserTypeLl.setVisibility(View.VISIBLE);
            }
        } else if (v == userTypeTv) {
            ((SignupActivity) getActivity()).displayView(FastLapConstant.SIGNUPUSERPERSONALDETAILFRAGMENT, "user", null);
        } else if (v == groupTypeTv) {
            ((SignupActivity) getActivity()).displayView(FastLapConstant.SIGNUPUSERPERSONALDETAILFRAGMENT, "group", null);
        } else if (v == trackTypeTv) {
            ((SignupActivity) getActivity()).displayView(FastLapConstant.SIGNUPUSERPERSONALDETAILFRAGMENT, "track", null);
        } else if (v == photographerTypeTv) {
            ((SignupActivity) getActivity()).displayView(FastLapConstant.SIGNUPUSERPERSONALDETAILFRAGMENT, "photographer", null);
        }
    }
}
