package com.os.fastlap.fragment.uploadlaptime;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.ContactUsActivity;
import com.os.fastlap.activity.UploadLapTimeActivity;
import com.os.fastlap.activity.dashboard.DashboardVehicleListActivity;
import com.os.fastlap.adapter.LapTimeAdapter;
import com.os.fastlap.adapter.TrackVersionSpinnerAdapter;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.beans.BeanLapTime;
import com.os.fastlap.beans.BeanTrackVersion;
import com.os.fastlap.beans.BeanUploadData;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.SessionsBean;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.constant.FastLapConstant;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.MyApiEndpointInterface;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by abhinava on 7/10/2017.
 */

public class UploadLapTimeStep1Fragment extends Fragment implements View.OnClickListener, SpinnerSelectorInterface, LapTimeAdapter.onSelected {
    static Context context;
    View rootview;
    private LinearLayout uploadedFileNameLl;
    private TextView uploadedFileNameTv;
    private LinearLayout gpsDeviceLl;
    private TextView gpsDeviceTv;
    private LinearLayout softwareVersionLl;
    private TextView softwareVersionTv;
    private LinearLayout numberofLapsFoundLl;
    private TextView numberofLapsFoundTv;
    private LinearLayout trackFoundLl;
    private TextView trackFoundTv;
    private LinearLayout versionLl;
    private TextView versionTv;
    private LinearLayout circuitDeatilLl;
    private ImageView circuitImageIv;
    private LinearLayout dateOfRecordingLl;
    private TextView dateOfRecordingTv;
    private LinearLayout vehicleLl;
    private TextView vehicleTv;
    private TextView trackNameTv;
    private TextView trackLocationTv;
    private LinearLayout lapsSelectionLl;
    private TextView backTv;
    private TextView nextTv;
    private static final int VEHICLE_ACTIVITY_RESULT_CODE = 0;

    String vehicle_name;
    String vehicle_id;
    String vehicleTypeId;
    String vehicleBrandName;
    String vehicleModelName;
    String vehicleBrandID;
    String vehicleModelId;

    String rentVehicleTypeId;
    String rentVehicleBrandId;
    String rentVehicleModelId;
    RecyclerView lap_gridview;
    ArrayList<SessionsBean> lapArray;
    private String TAG = UploadLapTimeStep1Fragment.class.getSimpleName();
    LapTimeAdapter lapTimeAdapter;
    public static Object object;
    ArrayList<BeanTrackVersion.Version> versionArrayList;

    private ArrayList<BeanVehicleType> vehicleTypes_list;
    private ArrayList<BeanVehicleType> vehicleBrand_list;
    private ArrayList<BeanVehicleType> vehicleModel_list;
    SpinnerSelectorInterface spinnerSelectorInterface;
    AuthAPI authAPI;

    private ArrayList<BeanVehicleType> groupTypes_list;
    TextView groupTypeTv;
    String groupTypeID;

    //new changes by mukeshs
    LinearLayout mRentedLyt;
    CheckBox mRentedChk;
    TextViewPlayRegular mTvVehicleModel;
    TextViewPlayRegular mTvVehicleType;
    TextViewPlayRegular mTvVehicleBrand;
    private MyProgressDialog mProgressDialog;


    public static UploadLapTimeStep1Fragment newInstance(Context contex, Object obj) {
        UploadLapTimeStep1Fragment f = new UploadLapTimeStep1Fragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.upload_lap_time_step1, container, false);
            context = getActivity();
            spinnerSelectorInterface = this;
        }
        authAPI = new AuthAPI(context);
        mProgressDialog = new MyProgressDialog(getActivity());
        initView(rootview);

        ((UploadLapTimeActivity) context).setStep1Fragment();

        gpsDeviceTv.setText(Util.getDeviceBrand() + " " + Util.getDeviceModel());
        softwareVersionTv.setText(Util.getDeviceVersionCode());
        dateOfRecordingTv.setText(Util.ConvertMiliSecondToDate(System.currentTimeMillis() + ""));

        UploadLapTimeActivity.beanUploadData.setGpsDevice(Util.getDeviceBrand() + " " + Util.getDeviceModel());
        UploadLapTimeActivity.beanUploadData.setSoftwareVersion(Util.getDeviceVersionCode());
        UploadLapTimeActivity.beanUploadData.setDateofRecording(Util.ConvertMiliSecondToDate(System.currentTimeMillis() + ""));

        uploadedFileNameTv.setText(UploadLapTimeActivity.beanUploadData.getFilePath());
       // uploadedFileNameTv.setText("/storage/emulated/0/Android/data/com.os.fastlap/files/Fastlap1529327455319.csv");
        //UploadLapTimeActivity.beanUploadData.setFilePath("/storage/emulated/0/Android/data/com.os.fastlap/files/Fastlap1529327455319.csv");

        versionArrayList = new ArrayList<>();
        vehicleTypes_list = new ArrayList<>();
        vehicleBrand_list = new ArrayList<>();
        vehicleModel_list=new ArrayList<>();

        versionArrayList = (ArrayList<BeanTrackVersion.Version>) object;
        //issue found
        UploadLapTimeActivity.beanUploadData.setNumberofLapsFound(lapArray.size() + "");
        numberofLapsFoundTv.setText(lapArray.size() + "");

        authAPI.uploadLaptimeFile(context, MySharedPreferences.getPreferences(context, S.user_id), UploadLapTimeActivity.beanUploadData);
        return rootview;
    }

    private void initView(View rootview) {
        lapArray = new ArrayList<>();
        lapArray.clear();

        groupTypes_list = new ArrayList<>();
        groupTypes_list.clear();

        uploadedFileNameLl = (LinearLayout) rootview.findViewById(R.id.uploaded_file_name_ll);
        uploadedFileNameTv = (TextView) rootview.findViewById(R.id.uploaded_file_name_tv);
        gpsDeviceLl = (LinearLayout) rootview.findViewById(R.id.gps_device_ll);
        gpsDeviceTv = (TextView) rootview.findViewById(R.id.gps_device_tv);
        softwareVersionLl = (LinearLayout) rootview.findViewById(R.id.software_version_ll);
        softwareVersionTv = (TextView) rootview.findViewById(R.id.software_version_tv);
        numberofLapsFoundLl = (LinearLayout) rootview.findViewById(R.id.numberof_laps_found_ll);
        numberofLapsFoundTv = (TextView) rootview.findViewById(R.id.numberof_laps_found_tv);
        trackFoundLl = (LinearLayout) rootview.findViewById(R.id.track_found_ll);
        trackFoundTv = (TextView) rootview.findViewById(R.id.track_found_tv);
        versionLl = (LinearLayout) rootview.findViewById(R.id.version_ll);
        versionTv = (TextView) rootview.findViewById(R.id.version_tv);
        circuitDeatilLl = (LinearLayout) rootview.findViewById(R.id.circuit_deatil_ll);
        circuitImageIv = (ImageView) rootview.findViewById(R.id.circuit_image_iv);
        dateOfRecordingLl = (LinearLayout) rootview.findViewById(R.id.date_of_recording_ll);
        dateOfRecordingTv = (TextView) rootview.findViewById(R.id.date_of_recording_tv);
        vehicleLl = (LinearLayout) rootview.findViewById(R.id.vehicle_ll);
        vehicleTv = (TextView) rootview.findViewById(R.id.vehicle_tv);
        lapsSelectionLl = (LinearLayout) rootview.findViewById(R.id.laps_selection_ll);
        backTv = (TextView) rootview.findViewById(R.id.back_tv);
        nextTv = (TextView) rootview.findViewById(R.id.next_tv);
        trackNameTv = (TextView) rootview.findViewById(R.id.track_name_tv);
        trackLocationTv = (TextView) rootview.findViewById(R.id.track_location_tv);
        groupTypeTv = (TextView) rootview.findViewById(R.id.group_type_tv);
        lap_gridview = (RecyclerView) rootview.findViewById(R.id.lap_gridview);
        mRentedLyt=(LinearLayout)rootview.findViewById(R.id.rentedLyt);
        mRentedChk=(CheckBox)rootview.findViewById(R.id.rentedChk);

       //new changes by mukeshs
        mTvVehicleModel=(TextViewPlayRegular)rootview.findViewById(R.id.tvVehicleModel);
        mTvVehicleType=(TextViewPlayRegular)rootview.findViewById(R.id.tvVehicleType);
        mTvVehicleBrand=(TextViewPlayRegular)rootview.findViewById(R.id.tvVehicleBrand);

        lapTimeAdapter = new LapTimeAdapter(getContext(), lapArray);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(onSpanSizeLookup);
        lap_gridview.setLayoutManager(gridLayoutManager);

        lap_gridview.setAdapter(lapTimeAdapter);
        lapTimeAdapter.shouldShowFooters(false);
        lapTimeAdapter.shouldShowHeadersForEmptySections(true);
        lap_gridview.setNestedScrollingEnabled(false);
        lapTimeAdapter.setonListner(this);
        clickListner();
    }
    /**
     * Helper class to set span size for grid items based on orientation and device type
     */
    GridLayoutManager.SpanSizeLookup onSpanSizeLookup = new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
            return lapTimeAdapter.isHeader(position) ? 2 : 1;

            //return lapTimeAdapter.getItemViewType(position) == TYPE_PREMIUM ? 2 : 1;
        }
    };

    private void clickListner() {
        UploadLapTimeActivity.beanUploadData.setRentVehicle("0");
        nextTv.setOnClickListener(this);
        vehicleLl.setOnClickListener(this);
        versionTv.setOnClickListener(this);
        groupTypeTv.setOnClickListener(this);
        mTvVehicleModel.setOnClickListener(this);
        mTvVehicleBrand.setOnClickListener(this);
        mTvVehicleType.setOnClickListener(this);
        mRentedChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    UploadLapTimeActivity.beanUploadData.setRentVehicle("1");
                    mRentedLyt.setVisibility(View.VISIBLE);
                    vehicleLl.setVisibility(View.GONE);
                }
                else {
                    UploadLapTimeActivity.beanUploadData.setRentVehicle("0");
                    mRentedLyt.setVisibility(View.GONE);
                    vehicleLl.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_tv:
                try {
                    String selectedValues = "";
                    String comaparelap = MySharedPreferences.getPreferences(context, S.userSubscriptionLaps);
                    JSONArray lapjsonArray = new JSONArray();

                    for (int i = 0; i < lapArray.size(); i++)
                    {
                        JSONObject jsonObject = new JSONObject();
                        JSONArray lapCount = new JSONArray();
                        for (int j = 0; j < lapArray.get(i).getLapArray().size(); j++)
                        {
                            if (lapArray.get(i).getLapArray().get(j).isSelected()) {
                                lapCount.put(Integer.parseInt(lapArray.get(i).getLapArray().get(j).getLap()));
                            }
                        }

                        if (lapCount.length() > 0) {

                            jsonObject.put("session", lapArray.get(i).getSessionId());
                            //jsonObject.put("sessionText", getString(R.string.session1) + lapArray.get(i).getSessionId());
                            jsonObject.put("laps", lapCount);
                            jsonObject.put("files", 0);
                            jsonObject.put("weather", "rain");
                            jsonObject.put("vehicleTyreBrandId", vehicleBrandID);
                           // jsonObject.put("vehicleTypeBrandName", vehicleBrandName);
                            jsonObject.put("vehicleTyreModelId", vehicleModelId);
                            jsonObject.put("notes", "");
                            jsonObject.put("url", "");
                            lapjsonArray.put(jsonObject);
                        }
                    }
                    Log.e("tag","fgfg"+lapjsonArray);
                    UploadLapTimeActivity.beanUploadData.setSelectedValue(lapjsonArray);
                    if (UploadLapTimeActivity.beanUploadData.getTrackVersionId().isEmpty() || UploadLapTimeActivity.beanUploadData.getTrackVersionId() == null) {
                        Util.showToast(context, getString(R.string.please_select_version));
                        return;
                    }
                    else if (lapjsonArray.length() == 0) {
                        Util.showToast(context, getString(R.string.please_select_at_least_one_lap_for_compare));
                        return;
                    }
                    //new changes
                    else if(!mRentedChk.isChecked()) {
                        if (UploadLapTimeActivity.beanUploadData.getVehicleId().isEmpty() || UploadLapTimeActivity.beanUploadData.getVehicleId() == null) {
                            Util.showToast(context, getString(R.string.please_select_vehicle));
                            return;
                        }
                    }
                    //new changes
                    else if(mRentedChk.isChecked())
                    {
                        if (rentVehicleTypeId.isEmpty() || rentVehicleTypeId == null) {
                            Util.showToast(context, getString(R.string.please_rented_select_vehicle));
                            return;
                        }
                        else if (rentVehicleBrandId.isEmpty() || rentVehicleBrandId == null) {
                            Util.showToast(context, getString(R.string.please_select_brand));
                            return;
                        }
                        else if (rentVehicleModelId.isEmpty() || rentVehicleModelId == null) {
                            Util.showToast(context, getString(R.string.please_select_model));
                            return;
                        }
                    }
               /* else if (!TextUtils.isEmpty(comaparelap))
                {*/
                    /*int planLap = Integer.parseInt(comaparelap);
                    if (planLap == -1)
                    {
                        UploadLapTimeActivity.beanUploadData.setSelectedValue(selectedValues);
                    } else if (selectedValues.split(",").length <= planLap)
                    {
                        UploadLapTimeActivity.beanUploadData.setSelectedValue(selectedValues);
                    } else {
                        Util.showSnackBar(nextTv, context.getString(R.string.according_to_your_subscription_plan_you_can_select_only) + " " + planLap + context.getString(R.string.lap_for_comparision));
                    }*/
               /* } else {
                    Util.showSnackBar(nextTv, context.getString(R.string.please_subscribe_any_plan_for_laps_comparision));
                }*/
                    ((UploadLapTimeActivity) getActivity()).displayView(FastLapConstant.UPLOADLAPTIMESTEP2FRAGMENT, null, null);
                } catch (Exception e) {
                    Log.e("tag","Exception "+e.toString());
                }
                break;

            case R.id.vehicle_ll:
                Intent intent = new Intent(context, DashboardVehicleListActivity.class);
                intent.putExtra("isShowAddButton",true);
                startActivityForResult(intent, VEHICLE_ACTIVITY_RESULT_CODE);
                break;
            case R.id.version_tv:
                showTrackVersionDialog(getString(R.string.select_track_version), versionArrayList, 0);
                break;
            case R.id.group_type_tv:
                authAPI.getVehicleTypeList(context, MySharedPreferences.getPreferences(context, S.user_id));
                break;
            case R.id.tvVehicleType:
                mProgressDialog.show();
                FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleType()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<ResponseBody>>() {
                            @Override
                            public void onSubscribe(@NonNull Disposable d) {

                            }

                            @Override
                            public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                                mProgressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(Util.convertRetrofitResponce(responseBodyResponse));
                                    String msg = jsonObject.getString(S.message);
                                    if (jsonObject.getInt(S.status) == S.adi_status_success) {
                                        vehicleTypes_list.clear();
                                        JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            BeanVehicleType beanVehicleType = new BeanVehicleType();
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                            beanVehicleType.setId(jsonObject1.getString(S._id));
                                            beanVehicleType.setName(jsonObject1.getString(S.name));
                                            vehicleTypes_list.add(beanVehicleType);
                                        }
                                        if (vehicleTypes_list.size() > 0)
                                            showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_type), vehicleTypes_list, I.VEHICLE_TYPE);
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, e.toString());
                                }
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                //mProgressDialog.dismiss();
                            }

                            @Override
                            public void onComplete() {

                            }
                        });
                break;
            case R.id.tvVehicleBrand:
                mProgressDialog.show();
                FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleBrand(rentVehicleTypeId)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<ResponseBody>>() {
                            @Override
                            public void onSubscribe(@NonNull Disposable d) {

                            }

                            @Override
                            public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                                mProgressDialog.dismiss();
                                try {

                                    JSONObject jsonObject = new JSONObject(Util.convertRetrofitResponce(responseBodyResponse));
                                    String msg = jsonObject.getString(S.message);
                                    if (jsonObject.getInt(S.status) == S.adi_status_success) {
                                        vehicleBrand_list.clear();
                                        JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            BeanVehicleType beanVehicleType = new BeanVehicleType();
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                            beanVehicleType.setId(jsonObject1.getString(S._id));
                                            beanVehicleType.setName(jsonObject1.getString(S.vehicleBrandName));
                                            vehicleBrand_list.add(beanVehicleType);
                                        }
                                        if (vehicleBrand_list.size() > 0)
                                            showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_brand), vehicleBrand_list, I.VEHICLE_BRAND);
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, e.toString());
                                }
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                            }

                            @Override
                            public void onComplete() {

                            }
                        });
                break;
            case R.id.tvVehicleModel:
                mProgressDialog.show();
                FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleModel(rentVehicleTypeId, rentVehicleBrandId)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<ResponseBody>>() {
                            @Override
                            public void onSubscribe(@NonNull Disposable d) {

                            }
                            @Override
                            public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                                mProgressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(Util.convertRetrofitResponce(responseBodyResponse));
                                    String msg = jsonObject.getString(S.message);
                                    if (jsonObject.getInt(S.status) == S.adi_status_success) {
                                        vehicleModel_list.clear();
                                        JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            BeanVehicleType beanVehicleType = new BeanVehicleType();
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                            beanVehicleType.setId(jsonObject1.getString(S._id));
                                            beanVehicleType.setName(jsonObject1.getString(S.vehicleModelName));
                                            vehicleModel_list.add(beanVehicleType);
                                        }
                                        if (vehicleModel_list != null)
                                            showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_model), vehicleModel_list, I.VEHICLE_MODEL);
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, e.toString());
                                }
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                               //mProgressDialog.dismiss();
                            }

                            @Override
                            public void onComplete() {
                            }
                        });
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == VEHICLE_ACTIVITY_RESULT_CODE) {
                vehicle_name = data.getStringExtra(S.vehiclename);
                vehicle_id = data.getStringExtra(S.vehicleId);
                vehicleTypeId = data.getStringExtra(S.vehicleTypeId);
                vehicleBrandName = data.getStringExtra(S.vehicleBrandName);
                vehicleModelName=data.getStringExtra(S.vehicleModelName);
                vehicleBrandID = data.getStringExtra(S.vehicleBrandId);
                vehicleModelId=data.getStringExtra(S.vehicleModelId);
                vehicleTv.setText(vehicle_name);

                Log.e("tag","bb"+vehicleBrandName);
                Log.e("tag","bb"+vehicleModelName);

                Log.e("tag","vehicleTypeId"+vehicleTypeId);


                UploadLapTimeActivity.beanUploadData.setVehicleBrandName(vehicleBrandName);
                UploadLapTimeActivity.beanUploadData.setVehicleModelName(vehicleModelName);
                UploadLapTimeActivity.beanUploadData.setVehicleId(vehicle_id);
                UploadLapTimeActivity.beanUploadData.setVehicleName(vehicle_name);
                UploadLapTimeActivity.beanUploadData.setVehicleTypeId(vehicleTypeId);
            }
        }
    }

    private void showTrackVersionDialog(String dialog_title, ArrayList<BeanTrackVersion.Version> versionArrayList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        TrackVersionSpinnerAdapter spinDialogAdapter = new TrackVersionSpinnerAdapter(context, versionArrayList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type){
            case I.VEHICLE_TYPE:
                mTvVehicleType.setText(value);
                rentVehicleTypeId = id;
                mTvVehicleBrand.setText("");
                mTvVehicleModel.setText("");
                rentVehicleBrandId = "";
                vehicleBrand_list.clear();
                Log.e("tag","rentVehicleTypeId"+rentVehicleTypeId);
                UploadLapTimeActivity.beanUploadData.setRentVehicleTypeId(rentVehicleTypeId);
                break;
            case I.VEHICLE_BRAND:
                mTvVehicleBrand.setText(value);
                rentVehicleBrandId = id;
                UploadLapTimeActivity.beanUploadData.setRentVehicleBrandId(rentVehicleBrandId);
                break;
            case I.VEHICLE_MODEL:
                mTvVehicleModel.setText(value);
                rentVehicleModelId = id;
                UploadLapTimeActivity.beanUploadData.setRentVehicleModelId(rentVehicleModelId);
                break;
        }

    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {
        switch (type) {
            case I.GROUPTYPE:
                groupTypeTv.setText(value);
                groupTypeID = id;
                UploadLapTimeActivity.beanUploadData.setGroupTypeId(groupTypeID);

                break;

            case 0:
                versionTv.setText(value);
                UploadLapTimeActivity.beanUploadData.setTrackVersionId(id);
                UploadLapTimeActivity.beanUploadData.setTrackVersion(value);
                break;
        }

    }

    @Override
    public void CheckedChange(boolean status, int position) {

    }
    public void checkLapTimeCSV(String response)
    {
        Log.e("tag","responce "+response);
        lapArray.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                Toast.makeText(getActivity(),"File uploaded suceffully",Toast.LENGTH_LONG).show();
                String lapcount = jsonObject.getJSONObject(S.data).getJSONObject(S.lapTimes).getString(S.totalLaps);

                UploadLapTimeActivity.beanUploadData.setNumberofLapsFound(lapcount);
                numberofLapsFoundTv.setText(lapcount);

                JSONArray jsonArray = new JSONArray();
                jsonArray = jsonObject.getJSONObject(S.data).getJSONObject(S.lapTimes).getJSONArray(S.sessions);


                for (int j = 0; j < jsonArray.length(); j++) {
                    SessionsBean sessionsBean = new SessionsBean();
                    JSONArray lapsJaonArray = new JSONArray();
                    lapsJaonArray = jsonArray.getJSONObject(j).getJSONArray(S.laps);
                    sessionsBean.setLapCount(jsonArray.getJSONObject(j).getString(S.lapCount));
                    sessionsBean.setSessionId(jsonArray.getJSONObject(j).getString(S.session));

                    ArrayList<BeanLapTime> lapTimes = new ArrayList<>();

                    for (int i = 0; i < lapsJaonArray.length(); i++) {
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1 = lapsJaonArray.getJSONObject(i);
                        BeanLapTime beanLapTime = new BeanLapTime();
                        beanLapTime.setLap(jsonObject1.getString(S.index));
                        beanLapTime.setLapTime(jsonObject1.getString(S.time));
                        lapTimes.add(beanLapTime);
                    }
                    sessionsBean.setLapArray(lapTimes);
                    lapArray.add(sessionsBean);
                }


                JSONObject trackJsonObject = new JSONObject();
                trackJsonObject = jsonObject.getJSONObject(S.data).getJSONObject(S.lapTimes).getJSONObject(S.track);

                UploadLapTimeActivity.beanUploadData.setTrackId(trackJsonObject.getString(S._id));
                UploadLapTimeActivity.beanUploadData.setTrackName(trackJsonObject.getString(S.name));
                UploadLapTimeActivity.beanUploadData.setTrackLocation(trackJsonObject.getString(S.location_response));
                UploadLapTimeActivity.beanUploadData.setTrackImage(trackJsonObject.getString(S.image));
                UploadLapTimeActivity.beanUploadData.setTrackVersionId(trackJsonObject.getJSONObject(S.version).getString(S._id));
                UploadLapTimeActivity.beanUploadData.setTrackVersion(trackJsonObject.getJSONObject(S.version).getString(S.name));

                trackFoundTv.setText(UploadLapTimeActivity.beanUploadData.getTrackName());
                trackNameTv.setText(UploadLapTimeActivity.beanUploadData.getTrackName());
                trackLocationTv.setText(UploadLapTimeActivity.beanUploadData.getTrackLocation());
                versionTv.setText(UploadLapTimeActivity.beanUploadData.getTrackVersion());
                ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + UploadLapTimeActivity.beanUploadData.getTrackImage(), circuitImageIv, Util.getImageLoaderOption(context));

                JSONArray versionJsonArray = new JSONArray();
                versionJsonArray = jsonObject.getJSONObject(S.data).getJSONArray(S.trackVersions);

                for (int j = 0; j < versionJsonArray.length(); j++) {
                    BeanTrackVersion.Version versions = new BeanTrackVersion().new Version();
                    versions.setVersionId(versionJsonArray.getJSONObject(j).getString(S._id));
                    versions.setVersionName(versionJsonArray.getJSONObject(j).getString(S.name));

                    versionArrayList.add(versions);
                }

                lapTimeAdapter.notifyDataSetChanged();
                Util.showAlertDialog(context, getString(R.string.file_upload_successfully), msg);

            } else
            {
                String data = jsonObject.getString(S.data);
                if (TextUtils.isEmpty(data) || data.equalsIgnoreCase("{}")) {
                    msg = getString(R.string.file_not_found_msg);
                    Util.showAlertDialogWithAction(context, getString(R.string.file_uploaded), msg, "1");
                } else {
                    String fileName = jsonObject.getJSONObject(S.data).getString(S.fileName);
                    if (TextUtils.isEmpty(fileName)) {
                        msg = getString(R.string.no_tracking_device_msg);
                        Util.showAlertDialogWithAction(context, getString(R.string.file_uploaded), msg, "2");
                    } else {
                        String track = jsonObject.getJSONObject(S.data).getJSONObject(S.lapTimes).getString(S.track);
                        if (TextUtils.isEmpty(track) || track.equalsIgnoreCase("{}"))
                        {
                            msg = getString(R.string.no_track_found_msg);
                            Util.showAlertDialogWithAction(context, getString(R.string.file_uploaded), msg, "3");
                        } else {
                            String version = jsonObject.getJSONObject(S.data).getJSONObject(S.lapTimes).getJSONObject(S.track).getString(S.version);
                            if (TextUtils.isEmpty(version) || version.equalsIgnoreCase("{}")) {
                                msg = getString(R.string.no_track_version_found_msg);
                                Util.showAlertDialogWithAction(context, getString(R.string.file_uploaded), msg, "4");
                            } else {
                                String lapcount = jsonObject.getJSONObject(S.data).getJSONObject(S.lapTimes).getString(S.totalLaps);
                                if (Integer.parseInt(lapcount) == 0) {
                                    msg = getString(R.string.no_laps_found_msg);
                                    Util.showAlertDialogWithAction(context, getString(R.string.file_uploaded), msg, "5");
                                }
                            }
                        }


                    }


                }

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    // Vechile Type List
    public void vehicleTypeWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                groupTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    groupTypes_list.add(beanVehicleType);
                }
                showVehicleTypeDialog(getString(R.string.select_group_type), groupTypes_list, I.GROUPTYPE);

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        if (vehicleList.size() != 0) {
            final Dialog dialog1 = new Dialog(context);
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.spin_dialog);
            TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
            RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
            ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
            TextView done_tv = dialog1.findViewById(R.id.done_tv);
            done_tv.setVisibility(View.GONE);
            base_toggle_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog1.dismiss();
                }
            });
            heading.setText(dialog_title);

            VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(spinDialogAdapter);

            dialog1.setCancelable(true);
            dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog1.getWindow().setGravity(Gravity.CENTER);
            dialog1.show();
        } else {
            if (type == I.VEHICLE_BRAND)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.VEHICLE_TYPE)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_type));
            if (type == I.VEHICLE_MODEL)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
            if (type == I.VEHICLE_VERSION)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_version));
            if (type == I.VEHICLE_YEAR)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_year));
            if (type == I.TYRE_BRAND)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.TYRE_MODEL)
                Util.showAlertDialog(context, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
        }
    }

    public void trackNotfound() {
        Intent intent = new Intent(context, ContactUsActivity.class);
        intent.putExtra(S.fileName, UploadLapTimeActivity.beanUploadData.getFilePath());
        startActivity(intent);
        UploadLapTimeActivity.beanUploadData = new BeanUploadData();
        ((Activity) context).finish();
    }
}
