package com.os.fastlap.fragment.paddock;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.dashboard.DashboardVehicleListActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.paddock.PaddockUploadsLapsAdapter;
import com.os.fastlap.adapter.paddock.UploadExpandMyLapAdapter;
import com.os.fastlap.beans.BeanSessionData;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.dialogs.ChooseImagePickerOptionDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by anandj on 7/18/2017.
 */

public class PaddockUploadsFragment extends Fragment implements View.OnClickListener, SpinnerSelectorInterface, OnImagePickerDialogSelect, UploadExpandMyLapAdapter.OnClick, DatePickerDialog.OnDateSetListener {

    Context context;
    private TextViewPlayRegular sessionsMenuTv;
    private TextViewPlayRegular lapsMenuTv;

    private LinearLayout lapsLayout;
    private RecyclerView lapsRecyclerview;
    SpinnerSelectorInterface spinnerSelectorInterface;

    private ArrayList<BeanSessionData> sessionArrayList;
    AuthAPI authAPI;
    private static final int VEHICLE_ACTIVITY_RESULT_CODE = 0;

    String vehicle_name = "";
    String vehicle_id = "";
    String vehicleTypeId = "";
    String vehicleTyreBrandId = "";
    String vehicleTyreModelId = "";
    private ArrayList<BeanVehicleType> vehicleTyreBrand_list;
    private ArrayList<BeanVehicleType> vehicleTyreModel_list;
    private String TAG = PaddockUploadsFragment.class.getSimpleName();

    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    public BeanSessionData beanSessionData;

    DatePickerDialog.OnDateSetListener onDateSetListener;
    int selectedPosition = 0;
    ExpandableListView expendableView;
    UploadExpandMyLapAdapter uploadExpandMyLapAdapter;
    PaddockUploadsLapsAdapter paddockUploadsLapsAdapter;
    int expandPosition = -1;
    TextView updateLaptv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mylaps_uploads_fragment, container, false);
        context = getActivity();
        authAPI = new AuthAPI(context);
        onDateSetListener = this;
        onImagePickerDialogSelect = this;
        spinnerSelectorInterface = this;
        initView(view);
        return view;
    }

    private void initView(View view) {
        expendableView = (ExpandableListView) view.findViewById(R.id.expendableView);
        sessionsMenuTv = (TextViewPlayRegular) view.findViewById(R.id.sessions_menu_tv);
        lapsMenuTv = (TextViewPlayRegular) view.findViewById(R.id.laps_menu_tv);

        lapsLayout = (LinearLayout) view.findViewById(R.id.laps_layout);
        lapsRecyclerview = (RecyclerView) view.findViewById(R.id.laps_recyclerview);
        updateLaptv = (TextView) view.findViewById(R.id.updateLaptv);

        beanSessionData = new BeanSessionData();
        sessionArrayList = new ArrayList<>();

        vehicleTyreBrand_list = new ArrayList<>();
        vehicleTyreModel_list = new ArrayList<>();

        uploadExpandMyLapAdapter = new UploadExpandMyLapAdapter(context, sessionArrayList);
        expendableView.setAdapter(uploadExpandMyLapAdapter);
        uploadExpandMyLapAdapter.setOnClickListner(this);

        expendableView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expendableView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
                expandPosition = groupPosition;
            }

        });

        expendableView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                expandPosition = -1;
            }
        });


        clickListner();
    }

    private void clickListner() {
        sessionsMenuTv.setOnClickListener(this);
        lapsMenuTv.setOnClickListener(this);
        updateLaptv.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sessions_menu_tv:
                showSession();
                break;

            case R.id.laps_menu_tv:
                showLaps();
                break;

            case R.id.updateLaptv:
                try {
                    JSONArray lapArray = new JSONArray();
                    for (int i = 0; i < sessionArrayList.get(selectedPosition).getLapList().size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("_id", sessionArrayList.get(selectedPosition).getLapList().get(i).getLapsId());
                        jsonObject.put("status", sessionArrayList.get(selectedPosition).getLapList().get(i).getStatus());
                        lapArray.put(jsonObject);
                    }

                    authAPI.updateLap(context,MySharedPreferences.getPreferences(context,S.user_id),lapArray.toString());
                }
                catch (Exception e) {
                    e.printStackTrace();
                    }
                break;
        }
    }

    private ArrayList<PagerBean> getNewFileArray() {
        ArrayList<PagerBean> list = new ArrayList<>();
        for (int i = 0; i < sessionArrayList.get(selectedPosition).getSessionsImagesList().size(); i++) {
            if (sessionArrayList.get(selectedPosition).getSessionsImagesList().get(i).isNew()) {
                list.add(sessionArrayList.get(selectedPosition).getSessionsImagesList().get(i));
            }
        }

        return list;
    }

    // Youtube URL get Data Response
    public void getYoutubeURLData(String response, int position) {
        System.out.println("response:-- " + response);
        String videoImage = "";
        String channelTitle = "";
        String VideoTitle = "";

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            if (jsonArray.length() > 0) {
                videoImage = jsonArray.getJSONObject(0).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url");
                channelTitle = jsonArray.getJSONObject(0).getJSONObject("snippet").getString("channelTitle");
                VideoTitle = jsonArray.getJSONObject(0).getJSONObject("snippet").getString("title");

                sessionArrayList.get(position).setVideoImage(videoImage);
                sessionArrayList.get(position).setChannelTitle(channelTitle);
                sessionArrayList.get(position).setVideoTitle(VideoTitle);

                uploadExpandMyLapAdapter.notifyDataSetChanged();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VEHICLE_ACTIVITY_RESULT_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                vehicle_name = data.getStringExtra(S.vehiclename);
                vehicle_id = data.getStringExtra(S.vehicleId);
                vehicleTypeId = data.getStringExtra(S.vehicleTypeId);

                sessionArrayList.get(selectedPosition).setUserVehicleId(vehicle_id);
                sessionArrayList.get(selectedPosition).setUserVehicletypeId(vehicleTypeId);
                sessionArrayList.get(selectedPosition).setUserVehicleModelName(vehicle_name);

                sessionArrayList.get(selectedPosition).setVehicletyreBrandId("");
                sessionArrayList.get(selectedPosition).setVehicletyreBrandName("");
                sessionArrayList.get(selectedPosition).setVehicletyreModelId("");
                sessionArrayList.get(selectedPosition).setVehicletyreModelName("");


                vehicleTyreModelId = "";
                vehicleTyreModel_list.clear();
                vehicleTyreBrand_list.clear();

                vehicleTyreBrandId = "";
                authAPI.getVehicleTyreBrand(context, vehicleTypeId);
                uploadExpandMyLapAdapter.notifyDataSetChanged();

                //postEdittext.setText(postEdittext.getText().toString() + " " + vehicle_name);

            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;

                if (sessionArrayList.get(selectedPosition).getSessionsImagesList().size() <= 5) {
                    sessionArrayList.get(selectedPosition).getSessionsImagesList().add(new PagerBean(imageFile, imageFile.getAbsolutePath(), imageFile.getName(), false, false, "", true, ""));
                    uploadExpandMyLapAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.spin_dialog);
        TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
        RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
        ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
        TextView done_tv = dialog1.findViewById(R.id.done_tv);
        done_tv.setVisibility(View.GONE);
        base_toggle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });
        heading.setText(dialog_title);

        VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(context, vehicleList, spinnerSelectorInterface, type, dialog1);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(spinDialogAdapter);

        dialog1.setCancelable(true);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().setGravity(Gravity.CENTER);
        dialog1.show();
    }

    // Vehicle Tyre Brand Response
    public void getVehicleTyreBrandResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTyreBrand_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.brandName));
                    vehicleTyreBrand_list.add(beanVehicleType);
                }

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Vehicle Tyre Model Response
    public void getVehicleTyreModelResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTyreModel_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehichleTyreModelName));
                    vehicleTyreModel_list.add(beanVehicleType);
                }
            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type) {
        switch (type) {
            case I.TYRE_BRAND:
                vehicleTyreModelId = "";
                vehicleTyreModel_list.clear();

                vehicleTyreBrandId = id;
                sessionArrayList.get(selectedPosition).setVehicletyreBrandId(vehicleTyreBrandId);
                sessionArrayList.get(selectedPosition).setVehicletyreBrandName(value);
                sessionArrayList.get(selectedPosition).setVehicletyreModelId("");

                authAPI.getVehicleTyreModel(context, vehicleTypeId, vehicleTyreBrandId);
                uploadExpandMyLapAdapter.notifyDataSetChanged();
                break;
            case I.TYRE_MODEL:
                vehicleTyreModelId = id;
                sessionArrayList.get(selectedPosition).setVehicletyreModelId(vehicleTyreModelId);
                sessionArrayList.get(selectedPosition).setVehicletyreModelName(value);
                uploadExpandMyLapAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }


    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                    }, 1);
        } else {
            showImageDialog();
        }
    }

    public void showImageDialog() {

        ChooseImagePickerOptionDialog chooseImagePickerOptionDialog = new ChooseImagePickerOptionDialog(context, onImagePickerDialogSelect);
        chooseImagePickerOptionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        chooseImagePickerOptionDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageDialog();
            } else {
                //mSelectImageClass = new SelectImageClass(mContext, getActivity(), "");
                // System.exit(0);
            }
        }
    }
    @Override
    public void OnCameraSelect() {
        EasyImage.openCamera(this, REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallerySelect() {
        EasyImage.openGallery(this, SELECT_FILE_PROFILE);
    }

    @Override
    public void onVideoSelect() {

    }
    // Update Lap time Response
    public void updateLapTimeResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                //((MyLapsActivity) getActivity()).getSessionData();

            } else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void getSessionDataResponse(String response) {
        try {
            sessionArrayList.clear();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.has(S.data)?jsonObject.getJSONArray(S.data):null;
                if(jsonArray!=null&&jsonArray.length()>0){
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject lapTimeIdJObj = new JSONObject();
                        JSONObject trackVersionJobj = new JSONObject();
                        JSONObject sessionJObj = new JSONObject();
                        sessionJObj = jsonArray.getJSONObject(i);
                        BeanSessionData beanSessionData = new BeanSessionData();
                        beanSessionData.setSessionId(sessionJObj.has(S._id)?sessionJObj.getString(S._id):"");
                        beanSessionData.setUserId(sessionJObj.has(S.userId)?sessionJObj.getString(S.userId):"");
                        beanSessionData.setNotes(sessionJObj.has(S.notes)?sessionJObj.getString(S.notes):"");
                        beanSessionData.setYoutubeUrl(sessionJObj.has(S.youtubeURL)?sessionJObj.getString(S.youtubeURL):"");
                        beanSessionData.setWeather(sessionJObj.has(S.weather)?sessionJObj.getString(S.weather):"");
                        beanSessionData.setSessionIndex(sessionJObj.has(S.sessionIndex)?sessionJObj.getString(S.sessionIndex):"");
                        lapTimeIdJObj = sessionJObj.has(S.lapTimeId)?sessionJObj.getJSONObject(S.lapTimeId):null;

                        if(lapTimeIdJObj!=null&&lapTimeIdJObj.length()>0)
                        {
                            beanSessionData.setLapTimeId(lapTimeIdJObj.has(S._id)?lapTimeIdJObj.getString(S._id):"");
                            beanSessionData.setNumberOfLapsFound(lapTimeIdJObj.getString(S.numberOfLapsFound));
                            beanSessionData.setDateOfRecording(Util.ConvertDateTimeZoneDateYear(lapTimeIdJObj.getString(S.dateOfRecording)));

                            try {
                                trackVersionJobj=lapTimeIdJObj.has(S.trackVersionId)?lapTimeIdJObj.getJSONObject(S.trackVersionId):null;
                                Log.e("tag","try"+trackVersionJobj);
                            }
                            catch (Exception e){
                                trackVersionJobj=null;
                                Log.e("tag","catch"+trackVersionJobj);
                                e.printStackTrace();
                            }
                            if(trackVersionJobj!=null&&trackVersionJobj.length()>0){
                                beanSessionData.setTrackVersionId(trackVersionJobj.has(S._id)?trackVersionJobj.getString(S._id):"");
                                beanSessionData.setTrackVersionName(trackVersionJobj.has(S.name)?trackVersionJobj.getString(S.name):"");
                                beanSessionData.setTrackId(trackVersionJobj.getJSONObject(S.trackId).has(S._id)?trackVersionJobj.getJSONObject(S.trackId).getString(S._id):"");
                                beanSessionData.setTrackName(trackVersionJobj.getJSONObject(S.trackId).has(S.name)?trackVersionJobj.getJSONObject(S.trackId).getString(S.name):"");

                            }
                        }
                        JSONObject userVehicleJobj = new JSONObject();
                        userVehicleJobj = lapTimeIdJObj.has(S.userVehicleId)?lapTimeIdJObj.getJSONObject(S.userVehicleId):null;
                        if(userVehicleJobj!=null&&userVehicleJobj.length()>0){
                            beanSessionData.setUserVehicleId(userVehicleJobj.has(S._id)?userVehicleJobj.getString(S._id):"");
                            beanSessionData.setUserVehicletypeId(userVehicleJobj.getJSONObject(S.vehicleTypeId).has(S._id)?userVehicleJobj.getJSONObject(S.vehicleTypeId).getString(S._id):"");
                            beanSessionData.setUserVehicletypeName(userVehicleJobj.getJSONObject(S.vehicleTypeId).has(S.name)?userVehicleJobj.getJSONObject(S.vehicleTypeId).getString(S.name):"");
                            beanSessionData.setUserVehicleBrandId(userVehicleJobj.getJSONObject(S.vehicleBrandId).has(S._id)?userVehicleJobj.getJSONObject(S.vehicleBrandId).getString(S._id):"");
                            beanSessionData.setUserVehicleModelId(userVehicleJobj.getJSONObject(S.vehicleModelId).has(S._id)?userVehicleJobj.getJSONObject(S.vehicleModelId).getString(S._id):"");
                            beanSessionData.setUserVehicleModelName(userVehicleJobj.getJSONObject(S.vehicleModelId).has(S.vehicleModelName)?userVehicleJobj.getJSONObject(S.vehicleModelId).getString(S.vehicleModelName):"");

                        }
                        beanSessionData.setVehicletyreModelId(sessionJObj.getJSONObject(S.vehicleTyreModelId).getString(S._id));
                        beanSessionData.setVehicletyreModelName(sessionJObj.getJSONObject(S.vehicleTyreModelId).getString(S.vehicleTyreModelName));
                        beanSessionData.setVehicletyreBrandId(sessionJObj.getJSONObject(S.vehicleTyreBrandId).getString(S._id));
                        beanSessionData.setVehicletyreBrandName(sessionJObj.getJSONObject(S.vehicleTyreBrandId).getString(S.brandName));

                        JSONArray lapJArray = new JSONArray();
                        lapJArray = sessionJObj.has(S.laps)?sessionJObj.getJSONArray(S.laps):null;
                        if(lapJArray!=null&&lapJArray.length()>0){
                            ArrayList lapArray = new ArrayList();
                            for (int j = 0; j < lapJArray.length(); j++) {
                                JSONObject lapObj = new JSONObject();
                                lapObj = lapJArray.getJSONObject(j);
                                BeanSessionData.Laps laps = new BeanSessionData().new Laps();
                                laps.setLapsId(lapObj.has(S._id)?lapObj.getString(S._id):"");
                                laps.setLapsIndex(lapObj.has(S.lapIndex)?lapObj.getString(S.lapIndex):"");
                                laps.setAvgSpeed(lapObj.has(S.averageSpeed)?lapObj.getString(S.averageSpeed):"");
                                laps.setMaxSpeed(lapObj.has(S.maxSpeed)?lapObj.getString(S.maxSpeed):"");
                                laps.setTime(lapObj.has(S.time)?lapObj.getString(S.time):"");
                                laps.setFileUrl(lapObj.has(S.fileUrl)?lapObj.getString(S.fileUrl):"");
                                laps.setStatus(lapObj.has(S.status)?lapObj.getString(S.status):"");
                                lapArray.add(laps);
                            }
                            beanSessionData.setLapList(lapArray);
                        }
                        JSONArray imagesJarray = new JSONArray();
                        imagesJarray = sessionJObj.has(S.sessionImages)?sessionJObj.getJSONArray(S.sessionImages):null;
                        if(imagesJarray!=null&&imagesJarray.length()>0){
                            ArrayList<PagerBean> imageArray = new ArrayList<>();
                            imageArray.add(new PagerBean(null, null, null, true, false, ""));
                            for (int j = 0; j < imagesJarray.length(); j++) {
                                imageArray.add(new PagerBean(null, null, null, false, true, imagesJarray.getJSONObject(j).getString(S.fileUrl), false, imagesJarray.getJSONObject(j).getString(S._id)));
                            }
                            beanSessionData.setSessionsImagesList(imageArray);
                        }
                        sessionArrayList.add(beanSessionData);
                    }
                }

                uploadExpandMyLapAdapter.notifyDataSetChanged();
                if (sessionArrayList.size() > 0) {
                    expendableView.expandGroup(0);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }
    public void showSession(){
        if (expendableView.getVisibility() != View.VISIBLE) {
            expendableView.setVisibility(View.VISIBLE);
            lapsLayout.setVisibility(View.GONE);
            sessionsMenuTv.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.theme_graycolor));
            lapsMenuTv.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white_color));

            sessionsMenuTv.setTextColor(ContextCompat.getColor(getActivity(), R.color.white_color));
            lapsMenuTv.setTextColor(ContextCompat.getColor(getActivity(), R.color.theme_graycolor));
        }
    }
    public void showLaps(){
        if (lapsLayout.getVisibility() != View.VISIBLE) {
            if (expandPosition != -1) {
                paddockUploadsLapsAdapter = new PaddockUploadsLapsAdapter(getContext(), sessionArrayList.get(expandPosition).getLapList(), getActivity());
                lapsRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
                lapsRecyclerview.setAdapter(paddockUploadsLapsAdapter);

                expendableView.setVisibility(View.GONE);
                lapsLayout.setVisibility(View.VISIBLE);
                sessionsMenuTv.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white_color));
                lapsMenuTv.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.theme_graycolor));

                sessionsMenuTv.setTextColor(ContextCompat.getColor(getActivity(), R.color.theme_graycolor));
                lapsMenuTv.setTextColor(ContextCompat.getColor(getActivity(), R.color.white_color));
            }

        }
    }
    @Override
    public void vehicleTvClick(int groupPosition) {
        selectedPosition = groupPosition;
        Intent intent = new Intent(getActivity(), DashboardVehicleListActivity.class);
        startActivityForResult(intent, VEHICLE_ACTIVITY_RESULT_CODE);
    }

    @Override
    public void onDateClick(int groupPosition) {
        selectedPosition = groupPosition;
        DatepickerFragment datePickerFragment = DatepickerFragment.newInstance(onDateSetListener);
        datePickerFragment.show(getActivity().getFragmentManager(), "date");

    }

    @Override
    public void tyreBrandTvClick(int groupPosition) {
        selectedPosition = groupPosition;
        showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_brand), vehicleTyreBrand_list, I.TYRE_BRAND);
    }

    @Override
    public void tyreModelTvClick(int groupPosition) {
        selectedPosition = groupPosition;
        showVehicleTypeDialog(context.getResources().getString(R.string.select_vehicle_tyre_model), vehicleTyreModel_list, I.TYRE_MODEL);
    }

    @Override
    public void weatherClick(int groupPosition, String weather) {
        sessionArrayList.get(groupPosition).setWeather(weather);
        uploadExpandMyLapAdapter.notifyDataSetChanged();
    }

    @Override
    public void saveTvClick(int groupPosition) {
        try {

            if (sessionArrayList.get(groupPosition).getDateOfRecording().isEmpty() || sessionArrayList.get(groupPosition).getDateOfRecording() == null) {
                Util.showToast(context, getString(R.string.please_select_date_of_recording));
                return;
            }

            if (sessionArrayList.get(groupPosition).getUserVehicleId().isEmpty() || sessionArrayList.get(groupPosition).getUserVehicleId() == null) {
                Util.showToast(context, getString(R.string.please_select_vehicle));
                return;
            }
            if (sessionArrayList.get(groupPosition).getVehicletyreBrandId().isEmpty()) {
                Util.showToast(context, getString(R.string.vehicle_tyrebrand_empty_error));
                return;
            }
            if (sessionArrayList.get(groupPosition).getVehicletyreModelId().isEmpty()) {
                Util.showToast(context, getString(R.string.vehicle_tyremodel_empty_error));
                return;
            }

            ArrayList<PagerBean> pagerBeanArrayList = new ArrayList<>();
            pagerBeanArrayList = getNewFileArray();

            JSONArray lapArray = new JSONArray();
            for (int i = 0; i < sessionArrayList.get(groupPosition).getLapList().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("lapId", sessionArrayList.get(groupPosition).getLapList().get(i).getLapsId());
                jsonObject.put("status", sessionArrayList.get(groupPosition).getLapList().get(i).getStatus());
                lapArray.put(jsonObject);
            }
            authAPI.updateUploadLapTime(context, MySharedPreferences.getPreferences(context, S.user_id), sessionArrayList.get(groupPosition).getSessionId(), sessionArrayList.get(groupPosition).getSessionIndex(), lapArray.toString(), getNewFileArray().size(), sessionArrayList.get(groupPosition).getWeather(), sessionArrayList.get(groupPosition).getVehicletyreBrandId(), sessionArrayList.get(groupPosition).getVehicletyreModelId(), sessionArrayList.get(groupPosition).getNotes(), sessionArrayList.get(groupPosition).getYoutubeUrl(), pagerBeanArrayList);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void getDeleteImageSessionDataResponse(String response)
    {

    }
    public static class DatepickerFragment extends DialogFragment {

        DatePickerDialog.OnDateSetListener onDateSetListener;

        public static DatepickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener) {
            DatepickerFragment datepickerFragment = new DatepickerFragment();
            datepickerFragment.onDateSetListener = onDateSetListener;
            return datepickerFragment;
        }

        public Dialog onCreateDialog(Bundle savedInstance) {

            // create Calendar Instance from Calendar class
            final Calendar calender = Calendar.getInstance();
            int year = calender.get(Calendar.YEAR);
            int month = calender.get(Calendar.MONTH);
            int day = calender.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dateFragment = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_DARK, onDateSetListener, year, month, day);
            dateFragment.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            return dateFragment;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
        setDate(cal);
    }

    public void setDate(Calendar calender) {
        Date current = calender.getTime();
        int diff1 = new Date().compareTo(current);
        if (diff1 < 0) {
            Util.showSnackBar(expendableView, getString(R.string.dob_valid_error));
        } else {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sessionArrayList.get(selectedPosition).setDateOfRecording(dateFormat.format(calender.getTime()));
            uploadExpandMyLapAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAddImageCLick(int groupPostion, int position) {
        selectedPosition = groupPostion;
        if (sessionArrayList.get(groupPostion).getSessionsImagesList().get(position).isDefault()) {
            if (sessionArrayList.get(groupPostion).getSessionsImagesList().size() <= 4) {
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    //CHECKING PERMISSION FOR MARSHMALLOW
                    checkRequestPermission();
                } else {
                    showImageDialog();
                }
            } else {
                Util.showAlertDialog(context, getString(R.string.app_name), getResources().getString(R.string.choose_only_4_pics));
            }
        }
    }

    @Override
    public void onDeleteImageClick(int groupPostion, int position) {
        selectedPosition = groupPostion;
        authAPI.deleteSessionImage(context, MySharedPreferences.getPreferences(context, S.user_id), sessionArrayList.get(selectedPosition).getSessionId(), sessionArrayList.get(selectedPosition).getSessionsImagesList().get(position).getImage_id());
        /* check before delete image is for edit or add*/
        if (!sessionArrayList.get(groupPostion).getSessionsImagesList().get(position).isEdit())
            sessionArrayList.get(groupPostion).getSessionsImagesList().remove(position);
        else {
            sessionArrayList.get(groupPostion).getSessionsImagesList().remove(position);
        }
        // refresh adapter
        uploadExpandMyLapAdapter.notifyDataSetChanged();
    }
}
