package com.os.fastlap.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.activity.AllUsersActivity;
import com.os.fastlap.adapter.ContactUserAdapter;
import com.os.fastlap.beans.Bean_Contact;
import com.os.fastlap.delegates.ActivityCommunicator;
import com.os.fastlap.delegates.FragmentCommunicatorTwo;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by riyazudinp on 6/17/2017.
 */

@SuppressLint("ValidFragment")
public class ContactFragment extends Fragment implements FragmentCommunicatorTwo {

    private Context context;
    private View mView;
    private RecyclerView recycler_view;
    List<Bean_Contact> mArrayList = new ArrayList<>();
    private ProgressDialog mProgressDialog;
    private ActivityCommunicator activityCommunicator;
    private ContactUserAdapter contactUserAdapter;

    @SuppressLint("ValidFragment")
    public ContactFragment(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.recycler_view_layout, container, false);
            initView();
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                /***********CHECKING PERMISSION FOR MARSHMALLOW****************/
                checkRequestPermission();
            } else {
                new LoadContactsAsyncTast().execute();
            }
        }

        return mView;
    }

    private void initView() {
        recycler_view = (RecyclerView) mView.findViewById(R.id.recycler_view);
    }

    @Override
    public void passDataToFragment(String someValue) {
        if (contactUserAdapter != null)
            contactUserAdapter.filter(someValue);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCommunicator = (ActivityCommunicator) context;
        ((AllUsersActivity) context).fragmentCommunicatorTwo = this;
    }

    public class LoadContactsAsyncTast extends AsyncTask<Void, ArrayList<Bean_Contact>, List<Bean_Contact>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(context, "", "Loading", true);
        }

        @Override
        protected List<Bean_Contact> doInBackground(Void... params) {
            try {
                try {
                    ContentResolver cr = context.getContentResolver();
                    String selection = ContactsContract.Contacts._ID + " > '" + 0 + "'";
                    Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, selection, null, null);
                    if (cur.getCount() > 0) {
                        while (cur.moveToNext()) {
                            String phone = "";
                            String emailContact = "";
                            String image_uri = "";
                            try {
                                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                image_uri = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                                    Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                                    while (emailCur.moveToNext()) {
                                        emailContact = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                    }
                                    emailCur.close();
                                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                                    while (pCur.moveToNext()) {
                                        Bean_Contact mBean_Contact = new Bean_Contact();
                                        phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                        mBean_Contact.setPersonName(name);
                                        mBean_Contact.setPersonImage(image_uri);
                                        mBean_Contact.setPersonNumber(phone.replaceAll("[^0-9+]", ""));
                                        mBean_Contact.setPersonEmail(emailContact);
                                        mArrayList.add(mBean_Contact);
                                    }
                                    pCur.close();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return mArrayList;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return mArrayList;
        }

        @SafeVarargs
        @Override
        protected final void onProgressUpdate(ArrayList<Bean_Contact>... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(List<Bean_Contact> mList) {
            super.onPostExecute(mList);
            try {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                if (mArrayList.size() != 0) {
                    setAdapterIntoListView();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void setAdapterIntoListView() {

        contactUserAdapter = new ContactUserAdapter(context, mArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recycler_view.setAdapter(contactUserAdapter);
    }

    public void inviteViaMessage(String personNumber) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", personNumber);
        smsIntent.putExtra("sms_body", "" + getString(R.string.message_string));
        startActivity(smsIntent);
    }

    private void checkRequestPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.READ_CONTACTS}, 1);
        } else {
            new LoadContactsAsyncTast().execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        int i = 0;
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new LoadContactsAsyncTast().execute();
            } else {
                //mSelectImageClass = new SelectImageClass(context, getActivity(), "");
                // System.exit(0);
            }
        }
    }
}
