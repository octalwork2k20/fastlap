package com.os.fastlap.fragment.uploadlaptime;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.UploadLapTimeActivity;
import com.os.fastlap.adapter.uploadlaptime.UploadLapTimeStep3Adapter;
import com.os.fastlap.beans.BeanStep3Data;
import com.os.fastlap.beans.PagerBean;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.OnImagePickerDialogSelect;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by abhinava on 7/10/2017.
 */

public class UploadLapTimeStep3Fragment extends Fragment implements View.OnClickListener, UploadLapTimeStep3Adapter.OnItemClickListener {
    static Context context;
    static Object object;
    View rootview;


    private TextView backTv;
    private TextView publishTv;
    private RecyclerView recyclerView;

    String getImagePathProfile = "";
    private int REQUEST_CAMERA_PROFILE = 34;
    private int SELECT_FILE_PROFILE = 45;

    AuthAPI authAPI;
    private String TAG = UploadLapTimeStep3Fragment.class.getSimpleName();
    OnImagePickerDialogSelect onImagePickerDialogSelect;
    ArrayList<BeanStep3Data> step3DataList;
    int itemPostion = 0;

    UploadLapTimeStep3Adapter uploadLapTimeStep3Adapter;

    public static UploadLapTimeStep3Fragment newInstance(Context contex, Object obj) {
        UploadLapTimeStep3Fragment f = new UploadLapTimeStep3Fragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.upload_lap_time_step3, container, false);
            context = getActivity();
        }
        initView(rootview);
        ((UploadLapTimeActivity) context).setStep3Fragment();


        return rootview;
    }

    private void initView(View rootview) {
        authAPI = new AuthAPI(context);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerView);

        backTv = (TextView) rootview.findViewById(R.id.back_tv);
        publishTv = (TextView) rootview.findViewById(R.id.publish_tv);
        step3DataList = new ArrayList<>();
        step3DataList.clear();

        uploadLapTimeStep3Adapter = new UploadLapTimeStep3Adapter(context, step3DataList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(uploadLapTimeStep3Adapter);
        uploadLapTimeStep3Adapter.setItemClickListener(this);

        clickListner();

        getSessionList();
    }

    private void getSessionList() {

        for (int i = 0; i < UploadLapTimeActivity.beanUploadData.getSelectedValue().length(); i++) {
            try
            {
                BeanStep3Data beanStep3Data = new BeanStep3Data();
                beanStep3Data.setSessionId(UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(i).getString("session"));
              //  beanStep3Data.setSessionNo(UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(i).getString("sessionText"));
                ArrayList<PagerBean> session1_pagerBeanArrayList = new ArrayList<>();
                session1_pagerBeanArrayList.add(new PagerBean(null, null, null, true, false, ""));
                beanStep3Data.setImageList(session1_pagerBeanArrayList);
                step3DataList.add(beanStep3Data);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        uploadLapTimeStep3Adapter.notifyDataSetChanged();
    }

    private void clickListner() {
        publishTv.setOnClickListener(this);
        backTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.publish_tv:
                // UploadLapTimeActivity.beanUploadData.setImageArrayList(session1_pagerBeanArrayList);
                //  UploadLapTimeActivity.beanUploadData.setLapVideoURL(session1UrlTv.getText().toString().trim());

                for (int i = 0; i < step3DataList.size(); i++) {
                    BeanStep3Data beanStep3Data = new BeanStep3Data();
                    beanStep3Data = step3DataList.get(i);
                    try {
                        UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(i).put("url", beanStep3Data.getVideoURL());
                        UploadLapTimeActivity.beanUploadData.getSelectedValue().getJSONObject(i).put("files", 0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                authAPI.uploadLapTimeData(context, MySharedPreferences.getPreferences(context, S.user_id), UploadLapTimeActivity.beanUploadData, step3DataList, getImageCount());
                break;

            case R.id.back_tv:
                ((UploadLapTimeActivity) getActivity()).OnBackCross();
                break;
        }
    }

    private int getImageCount() {
        int count = 0;
        for (int i = 0; i < step3DataList.size(); i++) {
            for (int j = 0; j < step3DataList.get(i).getImageList().size(); j++) {
                if (!step3DataList.get(i).getImageList().get(j).isDefault()) {
                    count++;
                }
            }

        }

        return count;
    }

    // Youtube URL get Data Response
    public void getYoutubeURLData(String response, int position) {
        System.out.println("response:-- " + response);
        String videoImage = "";
        String channelTitle = "";
        String VideoTitle = "";

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            if (jsonArray.length() > 0) {
                videoImage = jsonArray.getJSONObject(0).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url");
                channelTitle = jsonArray.getJSONObject(0).getJSONObject("snippet").getString("channelTitle");
                VideoTitle = jsonArray.getJSONObject(0).getJSONObject("snippet").getString("title");

                step3DataList.get(position).setVideoImage(videoImage);
                step3DataList.get(position).setChannelTitle(channelTitle);
                step3DataList.get(position).setVideoTitle(VideoTitle);
                uploadLapTimeStep3Adapter.notifyDataSetChanged();

            } else {
                Util.showSnackBar(publishTv, getString(R.string.invalid_youtube_url));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                File imageFile = file;

                step3DataList.get(itemPostion).getImageList().add(new PagerBean(imageFile, imageFile.getAbsolutePath(), imageFile.getName(), false, false, ""));
                uploadLapTimeStep3Adapter.notifyDataSetChanged();


            }


        });

    }

    @Override
    public void OnCamSelect(int postion) {
        itemPostion = postion;
        EasyImage.openCamera(getActivity(), REQUEST_CAMERA_PROFILE);
    }

    @Override
    public void OnGallSelect(int postion) {
        itemPostion = postion;
        EasyImage.openGallery(getActivity(), REQUEST_CAMERA_PROFILE);
    }
}
