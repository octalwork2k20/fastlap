package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;

import org.json.JSONObject;

/**
 * Created by abhinava on 7/27/2017.
 */

public class ChangeEmailFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private EditTextPlayRegular currentEt;
    private EditTextPlayRegular newEmailEt;
    private EditTextPlayRegular confirmNewEmailEt;
    private TextViewPlayBold EmailSaveTv;
    private TextViewPlayBold EmailDeleteTv;
    private AuthAPI authAPI;
    private String TAG = "ChangePasswordFragment.java";

    public static ChangeEmailFragment newInstance(Context contex, Object obj) {
        ChangeEmailFragment f = new ChangeEmailFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_change_email, container, false);
            context = getActivity();
        }
        initView(rootview);


        return rootview;
    }


    private void initView(View rootview) {
        authAPI = new AuthAPI(getContext());
        currentEt = rootview.findViewById(R.id.current_et);
        newEmailEt = rootview.findViewById(R.id.new_email_et);
        confirmNewEmailEt = rootview.findViewById(R.id.confirm_new_email_et);
        EmailSaveTv = rootview.findViewById(R.id.email_save_tv);
        EmailDeleteTv = rootview.findViewById(R.id.email_delete_tv);

        clickListner();
    }

    private void clickListner() {
        EmailSaveTv.setOnClickListener(this);
        EmailDeleteTv.setOnClickListener(this);
    }

    public void getChangeEmailResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                MySharedPreferences.setPreferences(context,newEmailEt.getText().toString().trim(),S.email);
                Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            } else
                Util.showAlertDialog(getContext(), getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.email_save_tv:
                Util.hideKeyboard(getActivity().getCurrentFocus(), getContext());
                if (Validation.changeEmailValidation(currentEt, newEmailEt, confirmNewEmailEt, EmailSaveTv, getContext()))
                    authAPI.changeEmail(getContext(), MySharedPreferences.getPreferences(getContext(), S.user_id), newEmailEt.getText().toString());
                break;
            case R.id.email_delete_tv:
                //getActivity().onBackPressed();
                break;
        }

    }
}
