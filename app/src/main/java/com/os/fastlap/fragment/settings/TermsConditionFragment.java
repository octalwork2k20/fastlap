package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONObject;

/**
 * Created by abhinava on 7/27/2017.
 */

public class TermsConditionFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private TextViewPlayRegular termsAndConditionsTv;
    private TextViewPlayBold downloadPdfTv;
    AppCompatCheckBox termsCheckbox;
    private String TAG = "TermsConditionFragment";

    public static TermsConditionFragment newInstance(Context contex, Object obj) {
        TermsConditionFragment f = new TermsConditionFragment();
        context = contex;
        object = obj;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_terms_conditions, container, false);
            context = getActivity();
        }
        initView(rootview);


        return rootview;
    }

    private void initView(View rootview) {
        termsAndConditionsTv = rootview.findViewById(R.id.terms_and_conditions_tv);
        downloadPdfTv = rootview.findViewById(R.id.download_pdf_tv);
        termsCheckbox = rootview.findViewById(R.id.terms_checkbox);
        clickListner();
        new AuthAPI(getContext()).getStaticPageInfo(getContext(), S.terms_and_conditions);

        if (object != null) {
            termsCheckbox.setVisibility(View.VISIBLE);
        }

        termsCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                getActivity().finish();
            }
        });
    }

    private void clickListner() {
        downloadPdfTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.download_pdf_tv:
                break;
        }
    }
    // get Page content
    public void getPageInformation(String details) {
        try {
            JSONObject jsonObject = new JSONObject(details);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONObject dataJson = jsonObject.getJSONObject(S.data);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    termsAndConditionsTv.setText(Html.fromHtml(dataJson.getString(S.description)), TextView.BufferType.SPANNABLE);
                }
                else {
                    termsAndConditionsTv.setText(Html.fromHtml(dataJson.getString(S.description)));

                }

            } else
                Util.showAlertDialog(getContext(), getString(R.string.alert), msg);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }
}
