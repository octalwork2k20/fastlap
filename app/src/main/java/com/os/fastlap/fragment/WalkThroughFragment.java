package com.os.fastlap.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.os.fastlap.R;


/**
 * Created by abhinava on 1/12/2017.
 */

public class WalkThroughFragment extends Fragment {

    private static final String ARG_PAGE_NUMBER = "page_number";
    View rootview;
    Context mContext;
    // TextView textView;
    RelativeLayout layout;
   // ImageView mImgWalk;
    ImageView walkImage;
    TextView walkTitle;
    TextView walkDesc;


    @SuppressLint("ValidFragment")
    public static WalkThroughFragment newInstance(int page) {
        WalkThroughFragment fragment = new WalkThroughFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.walkthroughfragment, container, false);
        mContext = getActivity();
        finViewbyID();

        Bundle bundle = getArguments();
        try {
            if (bundle != null) {
                int page = bundle.getInt(ARG_PAGE_NUMBER);
                if (page == 0) {
                    walkImage.setBackgroundResource(R.drawable.one);
                     walkTitle.setText(getResources().getString(R.string.walk_title1));
                     walkDesc.setText(getResources().getString(R.string.walk_desc1));
                }
                if (page == 1) {
                    walkImage.setBackgroundResource(R.drawable.two);
                    walkTitle.setText(getResources().getString(R.string.walk_title2));
                    walkDesc.setText(getResources().getString(R.string.walk_desc2));
                    //walkImage.setText(getResources().getString(R.string.walk_text2));
                }
                if (page == 2) {
                    walkImage.setBackgroundResource(R.drawable.three);
                    walkTitle.setText(getResources().getString(R.string.walk_title3));
                    walkDesc.setText(getResources().getString(R.string.walk_desc3));
                    //textView.setText(getResources().getString(R.string.walk_text3));
                }

            } else {
                walkImage.setBackgroundResource(R.drawable.one);
                walkTitle.setText(getResources().getString(R.string.walk_title1));
                walkDesc.setText(getResources().getString(R.string.walk_desc1));
                //textView.setText(getResources().getString(R.string.walk_text1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootview;
    }

    private void finViewbyID() {
        //  textView=(TextView)rootview.findViewById(R.id.text);
        layout = (RelativeLayout) rootview.findViewById(R.id.layout);
       // mImgWalk=(ImageView)rootview.findViewById(R.id.imgWalk);
        walkImage=(ImageView)rootview.findViewById(R.id.walkImage);
        walkTitle=(TextView)rootview.findViewById(R.id.walkTitle);
        walkDesc=(TextView) rootview.findViewById(R.id.walkDesc);

    }

}
