package com.os.fastlap.fragment.explore;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;



/*
 * Created by riyazudinp on 6/17/2017.
 */

@SuppressLint("ValidFragment")
public class ExploreWeatherFragment extends Fragment {

    private Context mContext;
    private View mView;
    TextView weather_text;
    String low;
    String high;
    String country;


    @SuppressLint("ValidFragment")
    public ExploreWeatherFragment(final String low,final String high,final String country ) {
        this.low = low;
        this.high = high;
        this.country=country;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.explore_weather_fragment, container, false);

        }
        initView(mView);
        weather_text.setText(country+" "+high+getResources().getString(R.string.temp_high)+" "+low+getResources().getString(R.string.temp_low));
        return mView;
    }
   /* @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCommunicator = (ActivityCommunicator) mContext;
        ((ExploreActivity) mContext).fragmentCommunicator = this;
    }*/

    private void initView(View mView) {
        weather_text = (TextView) mView.findViewById(R.id.weather_text);
    }
}
