package com.os.fastlap.fragment.paddock;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsActivity;
import com.os.fastlap.activity.mylaps.MyLapsRaceActivity;
import com.os.fastlap.adapter.VehicleTypeSpinnerAdapter;
import com.os.fastlap.adapter.paddock.PaddockMyLapAdapter;
import com.os.fastlap.beans.BeanTimeLap;
import com.os.fastlap.beans.BeanVehicleType;
import com.os.fastlap.beans.PaddockMyLapsParentModel;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.delegates.MyApiEndpointInterface;
import com.os.fastlap.delegates.SpinnerSelectorInterface;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.os.fastlap.activity.DashboardActivity.TAG;
import static com.os.fastlap.util.constants.S.user_id;
/**
 * Created by anandj on 7/18/2017.
 */

@SuppressLint("ValidFragment")
public class PaddockMyLapsSubFragment extends Fragment implements PaddockMyLapAdapter.OnClick,SpinnerSelectorInterface {
    private PaddockMyLapAdapter paddockMyLapsAdapter;
    private ArrayList<PaddockMyLapsParentModel> my_laps_list;
    private ExpandableListView expandable_list_my_laps;
    TextView no_data_tv;
    int type;
    private static final int DIALOG_ID = 0;
    private Context mContext;

    //its for filter dialog class
    String vehicleTypeId = "";
    String vehicleBrandID = "";
    String fromVehicleYearId = "";
    String toVehicleYearId = "";
    String trackCondition = "";
    TextViewPlayRegular classTv;
    TextViewPlayRegular categoryTv;
    TextViewPlayRegular brandTv;
    TextViewPlayRegular fromYearTv;
    TextViewPlayRegular toYearTv;
    TextViewPlayRegular trackConditionTv;
    private ArrayList<BeanVehicleType> vehicleTypes_list;
    private ArrayList<BeanVehicleType> vehicleBrand_list;
    private ArrayList<BeanVehicleType> vehicleYear_list;
    private ArrayList<BeanVehicleType> trackCondition_list;
    private MyProgressDialog mProgressDialog;
    SpinnerSelectorInterface spinnerSelectorInterface;
    String trackVersionId="0";
    int index=1;
    public FloatingActionButton mFiterButton;
    private String TAG_BEST_LAP = "BestLap";
    private String TAG_BEST_FRIEND = "FriendLap";


    @SuppressLint("ValidFragment")
    public PaddockMyLapsSubFragment(int favoritetracks,final String trackVersionId) {
        this.trackVersionId=trackVersionId;
        type = favoritetracks;
    }
    public PaddockMyLapsSubFragment() {
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mylaps_sub_fragment, container, false);
        mContext=getActivity();
        spinnerSelectorInterface=this;
        mProgressDialog=new MyProgressDialog(getActivity());
        initView(view);
        return view;
    }
    private void initView(View view) {
        my_laps_list = new ArrayList<>();
        mFiterButton=(FloatingActionButton)view.findViewById(R.id.img_filter);
        expandable_list_my_laps = (ExpandableListView) view.findViewById(R.id.expandable_list_my_laps);
        no_data_tv = (TextView) view.findViewById(R.id.no_data_tv);
        paddockMyLapsAdapter = new PaddockMyLapAdapter(getContext(), my_laps_list,PaddockMyLapsSubFragment.this);
        expandable_list_my_laps.setAdapter(paddockMyLapsAdapter);
        paddockMyLapsAdapter.setOnClickListner(this);

        mFiterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDataDialog();
            }
        });
        expandable_list_my_laps.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandable_list_my_laps.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });
    }
    public void getListData() {
        my_laps_list.clear();
        if (type == I.ALLTRACKS) {
            my_laps_list.addAll(MyLapsActivity.myLapsList);
        } else {
            for (int i = 0; i < MyLapsActivity.myLapsList.size(); i++) {
                if (MyLapsActivity.myLapsList.get(i).getIsFav().equalsIgnoreCase("1"))
                    my_laps_list.add(MyLapsActivity.myLapsList.get(i));
            }
        }
        paddockMyLapsAdapter.notifyDataSetChanged();

        if (my_laps_list.size() > 0) {
            no_data_tv.setVisibility(View.GONE);
            expandable_list_my_laps.setVisibility(View.VISIBLE);
        } else {
            no_data_tv.setVisibility(View.VISIBLE);
            expandable_list_my_laps.setVisibility(View.GONE);
        }
    }
    // search class
    public void filter(String ch) {
        my_laps_list.clear();
        if (ch.isEmpty())
            my_laps_list.addAll(MyLapsActivity.myLapsList);
        else {
            for (PaddockMyLapsParentModel wp : MyLapsActivity.myLapsList) {
                if (wp.getTrackCountry().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())) ||
                        wp.getTrackCountry().toLowerCase(Locale.getDefault()).contains(ch.toLowerCase(Locale.getDefault())))
                    my_laps_list.add(wp);
            }
        }
        paddockMyLapsAdapter.notifyDataSetChanged();
    }
    // search class
    public void filterVehicle(String id) {
        my_laps_list.clear();
        if (id.isEmpty())
            my_laps_list.addAll(MyLapsActivity.myLapsList);
        else {
            for (PaddockMyLapsParentModel wp : MyLapsActivity.myLapsList) {
                ArrayList<BeanTimeLap> timeLapArray = new ArrayList<>();
                timeLapArray.clear();

                for (BeanTimeLap child : wp.getMyLapsChildList()) {
                    if (child.getGroupTypeId().compareToIgnoreCase(id) == 0) {
                        timeLapArray.add(child);
                    }
                }

                if (timeLapArray.size() > 0) {
                    wp.setMyLapsChildList(timeLapArray);
                    my_laps_list.add(wp);
                }
            }
        }
        paddockMyLapsAdapter.notifyDataSetChanged();
    }
    @Override
    public void onRaceClick(final int groupPosition, final ArrayList<Integer> selectIds) {
        MyLapsRaceActivity.isSpeed=true;MyLapsRaceActivity.isBrakeOn=false;
        MyLapsRaceActivity.isAccOn=false;MyLapsRaceActivity.isBrakeCalc=false;
        MyLapsRaceActivity.isAccCalc=false;MyLapsRaceActivity.isXView=false;MyLapsRaceActivity.isYView=false;

        if(MySharedPreferences.getBooleanPreferences(mContext,S.checkSubscription)){
            mProgressDialog.show();
            if(selectIds.size()>11){
                Util.showAlertDialog(mContext,getString(R.string.app_name),mContext.getResources().getString(R.string.max_limit_comprision_10));
            }
            else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog.dismiss();
                        Intent intent = new Intent(getActivity(), MyLapsRaceActivity.class);
                        intent.putExtra("grouplist", my_laps_list.get(groupPosition));
                        intent.putIntegerArrayListExtra("selectIds", selectIds);
                        startActivity(intent);
                    }
                },3000);
            }

        }
        else {
            if(selectIds.size()>2){
                Util.showAlertDialog(mContext,getString(R.string.app_name),mContext.getResources().getString(R.string.Lap_comparison_for_free_version_maximum_2_laps_compare));
            }
            else {
                mProgressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog.dismiss();
                        Intent intent = new Intent(getActivity(), MyLapsRaceActivity.class);
                        intent.putExtra("grouplist", my_laps_list.get(groupPosition));
                        intent.putIntegerArrayListExtra("selectIds", selectIds);
                        startActivity(intent);
                    }
                },3000);
            }
        }
    }
    @Override
    public void onFilterDataClick(String groupPosition, String childPosition,int index) {
        trackVersionId=groupPosition;
        if(childPosition.equalsIgnoreCase("true")) {
            mFiterButton.setVisibility(View.VISIBLE);
            if(!MyLapsActivity.selectedItem.contains(index)) {
                MyLapsActivity.selectedItem.add(index);
                try {
                    this.index=index;
                    JSONObject jsonObject1=new JSONObject();
                    jsonObject1.put("userId",MySharedPreferences.getPreferences(mContext,S.user_id));
                    jsonObject1.put("trackVersionId",trackVersionId);
                    JsonParser jsonParser=new JsonParser();
                    JsonObject jsonObject2= (JsonObject) jsonParser.parse(jsonObject1.toString());
                    Log.e("tag","Object"+jsonObject1);
                    callBestLapApi(jsonObject2);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            if(MyLapsActivity.selectedItem.size()==0){
                MyLapsActivity.selectedItem.add(index);
                try {
                    this.index=index;
                    JSONObject jsonObject1=new JSONObject();
                    jsonObject1.put("userId",MySharedPreferences.getPreferences(mContext,S.user_id));
                    jsonObject1.put("trackVersionId",trackVersionId);
                    JsonParser jsonParser=new JsonParser();
                    JsonObject jsonObject2= (JsonObject) jsonParser.parse(jsonObject1.toString());
                    Log.e("tag","Object"+jsonObject1);
                    callBestLapApi(jsonObject2);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }

        }
        else {
            mFiterButton.setVisibility(View.INVISIBLE);
        }

    }
    @Override
    public void onColorPickerDialog(final int groupPosition, final int childPosition, int color) {
        ColorPickerDialogBuilder
                .with(getActivity())
                .setTitle(getString(R.string.choose_color))
                .initialColor(color)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                    }
                })
                .setPositiveButton(getString(R.string.ok), new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        my_laps_list.get(groupPosition).getMyLapsChildList().get(childPosition).setLapColor(selectedColor);
                        paddockMyLapsAdapter.notifyDataSetChanged();
                        // Toast.makeText(getActivity(), "Selected Color: #" +  Integer.toHexString(selectedColor), Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }
    private void filterDataDialog()
    {
        vehicleTypeId="";
        vehicleBrandID="";
        fromVehicleYearId="";
        toVehicleYearId="";
        trackCondition="";

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prefer_track_filter_dialog);


        ImageView closeBtn;
         TextViewPlayBold closeTv;
        TextViewPlayBold okTv;
        ImageView cross_iv;

        classTv = (TextViewPlayRegular) dialog.findViewById(R.id.class_tv);
        categoryTv = (TextViewPlayRegular) dialog.findViewById(R.id.category_tv);
        brandTv = (TextViewPlayRegular) dialog.findViewById(R.id.brand_tv);
        fromYearTv = (TextViewPlayRegular) dialog.findViewById(R.id.fromYearTv);
        toYearTv = (TextViewPlayRegular) dialog.findViewById(R.id.toYearTv);
        trackConditionTv = (TextViewPlayRegular) dialog.findViewById(R.id.track_condition_tv);
        closeBtn = (ImageView) dialog.findViewById(R.id.close_btn);
        closeBtn.setVisibility(View.GONE);
        closeTv = (TextViewPlayBold) dialog.findViewById(R.id.close_tv);
        closeTv.setVisibility(View.VISIBLE);
        okTv = (TextViewPlayBold) dialog.findViewById(R.id.ok_tv);
        okTv.setText(getResources().getString(R.string.ok));
        cross_iv = (ImageView) dialog.findViewById(R.id.cross_iv);

        vehicleTypes_list = new ArrayList<>();
        vehicleBrand_list = new ArrayList<>();
        vehicleYear_list = new ArrayList<>();
        trackCondition_list = new ArrayList<>();

        classTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getVehicleType(mContext);

            }
        });

        brandTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getVehicleBrand(mContext, vehicleTypeId);
            }
        });

        closeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        cross_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        fromYearTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vehicleYear_list.size() > 0);
                    showVehicleTypeDialog(mContext.getResources().getString(R.string.select_vehicle_year), vehicleYear_list, I.VEHICLE_YEAR);
            }
        });

        toYearTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vehicleYear_list.size() > 0);
                    showVehicleTypeDialog(mContext.getResources().getString(R.string.select_vehicle_year), vehicleYear_list, I.VEHICLE_YEAR2);
            }
        });

        trackConditionTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTrackCondition(mContext, MySharedPreferences.getPreferences(mContext, user_id));
            }
        });

        okTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("tag_octal","trackVersionId"+trackVersionId);
                JSONObject mainJson  = new JSONObject();
                JSONObject filterData  = new JSONObject();
                try {
                    filterData.put("vehicleTypeId", vehicleTypeId);
                    filterData.put("vehicleBrandId", vehicleBrandID);
                    filterData.put("vehicleYearFrom", fromVehicleYearId);
                    filterData.put("vehicleYearTo", toVehicleYearId);
                    filterData.put("weather", trackCondition);
                    filterData.put("trackVersionId", trackVersionId);
                    mainJson.put("filterData", filterData);
                    mainJson.put("userId", MySharedPreferences.getPreferences(mContext, S.user_id));
                    mainJson.put("groupTypeId", "");
                    JsonParser jsonParser=new JsonParser();
                    JsonObject jsonObject=jsonParser.parse(mainJson.toString()).getAsJsonObject();
                    Log.e("tag_octal","JSONObject"+jsonObject);
                    callMyLapApi(jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.cancel();
            }
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    private void callMyLapApi(JsonObject mainObject) {
          if(mProgressDialog!=null)
        mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getMyLapsListingFriends(mainObject)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {

                        getMyLapsWebserviceResponse(Util.convertRetrofitResponce(responseBodyResponse));
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
    private void callBestLapApi(JsonObject mainObject) {
        mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getMyBestLap(mainObject)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                        getMyBestLapsWebserviceResponse(Util.convertRetrofitResponce(responseBodyResponse));
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void getMyBestLapsWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray dataArray = jsonObject.getJSONArray(S.data);
                ArrayList<BeanTimeLap> userList = new ArrayList<>();
                userList.clear();
                for (int i = 0; i < dataArray.length(); i++)
                {
                    JSONObject jsonObject1 = dataArray.getJSONObject(i);
                    BeanTimeLap beanTimeLap = new BeanTimeLap();
                    if(i==0){
                        beanTimeLap.setLap_type("BEST_LAPS");
                    }
                    beanTimeLap.setMaxSpeed(jsonObject1.has(S.maxSpeed)?jsonObject1.getString(S.maxSpeed):"");
                    beanTimeLap.setLapId(jsonObject1.has(S.lapId)?jsonObject1.getString(S.lapId):"");
                    beanTimeLap.setTrackId(jsonObject1.has(S.trackId)?jsonObject1.getString(S.trackId):"");
                    beanTimeLap.setTimeLap_Id(jsonObject1.has(S.lapTimeId)?jsonObject1.getString(S.lapTimeId):"");
                    beanTimeLap.setTrackVersionId(jsonObject1.has(S.trackVersionId)?jsonObject1.getString(S.trackVersionId):"");
                    beanTimeLap.setFileUrl(jsonObject1.getString(S.fileUrl));
                    beanTimeLap.setAvgSpeed(jsonObject1.has(S.averageSpeed)?jsonObject1.getString(S.averageSpeed):"");
                    beanTimeLap.setAverageSpeedToShow(jsonObject1.has(S.averageSpeedToShow)?jsonObject1.getString(S.averageSpeedToShow):"");
                    beanTimeLap.setTime(jsonObject1.has(S.time)?jsonObject1.getString(S.time):"");
                    Random rnd = new Random();
                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                    beanTimeLap.setLapColor(color);

                    JSONObject userIdObject=new JSONObject();
                    userIdObject=jsonObject1.has(S.userId)?jsonObject1.getJSONObject(S.userId):null;;
                    beanTimeLap.setUserId(userIdObject.has(S._id)?userIdObject.getString(S._id):"");

                    JSONObject personalObject = userIdObject.has(S.personalInfo)?userIdObject.getJSONObject(S.personalInfo):null;
                    beanTimeLap.setUserImage(personalObject.getString(S.image));
                    beanTimeLap.setUserName(personalObject.getString(S.firstName_response) + " " + personalObject.getString(S.lastName_response));

                    JSONObject lapObject=new JSONObject();
                    lapObject = jsonObject1.has("lapTimeData")?jsonObject1.getJSONObject("lapTimeData"):null;
                    beanTimeLap.setTimeLap_Id(lapObject.has(S._id)?lapObject.getString(S._id):"");
                    beanTimeLap.setLapId(lapObject.has(S._id)?lapObject.getString(S._id):"");
                    beanTimeLap.setTrackVersionId(lapObject.has(S.trackVersionId)?lapObject.getString(S.trackVersionId):"");
                    beanTimeLap.setLapDate(lapObject.has(S.dateOfRecording)?lapObject.getString(S.dateOfRecording):"");


                    beanTimeLap.setVehicleModelName(lapObject.getJSONObject(S.userVehicleId).getJSONObject(S.vehicleModelId).getString(S.vehicleModelName));
                    beanTimeLap.setVehicleBrandName(lapObject.getJSONObject(S.userVehicleId).getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName));
                    userList.add(beanTimeLap);
                }
                my_laps_list.clear();
                my_laps_list.addAll(MyLapsActivity.myLapsList);
                List<BeanTimeLap> beanTimeLap=new ArrayList<>();
                beanTimeLap.addAll(my_laps_list.get(index).getMyLapsChildList());
                beanTimeLap.addAll(userList);
                my_laps_list.get(index).setMyLapsChildList(beanTimeLap);
                paddockMyLapsAdapter.notifyDataSetChanged();
                try {
                    JSONObject jsonObject1=new JSONObject();
                    jsonObject1.put("userId",MySharedPreferences.getPreferences(mContext,S.user_id));
                    jsonObject1.put("trackVersionId",trackVersionId);
                    JsonParser jsonParser=new JsonParser();
                    JsonObject jsonObject2= (JsonObject) jsonParser.parse(jsonObject1.toString());
                    Log.e("tag","Object"+jsonObject1);
                    callFriendLapApi(jsonObject2);
                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            Log.e(TAG_BEST_LAP, e.toString());
        }
    }
    private void callFriendLapApi(JsonObject mainObject) {
        if(mProgressDialog!=null)
            mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getMyFriendLap(mainObject)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                        getMyFriendLapsWebserviceResponse(Util.convertRetrofitResponce(responseBodyResponse));
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
    private void getMyFriendLapsWebserviceResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                JSONArray dataArray = jsonObject.getJSONArray(S.data);
                ArrayList<BeanTimeLap> userList = new ArrayList<>();
                for (int i = 0; i < dataArray.length(); i++)
                {
                    JSONObject jsonObject1 = dataArray.getJSONObject(i);
                    BeanTimeLap beanTimeLap = new BeanTimeLap();
                    if(i==0){
                        beanTimeLap.setLap_type("FRIENDS_LAPS");
                    }
                    beanTimeLap.setMaxSpeed(jsonObject1.has(S.maxSpeed)?jsonObject1.getString(S.maxSpeed):"");
                    beanTimeLap.setLapId(jsonObject1.has(S.lapId)?jsonObject1.getString(S.lapId):"");
                    beanTimeLap.setTrackId(jsonObject1.has(S.trackId)?jsonObject1.getString(S.trackId):"");
                    beanTimeLap.setTimeLap_Id(jsonObject1.has(S.lapTimeId)?jsonObject1.getString(S.lapTimeId):"");
                    beanTimeLap.setTrackVersionId(jsonObject1.has(S.trackVersionId)?jsonObject1.getString(S.trackVersionId):"");
                    beanTimeLap.setFileUrl(jsonObject1.getString(S.fileUrl));
                    beanTimeLap.setAvgSpeed(jsonObject1.has(S.averageSpeed)?jsonObject1.getString(S.averageSpeed):"");
                    beanTimeLap.setAverageSpeedToShow(jsonObject1.has(S.averageSpeedToShow)?jsonObject1.getString(S.averageSpeedToShow):"");
                    beanTimeLap.setTime(jsonObject1.has(S.time)?jsonObject1.getString(S.time):"");
                    Random rnd = new Random();
                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                    beanTimeLap.setLapColor(color);

                    JSONObject userIdObject=new JSONObject();
                    userIdObject=jsonObject1.has(S.userId)?jsonObject1.getJSONObject(S.userId):null;;
                    beanTimeLap.setUserId(userIdObject.has(S._id)?userIdObject.getString(S._id):"");

                    JSONObject personalObject = userIdObject.has(S.personalInfo)?userIdObject.getJSONObject(S.personalInfo):null;
                    beanTimeLap.setUserImage(personalObject.getString(S.image));
                    beanTimeLap.setUserName(personalObject.getString(S.firstName_response) + " " + personalObject.getString(S.lastName_response));

                    JSONObject lapObject=new JSONObject();
                    lapObject = jsonObject1.has("lapTimeData")?jsonObject1.getJSONObject("lapTimeData"):null;
                    beanTimeLap.setTimeLap_Id(lapObject.has(S._id)?lapObject.getString(S._id):"");
                    beanTimeLap.setLapId(lapObject.has(S._id)?lapObject.getString(S._id):"");
                    beanTimeLap.setTrackVersionId(lapObject.has(S.trackVersionId)?lapObject.getString(S.trackVersionId):"");
                    beanTimeLap.setLapDate(lapObject.has(S.dateOfRecording)?lapObject.getString(S.dateOfRecording):"");


                    beanTimeLap.setVehicleModelName(lapObject.getJSONObject(S.userVehicleId).getJSONObject(S.vehicleModelId).getString(S.vehicleModelName));
                    beanTimeLap.setVehicleBrandName(lapObject.getJSONObject(S.userVehicleId).getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName));
                    userList.add(beanTimeLap);
                }
                my_laps_list.clear();
                my_laps_list.addAll(MyLapsActivity.myLapsList);
                List<BeanTimeLap> beanTimeLap=new ArrayList<>();
                beanTimeLap.addAll(my_laps_list.get(index).getMyLapsChildList());
                beanTimeLap.addAll(userList);
                my_laps_list.get(index).setMyLapsChildList(beanTimeLap);
                paddockMyLapsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG_BEST_FRIEND, e.toString());
        }
    }
    public void getVehicleType(final Context context) {
        mProgressDialog=new MyProgressDialog(context);
        mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleType()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                        getVehicleTypeResponse(Util.convertRetrofitResponce(responseBodyResponse));
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getVehicleBrand(final Context context, String type_id) {
        mProgressDialog=new MyProgressDialog(context);
        mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleBrand(type_id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                        getVehicleBrandResponse(Util.convertRetrofitResponce(responseBodyResponse));
                       mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getTrackCondition(final Context context, String userId) {
        mProgressDialog=new MyProgressDialog(context);
        mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getTrackCondition(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                        getTrackConditionResponse(Util.convertRetrofitResponce(responseBodyResponse));
                        mProgressDialog.dismiss();
                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    // track condition in prefertrack filter
    public void getTrackConditionResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                trackCondition_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    beanVehicleType.setId(jsonArray.getString(i));
                    beanVehicleType.setName(jsonArray.getString(i));
                    trackCondition_list.add(beanVehicleType);
                }
                if (trackCondition_list.size() > 0)
                    showVehicleTypeDialog(mContext.getResources().getString(R.string.select_track_condition), trackCondition_list, I.TRACK_CONDITION);

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void getVehicleBrandResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleBrand_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.vehicleBrandName));
                    vehicleBrand_list.add(beanVehicleType);
                }
                if (vehicleBrand_list.size() > 0)
                    showVehicleTypeDialog(mContext.getResources().getString(R.string.select_vehicle_brand), vehicleBrand_list, I.VEHICLE_BRAND);
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void getVehicleTypeResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleTypes_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    beanVehicleType.setId(jsonObject1.getString(S._id));
                    beanVehicleType.setName(jsonObject1.getString(S.name));
                    vehicleTypes_list.add(beanVehicleType);
                }
                if (vehicleTypes_list.size() > 0)
                    showVehicleTypeDialog(mContext.getResources().getString(R.string.select_vehicle_type), vehicleTypes_list, I.VEHICLE_TYPE);
            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void showVehicleTypeDialog(String dialog_title, ArrayList<BeanVehicleType> vehicleList, int type) {
        if (vehicleList.size() != 0) {
            final Dialog dialog1 = new Dialog(mContext);
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.spin_dialog);
            TextView heading = (TextView) dialog1.findViewById(R.id.toolbarheading);
            RecyclerView recyclerView = (RecyclerView) dialog1.findViewById(R.id.recycler_view);
            ImageView base_toggle_icon = (ImageView) dialog1.findViewById(R.id.base_toggle_icon);
            TextView done_tv = dialog1.findViewById(R.id.done_tv);
            done_tv.setVisibility(View.GONE);
            base_toggle_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog1.dismiss();
                }
            });
            heading.setText(dialog_title);

            VehicleTypeSpinnerAdapter spinDialogAdapter = new VehicleTypeSpinnerAdapter(mContext, vehicleList, spinnerSelectorInterface, type, dialog1);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(spinDialogAdapter);

            dialog1.setCancelable(true);
            dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog1.getWindow().setGravity(Gravity.CENTER);
            dialog1.show();
        } else {
            if (type == I.VEHICLE_BRAND)
                Util.showAlertDialog(mContext, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.VEHICLE_TYPE)
                Util.showAlertDialog(mContext, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_type));
            if (type == I.VEHICLE_MODEL)
                Util.showAlertDialog(mContext, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
            if (type == I.VEHICLE_VERSION)
                Util.showAlertDialog(mContext, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_version));
            if (type == I.VEHICLE_YEAR)
                Util.showAlertDialog(mContext, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_year));
            if (type == I.TYRE_BRAND)
                Util.showAlertDialog(mContext, getString(R.string.app_name), getString(R.string.there_is_no_brand));
            if (type == I.TYRE_MODEL)
                Util.showAlertDialog(mContext, getString(R.string.app_name), getString(R.string.there_is_no_vehicle_model));
        }
    }
    @Override
    public void vehicleTypeName(String value, String id, int type) {
        Log.e("tag_octal","vehicleTypeName: "+value);
        switch (type) {
            case I.VEHICLE_TYPE:
                classTv.setText(value);
                vehicleTypeId = id;

                brandTv.setText("");
                vehicleBrandID = "";
                vehicleBrand_list.clear();

                fromYearTv.setText("");
                toYearTv.setText("");
                fromVehicleYearId = "";
                toVehicleYearId = "";
                vehicleYear_list.clear();
                break;
            case I.VEHICLE_BRAND:

                fromYearTv.setText("");
                toYearTv.setText("");
                fromVehicleYearId = "";
                toVehicleYearId = "";
                vehicleYear_list.clear();

                brandTv.setText(value);
                vehicleBrandID = id;
                getVehicleYear1(mContext);
                break;

            case I.VEHICLE_YEAR:
                fromYearTv.setText(value);
                fromVehicleYearId = id;
                break;

            case I.VEHICLE_YEAR2:
                toYearTv.setText(value);
                toVehicleYearId = id;
                break;

            case I.TRACK_CONDITION:
                trackConditionTv.setText(value);
                trackCondition = id;
                break;

        }
    }

    public void getVehicleYear1(final Context context) {
        mProgressDialog=new MyProgressDialog(mContext);
        mProgressDialog.show();
        FastLapApplication.getInstance().getRequestQueue().create(MyApiEndpointInterface.class).getVehicleYear1()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> responseBodyResponse) {
                        getVehicleYearResponse(Util.convertRetrofitResponce(responseBodyResponse));
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    // Vehicle Year Response
    public void getVehicleYearResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                vehicleYear_list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    BeanVehicleType beanVehicleType = new BeanVehicleType();
                    beanVehicleType.setId(jsonArray.getString(i));
                    beanVehicleType.setName(jsonArray.getString(i));
                    vehicleYear_list.add(beanVehicleType);
                }

            } /*else
                Util.showAlertDialog(context, getString(R.string.alert), msg);*/
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    @Override
    public void vehicleTypeName(String value, String id, int type, int pos) {

    }

    public void getMyLapsWebserviceResponse(String response) {
        try {
            ArrayList<String> countryList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                MyLapsActivity.selectedItem.clear();
                MyLapsActivity.myLapsList.clear();
                MyLapsActivity.countryName.clear();
                JSONArray dataArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < dataArray.length(); i++)
                {
                    JSONObject jsonObject1 = dataArray.getJSONObject(i);
                    PaddockMyLapsParentModel paddockMyLapsParentModel = new PaddockMyLapsParentModel();
                    trackVersionId=jsonObject1.has(S._id)?jsonObject1.getString(S._id):"";
                    paddockMyLapsParentModel.setUserLapId(trackVersionId);
                    JSONObject indexObject = new JSONObject();
                    JSONObject trackObject=new JSONObject();
                    JSONArray tempArray=jsonObject1.has(S.trackVersionData)?jsonObject1.getJSONArray(S.trackVersionData):null;
                    if(tempArray!=null&&tempArray.length()>0)
                    {
                        indexObject = tempArray.getJSONObject(0);
                        trackObject=indexObject.has(S.trackId)?indexObject.getJSONObject(S.trackId):null;
                        if(trackObject!=null)
                        {
                            paddockMyLapsParentModel.setTrack_id(trackObject.has(S._id)?trackObject.getString(S._id):"");
                            paddockMyLapsParentModel.setTrackName(trackObject.has(S.name)?trackObject.getString(S.name):"");
                            paddockMyLapsParentModel.setTrackImage(trackObject.has(S.image)?trackObject.getString(S.image):"");
                            paddockMyLapsParentModel.setTracklocation(trackObject.has(S.location_response)?trackObject.getString(S.location_response):"");
                            paddockMyLapsParentModel.setTrackLength(trackObject.has(S.trackLength)?trackObject.getString(S.trackLength):"");
                            paddockMyLapsParentModel.setTotalTurns(trackObject.getJSONObject(S.turns).has(S.totalTurns)? trackObject.getJSONObject(S.turns).getString(S.totalTurns):"");
                            // paddockMyLapsParentModel.setTotalLapRecord(jsonObject2.getJSONObject(S.trackId).getString(S.totalLapRecords));
                            paddockMyLapsParentModel.setTrackurl(trackObject.getJSONObject(S.contactDetails).getString(S.url));
                            paddockMyLapsParentModel.setTrackMobile(trackObject.getJSONObject(S.contactDetails).getString(S.mobileNumber_response));
                            paddockMyLapsParentModel.setTrackEmail(trackObject.getJSONObject(S.contactDetails).getString(S.email));
                            paddockMyLapsParentModel.setIsFav(indexObject.has(S.isFav)?indexObject.getString(S.isFav):"");
                            paddockMyLapsParentModel.setIsHome(indexObject.has(S.isHome)?indexObject.getString(S.isHome):"");
                            paddockMyLapsParentModel.setTrackCountry(trackObject.getString(S.country));
                            paddockMyLapsParentModel.setTrackVersionName(indexObject.getString(S.name));
                            paddockMyLapsParentModel.setTrackVersionId(indexObject.getString(S._id));
                            JSONArray rxArray = new JSONArray();
                            rxArray = indexObject.has(S.startRight)?indexObject.getJSONArray(S.startRight):null;
                            if (rxArray.length() > 0&&rxArray!=null) {
                                paddockMyLapsParentModel.setTrackRxLng(rxArray.getString(0));
                                paddockMyLapsParentModel.setTrackRxLat(rxArray.getString(1));
                            }
                            JSONArray lxArray = new JSONArray();
                            lxArray=indexObject.has(S.startLeft)?indexObject.getJSONArray(S.startLeft):null;
                            if (lxArray.length() > 0&&lxArray!=null) {
                                paddockMyLapsParentModel.setTrackLxLng(lxArray.getString(0));
                                paddockMyLapsParentModel.setTrackLxLat(lxArray.getString(1));
                            }
                            if (indexObject.has(S.sector)) {
                                if (indexObject.getJSONObject(S.sector).has(S.noSector)) {
                                    if (indexObject.getJSONObject(S.sector).getJSONObject(S.noSector).has(S.image)) {
                                        paddockMyLapsParentModel.setTrackRouteImage(indexObject.getJSONObject(S.sector).getJSONObject(S.noSector).getString(S.image));
                                    }
                                }
                            }
                        }

                    }
                    JSONArray userArray = jsonObject1.getJSONArray(S.lapData);
                    ArrayList<BeanTimeLap> userList = new ArrayList<>();
                    for (int j = 0; j < userArray.length(); j++)
                    {
                        try {
                            JSONObject lapObject = userArray.getJSONObject(j);
                            JSONObject userJsonObject = lapObject.has(S.userId)?lapObject.getJSONObject(S.userId):null;
                            if(userJsonObject!=null&&userJsonObject.length()>0)
                            {
                                BeanTimeLap beanTimeLap = new BeanTimeLap();
                                beanTimeLap.setUserId(userJsonObject.has(S._id)?userJsonObject.getString(S._id):"");
                                if((userJsonObject.getString(S._id).equalsIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id))&&lapObject.getString("status").equalsIgnoreCase("1")))
                                {
                                    if (TextUtils.isEmpty(paddockMyLapsParentModel.getTrackBestPosition()) || paddockMyLapsParentModel.getTrackBestPosition().equalsIgnoreCase("0")) {
                                        if (userJsonObject.getString(S._id).compareToIgnoreCase(MySharedPreferences.getPreferences(mContext, S.user_id)) == 0) {
                                            paddockMyLapsParentModel.setTrackBestPosition((j + 1) + "");
                                        }
                                    }
                                    else {
                                        paddockMyLapsParentModel.setTrackBestPosition((paddockMyLapsParentModel.getTrackBestPosition()));
                                    }
                                    beanTimeLap.setUserImage(userJsonObject.getJSONObject(S.personalInfo).getString(S.image));
                                    beanTimeLap.setUserName(userJsonObject.getJSONObject(S.personalInfo).getString(S.firstName_response) + " " + userJsonObject.getJSONObject(S.personalInfo).getString(S.lastName_response));

                                    beanTimeLap.setTimeLap_Id(lapObject.getString(S._id));
                                    beanTimeLap.setLapId(lapObject.getString(S._id));
                                    beanTimeLap.setTrackVersionId(lapObject.getString(S.trackVersionId));

                                    beanTimeLap.setGroupTypeId(lapObject.getJSONObject(S.lapTimeId).has(S.groupTypeId)?lapObject.getJSONObject(S.lapTimeId).getString(S.groupTypeId):"");

                                    beanTimeLap.setLapDate(lapObject.getJSONObject(S.lapTimeId).getString(S.dateOfRecording));

                                    beanTimeLap.setVehicleModelName(lapObject.getJSONObject(S.lapTimeId).getJSONObject(S.userVehicleId).getJSONObject(S.vehicleModelId).getString(S.vehicleModelName));
                                    beanTimeLap.setVehicleBrandName(lapObject.getJSONObject(S.lapTimeId).getJSONObject(S.userVehicleId).getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName));

                                    beanTimeLap.setTime(lapObject.getString(S.time));
                                    beanTimeLap.setFileUrl(lapObject.getString(S.fileUrl));
                                    beanTimeLap.setMaxSpeed(lapObject.getString(S.maxSpeed));
                                    beanTimeLap.setAvgSpeed(lapObject.getString(S.averageSpeed));
                                    beanTimeLap.setAverageSpeedToShow(lapObject.has(S.averageSpeedToShow)?lapObject.getString(S.averageSpeedToShow):"");
                                    beanTimeLap.setRank((j + 1) + "");
                                    beanTimeLap.setBestLapTime(lapObject.getString(S.time));
                                    Random rnd = new Random();
                                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                                    beanTimeLap.setLapColor(color);
                                    userList.add(beanTimeLap);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                    paddockMyLapsParentModel.setMyLapsChildList(userList);
                    MyLapsActivity.myLapsList.add(paddockMyLapsParentModel);
                    if (trackObject!=null && !countryList.contains(trackObject.getString(S.country))) {
                        countryList.add(indexObject.getJSONObject(S.trackId).getString(S.country));
                        BeanVehicleType beanVehicleType = new BeanVehicleType();
                        beanVehicleType.setId(indexObject.getJSONObject(S.trackId).getString(S.country));
                        beanVehicleType.setName(indexObject.getJSONObject(S.trackId).getString(S.country));
                        MyLapsActivity.countryName.add(beanVehicleType);
                    }
                }
                //getListData();

            }
            getListData();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }































































    /* public void getMyLapsWebserviceResponse(String response) {
        try {
            ArrayList<String> countryList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success)
            {
                MyLapsActivity.myLapsList.clear();
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    PaddockMyLapsParentModel paddockMyLapsParentModel = new PaddockMyLapsParentModel();
                    paddockMyLapsParentModel.setUserLapId(jsonObject1.getString(S._id));

                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2 = jsonObject1.getJSONArray(S.trackVersionData).getJSONObject(0);

                    paddockMyLapsParentModel.setIsFav(jsonObject2.has(S.isFav)?jsonObject2.getString(S.isFav):"");
                    paddockMyLapsParentModel.setIsHome(jsonObject2.has(S.isHome)?jsonObject2.getString(S.isHome):"");
                    paddockMyLapsParentModel.setTrackVersionName(jsonObject2.has(S.name)?jsonObject2.getString(S.name):"");
                    paddockMyLapsParentModel.setTrackVersionId(jsonObject2.has(S._id)?jsonObject2.getString(S._id):"");

                    JSONObject trackObject=jsonObject2.has(S.trackId)?jsonObject2.getJSONObject(S.trackId):null;
                    if(trackObject!=null&&trackObject.length()>0){
                        paddockMyLapsParentModel.setTrackCountry(trackObject.has(S.country)?trackObject.getString(S.country):"");
                        paddockMyLapsParentModel.setTrack_id(trackObject.has(S._id)?trackObject.getString(S._id):"");
                        paddockMyLapsParentModel.setTrackName(trackObject.has(S.name)?trackObject.getString(S.name):"");
                        paddockMyLapsParentModel.setTrackImage(trackObject.has(S.image)?trackObject.getString(S.image):"");
                        paddockMyLapsParentModel.setTracklocation(trackObject.has(S.location_response)?trackObject.getString(S.location_response):"");
                        paddockMyLapsParentModel.setTrackLength(trackObject.has(S.trackLength)?trackObject.getString(S.trackLength):"");
                        paddockMyLapsParentModel.setTotalTurns(trackObject.getJSONObject(S.turns).has(S.totalTurns)?trackObject.getJSONObject(S.turns).getString(S.totalTurns):"");
                        paddockMyLapsParentModel.setTrackurl(trackObject.getJSONObject(S.contactDetails).has(S.url)?trackObject.getJSONObject(S.contactDetails).getString(S.url):"");
                        paddockMyLapsParentModel.setTrackMobile(trackObject.getJSONObject(S.contactDetails).has(S.mobileNumber_response)?trackObject.getJSONObject(S.contactDetails).getString(S.mobileNumber_response):"");
                        paddockMyLapsParentModel.setTrackEmail(trackObject.getJSONObject(S.contactDetails).has(S.email)?trackObject.getJSONObject(S.contactDetails).getString(S.email):"");
                        }
                    JSONArray rxArray = new JSONArray();
                    rxArray = jsonObject2.getJSONArray(S.startRight);
                    if (rxArray.length() > 0) {
                        paddockMyLapsParentModel.setTrackRxLng(rxArray.getString(0));
                        paddockMyLapsParentModel.setTrackRxLat(rxArray.getString(1));
                    }
                    JSONArray lxArray = new JSONArray();
                    lxArray = jsonObject2.getJSONArray(S.startLeft);
                    if (lxArray.length() > 0) {
                        paddockMyLapsParentModel.setTrackLxLng(lxArray.getString(0));
                        paddockMyLapsParentModel.setTrackLxLat(lxArray.getString(1));
                    }

                    if (jsonObject2.has(S.sector)) {
                        if (jsonObject2.getJSONObject(S.sector).has(S.noSector)) {
                            if (jsonObject2.getJSONObject(S.sector).getJSONObject(S.noSector).has(S.image)) {
                                paddockMyLapsParentModel.setTrackRouteImage(jsonObject2.getJSONObject(S.sector).getJSONObject(S.noSector).getString(S.image));
                            }
                        }
                    }

                    JSONArray userjsonArray = jsonObject1.getJSONArray(S.lapData);
                    ArrayList<BeanTimeLap> userList = new ArrayList<>();
                    for (int j = 0; j < userjsonArray.length(); j++) {
                        try {
                            JSONObject lapObject = userjsonArray.getJSONObject(j);
                            JSONObject userJsonObject = lapObject.getJSONObject(S.userId);
                            BeanTimeLap beanTimeLap = new BeanTimeLap();
                            beanTimeLap.setUserId(userJsonObject.has(S._id)?userJsonObject.getString(S._id):"");
                            if (TextUtils.isEmpty(paddockMyLapsParentModel.getTrackBestPosition()) || paddockMyLapsParentModel.getTrackBestPosition().equalsIgnoreCase("0")) {
                                if (userJsonObject.getString(S._id).compareToIgnoreCase(MySharedPreferences.getPreferences(getActivity(), S.user_id)) == 0) {
                                    paddockMyLapsParentModel.setTrackBestPosition((j + 1) + "");
                                }
                            }
                            JSONObject personalObject=userJsonObject.has(S.personalInfo)?userJsonObject.getJSONObject(S.personalInfo):null;
                            if(personalObject!=null&&personalObject.length()>0){
                                beanTimeLap.setUserImage(personalObject.has(S.image)?personalObject.getString(S.image):"");
                                String firstName=personalObject.has(S.firstName_response)?personalObject.getString(S.firstName_response):"";
                                String lastName=personalObject.has(S.lastName_response)?personalObject.getString(S.lastName_response):"";
                                beanTimeLap.setUserName(firstName+" "+lastName);
                            }

                            beanTimeLap.setTimeLap_Id(lapObject.has(S._id)?lapObject.getString(S._id):"");
                            beanTimeLap.setLapId(lapObject.has(S._id)?lapObject.getString(S._id):"");
                            beanTimeLap.setTrackVersionId(lapObject.has(S.trackVersionId)?lapObject.getString(S.trackVersionId):"");
                            try {
                                beanTimeLap.setGroupTypeId(lapObject.getJSONObject(S.lapTimeId).has(S.groupTypeId)?lapObject.getJSONObject(S.lapTimeId).getString(S.groupTypeId):"");
                                beanTimeLap.setLapDate(lapObject.getJSONObject(S.lapTimeId).getString(S.dateOfRecording));
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                            JSONObject userVehicleObject=lapObject.getJSONObject(S.lapTimeId).has(S.userVehicleId)?lapObject.getJSONObject(S.lapTimeId).getJSONObject(S.userVehicleId):null;
                            if(userVehicleObject!=null&&userVehicleObject.length()>0){
                                beanTimeLap.setVehicleModelName(userVehicleObject.getJSONObject(S.vehicleModelId).has(S.vehicleModelName)?userVehicleObject.getJSONObject(S.vehicleModelId).getString(S.vehicleModelName):"");
                                beanTimeLap.setVehicleBrandName(userVehicleObject.getJSONObject(S.vehicleBrandId).has(S.vehicleBrandName)?userVehicleObject.getJSONObject(S.vehicleBrandId).getString(S.vehicleBrandName):"");
                            }
                            beanTimeLap.setTime(lapObject.has(S.time)?lapObject.getString(S.time):"");
                            beanTimeLap.setFileUrl(lapObject.has(S.fileUrl)?lapObject.getString(S.fileUrl):"");
                            beanTimeLap.setMaxSpeed(lapObject.has(S.maxSpeed)?lapObject.getString(S.maxSpeed):"");
                            beanTimeLap.setAvgSpeed(lapObject.has(S.averageSpeed)?lapObject.getString(S.averageSpeed):"");
                            beanTimeLap.setRank((j + 1) + "");
                            beanTimeLap.setBestLapTime(lapObject.has(S.time)?lapObject.getString(S.time):"");

                            Random rnd = new Random();
                            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                            beanTimeLap.setLapColor(color);

                            userList.add(beanTimeLap);

                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                    paddockMyLapsParentModel.setMyLapsChildList(userList);
                    MyLapsActivity.myLapsList.add(paddockMyLapsParentModel);
                    if (!countryList.contains(jsonObject2.getJSONObject(S.trackId).getString(S.country))) {
                        MyLapsActivity.countryName.clear();
                        countryList.add(jsonObject2.getJSONObject(S.trackId).getString(S.country));
                        BeanVehicleType beanVehicleType = new BeanVehicleType();
                        beanVehicleType.setId(jsonObject2.getJSONObject(S.trackId).getString(S.country));
                        beanVehicleType.setName(jsonObject2.getJSONObject(S.trackId).getString(S.country));
                        MyLapsActivity.countryName.add(beanVehicleType);
                    }
                }
                getListData();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }*/
}
