package com.os.fastlap.fragment.gallery;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.activity.profile.GalleryProfileActivity;
import com.os.fastlap.adapter.profile.PhotoRecyclerViewAdapter;
import com.os.fastlap.beans.BeanGallery;

import java.util.ArrayList;

/**
 * Created by anandj on 7/13/2017
 */

@SuppressLint("ValidFragment")
public class PhotoFragment extends Fragment {

    RecyclerView photo_recycler_photo;
    PhotoRecyclerViewAdapter mPhotoRecycler;
    ArrayList<BeanGallery> galleryList;
    int type;
    TextView no_data_tv;

    public PhotoFragment(int type) {
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_photo_video_fragment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        galleryList = new ArrayList<>();
        galleryList.clear();
        mPhotoRecycler = new PhotoRecyclerViewAdapter(getContext(), galleryList);
        photo_recycler_photo = (RecyclerView) view.findViewById(R.id.photo_recycler_photo);
        no_data_tv = (TextView) view.findViewById(R.id.no_data_tv);
        photo_recycler_photo.setLayoutManager(new GridLayoutManager(getContext(), 2));
        photo_recycler_photo.setAdapter(mPhotoRecycler);
        photo_recycler_photo.setNestedScrollingEnabled(true);

        setGalleryData();
    }

    public void setGalleryData() {
        galleryList.clear();
        if (GalleryProfileActivity.galleryList.size() == 0) {
            photo_recycler_photo.setVisibility(View.GONE);
            no_data_tv.setVisibility(View.VISIBLE);
        } else {
            photo_recycler_photo.setVisibility(View.VISIBLE);
            no_data_tv.setVisibility(View.GONE);
        }
        for (int i = 0; i < GalleryProfileActivity.galleryList.size(); i++) {
            if (GalleryProfileActivity.galleryList.get(i).getType().compareTo(type + "") == 0) {
                galleryList.add(GalleryProfileActivity.galleryList.get(i));
            }
        }
        mPhotoRecycler.notifyDataSetChanged();
    }

}
