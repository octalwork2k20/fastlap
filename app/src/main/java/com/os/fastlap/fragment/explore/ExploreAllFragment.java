package com.os.fastlap.fragment.explore;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.os.fastlap.R;
import com.os.fastlap.activity.ExploreActivity;
import com.os.fastlap.activity.community.EventActivity;
import com.os.fastlap.adapter.ViewPagerAdapter;
import com.os.fastlap.beans.BeanExplore;
import com.os.fastlap.beans.BeanWeather;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.delegates.FragmentCommunicator;
import com.os.fastlap.util.MyProgressDialog;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.I;
import com.os.fastlap.util.constants.S;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



/*
 * Created by riyazudinp on 6/17/2017.
 */

@SuppressLint("ValidFragment")
public class ExploreAllFragment extends Fragment implements FragmentCommunicator {

    public static int pos = 0;
    public FragmentCommunicator fragmentCommunicator;
    GoogleMap googleMap;
    private Context mContext;
    private View mView;
    private TextView curcuitNameTv;
    private TextView locationTv;
    private TextView lengthTv;
    private TextView turnsTv;
    private TextView locationAddressTv;
    private TextView distanceLocationTv;
    private TextView phoneNumberTv;
    private TextView webUrlTv;
    private ImageView circuitImageIv;
    private android.support.design.widget.TabLayout tabLayout;
    private ViewPager viewpager;
    private LinearLayout mTempratureLyt;
    private FragmentManager mFragmentManager;
    private ViewPagerAdapter adapter;
    int type;
    ArrayList<BeanExplore> trackList;
    LinearLayout info_ll;
    AuthAPI authAPI;
    ArrayList<BeanWeather> weatherList;
    String location;
    public  String url;
    private MyProgressDialog mProgressDialog;
    private String TAG = ExploreAllFragment.class.getSimpleName();

    @SuppressLint("ValidFragment")
    public ExploreAllFragment(int type) {
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.explore_fragment, container, false);

        }
        mProgressDialog = new MyProgressDialog(getActivity());

        mContext = getActivity();
        authAPI = new AuthAPI(mContext);
        initView(mView);
        initilizeMap();
        addEventListeners();
        return mView;
    }

    private void initView(View mView) {
        mFragmentManager = getChildFragmentManager();
        curcuitNameTv = (TextView) mView.findViewById(R.id.curcuit_name_tv);
        locationTv = (TextView) mView.findViewById(R.id.location_tv);
        lengthTv = (TextView) mView.findViewById(R.id.length_tv);
        turnsTv = (TextView) mView.findViewById(R.id.turns_tv);
        locationAddressTv = (TextView) mView.findViewById(R.id.location_address_tv);
        distanceLocationTv = (TextView) mView.findViewById(R.id.distance_location_tv);
        phoneNumberTv = (TextView) mView.findViewById(R.id.phone_number_tv);
        webUrlTv = (TextView) mView.findViewById(R.id.web_url_tv);
        circuitImageIv = (ImageView) mView.findViewById(R.id.circuit_image_iv);
        tabLayout = (TabLayout) mView.findViewById(R.id.TabLayout);
        viewpager = (ViewPager) mView.findViewById(R.id.viewpager);
        info_ll = (LinearLayout) mView.findViewById(R.id.info_ll);
        mTempratureLyt=(LinearLayout)mView.findViewById(R.id.tempratureLyt);
        trackList = new ArrayList<>();
        trackList.clear();
        weatherList = new ArrayList<>();
        weatherList.clear();


    }
    private void addEventListeners() {
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                // hideKeyboard();
                pos = position;
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    @Override
    public void passDataToFragment(String someValue) {

    }

    private void initilizeMap() {
        try {
            ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    googleMap.getUiSettings().setMapToolbarEnabled(true);
                    googleMap.getUiSettings().setZoomGesturesEnabled(true);
                    googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
                    {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            final int pos = (int) marker.getTag();

                            final Dialog dialog = new Dialog(mContext);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.explore_map_info_view);

                            TextView track_name_tv = (TextView) dialog.findViewById(R.id.track_name_tv);
                            TextView track_detail_tv = (TextView) dialog.findViewById(R.id.track_detail_tv);
                            TextView event_detail_tv = (TextView) dialog.findViewById(R.id.event_detail_tv);
                            track_name_tv.setText(trackList.get(pos).getTrackName());

                            track_detail_tv.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showTrackDetail(pos);
                                    dialog.dismiss();

                                }
                            });

                            event_detail_tv.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showEventList(pos);
                                    dialog.dismiss();

                                }
                            });

                            dialog.setCancelable(false);
                            dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            dialog.getWindow().setGravity(Gravity.CENTER);
                            dialog.show();

                            return false;
                        }
                    });

                    //
                     getExploreData();
                }
            });

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void showEventList(int pos) {

        Intent intent = new Intent(mContext, EventActivity.class);
        intent.putExtra(S.type, I.TRACKEVENT);
        intent.putExtra(S._id, trackList.get(pos).getTrackId());
        startActivity(intent);
    }

    private void showTrackDetail(int pos) {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("https://query.yahooapis.com/v1/public/yql?q=");
        mTempratureLyt.setVisibility(View.VISIBLE);
        BeanExplore beanExplore = new BeanExplore();
        beanExplore = trackList.get(pos);
        curcuitNameTv.setText(beanExplore.getTrackName());
        locationTv.setText(beanExplore.getTrackCountry());
        lengthTv.setText(beanExplore.getTrackLength() /*+ getString(R.string.km)*/);
        turnsTv.setText(!beanExplore.getTotalTurns().equalsIgnoreCase("")?beanExplore.getTotalTurns():"0");
        locationAddressTv.setText(beanExplore.getTrackLocation());
        distanceLocationTv.setText("");
        phoneNumberTv.setText(beanExplore.getTrackMobile());
        webUrlTv.setText(beanExplore.getTrackURL());
        ImageLoader.getInstance().displayImage(S.IMAGE_BASE_URL + beanExplore.getTrackImage(), circuitImageIv, Util.getImageLoaderOption(mContext));
        info_ll.setVisibility(View.VISIBLE);

        location=beanExplore.getTrackLocation();
        String url2="("+location+")";
        String url3="\""+url2+"\"";
        url=("select units.temperature, item.forecast from weather.forecast where woeid in (select woeid from geo.places where text="+url3+") and u =\"C\" limit 7| sort(field=\"item.forecast.date\", descending=\"false\")&format=json");
        Log.e("tag","whether url"+url);
        stringBuilder.append(url);
        getWeatherDetail(stringBuilder.toString());

        Location currentLocation = FastLapApplication.location;
        if(currentLocation!=null){
            Location mLastLocation = new Location("");
            mLastLocation.setLatitude(Double.parseDouble(beanExplore.getTrackLatPos()));
            mLastLocation.setLongitude(Double.parseDouble(beanExplore.getTrackLongPos()));
            float distance = currentLocation.distanceTo(mLastLocation);

            double dis = distance / 1000.0;
            DecimalFormat df = new DecimalFormat("#.##");
            distanceLocationTv.setText(df.format(dis) + getString(R.string.km_away_from_your_location));
        }

    }



    public void getWeatherDetail(final String url){
        mProgressDialog.show();
        final OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mProgressDialog.dismiss();
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                mProgressDialog.dismiss();
                String serverResponse = response.body().string();
                if (response.isSuccessful()){
                    try {
                        JSONObject jsonObject = new JSONObject(serverResponse);
                        Log.e("tag", jsonObject.toString());
                        try {
                            JSONObject queryObject=jsonObject.has("query")?jsonObject.getJSONObject("query"):null;
                            if(queryObject!=null){
                                JSONObject resultsObject=queryObject.has("results")?queryObject.getJSONObject("results"):null;
                                JSONArray channelArray=resultsObject.has("channel")?resultsObject.getJSONArray("channel"):null;
                                if(channelArray!=null){
                                    weatherList.clear();
                                    for(int i=0;i<channelArray.length();i++){
                                        JSONObject forecastObject=channelArray.getJSONObject(i).getJSONObject("item").getJSONObject("forecast");
                                        BeanWeather beanWeather=new BeanWeather();
                                        beanWeather.setDate(forecastObject.getString("date"));
                                        beanWeather.setDay(forecastObject.getString("day"));
                                        beanWeather.setHigh(forecastObject.getString("high"));
                                        beanWeather.setLow(forecastObject.getString("low"));
                                        weatherList.add(beanWeather);
                                    }
                                }
                            }
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setUpPager(weatherList);
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e){
                        e.printStackTrace();
                    }

                }

            }
        });

    }
    public void getExploreData() {
        trackList.clear();
        trackList.addAll(ExploreActivity.exploreTrackList);
        SetAllTrackonMap();
    }

    private void SetAllTrackonMap() {
        List<Marker> markers = new ArrayList<Marker>();
        if (googleMap != null) {
           googleMap.clear();
            if (trackList.size() > 0)
            {
                for (int i = 0; i < trackList.size(); i++)
                {
                    try {
                        if (trackList.get(i).getTrackLongPos() != "")
                        {
                            LatLng latlang = new LatLng(Double.parseDouble(trackList.get(i).getTrackLatPos()), Double.parseDouble(trackList.get(i).getTrackLongPos()));
                            Marker marker = googleMap.addMarker(new MarkerOptions().position(latlang).title(trackList.get(i).getTrackName())
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.locationred))); //...
                            markers.add(marker);
                            marker.setTag(i);
                           // CameraPosition cameraPosition = new CameraPosition.Builder().target(latlang).zoom(2).build();
                           // googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (Marker marker : markers) {
                    builder.include(marker.getPosition());
                }
                LatLngBounds bounds = builder.build();
                int padding = 0; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                googleMap.animateCamera(cu);
                googleMap.moveCamera(cu);
                markers.size();
            }
        }
    }

    public void getWeatherData(String response) {
        Log.e("tag","whether log"+response);
        /*weatherList.clear();
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {

                JSONObject jsonObject1 = jsonObject.getJSONObject(S.data);


                JSONObject jsonObject2 = jsonObject1.getJSONObject(S.query);
                if (jsonObject2.getJSONObject(S.results) != null) {
                    JSONArray forecastArray = new JSONArray();
                    forecastArray = jsonObject2.getJSONObject(S.results).getJSONObject(S.channel).getJSONObject(S.item).getJSONArray(S.forecast);

                    for (int i = 0; i < forecastArray.length() && i < 7; i++) {
                        JSONObject jsonObject11 = new JSONObject();
                        jsonObject11 = forecastArray.getJSONObject(i);
                        BeanWeather beanWeather = new BeanWeather();
                        beanWeather.setDate(jsonObject11.getString(S.date));
                        beanWeather.setDay(jsonObject11.getString(S.day));
                        beanWeather.setText(jsonObject11.getString(S.text));
                        weatherList.add(beanWeather);
                    }
                }

                setUpPager(weatherList);

            } *//*else
                Util.showAlertDialog(mContext, getString(R.string.alert), msg);*//*
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }*/

    }

    private void setUpPager(ArrayList<BeanWeather> weatherList) {
        adapter = new ViewPagerAdapter(mFragmentManager);

        for (int i = 0; i < weatherList.size(); i++) {
            adapter.addFragment(new ExploreWeatherFragment(weatherList.get(i).getHigh(),weatherList.get(i).getLow(),""),weatherList.get(i).getDay());
           // adapter.addFragment(new ExploreWeatherFragment(weatherList.get(0).getDay(),weatherList.get(0).getHigh(),weatherList.get(0).getLow()));
        }
        adapter.getItem(viewpager.getCurrentItem());
        viewpager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewpager);
        Util.changeTabsFont(tabLayout, getActivity());
    }

}
