package com.os.fastlap.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.RadioButtonPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;

/**
 * Created by abhinava on 7/27/2017.
 */

public class WeekStartDayFragment extends Fragment implements View.OnClickListener {
    static Context context;
    static Object object;
    View rootview;
    private RadioGroup weekStartRadiogroup;
    private RadioButtonPlayRegular rbSunday;
    private RadioButtonPlayRegular rbMonday;
    private TextViewPlayBold weekStartSaveTv;
    private TextViewPlayBold weekStartDeleteTv;
    AuthAPI authAPI;

    public static WeekStartDayFragment newInstance(Context contex, Object obj) {
        WeekStartDayFragment f = new WeekStartDayFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_week_start_day, container, false);
            context = getActivity();
        }
        initView(rootview);
        authAPI=new AuthAPI(context);

        return rootview;
    }

    private void initView(View rootview) {
        weekStartRadiogroup = (RadioGroup) rootview.findViewById(R.id.week_start_radiogroup);
        rbSunday = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_sunday);
        rbMonday = (RadioButtonPlayRegular) rootview.findViewById(R.id.rb_monday);
        weekStartSaveTv = (TextViewPlayBold) rootview.findViewById(R.id.week_start_save_tv);
        weekStartDeleteTv = (TextViewPlayBold) rootview.findViewById(R.id.week_start_delete_tv);

        clickListner();

        String weekStartDay = MySharedPreferences.getPreferences(context, S.weekStartDay);
        if (weekStartDay.equalsIgnoreCase("Sunday")) {
            rbSunday.setChecked(true);
            rbMonday.setChecked(false);
        } else {
            rbSunday.setChecked(false);
            rbMonday.setChecked(true);
        }
    }

    private void clickListner()
    {
        weekStartSaveTv.setOnClickListener(this);
        weekStartDeleteTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.week_start_save_tv:
                String weekStartDay = "";
                if(rbSunday.isChecked())
                {
                    weekStartDay="Sunday";
                }
                else if(rbMonday.isChecked())
                {
                    weekStartDay="Monday";
                }

                MySharedPreferences.setPreferences(context,weekStartDay,S.weekStartDay);

                authAPI.setWeekStartDay(context, MySharedPreferences.getPreferences(context, S.user_id),weekStartDay);
                break;

            case R.id.week_start_delete_tv:

                break;
        }

    }

}
