package com.os.fastlap.fragment.paddock;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.os.fastlap.R;
import com.os.fastlap.adapter.paddock.PaddockProfileAdapter;

import java.util.ArrayList;

/**
 * Created by anandj on 7/18/2017.
 */

public class PaddockProfileFragment extends Fragment {

    private RecyclerView recyclerview_diary;
    private PaddockProfileAdapter paddockProfileAdapter;
    private ArrayList<String> arrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mylaps_profile_fragment, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        arrayList = new ArrayList<>();
        arrayList.add("hr");
        arrayList.add("hr");
        arrayList.add("hr");
        arrayList.add("hr");

        recyclerview_diary = (RecyclerView) view.findViewById(R.id.recyclerview_diary);
        paddockProfileAdapter = new PaddockProfileAdapter(getContext(),arrayList);
        recyclerview_diary.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerview_diary.setAdapter(paddockProfileAdapter);

        recyclerview_diary.setNestedScrollingEnabled(false);


    }
}
