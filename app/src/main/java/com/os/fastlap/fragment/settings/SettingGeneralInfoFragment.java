package com.os.fastlap.fragment.settings;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.os.fastlap.R;
import com.os.fastlap.data.AuthAPI;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.Util;
import com.os.fastlap.util.Validation;
import com.os.fastlap.util.constants.S;
import com.os.fastlap.util.customclass.EditTextPlayRegular;
import com.os.fastlap.util.customclass.TextViewPlayBold;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static com.os.fastlap.util.constants.S.email;

/**
 * Created by abhinava on 7/27/2017.
 */

public class SettingGeneralInfoFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    static Context context;
    static Object object;
    View rootview;
    private EditTextPlayRegular fnameEt;
    private EditTextPlayRegular lnameEt;
    private EditTextPlayRegular nickNameEt;
    private EditTextPlayRegular emailAddressEt;
    private TextViewPlayRegular dobEt;
    private TextViewPlayBold generalSaveTv;
    private TextViewPlayBold generalDeleteTv;
    AuthAPI authAPI;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    public static String TAG = "SettingGeneralInfoFragment.java";
    private TextView selectNationalityEt;
    private TextView locationEt;
    ArrayList<String> stringArrayList=new ArrayList<>();
    SpinnerDialog locationSpinnerDialog;
    SpinnerDialog nationalitySpinnerDialog;
    String nationality="";
    String location="";


    public static SettingGeneralInfoFragment newInstance(Context contex, Object obj) {
        SettingGeneralInfoFragment f = new SettingGeneralInfoFragment();
        context = contex;
        object = obj;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.setting_generalinfo, container, false);
            context = getActivity();
        }
        initView(rootview);
        authAPI = new AuthAPI(getContext());
        authAPI.getNationality(getContext());

        return rootview;
    }

    private void initView(View rootview) {
        selectNationalityEt=(TextView)rootview.findViewById(R.id.tvNationality) ;
        locationEt=(TextView)rootview.findViewById(R.id.tvLocation) ;
        fnameEt = (EditTextPlayRegular) rootview.findViewById(R.id.fname_et);
        lnameEt = (EditTextPlayRegular) rootview.findViewById(R.id.lname_et);
        nickNameEt = (EditTextPlayRegular) rootview.findViewById(R.id.nick_name_et);
        emailAddressEt = (EditTextPlayRegular) rootview.findViewById(R.id.email_address_et);
        dobEt = (TextViewPlayRegular) rootview.findViewById(R.id.dob_et);
        //cityEt = (TextViewPlayRegular) rootview.findViewById(R.id.city_et);
        generalSaveTv = (TextViewPlayBold) rootview.findViewById(R.id.general_save_tv);
        generalDeleteTv = (TextViewPlayBold) rootview.findViewById(R.id.general_delete_tv);
        onDateSetListener = this;
        clickListner();
    }

    private void clickListner() {
        generalSaveTv.setOnClickListener(this);
        generalDeleteTv.setOnClickListener(this);
        dobEt.setOnClickListener(this);
        selectNationalityEt.setOnClickListener(this);
        locationEt.setOnClickListener(this);
        //cityEt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.general_save_tv:
                String fname = fnameEt.getText().toString().trim();
                String lname = lnameEt.getText().toString().trim();
                String username = nickNameEt.getText().toString().trim();
                String dob = dobEt.getText().toString().trim();
                String email = emailAddressEt.getText().toString().trim();
                Util.hideKeyboard(getActivity().getCurrentFocus(), getActivity());
                if (Validation.updateUserDetailsValidation(fname, lname, username, dob, nationality, location, generalSaveTv, context))
                    authAPI.updateInfo(context, fname, lname, username, dob, nationality, location, MySharedPreferences.getPreferences(context, S.user_id),email);
                break;

            case R.id.general_delete_tv:
                break;
            case R.id.dob_et:
                DatepickerFragment datePickerFragment = DatepickerFragment.newInstance(onDateSetListener);
                datePickerFragment.show(getActivity().getFragmentManager(), "date");
                break;
            case R.id.tvNationality:
                nationalitySpinnerDialog.showSpinerDialog();
                break;
            case R.id.tvLocation:
                locationSpinnerDialog.showSpinerDialog();
                break;
        }
    }

    public void receiveNationalityResponse(JSONArray jsonArray) {
        try {
            /*nationality_array = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                nationality_array[i] = jsonObject.getString(S.name);
            }
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_iten_gray, nationality_array);
            select_nationality_spinner.setAdapter(spinnerArrayAdapter);
            select_location_spinner.setAdapter(spinnerArrayAdapter);*/

            stringArrayList=new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                stringArrayList.add(jsonObject1.getString(S.name));
            }
            nationalitySpinnerDialog=new SpinnerDialog(getActivity(),stringArrayList,getResources().getString(R.string.search_nationality_and_select),getResources().getString(R.string.cancel));
            locationSpinnerDialog=new SpinnerDialog(getActivity(),stringArrayList,getResources().getString(R.string.search_location_and_select),getResources().getString(R.string.cancel));

            nationalitySpinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                @Override
                public void onClick(String item, int position) {
                    nationality=item;
                    selectNationalityEt.setText(item);
                }
            });

            locationSpinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                @Override
                public void onClick(String item, int position) {
                    location=item;
                    locationEt.setText(item);
                }
            });
            authAPI.getUserAbout(context, MySharedPreferences.getPreferences(context, S.user_id), MySharedPreferences.getPreferences(context, S.user_id));

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public static class DatepickerFragment extends DialogFragment {

        DatePickerDialog.OnDateSetListener onDateSetListener;

        public static DatepickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener) {
            DatepickerFragment datepickerFragment = new DatepickerFragment();
            datepickerFragment.onDateSetListener = onDateSetListener;
            return datepickerFragment;
        }

        public Dialog onCreateDialog(Bundle savedInstance) {

            // create Calendar Instance from Calendar class
            final Calendar calender = Calendar.getInstance();
            int year = calender.get(Calendar.YEAR);
            int month = calender.get(Calendar.MONTH);
            int day = calender.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dateFragment = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_DARK, onDateSetListener, year, month, day);
            dateFragment.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            return dateFragment;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
        setDate(cal);
    }

    public void setDate(Calendar calender) {
        Date current = calender.getTime();
        int diff1 = new Date().compareTo(current);
        if (diff1 < 0) {
            Util.showSnackBar(dobEt, getString(R.string.dob_valid_error));
        } else {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dobEt.setText(dateFormat.format(calender.getTime()));
        }
    }

    /* user info webservice response */
    public void getUserAboutResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = jsonObject.getString(S.message);
            if (jsonObject.getInt(S.status) == S.adi_status_success) {
                JSONArray jsonArray = jsonObject.getJSONArray(S.data);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                JSONObject userJson = jsonObject1.getJSONObject(S.userdata);
                JSONObject personInfo = userJson.getJSONObject(S.personalInfo);
                JSONObject addressJson = userJson.getJSONObject(S.address);

                fnameEt.setText(personInfo.getString(S.firstName_response));
                lnameEt.setText(personInfo.getString(S.lastName_response));
                nickNameEt.setText(userJson.getString(S.nickname));
                emailAddressEt.setText(userJson.getString(email));
                dobEt.setText(personInfo.getString(S.dateOfBirth_response));

                nationality=addressJson.has(S.location_response)?addressJson.getString(S.location_response):"";
                location=addressJson.has(S.nationality_response)?addressJson.getString(S.nationality_response):"";
                selectNationalityEt.setText(nationality);
                locationEt.setText(location);

                MySharedPreferences.setPreferences(context,personInfo.getString(S.firstName_response),S.firstName_response);
                MySharedPreferences.setPreferences(context,personInfo.getString(S.lastName_response),S.lastName_response);



               /* if (addressJson.getString(S.location_response) != null && !addressJson.getString(S.location_response).contentEquals("null")){
                    int locationIndex=Arrays.asList(nationality_array).indexOf(addressJson.getString(S.location_response).replaceAll("%20", " "));
                    select_location_spinner.setSelection(locationIndex);
                }
                try {
                    int index = Arrays.asList(nationality_array).indexOf((addressJson.getString(S.nationality_response).replaceAll("%20", " ")));
                    select_nationality_spinner.setSelection(index);

                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
            else
                Util.showAlertDialog(context, getString(R.string.alert), msg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


}
