package com.os.fastlap.listener;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.io.Serializable;


/**
 * Created by hemendrag on 1/28/2016.
 */
public class CenterLockRecyclerListener extends RecyclerView.OnScrollListener implements Serializable {

    //To avoid recursive calls
    private boolean                      mAutoSet = true;

    //The pivot to be snapped to
    private int                             mCenterPivot;
    Activity                                activity;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public CenterLockRecyclerListener(Activity activity, int center, final RecyclerView recyclerView) {

        this.mCenterPivot = center;
        this.activity = activity;

        /**
         * Automatically selected first
         */
        mAutoSet = true;

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onScrollStateChanged(final RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        try {
            if (recyclerView != null) {
                final LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (mCenterPivot == 0) {
                    if (lm != null)
                        mCenterPivot = lm.getOrientation() == LinearLayoutManager.HORIZONTAL ? (recyclerView.getLeft() + recyclerView.getRight()) : (recyclerView.getTop() + recyclerView.getBottom());
                }
                if (!mAutoSet) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        final View view = findCenterView(lm);

                        mAutoSet = true;

                    }
                }
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING || newState == RecyclerView.SCROLL_STATE_SETTLING) {

                    mAutoSet = false;
                }
            }
        }catch (Exception ex){ex.printStackTrace();}
    }

    @Override
    public void onScrolled(RecyclerView recyclerView,int dx, int dy){
                 super.onScrolled(recyclerView, dx, dy);

    }

    private View findCenterView(LinearLayoutManager lm) {

        int minDistance = 0,center=0;
        View view = null;
        View returnView = null;
        boolean notFound = true;

        for (int i = lm.findFirstVisibleItemPosition(); i <= lm.findLastVisibleItemPosition() && notFound; i++) {

            view = lm.findViewByPosition(i);

            try {
                if(lm!=null  && view!=null)
                center = lm.getOrientation() == LinearLayoutManager.HORIZONTAL ? (view.getLeft() + view.getRight()) / 2 : (view.getTop() + view.getBottom()) / 2;
            }catch (Exception ex){ex.printStackTrace();}
            int leastDifference = Math.abs(mCenterPivot - center);

            if (leastDifference <= minDistance || i == lm.findFirstVisibleItemPosition()) {
                minDistance = leastDifference;
                returnView = view;

            } else {
                notFound = false;

            }
        }

        return returnView;
    }
}