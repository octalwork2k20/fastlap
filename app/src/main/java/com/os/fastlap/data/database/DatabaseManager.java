package com.os.fastlap.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import com.os.fastlap.beans.BeanMessage;

import static me.crosswall.lib.coverflow.core.LinkageCoverTransformer.TAG;

/**
 * Singleton that controls access to the SQLiteDatabase instance
 * for this application.
 */
public class DatabaseManager {
    private static DatabaseManager sInstance;

    public static synchronized DatabaseManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseManager(context.getApplicationContext());
        }

        return sInstance;
    }

    private ChatDbHelper mChatDbHelper;

    private DatabaseManager(Context context) {
        mChatDbHelper = new ChatDbHelper(context);
    }

    /**
     * Return a {@link Cursor} that contains every insect in the database.
     *
     * @param sortOrder Optional sort order string for the query, can be null
     * @return {@link Cursor} containing all insect results.
     */
    public Cursor queryAllInsects(String sortOrder, String tableName) {
        //TODO: Implement the query
        String selectQuery = "";
        if (sortOrder == null)
            selectQuery = "SELECT  * FROM " + tableName;
        else
            selectQuery = "SELECT  * FROM " + tableName + " order by " + sortOrder;

        SQLiteDatabase db = mChatDbHelper.getReadableDatabase();

        return db.rawQuery(selectQuery, null);
    }

    public void insertQuery(ContentValues values, String tableName) {
        SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        // insert row
        db.insert(tableName, null, values);
    }

    /* Before insert message check in message table message already exit or not
     * if message already exit return false and not insert in table
     * else return true and message insert in table
    */
    public boolean checkMessageAlreayExitOrNot(String msgId) {
        SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        String query = "SELECT * FROM " + ChatDbHelper.TABLE_MESSAGE + " WHERE " + ChatDbHelper._ID + " = '" + msgId + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    /* Delete entire message history of specific user */
    public void deleteMessageHistorySpecificUser(String channelId, String userId) {
        SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        db.delete(ChatDbHelper.TABLE_MESSAGE, ChatDbHelper.CHANNEL_ID + "=?", new String[]{channelId});
        db.delete(ChatDbHelper.TABLE_LAST_MESSAGE, ChatDbHelper.LAST_CHANNEL_ID + "=?", new String[]{channelId});
        db.delete(ChatDbHelper.TABLE_USER, ChatDbHelper.USER_ID + "=?", new String[]{userId});
        db.close();
    }

    public int deleteQuery(String tableName, @Nullable String s, @Nullable String[] strings) {

        SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        int count = db.delete(tableName, s, strings);
        if (count > 0) {
            // update data
        }
        return count;
    }

    public int updateQuery(String tableName, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings)
    {
        int returnValue = 0;
        SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        returnValue = db.update(tableName, contentValues, s, strings);

        return returnValue;
    }

    public void updateUnreadCount(String channelId) {
        String updateQuery = "UPDATE " + mChatDbHelper.TABLE_LAST_MESSAGE + " SET " +
                mChatDbHelper.UNREAD_COUNT + "='" + 0 + "' WHERE "
                + mChatDbHelper.LAST_CHANNEL_ID + "='" + channelId + "'";

        SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        db.execSQL(updateQuery);
    }


    /**
     * Return a {@link Cursor} that contains a single insect for the given unique id.
     *
     * @return {@link Cursor} containing the insect result.
     */
    public Cursor queryInsectsById(String tableName, String columnName, String value) {
        String selectQuery = "";
        selectQuery = "SELECT  * FROM " + tableName + " where " + columnName + " ='" + value + "'";


        SQLiteDatabase db = mChatDbHelper.getReadableDatabase();

        return db.rawQuery(selectQuery, null);
    }

    public Cursor queryDistinctById(String tableName, String columnName) {
        String selectQuery = "SELECT * FROM " + tableName + " GROUP BY " + columnName + " ORDER BY dateTime desc";

        //String selectQuery1 = "SELECT m1.* FROM " + tableName + " m1 LEFT JOIN " + tableName + " m2 ON (m1." + ChatDbHelper.CHANNEL_ID + " = m2." + ChatDbHelper.CHANNEL_ID + " AND m1." + ChatDbHelper.MESSAGE_ID + " < m2." + ChatDbHelper.MESSAGE_ID + ") WHERE m2." + ChatDbHelper.MESSAGE_ID + " IS NULL";

        SQLiteDatabase db = mChatDbHelper.getReadableDatabase();

        return db.rawQuery(selectQuery, null);

    }

    public void chatSaveInDB(BeanMessage beanMessage) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatDbHelper._ID, beanMessage.getMessageId());
            contentValues.put(ChatDbHelper.MESSAGE, beanMessage.getMessageText());
            contentValues.put(ChatDbHelper.DATETIME, beanMessage.getMessageDate());
            contentValues.put(ChatDbHelper.IS_READ, beanMessage.getIsReadStatus());
            contentValues.put(ChatDbHelper.RECIVER_ID, beanMessage.getMessageReciverId());
            contentValues.put(ChatDbHelper.SENDER_ID, beanMessage.getMessageSenderId());
            contentValues.put(ChatDbHelper.CHANNEL_ID, beanMessage.getChannelId());
            contentValues.put(ChatDbHelper.MESSAGE_TYPE, beanMessage.getChatType());

            insertQuery(contentValues, ChatDbHelper.TABLE_MESSAGE);


            Cursor userCursor = queryInsectsById(ChatDbHelper.TABLE_USER, ChatDbHelper.USER_ID, beanMessage.getMessageSenderId());
            userCursor.moveToFirst();
            if (userCursor.getCount() == 0) {
                contentValues = new ContentValues();
                contentValues.put(ChatDbHelper.USER_ID, beanMessage.getMessageSenderId());
                contentValues.put(ChatDbHelper.USER_NAME, beanMessage.getMessageSenderName());
                contentValues.put(ChatDbHelper.USER_IMAGE, beanMessage.getMessageSenderImage());

                insertQuery(contentValues, ChatDbHelper.TABLE_USER);
            }

            userCursor = queryInsectsById(ChatDbHelper.TABLE_USER, ChatDbHelper.USER_ID, beanMessage.getMessageReciverId());
            userCursor.moveToFirst();
            if (userCursor.getCount() == 0) {
                contentValues = new ContentValues();
                contentValues.put(ChatDbHelper.USER_ID, beanMessage.getMessageReciverId());
                contentValues.put(ChatDbHelper.USER_NAME, beanMessage.getMessageReciverName());
                contentValues.put(ChatDbHelper.USER_IMAGE, beanMessage.getMessageReciverImage());

                insertQuery(contentValues, ChatDbHelper.TABLE_USER);
            }
            userCursor.close();


        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void chatSaveLastMessageInDB(String channelId, String content, String chatUserId, String chatUserName, String chatUserImage, String message_date, int unreadCount, String messageType,String last_send_user_type) {
        Cursor last_cursor = queryInsectsById(ChatDbHelper.TABLE_LAST_MESSAGE, ChatDbHelper.LAST_CHANNEL_ID, channelId);
        if (last_cursor.getCount() > 0) {
            last_cursor.moveToFirst();
            int old_unread_count = last_cursor.getInt(last_cursor.getColumnIndex(ChatDbHelper.UNREAD_COUNT));

            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatDbHelper.MESSAGE_TYPE, messageType);
            contentValues.put(ChatDbHelper.LAST_CHANNEL_ID, channelId);
            contentValues.put(ChatDbHelper.LAST_MESSAGE, content);
            contentValues.put(ChatDbHelper.LAST_USER_ID, chatUserId);
            contentValues.put(ChatDbHelper.LAST_USER_NAME, chatUserName);
            contentValues.put(ChatDbHelper.LAST_USER_IMAGE, chatUserImage);
            contentValues.put(ChatDbHelper.LAST_MESSAGE_DATE, message_date);
            if (unreadCount == 0) {
                contentValues.put(ChatDbHelper.UNREAD_COUNT, 0);
            } else {
                contentValues.put(ChatDbHelper.UNREAD_COUNT, old_unread_count + 1);
            }
            contentValues.put(ChatDbHelper.LAST_SEND_USER_TYPE, last_send_user_type);

            updateQuery(ChatDbHelper.TABLE_LAST_MESSAGE, contentValues, ChatDbHelper.CHANNEL_ID + "=" + channelId, null);
            last_cursor.close();
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatDbHelper.MESSAGE_TYPE, messageType);
            contentValues.put(ChatDbHelper.LAST_CHANNEL_ID, channelId);
            contentValues.put(ChatDbHelper.LAST_MESSAGE, content);
            contentValues.put(ChatDbHelper.LAST_USER_ID, chatUserId);
            contentValues.put(ChatDbHelper.LAST_USER_NAME, chatUserName);
            contentValues.put(ChatDbHelper.LAST_USER_IMAGE, chatUserImage);
            contentValues.put(ChatDbHelper.LAST_MESSAGE_DATE, message_date);
            contentValues.put(ChatDbHelper.UNREAD_COUNT, unreadCount);
            contentValues.put(ChatDbHelper.LAST_SEND_USER_TYPE, last_send_user_type);

            insertQuery(contentValues, ChatDbHelper.TABLE_LAST_MESSAGE);
        }
    }

    public void clearAllDatabaseTable() {
        SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        db.delete(ChatDbHelper.TABLE_LAST_MESSAGE, null, null);
        db.delete(ChatDbHelper.TABLE_USER, null, null);
        db.delete(ChatDbHelper.TABLE_MESSAGE, null, null);
    }
}
