package com.os.fastlap.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Created by anandj on 8/8/2017.
 */

public class ChatDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "fastlab_chat.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TAG = ChatDbHelper.class.getSimpleName();

    public static final String TABLE_USER = "users";
    public static final String TABLE_MESSAGE = "messages";
    public static final String TABLE_LAST_MESSAGE = "last_message";


    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_IMAGE = "user_image";


    public static final String MESSAGE_ID = "message_id";
    public static final String _ID = "_id";
    public static final String MESSAGE = "message";
    public static final String DATETIME = "dateTime";
    public static final String IS_READ = "is_read";
    public static final String SENDER_ID = "sender_id";
    public static final String RECIVER_ID = "reciver_id";
    public static final String CHANNEL_ID = "channel_id";


    public static final String LAST_CHANNEL_ID = "channel_id";
    public static final String LAST_MESSAGE = "message";
    public static final String LAST_USER_ID = "user_id";
    public static final String LAST_USER_NAME = "user_name";
    public static final String LAST_USER_IMAGE = "user_image";
    public static final String LAST_MESSAGE_DATE = "dateTime";
    public static final String UNREAD_COUNT = "unread_count";
    public static final String ONLINE_STATUS = "online_status";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String LAST_SEND_USER_TYPE = "last_send_user_type";


    private static final String SQL_CREATE_TABLE_USER = String.format("CREATE TABLE %s" + " (%s TEXT PRIMARY KEY, %s TEXT, %s TEXT)",
            TABLE_USER,
            USER_ID,
            USER_NAME,
            USER_IMAGE);

    private static final String SQL_CREATE_TABLE_MESSAGE = String.format("create table %s" + " (%s integer primary key autoincrement , %s text not null unique,%s text, %s text, %s integer, %s text, %s text, %s text,%s text)",
            TABLE_MESSAGE,
            MESSAGE_ID,
            _ID,
            MESSAGE,
            DATETIME,
            IS_READ,
            SENDER_ID,
            RECIVER_ID,
            CHANNEL_ID,
            MESSAGE_TYPE);

    private static final String SQL_CREATE_TABLE_LAST_MESSSAGE = String.format("create table %s" + " (%s TEXT primary key, %s text, %s text, %s text, %s text, %s text, %s integer,%s text,%s text)",
            TABLE_LAST_MESSAGE,
            LAST_CHANNEL_ID,
            LAST_MESSAGE,
            LAST_USER_ID,
            LAST_USER_NAME,
            LAST_USER_IMAGE,
            LAST_MESSAGE_DATE,
            UNREAD_COUNT,
            MESSAGE_TYPE,
            LAST_SEND_USER_TYPE);

    public ChatDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_USER);
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_MESSAGE);
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_LAST_MESSSAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_USER);
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_MESSAGE);
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_LAST_MESSAGE);
        onCreate(sqLiteDatabase);
    }
}
