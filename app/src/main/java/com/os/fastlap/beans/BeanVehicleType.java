package com.os.fastlap.beans;

/**
 * Created by abhinava on 8/4/2017.
 */

public class BeanVehicleType {
    public String id = "";
    public String name = "";
    public String status = "";
    public String unicode = "";

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    boolean isSelected = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode;
    }

    public BeanVehicleType() {

    }

    public BeanVehicleType(String id, String name) {
        setId(id);
        setName(name);
    }

}
