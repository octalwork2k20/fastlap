package com.os.fastlap.beans.gragemodals;

import java.io.Serializable;

/**
 * Created by anandj on 8/10/2017.
 */

public class VehicleBrandIdModal implements Serializable {
    String _id;
    String vehicleBrandName;
    String vehicleTypeId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getVehicleBrandName() {
        return vehicleBrandName;
    }

    public void setVehicleBrandName(String vehicleBrandName) {
        this.vehicleBrandName = vehicleBrandName;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }
}
