package com.os.fastlap.beans;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.os.fastlap.util.Util;
import com.os.fastlap.util.constants.S;

import java.io.File;
import java.io.Serializable;

/**
 * Created by riyazudinp on 2/9/2017.
 */

public class PagerBean implements Serializable{


    private File file;
    private String fileURl;
    private String fileName;
    private boolean isDefault;
    private String base64_img;
    private String imageUrl;
    private boolean isEdit;
    private String image;
    private String image_id;
    private String fileType;
    private Bitmap thumbnail;
    private File thumbnailFile;
    private boolean isNew = false;
    private static final String TAG = PagerBean.class.getSimpleName();

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public PagerBean(File file, String fileURl, String fileName, boolean isDefault, boolean isEdit, String image) {
        this.file = file;
        this.fileURl = fileURl;
        this.fileName = fileName;
        this.isDefault = isDefault;
        this.isEdit = isEdit;
        this.image = image;
    }

    public PagerBean(File file, String fileURl, String fileName, boolean isDefault, boolean isEdit, String image, boolean isNew, String image_id) {
        this.file = file;
        this.fileURl = fileURl;
        this.fileName = fileName;
        this.isDefault = isDefault;
        this.isEdit = isEdit;
        this.image = image;
        this.isNew = isNew;
        this.image_id = image_id;
    }

    public PagerBean(File file, String fileURl, String fileName, boolean isDefault, boolean isEdit, String image, String image_id) {
        this.file = file;
        this.fileURl = fileURl;
        this.fileName = fileName;
        this.isDefault = isDefault;
        this.isEdit = isEdit;
        this.image = image;
        this.image_id = image_id;
    }

    /* Default constructor */
    public PagerBean() {
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileURl() {
        return fileURl;
    }

    public void setFileURl(String fileURl) {
        this.fileURl = fileURl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getBase64_img() {
        return base64_img;
    }

    public void setBase64_img(String base64_img) {
        this.base64_img = base64_img;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType, final String filePath, final Context context) {
        this.fileType = fileType;
        if (fileType.equalsIgnoreCase(S.video))
            setThumbnail(Util.createVideoThumbnail(filePath), filePath, context);
        else
            setThumbnailFile(null);
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap bitmap, String filePath, Context context) {
        try {
            this.thumbnail = bitmap;
            setThumbnailFile(Util.storeImage(bitmap, context));
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public File getThumbnailFile() {
        return thumbnailFile;
    }

    public void setThumbnailFile(File thumbnailFile) {
        this.thumbnailFile = thumbnailFile;
    }
}


