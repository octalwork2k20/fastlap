
package com.os.fastlap.beans.dashboardmodals.tracklistmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Position implements Serializable
{

    @SerializedName("lng")
    @Expose
    private String lng="";
    @SerializedName("lat")
    @Expose
    private String lat="";
    private final static long serialVersionUID = -5976880880349872654L;

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

}
