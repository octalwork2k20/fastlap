package com.os.fastlap.beans;

import java.util.ArrayList;

/**
 * Created by abhinava on 11/22/2017.
 */

public class BeanAddAlbum
{
    String albumName = "";
    String trackId = "";
    String description = "";
    String dateOfEvent = "";
    int photoCount = 0;
    int videoCount = 0;
    String eventId = "";
    String groupTypeId = "";
    ArrayList<String> friendId = new ArrayList<>();
    ArrayList<PagerBean> mediaArrayList = new ArrayList<>();

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(String dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public int getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(int videoCount) {
        this.videoCount = videoCount;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public ArrayList<String> getFriendId() {
        return friendId;
    }

    public void setFriendId(ArrayList<String> friendId) {
        this.friendId = friendId;
    }

    public ArrayList<PagerBean> getMediaArrayList() {
        return mediaArrayList;
    }

    public void setMediaArrayList(ArrayList<PagerBean> mediaArrayList) {
        this.mediaArrayList = mediaArrayList;
    }

    public String getGroupTypeId() {
        return groupTypeId;
    }

    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }


}
