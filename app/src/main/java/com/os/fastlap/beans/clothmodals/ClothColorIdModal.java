package com.os.fastlap.beans.clothmodals;

/**
 * Created by anandj on 8/11/2017.
 */

public class ClothColorIdModal {
    String _id;
    String clothNameId;
    String clothModelId;
    String clothYearId;
    String name;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getClothNameId() {
        return clothNameId;
    }

    public void setClothNameId(String clothNameId) {
        this.clothNameId = clothNameId;
    }

    public String getClothModelId() {
        return clothModelId;
    }

    public void setClothModelId(String clothModelId) {
        this.clothModelId = clothModelId;
    }

    public String getClothYearId() {
        return clothYearId;
    }

    public void setClothYearId(String clothYearId) {
        this.clothYearId = clothYearId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
