package com.os.fastlap.beans.clothmodals;

/**
 * Created by anandj on 8/11/2017.
 */

public class ClothNameIdModal {
    String _id;
    String name;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
