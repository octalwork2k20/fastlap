package com.os.fastlap.beans;

import com.os.fastlap.beans.gragemodals.VehicleBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTyreBrandIdModal;
import com.os.fastlap.beans.gragemodals.VehicleTyreModelIdModal;
import com.os.fastlap.beans.gragemodals.VehicleVersionIdModal;
import com.os.fastlap.beans.gragemodals.VehicleYearIdModal;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abhinava on 8/4/2017.
 */

public class BeanVehicleList implements Serializable {
    String _id;
    String image;

    String description;
    VehicleTypeIdModal vehicleTypeIdModal;
    VehicleBrandIdModal vehicleBrandIdModal;
    VehicleModelIdModal vehicleModelIdModal;
    VehicleVersionIdModal vehicleVersionIdModal;
    VehicleTyreBrandIdModal vehicleTyreBrandIdModal;
    VehicleTyreModelIdModal vehicleTyreModelIdModal;
    VehicleYearIdModal vehicleYearIdModal;
    String uploadedLapCount;
    String trackVisitedCount;
    String bestRanking;
    String bestSpeed;


    public String getUploadedLapCount() {
        return uploadedLapCount;
    }

    public void setUploadedLapCount(String uploadedLapCount) {
        this.uploadedLapCount = uploadedLapCount;
    }

    public String getTrackVisitedCount() {
        return trackVisitedCount;
    }

    public void setTrackVisitedCount(String trackVisitedCount) {
        this.trackVisitedCount = trackVisitedCount;
    }

    public String getBestRanking() {
        return bestRanking;
    }

    public void setBestRanking(String bestRanking) {
        this.bestRanking = bestRanking;
    }

    public String getBestSpeed() {
        return bestSpeed;
    }

    public void setBestSpeed(String bestSpeed) {
        this.bestSpeed = bestSpeed;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public VehicleTypeIdModal getVehicleTypeIdModal() {
        return vehicleTypeIdModal;
    }

    public void setVehicleTypeIdModal(VehicleTypeIdModal vehicleTypeIdModal) {
        this.vehicleTypeIdModal = vehicleTypeIdModal;
    }

    public VehicleBrandIdModal getVehicleBrandIdModal() {
        return vehicleBrandIdModal;
    }

    public void setVehicleBrandIdModal(VehicleBrandIdModal vehicleBrandIdModal) {
        this.vehicleBrandIdModal = vehicleBrandIdModal;
    }

    public VehicleModelIdModal getVehicleModelIdModal() {
        return vehicleModelIdModal;
    }

    public void setVehicleModelIdModal(VehicleModelIdModal vehicleModelIdModal) {
        this.vehicleModelIdModal = vehicleModelIdModal;
    }

    public VehicleVersionIdModal getVehicleVersionIdModal() {
        return vehicleVersionIdModal;
    }

    public void setVehicleVersionIdModal(VehicleVersionIdModal vehicleVersionIdModal) {
        this.vehicleVersionIdModal = vehicleVersionIdModal;
    }

    public VehicleTyreBrandIdModal getVehicleTyreBrandIdModal() {
        return vehicleTyreBrandIdModal;
    }

    public void setVehicleTyreBrandIdModal(VehicleTyreBrandIdModal vehicleTyreBrandIdModal) {
        this.vehicleTyreBrandIdModal = vehicleTyreBrandIdModal;
    }

    public VehicleTyreModelIdModal getVehicleTyreModelIdModal() {
        return vehicleTyreModelIdModal;
    }

    public void setVehicleTyreModelIdModal(VehicleTyreModelIdModal vehicleTyreModelIdModal) {
        this.vehicleTyreModelIdModal = vehicleTyreModelIdModal;
    }

    public VehicleYearIdModal getVehicleYearIdModal() {
        return vehicleYearIdModal;
    }

    public void setVehicleYearIdModal(VehicleYearIdModal vehicleYearIdModal) {
        this.vehicleYearIdModal = vehicleYearIdModal;
    }

   /* public String getImage() {
        return image;
    }

    public void setImage(String image) {
        String[] image_arr = image.split(",");
        setImages_list(new ArrayList<String>(Arrays.asList(image_arr)));
        this.image = image;
    }*/

    ArrayList<PagerBean> imagesList = new ArrayList<>();

    public ArrayList<PagerBean> getImagesList() {
        return imagesList;
    }

    public void setImagesList(ArrayList<PagerBean> imagesList) {
        this.imagesList = imagesList;
    }


}
