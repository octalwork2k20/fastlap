package com.os.fastlap.beans;

import android.widget.PopupWindow;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by abhinava on 9/20/2017.
 */

public class MarkerPopUpWindowModel
{
    PopupWindow popupWindow;
    Marker marker;

    public PopupWindow getPopupWindow() {
        return popupWindow;
    }

    public void setPopupWindow(PopupWindow popupWindow) {
        this.popupWindow = popupWindow;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    int height;
    int width;

}
