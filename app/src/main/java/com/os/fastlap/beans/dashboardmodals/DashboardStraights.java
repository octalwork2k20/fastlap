package com.os.fastlap.beans.dashboardmodals;

/**
 * Created by anandj on 8/23/2017.
 */

public class DashboardStraights {
    String lengthOfStraights;
    String totalStraights;

    public String getLengthOfStraights() {
        return lengthOfStraights;
    }

    public void setLengthOfStraights(String lengthOfStraights) {
        this.lengthOfStraights = lengthOfStraights;
    }

    public String getTotalStraights() {
        return totalStraights;
    }

    public void setTotalStraights(String totalStraights) {
        this.totalStraights = totalStraights;
    }
}
