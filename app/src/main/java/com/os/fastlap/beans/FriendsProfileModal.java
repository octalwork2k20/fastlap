package com.os.fastlap.beans;

import java.util.List;

/**
 * Created by anandj on 7/14/2017.
 */

public class FriendsProfileModal {
    String id;
    String heading_name;
    List<ProfileMyFriendsList> friendsProfileChildModalList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeading_name() {
        return heading_name;
    }

    public void setHeading_name(String heading_name) {
        this.heading_name = heading_name;
    }

    public List<ProfileMyFriendsList> getFriendsProfileChildModalList() {
        return friendsProfileChildModalList;
    }

    public void setFriendsProfileChildModalList(List<ProfileMyFriendsList> friendsProfileChildModalList) {
        this.friendsProfileChildModalList = friendsProfileChildModalList;
    }
}
