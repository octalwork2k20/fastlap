package com.os.fastlap.beans;

/**
 * Created by abhinava on 9/21/2017.
 */

public class BeanWeather
{
    String day="";
    String high="";
    String low="";
    String date="";

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHigh() {
        return high;
    }

    public String getLow() {
        return low;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public void setLow(String low) {
        this.low = low;
    }

    String text="";
}
