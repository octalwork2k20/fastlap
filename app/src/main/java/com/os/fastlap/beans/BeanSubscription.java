package com.os.fastlap.beans;

/**
 * Created by abhinava on 9/26/2017.
 */

public class BeanSubscription
{
    String id="";
    String created="";
    String status="";
    String comparison="";
    String sessions="";
    String laps="";
    String garage="";
    String media="";
    String price="";
    String name="";
    String purchaseId="";

    String typeofFilters="";
    String dataComparisonCapability="";
    String vehicles="";

    public String getDataComparisonCapability() {
        return dataComparisonCapability;
    }

    public void setDataComparisonCapability(String dataComparisonCapability) {
        this.dataComparisonCapability = dataComparisonCapability;
    }

    public String getTypeofFilters() {
        return typeofFilters;
    }

    public void setTypeofFilters(String typeofFilters) {
        this.typeofFilters = typeofFilters;
    }

    public String getVehicles() {
        return vehicles;
    }

    public void setVehicles(String vehicles) {
        this.vehicles = vehicles;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getSubscriptionEndDate() {
        return subscriptionEndDate;
    }

    public void setSubscriptionEndDate(String subscriptionEndDate) {
        this.subscriptionEndDate = subscriptionEndDate;
    }

    String subscriptionEndDate="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComparison() {
        return comparison;
    }

    public void setComparison(String comparison) {
        this.comparison = comparison;
    }

    public String getSessions() {
        return sessions;
    }

    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    public String getLaps() {
        return laps;
    }

    public void setLaps(String laps) {
        this.laps = laps;
    }

    public String getGarage() {
        return garage;
    }

    public void setGarage(String garage) {
        this.garage = garage;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
