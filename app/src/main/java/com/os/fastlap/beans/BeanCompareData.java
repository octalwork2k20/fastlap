package com.os.fastlap.beans;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhinava on 11/1/2017.
 */

public class BeanCompareData
{
    public ArrayList<BeanLocation> getLocationArrayList() {
        return locationArrayList;
    }
    public ArrayList<Double> getTimeList() {
        return timeList;
    }
    public ArrayList<Double> getDistanceList() {
        return distanceList;
    }

    public void setDistanceList(ArrayList<Double> distanceList) {
        this.distanceList = distanceList;
    }

    public void setTimeList(ArrayList<Double> timeList) {
        this.timeList = timeList;
    }

    public void setLocationArrayList(ArrayList<BeanLocation> locationArrayList) {
        this.locationArrayList = locationArrayList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    ArrayList<BeanLocation> locationArrayList=new ArrayList<>();
    ArrayList<Double> timeList=new ArrayList<>();
    ArrayList<Double> distanceList=new ArrayList<>();
    String id="";

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    int color;

    public List<LatLng> getRouteLatLng() {
        return routeLatLng;
    }

    public void setRouteLatLng(List<LatLng> routeLatLng) {
        this.routeLatLng = routeLatLng;
    }

    List<LatLng> routeLatLng = new ArrayList<>();

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }

    Polyline polyline;

    public int getRunSpeed() {
        return runSpeed;
    }

    public void setRunSpeed(int runSpeed) {
        this.runSpeed = runSpeed;
    }

    int runSpeed;
}
