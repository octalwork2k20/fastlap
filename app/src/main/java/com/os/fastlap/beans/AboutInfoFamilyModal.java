package com.os.fastlap.beans;

import java.io.Serializable;

/**
 * Created by anandj on 8/21/2017.
 */

public class AboutInfoFamilyModal implements Serializable {
    String id;
    String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
