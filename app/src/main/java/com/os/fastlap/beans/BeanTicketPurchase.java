package com.os.fastlap.beans;

import java.io.Serializable;

/**
 * Created by abhinava on 11/23/2017.
 */

public class BeanTicketPurchase implements Serializable{

    String eventId = "";
    String transactionId = "";
    String quantity = "";
    String amount = "";
    String ticketDescription = "";
    String vehicleId="";

    public String getPresetsId() {
        return presetsId;
    }

    public void setPresetsId(String presetsId) {
        this.presetsId = presetsId;
    }

    String presetsId="";

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }



    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }


}
