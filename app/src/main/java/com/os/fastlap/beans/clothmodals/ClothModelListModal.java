package com.os.fastlap.beans.clothmodals;

import com.os.fastlap.beans.gragemodals.VehicleTypeIdModal;

/**
 * Created by anandj on 8/11/2017.
 */

public class ClothModelListModal {

    String _id;
    String clothName;
    String clothImage;
    ClothNameIdModal clothNameIdModal;
    ClothModelIdModal clothModelIdModal;
    VehicleTypeIdModal vehicleTypeIdModal;
    ClothYearIdModal clothYearIdModal;
    ClothColorIdModal clothColorIdModal;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ClothNameIdModal getClothNameIdModal() {
        return clothNameIdModal;
    }

    public void setClothNameIdModal(ClothNameIdModal clothNameIdModal) {
        this.clothNameIdModal = clothNameIdModal;
    }

    public ClothModelIdModal getClothModelIdModal() {
        return clothModelIdModal;
    }

    public void setClothModelIdModal(ClothModelIdModal clothModelIdModal) {
        this.clothModelIdModal = clothModelIdModal;
    }
    public void setVehicleTypeIdModal(VehicleTypeIdModal clothModelIdModal) {
        this.vehicleTypeIdModal = clothModelIdModal;
    }

    public VehicleTypeIdModal getVehicleTypeIdModal() {
        return vehicleTypeIdModal;
    }

    public ClothYearIdModal getClothYearIdModal() {
        return clothYearIdModal;
    }

    public void setClothYearIdModal(ClothYearIdModal clothYearIdModal) {
        this.clothYearIdModal = clothYearIdModal;
    }

    public ClothColorIdModal getClothColorIdModal() {
        return clothColorIdModal;
    }

    public void setClothColorIdModal(ClothColorIdModal clothColorIdModal) {
        this.clothColorIdModal = clothColorIdModal;
    }

    public String getClothName() {
        return clothName;
    }

    public void setClothName(String clothName) {
        this.clothName = clothName;
    }

    public String getClothImage() {
        return clothImage;
    }

    public void setClothImage(String clothImage) {
        this.clothImage = clothImage;
    }

}
