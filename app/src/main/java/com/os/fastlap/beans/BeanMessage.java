package com.os.fastlap.beans;

import java.io.Serializable;

/**
 * Created by abhinava on 9/25/2017.
 */

public class BeanMessage implements Serializable{

    public String messageId = "";
    public String messageText = "";
    public String messageDate = "";

    public String getMessageTimebin() {
        return messageTimebin;
    }

    public void setMessageTimebin(String messageTimebin) {
        this.messageTimebin = messageTimebin;
    }

    public String messageTimebin = "";
    public String messageSenderId = "";
    public String messageSenderName = "";
    public String messageSenderImage = "";
    public String messageReciverId = "";
    public String messageReciverName = "";
    public String messageReciverImage = "";
    public String chatType = "";
    public boolean onLineStatus = false;

    public boolean getOnLineStatus() {
        return onLineStatus;
    }

    public void setOnLineStatus(boolean onLineStatus) {
        this.onLineStatus = onLineStatus;
    }

    public String getLastSendUserType() {
        return lastSendUserType;
    }

    public void setLastSendUserType(String lastSendUserType) {
        this.lastSendUserType = lastSendUserType;
    }

    public String lastSendUserType = "";

    public int getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    public int unReadCount = 0;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String channelId = "";


    public int getIsReadStatus() {
        return isReadStatus;
    }

    public void setIsReadStatus(int isReadStatus) {
        this.isReadStatus = isReadStatus;
    }

    public int isReadStatus = 0;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessageSenderId() {
        return messageSenderId;
    }

    public void setMessageSenderId(String messageSenderId) {
        this.messageSenderId = messageSenderId;
    }

    public String getMessageSenderName() {
        return messageSenderName;
    }

    public void setMessageSenderName(String messageSenderName) {
        this.messageSenderName = messageSenderName;
    }

    public String getMessageSenderImage() {
        return messageSenderImage;
    }

    public void setMessageSenderImage(String messageSenderImage) {
        this.messageSenderImage = messageSenderImage;
    }

    public String getMessageReciverId() {
        return messageReciverId;
    }

    public void setMessageReciverId(String messageReciverId) {
        this.messageReciverId = messageReciverId;
    }

    public String getMessageReciverName() {
        return messageReciverName;
    }

    public void setMessageReciverName(String messageReciverName) {
        this.messageReciverName = messageReciverName;
    }

    public String getMessageReciverImage() {
        return messageReciverImage;
    }

    public void setMessageReciverImage(String messageReciverImage) {
        this.messageReciverImage = messageReciverImage;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }


}
