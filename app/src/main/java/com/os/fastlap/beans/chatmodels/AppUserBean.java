package com.os.fastlap.beans.chatmodels;

import java.io.Serializable;

/**
 * Created by abhinava on 8/24/2017.
 */

public class AppUserBean implements Serializable {
    String _id = "";
    String email = "";
    String username = "";
    private PersonalInfo personalInfo;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    String friendDataId = "";
    String friendDatafromuserId = "";
    String friendDatatouserId = "";
    String friendDataStatus = "";

    public String getFriendDataId() {
        return friendDataId;
    }

    public void setFriendDataId(String friendDataId) {
        this.friendDataId = friendDataId;
    }

    public String getFriendDatafromuserId() {
        return friendDatafromuserId;
    }

    public void setFriendDatafromuserId(String friendDatafromuserId) {
        this.friendDatafromuserId = friendDatafromuserId;
    }

    public String getFriendDatatouserId() {
        return friendDatatouserId;
    }

    public void setFriendDatatouserId(String friendDatatouserId) {
        this.friendDatatouserId = friendDatatouserId;
    }

    public String getFriendDataStatus() {
        return friendDataStatus;
    }

    public void setFriendDataStatus(String friendDataStatus) {
        this.friendDataStatus = friendDataStatus;
    }


}
