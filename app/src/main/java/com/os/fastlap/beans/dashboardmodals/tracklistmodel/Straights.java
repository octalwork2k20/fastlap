
package com.os.fastlap.beans.dashboardmodals.tracklistmodel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Straights implements Serializable
{

    @SerializedName("lengthOfStraights")
    @Expose
    private Integer lengthOfStraights;
    @SerializedName("totalStraights")
    @Expose
    private Integer totalStraights;
    private final static long serialVersionUID = 942347410202114614L;

    public Integer getLengthOfStraights() {
        return lengthOfStraights;
    }

    public void setLengthOfStraights(Integer lengthOfStraights) {
        this.lengthOfStraights = lengthOfStraights;
    }

    public Integer getTotalStraights() {
        return totalStraights;
    }

    public void setTotalStraights(Integer totalStraights) {
        this.totalStraights = totalStraights;
    }

}
