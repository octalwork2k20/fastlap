package com.os.fastlap.beans;

import java.util.ArrayList;

/**
 * Created by monikab on 1/12/2016.
 */
public class Bean_UserDetail {
    String user_id = "";
    String user_name = "";
    String profile_image = "";
    String created = "";
    String is_friend = "";
    String comment = "";
    String id = "";
    String feed_id = "";
    String type = "";
    String first_name = "";
    String last_name = "";
    String feed_owner_id = "";
    String like_count = "";
    String is_like = "";
    ArrayList<Bean_Reply> reply_Array ;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String email="";

    public ArrayList<Bean_Reply> getReply_Array() {
        return reply_Array;
    }

    public void setReply_Array(ArrayList<Bean_Reply> reply_Array) {
        this.reply_Array = reply_Array;
    }
/*public JSONArray getReply_Array() {
        return reply_Array;
    }

    public void setReply_Array(ArrayList<String> reply_Array) {
        this.reply_Array = reply_Array;
    }*/


    public String getReply_count() {
        return reply_count;
    }

    public void setReply_count(String reply_count) {
        this.reply_count = reply_count;
    }

    String reply_count = "";

    public String getLogin_user_id() {
        return login_user_id;
    }

    public void setLogin_user_id(String login_user_id) {
        this.login_user_id = login_user_id;
    }

    String login_user_id = "";

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    String role_id = "";

    public String getType() {
        return type;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeed_owner_id() {
        return feed_owner_id;
    }

    public void setFeed_owner_id(String feed_owner_id) {
        this.feed_owner_id = feed_owner_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getIs_friend() {
        return is_friend;
    }

    public void setIs_friend(String is_friend) {
        this.is_friend = is_friend;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
