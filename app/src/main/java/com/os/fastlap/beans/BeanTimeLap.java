package com.os.fastlap.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abhinava on 9/18/2017.
 */

public class BeanTimeLap implements Serializable {
    String timeLap_Id = "";
    String userId = "";
    String userName = "";
    String userImage = "";
    String lapId = "";
    String lapName = "";
    String lapDescription = "";
    String trackName = "";
    String trackId = "";
    String lap_type = "";
    String averageSpeedToShow = "";

    public String getAverageSpeedToShow() {
        return averageSpeedToShow;
    }

    public void setAverageSpeedToShow(String averageSpeedToShow) {
        this.averageSpeedToShow = averageSpeedToShow;
    }

    public String getLap_type() {
        return lap_type;
    }

    public void setLap_type(String lap_type) {
        this.lap_type = lap_type;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    String fileUrl = "";

    public String getTrackVersionId() {
        return trackVersionId;
    }

    public void setTrackVersionId(String trackVersionId) {
        this.trackVersionId = trackVersionId;
    }

    String trackVersionId = "";

    String created = "";
    String weather = "";
    String lapDate = "";
    String time = "";
    String numberOfLapsFound = "";
    String softwareVersion = "";

    String userVehicleId = "";
    String vehicleTypeId = "";
    String vehicleBrandId = "";
    String vehicleBrandName = "";
    String vehicleModelId = "";
    String vehicleTyreModelId = "";
    String vehicleTyreModelName = "";
    String vehicleTyreBrandName = "";
    String vehicleTyreBrandId = "";
    String vehicleModelName = "";
    String vehicleVersionId = "";
    String gpsDevices = "";
    String youtubeURL = "";
    String lapImage = "";
    String bestLapTime = "";
    String maxSpeed = "";
    String avgSpeed = "";

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    String distance = "";
    String rank = "";
    String accel_x="";
    String accel_y="";
    String accel_z="";
    String brake_calc="0",acc_calc="0",brake_on="0",acc_on="0";

    public String getBrake_on() {
        return brake_on;
    }
    public String getBrake_calc() {
        return brake_calc;
    }

    public String getAcc_on() {
        return acc_on;
    }

    public String getAcc_calc() {
        return acc_calc;
    }

    public void setBrake_on(String brake_on) {
        this.brake_on = brake_on;
    }

    public void setAcc_on(String acc_on) {
        this.acc_on = acc_on;
    }

    public void setBrake_calc(String brake_calc) {
        this.brake_calc = brake_calc;
    }

    public void setAcc_calc(String acc_calc) {
        this.acc_calc = acc_calc;
    }

    public String getAccel_z() {
        return accel_z;
    }

    public String getAccel_y() {
        return accel_y;
    }

    public String getAccel_x() {
        return accel_x;
    }

    public void setAccel_z(String accel_z) {
        this.accel_z = accel_z;
    }

    public void setAccel_y(String accel_y) {
        this.accel_y = accel_y;
    }

    public void setAccel_x(String accel_x) {
        this.accel_x = accel_x;
    }

    public int getLapColor() {
        return lapColor;
    }

    public void setLapColor(int lapColor) {
        this.lapColor = lapColor;
    }

    int lapColor = 0;

    public String getGroupTypeId() {
        return groupTypeId;
    }

    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }

    String groupTypeId = "";

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    String selectedValue = "";

    public String getVehicleTyreModelId() {
        return vehicleTyreModelId;
    }

    public void setVehicleTyreModelId(String vehicleTyreModelId) {
        this.vehicleTyreModelId = vehicleTyreModelId;
    }

    public String getVehicleTyreModelName() {
        return vehicleTyreModelName;
    }

    public void setVehicleTyreModelName(String vehicleTyreModelName) {
        this.vehicleTyreModelName = vehicleTyreModelName;
    }

    public String getVehicleTyreBrandName() {
        return vehicleTyreBrandName;
    }

    public void setVehicleTyreBrandName(String vehicleTyreBrandName) {
        this.vehicleTyreBrandName = vehicleTyreBrandName;
    }

    public String getVehicleTyreBrandId() {
        return vehicleTyreBrandId;
    }

    public void setVehicleTyreBrandId(String vehicleTyreBrandId) {
        this.vehicleTyreBrandId = vehicleTyreBrandId;
    }


    public String getUserVehicleId() {
        return userVehicleId;
    }

    public void setUserVehicleId(String userVehicleId) {
        this.userVehicleId = userVehicleId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleBrandId() {
        return vehicleBrandId;
    }

    public void setVehicleBrandId(String vehicleBrandId) {
        this.vehicleBrandId = vehicleBrandId;
    }

    public String getVehicleBrandName() {
        return vehicleBrandName;
    }

    public void setVehicleBrandName(String vehicleBrandName) {
        this.vehicleBrandName = vehicleBrandName;
    }

    public String getVehicleModelId() {
        return vehicleModelId;
    }

    public void setVehicleModelId(String vehicleModelId) {
        this.vehicleModelId = vehicleModelId;
    }

    public String getVehicleModelName() {
        return vehicleModelName;
    }

    public void setVehicleModelName(String vehicleModelName) {
        this.vehicleModelName = vehicleModelName;
    }

    public String getVehicleVersionId() {
        return vehicleVersionId;
    }

    public void setVehicleVersionId(String vehicleVersionId) {
        this.vehicleVersionId = vehicleVersionId;
    }

    public String getNumberOfLapsFound() {
        return numberOfLapsFound;
    }

    public void setNumberOfLapsFound(String numberOfLapsFound) {
        this.numberOfLapsFound = numberOfLapsFound;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public String getGpsDevices() {
        return gpsDevices;
    }

    public void setGpsDevices(String gpsDevices) {
        this.gpsDevices = gpsDevices;
    }

    public String getYoutubeURL() {
        return youtubeURL;
    }

    public void setYoutubeURL(String youtubeURL) {
        this.youtubeURL = youtubeURL;
    }

    public String getTimeLap_Id() {
        return timeLap_Id;
    }

    public void setTimeLap_Id(String timeLap_Id) {
        this.timeLap_Id = timeLap_Id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getLapId() {
        return lapId;
    }

    public void setLapId(String lapId) {
        this.lapId = lapId;
    }

    public String getLapName() {
        return lapName;
    }

    public void setLapName(String lapName) {
        this.lapName = lapName;
    }

    public String getLapDescription() {
        return lapDescription;
    }

    public void setLapDescription(String lapDescription) {
        this.lapDescription = lapDescription;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getLapDate() {
        return lapDate;
    }

    public void setLapDate(String lapDate) {
        this.lapDate = lapDate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLapImage() {
        return lapImage;
    }

    public void setLapImage(String lapImage) {
        this.lapImage = lapImage;
    }

    public ArrayList<UserLapImageData> getUserLapImageDataList() {
        return userLapImageDataList;
    }

    public void setUserLapImageDataList(ArrayList<UserLapImageData> userLapImageDataList) {
        this.userLapImageDataList = userLapImageDataList;
    }

    public String getBestLapTime() {
        return bestLapTime;
    }

    public void setBestLapTime(String bestLapTime) {
        this.bestLapTime = bestLapTime;
    }

    public String getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(String maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(String avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }


    ArrayList<UserLapImageData> userLapImageDataList = new ArrayList<>();

    public class UserLapImageData implements Serializable {
        String userLapId = "";

        public String getUserLapId() {
            return userLapId;
        }

        public void setUserLapId(String userLapId) {
            this.userLapId = userLapId;
        }

        public String getImageLog() {
            return imageLog;
        }

        public void setImageLog(String imageLog) {
            this.imageLog = imageLog;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        String imageLog = "";
        String type = "";
        String status = "";
        String _id = "";

    }

}
