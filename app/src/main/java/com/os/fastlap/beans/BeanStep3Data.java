package com.os.fastlap.beans;

import java.util.ArrayList;

/**
 * Created by abhinava on 1/4/2018.
 */

public class BeanStep3Data {
    String sessionNo = "";
    String videoURL = "";
    String videoImage = "";
    String channelTitle = "";
    String VideoTitle = "";

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    String sessionId = "";
    ArrayList<PagerBean> imageList = new ArrayList<>();

    public String getSessionNo() {
        return sessionNo;
    }

    public void setSessionNo(String sessionNo) {
        this.sessionNo = sessionNo;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        VideoTitle = videoTitle;
    }

    public ArrayList<PagerBean> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<PagerBean> imageList) {
        this.imageList = imageList;
    }


}
