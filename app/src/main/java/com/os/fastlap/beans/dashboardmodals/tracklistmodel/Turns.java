
package com.os.fastlap.beans.dashboardmodals.tracklistmodel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Turns implements Serializable
{

    @SerializedName("slowRightTurns")
    @Expose
    private Integer slowRightTurns;
    @SerializedName("slowLeftTurns")
    @Expose
    private Integer slowLeftTurns;
    @SerializedName("rightTurns")
    @Expose
    private Integer rightTurns;
    @SerializedName("leftTurns")
    @Expose
    private Integer leftTurns;
    @SerializedName("totalTurns")
    @Expose
    private Integer totalTurns;
    private final static long serialVersionUID = 4664485092881867126L;

    public Integer getSlowRightTurns() {
        return slowRightTurns;
    }

    public void setSlowRightTurns(Integer slowRightTurns) {
        this.slowRightTurns = slowRightTurns;
    }

    public Integer getSlowLeftTurns() {
        return slowLeftTurns;
    }

    public void setSlowLeftTurns(Integer slowLeftTurns) {
        this.slowLeftTurns = slowLeftTurns;
    }

    public Integer getRightTurns() {
        return rightTurns;
    }

    public void setRightTurns(Integer rightTurns) {
        this.rightTurns = rightTurns;
    }

    public Integer getLeftTurns() {
        return leftTurns;
    }

    public void setLeftTurns(Integer leftTurns) {
        this.leftTurns = leftTurns;
    }

    public Integer getTotalTurns() {
        return totalTurns;
    }

    public void setTotalTurns(Integer totalTurns) {
        this.totalTurns = totalTurns;
    }

}
