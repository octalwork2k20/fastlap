package com.os.fastlap.beans.dashboardmodals;

import com.os.fastlap.beans.chatmodels.PersonalInfo;

import java.io.Serializable;

/**
 * Created by anandj on 9/19/2017.
 */

public class FriendIdBean implements Serializable{
    private String _id;
    private String username;
    private String postId;
    private String email;
    private PersonalInfo personalInfo;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }
}
