package com.os.fastlap.beans;

import java.io.Serializable;

/**
 * Created by abhinava on 10/31/2017.
 */

public class BeanLocation implements Serializable
{
    private String time="0",speed="0",dist="0",lat="0",lng="0",accel_x="0",accel_y="0",accel_z="0",brake_calc="0",acc_calc="0",brake_on="0",acc_on="0";

    public void setDist(String dist) {
        this.dist = dist;
    }

    public void setAccel_x(String accel_x) {
        this.accel_x = accel_x;
    }

    public void setAccel_y(String accel_y) {
        this.accel_y = accel_y;
    }

    public void setAccel_z(String accel_z) {
        this.accel_z = accel_z;
    }

    public void setAcc_calc(String acc_calc) {
        this.acc_calc = acc_calc;
    }

    public void setBrake_calc(String brake_calc) {
        this.brake_calc = brake_calc;
    }

    public void setAcc_on(String acc_on) {
        this.acc_on = acc_on;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setBrake_on(String brake_on) {
        this.brake_on = brake_on;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAccel_x() {
        return accel_x;
    }

    public String getAccel_y() {
        return accel_y;
    }

    public String getAccel_z() {
        return accel_z;
    }

    public String getAcc_calc() {
        return acc_calc;
    }

    public String getAcc_on() {
        return acc_on;
    }

    public String getBrake_calc() {
        return brake_calc;
    }

    public String getBrake_on() {
        return brake_on;
    }

    public String getDist() {
        return dist;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getSpeed() {
        return speed;
    }

    public String getTime() {
        return time;
    }

}
