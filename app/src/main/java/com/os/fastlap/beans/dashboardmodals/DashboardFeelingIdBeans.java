package com.os.fastlap.beans.dashboardmodals;

import java.io.Serializable;

/**
 * Created by anandj on 9/1/2017.
 */

public class DashboardFeelingIdBeans implements Serializable {
    private String _id;
    private int status;
    private String unicodes;
    private String name;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUnicodes() {
        return unicodes;
    }

    public void setUnicodes(String unicodes) {
        this.unicodes = unicodes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
