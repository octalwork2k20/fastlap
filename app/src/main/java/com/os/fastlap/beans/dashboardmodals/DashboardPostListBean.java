package com.os.fastlap.beans.dashboardmodals;

/*
 * Created by anandj on 8/31/2017.
 */

import com.os.fastlap.beans.BeanVehicleList;
import com.os.fastlap.beans.LapTime;
import com.os.fastlap.beans.UserDataBeans;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.DashboardTrackListingAll;
import com.os.fastlap.beans.dashboardmodals.tracklistmodel.TrackData;

import java.io.Serializable;
import java.util.List;

public class DashboardPostListBean implements Serializable {
    private String _id;
    private UserDataBeans userDataBeans;
    private CommentBeans commentBeans;
    private int status;
    private String description;
    private String weather="";
    private String speed="";
    private String position_text;
    private DashboardFeelingIdBeans dashboardFeelingIdBeans;
    private DashboardTrackListingAll dashboardTrackListingAll;
    private TrackData trackData;
    private BeanVehicleList beanVehicleList;
    private List<DashboardPostImagesBeans> dashboardPostImagesBeans;
    private String likedPost;
    private String likedPostCount;
    private String commentPostCount;
    private String modified;
    private String created;
    private List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeanses;
    private List<DashboardPostShareUserModal> dashboardPostShareUserModals;
    private LapTime lapTime;


    public CommentBeans getCommentBeans() {
        return commentBeans;
    }

    public void setCommentBeans(CommentBeans commentBeans) {
        this.commentBeans = commentBeans;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public UserDataBeans getUserDataBeans() {
        return userDataBeans;
    }

    public void setUserDataBeans(UserDataBeans userDataBeans) {
        this.userDataBeans = userDataBeans;
    }

    public void setLapTime(LapTime lapTime) {
        this.lapTime = lapTime;
    }

    public LapTime getLapTime() {
        return lapTime;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPosition_text() {
        return position_text;
    }

    public void setPosition_text(String position_text) {
        this.position_text = position_text;
    }

    public DashboardFeelingIdBeans getDashboardFeelingIdBeans() {
        return dashboardFeelingIdBeans;
    }

    public void setDashboardFeelingIdBeans(DashboardFeelingIdBeans dashboardFeelingIdBeans) {
        this.dashboardFeelingIdBeans = dashboardFeelingIdBeans;
    }

    public DashboardTrackListingAll getDashboardTrackListingAll() {
        return dashboardTrackListingAll;
    }

    public void setDashboardTrackListingAll(DashboardTrackListingAll dashboardTrackListingAll) {
        this.dashboardTrackListingAll = dashboardTrackListingAll;
    }

    public BeanVehicleList getBeanVehicleList() {
        return beanVehicleList;
    }

    public void setBeanVehicleList(BeanVehicleList beanVehicleList) {
        this.beanVehicleList = beanVehicleList;
    }

    public List<DashboardPostImagesBeans> getDashboardPostImagesBeans() {
        return dashboardPostImagesBeans;
    }

    public void setDashboardPostImagesBeans(List<DashboardPostImagesBeans> dashboardPostImagesBeans) {
        this.dashboardPostImagesBeans = dashboardPostImagesBeans;
    }

    public String getLikedPost() {
        return likedPost;
    }

    public void setLikedPost(String likedPost) {
        this.likedPost = likedPost;
    }

    public String getLikedPostCount() {
        return likedPostCount;
    }

    public void setLikedPostCount(String likedPostCount) {
        this.likedPostCount = likedPostCount;
    }

    public String getCommentPostCount() {
        return commentPostCount;
    }

    public void setCommentPostCount(String commentPostCount) {
        this.commentPostCount = commentPostCount;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public TrackData getTrackData() {
        return trackData;
    }

    public void setTrackData(TrackData trackData) {
        this.trackData = trackData;
    }

    public List<DashboardCommentPostTagFriendsBeans> getDashboardCommentPostTagFriendsBeanses() {
        return dashboardCommentPostTagFriendsBeanses;
    }

    public void setDashboardCommentPostTagFriendsBeanses(List<DashboardCommentPostTagFriendsBeans> dashboardCommentPostTagFriendsBeanses) {
        this.dashboardCommentPostTagFriendsBeanses = dashboardCommentPostTagFriendsBeanses;
    }

    public List<DashboardPostShareUserModal> getDashboardPostShareUserModals() {
        return dashboardPostShareUserModals;
    }

    public void setDashboardPostShareUserModals(List<DashboardPostShareUserModal> dashboardPostShareUserModals) {
        this.dashboardPostShareUserModals = dashboardPostShareUserModals;
    }
}
