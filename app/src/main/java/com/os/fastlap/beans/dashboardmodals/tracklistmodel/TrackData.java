
package com.os.fastlap.beans.dashboardmodals.tracklistmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TrackData implements Serializable {

    String country="";
    @SerializedName("_id")
    @Expose
    private String id="";
    @SerializedName("ownerId")
    @Expose
    private String ownerId="";
    @SerializedName("name")
    @Expose
    private String name="";
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("created")
    @Expose
    private String created="";
    @SerializedName("modified")
    @Expose
    private String modified="";
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("position")
    @Expose
    private Position position;
    @SerializedName("contactDetails")
    @Expose
    private ContactDetails contactDetails;
    @SerializedName("aboutTrack")
    @Expose
    private String aboutTrack="";
    @SerializedName("totalLapRecords")
    @Expose
    private Integer totalLapRecords;
    @SerializedName("trackOpenDate")
    @Expose
    private String trackOpenDate="";
    @SerializedName("straights")
    @Expose
    private Straights straights;
    @SerializedName("turns")
    @Expose
    private Turns turns;
    @SerializedName("trackLength")
    @Expose
    private Integer trackLength;
    @SerializedName("location")
    @Expose
    private String location="";
    @SerializedName("coverImage")
    @Expose
    private String coverImage="";
    @SerializedName("image")
    @Expose
    private String image="";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @SerializedName("version")
    @Expose
    private String version="";

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    @SerializedName("version_id")
    @Expose
    private String versionId="";

    private final static long serialVersionUID = -1765428219805456957L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getAboutTrack() {
        return aboutTrack;
    }

    public void setAboutTrack(String aboutTrack) {
        this.aboutTrack = aboutTrack;
    }

    public Integer getTotalLapRecords() {
        return totalLapRecords;
    }

    public void setTotalLapRecords(Integer totalLapRecords) {
        this.totalLapRecords = totalLapRecords;
    }

    public String getTrackOpenDate() {
        return trackOpenDate;
    }

    public void setTrackOpenDate(String trackOpenDate) {
        this.trackOpenDate = trackOpenDate;
    }

    public Straights getStraights() {
        return straights;
    }

    public void setStraights(Straights straights) {
        this.straights = straights;
    }

    public Turns getTurns() {
        return turns;
    }

    public void setTurns(Turns turns) {
        this.turns = turns;
    }

    public Integer getTrackLength() {
        return trackLength;
    }

    public void setTrackLength(Integer trackLength) {
        this.trackLength = trackLength;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public class TrackVersion
    {


    }

}
