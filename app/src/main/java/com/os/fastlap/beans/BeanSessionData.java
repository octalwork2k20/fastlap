package com.os.fastlap.beans;

import java.util.ArrayList;

/**
 * Created by abhinava on 1/15/2018.
 */

public class BeanSessionData {
    String sessionId = "";
    String userId = "";
    String notes = "";
    String youtubeUrl = "";
    String weather = "";
    String sessionIndex = "";
    String vehicletyreModelId = "";
    String vehicletyreModelName = "";
    String vehicletyreBrandId = "";
    String vehicletyreBrandName = "";
    String lapTimeId = "";
    String trackVersionId = "";
    String trackVersionName = "";
    String trackId = "";
    String trackName = "";
    String userVehicleId = "";
    String userVehicletypeId = "";
    String userVehicletypeName = "";
    String userVehicleBrandId = "";
    String userVehicleModelId = "";
    String userVehicleModelName = "";
    String groupTypeId = "";
    String numberOfLapsFound = "";
    String dateOfRecording = "";
    String rentVehicle = "";
    ArrayList<PagerBean> sessionsImagesList = new ArrayList<>();
    ArrayList<Laps> lapList = new ArrayList<>();
    String videoImage = "";
    String channelTitle = "";
    String VideoTitle = "";
    String removeImages = "";

    public String getRemoveImages() {
        return removeImages;
    }

    public void setRemoveImages(String removeImages) {
        this.removeImages = removeImages;
    }


    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        VideoTitle = videoTitle;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getSessionIndex() {
        return sessionIndex;
    }

    public void setSessionIndex(String sessionIndex) {
        this.sessionIndex = sessionIndex;
    }

    public String getVehicletyreModelId() {
        return vehicletyreModelId;
    }

    public void setVehicletyreModelId(String vehicletyreModelId) {
        this.vehicletyreModelId = vehicletyreModelId;
    }

    public String getVehicletyreModelName() {
        return vehicletyreModelName;
    }

    public void setVehicletyreModelName(String vehicletyreModelName) {
        this.vehicletyreModelName = vehicletyreModelName;
    }

    public String getVehicletyreBrandId() {
        return vehicletyreBrandId;
    }

    public void setVehicletyreBrandId(String vehicletyreBrandId) {
        this.vehicletyreBrandId = vehicletyreBrandId;
    }

    public String getVehicletyreBrandName() {
        return vehicletyreBrandName;
    }

    public void setVehicletyreBrandName(String vehicletyreBrandName) {
        this.vehicletyreBrandName = vehicletyreBrandName;
    }

    public String getLapTimeId() {
        return lapTimeId;
    }

    public void setLapTimeId(String lapTimeId) {
        this.lapTimeId = lapTimeId;
    }

    public String getTrackVersionId() {
        return trackVersionId;
    }

    public void setTrackVersionId(String trackVersionId) {
        this.trackVersionId = trackVersionId;
    }

    public String getTrackVersionName() {
        return trackVersionName;
    }

    public void setTrackVersionName(String trackVersionName) {
        this.trackVersionName = trackVersionName;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getUserVehicleId() {
        return userVehicleId;
    }

    public void setUserVehicleId(String userVehicleId) {
        this.userVehicleId = userVehicleId;
    }

    public String getUserVehicletypeId() {
        return userVehicletypeId;
    }

    public void setUserVehicletypeId(String userVehicletypeId) {
        this.userVehicletypeId = userVehicletypeId;
    }

    public String getUserVehicletypeName() {
        return userVehicletypeName;
    }

    public void setUserVehicletypeName(String userVehicletypeName) {
        this.userVehicletypeName = userVehicletypeName;
    }

    public String getUserVehicleBrandId() {
        return userVehicleBrandId;
    }

    public void setUserVehicleBrandId(String userVehicleBrandId) {
        this.userVehicleBrandId = userVehicleBrandId;
    }

    public String getUserVehicleModelId() {
        return userVehicleModelId;
    }

    public void setUserVehicleModelId(String userVehicleModelId) {
        this.userVehicleModelId = userVehicleModelId;
    }

    public String getUserVehicleModelName() {
        return userVehicleModelName;
    }

    public void setUserVehicleModelName(String userVehicleModelName) {
        this.userVehicleModelName = userVehicleModelName;
    }

    public String getGroupTypeId() {
        return groupTypeId;
    }

    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }

    public String getNumberOfLapsFound() {
        return numberOfLapsFound;
    }

    public void setNumberOfLapsFound(String numberOfLapsFound) {
        this.numberOfLapsFound = numberOfLapsFound;
    }

    public String getDateOfRecording() {
        return dateOfRecording;
    }

    public void setDateOfRecording(String dateOfRecording) {
        this.dateOfRecording = dateOfRecording;
    }

    public String getRentVehicle() {
        return rentVehicle;
    }

    public void setRentVehicle(String rentVehicle) {
        this.rentVehicle = rentVehicle;
    }

    public ArrayList<PagerBean> getSessionsImagesList() {
        return sessionsImagesList;
    }

    public void setSessionsImagesList(ArrayList<PagerBean> sessionsImagesList) {
        this.sessionsImagesList = sessionsImagesList;
    }

    public ArrayList<Laps> getLapList()
    {
        return lapList;
    }

    public void setLapList(ArrayList<Laps> lapList)
    {
        this.lapList = lapList;
    }


    public class Laps {
        String lapsId = "";
        String lapsIndex = "";
        String avgSpeed = "";
        String maxSpeed = "";
        String time = "";
        String fileUrl = "";

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        String status = "";

        public String getLapsId() {
            return lapsId;
        }

        public void setLapsId(String lapsId) {
            this.lapsId = lapsId;
        }

        public String getLapsIndex() {
            return lapsIndex;
        }

        public void setLapsIndex(String lapsIndex) {
            this.lapsIndex = lapsIndex;
        }

        public String getAvgSpeed() {
            return avgSpeed;
        }

        public void setAvgSpeed(String avgSpeed) {
            this.avgSpeed = avgSpeed;
        }

        public String getMaxSpeed() {
            return maxSpeed;
        }

        public void setMaxSpeed(String maxSpeed) {
            this.maxSpeed = maxSpeed;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getFileUrl() {
            return fileUrl;
        }

        public void setFileUrl(String fileUrl) {
            this.fileUrl = fileUrl;
        }


    }

    public class SessionsImage {
        public String getImageId() {
            return imageId;
        }

        public void setImageId(String imageId) {
            this.imageId = imageId;
        }

        public String getFileUrl() {
            return fileUrl;
        }

        public void setFileUrl(String fileUrl) {
            this.fileUrl = fileUrl;
        }

        String imageId = "";
        String fileUrl = "";
    }

}
