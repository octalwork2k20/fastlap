package com.os.fastlap.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by abhinava on 8/21/2017.
 */

public class PaddockMyLapsParentModel implements Serializable {
    String userLapId;
    String track_id;
    String trackName="";
    String trackImage;
    String tracklocation;
    String trackLapUploaded;
    String trackBestPosition="0";
    String trackurl;
    String isFav = "";
    String isHome = "";
    String trackMobile="";
    String trackEmail="";
    String totalLapRecord="";
    String totalTurns="0";
    String trackLength="";
    String trackCountry="";
    List<BeanTimeLap> myLapsChildList;
    String trackVersionName="";

    String trackRxLat="0";
    String trackRxLng="0";
    String trackLxLat="0";
    String trackLxLng="0";
    String trackRouteImage="";
    String myLapCount="";
    String bestPosition="";
    String overlay_image="";

    public String getOverlay_image() {
        return overlay_image;
    }

    public void setOverlay_image(String overlay_image) {
        this.overlay_image = overlay_image;
    }

    public String getMyLapCount(){
        return myLapCount;
    }
    public void setMyLapCount(String myLapCount){
        this.myLapCount=myLapCount;
    }

    public String getBestPosition() {
        return bestPosition;
    }

    public void setBestPosition(String bestPosition) {
        this.bestPosition = bestPosition;
    }

    public String getTrackRxLat() {
        return trackRxLat;
    }

    public void setTrackRxLat(String trackRxLat) {
        this.trackRxLat = trackRxLat;
    }

    public String getTrackRxLng() {
        return trackRxLng;
    }

    public void setTrackRxLng(String trackRxLng) {
        this.trackRxLng = trackRxLng;
    }

    public String getTrackLxLat() {
        return trackLxLat;
    }

    public void setTrackLxLat(String trackLxLat) {
        this.trackLxLat = trackLxLat;
    }

    public String getTrackLxLng() {
        return trackLxLng;
    }

    public void setTrackLxLng(String trackLxLng) {
        this.trackLxLng = trackLxLng;
    }

    public String getTrackRouteImage() {
        return trackRouteImage;
    }

    public void setTrackRouteImage(String trackRouteImage) {
        this.trackRouteImage = trackRouteImage;
    }

    public String getTrackVersionName() {
        return trackVersionName;
    }

    public void setTrackVersionName(String trackVersionName) {
        this.trackVersionName = trackVersionName;
    }

    public String getTrackVersionId() {
        return trackVersionId;
    }

    public void setTrackVersionId(String trackVersionId) {
        this.trackVersionId = trackVersionId;
    }

    String trackVersionId;

    public String getTrackCountry() {
        return trackCountry;
    }

    public void setTrackCountry(String trackCountry) {
        this.trackCountry = trackCountry;
    }

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public String getIsHome() {
        return isHome;
    }

    public void setIsHome(String isHome) {
        this.isHome = isHome;
    }

    public String getTrackurl() {
        return trackurl;
    }

    public void setTrackurl(String trackurl) {
        this.trackurl = trackurl;
    }

    public String getTrackMobile() {
        return trackMobile;
    }

    public void setTrackMobile(String trackMobile) {
        this.trackMobile = trackMobile;
    }

    public String getTrackEmail() {
        return trackEmail;
    }

    public void setTrackEmail(String trackEmail) {
        this.trackEmail = trackEmail;
    }

    public String getTotalLapRecord() {
        return totalLapRecord;
    }

    public void setTotalLapRecord(String totalLapRecord) {
        this.totalLapRecord = totalLapRecord;
    }

    public String getTotalTurns() {
        return totalTurns;
    }

    public void setTotalTurns(String totalTurns) {
        this.totalTurns = totalTurns;
    }

    public String getTrackLength() {
        return trackLength;
    }

    public void setTrackLength(String trackLength) {
        this.trackLength = trackLength;
    }

    public String getUserLapId() {
        return userLapId;
    }

    public void setUserLapId(String userLapId) {
        this.userLapId = userLapId;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackImage() {
        return trackImage;
    }

    public void setTrackImage(String trackImage) {
        this.trackImage = trackImage;
    }

    public String getTracklocation() {
        return tracklocation;
    }

    public void setTracklocation(String tracklocation) {
        this.tracklocation = tracklocation;
    }

    public String getTrackLapUploaded() {
        return trackLapUploaded;
    }

    public void setTrackLapUploaded(String trackLapUploaded) {
        this.trackLapUploaded = trackLapUploaded;
    }

    public String getTrackBestPosition() {
        return trackBestPosition;
    }

    public void setTrackBestPosition(String trackBestPosition) {
        this.trackBestPosition = trackBestPosition;
    }

    public List<BeanTimeLap> getMyLapsChildList() {
        return myLapsChildList;
    }

    public void setMyLapsChildList(List<BeanTimeLap> myLapsChildList) {
        this.myLapsChildList = myLapsChildList;
    }

    @Override
    public String toString() {
        return "PaddockMyLapsParentModel{" +
                "userLapId='" + userLapId + '\'' +
                ", track_id='" + track_id + '\'' +
                ", trackName='" + trackName + '\'' +
                ", trackImage='" + trackImage + '\'' +
                ", tracklocation='" + tracklocation + '\'' +
                ", trackLapUploaded='" + trackLapUploaded + '\'' +
                ", trackBestPosition='" + trackBestPosition + '\'' +
                ", trackurl='" + trackurl + '\'' +
                ", isFav='" + isFav + '\'' +
                ", isHome='" + isHome + '\'' +
                ", trackMobile='" + trackMobile + '\'' +
                ", trackEmail='" + trackEmail + '\'' +
                ", totalLapRecord='" + totalLapRecord + '\'' +
                ", totalTurns='" + totalTurns + '\'' +
                ", trackLength='" + trackLength + '\'' +
                ", trackCountry='" + trackCountry + '\'' +
                ", myLapsChildList=" + myLapsChildList +
                ", trackVersionName='" + trackVersionName + '\'' +
                ", trackRxLat='" + trackRxLat + '\'' +
                ", trackRxLng='" + trackRxLng + '\'' +
                ", trackLxLat='" + trackLxLat + '\'' +
                ", trackLxLng='" + trackLxLng + '\'' +
                ", trackRouteImage='" + trackRouteImage + '\'' +
                ", trackVersionId='" + trackVersionId + '\'' +
                '}';
    }
}
