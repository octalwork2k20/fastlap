package com.os.fastlap.beans;

/**
 * Created by anandj on 8/25/2017.
 */

public class BlockUserListingBean {
    String _id;
    String userFriendId;
    String userId;
    String blockUserId__id;
    String lastName;
    String firstName;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserFriendId() {
        return userFriendId;
    }

    public void setUserFriendId(String userFriendId) {
        this.userFriendId = userFriendId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBlockUserId__id() {
        return blockUserId__id;
    }

    public void setBlockUserId__id(String blockUserId__id) {
        this.blockUserId__id = blockUserId__id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
