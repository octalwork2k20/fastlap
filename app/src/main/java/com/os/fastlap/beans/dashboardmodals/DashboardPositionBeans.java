package com.os.fastlap.beans.dashboardmodals;

/**
 * Created by anandj on 8/23/2017.
 */

public class DashboardPositionBeans {
    String lng;
    String lat;

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
