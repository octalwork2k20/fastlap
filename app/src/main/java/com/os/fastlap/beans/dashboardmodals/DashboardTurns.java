package com.os.fastlap.beans.dashboardmodals;

/**
 * Created by anandj on 8/23/2017.
 */

public class DashboardTurns {
    String slowRightTurns;
    String slowLeftTurns;
    String rightTurns;
    String leftTurns;
    String totalTurns;


    public String getSlowRightTurns() {
        return slowRightTurns;
    }

    public void setSlowRightTurns(String slowRightTurns) {
        this.slowRightTurns = slowRightTurns;
    }

    public String getSlowLeftTurns() {
        return slowLeftTurns;
    }

    public void setSlowLeftTurns(String slowLeftTurns) {
        this.slowLeftTurns = slowLeftTurns;
    }

    public String getRightTurns() {
        return rightTurns;
    }

    public void setRightTurns(String rightTurns) {
        this.rightTurns = rightTurns;
    }

    public String getLeftTurns() {
        return leftTurns;
    }

    public void setLeftTurns(String leftTurns) {
        this.leftTurns = leftTurns;
    }

    public String getTotalTurns() {
        return totalTurns;
    }

    public void setTotalTurns(String totalTurns) {
        this.totalTurns = totalTurns;
    }
}
