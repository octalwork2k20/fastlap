package com.os.fastlap.beans;

import java.util.ArrayList;

/**
 * Created by abhinava on 12/22/2017.
 */

public class SessionsBean
{
    String sessionId="";
    String lapCount="";
    ArrayList<BeanLapTime> lapArray=new ArrayList<>();

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getLapCount() {
        return lapCount;
    }

    public void setLapCount(String lapCount) {
        this.lapCount = lapCount;
    }

    public ArrayList<BeanLapTime> getLapArray() {
        return lapArray;
    }

    public void setLapArray(ArrayList<BeanLapTime> lapArray) {
        this.lapArray = lapArray;
    }
}
