package com.os.fastlap.beans;

import org.json.JSONArray;

/**
 * Created by abhinava on 9/7/2017.
 */

public class BeanAddGroup
{
    String vehicleTypeId="";
    String vehicleBrandId="";
    String trackId="";
    String groupName="";
    String groupDesc="";
    boolean showAllData;
    PagerBean profilePagerBean=new PagerBean();
    PagerBean coverPagerBean=new PagerBean();
    JSONArray tag_friend=new JSONArray();

    public boolean isShowAllData() {
        return showAllData;
    }

    public void setShowAllData(boolean showAllData) {
        this.showAllData = showAllData;
    }
    /*public boolean isShowAllData(boolean showAllData){
        return showAllData;
    }
    public void setShowAllData(boolean showAllData){

    }*/



    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    String privacy="";

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleBrandId() {
        return vehicleBrandId;
    }

    public void setVehicleBrandId(String vehicleBrandId) {
        this.vehicleBrandId = vehicleBrandId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public PagerBean getProfilePagerBean() {
        return profilePagerBean;
    }

    public void setProfilePagerBean(PagerBean profilePagerBean) {
        this.profilePagerBean = profilePagerBean;
    }

    public PagerBean getCoverPagerBean() {
        return coverPagerBean;
    }

    public void setCoverPagerBean(PagerBean coverPagerBean) {
        this.coverPagerBean = coverPagerBean;
    }

    public JSONArray getTag_friend() {
        return tag_friend;
    }

    public void setTag_friend(JSONArray tag_friend) {
        this.tag_friend = tag_friend;
    }

}
