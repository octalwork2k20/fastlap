package com.os.fastlap.beans;

import com.os.fastlap.beans.chatmodels.PersonalInfo;

/*
 * Created by anandj on 8/25/2017.
 */

public class ProfileMyFriendsList {
    private String id="";
    private String userId="";
    private PersonalInfo personalInfo;
    private String status="";
    private String email;

    public String getChannalId() {
        return channalId;
    }

    public void setChannalId(String channalId) {
        this.channalId = channalId;
    }

    public String getChannalType() {
        return channalType;
    }

    public void setChannalType(String channalType) {
        this.channalType = channalType;
    }

    public String getTimeBin() {
        return timeBin;
    }

    public void setTimeBin(String timeBin) {
        this.timeBin = timeBin;
    }

    private String channalId="";
    private String channalType="";
    private String timeBin="";

    public String getJoinedGroup() {
        return joinedGroup;
    }

    public void setJoinedGroup(String joinedGroup) {
        this.joinedGroup = joinedGroup;
    }

    public String getInvitedGroup() {
        return invitedGroup;
    }

    public void setInvitedGroup(String invitedGroup) {
        this.invitedGroup = invitedGroup;
    }

    private String joinedGroup="";
    private String invitedGroup="";

    public String getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(String isFriend) {
        this.isFriend = isFriend;
    }

    private String isFriend="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
