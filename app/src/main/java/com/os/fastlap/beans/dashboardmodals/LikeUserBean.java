package com.os.fastlap.beans.dashboardmodals;

/**
 * Created by anandj on 8/30/2017.
 */

public class LikeUserBean
{
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getLike_user_id() {
        return like_user_id;
    }

    public void setLike_user_id(String like_user_id) {
        this.like_user_id = like_user_id;
    }

    public String getLike_user_name() {
        return like_user_name;
    }

    public void setLike_user_name(String like_user_name) {
        this.like_user_name = like_user_name;
    }

    public String getLike_user_image() {
        return like_user_image;
    }

    public void setLike_user_image(String like_user_image) {
        this.like_user_image = like_user_image;
    }

    public String getLike_user_friend_status() {
        return like_user_friend_status;
    }

    public void setLike_user_friend_status(String like_user_friend_status) {
        this.like_user_friend_status = like_user_friend_status;
    }

    String _id="";
    String like_user_id="";
    String like_user_name="";
    String like_user_image="";
    String like_user_friend_status="";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String email="";
    String isAdmin="";
    String isSubAdmin="";

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    String groupId="";

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getIsSubAdmin() {
        return isSubAdmin;
    }

    public void setIsSubAdmin(String isSubAdmin) {
        this.isSubAdmin = isSubAdmin;
    }


}
