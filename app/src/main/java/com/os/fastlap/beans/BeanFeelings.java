package com.os.fastlap.beans;

/**
 * Created by abhinava on 8/25/2017.
 */

public class BeanFeelings
{
    String _id="";
    String unicodes="";
    String name="";

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUnicodes() {
        return unicodes;
    }

    public void setUnicodes(String unicodes) {
        this.unicodes = unicodes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
