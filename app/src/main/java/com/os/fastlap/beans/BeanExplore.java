package com.os.fastlap.beans;

/**
 * Created by abhinava on 9/20/2017.
 */

public class BeanExplore
{
    String id="";
    String vehicleTypeId="";
    String userId="";
    String userName="";
    String userImage="";

    String trackId="";
    String trackName="";
    String trackStatus="";
    String trackLatPos="";
    String trackLongPos="";
    String trackURL="";
    String trackMobile="";
    String trackEmail="";
    String aboutTrack="";
    String totalLapRecords="";
    String trackOpenedDate="";
    String totalTurns="";
    String trackLength="";
    String trackType="";
    String trackLocation="";
    String trackImage="";
    String trackCoverImage="";

    public String getTrackCountry() {
        return trackCountry;
    }

    public void setTrackCountry(String trackCountry) {
        this.trackCountry = trackCountry;
    }

    String trackCountry="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackStatus() {
        return trackStatus;
    }

    public void setTrackStatus(String trackStatus) {
        this.trackStatus = trackStatus;
    }

    public String getTrackLatPos() {
        return trackLatPos;
    }

    public void setTrackLatPos(String trackLatPos) {
        this.trackLatPos = trackLatPos;
    }

    public String getTrackLongPos() {
        return trackLongPos;
    }

    public void setTrackLongPos(String trackLongPos) {
        this.trackLongPos = trackLongPos;
    }

    public String getTrackURL() {
        return trackURL;
    }

    public void setTrackURL(String trackURL) {
        this.trackURL = trackURL;
    }

    public String getTrackMobile() {
        return trackMobile;
    }

    public void setTrackMobile(String trackMobile) {
        this.trackMobile = trackMobile;
    }

    public String getTrackEmail() {
        return trackEmail;
    }

    public void setTrackEmail(String trackEmail) {
        this.trackEmail = trackEmail;
    }

    public String getAboutTrack() {
        return aboutTrack;
    }

    public void setAboutTrack(String aboutTrack) {
        this.aboutTrack = aboutTrack;
    }

    public String getTotalLapRecords() {
        return totalLapRecords;
    }

    public void setTotalLapRecords(String totalLapRecords) {
        this.totalLapRecords = totalLapRecords;
    }

    public String getTrackOpenedDate() {
        return trackOpenedDate;
    }

    public void setTrackOpenedDate(String trackOpenedDate) {
        this.trackOpenedDate = trackOpenedDate;
    }

    public String getTotalTurns() {
        return totalTurns;
    }

    public void setTotalTurns(String totalTurns) {
        this.totalTurns = totalTurns;
    }

    public String getTrackLength() {
        return trackLength;
    }

    public void setTrackLength(String trackLength) {
        this.trackLength = trackLength;
    }

    public String getTrackType() {
        return trackType;
    }

    public void setTrackType(String trackType) {
        this.trackType = trackType;
    }

    public String getTrackLocation() {
        return trackLocation;
    }

    public void setTrackLocation(String trackLocation) {
        this.trackLocation = trackLocation;
    }

    public String getTrackImage() {
        return trackImage;
    }

    public void setTrackImage(String trackImage) {
        this.trackImage = trackImage;
    }

    public String getTrackCoverImage() {
        return trackCoverImage;
    }

    public void setTrackCoverImage(String trackCoverImage) {
        this.trackCoverImage = trackCoverImage;
    }


}
