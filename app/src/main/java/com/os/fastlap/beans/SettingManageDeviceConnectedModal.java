package com.os.fastlap.beans;

/**
 * Created by anandj on 8/22/2017.
 */

public class SettingManageDeviceConnectedModal {
    private String id;
    private String userId;
    private Integer v;
    private String status;
    private String deviceToken;
    private String deviceType;
    private String versioncode;
    private String brand;
    private String manufacture;
    private String mODEL;
    private String sERIAL;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getVersioncode() {
        return versioncode;
    }

    public void setVersioncode(String versioncode) {
        this.versioncode = versioncode;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public String getMODEL() {
        return mODEL;
    }

    public void setMODEL(String mODEL) {
        this.mODEL = mODEL;
    }

    public String getSERIAL() {
        return sERIAL;
    }

    public void setSERIAL(String sERIAL) {
        this.sERIAL = sERIAL;
    }

}
