package com.os.fastlap.beans;

/**
 * Created by abhinava on 11/9/2017.
 */

public class BeanForBestLapTime {

    private double time;
    private int lap;

    public BeanForBestLapTime(double time, int lap) {
        this.time = time;
        this.lap = lap;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }
}
