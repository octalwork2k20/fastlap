package com.os.fastlap.beans;

import com.os.fastlap.beans.chatmodels.PersonalInfo;

import java.io.Serializable;

/**
 * Created by anandj on 9/1/2017.
 */

public class UserDataBeans implements Serializable{
    private String _id;
    private String email;
    private String username;
    private String nickname;
    private PersonalInfo personalInfo;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }
}
