package com.os.fastlap.beans;

/**
 * Created by abhinava on 9/26/2017.
 */

public class BeanAdvertisement
{
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddName() {
        return addName;
    }

    public void setAddName(String addName) {
        this.addName = addName;
    }

    public String getAddDesc() {
        return addDesc;
    }

    public void setAddDesc(String addDesc) {
        this.addDesc = addDesc;
    }

    public String getAddImage() {
        return addImage;
    }

    public void setAddImage(String addImage) {
        this.addImage = addImage;
    }

    public String getAddURL() {
        return addURL;
    }

    public void setAddURL(String addURL) {
        this.addURL = addURL;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String id="";
    String addName="";
    String addDesc="";
    String addImage="";
    String addURL="";
    String expireDate="";
    String status="";

    String companyLogo="";
    String companyName="";
    String likeCount="";
    String commentCount="";
    String likedPost="";
    String sponsered="";

    public String getSponsered() {
        return sponsered;
    }

    public void setSponsered(String sponsered) {
        this.sponsered = sponsered;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getLikedPost() {
        return likedPost;
    }

    public void setLikedPost(String likedPost) {
        this.likedPost = likedPost;
    }



}
