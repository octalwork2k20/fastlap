
package com.os.fastlap.beans.dashboardmodals.tracklistmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DashboardTrackListingAll implements Serializable {

    @SerializedName("userTrackData")
    @Expose
    private UserTrackData userTrackData;
    @SerializedName("trackData")
    @Expose
    private TrackData trackData;
    private final static long serialVersionUID = 7487283687787410587L;

    public UserTrackData getUserTrackData() {
        return userTrackData;
    }

    public void setUserTrackData(UserTrackData userTrackData) {
        this.userTrackData = userTrackData;
    }

    public TrackData getTrackData() {
        return trackData;
    }

    public void setTrackData(TrackData trackData) {
        this.trackData = trackData;
    }

}
