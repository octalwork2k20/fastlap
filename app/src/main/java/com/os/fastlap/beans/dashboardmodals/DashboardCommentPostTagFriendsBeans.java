package com.os.fastlap.beans.dashboardmodals;

import java.io.Serializable;

/**
 * Created by anandj on 9/19/2017.
 */

public class DashboardCommentPostTagFriendsBeans implements Serializable {
    private String _id;
    private String userId;
    private String postId;
    private String status;
    private FriendIdBean friendIdBean;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FriendIdBean getFriendIdBean() {
        return friendIdBean;
    }

    public void setFriendIdBean(FriendIdBean friendIdBean) {
        this.friendIdBean = friendIdBean;
    }
}
