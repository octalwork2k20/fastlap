package com.os.fastlap.beans;

import java.util.List;

/**
 * Created by anandj on 10/5/2017.
 */

public class CartAlbumBeans {
    String _id;
    String userId;
    AlbumIdBeans albumIdBeans;
    List<CartAlbumImagesBeans> cartAlbumImagesBeanses;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public AlbumIdBeans getAlbumIdBeans() {
        return albumIdBeans;
    }

    public void setAlbumIdBeans(AlbumIdBeans albumIdBeans) {
        this.albumIdBeans = albumIdBeans;
    }

    public List<CartAlbumImagesBeans> getCartAlbumImagesBeanses() {
        return cartAlbumImagesBeanses;
    }

    public void setCartAlbumImagesBeanses(List<CartAlbumImagesBeans> cartAlbumImagesBeanses) {
        this.cartAlbumImagesBeanses = cartAlbumImagesBeanses;
    }
}
