package com.os.fastlap.beans.dashboardmodals;

import java.io.Serializable;

/**
 * Created by anandj on 8/30/2017.
 */

public class CommentBeans implements Serializable {
    String comment_id;
    String comment_text;
    String comment_date;
    String comment_like_count;
    String comment_reply_count;
    String post_id;
    String comment_user_id;
    String comment_user_name;
    String comment_user_image;

    public String getComment_like_status() {
        return comment_like_status;
    }

    public void setComment_like_status(String comment_like_status) {
        this.comment_like_status = comment_like_status;
    }

    String comment_like_status;

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public String getComment_date() {
        return comment_date;
    }

    public void setComment_date(String comment_date) {
        this.comment_date = comment_date;
    }

    public String getComment_like_count() {
        return comment_like_count;
    }

    public void setComment_like_count(String comment_like_count) {
        this.comment_like_count = comment_like_count;
    }

    public String getComment_reply_count() {
        return comment_reply_count;
    }

    public void setComment_reply_count(String comment_reply_count) {
        this.comment_reply_count = comment_reply_count;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getComment_user_id() {
        return comment_user_id;
    }

    public void setComment_user_id(String comment_user_id) {
        this.comment_user_id = comment_user_id;
    }

    public String getComment_user_name() {
        return comment_user_name;
    }

    public void setComment_user_name(String comment_user_name) {
        this.comment_user_name = comment_user_name;
    }

    public String getComment_user_image() {
        return comment_user_image;
    }

    public void setComment_user_image(String comment_user_image) {
        this.comment_user_image = comment_user_image;
    }




}
