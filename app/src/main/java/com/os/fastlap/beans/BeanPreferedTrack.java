package com.os.fastlap.beans;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abhinava on 9/21/2017.
 */

public class BeanPreferedTrack
{
    String trackId="";
    String trackName="";
    String preferredTrackId="";
    JSONObject filter_data;

    public ArrayList<BeanTrackUserData> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<BeanTrackUserData> userList) {
        this.userList = userList;
    }

    ArrayList<BeanTrackUserData> userList=new ArrayList<>();


    public void setFilter_data(JSONObject filter_data) {
        this.filter_data = filter_data;
    }

    public JSONObject getFilter_data() {
        return filter_data;
    }

    public String getPreferredTrackId() {
        return preferredTrackId;
    }

    public void setPreferredTrackId(String preferredTrackId) {
        this.preferredTrackId = preferredTrackId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public class BeanTrackUserData
    {
        String userId="";
        String userName="";
        String userTime="";

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserTime() {
            return userTime;
        }

        public void setUserTime(String userTime) {
            this.userTime = userTime;
        }

        public String getUserRank() {
            return userRank;
        }

        public void setUserRank(String userRank) {
            this.userRank = userRank;
        }

        public String getUserTimeLap() {
            return userTimeLap;
        }

        public void setUserTimeLap(String userTimeLap) {
            this.userTimeLap = userTimeLap;
        }

        String userRank="";
        String userTimeLap="";
    }
}
