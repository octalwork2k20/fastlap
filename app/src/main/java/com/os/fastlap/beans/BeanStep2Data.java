package com.os.fastlap.beans;

/**
 * Created by abhinava on 1/4/2018.
 */

public class BeanStep2Data {
    String sessionNo = "";
    String weather = "";
    String tyresBrandName = "";
    String tyresBrandId = "";
    String tyresModelName = "";
    String tyresModelId = "";
    String notesText = "";

    public String getSessionNo() {
        return sessionNo;
    }

    public void setSessionNo(String sessionNo) {
        this.sessionNo = sessionNo;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTyresBrandName() {
        return tyresBrandName;
    }

    public void setTyresBrandName(String tyresBrandName) {
        this.tyresBrandName = tyresBrandName;
    }

    public String getTyresBrandId() {
        return tyresBrandId;
    }

    public void setTyresBrandId(String tyresBrandId) {
        this.tyresBrandId = tyresBrandId;
    }

    public String getTyresModelName() {
        return tyresModelName;
    }

    public void setTyresModelName(String tyresModelName) {
        this.tyresModelName = tyresModelName;
    }

    public String getTyresModelId() {
        return tyresModelId;
    }

    public void setTyresModelId(String tyresModelId) {
        this.tyresModelId = tyresModelId;
    }

    public String getNotesText() {
        return notesText;
    }

    public void setNotesText(String notesText) {
        this.notesText = notesText;
    }


}
