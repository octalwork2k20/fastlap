package com.os.fastlap.beans.dashboardmodals;

/**
 * Created by anandj on 8/23/2017.
 */

public class DashboardContactDetails {
    String url;
    String mobileNumber;
    String email;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
