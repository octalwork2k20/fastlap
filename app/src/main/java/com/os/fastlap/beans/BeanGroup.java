package com.os.fastlap.beans;

/**
 * Created by abhinava on 9/11/2017.
 */

public class BeanGroup
{
    public String id="";
    public String name="";
    public String description="";
    public String image="";
    public String joinedCount="";
    String groupTypeId;

    public String getGroupOwnerId() {
        return groupOwnerId;
    }

    public void setGroupOwnerId(String groupOwnerId) {
        this.groupOwnerId = groupOwnerId;
    }

    String groupOwnerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJoinedCount() {
        return joinedCount;
    }

    public void setJoinedCount(String joinedCount) {
        this.joinedCount = joinedCount;
    }

    public String getAdduserStatus() {
        return adduserStatus;
    }

    public void setAdduserStatus(String adduserStatus) {
        this.adduserStatus = adduserStatus;
    }

    public String adduserStatus="";

    public String getGroupTypeId() {
        return groupTypeId;
    }

    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }
}
