package com.os.fastlap.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abhinava on 10/31/2017.
 */

public class BeanTrackVersion implements Serializable
{
    String trackId="";
    String trackName="";
    String trackCountry="";
    String trackLocation="";
    String trackCoverImage="";
    String trackImage="";

    ArrayList<Version> versionArrayList=new ArrayList<>();

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackCountry() {
        return trackCountry;
    }

    public void setTrackCountry(String trackCountry) {
        this.trackCountry = trackCountry;
    }

    public String getTrackLocation() {
        return trackLocation;
    }

    public void setTrackLocation(String trackLocation) {
        this.trackLocation = trackLocation;
    }

    public String getTrackCoverImage() {
        return trackCoverImage;
    }

    public void setTrackCoverImage(String trackCoverImage) {
        this.trackCoverImage = trackCoverImage;
    }

    public String getTrackImage() {
        return trackImage;
    }

    public void setTrackImage(String trackImage) {
        this.trackImage = trackImage;
    }

    public ArrayList<Version> getVersionArrayList() {
        return versionArrayList;
    }

    public void setVersionArrayList(ArrayList<Version> versionArrayList) {
        this.versionArrayList = versionArrayList;
    }



    public class Version implements Serializable
    {
        String versionId="";
        String versionName="";
        String created="";
        String startLeftLat="";
        String startLeftLong="";
        String startRightLat="";
        String startRightLong="";

        public String getVersionId() {
            return versionId;
        }

        public void setVersionId(String versionId) {
            this.versionId = versionId;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getStartLeftLat() {
            return startLeftLat;
        }

        public void setStartLeftLat(String startLeftLat) {
            this.startLeftLat = startLeftLat;
        }

        public String getStartLeftLong() {
            return startLeftLong;
        }

        public void setStartLeftLong(String startLeftLong) {
            this.startLeftLong = startLeftLong;
        }

        public String getStartRightLat() {
            return startRightLat;
        }

        public void setStartRightLat(String startRightLat) {
            this.startRightLat = startRightLat;
        }

        public String getStartRightLong() {
            return startRightLong;
        }

        public void setStartRightLong(String startRightLong) {
            this.startRightLong = startRightLong;
        }


    }
}
