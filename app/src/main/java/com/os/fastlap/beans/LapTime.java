package com.os.fastlap.beans;

import java.io.Serializable;

public class LapTime implements Serializable {
    String title="";
    String lap_time="";
    String lap_speed="";
    String lap_vehicle="";
    String date="";
    String id="";




    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setLap_speed(String lap_speed) {
        this.lap_speed = lap_speed;
    }

    public void setLap_time(String lap_time) {
        this.lap_time = lap_time;
    }

    public void setLap_vehicle(String lap_vehicle) {
        this.lap_vehicle = lap_vehicle;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLap_speed() {
        return lap_speed;
    }

    public String getLap_time() {
        return lap_time;
    }

    public String getLap_vehicle() {
        return lap_vehicle;
    }

    public String getTitle() {
        return title;
    }
}
