package com.os.fastlap.beans.dashboardmodals;

import com.os.fastlap.beans.UserDataBeans;

import java.io.Serializable;

/**
 * Created by anandj on 11/15/2017.
 */

public class DashboardPostShareUserModal implements Serializable {

    private String id;
    private String postId;
    private String modified;
    private String created;
    private String status;
    private UserDataBeans userDataBeans;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserDataBeans getUserDataBeans() {
        return userDataBeans;
    }

    public void setUserDataBeans(UserDataBeans userDataBeans) {
        this.userDataBeans = userDataBeans;
    }


    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
