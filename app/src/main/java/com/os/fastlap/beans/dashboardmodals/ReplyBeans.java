package com.os.fastlap.beans.dashboardmodals;

/**
 * Created by anandj on 8/31/2017.
 */

public class ReplyBeans
{
    String replyId="";
    String replyDateTime="";
    String replyText="";
    String replyStatus="";
    String replyCountLikeData="";
    String replyLikedCommentReply="";

    String replyUserId="";
    String replyUserEmail="";
    String replyUseruserName="";
    String replyUserImage="";
    String replyUserName="";

    String postId="";

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostCommentId() {
        return postCommentId;
    }

    public void setPostCommentId(String postCommentId) {
        this.postCommentId = postCommentId;
    }

    String postCommentId="";

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getReplyDateTime() {
        return replyDateTime;
    }

    public void setReplyDateTime(String replyDateTime) {
        this.replyDateTime = replyDateTime;
    }

    public String getReplyText() {
        return replyText;
    }

    public void setReplyText(String replyText) {
        this.replyText = replyText;
    }

    public String getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(String replyStatus) {
        this.replyStatus = replyStatus;
    }

    public String getReplyCountLikeData() {
        return replyCountLikeData;
    }

    public void setReplyCountLikeData(String replyCountLikeData) {
        this.replyCountLikeData = replyCountLikeData;
    }

    public String getReplyLikedCommentReply() {
        return replyLikedCommentReply;
    }

    public void setReplyLikedCommentReply(String replyLikedCommentReply) {
        this.replyLikedCommentReply = replyLikedCommentReply;
    }

    public String getReplyUserId() {
        return replyUserId;
    }

    public void setReplyUserId(String replyUserId) {
        this.replyUserId = replyUserId;
    }

    public String getReplyUserEmail() {
        return replyUserEmail;
    }

    public void setReplyUserEmail(String replyUserEmail) {
        this.replyUserEmail = replyUserEmail;
    }

    public String getReplyUseruserName() {
        return replyUseruserName;
    }

    public void setReplyUseruserName(String replyUseruserName) {
        this.replyUseruserName = replyUseruserName;
    }

    public String getReplyUserImage() {
        return replyUserImage;
    }

    public void setReplyUserImage(String replyUserImage) {
        this.replyUserImage = replyUserImage;
    }

    public String getReplyUserName() {
        return replyUserName;
    }

    public void setReplyUserName(String replyUserName) {
        this.replyUserName = replyUserName;
    }


}
