package com.os.fastlap.beans;

import com.google.android.gms.maps.model.Circle;

/**
 * Created by abhinava on 12/27/2017.
 */

public class BeanRunElement {
    Circle circle;
    Runnable runnable;
    int runningPosition = 0;

    public int getRunSpeed() {
        return runSpeed;
    }

    public void setRunSpeed(int runSpeed) {
        this.runSpeed = runSpeed;
    }

    int runSpeed;

    public int getOriginalSpeed() {
        return originalSpeed;
    }

    public void setOriginalSpeed(int originalSpeed) {
        this.originalSpeed = originalSpeed;
    }

    int originalSpeed;

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    public void setRunnable(Runnable runnable) {
        this.runnable = runnable;
    }

    public int getRunningPosition() {
        return runningPosition;
    }

    public void setRunningPosition(int runningPosition) {
        this.runningPosition = runningPosition;
    }


}
