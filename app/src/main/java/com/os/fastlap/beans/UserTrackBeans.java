package com.os.fastlap.beans;

/**
 * Created by anandj on 8/23/2017.
 */

public class UserTrackBeans
{
    String user_track_id;
    String track_id;
    String image;

    public String getUser_track_id() {
        return user_track_id;
    }

    public void setUser_track_id(String user_track_id) {
        this.user_track_id = user_track_id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



}
