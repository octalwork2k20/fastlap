package com.os.fastlap.beans.clothmodals;

/**
 * Created by anandj on 8/11/2017.
 */

public class ClothModelIdModal {
    String _id;
    String clothNameId;
    String name;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getClothNameId() {
        return clothNameId;
    }

    public void setClothNameId(String clothNameId) {
        this.clothNameId = clothNameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
