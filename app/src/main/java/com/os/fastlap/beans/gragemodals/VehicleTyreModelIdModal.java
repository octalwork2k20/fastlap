package com.os.fastlap.beans.gragemodals;

import java.io.Serializable;

/**
 * Created by anandj on 8/10/2017.
 */

public class VehicleTyreModelIdModal  implements Serializable {
    String _id;
    String  vehicleTypeId;
    String vehichlemodelName;
    String vehicleTyreBrandId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehichlemodelName() {
        return vehichlemodelName;
    }

    public void setVehichlemodelName(String vehichlemodelName) {
        this.vehichlemodelName = vehichlemodelName;
    }

    public String getVehichleModelName() {
        return vehichlemodelName;
    }


    public String getVehicleTyreBrandId() {
        return vehicleTyreBrandId;
    }

    public void setVehicleTyreBrandId(String vehicleTyreBrandId) {
        this.vehicleTyreBrandId = vehicleTyreBrandId;
    }
}
