
package com.os.fastlap.beans.dashboardmodals.tracklistmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContactDetails implements Serializable
{

    @SerializedName("url")
    @Expose
    private String url="";
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber="";
    @SerializedName("email")
    @Expose
    private String email="";
    private final static long serialVersionUID = 1582277321126045207L;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
