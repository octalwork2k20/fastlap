package com.os.fastlap.beans.gragemodals;

import java.io.Serializable;

/**
 * Created by anandj on 8/10/2017.
 */

public class VehicleVersionIdModal  implements Serializable {
    String _id;
    String vehicleVersionName;
    String vehicleModelId;
    String vehicleBrandId;
    String vehicleTypeId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getVehicleVersionName() {
        return vehicleVersionName;
    }

    public void setVehicleVersionName(String vehicleVersionName) {
        this.vehicleVersionName = vehicleVersionName;
    }

    public String getVehicleModelId() {
        return vehicleModelId;
    }

    public void setVehicleModelId(String vehicleModelId) {
        this.vehicleModelId = vehicleModelId;
    }

    public String getVehicleBrandId() {
        return vehicleBrandId;
    }

    public void setVehicleBrandId(String vehicleBrandId) {
        this.vehicleBrandId = vehicleBrandId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }
}
