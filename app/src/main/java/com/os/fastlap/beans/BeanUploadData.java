package com.os.fastlap.beans;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abhinava on 10/16/2017.
 */

public class BeanUploadData implements Serializable
{
    String filePath="";
    String gpsDevice="";
    String softwareVersion="";
    String numberofLapsFound="0";
    String trackName="";
    String trackId="";
    String trackImage="";
    String trackLocation="";
    String trackVersion="";
    String trackVersionId="";
    String dateofRecording="";
    String vehicleId="";
    String vehicleName="";
    String vehicleTypeId="";

    String lapWeather="";
    String lapTyreBrandId="";
    String lapTyreModelId="";
    String lapNotes="";
    String lapVideoURL="";
    JSONArray selectedValue=new JSONArray();
    String lapTime="";
    String groupTypeId="";

    String rentVehicle="";
    String rentVehicleModelId="";
    String rentVehicleBrandId="";
    String rentVehicleTypeId="";

    String vehicle_name="";
    String vehicleBrandID="";
    String vehicleBrandName="";
    String rentedVehicleBrandName="";
    String vehicleModelId="";
    String vehicleModelName="";
    String rentedVehicleModelName="";

    public String getRentVehicleBrandId() {
        return rentVehicleBrandId;
    }

    public void setRentVehicleBrandId(String rentVehicleBrandId) {
        this.rentVehicleBrandId = rentVehicleBrandId;
    }

    public String getRentVehicleTypeId() {
        return rentVehicleTypeId;
    }

    public void setRentedVehicleBrandName(String rentedVehicleBrandName) {
        this.rentedVehicleBrandName = rentedVehicleBrandName;
    }

    public String getRentedVehicleBrandName() {
        return rentedVehicleBrandName;
    }

    public void setRentedVehicleModelName(String rentedVehicleModelName) {
        this.rentedVehicleModelName = rentedVehicleModelName;
    }

    public String getRentedVehicleModelName() {
        return rentedVehicleModelName;
    }

    public void setRentVehicleTypeId(String rentVehicleTypeId) {
        this.rentVehicleTypeId = rentVehicleTypeId;
    }

    public String getRentVehicleModelId() {
        return rentVehicleModelId;
    }

    public void setRentVehicleModelId(String rentVehicleModelId) {
        this.rentVehicleModelId = rentVehicleModelId;
    }

    public String getGroupTypeId() {
        return groupTypeId;
    }

    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }

    public JSONArray getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(JSONArray selectedValue) {
        this.selectedValue = selectedValue;
    }



    public String getVehicleBrandName() {
        return vehicleBrandName;
    }

    public void setVehicleBrandName(String vehicleBrandName) {
        this.vehicleBrandName = vehicleBrandName;
    }

    public String getVehicleModelName() {
        return vehicleModelName;
    }

    public void setVehicleModelName(String vehicleModelName) {
        this.vehicleModelName = vehicleModelName;
    }
    public void setRentVehicle(String rentVehicle) {
        this.rentVehicle = rentVehicle;
    }

    public String getRentVehicle() {
        return rentVehicle;
    }

    public String getLapTime() {
        return lapTime;
    }

    public void setLapTime(String lapTime) {
        this.lapTime = lapTime;
    }



    public ArrayList<PagerBean> getImageArrayList() {
        return imageArrayList;
    }

    public void setImageArrayList(ArrayList<PagerBean> imageArrayList) {
        this.imageArrayList = imageArrayList;
    }

    private ArrayList<PagerBean> imageArrayList=new ArrayList<>();

    public String getLapWeather() {
        return lapWeather;
    }

    public void setLapWeather(String lapWeather) {
        this.lapWeather = lapWeather;
    }

    public String getLapTyreBrandId() {
        return lapTyreBrandId;
    }

    public void setLapTyreBrandId(String lapTyreBrandId) {
        this.lapTyreBrandId = lapTyreBrandId;
    }

    public String getLapTyreModelId() {
        return lapTyreModelId;
    }

    public void setLapTyreModelId(String lapTyreModelId) {
        this.lapTyreModelId = lapTyreModelId;
    }

    public String getLapNotes() {
        return lapNotes;
    }

    public void setLapNotes(String lapNotes) {
        this.lapNotes = lapNotes;
    }

    public String getLapVideoURL() {
        return lapVideoURL;
    }

    public void setLapVideoURL(String lapVideoURL) {
        this.lapVideoURL = lapVideoURL;
    }



    public String getTrackVersionId() {
        return trackVersionId;
    }

    public void setTrackVersionId(String trackVersionId) {
        this.trackVersionId = trackVersionId;
    }



    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }




    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getGpsDevice() {
        return gpsDevice;
    }

    public void setGpsDevice(String gpsDevice) {
        this.gpsDevice = gpsDevice;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public String getNumberofLapsFound() {
        return numberofLapsFound;
    }

    public void setNumberofLapsFound(String numberofLapsFound) {
        this.numberofLapsFound = numberofLapsFound;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackImage() {
        return trackImage;
    }

    public void setTrackImage(String trackImage) {
        this.trackImage = trackImage;
    }

    public String getTrackLocation() {
        return trackLocation;
    }

    public void setTrackLocation(String trackLocation) {
        this.trackLocation = trackLocation;
    }

    public String getTrackVersion() {
        return trackVersion;
    }

    public void setTrackVersion(String trackVersion) {
        this.trackVersion = trackVersion;
    }

    public String getDateofRecording() {
        return dateofRecording;
    }

    public void setDateofRecording(String dateofRecording) {
        this.dateofRecording = dateofRecording;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }


}
