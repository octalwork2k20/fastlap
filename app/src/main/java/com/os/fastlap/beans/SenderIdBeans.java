package com.os.fastlap.beans;

import com.os.fastlap.beans.chatmodels.PersonalInfo;

/**
 * Created by anandj on 11/6/2017.
 */

public class SenderIdBeans {
    String _id;
    String username;
    String email;
    PersonalInfo personalInfo;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }
}
