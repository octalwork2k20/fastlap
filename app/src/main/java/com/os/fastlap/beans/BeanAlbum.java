package com.os.fastlap.beans;

import com.os.fastlap.beans.dashboardmodals.DashboardPostShareUserModal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhinava on 9/19/2017
 */

public class BeanAlbum implements Serializable {
    private String _id;
    private String userId="";
    private String userName="";
    private String userImage="";
    private String trackId="";
    private String trackName="";
    private String eventId="";
    private String eventName="";
    private String created="";
    private String status="";
    private String dateOfEvent="";
    private String name="";
    private int mediaType;
    private String likedPost="";
    private String likedPostCount="";
    private String commentPostCount="";
    private String description="";
    private ArrayList<BeanFriend> friendList = new ArrayList<>();
    private ArrayList<BeanFiles> filesList = new ArrayList<>();
    private ArrayList<PhotoMOdel> photoList = new ArrayList<>();
    private List<DashboardPostShareUserModal> albumPostShareUserModals;
    private String groupTypeId="";
    private String groupTypeName="";
    String presetName="";
    String presetImage="";
    String presetOpacity="";

    public ArrayList<PhotoMOdel> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(ArrayList<PhotoMOdel> photoList) {
        this.photoList = photoList;
    }
    public String getPresetImage() {
        return presetImage;
    }

    public String getPresetName() {
        return presetName;
    }

    public String getPresetOpacity() {
        return presetOpacity;
    }

    public void setPresetName(String presetName) {
        this.presetName = presetName;
    }

    public void setPresetOpacity(String presetOpacity) {
        this.presetOpacity = presetOpacity;
    }

    public void setPresetImage(String presetImage) {
        this.presetImage = presetImage;
    }



    public String getAlbumPrice() {
        return albumPrice;
    }

    public void setAlbumPrice(String albumPrice) {
        this.albumPrice = albumPrice;
    }

    private String albumPrice="";

    public String getGroupTypeId() {
        return groupTypeId;
    }

    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }

    public String getGroupTypeName() {
        return groupTypeName;
    }

    public void setGroupTypeName(String groupTypeName) {
        this.groupTypeName = groupTypeName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }


    public String getLikedPost() {
        return likedPost;
    }

    public void setLikedPost(String likedPost) {
        this.likedPost = likedPost;
    }

    public String getLikedPostCount() {
        return likedPostCount;
    }

    public void setLikedPostCount(String likedPostCount) {
        this.likedPostCount = likedPostCount;
    }

    public String getCommentPostCount() {
        return commentPostCount;
    }

    public void setCommentPostCount(String commentPostCount) {
        this.commentPostCount = commentPostCount;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getEventId() {
        if (eventId != null)
            return eventId;
        return "";
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateOfEvent() {
        if (dateOfEvent != null && !dateOfEvent.equalsIgnoreCase("null"))
            return dateOfEvent;
        return "";
    }

    public void setDateOfEvent(String dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<BeanFriend> getFriendList() {
        return friendList;
    }

    public void setFriendList(ArrayList<BeanFriend> friendList) {
        this.friendList = friendList;
    }

    public ArrayList<BeanFiles> getFilesList() {
        return filesList;
    }

    public void setFilesList(ArrayList<BeanFiles> filesList) {
        this.filesList = filesList;
    }

    public List<DashboardPostShareUserModal> getAlbumPostShareUserModals() {
        return albumPostShareUserModals;
    }

    public void setAlbumPostShareUserModals(List<DashboardPostShareUserModal> dashboardPostShareUserModals) {
        this.albumPostShareUserModals = dashboardPostShareUserModals;
    }

    public class BeanFriend implements Serializable {
        String userFriendId = "";
        String friendId = "";
        String friendName = "";
        String friendImage = "";

        public String getUserFriendId() {
            return userFriendId;
        }

        public void setUserFriendId(String userFriendId) {
            this.userFriendId = userFriendId;
        }

        public String getFriendId() {
            return friendId;
        }

        public void setFriendId(String friendId) {
            this.friendId = friendId;
        }

        public String getFriendName() {
            return friendName;
        }

        public void setFriendName(String friendName) {
            this.friendName = friendName;
        }

        public String getFriendImage() {
            return friendImage;
        }

        public void setFriendImage(String friendImage) {
            this.friendImage = friendImage;
        }


    }

    public class BeanFiles implements Serializable {
        String fileId = "";
        String created = "";
        String type = "";
        String thumbName = "";
        String fileName = "";
        boolean isSelected = false;

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public String getFileId() {
            return fileId;
        }

        public void setFileId(String fileId) {
            this.fileId = fileId;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getThumbName() {
            return thumbName;
        }

        public void setThumbName(String thumbName) {
            this.thumbName = thumbName;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }


    }
    public class pricesPresetsModel implements Serializable {
        String status;
        String _id;
        String name;

        public String getName() {
            return name;
        }

        public String get_id() {
            return _id;
        }

        public String getStatus() {
            return status;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public void set_id(String _id) {
            this._id = _id;
        }
    }

    public class PhotoMOdel implements Serializable{
        String _id;
        String numberOfPhotos;
        String price;

        public String get_id() {
            return _id;
        }

        public String getNumberOfPhotos() {
            return numberOfPhotos;
        }

        public String getPrice() {
            return price;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public void setNumberOfPhotos(String numberOfPhotos) {
            this.numberOfPhotos = numberOfPhotos;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }

}
