/*
package com.os.fastlap.libs;

import android.content.Context;
import android.util.Log;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.highlight.Highlight;
import com.os.fastlap.R;
import com.os.fastlap.activity.mylaps.MyLapsRaceActivity;
import com.os.fastlap.beans.BeanLocation;
import com.os.fastlap.util.customclass.TextViewPlayRegular;

import java.util.ArrayList;

public class CustomMarkerView extends MarkerView implements MyLapsRaceActivity.CheckboxClickEvent {

    public  TextViewPlayRegular tvTime, tvSpeed,tvName,tvDistance;
    public  static TextViewPlayRegular tvX;
    public  static  TextViewPlayRegular tvY;
    public  static  TextViewPlayRegular tvZ;
    LineData data;
    ArrayList<BeanLocation> lapArray;
    MyLapsRaceActivity.CheckboxClickEvent checkboxClickEvent;
    private int pos;

    public CustomMarkerView(Context context, int layoutResource, LineData data, ArrayList<BeanLocation> lapArray) {
        super(context, layoutResource);
        this.data = data;
        this.lapArray=lapArray;
        ((MyLapsRaceActivity)context).checkboxClickEvent=checkboxClickEvent;
        // this markerview only displays a     textview
        //dotTv = (TextView) findViewById(R.id.dotTv);
        tvTime = (TextViewPlayRegular) findViewById(R.id.tvTime);
        tvSpeed = (TextViewPlayRegular) findViewById(R.id.tvSpeed);
        tvName = (TextViewPlayRegular) findViewById(R.id.tvName);
        tvDistance = (TextViewPlayRegular) findViewById(R.id.tvDistance);
        tvX = (TextViewPlayRegular) findViewById(R.id.tvX);
        tvY = (TextViewPlayRegular) findViewById(R.id.tvY);
        tvZ = (TextViewPlayRegular) findViewById(R.id.tvZ);
    }
    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        pos=e.getXIndex();
        Log.e("tag_octal","username 1"+data.getDataSetForEntry(e).getLabel());
        tvName.setText(data.getDataSetForEntry(e).getLabel());
        tvSpeed.setText(getResources().getString(R.string.string_speed_colon)+lapArray.get(e.getXIndex()).getSpeed());
        tvDistance.setText(getResources().getString(R.string.string_distance_colon)+lapArray.get(e.getXIndex()).getDist());
        tvTime.setText(getResources().getString(R.string.string_time_colon)+lapArray.get(e.getXIndex()).getTime());
        tvX.setText(getResources().getString(R.string.string_accelX_colon)+lapArray.get(e.getXIndex()).getAccel_x());
        tvY.setText(getResources().getString(R.string.string_accelY_colon)+lapArray.get(e.getXIndex()).getAccel_y());
        tvZ.setText(getResources().getString(R.string.string_accelZ_colon)+lapArray.get(e.getXIndex()).getAccel_z());
        // dotTv.setTextColor(data.getDataSets().get(highlight.getDataSetIndex()).getColor());
    }

    @Override
    public int getXOffset(float xpos) {
        // this will center the marker-view horizontally
        return -(getWidth() / 2);
    }

    @Override
    public int getYOffset(float ypos) {
        return -getHeight();
    }
    @Override
    public void onCheckedForTvX(boolean isChecked) {
        if(isChecked){
            tvX.setVisibility(VISIBLE);
        }
        else {
            tvX.setVisibility(GONE);
        }
        Log.e("tag_octal","for X"+isChecked);
    }

    @Override
    public void onCheckedForTvY(boolean isChecked) {
        Log.e("tag_octal","for Y"+isChecked);
        if(isChecked){
            tvY.setVisibility(VISIBLE);
        }
        else {
            tvY.setVisibility(GONE);
        }
    }

    @Override
    public void onCheckedForTvZ(boolean isChecked) {
        Log.e("tag_octal","for Z"+isChecked);
        if(isChecked){
            tvZ.setVisibility(VISIBLE);
        }
        else {
            tvZ.setVisibility(GONE);
        }
    }
}
*/
