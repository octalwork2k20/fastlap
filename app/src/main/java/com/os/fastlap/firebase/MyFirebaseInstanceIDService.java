package com.os.fastlap.firebase;

/**
 * Created by abhinava on 9/12/2016.
 */

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;

//Class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        sendRegistrationToServer(refreshedToken);

       // MySharedPreferences.setDevice_ID(getApplicationContext(),refreshedToken);

        //Displaying token on logcat
        Log.e("Device Token", "Refreshed token: " + refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        MySharedPreferences.setPreferences(getApplicationContext(),token, S.user_device_token);
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}