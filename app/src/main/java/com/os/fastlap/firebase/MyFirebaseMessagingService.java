package com.os.fastlap.firebase;

/*
 * Created by abhinava on 9/12/2016
 */

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.os.fastlap.R;
import com.os.fastlap.activity.AlbumCommentActivity;
import com.os.fastlap.activity.ChatActivity;
import com.os.fastlap.activity.ChatListActivity;
import com.os.fastlap.activity.DashboardActivity;
import com.os.fastlap.activity.SettingsActivity;
import com.os.fastlap.activity.dashboard.CommentActivity;
import com.os.fastlap.activity.dashboard.ReplyActivity;
import com.os.fastlap.activity.profile.FriendsProfileActivity;
import com.os.fastlap.beans.BeanMessage;
import com.os.fastlap.constant.FastLapApplication;
import com.os.fastlap.data.database.DatabaseManager;
import com.os.fastlap.util.MySharedPreferences;
import com.os.fastlap.util.constants.S;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    public static final String BROADCAST_FOR_SUBCRIPTION_PLAN = "com.os.fastlap.activity.SettingsActivity";
    public static final String ANDROID_CHANNEL_ID = "com.os.fastlap.ANDROID";
    public static final String ANDROID_CHANNEL_NAME = "ANDROID CHANNEL";
    int id = (int) (System.currentTimeMillis() * (int) (Math.random() * 100));
    DatabaseManager databaseManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        //  Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage);
        checkType(remoteMessage.getData().get(S.type), remoteMessage);
        //   pushNotification(remoteMessage.getData().get("info"), remoteMessage.getData().get("title"), remoteMessage.getData().get("type"));
    }

    private void checkType(String notification_type, RemoteMessage remoteMessage) {
        switch (notification_type) {

            case "USER_SUBSCRIPTION":
                MySharedPreferences.setBooleanPreferences(this,true,S.checkSubscription);
                pushNotificationSendAndAcceptRequest(remoteMessage);
                break;
            case "FriendRequestSent":
            case "AcceptRequest":
                pushNotificationSendAndAcceptRequest(remoteMessage);
                break;
            case "PostLike":
                pushNotificationPostComment(remoteMessage);
                break;
            case "PostComment":
                pushNotificationPostComment(remoteMessage);
                break;
            case "PostCommentReply":
                postCommentReply(remoteMessage);
                break;
            case "PostCommentLike":
                pushNotificationPostComment(remoteMessage);
                break;
            case "AlbumLike":
                pushNotificationAlbumComment(remoteMessage);
                break;
            case "AlbumComment":
                pushNotificationAlbumComment(remoteMessage);
                break;
            default:
                pushNotification(remoteMessage.getData().get(S.info), remoteMessage.getData().get(S.title), remoteMessage.getData().get(S.type));
        }
    }



    private void pushNotificationSendAndAcceptRequest(RemoteMessage remoteMessage) {
        try {
            Intent notificationIntent=null;
            if(remoteMessage.getData().get(S.type).equalsIgnoreCase("USER_SUBSCRIPTION")){
                if(getCuurentTopActivity().equalsIgnoreCase("com.os.fastlap.activity.SettingsActivity")){
                    notificationIntent=new Intent(BROADCAST_FOR_SUBCRIPTION_PLAN);
                    notificationIntent.putExtra("Notification_type",remoteMessage.getData().get(S.type));
                    sendBroadcast(notificationIntent);
                }
                else if(getCuurentTopActivity().equalsIgnoreCase("com.os.fastlap.activity.payment.AddCardDetail")){
                    notificationIntent=new Intent();
                }
                else {
                    notificationIntent = new Intent(this, SettingsActivity.class);
                    notificationIntent.putExtra("Notification_type",remoteMessage.getData().get(S.type));
                }
            }
            else {
                notificationIntent = new Intent(this, FriendsProfileActivity.class);
                Bundle notification_bundle = new Bundle();
                notificationIntent.putExtras(notification_bundle);
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, notificationIntent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = null;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);

            } else {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);
            }

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (notificationManager != null) {
                notificationManager.notify(id  /*ID of notification */, notificationBuilder.build());
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void postCommentReply(RemoteMessage remoteMessage) {
        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData().get(S.info));
            String post_id = jsonObject.getString(S.postId);
            String userPostCommentId = jsonObject.getString(S.userPostCommentId);

            Intent notificationIntent = new Intent(this, ReplyActivity.class);
            Bundle notification_bundle = new Bundle();
            notification_bundle.putString(S.postId, post_id);
            notification_bundle.putString(S.userPostCommentId, userPostCommentId);
            notificationIntent.putExtras(notification_bundle);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, notificationIntent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = null;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);

            } else {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);
            }

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (notificationManager != null) {
                notificationManager.notify(id  /*ID of notification */, notificationBuilder.build());
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void pushNotificationPostComment(RemoteMessage remoteMessage) {
        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData().get(S.info));
            String post_id = jsonObject.getString(S.postId);

            Intent notificationIntent = new Intent(this, CommentActivity.class);
            Bundle notification_bundle = new Bundle();
            notification_bundle.putString(S.postId, post_id);
            notification_bundle.putString(S.position, "0");
            notificationIntent.putExtras(notification_bundle);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, notificationIntent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = null;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);

            } else {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);
            }

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (notificationManager != null) {
                notificationManager.notify(id  /*ID of notification */, notificationBuilder.build());
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private void pushNotificationAlbumComment(RemoteMessage remoteMessage) {
        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData().get(S.info));
            String post_id = jsonObject.getString(S.albumId);

            Intent notificationIntent = new Intent(this, AlbumCommentActivity.class);
            Bundle notification_bundle = new Bundle();
            notification_bundle.putString(S.postId, post_id);
            notification_bundle.putString(S.position, "0");
            notificationIntent.putExtras(notification_bundle);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, notificationIntent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = null;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);

            } else {
                notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setSmallIcon(R.drawable.noti_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                        .setContentIntent(pendingIntent);
            }

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (notificationManager != null) {
                notificationManager.notify(id  /*ID of notification */, notificationBuilder.build());
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private void pushNotificationLike(RemoteMessage remoteMessage) {
        Intent notificationIntent = new Intent(this, DashboardActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.noti_icon)
                .setContentTitle(remoteMessage.getData().get(S.title))
                .setContentText(Html.fromHtml(remoteMessage.getData().get(S.message)))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(remoteMessage.getData().get(S.message))))
                .setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
        }
    }

    private void pushNotification(String message, String notification_title, String notification_type) {
        databaseManager = DatabaseManager.getInstance(this);
        String notificationTitle = "";
        try {

            JSONObject jsonObject = new JSONObject(message);

            if (notification_type.equalsIgnoreCase(S.chat_message)) {

                JSONObject jsonInfo = jsonObject;
                try {
                    String roomId = jsonInfo.getString(S.CHAT_CHANNEL_ID);
                    String username = jsonInfo.getString(S.CHAT_SENDER_NAME);
                    String content = jsonInfo.getString(S.CHAT_MESSAGE_TEXT);
                    String image = jsonInfo.getString(S.CHAT_SENDER_IMAGE);
                    String userId = jsonInfo.getString(S.CHAT_SENDER_ID);
                    String date = jsonInfo.getString(S.CHAT_DATE);
                    String timebin = jsonInfo.getString(S.CHAT_TIMEBIN);
                    String chattype = jsonInfo.getString(S.CHAT_TYPE);
                    String msgId = jsonInfo.getString(S._id);

                    if (!databaseManager.checkMessageAlreayExitOrNot(msgId)) {
                        BeanMessage beanMessage = new BeanMessage();
                        beanMessage.setChannelId(roomId);
                        beanMessage.setMessageText(content);
                        beanMessage.setMessageDate(date);
                        beanMessage.setMessageTimebin(timebin);
                        beanMessage.setMessageSenderId(userId);
                        beanMessage.setMessageSenderName(username);
                        beanMessage.setMessageSenderImage(image);
                        beanMessage.setMessageReciverId(MySharedPreferences.getPreferences(getApplicationContext(), S.user_id));
                        beanMessage.setMessageReciverImage(MySharedPreferences.getPreferences(getApplicationContext(), S.image));
                        beanMessage.setMessageReciverName(MySharedPreferences.getPreferences(getApplicationContext(), S.firstName_response) + " " + MySharedPreferences.getPreferences(getApplicationContext(), S.lastName_response));
                        beanMessage.setChatType(chattype);
                        beanMessage.setMessageId(msgId);

                        if (chattype.compareToIgnoreCase("G") == 0) {
                            notificationTitle = username + " @ " + jsonInfo.getString(S.CHAT_GROUP_NAME);
                            userId = jsonInfo.getString(S.CHAT_GROUP_ID);
                            username = jsonInfo.getString(S.CHAT_GROUP_NAME);
                            image = jsonInfo.getString(S.CHAT_GROUP_IMAGE);
                        } else {
                            notificationTitle = username;
                        }
                        jsonInfo.put("notificationTitle",notificationTitle);

                        if (FastLapApplication.isActivityVisible()) {
                            if (FastLapApplication.mCurrentContext instanceof ChatActivity) {
                                ((ChatActivity) FastLapApplication.mCurrentContext).onRecieveMsg(jsonInfo);
                                return;
                            }
                            //databaseManager.chatSaveLastMessageInDB(roomId, content, userId, username, image, date, 0, chattype);
                        } else {
                          /*  FastLapApplication fastLapApplication = (FastLapApplication) getApplication();
                            fastLapApplication.openInnerPNDialog(beanMessage);*/
                            databaseManager.chatSaveInDB(beanMessage);
                            databaseManager.chatSaveLastMessageInDB(roomId, content, userId, username, image, date, 1, chattype, "O");

                            if (FastLapApplication.mCurrentContext instanceof ChatListActivity) {
                                ((ChatListActivity) FastLapApplication.mCurrentContext).RefreshData();
                            }


                        }

                    } else {
                        if (chattype.compareToIgnoreCase("G") == 0) {
                            notificationTitle = username + " @ " + jsonInfo.getString(S.CHAT_GROUP_NAME);
                            userId = jsonInfo.getString(S.CHAT_GROUP_ID);
                            username = jsonInfo.getString(S.CHAT_GROUP_NAME);
                            image = jsonInfo.getString(S.CHAT_GROUP_IMAGE);
                        } else {
                            notificationTitle = username;
                        }

                        if (FastLapApplication.isActivityVisible()) {
                            databaseManager.chatSaveLastMessageInDB(roomId, content, userId, username, image, date, 0, chattype, "O");
                            return;
                        } else {
                            databaseManager.chatSaveLastMessageInDB(roomId, content, userId, username, image, date, 1, chattype, "O");
                        }


                        if (FastLapApplication.mCurrentContext instanceof ChatListActivity) {
                            ((ChatListActivity) FastLapApplication.mCurrentContext).RefreshData();
                        }
                    }


                    Intent notificationIntent = new Intent(this, ChatActivity.class);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addParentStack(DashboardActivity.class);
                    Bundle notification_bundle = new Bundle();
                    notification_bundle.putString(S.friendId, roomId);
                    notification_bundle.putString(S.userId, userId);
                    notification_bundle.putString(S.username, username);
                    notification_bundle.putString(S.image, image);
                    notification_bundle.putString(S.CHAT_TYPE, chattype);
                    notificationIntent.putExtras(notification_bundle);
                    stackBuilder.addNextIntent(notificationIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                    NotificationCompat.Builder notificationBuilder;


                    String notificationMessage = content;

                    notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.noti_icon)
                            .setContentTitle(notificationTitle)
                            .setContentText(Html.fromHtml(notificationMessage))
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setSubText("1" + " " + getString(R.string.new_message))
                            .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(notificationMessage)))
                            .setContentIntent(resultPendingIntent);


                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(id /* ID of notification */, notificationBuilder.build());

                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage());
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private String getCuurentTopActivity() {
        ActivityManager mActivityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> RunningTask = mActivityManager.getRunningTasks(1);
        ActivityManager.RunningTaskInfo ar = RunningTask.get(0);
        return ar.topActivity.getClassName().toString();
    }

}
